# Advent of code 2022

This is my first time blogging my way through AoC; I posted each day on our corporate slack #AoC channel and I am capturing those posts here.  Thanks for reading :))


## day01-calorie-counting

Ok, so aoc is over; but I generally don't solve the puzzles in Dec ... too busy.  I just solved day 1 which,
as per usual, is quite easy.  I'm using haskell b/c of the expressive power of it.  Part 1 is a 1 liner:


```haskell
main = interact $ show . maximum . map (sum . map str2int) . groupBy (\a b -> "" /= b) . lines
```

and part 2 is also a 1 liner:


```haskell
main = interact $ show . sum . take 3 . reverse . sort . map (sum . map str2int) . groupBy (\a b -> "" /= b) . lines
```


## day02-rock-paper-scissors

Day 2 was still easy, but a notch up from day 1.  Typical AOC.  Fun day tho.  Instead of posting code, here is my
repo: <a href="https://gitlab.com/rstromlund/advent-of-code/-/tree/master/2022">https://gitlab.com/rstromlund/advent-of-code/-/tree/master/2022</a>

It was "basically" a 1 liner w/ a couple of helper functions to translate play combos to score values.


## day03-rucksack-reorganization

day 3 done, getting into grouping, intersections, etc... Ramping up the string and set processing.


## day04-camp-cleanup

Day 4 is done, not too complex logic yet (find overlapping ranges) but the text input is getting slightly more
complex.  I introduced parsing today instead of using string chopping functions.


## day05-supply-stacks

Day 5 is done, still not complex logic; but I had to dust off my haskell parsing to get the day done.

These first few days, like normal, are exercising your data structure skills.  Algorithm kung-foo is soon to come I'm sure.


## day06-tuning-trouble

Day 6 is in the books, just finding unique character sequences.  No difficult parsing or algorithms yet.


## day07-no-space-left-on-device

Day 7 is done, ramping up the data structure complexity.  Needed n-way tree for this one to represent a disk
w/ files and directories.  Then some recursion to calculate directory sizes.  I think this is the first day of
"hard core" recursion was required.


## day08-treetop-tree-house

Day 8 is done, easier than day 7 IMO.  But the difficulty isn't always linear. : p

This is just searching compass directions in a 2-d space.


## day09-rope-bridge

Day 9 is done. You're tracking a rope move in a 2d grid.  Think more of the old snake game where the tail follows the head.

Working with grids (2d, 3d, finite, infinite, etc...) is classic AoC.  And part 2 is a classic escalation
where part 1 you track 1 head and 1 tail, part 2 you track 1 head and 9 "tails" (called knots but basically
joints in the snake).

Another classic AoC move is to give you the algorithm, the rules the rope/snake must follow.  Later on there
should be problems you have to choose the algorithm/solution on your own.


## day10-cathode-ray-tube

Day 10 is done. Our first VM of the year; I'm sure more will follow.  A simple 2 instruction machine with (warning spoilers) a 40x6 pixel display.

Just emulate the machine (counting cycles correctly along the way) and (in part 2) read the display for your answer.  Not too bad ... yet.


## note

Someone went through every <a href="https://twitter.com/search?q=%23AdventOfCode">#AdventOfCode</a> puzzle and categorized them by CS topic: 
<a href="https://www.reddit.com/r/adventofcode/comments/z0vmy0/350_stars_a_categorization_and_megaguide/">https://www.reddit.com/r/adventofcode/comments/z0vmy0/350_stars_a_categorization_and_megaguide/</a>


## day11-monkey-in-the-middle

Day 11 is done.  This is another typical escalation type puzzle.  Part 1 is not too difficult and Part 2 isn't
really except the rules change just enough that the numbers used to model the problem grow in size to
something like 10^800 ... which doesn't fit into an integer.  In fact trying to hold the <strong>SIZE</strong> of
that number (10^800) in memory would probably fill it up much less doing <em>operations</em> on an actual
number.

So part 2 of this puzzle requires re-reading the rules and figuring out how to represent it effectively in memory.

I hope that makes sense, I'm trying not to mention spoilers here : )

Happy AoC-ing and happy 2023.


## day12-hill-climbing-algorithm

Day 12 is done.  This is our first map using a "shortest distance algorithm" [a] between a starting point and
ending point.  Part 1 is hard-ish if you've never searched a graph/map; but it gets easier the more of these I
encounter in AoC (not really a problem you see in a data warehouse application).

Part 2 doesn't escalate the difficulty too much, just loop over part 1 more and find an optimal (i.e. least
distance) solution between multiple starting points and the ending point.

[a] - there are probably many algorithms that would foot the bill here but AoC seems fond of bringing up puzzles that fit <a href="https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm">dijkstra's algorithm</a>.


## day12-note

Just a fun note, with day 12 done we are half-way-ish : )

And as mentioned in day 9, eventually you have to take the problem and find a suitable algorithm; the
description in itself isn't enough to go on.  With a shortest path problem on Day 12, this is probably the
tipping point for difficulty this year ... we'll see.


## day13-distress-signal


Day 13 is done.  The problem sounds easy (and it isn't really nasty yet), but it requires recursion; much like
the disk drive problem on day 7.  A good head for parsing and recursion is a help, good learning opportunity
for those that do not have that tool/skill in their toolbox.

The puzzle give the rules on how to compare nested lists of integers.  You have to apply those rules in
expression evaluation on the input.  Compilers, optimizers, sql or any language really, spreadsheets; these
apps do stuff like this all the time.


## day14-regolith-reservoir

Day 14, done.  Simulate falling sand into a cave.  Follow the rules in the puzzle and test your edge cases : )

The hardest part might be choosing how to represent your cave in a data structure.

BTW, on day 12 part 2; we passed the half way mark for AoC! 😁


## day15-beacon-exclusion-zone

Day 15, done.  The problem description looks difficult, but as always picking a good data representation helps
a lot. When re-reading it and thinking about it, it looked easier. Then implementing it ... it starts to look
difficult again!  : p

You have a series of sensors and beacons spread out in a grid.  You're given a list of sensor x&amp;y positions and the x&amp;y position of the nearest beacon it can sense.

The requirement is to find the <em>gaps</em> in the sensor grid.  E.g. on row 10
how many x positions are <em>not</em> covered by a sensor sweep.

The example puzzle is a grid roughly 25x28; your puzzle input goes to at least row 2 million.  So creating a 2
dimensional array is probably not going to work.  Come up with a good data structure and the problem gets a
lot easier.


## day16-proboscidea-volcanium

Day 16 ... finally done; this is the first day I looked around for hints.  Back on day 12 I used Dijkstra's
algorithm which I only knew about because of AoC. It melted my brain a little the first time I had to
implement it; but this year it felt pretty natural.

Reading day 16 I thought I could use a modified version of Dijkstra's; but there is a fundamental difference
... day 16 requires visiting a "room" multiple time.  Dijkstra's is a shortest path finding algorithm visiting
a room only once; among other details that doesn't quite fit.

So day 16 needs <a href="https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm">Floyd–Warshall algorithm</a>.  Learning these new
algorithms and solutions is really the point of AoC.  Because, let's face it, you don't encounter problems
like these very often in enterprise technology projects.

But before looking at the reddit comments, I read the puzzle and kept it rolling around in my head for a
couple of days.  When I found the Floyd–Warshall algorithm I kept that rolling around too, it was the key that
unlocked the solution.  Once I coded Floyd–Warshall in haskell it was just a <em>depth first search</em> for
the solution from there. Not exactly simple to code a dfs ... but a great challenge and a great puzzle. : )

---

After struggling to figure this out and implementing a new (to me) algorithm; I really expected to get the
wrong answer a few times b/f getting the answer ... but both part 1 and part 2 were correct the first time! 😲

That was a nice surprise.


## day17-pyroclastic-flow

Day 17 is done!

This puzzle is of the style : make part 1 a fairly straight forward problem; maybe needing to simulate or
iterate a few thousand times.  Then in part 2 : do the same thing only a trillion times (literally
1000000000000 times).

My part 1 took a couple of seconds to run so I bumped the iterations to 1million just to see how I'd do and it
took over 2 minutes.  So to brute force this I'd have to wait 1million * 2+minutes.  Time to start thinking
outside the box.

The puzzle is kind of playing tetris with 5 rock types/shapes in a column that is only 7 squares wide.  The
puzzle explains that you are in a narrow tall chamber, there are "rocks" falling (tetris-ish pieces that look
like the horizontal I-beam, the + symbol, a backwards L, the vertical I-beam, and the square); and there are
jets in the walls that burst air that move the pieces left or right as they fall.

Part 1 : make 2022 rocks fall (just a loop of those 5 shapes) given a set of jet bursts.  Your shape cannot go
outside the walls, be pushed over another rock, and lands when it touches another rock below it (just like
tetris w/o the rotating).  The answer is how tall is your stack after 2022 rocks fall.

Part 2 : make 1trillion rocks fall according to the same rules.  This can't be simulated!  But, eventually,
the same rock will fall according to the same jet burst pattern, falling onto a stack with the same obstacles
stacked below it.  Once that happens, it will happen again.  I.e. the same rock, same jets, same obstacles
... over and over till the last rock is dropped.

The trick it to recognize the pattern, "fast forward" from one cycle of the pattern to the last one, and iterate the remaining few rocks.

I.e. if the rock/jet/skyline cycle occurs every 5,001 rocks; then there will be 199,960,000 cycles.  You can jump from an
early cycle to just before the end.  There would then be about 40 more blocks to drop to reach the 1trillion
mark.  MUCH easier &amp; faster!

But sheesh, there are so many edge cases here and is something 0 indexed or 1 indexed; I really beat my head
against the wall getting the formulas right; but it was a fun earned star :p


## day18-boiling-boulders

Day 18 is done.  I feel like this one was a tad bit simpler; maybe a breather b/f we get slammed again? :p

The puzzle is to find the surface area of a 3d object (a blob of lava flying out of our volcano).  You get
coordinates as input. E.g. (1,1,1) (2,1,1) would be 1 blob of lava 2 units wide with a surface area of
10. Think of it like 2 dice touching, the solution is to iterate over all the coordinates and only count the
sides with no dice/lava-touching neighbor (like (0,1,1) would be air to the left of our example).

I got the right answer for part 1 ... but I had this thought in the back of my head that the blob could have
air pockets in it .... enter part 2.

Part 2 : the answer in part 1 isn't quite sufficient; what about the air pockets?  That isn't really surface
area, at least not when considering only the outside surface.  Think of it like a big blog of lava with 1 die
worth taken out of the middle; there would be 6 dice/lava bits that surround the "hole" that say they have a
surface area of 1. That would make the final answer 6 too big. What about a hole 2 dice big?  What about a
tunnel of dice holes boring thru the bigger blob?

There's probably many different ways to solve that but I pulled in the Dijkstra algorithm from day 12; since
wandering paths/tunnels is what it is good at.  I calculated the surface area just like in part 1; then I
looped over a cube from (0, 0, 0) to (max x, max y, max z), not including the lava bits given to us, and ran
Dijkstra.* If Dijkstra can find a way from that air "dice" starting point to the edge of my cube it isn't a
trapped air pocket; it is a point on the outside of my blob. We ignore the air outside our blob. Otherwise if
Dijkstra got trapped and couldn't get out of this big cube; then it must be running into lava everywhere and
<em>is</em> an air pocket.  I just subtract the number of lava faces from this "die" of air inside the lava
blob.

Just like in part 1, let's consider (1,1,1) (2,1,1) but this time it is a trapped air pocket inside the lava
blob.  Now each die of "air" would have 5 lava neighbors trying to over state the surface area by 5; so the
answer is the same as part 1 minus 10.  If the air pocket was 3 dice wide, the middle die would overstate the
answer by 4, etc...

Just like almost every single day of AoC, if you pick the right data structure representation (in this case I
kept the coordinates in a Set) then the problem is much easier.  If you're wrestling with arrays or lists or
whatever trying to compare numbers or points or ___ in your puzzle, then you've probably picked a bad
in-memory representation.

* This is a brute force attack and I checked that minimum and maximum coordinates b/f starting this to make
sure the cube wouldn't be gigantic. I could also try and use Dijkstra's visitation point list and not
recalculate a few points but this works fast enough and premature optimization isn't really needed (or
wanted) in a marathon of 1-offs like AoC.


## day19-not-enough-minerals

Day 19 is finally in the books.  I had 3 delays, (1) a visit to Louisiana to see family, (2) came back to a
snow storm that knocked out power for 2 days, and (3) it was really hard!

You see geodes and want to collect them; I don't know why, it seems like a deviation while trying to save
Christmas but we'll play along.  You happen to have in your backpack 1 ore collecting robot.  Using it you can
build more ore collecting robots or clay collecting robots.  With clay and ore you can build obsidian
collecting robots, and with ore and obsidian you can build geode collecting robots ... TaDa!

Here's the scenario 1 from the test example (the actual puzzle input has 30 scenarios):

```text
Each ore robot costs 4 ore (you start with 1 ore robot).
Each clay robot costs 2 ore.
Each obsidian robot costs 3 ore and 14 clay.
Each geode robot costs 2 ore and 7 obsidian.
```

My first naive attempt was to build clay robots until we get close enough to building an obsidian robot, then
I'd suspend spending ore until we have just enough of both clay and ore.  Then resume building clay and
obsidian robots until we get close to the geode robot then I'd suspend spending ore again util we have enough
ore and obsidian to build a geode robot.  Rinse and repeat ... and ... it worked.  On the first scenario.  The
answer was, after 24 minutes you could gather 9 geodes.  Cool.

But the second test scenario (see the AoC website for specifics) wasn't that straight forward and my strategy
gathered 10 geodes when the optimum was 12.  I didn't <strong>really</strong> think the naive attempt would be
the final one, but it was worth a shot.  And I got to pick my data structures, parsing, etc... So it wasn't
all thrown away.

So the answer was going to require generating all the possibilities and finding the optimum solution.  So the
standard idea would be to do a depth-first or breadth-first search (wikipedia links below).  You have 24
minutes to build robots and collect geodes, there are 4 types of robots; so <em>if</em> you could build a
robot every minute then there would be 4^24 combinations to search; that's 281,474,976,710,656.  But you have
to have enough minerals to spend building robots so that number comes down ... but it is still a BIG search
space.

You can use other techniques to shrink the search space, I chose breadth-first search because then you know
things across the branches (see the image below).  Doing a depth-first search (with a recursive function), you
only know information top down; I thought this cross cutting knowledge could come in handy in reducing the
search space.  Plus I have coded DFS before and never BFS, of any complexity anyway; so it's another
challenge. : )

![image of a graph of nodes with a horizontal box drawn around nodes on level 3](images/2022-day19-breadth-first.png)

Coding my BFS wasn't too hard and I started getting the right answers to the test examples; but it took way
too long.  I confess I read some of the reddit discussions to see what others were using to constrain the
search space.  Day 19, like 17's tetris-esk puzzle, had edge cases and creating formulas to fast forward in
time and know how many minerals you'd have collected and when it was possible to build robots; lot's of little
details to keep juggling around in my head.  I'd code a little each day until I either got stuck or had to go
to bed and I'd wake up thinking about a fix to the next issue ... that happens a lot doing AoC ... that
happens a lot building code at work too.  : p

Part 2 reduced the # of scenarios you need to run but took the time limit up to 32 (or 4^32 or
18,446,744,073,709,551,616).  I'm sure they were testing your data structures and your search space
constraints; but it didn't really make the puzzle more difficult.

In the end it looks like DFS or BFS would have been about equal, there wasn't that much help from knowing
about the space across all the search branches; but I'm glad I have a good and complex one in my toolbox now.

Another hard earned 2 stars! : )) And we've rolled the day-counting odometer out of the teens into the
short-row 20's!

<a href="https://en.wikipedia.org/wiki/Depth-first_search">https://en.wikipedia.org/wiki/Depth-first_search</a>

<a href="https://en.wikipedia.org/wiki/Breadth-first_search">https://en.wikipedia.org/wiki/Breadth-first_search</a>


## day20-grove-positioning-system

Day 20 is done. I think Eric Wastl (created AoC and its puzzles) had pity on us; this is more of a day 5 problem.

<blockquote>You need to "mix" a list of numbers. To mix, move each number forward or backward a number of positions equal to the value of the number being moved. The list is circular, so moving a number off one end of the list wraps back around to the other end as if the ends were connected.


For example, to move the `1`  in a sequence like `4, 5, 6, 1, 7, 8, 9` , the `1`  moves one position forward: `4, 5, 6, 7, 1, 8, 9` . To move the `-2`  in a sequence like `4, -2, 5, 6, 7, 8, 9` , the `-2`  moves two positions backward, wrapping around: `4, 5, 6, 7, 8, -2, 9`.
</blockquote>

Some modulo math and you're done ... except I modeled this as a Set and my puzzle input had duplicate numbers
in the list.  D'oh.  So I created a tuple (aka. pair) of the number and its list position so I could add the
same number multiple times to my set; .... then we're done. : )


## day21-monkey-math

Day 21 is done ... mostly.  Part 1's test input looks like this:

```text
root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32
```

and you need to evaluate root.  Not too difficult, especially since I've done some parsing and evaluation
stuff in the past.  Just recursively evaluate a term until you hit a constant, then perform your operations.

Part 2 is where he tries and trip you up.  In Part 2 the "humn" term wasn't supposed to be 5; but it is a
number you the <em>human</em> pick.  And root isn't an add instruction but 2 terms that you want to be equal.
So your quest is to pick a number that makes root's 2 terms be the same value.

The example input for Part 2 evaluates to:

```text
((4 + (2 * (x - 3))) / 4) == 150
```

Solving for x = 301 to make both sides be 150.  That's easy enough to do on paper.  The equation from my
puzzle input has 72 terms; not something I want to do on paper.  There are ways to find roots of an equation
(which is basically the task here); see wikipedia links below.

But do I really want to take the time to implement a root finding solution?  To quickly get my star I copied
the monster equation into excel and used the Goal Seek tool to find my value of X. ¯\_(ツ)_/¯

I plan to revisit this puzzle and take it the rest of the way on its own; but for now .... on to day 22!

List of root finding algorithms: <a href="https://en.wikipedia.org/wiki/Root-finding_algorithms">https://en.wikipedia.org/wiki/Root-finding_algorithms</a>

Maybe I'll try to implement: <a href="https://en.wikipedia.org/wiki/Steffensen%27s_method">https://en.wikipedia.org/wiki/Steffensen%27s_method</a>


## day 21 revisit

I said I'd revisit day 21 and I did.  My first instinct was to solve for X.  Like in the sample puzzle input the simplified equation comes to:
<blockquote>((4 + (2 * (x - 3))) / 4) == 150</blockquote>

But it was late and my brain was foggy so I just pasted it (the real puzzle much longer 72 factor answer that
is) into excel and used the Goal Seek tool to get my star.

This is one I really wanted to revisit and in just about 1/2 an hour with just a couple simple functions I
solved for X w/ no excel and no root finding algorithm.  : ))

It is almost as easy and performing the opposite math operation on the right side.  E.g.

```text
x + 2 = 0
x = 0 - 2
```

Change the + to - and you're done.  Same with multiply; just change to divide.  But / and - are special.

Division:

```text
x / 2 = 10
x = 10 * 2
```

Works as expected.

```text
2 / x = 20
2 = 20 * x
2 / 20 = x
```

When x is in the denominator you have to switch things around just a little; this case doesn't turn into
multiplication.  Also for subtraction ...

Subtraction:

```text
x - 20 = 10
x = 10 + 20
```

Again, straight forward.  But:

```text
20 - x = 5
20 = 5 + x
20 - 5 = x 
```

This case doesn't turn into addition.

But since the puzzle guarantees only 1 unknown and everything else simplifies to a constant; the solution was
pretty easy once I was a little more clear headed.

There's other things that I'd thought about revisiting; but this is the one that I really wanted to do and
I'll chalk the rest up to learning and trying to solve puzzles with speed.

.... Now ... we're done w/ 2022.  😜


## day22-monkey-map

Day 22 is done! : ))

Part 1 starts out like a day 5-ish puzzle; it's a 2d map of a jungle with dot-es to walk and '#'-walls/trees
that block your way.  You have a list of instructions like walk 10 paces, turn right, 5 paces, turn left,
etc...E.g.

<pre>        ...#
        .#..
        #...
        ....
...#.......#
........#...
..#....#....
..........#.
        ...#....
        .....#..
        .#......
        ......#.

10R5L5R10L4R5L5</pre>

If you step off the map you warp to the other side of the map (like the old pacman or asteroids games).

So this is simple, right?  Just keep some sort of 2 dimensional array, track your (x,y) coordinates, and add
path blocking and warp around detection.  Easy-peasy.  And it kind of was.  Part 1 done.

Part 2 -- the map isn't a 2d map after all; you misunderstood the instructions.  It's a 3d map; think of
cutting it out and wrapping it onto a dice.  The above map is really this cutout:

<pre>        1111
        1111
        1111
        1111
222233334444
222233334444
222233334444
222233334444
        55556666
        55556666
        55556666
        55556666</pre>

So the instructions are basically the same, follow the walking and turning directions but now you don't warp
to the other side of the map.  Now you will "turn the corner" and continue walking on the adjacent die/cube
face.  E.g. when you walk to <em>the right</em> off the 1-face you step onto the 6-face <em>walking left</em>.
Each step can change <em>which face</em> you're walking on (even if it wasn't at the edge of the map, see
3-&gt;4, 1-&gt;4, etc...) and <em>the direction</em> you are walking (like the 1-&gt;6 example).

Talk about edge cases!  (<em>ahem</em>). Any way ...

This one took a little strategizing on how to represent state.  In Part 1 I tracked (x,y) coordinates over the
whole map and warped the coordinates as needed.  Now I decided to track (x,y) inside the face and to start
tracking the face number too.  Each face in the test puzzle was 4x4 so x and y can be in the range [0..3].
When either becomes -1 or 4 I know I just made a transition and I select the new face number and new
direction.  Lot's of details and transformations to make; but it worked.  : ))

What I did not do is try and make an origami solving function to figure out how to fold the map.  Maybe that
would have made the details easier ... ?  But I doubt it; I'd still have to solve how to move from one
die-face to the next.  Maybe there is a solution using modulus arithmetic .... ?  But I doubt that too.  I'm
going to go read the chat boards and see if there is a really cleaver solution; but this worked.  I'm sure
there are other ways to track the state of our "walker" thru this "jungle" but face-by-face seems logical and
<em>relatively</em> easy.

On to day 23!


## day23-unstable-diffusion


Day 23 is done!  On to the <a href="https://www.merriam-webster.com/words-at-play/what-does-penultimate-mean">penultimate</a> puzzle!

This wasn't too bad of a puzzle, I think we are easing into Christmas at this point.  The problem is very
similar to the <a href="https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life">game of life</a>; you take certain actions based
on the cells surrounding you on a 2d grid.  In this case elves can move or not move based on their neighbors.
Part 2 is simply ratcheting up the simulation to find out when it reaches a point where the board no longer
changes.  This equilibrium happens in Conway's Game of Life too; at least you can reach a point where the last
cycle repeats if not becomes static.

As always, choose a good data structure to model the input and state changes and the problem is much simpler. : ))

Happy AoC--ing!


## day24-blizzard-basin

Day 24 is d-d-dun-done!

This one wasn't too bad either; but was a challenge.  No special algorithms really, just implement the
simulation as presented.  You have to cross a valley, you are given a 2d map.  The catch is there are
blizzards that move N, S, E, and/or W and you can't be in the same place as a blizzard.  Each blizzard keeps
moving in the same direction and wraps around the valley map.

Think of it like frogger but you have to avoid obstacles moving up and down as well as left and right.  Maybe more like avoiding asteroids.  : )

The test map is, as always, small and simple and your program will run quickly on it ... but once you try the
real puzzle input if you have any <a href="https://betterprogramming.pub/big-o-notation-for-dummies-like-me-98ac2d141f9f">O(n)</a> logic
you'll be waiting a long time for your results.  Sometimes until the heat death of the universe.

Here's the test puzzle:

```text
#E######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#
```

You are the "E" in the upper left and you need to cross to the "." in the lower right ... while avoiding the
constantly moving blizzards.  To move you need to check all the compass points to see if there is nothing
there (a "."); and you need to check your current position in case you need to wait for storms to pass by.  It
is easy enough in this small map to re-compute the map each step; but you'll waste a couple cpu cycles on
coordinates that are not checked.

This might not scale tho.  So instead I used modulo arithmetic to find if a space had a blizzard or not.
E.g. on step 1 I need to look left 1 space for a right moving blizzard + look right 1 space for a left moving
blizzard + look up 1 space for a downward moving blizzard + look down 1 space for an upward moving blizzard.
That's just 4 computations instead of map-size-X * map-size-Y computations.  My puzzle input had 3162 roaming
blizzards that I'd need to move thousands of times b/f part 1 was solved; not the fastest way to do things.

Part 2 was pretty merciful; you had to go from entrance to exit point; <em>oops</em> we forgot something -&gt; go
from exit to entrance point; got it, <em>phew</em> -&gt; and finally find our way back to the exit
point.  If you wasted any cpu cycles on part 1 you'll really be waiting on part 2; otherwise just a bit of
coding to change entrance and exit points and compute each leg of the trip.

On to 🎶 dum-da-dadum 🎶 &lt;epic-echoing-voice&gt;DAY TWENTY-FIVE&lt;/epic-echoing-voice&gt;!

---

P.S. 2 things

a) in the meantime I took a trip to see family and a trip to corporate headquarters to celebrate a new application implementation;

and

2) once I created a blizzard movement solution, I used a breadth-first-search to find the desired exit point.  Thanks to day 19 on boning up on my BFS skills


## day25-full-of-hot-air

And DONE with Day 25!  Hallelujah! : ))

Day 25 is, as usual, a fairly easy puzzle so you can go join the FAM opening presents on Christmas day (if you're solving these "in read time" in December).

There's yet another Elven/Elephant/Monkey/Alien-esk computer console that needs you to input a number but the
manual doesn't use base10 but base5 ... with a few twists and turns to keep things interesting.  The number
system it uses is "Special Numeral-Analogue Fuel Units" - SNAFU for short.

So you read your puzzle input in SNAFU, sum the numbers, and display the results in SNAFU.  I guess I could
have written a SNAFU adder to get the answer directly but I wrote 2 functions 'snafu2int' and 'int2snafu'.
The rest was easy.

Thanks to <a href="http://was.tl/">Eric Wastl</a> for another fun and
challenging AoC.  I will add another post here in a day or two with my thoughts and reflections on what I've
learned.  Thanks for coming along on the ride with me; that made it all the more enjoyable.  This was the
first year I've blogged my experience and that was fun. 😁


## AoC-2022-retrospective


Lessons learned, aka. 2022 AoC Retrospective

I always learn a lot, fundamentally - be persistent.  Even when the problem is too much to even think about,
keep at it.  Even if you have to ask questions or read the AoC subreddit; keep at it.  It's like exercise, the
effort isn't always comfortable but the results are enjoyable! : ) If you have to, walk away or sleep on it;
that usually helps.  Your brain works on problems subconsciously and you might be surprised what good a break
can do.

There were a couple of puzzles where Dijkstra's algorithm was the right solution and I was pleased how easily
it was to use this year.  The puzzles that used Dijkstra's before was a brain melter for me, this year not so
much.  Nice.

All the days were fun and hit on different skills; some thoughts about a few of the days:

Day 16 was, at the heart of it, a graphing problem (you had to go from room to room opening valves that took
various amounts of time).  I didn't have the tools in my toolbox to solve this one so I checked out the AoC
subreddit and found people were using Floyd–Warshall algorithm; that's a new one on me.  I used the Wikipedia
article to implement a 'floydWarshall' function in Haskell; it was just 11 lines.  And then I just used a
depth-first search on its output.  This was a brain melter for this year ... maybe next year I'll just whip
out a Floyd–Warshall like it was nothing ... maybe.

Day 17 was a tetris-esk puzzle and in part 2 you had to drop 1,000,000,000,000 (10^12) pieces.  Not something
you want to simulate!  The key was that eventually the "game board" would repeat itself and you could use that
to fast-forward to the last repetition and simulate just the last few rocks being dropped.  There are problems
you'll need to solve even in enterprise software engineering that seem huge but when you look at it closely
there is a "cycle" to it.  You might only have to solve the bit leading up to the cycle, the cycle one time,
and the bit after the cycle.  Sorry if that's vague but I don't have a good example (besides AoC 2022 day 17 😜) to illustrate it.

Day 19 was where you had an ore mining robot; using ore you could build a clay mining robot; using clay and
ore you could build an obsidian mining robot; using ore and obsidian you could build a geode mining robot.
The goal was to find the optimum usage of your robots and resources to get the maximum amount of geodes.  This
one taught me breadth-first search.  I started with breadth-first search where I was generating too many
possible outcomes; that is I was generating a lot of dead-ends that I really didn't need to follow.  Once I
started generating possible outcomes in a better/smarter way; that helped a lot.  For example if it takes 4
ore to build a clay mining robot I don't really care about steps 1, 2, or 3; just jump forward to 4 and track
options from there.  Once you have a list of outcomes, some (like building clay robots all the time and never
saving up ore for obsidian or geode robots) will be suboptimal.  The key is finding a good pruning test to
kill of the unfruitful branches.  This BFS lesson will come in handy again soon.

Day 22 in part 1 you had a 2d map you had to walk around but in part 2 you discovered that it was a really a
flat "cut-out" map of a cube.  So you had to translate each of the 6 sides, their connections to the other
sides, and their orientation in 3 dimensional space.  This wasn't particularly hard as it was time consuming
and <em>detailed</em>.  I didn't really want to write an origami folding search function; so I looked at my
puzzle input and wrote my solution directly to that.  Once I had my 2 stars I read the subreddit, curious how
others tackled the problem.  I found several people <em>did</em> write an origami folding solution.  Then I
wondered how the leaders on the leaderboard solved it; the code I saw from them solved their particular puzzle
input too.  Maybe writing a "pattern folding solution" was either fun or the only way others thought to
approach it.  But I found it comforting that others took the direct approach like I did.  : p

Day 24 was simulating a map again but this time you had blizzards moving north or south or east or west.
I.e. these storms were <a data-stringify-link="https://en.wikipedia.org/wiki/Deterministic_algorithm"
href="https://en.wikipedia.org/wiki/Deterministic_algorithm" rel="noopener noreferrer">deterministic</a>
(def: given a particular input, will always produce the same output).  That means given 1 cycle you can always
tell where the blizzards will be; given 50 cycles you could tell where the blizzards would be.  So instead of
simulating a large board with a bunch of blizzards on it; I could instead ask "after 50 cycles what is in
square (4,3)"; and that would just take a few calculations instead of thousands.  So day 24 taught me not to
do unnecessary work.

I could keep going ... but then I'd probably just repeat all my posts above.  But does this really translate
into meaningful learning?  Can you apply this to the enterprise world of software engineering?  Absolutely.
All these puzzles are solved by programming and algorithms and reasoning that come in useful.  Take the
Floyd–Warshall algorithm for example; sql database servers evaluate your query given everything they know
about your data to produce the best and most efficient execution plan.  Think of your <em>from</em>, <em>join
</em>, and <em>where</em> clauses as a graph (which they really are) and Floyd–Warshall can help by "finding
shortest paths in a directed weighted graph with edge weights".  I.e. how can I get from the Customer table to
the Order table to the Product table to the Payment table the easiest and fastest using indexes, keys, table
scans, caches, etc...?  Let Floyd–Warshall help you.  : )

When I started AoC in 2020 I chose Haskell as my language because I wanted to know it better; and I sure do
now ... but I still have a lot to learn about it.  I am becoming more comfortable and more fluent in Haskell
which is exactly what I wanted.  I need to learn a more natural Haskell-y way to pass state around with the
State monad.  To learn parsing in Haskell I went back and solved AoC 2015 using parsing libraries.  Haskell is
a champ at parsing.  I used parsing a lot this year and it wasn't a big deal; I learned how to improve on my
2015 parsing efforts ... so another win!

Since I have now finished 2015, 2020, 2021, and 2022; maybe I'll go back and solve 2016 and use State and
other monads ... hmmmm.  You've heard of Christmas in July?  Maybe I'll have an Advent of Code in July. 🤔

I've come to love the expressiveness of Haskell and its lazy evaluation.  But that is off topic for now ... so
much to say there and this isn't the place for Haskell gushing.

So thanks for coming on this adventure with me and I hope y'all had fun and maybe inspired to at least try a
day or two worth of puzzles to see how you like it.  As for me, I'm going to veg out in front of the tv for
once instead of picking up my laptop.  Ahhhhh : ))

{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Data.Bifunctor (first)
import Data.Char (isDigit)
import Data.Functor (($>))
import Data.List (dropWhile, groupBy, sortBy)
import Data.Ord (Ordering(LT, EQ, GT))
import Data.List.Split (chunksOf)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, lowerChar, newline, space, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)
import Debug.Trace (trace)
trace_ = trace
-- trace_ _ x = x

{-
... TODO DESCR

Test:
	% cd ../.. && echo -e '[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]' | cabal run YYYY_solution-template # = ?
-}

data PuzzleList = PList [PuzzleList] | PInt Int deriving (Show)

type Parser = Parsec Void String

plists :: Parser [PuzzleList]
plists = many (plist <* space) <* eof

plist :: Parser PuzzleList
plist = choice
  [ char '[' *> (PList <$> (plist `sepBy` char ',')) <* char ']'
  , PInt <$> decimal
  ]

solve :: Either a [PuzzleList] -> Int
solve (Right ps) = trace_ (show ("solve", "ps", ps)) $ length ps


main :: IO ()
main = interact $ show . solve . runParser plists "<stdin>"

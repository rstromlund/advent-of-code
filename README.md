# Advent of code 2020

Haskell solutions to advent of code
https://adventofcode.com/

## Caveat

I am a beginning haskell/fp programmer; please don't take these as good idomatic haskell code. If you
have improvements and/or input please open an issue or better yet file a PR!

Thx.

## Ref

Other haskell solutions:

 * https://github.com/Bogdanp/awesome-advent-of-code#haskell

## Install dependencies

Add to ~/.stack/global-project/stack.yaml; e.g.

```yaml
extra-deps:
- megaparsec-9.2.2
```

And then:

```bash
stack build megaparsec-9.2.2
```

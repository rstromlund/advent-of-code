#!/bin/bash

set -x
typeset -r YEAR="${PWD##*/}"
typeset DAY='e.g. 01'

for DAY in "${@}" ; do
	[[ -r "${DAY}a.html" && ! -r "${DAY}b.html" ]] && curl --cookie-jar 'cookies.txt' --cookie 'cookies.txt' --output "${DAY}b.html" "https://adventofcode.com/${PWD##*/}/day/${DAY#0}"

	[[ -r "${DAY}a.html" ]] || curl --cookie 'cookies.txt' --output "${DAY}a.html" "https://adventofcode.com/${PWD##*/}/day/${DAY#0}"
	[[ -r "${DAY}a.txt"  ]] || curl --cookie 'cookies.txt' --output "${DAY}a.txt" "https://adventofcode.com/${PWD##*/}/day/${DAY#0}/input"

	# E.g. 'day01-trebuchet.hs'
	if ! ls "day${DAY}"*'.hs' ; then
		echo "Create HS templates for day ${DAY}"
		typeset title=$(grep "<h2>-* Day ${DAY#0}: .* -*</h2>" "${DAY}a.html" \
			| sed -E -e "s!^.*<h2>-* Day ${DAY#0}: !!" -e 's! -*</h2>.*$!!' \
			| tr '[:upper:]' '[:lower:]' \
			| tr ' ' '-'
		)
		typeset part1="day${DAY}-${title}.hs"
		typeset part2="day${DAY}-${title}_b.hs"
		sed -e "s!YYYY_solution-template!${YEAR}_${part1%.hs}!g" < '../../solution-template.hs' > "${part1}"
		sed -e "s!YYYY_solution-template!${YEAR}_${part2%.hs}!g" < '../../solution-template.hs' > "${part2}"
		chmod 755 "${part1}" "${part2}"

		sed -i -e '/exit \${rc}/d' -E -e 's!^(echo|time)!#\1!' 'run-all.bash'

		echo "echo -e \"\n\n==== day${DAY}\"
time cabal run ${YEAR}_${part1%.hs}   < \"src/${YEAR}/${DAY}a.txt\" ; (( rc += 0 ))
time cabal run ${YEAR}_${part2%.hs} < \"src/${YEAR}/${DAY}a.txt\" ; (( rc += 0 ))

exit \${rc}" >> 'run-all.bash'

		echo "
executable ${YEAR}_${part1%.hs}
  import:         aoc
  main-is:        ${YEAR}/${part1}
  build-depends:  megaparsec >= 9.7.0

executable ${YEAR}_${part2%.hs}
  import:         aoc
  main-is:        ${YEAR}/${part2}
  build-depends:  megaparsec >= 9.7.0" >> '../../advent-of-code.cabal'

	fi
done

{-# OPTIONS_GHC -Wno-unused-top-binds #-}
{-# LANGUAGE MagicHash, UnboxedTuples #-}
module Point where

{- The advanced type class code borrowed from 'https://github.com/glguy/advent/blob/main/common/src/Advent/Coord.hs'; thanks glguy! -}

import Control.Monad.ST (runST, stToIO)
import Data.Array.Base (IArray, MArray, STUArray(STUArray), UArray(UArray), bounds, getBounds, getNumElements, newArray, newArray_, numElements, unsafeAccum, unsafeAccumArray, unsafeAccumArrayUArray, unsafeAccumUArray, unsafeAt, unsafeArray, unsafeArrayUArray, unsafeNewArray_, unsafeNewArraySTUArray_, unsafeRead, unsafeReplace, unsafeReplaceUArray, unsafeWrite, wORD_SCALE)
import Data.Array.IO.Internals (IOUArray(IOUArray))
import Data.Array.Unboxed (Array, listArray)
import GHC.Exts (Int(I#), (+#), (*#), indexIntArray#, readIntArray#, writeIntArray#)
import GHC.Ix (Ix(unsafeIndex, range, index, inRange, unsafeRangeSize), indexError)
import GHC.ST (ST(ST))

data Point = Point !Int !Int -- asumed to be in (Y,X) or (DY,DX) order
  deriving (Read, Show, Ord, Eq)

type PuzzleMap = Array Point Char

parseMap :: String -> PuzzleMap
parseMap cs = listArray (Point 0 0, Point ly lx) . concat $ ls
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls

-- | Y (row) of Point
pointY :: Point -> Int
pointY (Point y _) = y

-- | X (column) of Point
pointX :: Point -> Int
pointX (Point _ x) = x

north :: Point
north = Point (-1) 0

east :: Point
east = Point 0 1

south :: Point
south = Point 1 0

west :: Point
west = Point 0 (-1)

cardinalDirections :: [Point]
cardinalDirections = [north, east, south, west]

northeast :: Point
northeast = Point (-1) 1

southeast :: Point
southeast = Point 1 1

southwest :: Point
southwest = Point 1 (-1)

northwest :: Point
northwest = Point (-1) (-1)

diagonalDirections :: [Point]
diagonalDirections = [northeast, southeast, southwest, northwest]

ordinalDirections :: [Point]
ordinalDirections = [north, northeast, east, southeast, south, southwest, west, northwest]

neighbors :: [Point] -> Point -> [Point]
neighbors dirs c = c `seq` map (c +) dirs

turnLeft :: Point -> Point
turnLeft  (Point y x) = Point (-x) y

turnRight :: Point -> Point
turnRight (Point y x) = Point x (-y)

turnAround :: Point -> Point
turnAround (Point y x) = Point (-y) (-x)

manhattan :: Point -> Point -> Int
manhattan a b = norm1 (a - b)

norm1 :: Point -> Int
norm1 (Point y x) = abs y + abs x

mapPoint :: (Int -> Int) -> Point -> Point
mapPoint f (Point y x) = Point (f y) (f x)

zipPoint :: (Int -> Int -> Int) -> Point -> Point -> Point
zipPoint f (Point y1 x1) (Point y2 x2) = Point (f y1 y2) (f x1 x2)


instance Num Point where
  (+) = zipPoint (+)
  {-# INLINE (+) #-}
  (-) = zipPoint (-)
  {-# INLINE (-) #-}
  (*) = zipPoint (*)
  {-# INLINE (*) #-}
  negate = mapPoint negate
  {-# INLINE negate #-}
  abs = mapPoint abs
  {-# INLINE abs #-}
  signum = mapPoint signum
  {-# INLINE signum #-}
  fromInteger = (\i -> Point i i) . fromInteger
  {-# INLINE fromInteger #-}


instance Ix Point where
  unsafeIndex (Point lorow locol, Point hirow hicol) (Point row col) =
    unsafeIndex (lorow,hirow) row * unsafeRangeSize (locol,hicol) + unsafeIndex (locol,hicol) col
  {-# INLINE unsafeIndex #-}

  index b i
    | inRange b i = unsafeIndex b i
    | otherwise   = indexError b i "Point"
  {-# INLINE index #-}

  inRange (Point lorow locol, Point hirow hicol) (Point row col) =
    inRange (lorow,hirow) row && inRange (locol,hicol) col
  {-# INLINE inRange #-}

  range (Point lorow locol, Point hirow hicol) =
    [Point row col | row <- [lorow..hirow], col <- [locol..hicol]]
  {-# INLINE range #-}

  unsafeRangeSize (Point lorow locol, Point hirow hicol) =
    (hirow - lorow + 1) * (hicol - locol + 1)
  {-# INLINE unsafeRangeSize #-}


instance IArray UArray Point where
  {-# INLINE bounds #-}
  bounds (UArray l u _ _) = (l,u)
  {-# INLINE numElements #-}
  numElements (UArray _ _ n _) = n
  {-# INLINE unsafeArray #-}
  unsafeArray lu ies = runST (unsafeArrayUArray lu ies 0)
  {-# INLINE unsafeAt #-}
  unsafeAt (UArray _ _ _ arr#) (I# i#) =
    Point (I# (indexIntArray# arr# (2# *# i#)))
          (I# (indexIntArray# arr# (2# *# i# +# 1#)))
  {-# INLINE unsafeReplace #-}
  unsafeReplace arr ies = runST (unsafeReplaceUArray arr ies)
  {-# INLINE unsafeAccum #-}
  unsafeAccum f arr ies = runST (unsafeAccumUArray f arr ies)
  {-# INLINE unsafeAccumArray #-}
  unsafeAccumArray f initialValue lu ies = runST (unsafeAccumArrayUArray f initialValue lu ies)


instance MArray (STUArray s) Point (ST s) where
    {-# INLINE getBounds #-}
    getBounds (STUArray l u _ _) = return (l,u)
    {-# INLINE getNumElements #-}
    getNumElements (STUArray _ _ n _) = return n
    {-# INLINE unsafeNewArray_ #-}
    unsafeNewArray_ (l,u) = unsafeNewArraySTUArray_ (l,u) (\x -> 2# *# wORD_SCALE x)
    {-# INLINE newArray_ #-}
    newArray_ arrBounds = newArray arrBounds 0
    {-# INLINE unsafeRead #-}
    unsafeRead (STUArray _ _ _ marr#) (I# i#) = ST $ \s1# ->
        case readIntArray# marr# (2# *# i#      ) s1# of { (# s2#, y# #) ->
        case readIntArray# marr# (2# *# i# +# 1#) s2# of { (# s3#, x# #) ->
        (# s3#, Point (I# y#) (I# x#) #) }}
    {-# INLINE unsafeWrite #-}
    unsafeWrite (STUArray _ _ _ marr#) (I# i#) (Point (I# y#) (I# x#)) = ST $ \s1# ->
        case writeIntArray# marr# (2# *# i#      ) y# s1# of { s2# ->
        case writeIntArray# marr# (2# *# i# +# 1#) x# s2# of { s3# ->
        (# s3#, () #) }}


instance MArray IOUArray Point IO where
    {-# INLINE getBounds #-}
    getBounds (IOUArray arr) = stToIO (getBounds arr)
    {-# INLINE getNumElements #-}
    getNumElements (IOUArray arr) = stToIO (getNumElements arr)
    {-# INLINE newArray #-}
    newArray lu initialValue = stToIO (IOUArray <$> newArray lu initialValue)
    {-# INLINE unsafeNewArray_ #-}
    unsafeNewArray_ lu = stToIO (IOUArray <$> unsafeNewArray_ lu)
    {-# INLINE newArray_ #-}
    newArray_ = unsafeNewArray_
    {-# INLINE unsafeRead #-}
    unsafeRead (IOUArray marr) i = stToIO (unsafeRead marr i)
    {-# INLINE unsafeWrite #-}
    unsafeWrite (IOUArray marr) i e = stToIO (unsafeWrite marr i e)

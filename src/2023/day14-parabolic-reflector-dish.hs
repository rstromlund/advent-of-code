module Main where
import Data.Ord (Down(..))
import Data.HashMap.Strict as M (HashMap, (!?), delete, filter, fromList, insert, keys)
import Data.List as L (filter, sortOn)
import Data.List.Extra (sumOn')
import Data.Maybe (isJust)

--	... The rounded rocks (O) will roll when the platform is tilted, while the cube-shaped rocks (#) will
--	stay in place. You note the positions of all of the empty spaces (.) and rocks (your puzzle
--	input). For example:
--
--	O....#....
--	O.OO#....#
--	.....##...
--	OO.#O....O
--	.O.....O#.
--	O.#..O.#.#
--	..O..#O..O
--	.......O..
--	#....###..
--	#OO..#....
--
--	Start by tilting the lever so all of the rocks will slide north as far as they will go:
--
--	OOOO.#.O..
--	OO..#....#
--	OO..O##..O
--	O..#.OO...
--	........#.
--	..#....#.#
--	..O..#.O.O
--	..O.......
--	#....###..
--	#....#....
--
--	You notice that the support beams along the north side of the platform are damaged; to ensure the
--	platform doesn't collapse, you should calculate the total load on the north support beams.
--
--	The amount of load caused by a single rounded rock (O) is equal to the number of rows from the rock to
--	the south edge of the platform, including the row the rock is on. (Cube-shaped rocks (#) don't
--	contribute to load.) So, the amount of load caused by each rock in each row is as follows:
--
--	OOOO.#.O.. 10
--	OO..#....#  9
--	OO..O##..O  8
--	O..#.OO...  7
--	........#.  6
--	..#....#.#  5
--	..O..#.O.O  4
--	..O.......  3
--	#....###..  2
--	#....#....  1
--
--	The total load is the sum of the load caused by all of the rounded rocks. In this example, the total load is 136.
--
--	Tilt the platform so that the rounded rocks all roll north. Afterward, what is the total load on the north support beams?
--
-- Test:
--	% cd ../.. && echo -e 'O....#....\nO.OO#....#\n.....##...\nOO.#O....O\n.O.....O#.\nO.#..O.#.#\n..O..#O..O\n.......O..\n#....###..\n#OO..#....' | cabal run 2023_day14-parabolic-reflector-dish # = 136

type Point = (Int, Int) -- Y,X

mkMap :: [String] -> (Point, HashMap Point Char)
mkMap gs = ((ly, lx),) . fromList . L.filter (('.' /=) . snd) . zip [pt | y <- [ly,ly-1..1], x <- [0..lx-1], let pt = (y, x)] . concat $ gs
  where ly = length gs
        lx = length . head $ gs

getRollers :: HashMap Point Char -> [Point]
getRollers = sortOn Down . keys . M.filter ('O' ==)

move :: (Point, HashMap Point Char) -> [Point] -> HashMap Point Char
move ((maxY, maxX), m) = foldl' move' m
  where move' :: HashMap Point Char -> Point -> HashMap Point Char
        move' m pt@(y, x)
          | y >= maxY = m
          | isBlocked = m
          | otherwise = move' (insert pt' 'O' (delete pt m)) pt'
          where pt' = (y + 1, x)
                isBlocked = isJust $ m !? pt'

solve :: (Point, HashMap Point Char) -> Int
solve i@(_, m) = sumOn' fst . getRollers . move i . getRollers $ m


main :: IO ()
main = interact $ show . solve . mkMap . lines

module Main where
import Control.Monad (liftM2)
import Control.Monad.Memo (MonadMemo, memo, startEvalMemo)
import Data.Bifunctor (bimap, first)
import Data.List (dropWhile, group, intercalate, nub, splitAt)
import Data.List.Split (splitOn)
import qualified Data.Map.Strict as M ((!?), Map, empty, insert)
import Data.Maybe (fromMaybe)

--	... To unfold the records, on each row, replace the list of spring conditions with five copies of itself (separated by ?) and replace the list of contiguous groups of damaged springs with five copies of itself (separated by ,).
--
--	So, this row:
--
--	.# 1
--
--	Would become:
--
--	.#?.#?.#?.#?.# 1,1,1,1,1
--
--	The first line of the above example would become:
--
--	???.###????.###????.###????.###????.### 1,1,3,1,1,3,1,1,3,1,1,3,1,1,3
--
--	In the above example, after unfolding, the number of possible arrangements for some rows is now much larger:
--
--	???.### 1,1,3 - 1 arrangement
--	.??..??...?##. 1,1,3 - 16384 arrangements
--	?#?#?#?#?#?#?#? 1,3,1,6 - 1 arrangement
--	????.#...#... 4,1,1 - 16 arrangements
--	????.######..#####. 1,6,5 - 2500 arrangements
--	?###???????? 3,2,1 - 506250 arrangements
--	After unfolding, adding all of the possible arrangement counts together produces 525152.
--
--	Unfold your condition records; what is the new sum of possible arrangement counts?
--
-- Test:
--	% cd ../.. && echo -e '???.### 1,1,3\n.??..??...?##. 1,1,3\n?#?#?#?#?#?#?#? 1,3,1,6\n????.#...#... 4,1,1\n????.######..#####. 1,6,5\n?###???????? 3,2,1' | cabal run 2023_day12-hot-springs_b # = 525152

type Input = (String, [Int])

parse :: [String] -> [Input]
parse = map (parse' . words)
  where parse' [cs, ns] = (cs, map read . splitOn "," $ ns)

match :: (MonadMemo Input Int m) => Input -> m Int
match (cs@(c:cs'), ns@(n:ns')) = do
  let op  | '#' /= c             = memo match (cs', ns)
              -- ^^ a '.' or '?' can both be operative/functional (only '#' is certainly damaged/non-functional)
          | otherwise            = return 0
  let dmg | '.' /= c  && dmgCond = memo match (drop (n + 1) cs, ns')
              -- ^^ a '#' or '?' can both be damaged (only '.' is undamaged)
          | otherwise            = return 0
  liftM2 (+) op dmg
  where dmgCond = -- There has to be enough springs left for our number in the lab notes
                  n <= length cs &&
                  -- And the next "n" springs must be damaged (to match the notes) and the next one cannot be damaged (or the note would be wrong)
                    (uncurry (&&) . bimap ('.' `notElem`) (\rest -> n == length cs || '#' /= head rest) . splitAt n $ cs)
match ([], ns) = return $ if null ns then 1 else 0
match (cs, []) = return $ if '#' `notElem` cs then 1 else 0

solve :: [Input] -> Int
solve = sum . map (\(cs, ns) -> startEvalMemo $ match (intercalate "," . replicate 5 $ cs, concat . replicate 5 $ ns))


main :: IO ()
main = interact $ show . solve . parse . lines

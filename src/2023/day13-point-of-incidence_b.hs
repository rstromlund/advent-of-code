module Main where
import Data.List ((\\), nub, tails, transpose)
import Data.List.Split (splitOn)

--	... Upon closer inspection, you discover that every mirror has exactly one smudge: exactly one . or # should be the opposite type.
--
--	In each pattern, you'll need to locate and fix the smudge that causes a different reflection line to
--	be valid. (The old reflection line won't necessarily continue being valid after the smudge is fixed.)
--
--	Here's the above example again:
--
--	#.##..##.
--	..#.##.#.
--	##......#
--	##......#
--	..#.##.#.
--	..##..##.
--	#.#.##.#.
--
--	#...##..#
--	#....#..#
--	..##..###
--	#####.##.
--	#####.##.
--	..##..###
--	#....#..#
--
--	The first pattern's smudge is in the top-left corner. If the top-left # were instead ., it would have
--	a different, horizontal line of reflection:
--
--	1 ..##..##. 1
--	2 ..#.##.#. 2
--	3v##......#v3
--	4^##......#^4
--	5 ..#.##.#. 5
--	6 ..##..##. 6
--	7 #.#.##.#. 7
--
--	With the smudge in the top-left corner repaired, a new horizontal line of reflection between rows 3
--	and 4 now exists. Row 7 has no corresponding reflected row and can be ignored, but every other row
--	matches exactly: row 1 matches row 6, row 2 matches row 5, and row 3 matches row 4.
--
--	In the second pattern, the smudge can be fixed by changing the fifth symbol on row 2 from . to #:
--
--	1v#...##..#v1
--	2^#...##..#^2
--	3 ..##..### 3
--	4 #####.##. 4
--	5 #####.##. 5
--	6 ..##..### 6
--	7 #....#..# 7
--
--	Now, the pattern has a different horizontal line of reflection between rows 1 and 2.
--
--	Summarize your notes as before, but instead use the new different reflection lines. In this example,
--	the first pattern's new horizontal line has 3 rows above it and the second pattern's new horizontal
--	line has 1 row above it, summarizing to the value 400.
--
--	In each pattern, fix the smudge and find the different line of reflection. What number do you get
--	after summarizing the new reflection line in each pattern in your notes?
--
-- Test:
--	% cd ../.. && echo -e '#.##..##.\n..#.##.#.\n##......#\n##......#\n..#.##.#.\n..##..##.\n#.#.##.#.\n\n#...##..#\n#....#..#\n..##..###\n#####.##.\n#####.##.\n..##..###\n#....#..#' | cabal run 2023_day13-point-of-incidence_b # = 400

type Map = [String]
type Input = [Map]

type Mirrors = [(Int, String)]

parse :: String -> Input
parse = map lines . splitOn "\n\n"

type LineNbrdString = (Int, String)

potentialSmudge :: [String] -> [(LineNbrdString, LineNbrdString)]
potentialSmudge = map snd . concatMap (filter ((1 ==) . fst) . findDiffBy1) . tails . zip [0 .. ]
  where findDiffBy1 :: [LineNbrdString] -> [(Int, (LineNbrdString, LineNbrdString))]
        findDiffBy1 (h@(_,s1):ts@(_:_)) = map (\t@(_,s2) -> (length . filter not . zipWith (==) s1 $ s2, (h, t))) ts
        findDiffBy1 _ = []

rotate90 = map reverse . transpose

hunt :: Int -> Map -> [Int]
hunt mul (m:ms) = hunt' [m] ms
  where hunt' :: Map -> Map -> [Int]
        hunt' ls@(l:_) rs@(r:rs')
          | l == r && (and . zipWith (==) ls $ rs)
                      = (mul * length ls):hunt' (r:ls) rs'
          | otherwise = hunt' (r:ls) rs'
        hunt' _ _ = []

updateList n s ls = let (l,_:ls') = splitAt n ls
                    in l ++ s:ls'

wipe :: Map -> (LineNbrdString, LineNbrdString) -> Map
wipe ls ((n1, l1), (n2, l2)) = updateList n1 l2 ls

solve :: Input -> Int
solve = sum . map huntSmudge
  where huntSmudge ls =
          let h   = hunt 100 ls -- find original winner (if horizontal has one)
              hs  = concatMap (hunt 100 . wipe ls) . potentialSmudge $ ls -- find and wipe each smudge and look for a winner
              --
              ls' = rotate90 ls -- repeat the same steps for columnar search
              h'  = hunt 1 ls'
              hs' = concatMap (hunt 1 . wipe ls') . potentialSmudge $ ls'
              --
              w1s = nub (h ++ h') -- unique winners from part 1
              w2s = nub (hs ++ hs') \\ w1s -- remove part 1 winners to find smudged winners
          in head $ if null w2s then w1s else w2s


main :: IO ()
main = interact $ show . solve . parse

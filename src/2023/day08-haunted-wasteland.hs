module Main where
import Data.List (lookup)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, some)
import Text.Megaparsec.Char (letterChar, string)

--	... It seems like you're meant to use the left/right instructions to navigate the network. Perhaps if you have the camel follow the same instructions, you can escape the haunted wasteland!
--
--	After examining the maps for a bit, two nodes stick out: AAA and ZZZ. You feel like AAA is where you are now, and you have to follow the left/right instructions until you reach ZZZ.
--
--	This format defines each node of the network individually. For example:
--
--	RL
--
--	AAA = (BBB, CCC)
--	BBB = (DDD, EEE)
--	CCC = (ZZZ, GGG)
--	DDD = (DDD, DDD)
--	EEE = (EEE, EEE)
--	GGG = (GGG, GGG)
--	ZZZ = (ZZZ, ZZZ)
--
--	Starting with AAA, you need to look up the next element based on the next left/right instruction in
--	your input. In this example, start with AAA and go right (R) by choosing the right element of AAA,
--	CCC. Then, L means to choose the left element of CCC, ZZZ. By following the left/right instructions,
--	you reach ZZZ in 2 steps.
--
--	Of course, you might not find ZZZ right away. If you run out of left/right instructions, repeat the
--	whole sequence of instructions as necessary: RL really means RLRLRLRLRLRLRLRL... and so on. For
--	example, here is a situation that takes 6 steps to reach ZZZ:
--
--	LLR
--
--	AAA = (BBB, BBB)
--	BBB = (AAA, ZZZ)
--	ZZZ = (ZZZ, ZZZ)
--
--	Starting at AAA, follow the left/right instructions. How many steps are required to reach ZZZ?
--
-- Test:
--	% cd ../.. && echo -e 'RL\n\nAAA = (BBB, CCC)\nBBB = (DDD, EEE)\nCCC = (ZZZ, GGG)\nDDD = (DDD, DDD)\nEEE = (EEE, EEE)\nGGG = (GGG, GGG)\nZZZ = (ZZZ, ZZZ)' | cabal run 2023_day08-hauntedwasteland # = 2
--	% cd ../.. && echo -e 'LLR\n\nAAA = (BBB, BBB)\nBBB = (AAA, ZZZ)\nZZZ = (ZZZ, ZZZ)' | cabal run 2023_day08-hauntedwasteland # = 6

type Document = (String, (String, String))
type Parser = Parsec Void String

document :: Parser Document
document = mkDoc <$> (word <* string " = (") <*> (word <* string ", ") <*> (word <* string ")") <* eof
  where word = some letterChar
        mkDoc loc l r = (loc, (l, r))

-- e.g. ("RL",Right [("AAA",("BBB","CCC")),("BBB",("DDD","EEE")),("CCC",("ZZZ","GGG")),("DDD",("DDD","DDD")),("EEE",("EEE","EEE")),("GGG",("GGG","GGG")),("ZZZ",("ZZZ","ZZZ"))])
parse (l:_:ls) = (l, mapM (runParser document "<stdin>") ls)

walk :: [Document] -> Int -> String -> String -> Int
walk _ stps "ZZZ" _ = stps
walk ds stps loc (rl:rls) = walk ds (stps + 1) (dest rl) rls
  where Just drl = lookup loc ds
        dest 'L' = fst drl
        dest 'R' = snd drl

solve :: (String, Either a [Document]) -> Int
solve (rls, Right ds) = walk ds 0 "AAA" . cycle $ rls


main :: IO ()
main = interact $ show . solve . parse . lines

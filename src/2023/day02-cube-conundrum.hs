module Main where
import Data.List.Extra (sumOn')
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, sepBy, some)
import Text.Megaparsec.Char (letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... For example, the record of a few games might look like this:
--
--	Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
--	Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
--	Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
--	Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
--	Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
--
--	In game 1, three sets of cubes are revealed from the bag (and then put back again). The first set is 3
--	blue cubes and 4 red cubes; the second set is 1 red cube, 2 green cubes, and 6 blue cubes; the third
--	set is only 2 green cubes.
--
--	The Elf would first like to know which games would have been possible if the bag contained only 12 red
--	cubes, 13 green cubes, and 14 blue cubes?
--
--	In the example above, games 1, 2, and 5 would have been possible if the bag had been loaded with that
--	configuration. However, game 3 would have been impossible because at one point the Elf showed you 20
--	red cubes at once; similarly, game 4 would also have been impossible because the Elf showed you 15
--	blue cubes at once. If you add up the IDs of the games that would have been possible, you get 8.
--
--	Determine which games would have been possible if the bag had been loaded with only 12 red cubes, 13
--	green cubes, and 14 blue cubes. What is the sum of the IDs of those games?
--
-- Test:
--	% cd ../.. && echo -e 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green' | cabal run 2023_day02-cube-conundrum # = 8

data Cubes = Cubes Int Int Int deriving (Show, Eq, Ord) -- Red, Green, Blue cubes
data Game  = Game Int [Cubes]  deriving (Show, Eq, Ord)

type Parser = Parsec Void String

games :: Parser [Game]
games = some (game <* newline) <* eof

game :: Parser Game
game = string "Game " *> (Game <$> decimal <* string ": " <*> (cubes `sepBy` string "; "))

cubes :: Parser Cubes
cubes = foldl' toCube (Cubes 0 0 0) <$> (cube `sepBy` string ", ")
  where toCube :: Cubes -> (Int, String) -> Cubes
        toCube (Cubes r g b) (n, "red")   = Cubes (r + n) g b
        toCube (Cubes r g b) (n, "green") = Cubes r (g + n) b
        toCube (Cubes r g b) (n, "blue")  = Cubes r g (b + n)

cube :: Parser (Int, String)
cube = (,) <$> (decimal <* space) <*> some letterChar


chkCubeR = 12
chkCubeG = 13
chkCubeB = 14

solve :: Either a [Game] -> Int
solve (Right gs) = sumOn' (\(Game id _) -> id) . filter chkGame $ gs
  where chkGame (Game id cs) = all chkCube cs
        chkCube (Cubes r g b) = r <= chkCubeR && g <= chkCubeG && b <= chkCubeB


main :: IO ()
main = interact $ show . solve . runParser games "<stdin>"

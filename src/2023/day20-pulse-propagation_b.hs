{-# Language BangPatterns #-}
module Main where
import Data.HashMap.Strict ((!?), HashMap, elems, empty, filterWithKey, fromList, insert, keys, toList)
import Data.List (filter, )
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (char, letterChar, newline, string)

--	... Flip-flop modules (prefix %) are either on or off; they are initially off. If a flip-flop module
--	receives a high pulse, it is ignored and nothing happens. However, if a flip-flop module receives a
--	low pulse, it flips between on and off. If it was off, it turns on and sends a high pulse. If it was
--	on, it turns off and sends a low pulse.
--
--	Conjunction modules (prefix &) remember the type of the most recent pulse received from each of their
--	connected input modules; they initially default to remembering a low pulse for each input. When a
--	pulse is received, the conjunction module first updates its memory for that input. Then, if it
--	remembers high pulses for all inputs, it sends a low pulse; otherwise, it sends a high pulse.
--
--	There is a single broadcast module (named broadcaster). When it receives a pulse, it sends the same
--	pulse to all of its destination modules.
--
--	Here at Desert Machine Headquarters, there is a module with a single button on it called, aptly, the
--	button module. When you push the button, a single low pulse is sent directly to the broadcaster
--	module.
--
--	After pushing the button, you must wait until all pulses have been delivered and fully handled before
--	pushing it again. Never push the button if modules are still processing pulses.
--
--	Pulses are always processed in the order they are sent. So, if a pulse is sent to modules a, b, and c,
--	and then module a processes its pulse and sends more pulses, the pulses sent to modules b and c would
--	have to be handled first.
--
--	The module configuration (your puzzle input) lists each module. The name of the module is preceded by
--	a symbol identifying its type, if any. The name is then followed by an arrow and a list of its
--	destination modules. For example:
--
--	broadcaster -> a, b, c
--	%a -> b
--	%b -> c
--	%c -> inv
--	&inv -> a
--
--	In this module configuration, the broadcaster has three destination modules named a, b, and c. Each of
--	these modules is a flip-flop module (as indicated by the % prefix). a outputs to b which outputs to c
--	which outputs to another module named inv. inv is a conjunction module (as indicated by the & prefix)
--	which, because it has only one input, acts like an inverter (it sends the opposite of the pulse type
--	it receives); it outputs to a.
--
--	By pushing the button once, the following pulses are sent:
--
--	button -low-> broadcaster
--	broadcaster -low-> a
--	broadcaster -low-> b
--	broadcaster -low-> c
--	a -high-> b
--	b -high-> c
--	c -high-> inv
--	inv -low-> a
--	a -low-> b
--	b -low-> c
--	c -low-> inv
--	inv -high-> a
--
--	After this sequence, the flip-flop modules all end up off, so pushing the button again repeats the same sequence.
--
--	Here's a more interesting example:
--
--	broadcaster -> a
--	%a -> inv, con
--	&inv -> b
--	%b -> con
--	&con -> output
--
--	This module configuration includes the broadcaster, two flip-flops (named a and b), a single-input
--	conjunction module (inv), a multi-input conjunction module (con), and an untyped module named output
--	(for testing purposes). The multi-input conjunction module con watches the two flip-flop modules and,
--	if they're both on, sends a low pulse to the output module.
--
--	Here's what happens if you push the button once:
--
--	button -low-> broadcaster
--	broadcaster -low-> a
--	a -high-> inv
--	a -high-> con
--	inv -low-> b
--	con -high-> output
--	b -high-> con
--	con -low-> output
--
--	Both flip-flops turn on and a low pulse is sent to output! However, now that both flip-flops are on
--	and con remembers a high pulse from each of its two inputs, pushing the button a second time does
--	something different:
--
--	button -low-> broadcaster
--	broadcaster -low-> a
--	a -low-> inv
--	a -low-> con
--	inv -high-> b
--	con -high-> output
--
--	Flip-flop a turns off! Now, con remembers a low pulse from module a, and so it sends only a high pulse
--	to output.
--
--	Push the button a third time:
--
--	button -low-> broadcaster
--	broadcaster -low-> a
--	a -high-> inv
--	a -high-> con
--	inv -low-> b
--	con -low-> output
--	b -low-> con
--	con -high-> output
--
--	This time, flip-flop a turns on, then flip-flop b turns off. However, before b can turn off, the pulse
--	sent to con is handled first, so it briefly remembers all high pulses for its inputs and sends a low
--	pulse to output. After that, flip-flop b turns off, which causes con to update its state and send a
--	high pulse to output.
--
--	Finally, with a on and b off, push the button a fourth time:
--
--	button -low-> broadcaster
--	broadcaster -low-> a
--	a -low-> inv
--	a -low-> con
--	inv -high-> b
--	con -high-> output
--
--	This completes the cycle: a turns off, causing con to remember only low pulses and restoring all modules to their original states.
--
--	To get the cables warmed up, the Elves have pushed the button 1000 times. How many pulses got sent as
--	a result (including the pulses sent by the button itself)?
--
--	In the first example, the same thing happens every time the button is pushed: 8 low pulses and 4 high
--	pulses are sent. So, after pushing the button 1000 times, 8000 low pulses and 4000 high pulses are
--	sent. Multiplying these together gives 32000000.
--
--	In the second example, after pushing the button 1000 times, 4250 low pulses and 2750 high pulses are
--	sent. Multiplying these together gives 11687500.
--
--	Consult your module configuration; determine the number of low pulses and high pulses that would be
--	sent after pushing the button 1000 times, waiting for all pulses to be fully handled after each push
--	of the button. What do you get if you multiply the total number of low pulses sent by the total number
--	of high pulses sent?
--
-- Test:
--	% cd ../.. && echo -e 'broadcaster -> a, b, c\n%a -> b\n%b -> c\n%c -> inv\n&inv -> a' | cabal run 2023_day20-pulse-propagation # 32000000
--	% cd ../.. && echo -e 'broadcaster -> a\n%a -> inv, con\n&inv -> b\n%b -> con\n&con -> output' | cabal run 2023_day20-pulse-propagation # 11687500

type Pulse = Bool -- True = High, False = Low
high = True
low  = False
flipFlop = not

type Inputs = HashMap String Pulse
data Gate = FlipFlop !Pulse [String] | Conjunction !Inputs [String] | Broadcaster [String] deriving (Eq, Ord, Show)

getDests :: Gate -> [String]
getDests (FlipFlop _    ds) = ds
getDests (Conjunction _ ds) = ds
getDests (Broadcaster   ds) = ds

type Parser = Parsec Void String

puzzle :: Parser [(String, Gate)]
puzzle = many (gate <* newline) <* eof

word :: Parser String
word = many letterChar

gate :: Parser (String, Gate)
gate = choice
  [ char '%' *> ((,) <$> word      <*> (FlipFlop    low   <$> dests))
  , char '&' *> ((,) <$> word      <*> (Conjunction empty <$> dests))
  , ((,) <$> string "broadcaster") <*> (Broadcaster       <$> dests)
  ]
  where dests :: Parser [String]
        dests = string " -> " *> (word `sepBy` string ", ")

type System = HashMap String Gate
type Signal = (String, (String, Pulse))

isHigh :: Signal -> Pulse
isHigh = snd . snd

signal :: String -> (Bool, System, Signal) -> (Bool, System, [Signal])
signal f (done, sys, (dst, (src, puls))) = signal' $ sys !? dst
  where signal' :: Maybe Gate -> (Bool, System, [Signal])
        signal' (Just (Broadcaster dests)) = (done, sys, map (,(dst,puls)) dests)
        --
        signal' (Just (FlipFlop st dests))
          | puls == low =
              let st'  = flipFlop st
                  ss'  = map (,(dst,st')) dests
                  sys' = insert dst (FlipFlop st' dests) sys
              in (done, sys', ss')
        --
        signal' (Just (Conjunction is dests)) =
          let is'   = insert src puls is
              st'   = not . and . elems $ is' -- if it remembers high pulses for all inputs, it sends a low pulse
              ss'   = map (,(dst,st')) dests
              done' = done || (f == dst && high == st')
          in (done', insert dst (Conjunction is' dests) sys, ss')
        --
        signal' _ = (done, sys, [])

step :: String -> (Bool, System, [Signal]) -> (Bool, System, [Signal])
step f (done, sys, ss) =
  let (done', sys', ss') = foldl' send (done, sys, []) ss
  in (done || done', sys', ss')
  where send :: (Bool, System, [Signal]) -> Signal -> (Bool, System, [Signal])
        send (done, sys, acc) s =
          let (done', sys', ss') = signal f (done, sys, s)
          in (done || done', sys', acc ++ ss')

{-
Find the # of button presses that triggers a High pulse on each gates sending to RX.
Find the LCM of these to find when RX will receive a low pulse.

I suspect this is some form of counter in assembly language and maybe a number, like 2023,
is what triggers the RX low signal.  Just a guess tho.
-}

button :: String -> (Bool, System, [Signal]) -> (Bool, System, [Signal])
button f st@(True, _, _)     = st
button f st@(_, _, ss@(_:_)) = button f . step f $ st
button f st@(_, _, [])       = st

solve :: Either a [(String, Gate)] -> Int
solve (Right ms) = foldl1 lcm facts
  where push :: String -> Int -> (Bool, System, [Signal]) -> Int
        push _ n st@(True, _, _) = n
        push f !n (done, sys, _) = push f (n + 1) . button f $ (done, sys, [("broadcaster", ("button", low))])
        --
        [(rxSender, Conjunction _ _)] = toList . filterWithKey (\src cg -> "rx" `elem` getDests cg) $ sys
        lcmgs = keys . filterWithKey (\src g -> rxSender `elem` getDests g) $ sys
        facts = map (\f -> push f 0 (False, sys, [])) lcmgs
        --
        sys = fromList . map updateConjunction $ ms
        updateConjunction :: (String, Gate) -> (String, Gate)
        updateConjunction (nm, Conjunction is dests) = (nm, Conjunction (fromList . map ((,low) . fst) . filter (\(nm', g) -> nm /= nm' && nm `elem` getDests g) $ ms) dests)
        updateConjunction g = g


main :: IO ()
main = interact $ show . solve . runParser puzzle "<stdin>"

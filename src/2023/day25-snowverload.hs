module Main where
import Data.Graph.Inductive (Gr, edges, insert, labNodes, match, mkUGraph, nmap, order, size)
import Data.Hashable (hash)
import Data.List (nub)
import System.Random (RandomGen, getStdRandom, randomR)

--	... you need to find a way to disconnect at least half of the equipment here, but it's already Christmas! You only have time to disconnect three wires.
--
--	Fortunately, someone left a wiring diagram (your puzzle input) that shows how the components are connected. For example:
--
--	jqt: rhn xhk nvd
--	rsh: frs pzl lsr
--	xhk: hfx
--	cmg: qnr nvd lhk bvb
--	rhn: xhk bvb hfx
--	bvb: xhk hfx
--	pzl: lsr hfx nvd
--	qnr: nvd
--	ntq: jqt hfx bvb xhk
--	nvd: lhk
--	lsr: lhk
--	rzs: qnr cmg lsr rsh
--	frs: qnr lhk lsr
--
--	Each line shows the name of a component, a colon, and then a list of other components to which that
--	component is connected. Connections aren't directional; abc: xyz and xyz: abc both represent the same
--	configuration. Each connection between two components is represented only once, so some components
--	might only ever appear on the left or right side of a colon.
--
--	In this example, if you disconnect the wire between hfx/pzl, the wire between bvb/cmg, and the wire
--	between nvd/jqt, you will divide the components into two separate, disconnected groups:
--
--	* 9 components: cmg, frs, lhk, lsr, nvd, pzl, qnr, rsh, and rzs.
--	* 6 components: bvb, hfx, jqt, ntq, rhn, and xhk.
--
--	Multiplying the sizes of these groups together produces 54.
--
--	Find the three wires you need to disconnect in order to divide the components into two separate
--	groups. What do you get if you multiply the sizes of these two groups together?
--
-- Test:
--	% cd ../.. && echo -e 'jqt: rhn xhk nvd\nrsh: frs pzl lsr\nxhk: hfx\ncmg: qnr nvd lhk bvb\nrhn: xhk bvb hfx\nbvb: xhk hfx\npzl: lsr hfx nvd\nqnr: nvd\nntq: jqt hfx bvb xhk\nnvd: lhk\nlsr: lhk\nrzs: qnr cmg lsr rsh\nfrs: qnr lhk lsr' | cabal run 2023_day25-snowverload # 54

{-
 * https://en.wikipedia.org/wiki/Karger's_algorithm ??
 * https://github.com/jrp2014/AoC2023/blob/main/Day25/Day25.hs
 * https://www.reddit.com/r/adventofcode/comments/18qbsxs/comment/ketzp94/?utm_source=share&utm_medium=web2x&context=3&rdt=46331
 * https://en.wikipedia.org/wiki/Minimum_cut
 * https://www.altcademy.com/blog/determine-the-minimum-cut-in-a-graph/

 1. Select a random edge from the graph.
 2. Contract the edge, merging the two nodes it connects into a single node, and removing any self-loops that may occur.
 3. Repeat steps 1 and 2 until only two nodes remain.
 4. The set of edges between the two remaining nodes is a cut. Record the size of this cut.
 5. Repeat steps 1-4 multiple times to increase the probability of finding the minimum cut.
 6. Return the smallest cut found in all iterations.

import random
from collections import defaultdict

def karger_min_cut(graph):
    while len(graph) > 2:
        node1, node2 = random.choice(list(graph.items()))
        graph[node1].extend(graph[node2])
        for node in graph[node2]:
            graph[node].remove(node2)
            graph[node].append(node1)
        del graph[node2]
    return len(list(graph.values())[0])

def find_min_cut(graph, iterations):
    min_cut = float('inf')
    for _ in range(iterations):
        new_graph = defaultdict(list, {k: v[:] for k, v in graph.items()})
        cut = karger_min_cut(new_graph)
        min_cut = min(min_cut, cut)
    return min_cut

-}

type Component = (Int, [Int]) -- vertex and edges
type CGraph = Gr Int ()

parse :: String -> IO [Component]
parse = return . map ((\(w:ws) -> (hash . init $ w, map hash ws)) . words) . lines

toGraph :: [Component] -> CGraph
toGraph cs = nmap (const 1) . mkUGraph (nub [n | (v, es) <- cs, n <- v:es]) $ [(v, v') | (v, es) <- cs, v' <- es]

merge :: Int ->  CGraph -> CGraph
merge e0 g = g3
  where e@(l, r) = edges g !! e0
        (Just (lns0, _, !szl, lns1), g1) = match l g
        (Just (rns0, _, !szr, rns1), g2) = match r g1
        adj = [a | a <- lns0 ++ lns1, snd a /= r] ++ rns0 ++ rns1
        g3 = insert ([], l, szl + szr, adj) g2

kargerMinCut :: forall gen. (RandomGen gen) => CGraph -> gen -> (CGraph, gen)
kargerMinCut g gen
  | order g > 2 = let (e0, gen') = randomR (0, size g - 1) gen
                  in kargerMinCut (merge e0 g) gen'
  | otherwise   = (g, gen)

solve :: forall gen. (RandomGen gen) => [Component] -> gen -> (Int, gen)
solve cs gen = let (ans, gen') = score . find3 $ (g, gen)
               in (ans, gen')
  where g = toGraph cs
        --
        find3 :: (CGraph, gen) -> (CGraph, gen)
        find3 (h, gen)
          | 3 == size h = (h, gen)
          | otherwise   = find3 . kargerMinCut g $ gen
        --
        score :: (CGraph, gen) -> (Int, gen)
        score (g, gen) = let [(_, sz0), (_, sz1)] = labNodes g
                         in (sz0 * sz1, gen)


main :: IO ()
main = getContents >>=
  parse >>=
  getStdRandom . solve >>=
  print

module Main where

--	... For each history, repeat the process of finding differences until the sequence of differences is
--	entirely zero. Then, rather than adding a zero to the end and filling in the next values of each
--	previous sequence, you should instead add a zero to the beginning of your sequence of zeroes, then
--	fill in new first values for each previous sequence.
--
--	In particular, here is what the third example history looks like when extrapolating back in time:
--
--	*5  10  13  16  21  30  45
--	  *5   3   3   5   9  15
--	   *-2   0   2   4   6
--	      *2   2   2   2
--	        *0   0   0
--
--	Adding the new values on the left side of each sequence from bottom to top eventually reveals the new left-most history value: 5.
--
--	Doing this for the remaining example data above results in previous values of -3 for the first history
--	and 0 for the second history. Adding all three new values together produces 2.
--
--	Analyze your OASIS report again, this time extrapolating the previous value for each history. What is the sum of these extrapolated values?
--
-- Test:
--	% cd ../.. && echo -e '10 13 16 21 30 45' | cabal run 2023_day09-mirage-maintenance_b # = 2 (-3, 0, 5)
--	% cd ../.. && echo -e '0 3 6 9 12 15\n1 3 6 10 15 21\n10 13 16 21 30 45' | cabal run 2023_day09-mirage-maintenance_b # = 2 (-3, 0, 5)

solve :: [[Int]] -> Int
solve = sum . map (score . (\is -> is:diffs is))
  where diffs :: [Int] -> [[Int]]
        diffs is
          | all (0 ==) is = []
          | otherwise     = let d = diff is in d:diffs d
        --
        diff :: [Int] -> [Int]
        diff [i] = []
        diff (i0:is@(i1:_)) = (i1 - i0):diff is
        --
        score :: [[Int]] -> Int
        score = foldl' (flip ((-) . head)) 0 . reverse


main :: IO ()
main = interact $ show . solve . map (map (read :: String -> Int) . words) . lines

#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint *.hs
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-trebuchet.hs   < 01a.txt ; (( rc += ${?} ))
#time ./day01-trebuchet_b.hs < 01a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-cube-conundrum.hs   < 02a.txt ; (( rc += ${?} ))
#time ./day02-cube-conundrum_b.hs < 02a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-gear-ratios.hs   < 03a.txt ; (( rc += ${?} ))
#time ./day03-gear-ratios_b.hs < 03a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-scratchcards.hs   < 04a.txt ; (( rc += ${?} ))
#time ./day04-scratchcards_b.hs < 04a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-if-you-give-a-seed-a-fertilizer.hs   < 05a.txt ; (( rc += ${?} ))
#time ./day05-if-you-give-a-seed-a-fertilizer_b.hs < 05a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-wait-for-it.hs   < 06a.txt ; (( rc += ${?} ))
#time ./day06-wait-for-it_b.hs < 06a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-camel-cards.hs   < 07a.txt ; (( rc += ${?} ))
#time ./day07-camel-cards_b.hs < 07a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-haunted-wasteland.hs   < 08a.txt ; (( rc += ${?} ))
#time ./day08-haunted-wasteland_b.hs < 08a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-mirage-maintenance.hs   < 09a.txt ; (( rc += ${?} ))
#time ./day09-mirage-maintenance_b.hs < 09a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-pipe-maze.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-pipe-maze_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-cosmic-expansion.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-cosmic-expansion_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-hot-springs.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-hot-springs_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-point-of-incidence.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-point-of-incidence_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-parabolic-reflector-dish.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-parabolic-reflector-dish_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-lens-library.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-lens-library_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-the-floor-will-be-lava.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-the-floor-will-be-lava_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-clumsy-crucible.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-clumsy-crucible_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time ./day18-lavaduct-lagoon.hs   < 18a.txt ; (( rc += ${?} ))
#time ./day18-lavaduct-lagoon_b.hs < 18a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-aplenty.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-aplenty_b.hs < 19a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day20"
time ./day20-pulse-propagation.hs   < 20a.txt ; (( rc += ${?} ))
time ./day20-pulse-propagation_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-step-counter.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-step-counter_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-sand-slabs.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-sand-slabs_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time ./day23-a-long-walk.hs   < 23a.txt ; (( rc += ${?} ))
#time ./day23-a-long-walk_b.hs < 23a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-never-tell-me-the-odds.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-never-tell-me-the-odds_b.hs < 24a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day25"
#time ./day25-snowverload.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

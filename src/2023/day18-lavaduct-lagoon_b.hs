module Main where
import Data.Char (isHexDigit)
import Data.List.Extra (sumOn')

--	... the planned lagoon would be much too small.
--
--	After a few minutes, someone realizes what happened; someone swapped the color and instruction
--	parameters when producing the dig plan. They don't have time to fix the bug; one of them asks if you
--	can extract the correct instructions from the hexadecimal codes.
--
--	Each hexadecimal code is six hexadecimal digits long. The first five hexadecimal digits encode the
--	distance in meters as a five-digit hexadecimal number. The last hexadecimal digit encodes the
--	direction to dig: 0 means R, 1 means D, 2 means L, and 3 means U.
--
--	So, in the above example, the hexadecimal codes can be converted into the true instructions:
--
--	#70c710 = R 461937
--	#0dc571 = D 56407
--	#5713f0 = R 356671
--	#d2c081 = D 863240
--	#59c680 = R 367720
--	#411b91 = D 266681
--	#8ceee2 = L 577262
--	#caa173 = U 829975
--	#1b58a2 = L 112010
--	#caa171 = D 829975
--	#7807d2 = L 491645
--	#a77fa3 = U 686074
--	#015232 = L 5411
--	#7a21e3 = U 500254
--
--	Digging out this loop and its interior produces a lagoon that can hold an impressive 952408144115
--	cubic meters of lava.
--
--	Convert the hexadecimal color codes into the correct instructions; if the Elves follow this new dig
--	plan, how many cubic meters of lava could the lagoon hold?
--
-- Test:
--	% cd ../.. && echo -e 'R 6 (#70c710)\nD 5 (#0dc571)\nL 2 (#5713f0)\nD 2 (#d2c081)\nR 2 (#59c680)\nD 2 (#411b91)\nL 5 (#8ceee2)\nU 2 (#caa173)\nL 1 (#1b58a2)\nU 2 (#caa171)\nR 2 (#7807d2)\nU 3 (#a77fa3)\nL 2 (#015232)\nU 2 (#7a21e3)' | cabal run 2023_day18-lavaduct-lagoon_b # = 952408144115

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type Input = (Char, Int)

parse :: String -> [Input]
parse = map ((\[_, _, rgbs] -> let rgb = filter isHexDigit rgbs in (dir $ last rgb, read $ "0x" ++ init rgb)::Input) . words) . lines
  where dir ch | '0' == ch = 'R'
               | '1' == ch = 'D'
               | '2' == ch = 'L'
               | '3' == ch = 'U'

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

-- https://en.wikipedia.org/wiki/Shoelace_formula#Trapezoid_formula
shoelaceArea :: [Point] -> Int
shoelaceArea ps@((y0, x0):_) = (`div` 2) . abs . shoelaceArea' $ ps
  where shoelaceArea' [(yn, xn)]                 = (yn + y0) * (xn - x0)
        shoelaceArea' ((y1, x1):ps@((y2, x2):_)) = (y1 + y2) * (x1 - x2) + shoelaceArea' ps

move :: Point -> [Input] -> [Point]
move (y, x) ((d, n):is) = pt:move pt is
  where (dy, dx) | 'U' == d = north
                 | 'R' == d = east
                 | 'D' == d = south
                 | 'L' == d = west
        pt = (y + dy * n, x + dx * n)
move pos []     = []

solve :: [Input] -> Int
solve is = (+) lineArea . shoelaceArea . move (0, 0) $ is
  where lineArea = let len = sumOn' snd is
                   in len `div` 2 + 1


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Algorithm.Search (dijkstra, pruning)
import Data.Array ((!), Array, array, assocs, bounds, elems, inRange, listArray)
import Data.Bifunctor (first, bimap)
import Data.Function (on)
import Data.List (groupBy, sort)
import Data.Maybe (fromMaybe)

--	... you should calculate how many tiles are contained within the loop. For example:
--
--	...........
--	.S-------7.
--	.|F-----7|.
--	.||.....||.
--	.||.....||.
--	.|L-7.F-J|.
--	.|..|.|..|.
--	.L--J.L--J.
--	...........
--
--	The above loop encloses merely four tiles - the two pairs of . in the southwest and southeast (marked
--	I below). The middle . tiles (marked O below) are not in the loop. Here is the same loop again with
--	those regions marked:
--
--	...........
--	.S-------7.
--	.|F-----7|.
--	.||OOOOO||.
--	.||OOOOO||.
--	.|L-7OF-J|.
--	.|II|O|II|.
--	.L--JOL--J.
--	.....O.....
--
--	In fact, there doesn't even need to be a full tile path to the outside for tiles to count as outside
--	the loop - squeezing between pipes is also allowed! Here, I is still within the loop and O is still
--	outside the loop:
--
--	..........
--	.S------7.
--	.|F----7|.
--	.||OOOO||.
--	.||OOOO||.
--	.|L-7F-J|.
--	.|II||II|.
--	.L--JL--J.
--	..........
--
--	In both of the above examples, 4 tiles are enclosed by the loop.
--
--	Here's a larger example:
--
--	.F----7F7F7F7F-7....
--	.|F--7||||||||FJ....
--	.||.FJ||||||||L7....
--	FJL7L7LJLJ||LJ.L-7..
--	L--J.L7...LJS7F-7L7.
--	....F-J..F7FJ|L7L7L7
--	....L7.F7||L7|.L7L7|
--	.....|FJLJ|FJ|F7|.LJ
--	....FJL-7.||.||||...
--	....L---J.LJ.LJLJ...
--
--	The above sketch has many random bits of ground, some of which are in the loop (I) and some of which are outside it (O):
--
--	OF----7F7F7F7F-7OOOO
--	O|F--7||||||||FJOOOO
--	O||OFJ||||||||L7OOOO
--	FJL7L7LJLJ||LJIL-7OO
--	L--JOL7IIILJS7F-7L7O
--	OOOOF-JIIF7FJ|L7L7L7
--	OOOOL7IF7||L7|IL7L7|
--	OOOOO|FJLJ|FJ|F7|OLJ
--	OOOOFJL-7O||O||||OOO
--	OOOOL---JOLJOLJLJOOO
--
--	In this larger example, 8 tiles are enclosed by the loop.
--
--	Any tile that isn't part of the main loop can count as being enclosed by the loop. Here's another
--	example with many bits of junk pipe lying around that aren't connected to the main loop at all:
--
--	FF7FSF7F7F7F7F7F---7
--	L|LJ||||||||||||F--J
--	FL-7LJLJ||||||LJL-77
--	F--JF--7||LJLJ7F7FJ-
--	L---JF-JLJ.||-FJLJJ7
--	|F|F-JF---7F7-L7L|7|
--	|FFJF7L7F-JF7|JL---7
--	7-L-JL7||F7|L7F-7F7|
--	L.L7LFJ|||||FJL7||LJ
--	L7JLJL-JLJLJL--JLJ.L
--
--	Here are just the tiles that are enclosed by the loop marked with I:
--
--	FF7FSF7F7F7F7F7F---7
--	L|LJ||||||||||||F--J
--	FL-7LJLJ||||||LJL-77
--	F--JF--7||LJLJIF7FJ-
--	L---JF-JLJIIIIFJLJJ7
--	|F|F-JF---7IIIL7L|7|
--	|FFJF7L7F-JF7IIL---7
--	7-L-JL7||F7|L7F-7F7|
--	L.L7LFJ|||||FJL7||LJ
--	L7JLJL-JLJLJL--JLJ.L
--
--	In this last example, 10 tiles are enclosed by the loop.
--
--	Figure out whether you have time to search for the nest by calculating the area within the loop. How
--	many tiles are enclosed by the loop?
--
-- Test:
--	% cd ../.. && echo -e '...........\n.S-------7.\n.|F-----7|.\n.||.....||.\n.||.....||.\n.|L-7.F-J|.\n.|..|.|..|.\n.L--J.L--J.\n...........' | cabal run 2023_day10-pipe-maze_b # = 4
--	% cd ../.. && echo -e '..........\n.S------7.\n.|F----7|.\n.||....||.\n.||....||.\n.|L-7F-J|.\n.|..||..|.\n.L--JL--J.\n..........' | cabal run 2023_day10-pipe-maze_b # = 4
--	% cd ../.. && echo -e '.F----7F7F7F7F-7....\n.|F--7||||||||FJ....\n.||.FJ||||||||L7....\nFJL7L7LJLJ||LJ.L-7..\nL--J.L7...LJS7F-7L7.\n....F-J..F7FJ|L7L7L7\n....L7.F7||L7|.L7L7|\n.....|FJLJ|FJ|F7|.LJ\n....FJL-7.||.||||...\n....L---J.LJ.LJLJ...' | cabal run 2023_day10-pipe-maze_b # = 8
--	% cd ../.. && echo -e 'FF7FSF7F7F7F7F7F---7\nL|LJ||||||||||||F--J\nFL-7LJLJ||||||LJL-77\nF--JF--7||LJLJ7F7FJ-\nL---JF-JLJ.||-FJLJJ7\n|F|F-JF---7F7-L7L|7|\n|FFJF7L7F-JF7|JL---7\n7-L-JL7||F7|L7F-7F7|\nL.L7LFJ|||||FJL7||LJ\nL7JLJL-JLJLJL--JLJ.L' | cabal run 2023_day10-pipe-maze_b # = 10

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

mkArray :: [String] -> Array Point Char
mkArray cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

cardinalDirections = [north, south, east, west]

exitDirs = [
  ('|', [north, south]),
  ('-', [east,  west]),
  ('L', [north, east]),
  ('J', [north, west]),
  ('7', [south, west]),
  ('F', [south, east])
  ]

exits ch = fromMaybe [] . lookup ch $ exitDirs

safeGet :: PuzzleMap -> Point -> Char
safeGet a pt@(y, x) | not . inRange (bounds a) $ pt = '.'
                    | otherwise = a ! (y, x)

{-
NOTE: moved from 'countHoles' that tracked the In/Out status horizontally per row to the shoelace formula
'countHoles' also found the true map character for 'S'; but this is shorter and simpler.
-}

-- https://en.wikipedia.org/wiki/Shoelace_formula#Trapezoid_formula
shoelaceArea :: [Point] -> Int
shoelaceArea ps@((y0, x0):_) = (`div` 2) . abs . shoelaceArea' $ ps
  where shoelaceArea' [(yn, xn)]                 = (yn + y0) * (xn - x0)
        shoelaceArea' ((y1, x1):ps@((y2, x2):_)) = (y1 + y2) * (x1 - x2) + shoelaceArea' ps

solve :: PuzzleMap -> Int
solve pm = shoelaceArea spath - (length spath `div` 2 - 1) -- subtract the area used by the path
  where start@(sy, sx) = fst . head . filter (('S' ==) . snd) . assocs $ pm
        [beg_p1, end_p2] = filter (linked start) . map (bimap (+ sy) (+ sx)) $ cardinalDirections
        --
        -- dijkstra finds the 1 (shortest) path thru the maze; pruning shouldn't be necessary or we'd have multiple path solutions, but to be safe ...
        Just (cost, path') = dijkstra (\st -> (getNeighbors `pruning` (not . linked st)) st) (const . const $ 1) (end_p2 ==) beg_p1
        spath = start:beg_p1:path' -- solution path w/ the beginning point and true starting point added (i.e. the full loop)
        --
        linked :: Point -> Point -> Bool
        linked cur pt = cur `elem` getNeighbors pt
        --
        getNeighbors :: Point -> [Point]
        getNeighbors (y, x) = map (bimap (+ y) (+ x)) . exits . safeGet pm $ (y, x)


main :: IO ()
main = interact $ show . solve . mkArray . lines

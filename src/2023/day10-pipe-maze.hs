module Main where
import Algorithm.Search (dijkstra, pruning)
import Data.Array ((!), Array, assocs, bounds, inRange, listArray)
import Data.Bifunctor (bimap)
import Data.Maybe (fromMaybe)

--	... The pipes are arranged in a two-dimensional grid of tiles:
--
--	| is a vertical pipe connecting north and south.
--	- is a horizontal pipe connecting east and west.
--	L is a 90-degree bend connecting north and east.
--	J is a 90-degree bend connecting north and west.
--	7 is a 90-degree bend connecting south and west.
--	F is a 90-degree bend connecting south and east.
--	. is ground; there is no pipe in this tile.
--	S is the starting position of the animal; there is a pipe on this tile, but your sketch doesn't show what shape the pipe has.
--
--	Based on the acoustics of the animal's scurrying, you're confident the pipe that contains the animal is one large, continuous loop.
--
--	For example, here is a square loop of pipe:
--
--	.....
--	.F-7.
--	.|.|.
--	.L-J.
--	.....
--	If the animal had entered this loop in the northwest corner, the sketch would instead look like this:
--
--	.....
--	.S-7.
--	.|.|.
--	.L-J.
--	.....
--	In the above diagram, the S tile is still a 90-degree F bend: you can tell because of how the adjacent pipes connect to it.
--
--	Unfortunately, there are also many pipes that aren't connected to the loop! This sketch shows the same loop as above:
--
--	-L|F7
--	7S-7|
--	L|7||
--	-L-J|
--	L|-JF
--
--	In the above diagram, you can still figure out which pipes form the main loop: they're the ones
--	connected to S, pipes those pipes connect to, pipes those pipes connect to, and so on. Every pipe in
--	the main loop connects to its two neighbors (including S, which will have exactly two pipes connecting
--	to it, and which is assumed to connect back to those two pipes).
--
--	Here is a sketch that contains a slightly more complex main loop:
--
--	..F7.
--	.FJ|.
--	SJ.L7
--	|F--J
--	LJ...
--	Here's the same example sketch with the extra, non-main-loop pipe tiles also shown:
--
--	7-F7-
--	.FJ|7
--	SJLL7
--	|F--J
--	LJ.LJ
--
--	If you want to get out ahead of the animal, you should find the tile in the loop that is farthest from
--	the starting position. Because the animal is in the pipe, it doesn't make sense to measure this by
--	direct distance. Instead, you need to find the tile that would take the longest number of steps along
--	the loop to reach from the starting point - regardless of which way around the loop the animal went.
--
--	In the first example with the square loop:
--
--	.....
--	.S-7.
--	.|.|.
--	.L-J.
--	.....
--	You can count the distance each tile in the loop is from the starting point like this:
--
--	.....
--	.012.
--	.1.3.
--	.234.
--	.....
--	In this example, the farthest point from the start is 4 steps away.
--
--	Here's the more complex loop again:
--
--	..F7.
--	.FJ|.
--	SJ.L7
--	|F--J
--	LJ...
--	Here are the distances for each tile on that loop:
--
--	..45.
--	.236.
--	01.78
--	14567
--	23...
--
--	Find the single giant loop starting at S. How many steps along the loop does it take to get from the
--	starting position to the point farthest from the starting position?
--
-- Test:
--	% cd ../.. && echo -e '.....\n.S-7.\n.|.|.\n.L-J.\n.....' | cabal run 2023_day10-pipe-maze # = 4
--	% cd ../.. && echo -e '-L|F7\n7S-7|\nL|7||\n-L-J|\nL|-JF' | cabal run 2023_day10-pipe-maze # = 4
--	% cd ../.. && echo -e '..F7.\n.FJ|.\nSJ.L7\n|F--J\nLJ...' | cabal run 2023_day10-pipe-maze # = 8

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

mkArray :: [String] -> Array Point Char
mkArray cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

cardinalDirections = [north, south, east, west]

exitDirs = [
  ('|', [north, south]),
  ('-', [east,  west]),
  ('L', [north, east]),
  ('J', [north, west]),
  ('7', [south, west]),
  ('F', [south, east])
  ]

exits ch = fromMaybe [] . lookup ch $ exitDirs

safeGet :: PuzzleMap -> Point -> Char
safeGet a pt@(y, x) | not . inRange (bounds a) $ pt = '.'
                    | otherwise = a ! (y, x)

solve :: PuzzleMap -> Int
solve pm = (cost + 2) `div` 2
  where start@(sy, sx) = fst . head . filter (('S' ==) . snd) . assocs $ pm
        [beg_p1, end_p2] = filter (linked start) . map (bimap (+ sy) (+ sx)) $ cardinalDirections
        Just (cost, ans) = dijkstra (\st -> (getNeighbors `pruning` (not . linked st)) st) (const . const $ 1) (end_p2 ==) beg_p1
        --
        linked :: Point -> Point -> Bool
        linked cur pt = cur `elem` getNeighbors pt
        --
        getNeighbors :: Point -> [Point]
        getNeighbors (y, x) = map (bimap (+ y) (+ x)) . exits . safeGet pm $ (y, x)



main :: IO ()
main = interact $ show . solve . mkArray . lines

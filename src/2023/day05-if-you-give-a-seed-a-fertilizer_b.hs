module Main where
import Data.Either (fromRight)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, runParser, sepBy, some)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	seeds: 79 14 55 13
--
--	seed-to-soil map:
--	50 98 2
--	52 50 48
--
--	soil-to-fertilizer map:
--	0 15 37
--	37 52 2
--	39 0 15
--
--	fertilizer-to-water map:
--	49 53 8
--	0 11 42
--	42 0 7
--	57 7 4
--
--	water-to-light map:
--	88 18 7
--	18 25 70
--
--	light-to-temperature map:
--	45 77 23
--	81 45 19
--	68 64 13
--
--	temperature-to-humidity map:
--	0 69 1
--	1 0 69
--
--	humidity-to-location map:
--	60 56 37
--	56 93 4
--
--	... The values on the initial seeds: line come in pairs. Within each pair, the first value is the
--	start of the range and the second value is the length of the range.
--
--	... In the above example, the lowest location number can be obtained from seed number 82, which
--	corresponds to soil 84, fertilizer 84, water 84, light 77, temperature 45, humidity 46, and location
--	46. So, the lowest location number is 46.
--
--	... Consider all of the initial seed numbers listed in the ranges on the first line of the
--	almanac. What is the lowest location number that corresponds to any of the initial seed numbers?
--
-- Test:
--	% cd ../.. && echo -e 'seeds: 79 14 55 13\n\nseed-to-soil map:\n50 98 2\n52 50 48\n\nsoil-to-fertilizer map:\n0 15 37\n37 52 2\n39 0 15\n\nfertilizer-to-water map:\n49 53 8\n0 11 42\n42 0 7\n57 7 4\n\nwater-to-light map:\n88 18 7\n18 25 70\n\nlight-to-temperature map:\n45 77 23\n81 45 19\n68 64 13\n\ntemperature-to-humidity map:\n0 69 1\n1 0 69\n\nhumidity-to-location map:\n60 56 37\n56 93 4' | cabal run 2023_day05-if-you-give-a-seed-a-fertilizer_b # = 46

type Range   = (Int, Int)
type Seeds   = [Range]
type SeedMap = (String, [(Range, Int)])

type Parser = Parsec Void String

seeds :: Parser Seeds
seeds = mkSeeds <$> (string "seeds: " *> (decimal `sepBy` char ' ')) <* eof
  where mkSeeds :: [Int] -> Seeds
        mkSeeds [] = []
        mkSeeds (s:sz:ss) = (s, s + sz - 1):mkSeeds ss

seedMaps :: Parser [SeedMap]
seedMaps = some seedMap <* eof

seedMap :: Parser SeedMap
seedMap = toSeedMap <$> (some (letterChar <|> char '-') <* string " map:" <* newline) <*> some mapRange
  where toSeedMap :: String -> [[Int]] -> SeedMap
        toSeedMap nm mnbrs = (nm, map (\[d, s, sz] -> ((s, s + sz - 1), d)) . filter (not . null) $ mnbrs)

mapRange :: Parser [Int]
mapRange = decimal `sepBy` char ' ' <* newline

parse :: [String] -> (Seeds, [SeedMap])
parse (ss:_:ms) = (sds, mps)
  where (Right sds) = runParser seeds "<stdin>" ss
        (Right mps) = runParser seedMaps "<stdin>" . unlines $ ms

-- e.g. "[(\"seed-to-soil\",[((98,99),50),((50,57),52)]),...,(\"humidity-to-location\",[((56,93),60),((93,96),56)])]"
plantSeed :: [SeedMap] -> Range -> Int
plantSeed [] sd@(sb, se) = sb
plantSeed ((nm, rs):mps) sd = findMaps sd rs
  where findMaps :: Range -> [(Range, Int)] -> Int
        findMaps sd [] = plantSeed mps sd -- no more maps, move seeds to next map as is
        findMaps sd@(sb, se) ((mr@(mb, me), d):rs) =
          let -- potential ranges, left of map, middle of map, and right of map
              left     = (sb, min se (mb - 1))
              right    = (max (me + 1) sb, se)
              midb     = max sb mb
              mide     = min se me
              o        = midb - mb
              mid      = (d + o, d + o + (mide - midb))
              --
              -- The left and right ranges are "uncovered" by this map, keep searching.
              -- The middle range is "covered", shift the seed range and search subsequent maps.
              minLeft  = if uncurry (>) left  then maxBound :: Int else findMaps left  rs  -- keep looking thru map ranges
              minRight = if uncurry (>) right then maxBound :: Int else findMaps right rs  -- keep looking thru map ranges
              minMid   = if uncurry (>) mid   then maxBound :: Int else plantSeed mps mid  -- mapped, move to next map
              --
          in min minRight $ min minLeft minMid

solve :: (Seeds, [SeedMap]) -> Int
solve (sds, mps) = minimum . map (plantSeed mps) $ sds


main :: IO ()
main = interact $ show . solve . parse . lines

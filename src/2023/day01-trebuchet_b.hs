module Main where
import Data.Char (isDigit)
import Data.List.Extra (replace)

--	Your calculation isn't quite right. It looks like some of the digits are actually spelled out with letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".
--
--	Equipped with this new information, you now need to find the real first and last digit on each line. For example:
--
--	two1nine
--	eightwothree
--	abcone2threexyz
--	xtwone3four
--	4nineeightseven2
--	zoneight234
--	7pqrstsixteen
--	In this example, the calibration values are 29, 83, 13, 24, 42, 14, and 76. Adding these together produces 281.
--
--	What is the sum of all of the calibration values?
--
-- Test:
--	% cd ../.. && echo -e 'two1nine\neightwothree\nabcone2threexyz\nxtwone3four\n4nineeightseven2\nzoneight234\n7pqrstsixteen' | cabal run 2023_day01-trebuchet_b # = 281


nbrs :: [(String, String)]
nbrs = [
  ("one",   "o1e"),
  ("two",   "t2o"),
  ("three", "t3e"),
  ("four",  "f4r"),
  ("five",  "f5e"),
  ("six",   "s6x"),
  ("seven", "s7n"),
  ("eight", "e8t"),
  ("nine",  "n9e")
  ]

firstLast :: String -> Int
firstLast str = read $ head str:[last str]

main :: IO ()
main = interact $ show . sum . map (firstLast . filter isDigit) . lines . flip (foldr (uncurry replace)) nbrs

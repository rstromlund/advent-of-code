module Main where
import Data.Either (fromRight)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, runParser, sepBy, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... For example:
--
--	seeds: 79 14 55 13
--
--	seed-to-soil map:
--	50 98 2
--	52 50 48
--
--	soil-to-fertilizer map:
--	0 15 37
--	37 52 2
--	39 0 15
--
--	fertilizer-to-water map:
--	49 53 8
--	0 11 42
--	42 0 7
--	57 7 4
--
--	water-to-light map:
--	88 18 7
--	18 25 70
--
--	light-to-temperature map:
--	45 77 23
--	81 45 19
--	68 64 13
--
--	temperature-to-humidity map:
--	0 69 1
--	1 0 69
--
--	humidity-to-location map:
--	60 56 37
--	56 93 4
--
--	The almanac starts by listing which seeds need to be planted: seeds 79, 14, 55, and 13.
--
--	... The first line has a destination range start of 50, a source range start of 98, and a range length of
--	2. This line means that the source range starts at 98 and contains two values: 98 and 99. The
--	destination range is the same length, but it starts at 50, so its two values are 50 and 51. With this
--	information, you know that seed number 98 corresponds to soil number 50 and that seed number 99
--	corresponds to soil number 51.
--
--	The second line means that the source range starts at 50 and contains 48 values: 50, 51, ..., 96,
--	97. This corresponds to a destination range starting at 52 and also containing 48 values: 52, 53, ...,
--	98, 99. So, seed number 53 corresponds to soil number 55.
--
--	Any source numbers that aren't mapped correspond to the same destination number. So, seed number 10
--	corresponds to soil number 10.
--
--	With this map, you can look up the soil number required for each initial seed number:
--
--	Seed number 79 corresponds to soil number 81.
--	Seed number 14 corresponds to soil number 14.
--	Seed number 55 corresponds to soil number 57.
--	Seed number 13 corresponds to soil number 13.
--
--	The gardener and his team want to get started as soon as possible, so they'd like to know the closest
--	location that needs a seed. Using these maps, find the lowest location number that corresponds to any
--	of the initial seeds. To do this, you'll need to convert each seed number through other categories
--	until you can find its corresponding location number. In this example, the corresponding types are:
--
--	Seed 79, soil 81, fertilizer 81, water 81, light 74, temperature 78, humidity 78, location 82.
--	Seed 14, soil 14, fertilizer 53, water 49, light 42, temperature 42, humidity 43, location 43.
--	Seed 55, soil 57, fertilizer 57, water 53, light 46, temperature 82, humidity 82, location 86.
--	Seed 13, soil 13, fertilizer 52, water 41, light 34, temperature 34, humidity 35, location 35.
--	So, the lowest location number in this example is 35.
--
--	What is the lowest location number that corresponds to any of the initial seed numbers?
--
-- Test:
--	% cd ../.. && echo -e 'seeds: 79 14 55 13\n\nseed-to-soil map:\n50 98 2\n52 50 48\n\nsoil-to-fertilizer map:\n0 15 37\n37 52 2\n39 0 15\n\nfertilizer-to-water map:\n49 53 8\n0 11 42\n42 0 7\n57 7 4\n\nwater-to-light map:\n88 18 7\n18 25 70\n\nlight-to-temperature map:\n45 77 23\n81 45 19\n68 64 13\n\ntemperature-to-humidity map:\n0 69 1\n1 0 69\n\nhumidity-to-location map:\n60 56 37\n56 93 4' | cabal run 2023_day05-if-you-give-a-seed-a-fertilizer # = 35

type Seeds   = [Int]
type SeedMap = (String, [[Int]])

type Parser = Parsec Void String

seeds :: Parser Seeds
seeds = string "seeds: " *> (decimal `sepBy` char ' ') <* eof

seedMaps :: Parser [SeedMap]
seedMaps = some seedMap <* eof

seedMap :: Parser SeedMap
seedMap = toSeedMap <$> (some (letterChar <|> char '-') <* string " map:" <* newline) <*> some mapRange
  where toSeedMap :: String -> [[Int]] -> SeedMap
        toSeedMap nm rng = (nm, filter (not . null) rng)

mapRange :: Parser [Int]
mapRange = decimal `sepBy` char ' ' <* newline

parse :: [String] -> (Seeds, [SeedMap])
parse (ss:_:ms) = (sds, mps)
  where (Right sds) = runParser seeds "<stdin>" ss
        (Right mps) = runParser seedMaps "<stdin>" . unlines $ ms

-- e.g. "[(\"seed-to-soil\",[[50,98,2],[52,50,48]]),...,(\"humidity-to-location\",[[60,56,37],[56,93,4]])]"
plantSeed :: [SeedMap] -> Int -> Int
plantSeed mps sd = foldl' findRange sd mps
  where findRange :: Int -> SeedMap -> Int
        findRange sd (nm, ms) = xlate . filter (\[d, s, l] -> sd >= s && sd < (s + l)) $ ms
          where xlate [] = sd
                xlate [[d, s, l]] = d + (sd - s)

solve :: (Seeds, [SeedMap]) -> Int
solve (sds, mps) = minimum . map (plantSeed mps) $ sds


main :: IO ()
main = interact $ show . solve . parse . lines

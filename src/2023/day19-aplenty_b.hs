module Main where
import Data.List (elemIndex, lookup, splitAt)
import Data.Maybe (fromMaybe)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, many, oneOf, runParser, sepBy, try)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... maybe you can figure out in advance which combinations of ratings will be accepted or rejected.
--
--	Each of the four ratings (x, m, a, s) can have an integer value ranging from a minimum of 1 to a
--	maximum of 4000. Of all possible distinct combinations of ratings, your job is to figure out which
--	ones will be accepted.
--
--	In the above example, there are 167409079868000 distinct combinations of ratings that will be
--	accepted.
--
--	Consider only your list of workflows; the list of part ratings that the Elves wanted you to sort is no
--	longer relevant. How many distinct combinations of ratings will be accepted by the Elves' workflows?
--
-- Test:
--	% cd ../.. && echo -e 'px{a<2006:qkq,m>2090:A,rfg}\npv{a>1716:R,A}\nlnx{m>1548:A,A}\nrfg{s<537:gd,x>2440:R,A}\nqs{s>3448:A,lnx}\nqkq{x<1416:A,crn}\ncrn{x>2662:A,R}\nin{s<1351:px,qqz}\nqqz{s>2770:qs,m<1801:hdj,R}\ngd{a>3333:R,R}\nhdj{m>838:A,pv}\n\n{x=787,m=2655,a=1222,s=2876}\n{x=1679,m=44,a=2067,s=496}\n{x=2036,m=264,a=79,s=2244}\n{x=2461,m=1339,a=466,s=291}\n{x=2127,m=1623,a=2188,s=1013}' | cabal run 2023_day19-aplenty_b # 167409079868000
--	% cd ../.. && echo -e 'lqv{x>2509:rgv,mdj}' | cabal run 2023_day19-aplenty_b

type Name = String
type Rule = (Char, Char, Int, Name) -- category, LT or GT, int literal, destination workflow
type Workflow = (Name, [Rule]) -- workflow name, list of rules (default destination is last rule w/ 'D' category)

type Parser = Parsec Void String

puzzle :: Parser [Workflow]
puzzle = many workflow <* newline

workflow :: Parser Workflow
workflow = (,) <$> (many letterChar <* char '{') <*> (rule `sepBy` char ',' <* char '}') <* newline

rule :: Parser Rule
rule = try ((,,,) <$> oneOf "xmas" <*> oneOf "<>" <*> (decimal <* char ':') <*> many letterChar)
           <|> (('D', '~', 0,) <$> many letterChar) -- default rule

type Part = [(Int, Int)]

resolve :: [Workflow] -> Maybe Part -> Int
resolve ws Nothing = 0
resolve ws p = resolve' "in" p
  where resolve' :: Name -> Maybe Part -> Int
        resolve' nm Nothing = 0
        resolve' nm (Just p)
          | nm == "R" = 0
          | nm == "A" = product . map (succ . uncurry (flip (-))) $ p
          | otherwise = fst . foldl' splitResolve (0, Just p) $ rs
          where Just rs = lookup nm ws
                --
                splitResolve :: (Int, Maybe Part) -> Rule -> (Int, Maybe Part)
                splitResolve acc@(_, Nothing) _ = acc
                splitResolve (acc, p) ('D', op, val, nm) = (acc + resolve' nm p, Nothing) -- final default rule
                splitResolve (acc, Just p) (cat, op, val, nm) =
                  let p' | '<' == op = splitLT cat val p
                         | '>' == op = splitGT cat val p
                      np | '<' == op = splitGT cat (val - 1) p
                         | '>' == op = splitLT cat (val + 1) p
                  in (acc + resolve' nm p', np)
                --
                getIndex ch = fromMaybe 999 $ ch `elemIndex` "xmas"
                splitLT :: Char -> Int -> Part -> Maybe Part
                splitLT cat val p = let (h,(x0, x1):cs) = splitAt (getIndex cat) p
                                        x' = (x0, min x1 (val - 1))
                                    in  if x0 < val then Just $ h ++ (x':cs) else Nothing
                splitGT :: Char -> Int -> Part -> Maybe Part
                splitGT cat val p = let (h,(x0, x1):cs) = splitAt (getIndex cat) p
                                        x' = (max x0 (val + 1), x1)
                                    in  if x1 > val then Just $ h ++ (x':cs) else Nothing

solve :: Either a [Workflow] -> Int
solve (Right ws) = resolve ws $ Just [(1, 4000), (1, 4000), (1, 4000), (1, 4000)]


main :: IO ()
main = interact $ show . solve . runParser puzzle "<stdin>"

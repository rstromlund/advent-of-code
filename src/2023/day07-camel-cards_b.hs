module Main where
import Data.List (group, sort, sortOn)
import Data.List.Extra (groupOn)
import Data.Ord (Down(..))

--	... Now, J cards are jokers - wildcards that can act like whatever card would make the hand the strongest type possible.
--
--	To balance this, J cards are now the weakest individual cards, weaker even than 2. The other cards stay in the same order: A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J.
--
--	J cards can pretend to be whatever card is best for the purpose of determining hand type; for example,
--	QJJQ2 is now considered four of a kind. However, for the purpose of breaking ties between two hands of
--	the same type, J is always treated as J, not the card it's pretending to be: JKKK2 is weaker than
--	QQQQ2 because J is weaker than Q.
--
--	Now, the above example goes very differently:
--
--	32T3K 765
--	T55J5 684
--	KK677 28
--	KTJJT 220
--	QQQJA 483
--
--	* 32T3K is still the only one pair; it doesn't contain any jokers, so its strength doesn't increase.
--	* KK677 is now the only two pair, making it the second-weakest hand.
--	* T55J5, KTJJT, and QQQJA are now all four of a kind! T55J5 gets rank 3, QQQJA gets rank 4, and KTJJT gets rank 5.
--
--	With the new joker rule, the total winnings in this example are 5905.
--
--	Using the new joker rule, find the rank of every hand in your set. What are the new total winnings?
--
-- Test:
--	% cd ../.. && echo -e '32T3K 765\nT55J5 684\nKK677 28\nKTJJT 220\nQQQJA 483' | cabal run 2023_day07-camel-cards_b # = 5905

cardOrder = "J23456789TQKA" -- 13 unique cards

strengths = [
  [1, 1, 1, 1, 1], -- 0: high card
  [2, 1, 1, 1],    -- 1: one pair
  [2, 2, 1],       -- 2: two pair
  [3, 1, 1],       -- 3: three of a kind
  [3, 2],          -- 4: full house
  [4, 1],          -- 5: four of a kind
  [5]              -- 6: five of a kind
  ]

indexOf elt = length . takeWhile (/= elt)

value :: String -> Int
value = foldl' (\acc c -> (acc * 13) + val c) 0
  where val c = indexOf c cardOrder

woJoker = filter ('J' /=)

hand h = improve . sortOn Down . map length . group . sort $ nojs
  where nojs = woJoker h
        jcnt = 5 - length nojs
        improve []     = [5] -- all jokers, we got 5 of a kind! : ))
        improve (i:is) = (i + jcnt):is -- improve the strongest group (2 of a kind -> 3 of a kind, etc...)

strength (h, b) = ((indexOf (hand h) strengths, value h), (h, b))

score :: (Int, ((Int, Int), (String, Int))) -> Int
score (r, ((_s, _h), (_cs, b))) = r * b

solve :: [(String, Int)] -> Int
solve = sum . zipWith (curry score) [1..] . sortOn fst . map strength


main :: IO ()
main = interact $ show . solve . map ((\[h, b] -> (h, read b)) . words) . lines

module Main where
import Data.Array.Unboxed as A ((!), Array, assocs, bounds, inRange, listArray)
import Data.Bifunctor (bimap)
import Data.Function (on)
import Data.List as L (groupBy, map, sort)
import Data.Maybe (fromMaybe)
import Data.Set as S (Set, elems, empty, foldl', lookupGT, lookupLE, lookupMax, lookupMin, insert, map, singleton)

--	The Elf seems confused by your answer until he realizes his mistake: he was reading from a list of his
--	favorite numbers that are both perfect squares and perfect cubes, not his step counter.
--
--	The actual number of steps he needs to get today is exactly 26501365.
--
--	He also points out that the garden plots and rocks are set up so that the map repeats infinitely in
--	every direction.
--
--	So, if you were to look one additional map-width or map-height out from the edge of the example map
--	above, you would find that it keeps repeating:
--
--	...
--
--	This is just a tiny three-map-by-three-map slice of the inexplicably-infinite farm layout; garden
--	plots and rocks repeat as far as you can see. The Elf still starts on the one middle tile marked S,
--	though - every other repeated S is replaced with a normal garden plot (.).
--
--	Here are the number of reachable garden plots in this new infinite version of the example map for different numbers of steps:
--
--	* In exactly 6 steps, he can still reach 16 garden plots.
--	* In exactly 10 steps, he can reach any of 50 garden plots.
--	* In exactly 50 steps, he can reach 1594 garden plots.
--	* In exactly 100 steps, he can reach 6536 garden plots.
--	* In exactly 500 steps, he can reach 167004 garden plots.
--	* In exactly 1000 steps, he can reach 668697 garden plots.
--	* In exactly 5000 steps, he can reach 16733044 garden plots.
--
--	However, the step count the Elf needs is much larger! Starting from the garden plot marked S on your
--	infinite map, how many garden plots could the Elf reach in exactly 26501365 steps?
--
-- Test:
--	% cd ../.. && echo -e '...........\n.....###.#.\n.###.##..#.\n..#.#...#..\n....#.#....\n.##..S####.\n.##..#...#.\n.......##..\n.##.#.####.\n.##..##.##.\n...........' | cabal run 2023_day21-step-counter_b # = 50

{-
26501365 = 131 * 202300
https://www.reddit.com/media?url=https%3A%2F%2Fi.redd.it%2F7amw9s5jdm7c1.gif
https://github.com/villuna/aoc23/wiki/A-Geometric-solution-to-advent-of-code-2023,-day-21


visualize test data:

["  6 17  9"
," 22 39 24"
,"  9 17  2"]


["     6 17  9   "
,"  6 36 39 38  9"
," 19 39 42 39 18"
,"  9 38 39 29  2"
,"     9 15  2   "]


["        6 17  9      "
,"     6 36 39 38  9   "
,"  6 36 39 42 39 38  9"
," 18 39 42 39 42 39 15"
,"  9 38 39 42 39 29  2"
,"     9 38 39 29  2   "
,"        9 15  2      "]

Diagonals, diamond points, etc... all grow uniformly. Use that in "real" answer.

Visualize step 3 in real puzzle data:

            932 5522  935
       932 6415 7335 6427  935
  932 6415 7335 7320 7335 6427  935
 5506 7335 7320 7335 7320 7335 5534
  937 6411 7335 7320 7335 6427  931
       937 6411 7335 6427  931
            937 5518  931

Diamond points (N,S,E,W), diagonals and interior to diagonals, odd tiles, even tiles all contribute to the formula below.

-}

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) . concat $ ls
  where ls@(l0:_) = lines cs
        ly = pred . length $ ls
        lx = pred . length $ l0

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

cardinalDirections = [north, south, east, west]

type State = Set Point

moves :: PuzzleMap -> Int -> State -> State
moves pm = moves'
  where ((minY, minX), (maxY, maxX)) = bounds pm
        height = maxY - minY + 1
        width  = maxX - minX + 1
        --
        moves' :: Int -> State -> State
        moves' 0   ss = ss
        moves' cnt ss = moves' (cnt - 1) . S.foldl' getNeighbors empty $ ss
        --
        getNeighbors :: State -> Point -> State
        getNeighbors acc (y, x) = L.foldl' (flip insert) acc [pt | pt <- L.map (bimap (+ y) (+ x)) cardinalDirections, garden pt]
        --
        garden pt@(y, x) = pm ! (y `mod` height, x `mod` width) /= '#'

groupTiles :: PuzzleMap -> State -> [[Int]]
groupTiles pm ss = [[lookupCell (y,x) | x <- rangeX y] | y <- [minBY..maxBY]]
  where ((minY, minX), (maxY, maxX)) = bounds pm
        height = maxY - minY + 1
        width  = maxX - minX + 1
        Just ((minBY, _), _) = lookupMin gs
        Just ((maxBY, _), _) = lookupMax gs
        rangeX y = let maxX = snd . fst . fromMaybe ((0,0), 0) . lookupLE ((y, maxBound), maxBound) $ gs
                       minX = snd . fst . fromMaybe ((0,0), 0) . lookupGT ((y, minBound), maxBound) $ gs
                   in [minX..maxX]
        --
        gs :: Set (Point, Int)
        gs = L.foldl' (\acc bs@((bpt@(y, x), _):_) -> insert (bpt, length bs) acc) empty . groupBy ((==) `on` fst) . sort . L.map groupTiles' . elems $ ss
        --
        lookupCell :: Point -> Int
        lookupCell pt = snd . fromMaybe ((0,0), 0) . lookupLE (pt, maxBound) $ gs
        --
        groupTiles' pt@(y, x) = ((y `div` height, x `div` width), pt)

score :: [[Int]] -> Int
score nss = let odd = mid . mid $ nss
                evn = mid nss !! 2
                n   = mid . head $ nss
                s   = mid . last $ nss
                e   = last . mid $ nss
                w   = head . mid $ nss
                nw  = head . head $ nss
                nw1 = (nss !! 1) !! 1
                ne  = last . head $ nss
                ne1 = reverse (nss !! 1) !! 1
                sw  = head . last $ nss
                sw1 = reverse nss !! 1 !! 1
                se  = last . last $ nss
                se1 = reverse (reverse nss !! 1) !! 1
                n_  = 202300
            in n+s+e+w+(n_*nw)+(n_*ne)+(n_*sw)+(n_*se)+((n_-1)*nw1)+((n_-1)*ne1)+((n_-1)*sw1)+((n_-1)*se1)+(odd*n_*n_)+(evn*(n_-1)*(n_-1))
  where mid xs = xs !! (length xs `div` 2)

solve :: PuzzleMap -> Int
solve pm = score ms
  where start = bimap (+ sftY) (+ sftX) . head $ [start | (start, 'S') <- assocs pm]
        ((minY, minX), (maxY, maxX)) = bounds pm
        sftY = 999999 * (maxY - minY + 1) -- shift "tiles" away from (0,0) to help groupTiles
        sftX = 999999 * (maxX - minX + 1)
        --
        ms = let cnt = 3 * 131 + 65
             in groupTiles pm . moves pm cnt . singleton $ start


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Algorithm.Search (dijkstra)
import Data.Array ((!), Array, bounds, inRange, listArray)

--	... For example:
--
--	2413432311323 0
--	3215453535623 1
--	3255245654254 2
--	3446585845452 3
--	4546657867536 4
--	1438598798454 5
--	4457876987766 6
--	3637877979653 7
--	4654967986887 8
--	4564679986453 9
--	1224686865563 A
--	2546548887735 B
--	4322674655533 C
--
--	... Each city block is marked by a single digit that represents the amount of heat loss if the
--	crucible enters that block. The starting point, the lava pool, is the top-left city block; the
--	destination, the machine parts factory, is the bottom-right city block. (Because you already start in
--	the top-left block, you don't incur that block's heat loss unless you leave that block and then return
--	to it.)
--
--	Because it is difficult to keep the top-heavy crucible going in a straight line for very long, it can
--	move at most three blocks in a single direction before it must turn 90 degrees left or right. The
--	crucible also can't reverse direction; after entering each city block, it may only turn left, continue
--	straight, or turn right.
--
--	One way to minimize heat loss is this path:
--
--	2>>34^>>>1323 0
--	32v>>>35v5623 1
--	32552456v>>54 2
--	3446585845v52 3
--	4546657867v>6 4
--	14385987984v4 5
--	44578769877v6 6
--	36378779796v> 7
--	465496798688v 8
--	456467998645v 9
--	12246868655<v A
--	25465488877v5 B
--	43226746555v> C
--
--	This path never moves more than three consecutive blocks in the same direction and incurs a heat loss
--	of only 102.
--
--	Directing the crucible from the lava pool to the machine parts factory, but not moving more than three
--	consecutive blocks in the same direction, what is the least heat loss it can incur?
--
-- Test:
--	% cd ../.. && echo -e '2413432311323\n3215453535623\n3255245654254\n3446585845452\n4546657867536\n1438598798454\n4457876987766\n3637877979653\n4654967986887\n4564679986453\n1224686865563\n2546548887735\n4322674655533' | cabal run 2023_day17-clumsy-crucible # = 102

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Int

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) . map (read . (:[])) . concat $ ls
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

-- | Rotate coordinate 90-degrees CCW about the origin
turnLeft :: Point -> Point
turnLeft  (y, x) = (-x, y)

-- | Rotate coordinate 90-degrees CW about the origin
turnRight :: Point -> Point
turnRight (y, x) = (x, -y)

type State = (Point, (Point, Int)) -- location, direction, straight traveled

solve :: PuzzleMap -> Int
solve m = cost
  where Just (cost, path') = dijkstra
          getNeighbors -- next
          (\_ (np, _) -> m ! np) -- cost
          ((end_p2 ==) . fst) -- found
          (beg_p1, (east, 0)) -- initial
        --
        (beg_p1@(minY, minX), end_p2@(maxY, maxX)) = bounds m
        --
        getNeighbors :: State -> [State]
        getNeighbors st@((y, x), (d@(dy, dx), t)) =
          let st8 | t >= 3    = []
                  | otherwise = [((y + dy, x + dx), (d, t + 1))]
              r@(dyr, dxr) = turnRight d
              l@(dyl, dxl) = turnLeft d
              stR = ((y + dyr, x + dxr), (r, 1))
              stL = ((y + dyl, x + dxl), (l, 1))
          in filter (inRange (bounds m) . fst) $ stR:stL:st8


main :: IO ()
main = interact $ show . solve . parse

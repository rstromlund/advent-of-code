module Main where
import Data.Foldable (for_)
import Data.List.Split (splitOn)
import Data.SBV (Symbolic, AlgReal, SReal, free, sat, (.==), constrain, getModelValue, SatResult(SatResult)) -- A framework for writing symbolic programs in Haskell

--	... you should be able to hit every hailstone in a single throw!
--
--	You can use the probably-magical winds to reach any integer position you like and to propel the rock
--	at any integer velocity. Now including the Z axis in your calculations, if you throw the rock at time
--	0, where do you need to be so that the rock perfectly collides with every hailstone? Due to
--	probably-magical inertia, the rock won't slow down or change direction when it collides with a
--	hailstone.
--
--	In the example above, you can achieve this by moving to position 24, 13, 10 and throwing the rock at
--	velocity -3, 1, 2. If you do this, you will hit every hailstone
--
--	... So, at time 0, the rock needs to be at X position 24, Y position 13, and Z position 10. Adding these
--	three coordinates together produces 47. (Don't add any coordinates from the rock's velocity.)
--
--	Determine the exact position and velocity the rock needs to have at time 0 so that it perfectly
--	collides with every hailstone. What do you get if you add up the X, Y, and Z coordinates of that
--	initial position?
--
-- Test:
--	% cd ../.. && echo -e '19, 13, 30 @ -2,  1, -2\n18, 19, 22 @ -1, -1, -2\n20, 25, 34 @ -2, -2, -4\n12, 31, 28 @ -1, -2, -1\n20, 19, 15 @  1, -5, -3' | cabal run 2023_day24-never-tell-me-the-odds_b # = 47

type Point = (Int, Int, Int) -- X,Y,Z (or velocity/magnitude X,Y,Z)
type Stone = (Point, Point)
type Hail  = [Stone]

parse :: String -> IO Hail
parse = return . map (mkStone . splitOn "@") . lines
  where mkStone :: [String] -> Stone
        mkStone [p, v] = (mkPoint p, mkPoint v)
        mkPoint :: String -> Point
        mkPoint = (\[x,y,z] -> (x,y,z)) . map read . splitOn ","

{-
Z3 = https://en.wikipedia.org/wiki/Z3_Theorem_Prover

https://www.youtube.com/watch?v=N-qUfr-rz_Y
Intersection of Two Lines in 3D Space | Intersecting Lines

https://www.youtube.com/watch?v=bvlIYX9cgls
Find the Intersection of Two Line Segments in 2D (Easy Method)

https://demonstrations.wolfram.com/IntersectionOfTwoLinesUsingVectors/

https://math.stackexchange.com/questions/406864/intersection-of-two-lines-in-vector-form

First 3 stone equations:
x - 309254625334097 + t0 * (dx - -42) = 0
y - 251732589486275 + t0 * (dy - -22) = 0
z - 442061964691135 + t0 * (dz - -45) = 0

x - 494902262649699 + t1 * (dx - -345) = 0
y - 448845738683125 + t1 * (dy - -319) = 0
z - 408766676225787 + t1 * (dz - -201) = 0

x - 281199817421623 + t2 * (dx - 89) = 0
y - 235413393248399 + t2 * (dy - 152) = 0
z - 236652333766125 + t2 * (dz - -70) = 0
-}

solve :: Hail -> IO Int -- AlgReal
solve ss = do
  SatResult res <- sat $ do
    x_  <- free "x" :: Symbolic SReal
    y_  <- free "y"
    z_  <- free "z"
    dx_ <- free "dx"
    dy_ <- free "dy"
    dz_ <- free "dz"
    for_ (take 3 ss) (\((x, y, z), (dx, dy, dz)) ->
      do t_ <- free "t"
         constrain (t_ * (dx_ - fromIntegral dx) .== (fromIntegral x - x_))
         constrain (t_ * (dy_ - fromIntegral dy) .== (fromIntegral y - y_))
         constrain (t_ * (dz_ - fromIntegral dz) .== (fromIntegral z - z_)))

  case (getModelValue "x" res, getModelValue "y" res, getModelValue "z" res) of
    (Just x, Just y, Just z) -> return (x+y+z :: AlgReal)
    _ -> fail "no solution"


main :: IO ()
main = getContents >>= parse >>= solve >>= print

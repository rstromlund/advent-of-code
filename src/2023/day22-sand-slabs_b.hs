module Main where
import Control.Parallel.Strategies (parList, rdeepseq, using)
import Data.List (sort)
import Data.List.Split (splitOn)

--	... For each brick, determine how many other bricks would fall if that brick were disintegrated.
--
--	Using the same example as above:
--
--	* Disintegrating brick A would cause all 6 other bricks to fall.
--	* Disintegrating brick F would cause only 1 other brick, G, to fall.
--
--	Disintegrating any other brick would cause no other bricks to fall. So, in this example, the sum of
--	the number of other bricks that would fall as a result of disintegrating each brick is 7.
--
--	For each brick, determine how many other bricks would fall if that brick were disintegrated. What is
--	the sum of the number of other bricks that would fall?
--
-- Test:
--	% cd ../.. && echo -e '1,0,1~1,2,1\n0,0,2~2,0,2\n0,2,3~2,2,3\n0,0,4~0,2,4\n2,0,5~2,2,5\n0,1,6~2,1,6\n1,1,8~1,1,9' | cabal run 2023_day22-sand-slabs_b # = 7

type Point = (Int, Int, Int) -- Z,X,Y
type Brick = (Point, Point)
type Bricks = [Brick]

parse :: String -> Bricks
parse = sort . map (mkBrick . splitOn "~") . lines
  where mkBrick :: [String] -> Brick
        mkBrick [b, e] = assertBrick (mkPoint b, mkPoint e)
        mkPoint :: String -> Point
        mkPoint = (\[x,y,z] -> (z,x,y)) . map read . splitOn ","
        --
        assertBrick b@((z0,x0,y0), (z1,x1,y1))
          | x0 <= x1 && y0 <= y1 && z0 <= z1 && ((x0 == x1 && y0 == y1) || (x0 == x1 && z0 == z1) || (y0 == y1 && z0 == z1)) = b
          | otherwise = error . show $ ("assertBrick: failed tests", b)

collision :: Brick -> Brick -> Bool
collision ((z0,x0,y0), (z1,x1,y1)) ((c0,a0,b0), (c1,a1,b1))
  | z0 < 1 || z1 < 1 = True -- collide w/ the ground
  | otherwise        = x0 <= a1 && x1 >= a0 && y0 <= b1 && y1 >= b0 && z0 <= c1 && z1 >= c0
  -- ^^ I don't think we need a fully generic line intersection in 3d space algorithm since only 1 dimension varies. (?)

fall' :: Bricks -> Brick -> Brick
fall' [] ((z0,x0,y0), (z1,x1,y1)) = ((1,x0,y0), (z1-z0+1,x1,y1))
fall' acc b@((z0,x0,y0), (z1,x1,y1)) =
  let b' = ((z0-1,x0,y0), (z1-1,x1,y1))
  in if any (collision b') acc then b else fall' acc b'

fall :: Bricks -> Bricks
fall = reverse . foldl' (\acc b -> fall' acc b:acc) []

-- TODO: this has to benefit from memoization, maybe more than parallelization.  This took a little over 4 minutes this way.

disintegrate :: Bricks -> Int
disintegrate bs = let is = map (\b -> disintegrate' . splitOn [b] $ bs) bs  -- instead of splitOn, zip inits and tails
                  in sum (is `using` parList rdeepseq) -- evaluate each brick in parallel
  where disintegrate' :: [Bricks] -> Int
        disintegrate' [acc, bs] = cntFall 0 acc bs
        cntFall n _ [] = n
        cntFall n acc (b:bs) =
          let b' = fall' acc b
          in cntFall (n + if b' /= b then 1 else 0) (b':acc) bs

solve :: Bricks -> Int
solve = disintegrate . fall


main :: IO ()
main = interact $ show . solve . parse

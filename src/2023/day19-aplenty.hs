module Main where
import Data.HashMap.Strict as M (HashMap, empty, findWithDefault, insert)
import Data.List (last, lookup)
import Data.List.Extra (sumOn')
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, oneOf, runParser, sepBy, try)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... each part is rated in each of four categories:
--
--	* x: Extremely cool looking
--	* m: Musical (it makes a noise when you hit it)
--	* a: Aerodynamic
--	* s: Shiny
--
--	Then, each part is sent through a series of workflows that will ultimately accept or reject the
--	part. Each workflow has a name and contains a list of rules; each rule specifies a condition and where
--	to send the part if the condition is true. The first rule that matches the part being considered is
--	applied immediately, and the part moves on to the destination described by the rule. (The last rule in
--	each workflow has no condition and always applies if reached.)
--
--	Consider the workflow ex{x>10:one,m<20:two,a>30:R,A}. This workflow is named ex and contains four
--	rules. If workflow ex were considering a specific part, it would perform the following steps in order:
--
--	* Rule "x>10:one": If the part's x is more than 10, send the part to the workflow named one.
--	* Rule "m<20:two": Otherwise, if the part's m is less than 20, send the part to the workflow named two.
--	* Rule "a>30:R": Otherwise, if the part's a is more than 30, the part is immediately rejected (R).
--	* Rule "A": Otherwise, because no other rules matched the part, the part is immediately accepted (A).
--
--	If a part is sent to another workflow, it immediately switches to the start of that workflow instead
--	and never returns. If a part is accepted (sent to A) or rejected (sent to R), the part immediately
--	stops any further processing.
--
--	The system works, but it's not keeping up with the torrent of weird metal shapes. The Elves ask if you
--	can help sort a few parts and give you the list of workflows and some part ratings (your puzzle
--	input). For example:
--
--	px{a<2006:qkq,m>2090:A,rfg}
--	pv{a>1716:R,A}
--	lnx{m>1548:A,A}
--	rfg{s<537:gd,x>2440:R,A}
--	qs{s>3448:A,lnx}
--	qkq{x<1416:A,crn}
--	crn{x>2662:A,R}
--	in{s<1351:px,qqz}
--	qqz{s>2770:qs,m<1801:hdj,R}
--	gd{a>3333:R,R}
--	hdj{m>838:A,pv}
--
--	{x=787,m=2655,a=1222,s=2876}
--	{x=1679,m=44,a=2067,s=496}
--	{x=2036,m=264,a=79,s=2244}
--	{x=2461,m=1339,a=466,s=291}
--	{x=2127,m=1623,a=2188,s=1013}
--
--	The workflows are listed first, followed by a blank line, then the ratings of the parts the Elves
--	would like you to sort. All parts begin in the workflow named in. In this example, the five listed
--	parts go through the following workflows:
--
--	* {x=787,m=2655,a=1222,s=2876}: in -> qqz -> qs -> lnx -> A
--	* {x=1679,m=44,a=2067,s=496}: in -> px -> rfg -> gd -> R
--	* {x=2036,m=264,a=79,s=2244}: in -> qqz -> hdj -> pv -> A
--	* {x=2461,m=1339,a=466,s=291}: in -> px -> qkq -> crn -> R
--	* {x=2127,m=1623,a=2188,s=1013}: in -> px -> rfg -> A
--
--	Ultimately, three parts are accepted. Adding up the x, m, a, and s rating for each of the accepted
--	parts gives 7540 for the part with x=787, 4623 for the part with x=2036, and 6951 for the part with
--	x=2127. Adding all of the ratings for all of the accepted parts gives the sum total of 19114.
--
--	Sort through all of the parts you've been given; what do you get if you add together all of the rating
--	numbers for all of the parts that ultimately get accepted?
--
-- Test:
--	% cd ../.. && echo -e 'px{a<2006:qkq,m>2090:A,rfg}\npv{a>1716:R,A}\nlnx{m>1548:A,A}\nrfg{s<537:gd,x>2440:R,A}\nqs{s>3448:A,lnx}\nqkq{x<1416:A,crn}\ncrn{x>2662:A,R}\nin{s<1351:px,qqz}\nqqz{s>2770:qs,m<1801:hdj,R}\ngd{a>3333:R,R}\nhdj{m>838:A,pv}\n\n{x=787,m=2655,a=1222,s=2876}\n{x=1679,m=44,a=2067,s=496}\n{x=2036,m=264,a=79,s=2244}\n{x=2461,m=1339,a=466,s=291}\n{x=2127,m=1623,a=2188,s=1013}' | cabal run 2023_day19-aplenty # 19114

type Name = String
type Comparison = (Int -> Int -> Bool)
type Rule = (Char, Comparison, Int, Name) -- category, LT or GT, int literal, destination workflow
type Workflow = (Name, ([Rule], Name)) -- Our name, list of rules, default destination workflow
type Part = (Int, Int, Int, Int) -- x, m, a, s

type Parser = Parsec Void String

puzzle :: Parser ([Workflow], [Part])
puzzle = (,) <$> (many workflow <* newline) <*> many part <* eof -- <* newline

workflow :: Parser Workflow
workflow = mkWorkflow <$> (many letterChar <* char '{') <*> (rule `sepBy` char ',' <* char '}') <* newline
  where mkWorkflow :: Name -> [Rule] -> Workflow
        mkWorkflow nm rs = (nm, (init rs, (\(_, _, _, nm) -> nm) . last $ rs))

rule :: Parser Rule
rule = try ((,,,) <$> oneOf "xmas" <*> choice [(<) <$ char '<', (>) <$ char '>'] <*> (decimal <* char ':') <*> many letterChar)
           <|> (('*', (==), 0,) <$> many letterChar)

part :: Parser Part
part = (,,,) <$> (string "{x=" *> decimal) <*> (string ",m=" *> decimal) <*> (string ",a=" *> decimal) <*> (string ",s=" *> decimal) <* char '}' <* newline

nextWorkflow :: Name -> Part -> [Rule] -> Name
nextWorkflow dnm p@(x_,m_,a_,s_) (r@(cat, op, val, nm):rs)
  | getOperand cat `op` val = nm
  | otherwise               = nextWorkflow dnm p rs
  where getOperand 'x' = x_
        getOperand 'm' = m_
        getOperand 'a' = a_
        getOperand 's' = s_
nextWorkflow dnm _ [] = dnm

resolve :: HashMap Name [Part] -> [Workflow] -> [Part] -> HashMap Name [Part]
resolve m ws (p:ps) = let m' = resolve' "in"
                      in resolve m' ws ps
  where resolve' nm
          | nm `elem` ["A", "R"] = insert nm (p:findWithDefault [] nm m) m
          | otherwise = let Just (rs, dnm) = w
                            nm' = nextWorkflow dnm p rs
                        in resolve' nm'
          where w = lookup nm ws
resolve m ws [] = m

solve :: Either a ([Workflow], [Part]) -> Int
solve (Right (ws, ps)) = sumOn' (\(x_,m_,a_,s_) -> x_ + m_ + a_ + s_) . findWithDefault [] "A" . resolve empty ws $ ps


main :: IO ()
main = interact $ show . solve . runParser puzzle "<stdin>"

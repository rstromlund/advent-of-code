{-# Language BangPatterns #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed ((!), Array, assocs, bounds, inRange, listArray)
import Data.Bifunctor (bimap)
import Data.List (find, )
import Data.Maybe (isNothing)
import Data.Set as S (Set, insert, notMember, singleton)

--	... As you reach the trailhead, you realize that the ground isn't as slippery as you expected; you'll have no problem climbing up the steep slopes.
--
--	Now, treat all slopes as if they were normal paths (.). You still want to make sure you have the most
--	scenic hike possible, so continue to ensure that you never step onto the same tile twice. What is the
--	longest hike you can take?
--
--	In the example above, this increases the longest hike to 154 steps
--
-- Test:
--	% cd ../.. && echo -e '#.#####################\n#.......#########...###\n#######.#########.#.###\n###.....#.>.>.###.#.###\n###v#####.#v#.###.#.###\n###.>...#.#.#.....#...#\n###v###.#.#.#########.#\n###...#.#.#.......#...#\n#####.#.#.#######.#.###\n#.....#.#.#.......#...#\n#.#####.#.#.#########v#\n#.#...#...#...###...>.#\n#.#.#v#######v###.###v#\n#...#.>.#...>.>.#.###.#\n#####v#.#.###v#.#.###.#\n#.....#...#...#.#.#...#\n#.#########.###.#.#.###\n#...###...#...#...#.###\n###.###.#.###v#####v###\n#...#...#.#.>.>.#.>.###\n#.###.###.#.###.#.#v###\n#.....###...###...#...#\n#####################.#' | cabal run 2023_day23-a-long-walk_b # = 154

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls
        pm = listArray ((0, 0), (ly, lx)) . concat $ ls
        [(start, _), (end, _)] = filter (\((y, _), c) -> y `elem` [0, ly] && '.' == c) . assocs $ pm

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

cardinalDirections = [north, south, east, west]

-- https://en.wikipedia.org/wiki/Edge_contraction

-- Return all the vertices and valid edges w/ edge length
edgeContraction :: (PuzzleMap, Point, Point) -> [(Point, [(Point, Int)])]
edgeContraction (!pm, !s, !e) = map getEdges vs
  where getNeighbors :: Point -> [Point]
        getNeighbors st@(y, x) = filter (\st' -> inRange (bounds pm) st' && '#' /= (pm ! st')) . map (bimap (+ y) (+ x)) $ cardinalDirections
        --
        vs = s:([pt | (pt, ch) <- assocs pm, '#' /= ch, (length . getNeighbors $ pt) > 2] ++ [e])
        addEdge p d acc dij
          | p == d || isNothing dij = acc
          | otherwise               = let Just (cost, path) = dij in (d, cost):acc
        --
        getEdges p = (p,
          -- Use dijkstra to find our edge, if it exists
          reverse . foldl' (
              \acc d -> addEdge p d acc . dijkstra
                          (filter (\n -> n == d || n `notElem` vs) . getNeighbors) -- don't cross other vertices
                          (const . const $ 1) (d ==)
                          $ p
              ) [] $ vs)

findPaths :: (PuzzleMap, Point, Point) -> [(Point, [(Point, Int)])] -> Int
findPaths (!pm, _, !e) vs@((s,_):_) = maximum . findPaths' s $ (0, singleton s)
  where findPaths' :: Point -> (Int, Set Point) -> [Int]
        findPaths' !pt (!c, !seen)
          | e == pt   = [c]
          | otherwise = concatMap (\v@(n, c') -> findPaths' n (c + c', insert n seen)) . filter ((`notMember` seen) . fst) . (\(Just (_, ns)) -> ns) . find (\(pt', _) -> pt == pt') $ vs

solve :: (PuzzleMap, Point, Point) -> Int
solve ps = findPaths ps . edgeContraction $ ps


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.List (transpose)

--	The galaxies are much older (and thus much farther apart) than the researcher initially estimated.
--
--	Now, instead of the expansion you did before, make each empty row or column one million times
--	larger. That is, each empty row should be replaced with 1000000 empty rows, and each empty column
--	should be replaced with 1000000 empty columns.
--
--	(In the example above, if each empty row or column were merely 10 times larger, the sum of the
--	shortest paths between every pair of galaxies would be 1030. If each empty row or column were merely
--	100 times larger, the sum of the shortest paths between every pair of galaxies would be 8410. However,
--	your universe will need to expand far beyond these values.)
--
--	Starting with the same initial image, expand the universe according to these new rules, then find the
--	length of the shortest path between every pair of galaxies. What is the sum of these lengths?
--
-- Test:
--	% cd ../.. && echo -e '...#......\n.......#..\n#.........\n..........\n......#...\n.#........\n.........#\n..........\n.......#..\n#...#.....' | cabal run 2023_day11-cosmic-expansion_b # = 1030 (for expand by 10)

-- expandFactor = 9 -- test input
expandFactor = 999999 -- puzzle input

type Point = (Int, Int) -- Y,X

manhattanDist :: (Point, Point) -> Int
manhattanDist ((y1, x1), (y2, x2)) = abs (y2 - y1) + abs (x2 - x1)

expand :: [String] -> ([Int], [Int])
expand gs = (expand' gs, expand' . transpose $ gs)
  where expand' = foldl' (\acc (y, xs) -> if all ('.' ==) xs then y:acc else acc) [] . zip [0..]

grid :: [String] -> [(Point, Char)]
grid gs = zip [pt | y <- [0..ly], x <- [0..lx], let pt = (y, x)] . concat $ gs
  where ly = pred . length $ gs
        lx = pred . length . head $ gs

combos :: [Point] -> [(Point, Point)]
combos [_]    = []
combos (g:gs) = map (g,) gs ++ combos gs

moveGalaxy (rs, cs) (y, x) = (shiftCoord y rs, shiftCoord x cs)
  where shiftCoord = foldl' (\y r -> if y > r then y + expandFactor else y)

solve :: [String] -> Int
solve gs = sum . map manhattanDist . combos $ gpts
  where gpts = map (moveGalaxy es . fst) . filter (('.' /=) . snd) . grid $ gs
        es = expand gs


main :: IO ()
main = interact $ show . solve . lines

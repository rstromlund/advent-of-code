module Main where
import Data.Array.Unboxed ((!), Array, assocs, bounds, inRange, listArray)
import Data.Bifunctor (bimap)
import Data.Set as S (Set, insert, notMember, singleton, size)

--	There's a map of nearby hiking trails (your puzzle input) that indicates paths (.), forest (#), and steep slopes (^, >, v, and <).
--
--	For example:
--
--	#.#####################
--	#.......#########...###
--	#######.#########.#.###
--	###.....#.>.>.###.#.###
--	###v#####.#v#.###.#.###
--	###.>...#.#.#.....#...#
--	###v###.#.#.#########.#
--	###...#.#.#.......#...#
--	#####.#.#.#######.#.###
--	#.....#.#.#.......#...#
--	#.#####.#.#.#########v#
--	#.#...#...#...###...>.#
--	#.#.#v#######v###.###v#
--	#...#.>.#...>.>.#.###.#
--	#####v#.#.###v#.#.###.#
--	#.....#...#...#.#.#...#
--	#.#########.###.#.#.###
--	#...###...#...#...#.###
--	###.###.#.###v#####v###
--	#...#...#.#.>.>.#.>.###
--	#.###.###.#.###.#.#v###
--	#.....###...###...#...#
--	#####################.#
--
--	You're currently on the single path tile in the top row; your goal is to reach the single path tile in
--	the bottom row. Because of all the mist from the waterfall, the slopes are probably quite icy; if you
--	step onto a slope tile, your next step must be downhill (in the direction the arrow is pointing). To
--	make sure you have the most scenic hike possible, never step onto the same tile twice. What is the
--	longest hike you can take?
--
--	In the example above, the longest hike you can take is marked with O, and your starting position is marked S:
--
--	#S#####################
--	#OOOOOOO#########...###
--	#######O#########.#.###
--	###OOOOO#OOO>.###.#.###
--	###O#####O#O#.###.#.###
--	###OOOOO#O#O#.....#...#
--	###v###O#O#O#########.#
--	###...#O#O#OOOOOOO#...#
--	#####.#O#O#######O#.###
--	#.....#O#O#OOOOOOO#...#
--	#.#####O#O#O#########v#
--	#.#...#OOO#OOO###OOOOO#
--	#.#.#v#######O###O###O#
--	#...#.>.#...>OOO#O###O#
--	#####v#.#.###v#O#O###O#
--	#.....#...#...#O#O#OOO#
--	#.#########.###O#O#O###
--	#...###...#...#OOO#O###
--	###.###.#.###v#####O###
--	#...#...#.#.>.>.#.>O###
--	#.###.###.#.###.#.#O###
--	#.....###...###...#OOO#
--	#####################O#
--
--	This hike contains 94 steps. (The other possible hikes you could have taken were 90, 86, 82, 82, and 74 steps long.)
--
--	Find the longest hike you can take through the hiking trails listed on your map. How many steps long is the longest hike?
--
-- Test:
--	% cd ../.. && echo -e '#.#####################\n#.......#########...###\n#######.#########.#.###\n###.....#.>.>.###.#.###\n###v#####.#v#.###.#.###\n###.>...#.#.#.....#...#\n###v###.#.#.#########.#\n###...#.#.#.......#...#\n#####.#.#.#######.#.###\n#.....#.#.#.......#...#\n#.#####.#.#.#########v#\n#.#...#...#...###...>.#\n#.#.#v#######v###.###v#\n#...#.>.#...>.>.#.###.#\n#####v#.#.###v#.#.###.#\n#.....#...#...#.#.#...#\n#.#########.###.#.#.###\n#...###...#...#...#.###\n###.###.#.###v#####v###\n#...#...#.#.>.>.#.>.###\n#.###.###.#.###.#.#v###\n#.....###...###...#...#\n#####################.#' | cabal run 2023_day23-a-long-walk # = 94

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls
        pm = listArray ((0, 0), (ly, lx)) . concat $ ls
        [(start, _), (end, _)] = filter (\((y, _), c) -> y `elem` [0, ly] && '.' == c) . assocs $ pm

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

cardinalDirections = [north, south, east, west]

findPaths :: (PuzzleMap, Point, Point) -> Point -> Set Point -> [Set Point]
findPaths ps@(pm, _, e) st@(y, x) ss
  | e == st   = [ss]
  | otherwise = concatMap (\n -> findPaths ps n . insert n $ ss) getNeighbors
  where bndry = bounds pm
        getNeighbors =
          let c   = pm ! st
              cds | '^' == c  = [north]
                  | 'v' == c  = [south]
                  | '>' == c  = [east]
                  | '<' == c  = [west]
                  | otherwise = cardinalDirections
              ns  = map (bimap (+ y) (+ x)) cds
          in filter (\st' -> inRange bndry st' && '#' /= (pm ! st') && st' `notMember` ss) ns

solve :: (PuzzleMap, Point, Point) -> Int
solve (m, start, end) = pred . maximum . map size . findPaths (m, start, end) start . singleton $ start


main :: IO ()
main = interact $ show . solve . parse

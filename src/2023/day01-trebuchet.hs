module Main where
import Data.Char (isDigit)

--	... On each line, the calibration value can be found by combining the first digit and the last digit (in that order) to form a single two-digit number.
--
--	For example:
--
--	1abc2
--	pqr3stu8vwx
--	a1b2c3d4e5f
--	treb7uchet
--	In this example, the calibration values of these four lines are 12, 38, 15, and 77. Adding these together produces 142.
--
--	Consider your entire calibration document. What is the sum of all of the calibration values?
--
-- Test:
--	% cd ../.. && echo -e '1abc2\npqr3stu8vwx\na1b2c3d4e5f\ntreb7uchet' | cabal run 2023_day01-trebuchet # = 142

firstLast :: String -> Int
firstLast str = read $ head str:[last str]

main :: IO ()
main = interact $ show . sum . map (firstLast . filter isDigit) . lines

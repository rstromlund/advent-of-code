module Main where
import Data.Array ((!), Array, bounds, inRange, listArray)
import Data.List as L (lookup, map)
import Data.Maybe (fromMaybe)
import Data.Set as S (Set, empty, fromList, map, notMember, size, union)

--	... Upon closer inspection, the contraption appears to be a flat, two-dimensional square grid containing empty space (.), mirrors (/ and \), and splitters (| and -).
--
--	The contraption is aligned so that most of the beam bounces around the grid, but each tile on the grid
--	converts some of the beam's light into heat to melt the rock in the cavern.
--
--	You note the layout of the contraption (your puzzle input). For example:
--
--	.|...\....
--	|.-.\.....
--	.....|-...
--	........|.
--	..........
--	.........\
--	..../.\\..
--	.-.-/..|..
--	.|....-|.\
--	..//.|....
--
--	The beam enters in the top-left corner from the left and heading to the right. Then, its behavior depends on what it encounters as it moves:
--
--	* If the beam encounters empty space (.), it continues in the same direction.
--	* If the beam encounters a mirror (/ or \), the beam is reflected 90 degrees depending on the angle of
--	  the mirror. For instance, a rightward-moving beam that encounters a / mirror would continue upward
--	  in the mirror's column, while a rightward-moving beam that encounters a \ mirror would continue
--	  downward from the mirror's column.
--	* If the beam encounters the pointy end of a splitter (| or -), the beam passes through the splitter
--	  as if the splitter were empty space. For instance, a rightward-moving beam that encounters a -
--	  splitter would continue in the same direction.
--	* If the beam encounters the flat side of a splitter (| or -), the beam is split into two beams going
--	  in each of the two directions the splitter's pointy ends are pointing. For instance, a
--	  rightward-moving beam that encounters a | splitter would split into two beams: one that continues
--	  upward from the splitter's column and one that continues downward from the splitter's column.
--
--	Beams do not interact with other beams; a tile can have many beams passing through it at the same
--	time. A tile is energized if that tile has at least one beam pass through it, reflect in it, or split
--	in it.
--
--	In the above example, here is how the beam of light bounces around the contraption:
--
--	>|<<<\.... 0
--	|v-.\^.... 1
--	.v...|->>> 2
--	.v...v^.|. 3
--	.v...v^... 4
--	.v...v^..\ 5
--	.v../2\\.. 6
--	<->-/vv|.. 7
--	.|<<<2-|.\ 8
--	.v//.|.v.. 9
--
--	Beams are only shown on empty tiles; arrows indicate the direction of the beams. If a tile contains
--	beams moving in multiple directions, the number of distinct directions is shown instead. Here is the
--	same diagram but instead only showing whether a tile is energized (#) or not (.):
--
--	######....
--	.#...#....
--	.#...#####
--	.#...##...
--	.#...##...
--	.#...##...
--	.#..####..
--	########..
--	.#######..
--	.#...#.#..
--
--	Ultimately, in this example, 46 tiles become energized.
--
--	The light isn't energizing enough tiles to produce lava; to debug the contraption, you need to start
--	by analyzing the current situation. With the beam starting in the top-left heading right, how many
--	tiles end up being energized?
--
-- Test:
--	% cd ../.. && echo -e '.|...\\....\n|.-.\\.....\n.....|-...\n........|.\n..........\n.........\\\n..../.\\\\..\n.-.-/..|..\n.|....-|.\\\n..//.|....' | cabal run 2023_day16-the-floor-will-be-lava # = 46

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: [String] -> Array Point Char
parse cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

cardinalDirections = [north, south, east, west]

exitDirs = [
  (('|',  east), [north, south]),
  (('|',  west), [north, south]),

  (('-',  north), [east, west]),
  (('-',  south), [east, west]),

  (('/',  north), [east]),
  (('/',  south), [west]),
  (('/',  east),  [north]),
  (('/',  west),  [south]),

  (('\\', north), [west]),
  (('\\', south), [east]),
  (('\\', east),  [south]),
  (('\\', west),  [north])
  ]

type Beam = (Point, Point)
type State = ([Beam], Set Beam)

zap :: PuzzleMap -> State -> State
zap _ s@([], es) = s
zap m (bs, es)   = zap m . foldl' zap' ([], es) $ bs
  where zap' (bs, es) b@(dir@(dy, dx), pt@(y, x)) = (bs' ++ bs, es `union` fromList bs')
          where pt'@(y', x') = (y + dy, x + dx)
                bs' = if inRange (bounds m) pt' then filter (`notMember` es) . L.map (,pt') . fromMaybe [dir] . lookup (m ! pt', dir) $ exitDirs else []


solve :: PuzzleMap -> Int
solve = size . S.map snd . snd . flip zap ([(east, (0, -1))], empty)


main :: IO ()
main = interact $ show . solve . parse . lines

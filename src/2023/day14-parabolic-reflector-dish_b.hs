module Main where
import Data.Ord (Down(..))
import Data.HashMap.Strict as M (HashMap, (!?), delete, filter, fromList, insert, keys, toList)
import Data.List as L (filter, sort, sortOn)
import Data.List.Extra (sumOn')
import Data.Maybe (isJust)

--	... Each cycle tilts the platform four times so that the rounded rocks roll north, then west, then
--	south, then east. After each tilt, the rounded rocks roll as far as they can before the platform tilts
--	in the next direction. After one cycle, the platform will have finished rolling the rounded rocks in
--	those four directions in that order.
--
--	After 1 cycle:
--	.....#....
--	....#...O#
--	...OO##...
--	.OO#......
--	.....OOO#.
--	.O#...O#.#
--	....O#....
--	......OOOO
--	#...O###..
--	#..OO#....
--
--	After 2 cycles:
--	.....#....
--	....#...O#
--	.....##...
--	..O#......
--	.....OOO#.
--	.O#...O#.#
--	....O#...O
--	.......OOO
--	#..OO###..
--	#.OOO#...O
--
--	After 3 cycles:
--	.....#....
--	....#...O#
--	.....##...
--	..O#......
--	.....OOO#.
--	.O#...O#.#
--	....O#...O
--	.......OOO
--	#...O###.O
--	#.OOO#...O
--
--	... This process should work if you leave it running long enough, but you're still worried about the north
--	support beams. To make sure they'll survive for a while, you need to calculate the total load on the
--	north support beams after 1000000000 cycles.
--
--	In the above example, after 1000000000 cycles, the total load on the north support beams is 64.
--
--	Run the spin cycle for 1000000000 cycles. Afterward, what is the total load on the north support beams?
--
-- Test:
--	% cd ../.. && echo -e 'O....#....\nO.OO#....#\n.....##...\nOO.#O....O\n.O.....O#.\nO.#..O.#.#\n..O..#O..O\n.......O..\n#....###..\n#OO..#....' | cabal run 2023_day14-parabolic-reflector-dish_b # = 64

type Point = (Int, Int) -- Y,X

parse :: [String] -> (Point, HashMap Point Char)
parse gs = ((ly, lx),) . fromList . L.filter (('.' /=) . snd) . zip [pt | y <- [ly,ly-1..1], x <- [lx,lx-1..1], let pt = (y, x)] . concat $ gs
  where ly = length gs
        lx = length . head $ gs

north =  ( 1,  0)
south =  (-1,  0)
east  =  ( 0, -1)
west  =  ( 0,  1)
cardinalDirections = [north, west, south, east]

getRollers :: HashMap Point Char -> [Point]
getRollers = keys . M.filter ('O' ==)

sortRollers :: Point -> [Point] -> [Point]
sortRollers d
  | north == d = sortOn Down
  | south == d = sort
  | east  == d = sortOn snd
  | west  == d = sortOn (Down . snd)

move :: Point -> HashMap Point Char -> Point -> HashMap Point Char
move (maxY, maxX) m d@(dy, dx) = foldl' move' m . sortRollers d . getRollers $ m
  where move' :: HashMap Point Char -> Point -> HashMap Point Char
        move' m pt@(y, x)
          | y' > maxY || y' < 1 || x' > maxX || x' < 1 = m
          | isBlocked = m
          | otherwise = move' (insert pt' 'O' (delete pt m)) pt'
          where isBlocked = isJust $ m !? pt'
                pt' = (y', x')
                y' = y + dy
                x' = x + dx

oneCycle :: Point -> Int -> HashMap Point Char -> [HashMap Point Char]
oneCycle maxs 0 m = []
oneCycle maxs n m = let m' = foldl' (move maxs) m cardinalDirections
                    in m':oneCycle maxs (n - 1) m'

score :: HashMap Point Char -> Int
score = sumOn' fst . getRollers

findCycle :: [HashMap Point Char] -> (Int, Int, [HashMap Point Char])
findCycle ms = (beglen, cyclen, cycms)
  where -- vv TODO I don't like the "hopefully" part, should find a better way to detect cycle beginning (a hashmap of maps seen, loop till match found?) vv
        fm     = ms !! 200 -- fast forward to "future" maps, hopefully we're in the middle of a cycle by now.
        (onramp, cycbeg) = break (== fm) ms
        beglen = length onramp
        cycms  = (:) fm . takeWhile (/= fm) . tail $ cycbeg
        cyclen = length cycms

maxCycles = 1000000000 :: Int

solve :: (Point, HashMap Point Char) -> Int
solve i@(maxs, m) = score . finish . findCycle . oneCycle maxs maxCycles $ m
  where finish :: (Int, Int, [HashMap Point Char]) -> HashMap Point Char
        finish (beglen, cyclen, cycms) =
          let (n, r) = (maxCycles - beglen) `divMod` cyclen
              ans | 0 == r    = last cycms
                  | otherwise = cycms !! (r - 1)
          in ans


main :: IO ()
main = interact $ show . solve . parse . lines

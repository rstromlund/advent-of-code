module Main where
import Data.Char (isDigit)

--	... one of the gears in the engine is wrong. A gear is any * symbol that is adjacent to exactly two part numbers. Its gear ratio is the result of multiplying those two numbers together.
--
--	This time, you need to find the gear ratio of every gear and add them all up so that the engineer can figure out which gear needs to be replaced.
--
--	Consider the same engine schematic again:
--
--	467..114..
--	...*......
--	..35..633.
--	......#...
--	617*......
--	.....+.58.
--	..592.....
--	......755.
--	...$.*....
--	.664.598..
--
--	In this schematic, there are two gears. The first is in the top left; it has part numbers 467 and 35,
--	so its gear ratio is 16345. The second gear is in the lower right; its gear ratio is 451490. (The *
--	adjacent to 617 is not a gear because it is only adjacent to one part number.) Adding up all of the
--	gear ratios produces 467835.
--
--	What is the sum of all of the gear ratios in your engine schematic?
--
-- Test:
--	% cd ../.. && echo -e '467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598..' | cabal run 2023_day03-gear-ratios_b ## 467835

getCh x y ls
  | x < 0 || y < 0 || y >= length ls || x >= length (head ls) = '.'
  | otherwise = (ls !! y) !! x

type Point = (Int, Int)
type Nbr   = (Int, [Point])
type Nbrs  = [Nbr]

collect :: Nbrs -> [String] -> Nbrs
collect ns = fst . foldl' collY (ns, 0)
  where collY :: (Nbrs, Int) -> String -> (Nbrs, Int)
        collY (ns, y) xs = (fst . foldl' collX (ns, (y, 0, ("", []), False)) $ (xs ++ "."), y + 1)
        collX :: (Nbrs, (Int, Int, (String, [Point]), Bool)) -> Char -> (Nbrs, (Int, Int, (String, [Point]), Bool))
        collX (ns, (y, x, (nbr, ps), isn)) c
          | isDigit c = (ns, (y, x + 1, (c:nbr, (x, y):ps), True))
          | isn && not (isDigit c) = ((read . reverse $ nbr, ps):ns, (y, x + 1, ("", []), False))
          | otherwise = (ns, (y, x + 1, (nbr, ps), isn))

collect' :: Nbrs -> [String] -> Nbrs
collect' ns = fst . foldl' collY (ns, 0)
  where collY :: (Nbrs, Int) -> String -> (Nbrs, Int)
        collY (ns, y) xs = (fst . foldl' collX (ns, (y, 0, ("", []), False)) $ (xs ++ "."), y + 1)
        collX :: (Nbrs, (Int, Int, (String, [Point]), Bool)) -> Char -> (Nbrs, (Int, Int, (String, [Point]), Bool))
        collX (ns, (y, x, (nbr, ps), isn)) c
          | isDigit c = (ns, (y, x + 1, (c:nbr, (x, y):ps), True))
          | isn && not (isDigit c) = ((read . reverse $ nbr, ps):ns, (y, x + 1, ("", []), False))
          | otherwise = (ns, (y, x + 1, (nbr, ps), isn))

directions :: [Point]
directions = [
  ( 1,  0),
  ( 1,  1),
  ( 1, -1),

  (-1,  0),
  (-1,  1),
  (-1, -1),

  ( 0,  1),
  ( 0, -1)
  ]

findGears :: [String] -> Nbrs -> Int
findGears ls ns = foldl' (\acc y -> foldl' (\acc x -> acc + isGear x y (getCh x y ls)) acc [0..(length . head $ ls) - 1]) 0 [0..length ls - 1]
  where isGear x y c
          | '*' /= c = 0
          | otherwise = let ns' = filter (\(_, ps) -> any (\(px, py) -> any (\(dx, dy) -> x == (px + dx) && y == (py + dy)) directions) ps) ns
                        in if 2 == length ns' then product . map fst $ ns' else 0

solve :: [String] -> Int
solve ls = findGears ls . collect [] $ ls


main :: IO ()
main = interact $ show . solve . lines

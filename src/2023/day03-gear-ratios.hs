module Main where
import Data.Char (isDigit)

--	... any number adjacent to a symbol, even diagonally, is a "part number" and should be included in your sum. (Periods (.) do not count as a symbol.)
--
--	Here is an example engine schematic:
--
--	467..114..
--	...*......
--	..35..633.
--	......#...
--	617*......
--	.....+.58.
--	..592.....
--	......755.
--	...$.*....
--	.664.598..
--
--	In this schematic, two numbers are not part numbers because they are not adjacent to a symbol: 114
--	(top right) and 58 (middle right). Every other number is adjacent to a symbol and so is a part number;
--	their sum is 4361.
--
--	Of course, the actual engine schematic is much larger. What is the sum of all of the part numbers in the engine schematic?
--
-- Test:
--	% cd ../.. && echo -e '467..114..\n...*......\n..35..633.\n......#...\n617*......\n.....+.58.\n..592.....\n......755.\n...$.*....\n.664.598..' | cabal run 2023_day03-gear-ratios ## 4361

getCh x y ls
  | x < 0 || y < 0 || y >= length ls || x >= length (head ls) = '.'
  | otherwise = (ls !! y) !! x

type Point = (Int, Int)
type Nbr   = (Int, [Point])
type Nbrs  = [Nbr]

collect :: Nbrs -> [String] -> Nbrs
collect ns = fst . foldl' collY (ns, 0)
  where collY :: (Nbrs, Int) -> String -> (Nbrs, Int)
        collY (ns, y) xs = (fst . foldl' collX (ns, (y, 0, ("", []), False)) $ (xs ++ "."), y + 1)
        collX :: (Nbrs, (Int, Int, (String, [Point]), Bool)) -> Char -> (Nbrs, (Int, Int, (String, [Point]), Bool))
        collX (ns, (y, x, (nbr, ps), isn)) c
          | isDigit c = (ns, (y, x + 1, (c:nbr, (x, y):ps), True))
          | isn && not (isDigit c) = ((read . reverse $ nbr, ps):ns, (y, x + 1, ("", []), False))
          | otherwise = (ns, (y, x + 1, (nbr, ps), isn))

directions :: [Point]
directions = [
  ( 1,  0),
  ( 1,  1),
  ( 1, -1),

  (-1,  0),
  (-1,  1),
  (-1, -1),

  ( 0,  1),
  ( 0, -1)
  ]

hasSym :: [String] -> Nbr -> Bool
hasSym ls (n, ps) = any hasSym' ps
  where hasSym' (x, y) = any (\(dx, dy) -> isSym . getCh (x + dx) (y + dy) $ ls) directions
        isSym c = not (isDigit c) && '.' /= c

solve :: [String] -> Int
solve ls = sum . map fst . filter (hasSym ls) . collect [] $ ls


main :: IO ()
main = interact $ show . solve . lines

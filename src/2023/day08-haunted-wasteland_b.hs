module Main where
import Data.List (isSuffixOf, lookup)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, some)
import Text.Megaparsec.Char (alphaNumChar, string)

--	... just start at every node that ends with A and follow all of the paths at the same time until they all simultaneously end up at nodes that end with Z.
--
--	For example:
--
--	LR
--
--	11A = (11B, XXX)
--	11B = (XXX, 11Z)
--	11Z = (11B, XXX)
--	22A = (22B, XXX)
--	22B = (22C, 22C)
--	22C = (22Z, 22Z)
--	22Z = (22B, 22B)
--	XXX = (XXX, XXX)
--
--	Here, there are two starting nodes, 11A and 22A (because they both end with A). As you follow each
--	left/right instruction, use that instruction to simultaneously navigate away from both nodes you're
--	currently on. Repeat this process until all of the nodes you're currently on end with Z. (If only some
--	of the nodes you're on end with Z, they act like any other node and you continue as normal.) In this
--	example, you would proceed as follows:
--
--	Step 0: You are at 11A and 22A.
--	Step 1: You choose all of the left paths, leading you to 11B and 22B.
--	Step 2: You choose all of the right paths, leading you to 11Z and 22C.
--	Step 3: You choose all of the left paths, leading you to 11B and 22Z.
--	Step 4: You choose all of the right paths, leading you to 11Z and 22B.
--	Step 5: You choose all of the left paths, leading you to 11B and 22C.
--	Step 6: You choose all of the right paths, leading you to 11Z and 22Z.
--
--	So, in this example, you end up entirely on nodes that end in Z after 6 steps.
--
--	Simultaneously start on every node that ends with A. How many steps does it take before you're only on nodes that end with Z?
--
-- Test:
--	% cd ../.. && echo -e 'LR\n\n11A = (11B, XXX)\n11B = (XXX, 11Z)\n11Z = (11B, XXX)\n22A = (22B, XXX)\n22B = (22C, 22C)\n22C = (22Z, 22Z)\n22Z = (22B, 22B)\nXXX = (XXX, XXX)' | cabal run 2023_day08-hauntedwasteland_b # = 6

type Document = (String, (String, String))
type Parser = Parsec Void String

document :: Parser Document
document = mkDoc <$> (word <* string " = (") <*> (word <* string ", ") <*> (word <* string ")") <* eof
  where word = some alphaNumChar
        mkDoc loc l r = (loc, (l, r))

-- e.g. ("RL",Right [("AAA",("BBB","CCC")),("BBB",("DDD","EEE")),("CCC",("ZZZ","GGG")),("DDD",("DDD","DDD")),("EEE",("EEE","EEE")),("GGG",("GGG","GGG")),("ZZZ",("ZZZ","ZZZ"))])
parse (l:_:ls) = (l, mapM (runParser document "<stdin>") ls)

walk :: [Document] -> Int -> String -> String -> [(Int, String)]
walk ds stps loc (rl:rls) = (stps, loc):walk ds (stps + 1) (move loc) rls
  where move src = dest rl (lookup src ds)
        dest 'L' (Just (l, _)) = l
        dest 'R' (Just (_, r)) = r

solve :: (String, Either a [Document]) -> Int
solve (rls, Right ds) = foldl1 lcm . map (\l -> findSeq . filter (("Z" `isSuffixOf`) . snd) . walk ds 0 l . cycle $ rls) $ locs
  where locs = map fst . filter (("A" `isSuffixOf`) . fst) $ ds
        findSeq :: [(Int, String)] -> Int
        findSeq ((stp0, l0):(stp1, l1):_) | l0 == l1 = stp1 - stp0


main :: IO ()
main = interact $ show . solve . parse . lines

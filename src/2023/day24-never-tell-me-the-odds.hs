module Main where
import Data.List (tails)
import Data.List.Split (splitOn)
import Data.Maybe (mapMaybe)
import Numeric.LinearAlgebra (Matrix, linearSolve, atIndex, (><))

--	... You make a note of each hailstone's position and velocity (your puzzle input). For example:
--
--	19, 13, 30 @ -2,  1, -2
--	18, 19, 22 @ -1, -1, -2
--	20, 25, 34 @ -2, -2, -4
--	12, 31, 28 @ -1, -2, -1
--	20, 19, 15 @  1, -5, -3
--
--	Each line of text corresponds to the position and velocity of a single hailstone. The positions
--	indicate where the hailstones are right now (at time 0). The velocities are constant and indicate
--	exactly how far each hailstone will move in one nanosecond.
--
--	Each line of text uses the format px py pz @ vx vy vz. For instance, the hailstone specified by 20,
--	19, 15 @ 1, -5, -3 has initial X position 20, Y position 19, Z position 15, X velocity 1, Y velocity
--	-5, and Z velocity -3. After one nanosecond, the hailstone would be at 21, 14, 12.
--
--	Perhaps you won't have to do anything. How likely are the hailstones to collide with each other and
--	smash into tiny ice crystals?
--
--	To estimate this, consider only the X and Y axes; ignore the Z axis. Looking forward in time, how many
--	of the hailstones' paths will intersect within a test area? (The hailstones themselves don't have to
--	collide, just test for intersections between the paths they will trace.)
--
--	In this example, look for intersections that happen with an X and Y position each at least 7 and at
--	most 27; in your actual data, you'll need to check a much larger test area. Comparing all pairs of
--	hailstones' future paths produces the following results:
--
--	Hailstone A: 19, 13, 30 @ -2, 1, -2
--	Hailstone B: 18, 19, 22 @ -1, -1, -2
--	Hailstones' paths will cross inside the test area (at x=14.333, y=15.333).
--
--	Hailstone A: 19, 13, 30 @ -2, 1, -2
--	Hailstone B: 20, 25, 34 @ -2, -2, -4
--	Hailstones' paths will cross inside the test area (at x=11.667, y=16.667).
--
--	Hailstone A: 19, 13, 30 @ -2, 1, -2
--	Hailstone B: 12, 31, 28 @ -1, -2, -1
--	Hailstones' paths will cross outside the test area (at x=6.2, y=19.4).
--
--	Hailstone A: 19, 13, 30 @ -2, 1, -2
--	Hailstone B: 20, 19, 15 @ 1, -5, -3
--	Hailstones' paths crossed in the past for hailstone A.
--
--	Hailstone A: 18, 19, 22 @ -1, -1, -2
--	Hailstone B: 20, 25, 34 @ -2, -2, -4
--	Hailstones' paths are parallel; they never intersect.
--
--	Hailstone A: 18, 19, 22 @ -1, -1, -2
--	Hailstone B: 12, 31, 28 @ -1, -2, -1
--	Hailstones' paths will cross outside the test area (at x=-6, y=-5).
--
--	Hailstone A: 18, 19, 22 @ -1, -1, -2
--	Hailstone B: 20, 19, 15 @ 1, -5, -3
--	Hailstones' paths crossed in the past for both hailstones.
--
--	Hailstone A: 20, 25, 34 @ -2, -2, -4
--	Hailstone B: 12, 31, 28 @ -1, -2, -1
--	Hailstones' paths will cross outside the test area (at x=-2, y=3).
--
--	Hailstone A: 20, 25, 34 @ -2, -2, -4
--	Hailstone B: 20, 19, 15 @ 1, -5, -3
--	Hailstones' paths crossed in the past for hailstone B.
--
--	Hailstone A: 12, 31, 28 @ -1, -2, -1
--	Hailstone B: 20, 19, 15 @ 1, -5, -3
--	Hailstones' paths crossed in the past for both hailstones.
--
--	So, in this example, 2 hailstones' future paths cross inside the boundaries of the test area.
--
--	However, you'll need to search a much larger test area if you want to see if any hailstones might
--	collide. Look for intersections that happen with an X and Y position each at least 200000000000000 and
--	at most 400000000000000. Disregard the Z axis entirely.
--
--	Considering only the X and Y axes, check all pairs of hailstones' future paths for intersections. How many of these intersections occur within the test area?
--
-- Test:
--	% cd ../.. && echo -e '19, 13, 30 @ -2,  1, -2\n18, 19, 22 @ -1, -1, -2\n20, 25, 34 @ -2, -2, -4\n12, 31, 28 @ -1, -2, -1\n20, 19, 15 @  1, -5, -3' | cabal run 2023_day24-never-tell-me-the-odds # = 2

type Point = (Int, Int, Int) -- X,Y,Z (or velocity/magnitude X,Y,Z)
type Stone = (Point, Point)
type Hail  = [Stone]

parse :: String -> Hail
parse = map (mkStone . splitOn "@") . lines
  where mkStone :: [String] -> Stone
        mkStone [p, v] = (mkPoint p, mkPoint v)
        mkPoint :: String -> Point
        mkPoint = (\[x,y,z] -> (x,y,z)) . map read . splitOn ","

{-
https://www.youtube.com/watch?v=N-qUfr-rz_Y
Intersection of Two Lines in 3D Space | Intersecting Lines

https://www.youtube.com/watch?v=bvlIYX9cgls
Find the Intersection of Two Line Segments in 2D (Easy Method)

https://demonstrations.wolfram.com/IntersectionOfTwoLinesUsingVectors/

https://math.stackexchange.com/questions/406864/intersection-of-two-lines-in-vector-form


E.g.
  Hailstone A: 19, 13, 30 @ -2, 1, -2
  Hailstone B: 18, 19, 22 @ -1, -1, -2
  Hailstones' paths will cross inside the test area (at x=14.333, y=15.333).

Line 1:                 Line 2:
|x|   |19|     |-2|     |x|   |18|     |-1|
|y| = |13| + a | 1| and |y| = |19| + b |-1|
|z|   |30|     |-2|     |z|   |22|     |-2|

-}

intersect :: Stone -> Stone -> Maybe (Double, Double)
intersect ((x1, y1, _), (dx1, dy1, _)) ((x2, y2, _), (dx2, dy2, _)) = do
  solvd <- linearSolve a b
  let t = solvd `atIndex` (0,0) -- time on line/stone 1
      u = solvd `atIndex` (1,0) -- time on line/stone 2
      x = fromIntegral x1 + fromIntegral dx1 * t -- intersection points
      y = fromIntegral y1 + fromIntegral dy1 * t
  if t >= 0 && u >= 0 then {-time is positive for both-} return (x, y) else Nothing
  where
    a :: Matrix Double
    a = (2><2) [- fromIntegral dx1, fromIntegral dx2,
                - fromIntegral dy1, fromIntegral dy2]
    b = (2><1) [fromIntegral $ x1 - x2, fromIntegral $ y1 - y2]

solve :: Hail -> Int
solve = length . concatMap (filter (\(x, y) -> testArea x && testArea y) . collide) . tails
  where collide (x:xs@(_:_)) = mapMaybe (intersect x) xs
        collide _ = []
        --
        -- testArea p = p >= 7 && p <= 27
        testArea p = p >= 200000000000000 && p <= 400000000000000


main :: IO ()
main = interact $ show . solve . parse

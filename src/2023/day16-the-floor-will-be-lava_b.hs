module Main where
import Data.Array ((!), Array, bounds, inRange, listArray)
import Data.List as L (lookup, map)
import Data.Maybe (fromMaybe)
import Data.Set as S (Set, empty, fromList, map, notMember, size, union)

--	As you try to work out what might be wrong, the reindeer tugs on your shirt and leads you to a nearby
--	control panel. There, a collection of buttons lets you align the contraption so that the beam enters
--	from any edge tile and heading away from that edge. (You can choose either of two directions for the
--	beam if it starts on a corner; for instance, if the beam starts in the bottom-right corner, it can
--	start heading either left or upward.)
--
--	So, the beam could start on any tile in the top row (heading downward), any tile in the bottom row
--	(heading upward), any tile in the leftmost column (heading right), or any tile in the rightmost column
--	(heading left). To produce lava, you need to find the configuration that energizes as many tiles as
--	possible.
--
--	... Find the initial beam configuration that energizes the largest number of tiles; how many tiles are
--	energized in that configuration?
--
-- Test:
--	% cd ../.. && echo -e '.|...\\....\n|.-.\\.....\n.....|-...\n........|.\n..........\n.........\\\n..../.\\\\..\n.-.-/..|..\n.|....-|.\\\n..//.|....' | cabal run 2023_day16-the-floor-will-be-lava_b # = 51

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: [String] -> Array Point Char
parse cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

cardinalDirections = [north, south, east, west]

exitDirs = [
  (('|',  east), [north, south]),
  (('|',  west), [north, south]),

  (('-',  north), [east, west]),
  (('-',  south), [east, west]),

  (('/',  north), [east]),
  (('/',  south), [west]),
  (('/',  east),  [north]),
  (('/',  west),  [south]),

  (('\\', north), [west]),
  (('\\', south), [east]),
  (('\\', east),  [south]),
  (('\\', west),  [north])
  ]

type Beam = (Point, Point)
type State = ([Beam], Set Beam)

zap :: PuzzleMap -> State -> State
zap _ s@([], es) = s
zap m (bs, es)   = zap m . foldl' zap' ([], es) $ bs
  where zap' (bs, es) b@(dir@(dy, dx), pt@(y, x)) = (bs' ++ bs, es `union` fromList bs')
          where pt'@(y', x') = (y + dy, x + dx)
                bs' = if inRange (bounds m) pt' then filter (`notMember` es) . L.map (,pt') . fromMaybe [dir] . lookup (m ! pt', dir) $ exitDirs else []


solve :: PuzzleMap -> Int
solve m = maximum . L.map (size . S.map snd . snd . zap m . (, empty) . (:[])) $ starts
  where ((minY, minX), (maxY, maxX)) = bounds m
        starts = [(dir, (y,x)) | y <- [minY..maxY], x <- [minX - 1, maxX + 1], dir <- [east,  west]] ++
                 [(dir, (y,x)) | y <- [minY - 1, maxY + 1], x <- [minX..maxX], dir <- [north, south]]

main :: IO ()
main = interact $ show . solve . parse . lines

module Main where
import Data.Bifunctor (bimap)
import Data.HashMap.Strict as M (HashMap, (!?), empty, foldMapWithKey, insert)
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe)

--	... If the operation character is an equals sign (=), it will be followed by a number indicating the
--	focal length of the lens that needs to go into the relevant box; be sure to use the label maker to
--	mark the lens with the label given in the beginning of the step so you can find it later. There are
--	two possible situations:
--
--	* If there is already a lens in the box with the same label, replace the old lens with the new lens:
--	  remove the old lens and put the new lens in its place, not moving any other lenses in the box.
--	* If there is not already a lens in the box with the same label, add the lens to the box immediately
--	  behind any lenses already in the box. Don't move any of the other lenses when you do this. If there
--	  aren't any lenses in the box, the new lens goes all the way to the front of the box.
--
--	Here is the contents of every box after each step in the example initialization sequence above:
--
--	After "rn=1":
--	Box 0: [rn 1]
--
--	After "cm-":
--	Box 0: [rn 1]
--
--	After "qp=3":
--	Box 0: [rn 1]
--	Box 1: [qp 3]
--
--	After "cm=2":
--	Box 0: [rn 1] [cm 2]
--	Box 1: [qp 3]
--
--	After "qp-":
--	Box 0: [rn 1] [cm 2]
--
--	After "pc=4":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [pc 4]
--
--	After "ot=9":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [pc 4] [ot 9]
--
--	After "ab=5":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [pc 4] [ot 9] [ab 5]
--
--	After "pc-":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [ot 9] [ab 5]
--
--	After "pc=6":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [ot 9] [ab 5] [pc 6]
--
--	After "ot=7":
--	Box 0: [rn 1] [cm 2]
--	Box 3: [ot 7] [ab 5] [pc 6]
--
--	All 256 boxes are always present; only the boxes that contain any lenses are shown here. Within each
--	box, lenses are listed from front to back; each lens is shown as its label and focal length in square
--	brackets.
--
--	To confirm that all of the lenses are installed correctly, add up the focusing power of all of the
--	lenses. The focusing power of a single lens is the result of multiplying together:
--
--	* One plus the box number of the lens in question.
--	* The slot number of the lens within the box: 1 for the first lens, 2 for the second lens, and so on.
--	* The focal length of the lens.
--
--	At the end of the above example, the focusing power of each lens is as follows:
--
--	* rn: 1 (box 0) * 1 (first slot) * 1 (focal length) = 1
--	* cm: 1 (box 0) * 2 (second slot) * 2 (focal length) = 4
--	* ot: 4 (box 3) * 1 (first slot) * 7 (focal length) = 28
--	* ab: 4 (box 3) * 2 (second slot) * 5 (focal length) = 40
--	* pc: 4 (box 3) * 3 (third slot) * 6 (focal length) = 72
--
--	So, the above example ends up with a total focusing power of 145.
--
--	With the help of an over-enthusiastic reindeer in a hard hat, follow the initialization sequence. What
--	is the focusing power of the resulting lens configuration?
--
-- Test:
--	% cd ../.. && echo -e 'rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7' | cabal run 2023_day15-lens-library_b # = 145

type Input = (String, String) -- (Label, Op/Operand)
type Box   = (String, Int) -- (Label, Focal length)
type Boxes = HashMap Int [Box]

parse :: String -> [Input] -- (Label, Op/Operand)
parse = map (break (`elem` "=-")) . splitOn "," . filter ('\n' /=)

asciiHash :: String -> Int
asciiHash = foldl' hash' 0
  where hash' acc c = let ord = fromEnum c
                      in ((acc + ord) * 17) `mod` 256

upsertList :: Box -> [Box] -> [Box]
upsertList b@(lens, n) = upsertList' []
  where upsertList' acc [] = reverse (b:acc)
        upsertList' acc (b'@(lens', n'):bs')
          | lens == lens' = reverse acc ++ b:bs'
          | otherwise     = b':upsertList' acc bs'

op :: Boxes -> Input -> Boxes
op bs i@(lens, ch:ns) = insert hlens (a' ch) bs
  where hlens  = asciiHash lens
        a      = fromMaybe [] $ bs !? hlens
        a' '=' = upsertList (lens, read ns) a
        a' '-' = filter ((lens /=) . fst) a

solve :: [Input] -> Int
solve = sum . score . foldl' op empty
  where score = foldMapWithKey (\bn -> zipWith (\sn (_, fl) -> (bn + 1) * sn * fl) [1..])


main :: IO ()
main = interact $ show . solve . parse

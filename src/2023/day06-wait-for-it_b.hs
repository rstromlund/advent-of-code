module Main where

--	... There's really only one race - ignore the spaces between the numbers on each line.
--
--	So, the example from before:
--
--	Time:      7  15   30
--	Distance:  9  40  200
--	...now instead means this:
--
--	Time:      71530
--	Distance:  940200
--
--	Now, you have to figure out how many ways there are to win this single race. In this example, the race
--	lasts for 71530 milliseconds and the record distance you need to beat is 940200 millimeters. You could
--	hold the button anywhere from 14 to 71516 milliseconds and beat the record, a total of 71503 ways!
--
--	How many ways can you beat the record in this one much longer race?
--
-- Test:
--	% cd ../.. && echo -e 'Time:      7  15   30\nDistance:  9  40  200' | cabal run 2023_day06-wait-for-it # 71503

parse :: String -> (Int, Int)
parse = toPairs . map (read . concat . tail . words) . lines
  where toPairs [as, bs] = (as, bs)

{-
time = 7 and record = 9; hold winners: 2, 3, 4, or 5
distance = hold * (time - hold)
distance = (hold * time) - (hold ^ 2)

(hold * time) - (hold ^ 2) - 9 > 0


time = 71530 and record = 940200; hold winners: 2, 3, 4, or 5
distance = hold * (time - hold)
distance = (hold * time) - (hold ^ 2)
distance = - (hold ^ 2) + (time * hold)

x = hold, time = 7:
(-1) x^2 + 7x > 9
(-1) x^2 + 7x - 9 > 0

ax^2 + bx + c = 0
x = [-b ± √(b^2 – 4ac)]/2a

[(-7) ± √(7^2 – 4(-1)(-9))]/2(-1)
[(-7) ± √(49 – 36)]/(-2)
[(-7) ± √7]/(-2)
[(-7) ± (2.645751311064591)]/(-2)

Time	71530
Record	940200


(	-71530	+/-	71503.7069	)	/-2
	13.14655154
	71516.85345

(-1) x^2 + 71530x - 940200 > 0
	 (0.00)
	 (0.00)

Winning holds: (71516 - 14 + 1) = 71503

-}

-- Brute force.  We're probably supposed to find the root tho, right?  See above ... but this is quick too:
solve :: (Int, Int) -> Int
solve (tm, dist) = length . filter (> dist) . map (\hld -> (hld * tm) - (hld * hld)) $  [1..tm]


main :: IO ()
main = interact $ show . solve . parse

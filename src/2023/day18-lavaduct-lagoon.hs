module Main where
import Data.Char (isHexDigit)

--	... look at the dig plan (your puzzle input). For example:
--
--	R 6 (#70c710)
--	D 5 (#0dc571)
--	L 2 (#5713f0)
--	D 2 (#d2c081)
--	R 2 (#59c680)
--	D 2 (#411b91)
--	L 5 (#8ceee2)
--	U 2 (#caa173)
--	L 1 (#1b58a2)
--	U 2 (#caa171)
--	R 2 (#7807d2)
--	U 3 (#a77fa3)
--	L 2 (#015232)
--	U 2 (#7a21e3)
--
--	The digger starts in a 1 meter cube hole in the ground. They then dig the specified number of meters
--	up (U), down (D), left (L), or right (R), clearing full 1 meter cubes as they go. The directions are
--	given as seen from above, so if "up" were north, then "right" would be east, and so on. Each trench is
--	also listed with the color that the edge of the trench should be painted as an RGB hexadecimal color
--	code.
--
--	When viewed from above, the above example dig plan would result in the following loop of trench (#)
--	having been dug out from otherwise ground-level terrain (.):
--
--	#######
--	#.....#
--	###...#
--	..#...#
--	..#...#
--	###.###
--	#...#..
--	##..###
--	.#....#
--	.######
--
--	At this point, the trench could contain 38 cubic meters of lava. However, this is just the edge of the
--	lagoon; the next step is to dig out the interior so that it is one meter deep as well:
--
--	#######
--	#######
--	#######
--	..#####
--	..#####
--	#######
--	#####..
--	#######
--	.######
--	.######
--
--	Now, the lagoon can contain a much more respectable 62 cubic meters of lava. While the interior is dug
--	out, the edges are also painted according to the color codes in the dig plan.
--
--	The Elves are concerned the lagoon won't be large enough; if they follow their dig plan, how many
--	cubic meters of lava could it hold?
--
-- Test:
--	% cd ../.. && echo -e 'R 6 (#70c710)\nD 5 (#0dc571)\nL 2 (#5713f0)\nD 2 (#d2c081)\nR 2 (#59c680)\nD 2 (#411b91)\nL 5 (#8ceee2)\nU 2 (#caa173)\nL 1 (#1b58a2)\nU 2 (#caa171)\nR 2 (#7807d2)\nU 3 (#a77fa3)\nL 2 (#015232)\nU 2 (#7a21e3)' | cabal run 2023_day18-lavaduct-lagoon # = 62

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type Input = (Char, Int, String)

parse :: String -> [Input]
parse = map ((\[ds, ns, rgbs] -> (head ds, read ns, filter isHexDigit rgbs)::Input) . words) . lines

north =  (-1,  0)
south =  ( 1,  0)
east  =  ( 0,  1)
west  =  ( 0, -1)

-- https://en.wikipedia.org/wiki/Shoelace_formula#Trapezoid_formula
shoelaceArea :: [Point] -> Int
shoelaceArea ps@((y0, x0):_) = (`div` 2) . abs . shoelaceArea' $ ps
  where shoelaceArea' [(yn, xn)]                 = (yn + y0) * (xn - x0)
        shoelaceArea' ((y1, x1):ps@((y2, x2):_)) = (y1 + y2) * (x1 - x2) + shoelaceArea' ps

move :: Point -> [Input] -> [(Point, String)]
move (y, x) ((dir, n, rgb):is) = ps ++ move (fst . last $ ps) is
  where (dy, dx) | 'U' == dir = north
                 | 'R' == dir = east
                 | 'D' == dir = south
                 | 'L' == dir = west
        ps = map (\n -> ((y + dy * n, x + dx * n), rgb)) [1..n]
move pos [] = []

solve :: [Input] -> Int
solve = score . map fst . move (0, 0)
  where score pts = shoelaceArea pts + (length pts `div` 2 + 1)


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.Array.Unboxed ((!), Array, assocs, bounds, inRange, listArray)
import Data.Bifunctor (bimap)
import Data.List (nub)

--	... map (your puzzle input) of his starting position (S), garden plots (.), and rocks (#). For example:
--
--	...........
--	.....###.#.
--	.###.##..#.
--	..#.#...#..
--	....#.#....
--	.##..S####.
--	.##..#...#.
--	.......##..
--	.##.#.####.
--	.##..##.##.
--	...........
--
--	The Elf starts at the starting position (S) which also counts as a garden plot. Then, he can take one
--	step north, south, east, or west, but only onto tiles that are garden plots. This would allow him to
--	reach any of the tiles marked O:
--
--	...........
--	.....###.#.
--	.###.##..#.
--	..#.#...#..
--	....#O#....
--	.##.OS####.
--	.##..#...#.
--	.......##..
--	.##.#.####.
--	.##..##.##.
--	...........
--
--	Then, he takes a second step. Since at this point he could be at either tile marked O, his second step
--	would allow him to reach any garden plot that is one step north, south, east, or west of any tile that
--	he could have reached after the first step:
--
--	...........
--	.....###.#.
--	.###.##..#.
--	..#.#O..#..
--	....#.#....
--	.##O.O####.
--	.##.O#...#.
--	.......##..
--	.##.#.####.
--	.##..##.##.
--	...........
--
--	After two steps, he could be at any of the tiles marked O above, including the starting position
--	(either by going north-then-south or by going west-then-east).
--
--	A single third step leads to even more possibilities:
--
--	...........
--	.....###.#.
--	.###.##..#.
--	..#.#.O.#..
--	...O#O#....
--	.##.OS####.
--	.##O.#...#.
--	....O..##..
--	.##.#.####.
--	.##..##.##.
--	...........
--
--	He will continue like this until his steps for the day have been exhausted. After a total of 6 steps,
--	he could reach any of the garden plots marked O:
--
--	...........
--	.....###.#.
--	.###.##.O#.
--	.O#O#O.O#..
--	O.O.#.#.O..
--	.##O.O####.
--	.##.O#O..#.
--	.O.O.O.##..
--	.##.#.####.
--	.##O.##.##.
--	...........
--
--	In this example, if the Elf's goal was to get exactly 6 more steps today, he could use them to reach any of 16 garden plots.
--
--	However, the Elf actually needs to get 64 steps today, and the map he's handed you is much larger than the example map.
--
--	Starting from the garden plot marked S on your map, how many garden plots could the Elf reach in exactly 64 steps?
--
-- Test:
--	% cd ../.. && echo -e '...........\n.....###.#.\n.###.##..#.\n..#.#...#..\n....#.#....\n.##..S####.\n.##..#...#.\n.......##..\n.##.#.####.\n.##..##.##.\n...........' | cabal run 2023_day21-step-counter # = 16

-- stepCnt = 6 -- Test value
stepCnt = 64

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) . concat $ ls
  where ls@(l0:_) = lines cs
        ly = pred . length $ ls
        lx = pred . length $ l0

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

cardinalDirections = [north, south, east, west]

type State = [Point]

moves :: PuzzleMap -> Int -> State -> State
moves _ 0 ss = ss
moves pm cnt ss = moves pm (cnt - 1) . nub . concatMap getNeighbors $ ss
  where bnds = bounds pm
        getNeighbors :: Point -> State
        getNeighbors st@(y, x) = filter (\pt -> inRange bnds pt && garden pt) . map (bimap (+ y) (+ x)) $ cardinalDirections
        garden pt = pm ! pt `elem` ".S"

solve :: PuzzleMap -> Int
solve pm = length . moves pm stepCnt $ [start]
  where start = fst . head . filter (('S' ==) . snd) . assocs $ pm


main :: IO ()
main = interact $ show . solve . parse

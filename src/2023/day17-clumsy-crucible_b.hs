module Main where
import Algorithm.Search (dijkstra)
import Data.Array ((!), Array, bounds, inRange, listArray)
import Data.Char (digitToInt)

--	The crucibles of lava simply aren't large enough to provide an adequate supply of lava to the machine
--	parts factory. Instead, the Elves are going to upgrade to ultra crucibles.
--
--	Ultra crucibles are even more difficult to steer than normal crucibles. Not only do they have trouble
--	going in a straight line, but they also have trouble turning!
--
--	Once an ultra crucible starts moving in a direction, it needs to move a minimum of four blocks in that
--	direction before it can turn (or even before it can stop at the end). However, it will eventually
--	start to get wobbly: an ultra crucible can move a maximum of ten consecutive blocks without turning.
--
--	In the above example, an ultra crucible could follow this path to minimize heat loss:
--
--	2>>>>>>>>1323
--	32154535v5623
--	32552456v4254
--	34465858v5452
--	45466578v>>>>
--	143859879845v
--	445787698776v
--	363787797965v
--	465496798688v
--	456467998645v
--	122468686556v
--	254654888773v
--	432267465553v
--
--	In the above example, an ultra crucible would incur the minimum possible heat loss of 94.
--
--	Here's another example:
--
--	111111111111
--	999999999991
--	999999999991
--	999999999991
--	999999999991
--
--	Sadly, an ultra crucible would need to take an unfortunate path like this one:
--
--	1>>>>>>>1111
--	9999999v9991
--	9999999v9991
--	9999999v9991
--	9999999v>>>>
--
--	This route causes the ultra crucible to incur the minimum possible heat loss of 71.
--
--	Directing the ultra crucible from the lava pool to the machine parts factory, what is the least heat loss it can incur?
--
-- Test:
--	% cd ../.. && echo -e '2413432311323\n3215453535623\n3255245654254\n3446585845452\n4546657867536\n1438598798454\n4457876987766\n3637877979653\n4654967986887\n4564679986453\n1224686865563\n2546548887735\n4322674655533' | cabal run 2023_day17-clumsy-crucible_b # = 94
--	% cd ../.. && echo -e '111111111111\n999999999991\n999999999991\n999999999991\n999999999991' | cabal run 2023_day17-clumsy-crucible_b # = 71

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Int

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) . map digitToInt . concat $ ls
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls

north =  (-1,  0) :: Point
south =  ( 1,  0) :: Point
east  =  ( 0,  1) :: Point
west  =  ( 0, -1) :: Point

-- | Rotate coordinate 90-degrees CCW about the origin
turnLeft :: Point -> Point
turnLeft  (y, x) = (-x, y)

-- | Rotate coordinate 90-degrees CW about the origin
turnRight :: Point -> Point
turnRight (y, x) = (x, -y)

type State = (Point, (Point, Int)) -- location, direction, straight traveled

solve :: PuzzleMap -> Int
solve m = cost
  where Just (cost, path') = dijkstra
          getNeighbors -- next
          (\_ (np, _) -> m ! np) -- cost
          (\(np, (_, t)) -> t >= 4 && end_p2 == np) -- found
          (beg_p1, (east, 0)) -- initial
        --
        (beg_p1@(minY, minX), end_p2@(maxY, maxX)) = bounds m
        --
        getNeighbors :: State -> [State]
        getNeighbors st@((y, x), (d@(dy, dx), t)) =
          let r                = turnRight d
              l                = turnLeft d
              str8 | t >= 10   = []
                   | otherwise = [((y + dy, x + dx), (d, t + 1))]
              stRL | t < 4     = str8
                   | otherwise = foldl' (\acc d@(dy, dx) -> ((y + dy, x + dx), (d, 1)):acc) str8 [r, l]
          in filter (inRange (bounds m) . fst) stRL


main :: IO ()
main = interact $ show . solve . parse

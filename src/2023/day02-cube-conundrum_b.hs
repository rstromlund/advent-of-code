module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, sepBy, some)
import Text.Megaparsec.Char (letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... in each game you played, what is the fewest number of cubes of each color that could have been in the bag to make the game possible?
--
--	Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
--	Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
--	Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
--	Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
--	Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
--
--	In game 1, the game could have been played with as few as 4 red, 2 green, and 6 blue cubes. If any color had even one fewer cube, the game would have been impossible.
--	Game 2 could have been played with a minimum of 1 red, 3 green, and 4 blue cubes.
--	Game 3 must have been played with at least 20 red, 13 green, and 6 blue cubes.
--	Game 4 required at least 14 red, 3 green, and 15 blue cubes.
--	Game 5 needed no fewer than 6 red, 3 green, and 2 blue cubes in the bag.
--
--	The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied
--	together. The power of the minimum set of cubes in game 1 is 48. In games 2-5 it was 12, 1560, 630,
--	and 36, respectively. Adding up these five powers produces the sum 2286.
--
--	For each game, find the minimum set of cubes that must have been present. What is the sum of the power of these sets?
--
--	In game 1, three sets of cubes are revealed from the bag (and then put back again). The first set is 3
--
-- Test:
--	% cd ../.. && echo -e 'Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green\nGame 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue\nGame 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red\nGame 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red\nGame 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green' | cabal run 2023_day02-cube-conundrum_b # = 2286

type Cubes = [Int]
data Game  = Game Int [Cubes] deriving (Show, Eq, Ord)

type Parser = Parsec Void String

games :: Parser [Game]
games = some (game <* newline) <* eof

game :: Parser Game
game = string "Game " *> (Game <$> decimal <* string ": " <*> (cubes `sepBy` string "; "))

cubes :: Parser Cubes
cubes = foldl' toCube [0, 0, 0] <$> (cube `sepBy` string ", ")
  where toCube :: Cubes -> (Int, String) -> Cubes
        toCube [r, g, b] (n, "red")   = [r + n, g, b]
        toCube [r, g, b] (n, "green") = [r, g + n, b]
        toCube [r, g, b] (n, "blue")  = [r, g, b + n]

cube :: Parser (Int, String)
cube = (,) <$> (decimal <* space) <*> some letterChar


solve :: Either a [Game] -> Int
solve (Right gs) = sum . map (product . maxCubes) $ gs
  where maxCubes (Game id cs) = foldl1' (zipWith max) cs


main :: IO ()
main = interact $ show . solve . runParser games "<stdin>"

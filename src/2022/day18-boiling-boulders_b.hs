module Main where
import Data.Bool (bool)
import Data.List as L (map)
import Data.Set  as S (Set, deleteFindMin, empty, foldl', fromList, insert, member, notMember, null, singleton)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	Something seems off about your calculation. The cooling rate depends on exterior surface area, but
--	your calculation also included the surface area of air pockets trapped in the lava droplet.
--
--	Instead, consider only cube sides that could be reached by the water and steam as the lava droplet
--	tumbles into the pond. The steam will expand to reach as much as possible, completely displacing any
--	air on the outside of the lava droplet but never expanding diagonally.
--
--	In the larger example above, exactly one cube of air is trapped within the lava droplet (at 2,2,5), so
--	the exterior surface area of the lava droplet is 58.
--
--	What is the exterior surface area of your scanned lava droplet?
--
-- Test:
--	% cd ../.. && echo -e '2,2,2\n1,2,2\n3,2,2\n2,1,2\n2,3,2\n2,2,1\n2,2,3\n2,2,4\n2,2,6\n1,2,5\n3,2,5\n2,1,5\n2,3,5' | cabal run 2022_day18-boiling-boulders_b # = 58

type Parser = Parsec Void String
type Point = (Int, Int, Int)

points :: Parser [Point]
points = many point <* optional newline <* eof

point :: Parser Point
point = mkTuple <$> (decimal `sepBy` char ',') <* newline
  where mkTuple [x,y,z] = (x, y, z)

--

ckSides =
  [ (-1,  0,  0)
  , ( 0, -1,  0)
  , ( 0,  0, -1)
  , ( 1,  0,  0)
  , ( 0,  1,  0)
  , ( 0,  0,  1)
  ]

solve :: Either a [Point] -> Int
solve (Right ds) = S.foldl' surfaceArea 0 ss - airPockets
  where ss = fromList ds
        (maxX, maxY, maxZ) = L.foldl' (\(mx, my, mz) (x, y, z) -> (max mx x, max my y, max mz z)) (0, 0, 0) ds
        surfaceArea acc d@(x, y, z) = (+) acc . sum . L.map (\(dx, dy, dz) -> bool 1 0 $ (x + dx, y + dy, z + dz) `member` ss) $ ckSides
        --
        -- Each air pocket/space will be traversed multiple times by dijkstra; an optimization would be to
        -- consider all the vps (visited points) from dijkstra as the same pocket.  But this is very simple
        -- and fast enough.  No premature optimizations applied ;p
        airPockets = sum
          [ bool 0 (6 - surfaceArea 0 pt) $ dijkstra empty (singleton (False, pt))
          | x <- [0..maxX]
          , y <- [0..maxY]
          , z <- [0..maxZ]
          , let pt = (x, y, z)
          , pt `notMember` ss
          ]
        --
        dijkstra :: Set Point -> Set (Bool, Point) -> Bool
        dijkstra vps costs
          | S.null costs = True -- no next steps?  must be an air pocket
          | otherwise    = nextStep nxtCell costs'
          where (nxtCell, costs') = deleteFindMin costs
                nextStep (r, pt@(x, y, z)) costs
                  | x < 0 || x > maxX ||
                    y < 0 || y > maxY ||
                    z < 0 || z > maxZ = r -- are we at the edge? free? not an air pocket
                  | pt `member` vps   = dijkstra vps costs
                  | otherwise         = dijkstra (insert pt vps) neighbors
                  where neighbors =
                          L.foldl' (flip insert) costs' [ (False, pt')
                                                        | ck@(dx, dy, dz) <- ckSides
                                                        , let pt'@(x', y', z') = (x + dx, y + dy, z + dz)
                                                        , x' >= -1 && x' <= maxX + 1
                                                        , y' >= -1 && y' <= maxY + 1
                                                        , z' >= -1 && z' <= maxZ + 1
                                                        , pt' `notMember` vps
                                                        , pt' `notMember` ss
                                                        ]


main :: IO ()
main = interact $ show . solve . runParser points "<stdin>"

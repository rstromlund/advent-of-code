module Main where
import Data.Bool (bool)
import Data.List (lookup)
import Data.Set (Set, fromList, insert, size)

--	... Rather than two knots, you now must simulate a rope consisting of ten knots. One knot is still the
--	head of the rope and moves according to the series of motions. Each knot further down the rope follows
--	the knot in front of it using the same rules as before.
--
-- Test:
--	% cd ../.. && echo -e 'R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2' | cabal run 2022_day09-rope-bridge_b # = 1
--	% cd ../.. && echo -e 'R 5\nU 8\nL 8\nD 3\nR 17\nD 10\nL 25\nU 20' | cabal run 2022_day09-rope-bridge_b # = 36

type TailPos = Set (Int, Int)

movements =
  [
    ('R', ( 1,  0)),
    ('L', (-1,  0)),
    ('U', ( 0, -1)),
    ('D', ( 0,  1))
  ]

solve :: [[String]] -> Int
solve = size . fst . foldl' move (fromList [(0,0)], replicate 10 (0,0))
  where move :: (TailPos, [(Int,Int)]) -> [String] -> (TailPos, [(Int,Int)])
        move acc [dir, mag] = step acc (head dir, read mag)
        touching (fx, fy) (sx, sy) =
          let dx  = abs $ fx - sx
              dy  = abs $ fy - sy
          in abs dx <= 1 && abs dy <= 1
        newTail (fx, fy) (sx, sy) =
          let dx  = signum $ fx - sx
              dy  = signum $ fy - sy
          in (sx + dx, sy + dy)
        --
        step :: (TailPos, [(Int,Int)]) -> (Char, Int) -> (TailPos, [(Int,Int)])
        step acc (_, 0) = acc
        step acc (c, mag) =
          let Just (dx, dy) = lookup c movements
              (ts', ks') = inch acc (dx, dy) []
          in step (ts', ks') (c, mag - 1)
        --
        inch (ts, [kl]) _ ks' = (insert kl ts, reverse (kl:ks'))
        inch (ts, k0@(k0x, k0y):k1:ks) (dx, dy) ks' =
          let k0' = bool k0 (k0x + dx, k0y + dy) $ null ks' -- first time (at head) move head, all other inching motion comes from newTail
              k1' = bool (newTail k0' k1) k1 $ touching k0' k1
          in inch (ts, k1':ks) (dx, dy) (k0':ks')


main :: IO ()
main = interact $ show . solve . map words . lines

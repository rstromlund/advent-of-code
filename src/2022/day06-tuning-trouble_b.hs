module Main where
import Data.List (nub)

--	... it also needs to look for messages.
--
--	A start-of-message marker is just like a start-of-packet marker, except it consists of 14 distinct characters rather than 4.
--
--	Here are the first positions of start-of-message markers for all of the above examples:
--
--	* mjqjpqmgbljsphdztnvjfqwrcgsmlb: first marker after character 19
--	* bvwbjplbgvbhsrlpgdmjqwftvncz: first marker after character 23
--	* nppdvjthqldpwncqszvftbrmjlhg: first marker after character 23
--	* nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg: first marker after character 29
--	* zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw: first marker after character 26
--
--	How many characters need to be processed before the first start-of-message marker is detected?
--
-- Test:
--	% cd ../.. && echo -e 'mjqjpqmgbljsphdztnvjfqwrcgsmlb' | cabal run 2022_day06-tuning-trouble_b # = 19
--	% cd ../.. && echo -e 'bvwbjplbgvbhsrlpgdmjqwftvncz' | cabal run 2022_day06-tuning-trouble_b # = 23
--	% cd ../.. && echo -e 'nppdvjthqldpwncqszvftbrmjlhg' | cabal run 2022_day06-tuning-trouble_b # = 23
--	% cd ../.. && echo -e 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg' | cabal run 2022_day06-tuning-trouble_b # = 29
--	% cd ../.. && echo -e 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw' | cabal run 2022_day06-tuning-trouble_b # = 26

solve :: [Char] -> Int
solve cs = findUniques False 0 cs
  where l = 14
        findUniques True  cnt cs = cnt + l - 1
        findUniques False cnt cs = findUniques (l == (length . nub $ take l cs)) (cnt + 1) $ tail cs


main :: IO ()
main = interact $ show . solve

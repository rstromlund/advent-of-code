module Main where
import Data.Either (fromRight)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... the Elves would like to know the number of pairs that overlap at all.
--
--	In the above example, the first two pairs (2-4,6-8 and 2-3,4-5) don't overlap,
--	while the remaining four pairs (5-7,7-9, 2-8,3-7, 6-6,4-6, and 2-6,4-8) do
--	overlap:
--
--	* 5-7,7-9 overlaps in a single section, 7.
--	* 2-8,3-7 overlaps all of the sections 3 through 7.
--	* 6-6,4-6 overlaps in a single section, 6.
--	* 2-6,4-8 overlaps in sections 4, 5, and 6.
--
--	So, in this example, the number of overlapping assignment pairs is 4.
--
--	In how many assignment pairs do the ranges overlap?
--
-- Test:
--	% cd ../.. && echo -e '2-4,6-8\n2-3,4-5\n5-7,7-9\n2-8,3-7\n6-6,4-6\n2-6,4-8' | cabal run 2022_day04-camp-cleanup_b # = 4

type Parser = Parsec Void String

duties :: Parser [((Int, Int), (Int, Int))]
duties = many assignments <* optional newline <* eof

assignments :: Parser ((Int, Int), (Int, Int))
assignments = (,) <$> assignment <* char ',' <*> assignment <* newline

assignment :: Parser (Int, Int)
assignment = (,) <$> decimal <* char '-' <*> decimal

solve :: [((Int,Int), (Int,Int))] -> Int
solve = sum . map overlaps
  where overlaps ((v1l, v1u), (v2l, v2u))
          | v1l <= v2u && v1u >= v2l = 1
          | v2l <= v1u && v2u >= v1l = 1
          | otherwise = 0


main :: IO ()
main = interact $ show . solve . fromRight [] . runParser duties "<stdin>"

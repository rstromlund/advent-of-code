module Main where
import Data.Char (isAsciiLower, isAsciiUpper, ord)
import Data.List (intersect)
import Data.List.Split (chunksOf)

--	... The only way to tell which item type is the right one is by finding the one
--	item type that is common between all three Elves in each group.
--
--	Every set of three lines in your list corresponds to a single group, but each
--	group can have a different badge item type. So, in the above example, the first
--	group's rucksacks are the first three lines:
--
--	vJrwpWtwJgWrhcsFMMfFFhFp
--	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
--	PmmdzqPrVvPwwTWBwg
--
--	And the second group's rucksacks are the next three lines:
--
--	wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
--	ttgJtRGJQctTZtZT
--	CrZsJsPPZsGzwwsLwLmpwMDw
--
--	In the first group, the only item type that appears in all three rucksacks is
--	lowercase r; this must be their badges. In the second group, their badge item type
--	must be Z.
--
--	Priorities for these items must still be found to organize the sticker attachment
--	efforts: here, they are 18 (r) for the first group and 52 (Z) for the second
--	group. The sum of these is 70.
--
--	Find the item type that corresponds to the badges of each three-Elf group. What is
--	the sum of the priorities of those item types?
--
-- Test:
--	% cd ../.. && echo -e 'vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw' | cabal run 2022_day03-rucksack-reorganization_b # = 70

solve :: [String] -> Int
solve = sum . map (priority . commonItem) . chunksOf 3
  where commonItem = head . foldl1' intersect
        priority ch
          | isAsciiLower ch = ord ch - ord 'a' + 1
          | isAsciiUpper ch = ord ch - ord 'A' + 27


main :: IO ()
main = interact $ show . solve . lines

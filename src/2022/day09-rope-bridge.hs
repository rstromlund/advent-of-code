module Main where
import Data.Bool (bool)
import Data.List (lookup)
import Data.Set (Set, fromList, insert, size)

--	... the head (H) and tail (T) must always be touching (diagonally adjacent and even overlapping both
--	count as touching)
--
--	... If the head is ever two steps directly up, down, left, or right from the tail, the tail must also
--	move one step in that direction so it remains close enough:
--
--	... Otherwise, if the head and tail aren't touching and aren't in the same row or column, the tail
--	always moves one step diagonally to keep up:
--
--	... After simulating the rope, you can count up all of the positions the tail visited at least
--	once. In this diagram, s again marks the starting position (which the tail also visited) and # marks
--	other positions the tail visited:
--
--	..##..
--	...##.
--	.####.
--	....#.
--	s###..
--
--	So, there are 13 positions the tail visited at least once.
--
--	Simulate your complete hypothetical series of motions. How many positions does the tail of the rope visit at least once?
--
-- Test:
--	% cd ../.. && echo -e 'R 4\nU 4\nL 3\nD 1\nR 4\nD 1\nL 5\nR 2' | cabal run 2022_day09-rope-bridge # = 13

type TailPos = Set (Int, Int)

movements =
  [
    ('R', ( 1,  0)),
    ('L', (-1,  0)),
    ('U', ( 0, -1)),
    ('D', ( 0,  1))
  ]

solve :: [[String]] -> Int
solve = size . fst . foldl' move (fromList [(0,0)], ((0,0), (0,0)))
  where move :: (TailPos, ((Int,Int), (Int,Int))) -> [String] -> (TailPos, ((Int,Int), (Int,Int)))
        move acc [dir, mag] = step acc (head dir, read mag)
        touching (hx, hy) (tx, ty) =
          let dx  = abs $ hx - tx
              dy  = abs $ hy - ty
          in abs dx <= 1 && abs dy <= 1
        newTail (hx, hy) (tx, ty) =
          let dx  = signum $ hx - tx
              dy  = signum $ hy - ty
          in (tx + dx, ty + dy)
        --
        step :: (TailPos, ((Int,Int), (Int,Int))) -> (Char, Int) -> (TailPos, ((Int,Int), (Int,Int)))
        step acc (_, 0) = acc
        step (ts, ((hx, hy), t@(tx, ty))) (c, mag) =
          let Just (dx, dy) = lookup c movements
              h'  = (hx + dx, hy + dy)
              t'  = bool (newTail h' t) t $ touching h' t
          in step (insert t' ts, (h', t')) (c, mag - 1)


main :: IO ()
main = interact $ show . solve . map words . lines

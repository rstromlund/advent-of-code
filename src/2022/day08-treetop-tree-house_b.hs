module Main where
import Data.Bifunctor (second)
import Data.List (transpose)

--	... To measure the viewing distance from a given tree, look up, down, left, and right from that tree;
--	stop if you reach an edge or at the first tree that is the same height or taller than the tree under
--	consideration. (If a tree is right on the edge, at least one of its viewing distances will be zero.)
--
--	The Elves don't care about distant trees taller than those found by the rules above; the proposed tree
--	house has large eaves to keep it dry, so they wouldn't be able to see higher than the tree house
--	anyway.
--
--	In the example above, consider the middle 5 in the second row:
--
--	30373
--	25512 <-- second 5; middle number
--	65332
--	33549
--	35390
--
--	* Looking up, its view is not blocked; it can see 1 tree (of height 3).
--	* Looking left, its view is blocked immediately; it can see only 1 tree (of height 5, right next to it).
--	* Looking right, its view is not blocked; it can see 2 trees.
--	* Looking down, its view is blocked eventually; it can see 2 trees (one of height 3, then the tree of height 5 that blocks its view).
--
--	A tree's scenic score is found by multiplying together its viewing distance in each of the four directions. For this tree, this is 4 (found by multiplying 1 * 1 * 2 * 2).
--
--	However, you can do even better: consider the tree of height 5 in the middle of the fourth row:
--
--	30373
--	25512
--	65332
--	33549 <-- the 5; middle number
--	35390
--
--	* Looking up, its view is blocked at 2 trees (by another tree with a height of 5).
--	* Looking left, its view is not blocked; it can see 2 trees.
--	* Looking down, its view is also not blocked; it can see 1 tree.
--	* Looking right, its view is blocked at 2 trees (by a massive tree of height 9).
--
--	This tree's scenic score is 8 (2 * 2 * 1 * 2); this is the ideal spot for the tree house.
--
--	Consider each tree on your map. What is the highest scenic score possible for any tree?
--
-- Test:
--	% cd ../.. && echo -e '30373\n25512\n65332\n33549\n35390' | cabal run 2022_day08-treetop-tree-house_b # = 8

solve :: [String] -> Int
solve ss = maximum scenicScores
  where maxX = (length . head $ ss) - 1
        maxY = length ss - 1
        tss  = transpose ss
        get x y ns = (ns !! y) !! x
        beforeAfter x y ns = second tail . splitAt x $ ns !! y
        viewingDist h ns = viewingDist' ns
          where viewingDist' [] = 0
                viewingDist' (n:_) | n >= h = 1
                viewingDist' (n:ns) = 1 + viewingDist' ns
        scenicScore x y =
          let h = get x y ss
              (hb, ha) = beforeAfter x y ss
              (vb, va) = beforeAfter y x tss
          in viewingDist h (reverse hb) * viewingDist h ha * viewingDist h (reverse vb) * viewingDist h va
        scenicScores = foldl' (\acc y -> foldl' (\acc' x -> scenicScore x y:acc') acc [1..maxX-1]) [] [1..maxY-1]


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.Array as A ((!), Array, bounds, listArray)
import Data.List as L (find, map)
import Data.Maybe (fromMaybe, isJust)
import Data.Set as S (Set, empty, foldl', insert, map, singleton)

--	... From the same initial conditions, how quickly can you make it from the start to the goal, then
--	back to the start, then back to the goal?
--
--	In the above example, the first trip to the goal takes 18 minutes, the trip back to the start takes 23
--	minutes, and the trip back to the goal again takes 13 minutes, for a total time of 54 minutes.
--
--	What is the fewest number of minutes required to reach the goal, go back to the start, then reach the goal again?
--
-- Test:
--	% cd ../.. && echo -e '#.######\n#>>.<^<#\n#.<..<<#\n#>v.><>#\n#<^v^^>#\n######.#' | cabal run 2022_day24-blizzard-basin_b # = 54

type ValleyMap = Array (Int, Int) Char
type State = (Int, (Int, Int), (Int, Int)) -- Steps/Minutes, (GoalY GoalX), (YouY, YouX)

gmround :: ValleyMap -> Set State -> State -> Set State
gmround vm acc s@(stps, g, u) = moveS acc s
  where stps' = stps + 1
        ((0, 0), (maxY, maxX)) = bounds vm
        --
        moveS :: Set State -> State -> Set State
        moveS acc s@(stps, g, (uY, uX))
          | g == (uY + 1, uX)                                = singleton (stps', g, g)
          | uY >= maxY || '.' /= getCh stps' (uY + 1, uX) vm = moveN acc s
          | otherwise                                        = moveN (insert (stps', g, (uY + 1, uX)) acc) s
        moveN acc s@(stps, g, (uY, uX))
          | g == (uY - 1, uX)                                = singleton (stps', g, g)
          | uY <= 1 || '.' /= getCh stps' (uY - 1, uX) vm    = moveE acc s
          | otherwise                                        = moveE (insert (stps', g, (uY - 1, uX)) acc) s
        moveE acc s@(stps, g, (uY, uX))
          | uX >= maxX || '.' /= getCh stps' (uY, uX + 1) vm = moveW acc s
          | otherwise                                        = moveW (insert (stps', g, (uY, uX + 1)) acc) s
        moveW acc s@(stps, g, (uY, uX))
          | uX <= 1 || '.' /= getCh stps' (uY, uX - 1) vm    = wait acc s
          | otherwise                                        = wait (insert (stps', g, (uY, uX - 1)) acc) s
        wait :: Set State -> State -> Set State
        wait acc s@(stps, g, u@(uY, uX))
          | not (null acc) && (0 == uY || maxY == uY) = acc -- only wait in "homebase" is there is no other option ... maybe I got lucky on this, maybe waiting and not moving could be valid?
          | '.' /= getCh stps' (uY, uX) vm            = acc
          | otherwise                                 = insert (stps', g, u) acc

gmstates :: ValleyMap -> Set State -> Set State
gmstates vm = S.foldl' (gmround vm) empty

-- Run until the first (i.e. fastest) winning state is found
run :: ValleyMap -> Set State -> State
run vm ss
  | null ss   = error "No game states"
  | otherwise = let win = find (\s@(_, e, u) -> e == u) ss
                in if isJust win then fromMaybe (0, (0,0), (0,0)) win else run vm . gmstates vm $ ss

{-* `getCh` uses modulo math to find the contents of the specified coordinates; no need to recalculate the board for just 4 or 5 characters. *-}
getCh :: Int -> (Int, Int) -> ValleyMap -> Char
getCh stps (y, x) vm = ch
  where ch
          | 0 == y || maxY == y || 0 == x || maxX == x = vm ! (y, x)
          | '^' == vm ! (newY $ y + stps, x) = '^'
          | 'v' == vm ! (newY $ y - stps, x) = 'v'
          | '<' == vm ! (y, newX $ x + stps) = '<'
          | '>' == vm ! (y, newX $ x - stps) = '>'
          | otherwise = '.'
        newY y = 1 + (y - 1) `mod` (maxY - 1)
        newX x = 1 + (x - 1) `mod` (maxX - 1)
        ((0, 0), (maxY, maxX)) = bounds vm

{-* `showmap` is useful for debugging/visualizing; leaving in final solution. *-}
showmap :: ValleyMap -> State -> [String]
showmap vm s@(stps, _, (uY, uX)) = L.map showXs [0..maxY]
  where showXs y = L.map (showCs y) [0..maxX]
        showCs y x | y == uY && x == uX = 'E'
                   | otherwise          = getCh stps (y, x) vm
        ((0, 0), (maxY, maxX)) = bounds vm

solve :: [String] -> Int
solve ys = stps3
  where maxY  = length ys - 1 :: Int
        maxX  = (length . head $ ys) - 1 :: Int
        enter = (0, length . takeWhile ('#' ==) $ head ys) :: (Int, Int)
        exit  = (maxY, length . takeWhile ('#' ==) $ last ys) :: (Int, Int)
        vm :: ValleyMap
        vm = listArray ((0, 0), (maxY, maxX)) . concat $ ys
        --
        s1@(stps1, _, _) = run vm $ singleton (    0, exit, enter) -- find exit
        s2@(stps2, _, _) = run vm $ singleton (stps1, enter, exit) -- return to entrance from the exit point
        s3@(stps3, _, _) = run vm $ singleton (stps2, exit, enter) -- find exit ... again


main :: IO ()
main = interact $ show . solve . lines

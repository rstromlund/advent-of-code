module Main where
import Data.Bool (bool)
import Data.List as L (elem, filter, map)
import Data.Maybe (isJust, fromMaybe)
import Data.Set as S (Set, empty, filter, foldl', fromList, insert, lookupMax, map, member, union)
import Data.Vector.Unboxed as V ((!), (//), Vector, foldl', fromList, map, replicate, unsafeUpd)

--	The elephants are not impressed by your simulation. They demand to know how tall the tower will be
--	after 1000000000000 rocks have stopped! Only then will they feel confident enough to proceed through
--	the cave.
--
--	In the example above, the tower would be 1514285714288 units tall!
--
--	How tall will the tower be after 1000000000000 rocks have stopped?
--
-- Test:
--	% cd ../.. && echo -e '>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>' | cabal run 2022_day17-pyroclastic-flow_b # 1514285714288

rocks :: [Set (Int, Int)]
rocks =
  [
    S.fromList [(0, 0), (1, 0), (2, 0), (3, 0)],         -- shape "-"
    S.fromList [
                  (1, 2),
          (0, 1),         (2, 1),
                  (1, 0)],                               -- shape "+"
    S.fromList [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)], -- shape backwards "L"
    S.fromList [(0, 0), (0, 1), (0, 2), (0, 3)],         -- shape "|"
    S.fromList [(0, 0), (1, 0), (0, 1), (1, 1)]          -- shape "*"
  ]

-- +-------+
rockyFloor = S.fromList $ zip [0..width - 1] [-1,-1..]

width     = 7 :: Int
leftEdge  = 2 :: Int
bottomGap = 3 :: Int

puzzleIterations = 1000000000000 :: Int -- ans: 1514285714288 / 1585632183915
-- puzzleIterations = 2022 :: Int -- ans: 3068 / 3232
-- puzzleIterations = 5555 :: Int -- ans: 8417 / 8825

{-*
These magic numbers come from sleuthing in bash; but they could be discovered programatically.  I cheated :p
I added trace printouts and did commands like:

	rm -f nohup.out && time nohup cabal run 2022_day17-pyroclastic-flow_b.hs< ./17a.txt
	sort nohup.out |uniq -c|sort -n |grep -v '  1 ' | cut -d'(' -f2- | sed -e 's!\[!\\[!g' -e 's!\]!\\]!g' > x.puzzle-2022
	grep -f x.puzzle-2022 nohup.out |less

This helped find the rock# that cycled in with the same jet# when the "skyline"s were congruent.  One set of magic
numbers for the test example and one for the puzzle input (b/c the different jet bursts and lengths).  This is very
specific to my puzzle input!
*-}

pMagicRock    = 0   :: Int
pMagicJet     = 518 :: Int
pMagicCavern  = V.fromList [5 :: Int,4,0,4,2,4,4]

eMagicRock    = 3  :: Int
eMagicJet     = 27 :: Int
eMagicCavern  = V.fromList [2 :: Int,2,0,2,3,5,7]


-- NOTE: flipping the set to (y,x) format would make getting max height easier.
getHeight :: Set (Int, Int) -> Int
getHeight = S.foldl' (\acc (_, y) -> max acc y) (-1)

{-*
I guessed that when a rock# and jet# and skyline had cycled; then that must be a place to fast forward.
Thinking about it again, there could be the same skyline appear many times for many different rock/jet configurations.
But with only 7 columns and only 5 rock types, it would be difficult to break this assumption; I got lucky I think.
*-}
cavernSkyline :: Set (Int, Int) -> Vector Int
cavernSkyline cavern =
  let h  = getHeight cavern
      vs = reverse . L.foldl' (\acc x -> (snd . fromMaybe (-1,-1) . lookupMax . S.filter (\(x', y) -> x == x') $ cavern):acc) [] $ [0..6]
  in V.fromList $ L.map (h -) vs

-- This is just for debugging; keeping it here b/c it was handy :)
drawCavern :: Set (Int, Int) -> [[Char]]
drawCavern c = L.foldl' drawY [] [0..getHeight c + 4]
  where drawY acc y = reverse (L.foldl' drawX [] [0..width - 1]):acc
          where drawX acc x = bool '.' '#' ((x, y) `member` c):acc

-- This state grew quite a bit; maybe a record or even a state monad would have been better.  But, you know, brute force when in a hurry.
type State = (Set (Int, Int), (Int, [Set (Int, Int)], Int, [Char], [(Int, Int, Int)])) -- (Cavern (y heights per x slot), (Current rock, Cycle of rocks, Current jet, Cycle of jets, [(Magic Rock#, Jet#, Jet burn, Max height]))

solve :: [Char] -> Int
solve jets = (+1) . getHeight . fst . dropRocks $ (rockyFloor, (0, cycle rocks, -1, cycle jets, []))
  where totalRocks = length rocks
        totalJets  = length jets
        --
        dropRocks :: State -> State
        dropRocks st@(_, (rcnt, _, _, _, _)) | puzzleIterations == rcnt = st
        dropRocks st = let (cavern', (rcnt', rocks', jcnt', jets', ms')) = timeWarp st
                       in dropRocks (cavern', (rcnt' + 1, rocks', jcnt', jets', ms'))
        --
        timeWarp :: State -> State
        timeWarp (cavern, (rcnt, rocks, jcnt, jets, []))
          | (40 == totalJets && eMagicRock == rcnt `mod` totalRocks && eMagicJet == jcnt `mod` totalJets && eMagicCavern == cavernSkyline cavern) ||
            (40 /= totalJets && pMagicRock == rcnt `mod` totalRocks && pMagicJet == jcnt `mod` totalJets && pMagicCavern == cavernSkyline cavern)
          = dropRock (cavern, (rcnt, tail rocks, jcnt, jets, [(rcnt, jcnt, getHeight cavern)])) . startNewRock cavern . head $ rocks

        timeWarp (cavern, (rcnt, rocks, jcnt, jets, [m1@(r1, j1, h1)]))
          | (40 == totalJets && eMagicRock == rcnt `mod` totalRocks && eMagicJet == jcnt `mod` totalJets && eMagicCavern == cavernSkyline cavern) ||
            (40 /= totalJets && pMagicRock == rcnt `mod` totalRocks && pMagicJet == jcnt `mod` totalJets && pMagicCavern == cavernSkyline cavern)
          = let r2      = rcnt
                j2      = jcnt -- Note: jet#s are not used, could drop from the state 3-tuple (I though I'd need them here).
                h2      = getHeight cavern
                --
                -- I created these formulas in a spreadsheet after running the above bash commands, then translated here:
                --
                dr      = r2 - r1 -- delta rock#
                dh      = h2 - h1 -- delta height
                cycles  = (puzzleIterations - 1 - r1) `div` dr
                rcnt'   = cycles * dr + r1
                cavern' = cavern `S.union` S.map (\(x, y) -> (x, y + (cycles - 1) * dh)) cavern
                ms'     = [m1, (r2, j2, h2)] -- force thru the default clause below
            in dropRock (cavern', (rcnt', tail rocks, jcnt, jets, ms')) . startNewRock cavern' . head $ rocks

        timeWarp (cavern, (rcnt, rocks, jcnt, jets, ms)) = dropRock (cavern, (rcnt, tail rocks, jcnt, jets, ms)) . startNewRock cavern . head $ rocks
        --
        startNewRock cavern rock =
          let h = getHeight cavern
          in S.map (\(x, y) -> (x + leftEdge, y + h + bottomGap + 1)) rock
        --
        dropRock :: State -> Set (Int, Int) -> State
        dropRock (cavern, (rcnt, rocks, jcnt, jets, ms)) rock =
          let jets' = head jets
              rockJ = moveByJet jets' cavern rock
              rockD = moveDown cavern $ fromMaybe rock rockJ
          in if isJust rockD then dropRock (cavern, (rcnt, rocks, jcnt + 1, tail jets, ms)) $ fromMaybe empty rockD else (cavern `union` fromMaybe rock rockJ, (rcnt, rocks, jcnt + 1, tail jets, ms))
        --
        moveByJet :: Char -> Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveByJet '<' = moveRock (-1) 0
        moveByJet '>' = moveRock   1  0
        --
        moveDown :: Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveDown = moveRock 0 1
        --
        moveRock :: Int -> Int -> Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveRock dx dy c = S.foldl' moveRock' (Just empty)
          where moveRock' Nothing _ = Nothing
                moveRock' (Just acc) (x, y) =
                  let pt'@(x', _) = (x + dx, y - dy)
                  in if pt' `member` c || x' < 0 || x' >= width then Nothing else Just $ insert pt' acc


main :: IO ()
main = interact $ show . solve . L.filter (`elem` "<>")

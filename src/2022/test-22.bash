#!/usr/bin/env bash

typeset width=50

## Test my "wrapping" logic for day22. Submit various trips and test the ending point.
## This only works if `solve` returns the state and not the score; a couple of well placed "--" comments can make this happen.

function test22 {
	typeset f="${1}"
	typeset pt="${2}"
	typeset dpt="${3}"
	typeset is="${4}"
	echo "TEST: ${f} ${pt} ${dpt} ${is}"
	sed -e "s/initState *=.*$/initState = (${f}, (${pt}, ${dpt}))/" < day22-monkey-map_b.hs > x
	chmod 700 x
	if (( 50 == width )) ; then
		typeset res=$(
			echo -e "`cat test-22.txt`\n\n${is}" | ./x
		)
	else
		typeset res=$(
			echo -e "        ....\n        ....\n        ....\n        ....\n............\n............\n............\n............\n        ........\n        ........\n        ........\n        ........\n\n${is}" \
				| ./x
		)
	fi
	typeset ept="${res:4:7}"
	typeset ept="${ept%%[)]*})"
	if [[ "${pt}" != "${ept}" ]] ; then echo "${f}/${dpt}/${is}: ${pt} != ${ept}" ; exit 1 ; fi
}

typeset face=1
typeset dir=dRight
typeset spt='(0,0)'
typeset circumference=$(( 4 * width ))

for dir in dRight dLeft dUp dDown ; do
	for face in {1..6} ; do
		for spt in '(0,0)' '(3,0)' '(0,3)' '(3,3)' ; do
			# cross a boundary and return to starting point
			test22 ${face} ${spt} ${dir} '1RR1'
			test22 ${face} ${spt} ${dir} '1RR1'
			test22 ${face} ${spt} ${dir} '1RR1'
			test22 ${face} ${spt} ${dir} '1RR1'
	
			# circumnavigate the cube around to the starting point; then go and come back
			test22 ${face} "${spt}" ${dir} "${circumference}"
			test22 ${face} "${spt}" ${dir} "${circumference}RR${circumference}"
		done
	done
done

exit 0

module Main where

--	... "Anyway, the second column says how the round needs to end: X means you need to lose, Y means you need to end the round in a draw, and Z means you need to win. Good luck!"

--	The total score is still calculated in the same way, but now you need to figure
--	out what shape to choose so the round ends as indicated. The example above now
--	goes like this:
--
--	* In the first round, your opponent will choose Rock (A), and you need the round
--	  to end in a draw (Y), so you also choose Rock. This gives you a score of 1 + 3 = 4.
--	* In the second round, your opponent will choose Paper (B), and you choose Rock so
--	  you lose (X) with a score of 1 + 0 = 1.
--	* In the third round, you will defeat your opponent's Scissors with Rock for a score of 1 + 6 = 7.
--
--	Now that you're correctly decrypting the ultra top secret strategy guide, you would get a total score of 12.
--
--	Following the Elf's instructions for the second column, what would your total score be if everything goes exactly according to your strategy guide?
--
-- Test:
--	% cd ../.. && echo -e 'A Y\nB X\nC Z' | cabal run 2022_day02-rock-paper-scissors_b # = 12

data RPS = Rock | Paper | Scissors
data WLD = Win | Lose | Draw

selectP1 'A' = Rock
selectP1 'B' = Paper
selectP1 'C' = Scissors

selectP2 'X' = Lose
selectP2 'Y' = Draw
selectP2 'Z' = Win

myScore :: RPS -> Int
myScore Rock     = 1
myScore Paper    = 2
myScore Scissors = 3

play :: RPS -> WLD -> Int
play p1@Scissors Draw = 3 + myScore p1
play p1@Rock     Draw = 3 + myScore p1
play p1@Paper    Draw = 3 + myScore p1

play Rock        Win  = 6 + myScore Paper
play Paper       Win  = 6 + myScore Scissors
play Scissors    Win  = 6 + myScore Rock

play Rock        Lose = myScore Scissors
play Paper       Lose = myScore Rock
play Scissors    Lose = myScore Paper

playRound [p1, _, p2] = play (selectP1 p1) $ selectP2 p2

main :: IO ()
main = interact $ show . sum . map playRound . lines

module Main where
import Data.Char (isAsciiLower, isAsciiUpper, ord)
import Data.List (intersect)

--	... For example, suppose you have the following list of contents from six rucksacks:
--
--	vJrwpWtwJgWrhcsFMMfFFhFp
--	jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
--	PmmdzqPrVvPwwTWBwg
--	wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
--	ttgJtRGJQctTZtZT
--	CrZsJsPPZsGzwwsLwLmpwMDw
--
--	* The first rucksack contains the items vJrwpWtwJgWrhcsFMMfFFhFp, which means its
--	  first compartment contains the items vJrwpWtwJgWr, while the second compartment
--	  contains the items hcsFMMfFFhFp. The only item type that appears in both
--	  compartments is lowercase p.
--	* The second rucksack's compartments contain jqHRNqRjqzjGDLGL and
--	  rsFMfFZSrLrFZsSL. The only item type that appears in both compartments is
--	  uppercase L.
--	* The third rucksack's compartments contain PmmdzqPrV and vPwwTWBwg; the only common
--	  item type is uppercase P.
--	* The fourth rucksack's compartments only share item type v.
--	* The fifth rucksack's compartments only share item type t.
--	* The sixth rucksack's compartments only share item type s.
--
--	To help prioritize item rearrangement, every item type can be converted to a priority:
--	* Lowercase item types a through z have priorities 1 through 26.
--	* Uppercase item types A through Z have priorities 27 through 52.
--
--	In the above example, the priority of the item type that appears in both
--	compartments of each rucksack is 16 (p), 38 (L), 42 (P), 22 (v), 20 (t), and 19
--	(s); the sum of these is 157.
--
--	Find the item type that appears in both compartments of each rucksack. What is the sum of the priorities of those item types?
--
-- Test:
--	% cd ../.. && echo -e 'vJrwpWtwJgWrhcsFMMfFFhFp\njqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL\nPmmdzqPrVvPwwTWBwg\nwMqvLMZHhHMvwLHjbvcjnnSBnvTQFn\nttgJtRGJQctTZtZT\nCrZsJsPPZsGzwwsLwLmpwMDw' | cabal run 2022_day03-rucksack-reorganization # = 157

solve :: [String] -> Int
solve = sum . map (priority . head . uncurry intersect . halve)
  where halve s = splitAt (length s `div` 2) s
        priority ch
          | isAsciiLower ch = ord ch - ord 'a' + 1
          | isAsciiUpper ch = ord ch - ord 'A' + 27


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.Array (Array, bounds, inRange, listArray, (!))
import Data.Set (Set, deleteFindMin, empty, insert, member, notMember, singleton)

--	... the heightmap are marks for your current position (S) and the location that should get the best
--	signal (E). Your current position (S) has elevation a, and the location that should get the best
--	signal (E) has elevation z.
--
--	You'd like to reach E, but to save energy, you should do it in as few steps as possible. During each
--	step, you can move exactly one square up, down, left, or right. To avoid needing to get out your
--	climbing gear, the elevation of the destination square can be at most one higher than the elevation of
--	your current square; that is, if your current elevation is m, you could step to elevation n, but not
--	to elevation o. (This also means that the elevation of the destination square can be much lower than
--	the elevation of your current square.)
--
--	For example:
--
--	Sabqponm
--	abcryxxl
--	accszExk
--	acctuvwj
--	abdefghi
--
--	Here, you start in the top-left corner; your goal is near the middle. You could start by moving down
--	or right, but eventually you'll need to head toward the e at the bottom. From there, you can spiral
--	around to the goal:
--
--	v..v<<<<
--	>v.vv<<^
--	.>vv>E^^
--	..v>>>^^
--	..>>>>>^
--
--	In the above diagram, the symbols indicate whether the path exits each square moving up (^), down (v),
--	left (<), or right (>). The location that should get the best signal is still E, and . marks unvisited
--	squares.
--
--	This path reaches the goal in 31 steps, the fewest possible.
--
--	What is the fewest steps required to move from your current position to the location that should get the best signal?
--
-- Test:
--	% cd ../.. && echo -e 'Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi' | cabal run 2022_day12-hill-climbing-algorithm ## 31

-- Algorithm: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way

mkGrid :: [String] -> (Point, Point, Array Point Char)
mkGrid cs = (b, e, listArray ((0, 0), (ly, lx)) $ reverse hs)
  where ((_, b, e), hs) = foldl' traverseYs (((0,0), (-10,-10), (-5, -5)), []) cs
        ly = pred . length $ cs
        lx = pred . length . head $ cs
        traverseYs acc xs =
          let (((y, _), s, e), map) = foldl' traverseXs acc xs
          in (((y + 1, 0), s, e), map)
        traverseXs ((pt@(y, x), s, e), map) ch =
          let (val, s', e')
                | 'S' == ch = ('a', pt, e)
                | 'E' == ch = ('z', s,  pt)
                | otherwise = (ch,  s,  e)
              acc = (((y, x + 1), s', e'), val:map)
          in acc

solve :: (Point, Point, Array Point Char) -> Int
solve (b, e, hmap) = dijkstra empty $ singleton (0, b)
  where dijkstra :: Set Point -> Set (Int, Point) -> Int
        dijkstra vps costs = nextStep (r, pt) costs'
          where ((r, pt), costs') = deleteFindMin costs
                curHsucc = succ $ hmap ! pt
                nextStep (r, pt) costs
                  | pt == e           = r
                  | pt `member` vps = dijkstra vps costs
                  | otherwise         = dijkstra (insert pt vps) neighbors
                --
                -- Upto 4 neighbors per `dijkstra` visit point.
                neighbors = foldl' (flip insert) costs' [(r + 1, pt) | pt <- findNeighbors pt, pt `notMember` vps]
                findNeighbors (y, x) =
                  [ neighbor | neighbor <- [(y - 1, x), (y + 1, x), (y, x + 1), (y, x - 1)]
                             , inRange (bounds hmap) neighbor
                             , hmap ! neighbor <= curHsucc ]


main :: IO ()
main = interact $ show . solve . mkGrid . lines

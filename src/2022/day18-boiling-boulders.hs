module Main where
import Data.Bool (bool)
import Data.List as L (map)
import Data.Set  as S (Set, foldl', fromList, member)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... To approximate the surface area, count the number of sides of each cube that are not immediately
--	connected to another cube. So, if your scan were only two adjacent cubes like 1,1,1 and 2,1,1, each
--	cube would have a single side covered and five sides exposed, a total surface area of 10 sides.
--
--	Here's a larger example:
--
--	2,2,2
--	1,2,2
--	3,2,2
--	2,1,2
--	2,3,2
--	2,2,1
--	2,2,3
--	2,2,4
--	2,2,6
--	1,2,5
--	3,2,5
--	2,1,5
--	2,3,5
--
--	In the above example, after counting up all the sides that aren't connected to another cube, the total surface area is 64.
--
--	What is the surface area of your scanned lava droplet?
--
-- Test:
--	% cd ../.. && echo -e '1,1,1\n2,1,1' | cabal run 2022_day18-boiling-boulders # = 10
--	% cd ../.. && echo -e '2,2,2\n1,2,2\n3,2,2\n2,1,2\n2,3,2\n2,2,1\n2,2,3\n2,2,4\n2,2,6\n1,2,5\n3,2,5\n2,1,5\n2,3,5' | cabal run 2022_day18-boiling-boulders # = 64

type Parser = Parsec Void String
type Droplet = (Int, Int, Int)

droplets :: Parser [Droplet]
droplets = many droplet <* optional newline <* eof

droplet :: Parser Droplet
droplet = mkTuple <$> (decimal `sepBy` char ',') <* newline
  where mkTuple [x,y,z] = (x, y, z)

--

ckSides =
  [ ( 1,  0,  0)
  , (-1,  0,  0)
  , ( 0,  1,  0)
  , ( 0, -1,  0)
  , ( 0,  0,  1)
  , ( 0,  0, -1)
  ]

solve :: Either a [Droplet] -> Int
solve (Right ds) = S.foldl' surfaceArea 0 ss
  where ss = fromList ds
        surfaceArea acc d@(x, y, z) = (+) acc . sum . L.map (\(dx, dy, dz) -> bool 1 0 $ (x + dx, y + dy, z + dz) `member` ss) $ ckSides


main :: IO ()
main = interact $ show . solve . runParser droplets "<stdin>"

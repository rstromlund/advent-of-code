module Main where
import Data.Bool (bool)
import Data.List (find, nub, sort)
import Data.Maybe (isJust)
import Data.Tuple.Extra (fst3, snd3)
import Data.Void (Void)
import Numeric.Interval ((...), (==?), Interval, hull, inflate, member, width)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Once a sensor finds a spot it thinks will give it a good reading, it attaches itself to a hard
--	surface and begins monitoring for the nearest signal source beacon. Sensors and beacons always exist
--	at integer coordinates. Each sensor knows its own position and can determine the position of a beacon
--	precisely; however, sensors can only lock on to the one beacon closest to the sensor as measured by
--	the Manhattan distance. (There is never a tie where two beacons are the same distance to a sensor.)
--
--	It doesn't take long for the sensors to report back their positions and closest beacons (your puzzle input).
--
--	... This sensor's closest beacon is at 2,10, and so you know there are no beacons that close or closer
--	(in any positions marked #).
--
--	None of the detected beacons seem to be producing the distress signal, so you'll need to work out
--	where the distress beacon is by working out where it isn't. For now, keep things simple by counting
--	the positions where a beacon cannot possibly be along just a single row.
--
--	So, suppose you have an arrangement of beacons and sensors like in the example above and, just in the
--	row where y=10, you'd like to count the number of positions a beacon cannot possibly exist. The
--	coverage from all sensors near that row looks like this:
--
--	                 1    1    2    2
--	       0    5    0    5    0    5
--	 9 ...#########################...
--	10 ..####B######################..
--	11 .###S#############.###########.
--
--	In this example, in the row where y=10, there are 26 positions where a beacon cannot be present.
--
--	Consult the report from the sensors you just deployed. In the row where y=2000000, how many positions cannot contain a beacon?
--
-- Test:
--	% cd ../.. && echo -e 'Sensor at x=2, y=18: closest beacon is at x=-2, y=15\nSensor at x=9, y=16: closest beacon is at x=10, y=16\nSensor at x=13, y=2: closest beacon is at x=15, y=3\nSensor at x=12, y=14: closest beacon is at x=10, y=16\nSensor at x=10, y=20: closest beacon is at x=10, y=16\nSensor at x=14, y=17: closest beacon is at x=10, y=16\nSensor at x=8, y=7: closest beacon is at x=2, y=10\nSensor at x=2, y=0: closest beacon is at x=2, y=10\nSensor at x=0, y=11: closest beacon is at x=2, y=10\nSensor at x=20, y=14: closest beacon is at x=25, y=17\nSensor at x=17, y=20: closest beacon is at x=21, y=22\nSensor at x=16, y=7: closest beacon is at x=15, y=3\nSensor at x=14, y=3: closest beacon is at x=15, y=3\nSensor at x=20, y=1: closest beacon is at x=15, y=3' | cabal run 2022_day15-beacon-exclusion-zone # 26

type Point = (Int, Int) -- X,Y
type Sensor = (Point, Point, Int) -- Sensor, Closest Beacon, Manhattan distance

type Parser = Parsec Void String

sensors :: Parser [Sensor]
sensors = many sensor <* optional newline <* eof

sensor :: Parser Sensor
sensor = mkSensor <$> s <*> b
  where s = (,) <$> (string "Sensor at x=" *> signed) <*> (string ", y=" *> signed)
        b = (,) <$> (string ": closest beacon is at x=" *> signed) <*> (string ", y=" *> signed <* newline)
        mkSensor s@(sx, sy) b@(bx, by) = (s, b, abs (bx - sx) + abs (by - sy))

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

--

{-*
parsed input: "[((2,18),(-2,15),7),((9,16),(10,16),1),((13,2),(15,3),3),((12,14),(10,16),4),((10,20),(10,16),4),((14,17),(10,16),5),((8,7),(2,10),9),((2,0),(2,10),10),((0,11),(2,10),3),((20,14),(25,17),8),((17,20),(21,22),6),((16,7),(15,3),5),((14,3),(15,3),1),((20,1),(15,3),7)]"


     #####                            ,(-2,2,((0,11),(2,10),3))
         #                            ,(2,2,((2,0),(2,10),10))
         #############                ,(2,14,((8,7),(2,10),9))
                   #                  ,(12,12,((12,14),(10,16),4))]"
                     #####            "[(14,18,((16,7),(15,3),5))
                       #########      ,(16,24,((20,14),(25,17),8))
                 1    1    2    2
       0    5    0    5    0    5
 9 ...#########################...
10 ..####B######################..
11 .###S#############.###########.

      ###                             ,Interval  (Closed (-1)) (Closed 1)
        ###############               ,Interval  (Closed 1) (Closed 15)]"
        ###                           ,Interval  (Closed 1) (Closed 3)
                    #######           "[Interval (Closed 13) (Closed 19)
                        #######       ,Interval  (Closed 17) (Closed 23)
                 1    1    2    2
       0    5    0    5    0    5
 9 ...#########################... (25)
10 ..####B######################.. (26)
11 .###S#############.###########. (27)

*-}

converge :: Eq a => (a -> a) -> a -> a
converge f x = let x' = f x in if x' == x then x else converge f x'

-- examineY = 10   -- on test data
examineY = 2000000 -- on real input (4879972)

mergeInterval :: [Interval Int] -> [Interval Int] -> [Interval Int]
mergeInterval acc []  = acc
mergeInterval acc [r] = r:acc
mergeInterval acc (r1:r2:rs)
  | inflate 1 r1 ==? r2 = mergeInterval acc (hull r1 r2:rs)
  | otherwise           = mergeInterval (r1:acc) (r2:rs)

simplify :: [Interval Int] -> [Interval Int]
simplify = mergeInterval [] . sort

solve :: Either a [Sensor] -> Int
solve (Right ss) = subObjects . converge simplify . foldl' crossesY [] $ ss
  where crossesY :: [Interval Int] -> Sensor -> [Interval Int]
        crossesY acc s@((_, sy), _, d)
          | abs (sy - examineY) <= d = crossedXs s:acc
          | otherwise                = acc
        crossedXs s@((sx, sy), _, d) =
          let a  = abs (sy - examineY)
              a' = d - a
          in sx - a' ... sx + a'
        subObjects ys =
          let w  = foldl' (\acc s -> acc + 1 + width s) 0 ys
              ss' = map fst3 ss
              bs' = nub . map snd3 $ ss
              bs = foldl (\acc (x, y) -> acc + bool 0 1 (examineY == y && isCovered x)) 0 (ss' ++ bs')
              isCovered x = isJust . find (\r -> x `member` r) $ ys
          in w - bs


main :: IO ()
main = interact $ show . solve . runParser sensors "<stdin>"

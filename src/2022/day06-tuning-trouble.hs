module Main where

--	... your subroutine needs to identify the first position where the four most recently received
--	characters were all different. Specifically, it needs to report the number of characters from the
--	beginning of the buffer to the end of the first such four-character marker.
--
--	For example, suppose you receive the following datastream buffer:
--
--	mjqjpqmgbljsphdztnvjfqwrcgsmlb
--
--	After the first three characters (mjq) have been received, there haven't been enough characters
--	received yet to find the marker. The first time a marker could occur is after the fourth character is
--	received, making the most recent four characters mjqj. Because j is repeated, this isn't a marker.
--
--	The first time a marker appears is after the seventh character arrives. Once it does, the last four
--	characters received are jpqm, which are all different. In this case, your subroutine should report the
--	value 7, because the first start-of-packet marker is complete after 7 characters have been processed.
--
--	Here are a few more examples:
--
--	* bvwbjplbgvbhsrlpgdmjqwftvncz: first marker after character 5
--	* nppdvjthqldpwncqszvftbrmjlhg: first marker after character 6
--	* nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg: first marker after character 10
--	* zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw: first marker after character 11
--
--	How many characters need to be processed before the first start-of-packet marker is detected?
--
-- Test:
--	% cd ../.. && echo -e 'mjqjpqmgbljsphdztnvjfqwrcgsmlb' | cabal run 2022_day06-tuning-trouble # = 7
--	% cd ../.. && echo -e 'bvwbjplbgvbhsrlpgdmjqwftvncz' | cabal run 2022_day06-tuning-trouble # = 5
--	% cd ../.. && echo -e 'nppdvjthqldpwncqszvftbrmjlhg' | cabal run 2022_day06-tuning-trouble # = 6
--	% cd ../.. && echo -e 'nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg' | cabal run 2022_day06-tuning-trouble # = 10
--	% cd ../.. && echo -e 'zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw' | cabal run 2022_day06-tuning-trouble # = 11

solve :: [Char] -> Int
solve = solve' 4
  where solve' cnt (a:cs@(b:c:d:_))
          | a /= b && a /= c && a /= d &&
            b /= c && b /= d &&
            c /= d    = cnt
          | otherwise = solve' (cnt + 1) cs

main :: IO ()
main = interact $ show . solve

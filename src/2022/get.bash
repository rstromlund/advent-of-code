#!/bin/bash
typeset DAY="${1:-1}"
set -x
[[ -r "${DAY}a.html" && ! -r "${DAY}b.html" ]] && curl --cookie-jar 'cookies.txt' --cookie 'cookies.txt' --output "${DAY}b.html" "https://adventofcode.com/2022/day/${DAY#0}"

[[ -r "${DAY}a.html" ]] || curl --cookie-jar 'cookies.txt' --cookie 'cookies.txt' --output "${DAY}a.html" "https://adventofcode.com/2022/day/${DAY#0}"
[[ -r "${DAY}a.txt"  ]] || curl --cookie-jar 'cookies.txt' --cookie 'cookies.txt' --output "${DAY}a.txt" "https://adventofcode.com/2022/day/${DAY#0}/input"

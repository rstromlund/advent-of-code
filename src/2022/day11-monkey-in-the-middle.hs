module Main where
import Data.Bool (bool)
import Data.List (sortBy, splitAt)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, oneOf, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... As you finally start making your way upriver, you realize your pack is much lighter than you
--	remember. Just then, one of the items from your pack goes flying overhead. Monkeys are playing Keep
--	Away with your missing things!
--
--	To get your stuff back, you need to be able to predict where the monkeys will throw your items. After
--	some careful observation, you realize the monkeys operate based on how worried you are about each
--	item.
--
--	You take some notes (your puzzle input) on the items each monkey currently has, how worried you are
--	about those items, and how the monkey makes decisions based on your worry level. For example:
--
--	Monkey 0:
--	  Starting items: 79, 98
--	  Operation: new = old * 19
--	  Test: divisible by 23
--	    If true: throw to monkey 2
--	    If false: throw to monkey 3
--
--	Monkey 1:
--	  Starting items: 54, 65, 75, 74
--	  Operation: new = old + 6
--	  Test: divisible by 19
--	    If true: throw to monkey 2
--	    If false: throw to monkey 0
--
--	Monkey 2:
--	  Starting items: 79, 60, 97
--	  Operation: new = old * old
--	  Test: divisible by 13
--	    If true: throw to monkey 1
--	    If false: throw to monkey 3
--
--	Monkey 3:
--	  Starting items: 74
--	  Operation: new = old + 3
--	  Test: divisible by 17
--	    If true: throw to monkey 0
--	    If false: throw to monkey 1
--
--	Each monkey has several attributes:
--
--	* Starting items lists your worry level for each item the monkey is currently holding in the order they will be inspected.
--	* Operation shows how your worry level changes as that monkey inspects an item. (An operation like new = old * 5
--	  means that your worry level after the monkey inspected the item is five times whatever your worry level was before inspection.)
--	* Test shows how the monkey uses your worry level to decide where to throw an item next.
--		* If true shows what happens with an item if the Test was true.
--		* If false shows what happens with an item if the Test was false.
--
--	After each monkey inspects an item but before it tests your worry level, your relief that the monkey's
--	inspection didn't damage the item causes your worry level to be divided by three and rounded down to
--	the nearest integer.
--
--	The monkeys take turns inspecting and throwing items. On a single monkey's turn, it inspects and
--	throws all of the items it is holding one at a time and in the order listed. Monkey 0 goes first, then
--	monkey 1, and so on until each monkey has had one turn. The process of each monkey taking a single
--	turn is called a round.
--
--	When a monkey throws an item to another monkey, the item goes on the end of the recipient monkey's
--	list. A monkey that starts a round with no items could end up inspecting and throwing many items by
--	the time its turn comes around. If a monkey is holding no items at the start of its turn, its turn
--	ends.
--
--	... Chasing all of the monkeys at once is impossible; you're going to have to focus on the two most
--	active monkeys if you want any hope of getting your stuff back. Count the total number of times each
--	monkey inspects items over 20 rounds:
--
--	Monkey 0 inspected items 101 times.
--	Monkey 1 inspected items 95 times.
--	Monkey 2 inspected items 7 times.
--	Monkey 3 inspected items 105 times.
--
--	In this example, the two most active monkeys inspected items 101 and 105 times. The level of monkey
--	business in this situation can be found by multiplying these together: 10605.
--
--	Figure out which monkeys to chase by counting how many items they inspect over 20 rounds. What is the
--	level of monkey business after 20 rounds of stuff-slinging simian shenanigans?
--
-- Test:
--	% cd ../.. && echo -e 'Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1' | cabal run 2022_day11-monkey-in-the-middle # = 10605

data Operation = Plus Int | Times Int | Squared deriving (Show)

calcOp (Plus x)  old = old + x
calcOp (Times x) old = old * x
calcOp Squared   old = old * old



data Note = Note
  {
    nMonkey :: Int
  , nItems :: [Int]
  , nOp :: Operation
  , nDiv :: Int
  , nTrue :: Int
  , nFalse :: Int
  , nState :: Int
  } deriving (Show)


type Parser = Parsec Void String

notes :: Parser [Note]
notes = note `sepBy` char '\n' <* optional newline <* eof

note :: Parser Note
note = do
  m  <- monkey
  is <- items
  o  <- op
  d  <- divisible
  t  <- ifexp
  f  <- ifexp
  return Note{nMonkey = m, nItems = is, nOp = o, nDiv = d, nTrue = t, nFalse = f, nState = 0}

monkey :: Parser Int
monkey = string "Monkey " *> decimal <* string ":\n"

items :: Parser [Int]
items = string "  Starting items:" *> (char ' ' *> decimal) `sepBy` char ',' <* newline

op :: Parser Operation
op = string "  Operation: new = old " *> choice
  [ Plus    <$> (string "+ " *> decimal)
  , Squared <$  string "* old"
  , Times   <$> (string "* " *> decimal)
  ] <* newline

divisible :: Parser Int
divisible = string "  Test: divisible by " *> decimal <* newline

ifexp :: Parser Int
ifexp = string "    If " *> (string "true" <|> string "false") *> string ": throw to monkey " *> decimal <* newline

--

solve :: Either a [Note] -> Int
solve (Right ns) = score . foldl' round ns $ [1..20]
  where round ns _ = foldl' mbusiness ns [0..length ns - 1]
        mbusiness :: [Note] -> Int -> [Note]
        mbusiness ns mnbr =
          let m = ns !! mnbr
              ns' = inspects m ns
          in ns'
        --
        -- FIXME: it would probably be more efficient to keep a separate list of items instead of modifing the input,
        -- maybe a separate list of state too, keeping the original parsed puzzle state constant.
        --
        inspects m@Note{nMonkey = mnbr, nItems = []} ns = replaceAt mnbr m ns
        inspects m@Note{nMonkey = mnbr, nItems = (i:is), nOp = op, nDiv = d, nTrue = nT, nFalse = nF, nState = cnt} ns =
          let wl  = calcOp op i `div` 3
              rem = wl `mod` d
              nm  = bool nF nT $ 0 == rem
              t@Note{nItems = ti} = ns !! nm
              t'  = t{nItems = ti ++ [wl]}
          in inspects m{nItems = is, nState = cnt + 1} $ replaceAt nm t' ns
        replaceAt i x xs =
          let (b, a) = splitAt i xs
          in b ++ (x:tail a)
        score = product . take 2 . sortBy (flip compare) . map nState


main :: IO ()
main = interact $ show . solve . runParser notes "<stdin>"

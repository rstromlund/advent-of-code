module Main where
import Data.List (sort)
import Data.Void (Void)
import Numeric.Interval ((...), (==?), Interval, hull, inf, inflate)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your handheld device indicates that the distress signal is coming from a beacon nearby. The
--	distress beacon is not detected by any sensor, but the distress beacon must have x and y coordinates
--	each no lower than 0 and no larger than 4000000.
--
--	To isolate the distress beacon's signal, you need to determine its tuning frequency, which can be
--	found by multiplying its x coordinate by 4000000 and then adding its y coordinate.
--
--	In the example above, the search space is smaller: instead, the x and y coordinates can each be at
--	most 20. With this reduced search area, there is only a single position that could have a beacon:
--	x=14, y=11. The tuning frequency for this distress beacon is 56000011.
--
--	Find the only possible position for the distress beacon. What is its tuning frequency?
--
-- Test:
--	% cd ../.. && echo -e 'Sensor at x=2, y=18: closest beacon is at x=-2, y=15\nSensor at x=9, y=16: closest beacon is at x=10, y=16\nSensor at x=13, y=2: closest beacon is at x=15, y=3\nSensor at x=12, y=14: closest beacon is at x=10, y=16\nSensor at x=10, y=20: closest beacon is at x=10, y=16\nSensor at x=14, y=17: closest beacon is at x=10, y=16\nSensor at x=8, y=7: closest beacon is at x=2, y=10\nSensor at x=2, y=0: closest beacon is at x=2, y=10\nSensor at x=0, y=11: closest beacon is at x=2, y=10\nSensor at x=20, y=14: closest beacon is at x=25, y=17\nSensor at x=17, y=20: closest beacon is at x=21, y=22\nSensor at x=16, y=7: closest beacon is at x=15, y=3\nSensor at x=14, y=3: closest beacon is at x=15, y=3\nSensor at x=20, y=1: closest beacon is at x=15, y=3' | cabal run 2022_day15-beacon-exclusion-zone_b # 56000011

type Point = (Int, Int) -- X,Y
type Sensor = (Point, Point, Int) -- Sensor, Closest Beacon, Manhattan distance

type Parser = Parsec Void String

sensors :: Parser [Sensor]
sensors = many sensor <* optional newline <* eof

sensor :: Parser Sensor
sensor = mkSensor <$> s <*> b
  where s = (,) <$> (string "Sensor at x=" *> signed) <*> (string ", y=" *> signed)
        b = (,) <$> (string ": closest beacon is at x=" *> signed) <*> (string ", y=" *> signed <* newline)
        mkSensor s@(sx, sy) b@(bx, by) = (s, b, abs (bx - sx) + abs (by - sy))

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

--

converge :: Eq a => (a -> a) -> a -> a
converge f x = let x' = f x in if x' == x then x else converge f x'

tuningFreqMult = 4000000

minXY = 0
--- maxXY = 20  -- on test data
maxXY = 4000000 -- on real input (12525726647448)


mergeInterval :: [Interval Int] -> [Interval Int] -> [Interval Int]
mergeInterval acc []  = acc
mergeInterval acc [r] = r:acc
mergeInterval acc (r1:r2:rs)
  | inflate 1 r1 ==? r2 = mergeInterval acc (hull r1 r2:rs)
  | otherwise           = mergeInterval (r1:acc) (r2:rs)

simplify :: [Interval Int] -> [Interval Int]
simplify = mergeInterval [] . sort

solve :: Either a [Sensor] -> Int
solve (Right ss) = huntGap minXY
  where huntGap y
          | y <= maxXY = huntGap' y $ converge simplify . foldl' (crossesY y) [] $ ss
          | otherwise = -1
          where huntGap' y [_]   = huntGap (y + 1) -- only 1 massive range over the whole row? Hunt on ...
                huntGap' y (i:_) = (inf i - 1) * tuningFreqMult + y -- Else bingo!
        --
        crossesY :: Int -> [Interval Int] -> Sensor -> [Interval Int]
        crossesY y acc s@((_, sy), _, d)
          | abs (sy - y) <= d = crossedXs y s:acc
          | otherwise         = acc
        crossedXs y s@((sx, sy), _, d) =
          let a  = abs (sy - y)
              a' = d - a
          in max minXY (sx - a') ... min maxXY (sx + a')


main :: IO ()
main = interact $ show . solve . runParser sensors "<stdin>"

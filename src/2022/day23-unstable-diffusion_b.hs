module Main where
import Data.Bool (bool)
import Data.Function (on)
import Data.List as L (concatMap, groupBy, map, sort, splitAt)
import Data.Set as S (Set, deleteFindMin, empty, foldl', fromList, insert, map, member, notMember, singleton, size)

--	... It seems you're on the right track. Finish simulating the process and figure out where the Elves need to go. How many rounds did you save them?
--
--	In the example above, the first round where no Elf moved was round 20:
--
--	.......#......
--	....#......#..
--	..#.....#.....
--	......#.......
--	...#....#.#..#
--	#.............
--	....#.....#...
--	..#.....#.....
--	....#.#....#..
--	.........#....
--	....#......#..
--	.......#......
--	Figure out where the Elves need to go. What is the number of the first round where no Elf moves?
--
-- Test:
--	% cd ../.. && echo -e '..............\n..............\n.......#......\n.....###.#....\n...#...#.#....\n....#...##....\n...#.###......\n...##.#.##....\n....#..#......\n..............\n..............\n..............' | cabal run 2022_day23-unstable-diffusion_b # = 20

type Elf = ((Int, Int), Char)

neighbors :: Elf -> Set Elf -> (Int, Int, Int, Int)
neighbors ((y, x), c) gm =
  let nw = bool 0 1 $ ((y - 1, x - 1), c) `S.member` gm
      ne = bool 0 1 $ ((y - 1, x + 1), c) `S.member` gm
      sw = bool 0 1 $ ((y + 1, x - 1), c) `S.member` gm
      se = bool 0 1 $ ((y + 1, x + 1), c) `S.member` gm
      ans =
        ( -- NW,N,NE
          nw + bool 0 1 (((y - 1,     x), c) `S.member` gm) + ne
        , -- SW,S,SE
          sw + bool 0 1 (((y + 1,     x), c) `S.member` gm) + se
        , -- NW,W,SW
          nw + bool 0 1 (((    y, x - 1), c) `S.member` gm) + sw
        , -- NE,E,SE
          ne + bool 0 1 (((    y, x + 1), c) `S.member` gm) + se
        )
  in ans

survey :: Set Elf -> Elf -> (Bool, Bool, Bool, Bool)
survey gm e@((y, x), c) =
  let (cn, cs, cw, ce) = neighbors e gm
  in (0 == cn, 0 == cs, 0 == cw, 0 == ce) -- 4 True(s) means no neighbors and DO NOT move

gmround :: (Set Elf, [Char]) -> (Set Elf, [Char])
gmround (gm, dirs) = (ps', tail ds')
  where ps :: Set (((Int ,Int), Char), (Bool, Bool, Bool, Bool))
        ps = S.map (\e -> (e, survey gm e)) gm
        (ds, ds') = splitAt 4 dirs
        ps' = fromList . concatMap grantProposal . groupBy ((==) `on` fst) . sort . L.foldl' proposal [] $ ps
        --
        proposal :: [(Elf, Elf)] -> (((Int ,Int), Char), (Bool, Bool, Bool, Bool)) -> [(Elf, Elf)]
        proposal acc (e, (True, True, True, True))     = (e, e):acc
        proposal acc (e, (False, False, False, False)) = (e, e):acc
        proposal acc (e@((y, x), c), (bn, bs, bw, be)) = (head . L.foldl' pick [] $ ds, e):acc
          where pick :: [Elf] -> Char -> [Elf]
                pick [] 'N' | bn = [((y - 1, x), c)]
                pick [] 'S' | bs = [((y + 1, x), c)]
                pick [] 'W' | bw = [((y, x - 1), c)]
                pick [] 'E' | be = [((y, x + 1), c)]
                pick acc _ = acc
        grantProposal :: [(Elf, Elf)] -> [Elf]
        grantProposal [e1] = [fst e1]
        grantProposal es = L.map snd es

converge :: Int -> ((Set Elf, [Char]) -> (Set Elf, [Char])) -> (Set Elf, [Char]) -> Int
converge n f x = let x' = f x in if ((==) `on` fst) x' x then n else converge (n + 1) f x'

solve :: [String] -> Int
solve ys = converge 1 gmround (gm, dirs)
  where gm :: Set Elf
        gm = fromList . L.foldl' buildY [] $ zip [0..] ys
        dirs = cycle "NSWE"
        --
        buildY acc (y,xs)    = L.foldl' (buildX y) acc (zip [0..] xs)
        buildX y acc (x,'.') = acc
        buildX y acc (x,c)   = ((y, x), c):acc


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.List (notElem, sort)
import Data.Map.Strict ((!), (!?), Map, delete, filterWithKey, foldlWithKey', fromList, insert, size)
import Data.Maybe (fromMaybe)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, sepBy, some)
import Text.Megaparsec.Char (char, newline, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your device estimates that you have 30 minutes before the volcano erupts, so you don't have time
--	to go back out the way you came in.
--
--	... There's even a valve in the room you and the elephants are currently standing in labeled AA. You
--	estimate it will take you one minute to open a single valve and one minute to follow any tunnel from
--	one valve to another. What is the most pressure you could release?
--
--	For example, suppose you had the following scan output:
--
--	Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
--	Valve BB has flow rate=13; tunnels lead to valves CC, AA
--	Valve CC has flow rate=2; tunnels lead to valves DD, BB
--	Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
--	Valve EE has flow rate=3; tunnels lead to valves FF, DD
--	Valve FF has flow rate=0; tunnels lead to valves EE, GG
--	Valve GG has flow rate=0; tunnels lead to valves FF, HH
--	Valve HH has flow rate=22; tunnel leads to valve GG
--	Valve II has flow rate=0; tunnels lead to valves AA, JJ
--	Valve JJ has flow rate=21; tunnel leads to valve II
--
--	... All of the valves begin closed. You start at valve AA, but it must be damaged or jammed or
--	something: its flow rate is 0, so there's no point in opening it. However, you could spend one minute
--	moving to valve BB and another minute opening it; doing so would release pressure during the remaining
--	28 minutes at a flow rate of 13, a total eventual pressure release of 28 * 13 = 364. Then, you could
--	spend your third minute moving to valve CC and your fourth minute opening it, providing an additional
--	26 minutes of eventual pressure release at a flow rate of 2, or 52 total pressure released by valve
--	CC.
--
--	... you need to release as much pressure as possible, so you'll need to be methodical.
--
--	... This approach lets you release the most pressure possible in 30 minutes with this valve layout, 1651.
--
--	Work out the steps to release the most pressure in 30 minutes. What is the most pressure you can release?
--
-- Test:
--	% cd ../.. && echo -e 'Valve AA has flow rate=0; tunnels lead to valves DD, II, BB\nValve BB has flow rate=13; tunnels lead to valves CC, AA\nValve CC has flow rate=2; tunnels lead to valves DD, BB\nValve DD has flow rate=20; tunnels lead to valves CC, AA, EE\nValve EE has flow rate=3; tunnels lead to valves FF, DD\nValve FF has flow rate=0; tunnels lead to valves EE, GG\nValve GG has flow rate=0; tunnels lead to valves FF, HH\nValve HH has flow rate=22; tunnel leads to valve GG\nValve II has flow rate=0; tunnels lead to valves AA, JJ\nValve JJ has flow rate=21; tunnel leads to valve II' | cabal run 2022_day16-proboscidea-volcanium # 1651

type RoomConfig = (Int, [(String, Int)]) -- (Flow rate, [Connected rooms, Distance (1 to begin)])
type Room = (String, RoomConfig) -- Room name, Configuation

type Parser = Parsec Void String

rooms :: Parser [Room]
rooms = many room <* optional newline <* eof

room :: Parser Room
room = mkRoom <$> r <*> f <*> j <* newline
  where r = string "Valve " *> some upperChar
        f = string " has flow rate=" *> decimal
        j = string "; tunnel" *> optional (char 's') *> string " lead" *> optional (char 's') *> string " to valve" *> optional (char 's') *> (((,1) <$> (char ' ' *> some upperChar)) `sepBy` char ',')
        mkRoom r f j = (r, (f, j))

--

-- Floyd–Warshall algorithm
-- ref: https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm

getVertices :: Map String RoomConfig -> [String]
getVertices = sort . foldlWithKey' (\acc k _ -> k:acc) []

getEdges :: Map String RoomConfig -> [((String, String), Int)]
getEdges = foldlWithKey' getEdges' []
  where getEdges' :: [((String, String), Int)] -> String -> RoomConfig -> [((String, String), Int)]
        getEdges' acc r (_, js) = foldl' (\acc' (jx, dx) -> ((r, jx), dx):acc') acc js


-- Not the prettiest but it seems to work : )
floydWarshall :: Map String RoomConfig -> Map (String, String) Int
floydWarshall ss = fw
  where vs = getVertices ss
        dist = fromList $ getEdges ss ++ map (\r -> ((r, r), 0)) vs
        getW r1 r2 dist = fromMaybe (maxBound `div` 3) $ dist !? (r1, r2)
        fw = foldl' ks dist vs
          where ks dist' k = foldl' is dist' vs
                  where is dist' i = foldl' js dist' vs
                          where js dist' j =
                                  let dIJ = getW i j dist'
                                      dIK = getW i k dist'
                                      dKJ = getW k j dist'
                                  in if dIJ > dIK + dKJ then insert (i, j) (dIK + dKJ) dist' else dist'

--

{-*
[("AA",( 0,[("DD",1),("II",1),("BB",1)]))
,("BB",(13,[("CC",1),("AA",1)]))
,("CC",( 2,[("DD",1),("BB",1)]))
,("DD",(20,[("CC",1),("AA",1),("EE",1)]))
,("EE",( 3,[("FF",1),("DD",1)]))
,("FF",( 0,[("EE",1),("GG",1)]))
,("GG",( 0,[("FF",1),("HH",1)]))
,("HH",(22,[("GG",1)]))
,("II",( 0,[("AA",1),("JJ",1)]))
,("JJ",(21,[("II",1)]))
]

elided 0's : fromList
[("AA",(0,[("DD",1),("JJ",2),("BB",1)]))
,("BB",(13,[("CC",1),("AA",1)]))
,("CC",(2,[("DD",1),("BB",1)]))
,("DD",(20,[("CC",1),("AA",1),("EE",1)]))
,("EE",(3,[("HH",3),("DD",1)]))
,("HH",(22,[("EE",3)]))
,("JJ",(21,[("AA",2)]))
]

fw = fromList
[(("AA","AA"),0)
,(("AA","BB"),1)
,(("AA","CC"),2)
,(("AA","DD"),1)
,(("AA","EE"),2)
,(("AA","HH"),5)
,(("AA","JJ"),2)
,(("BB","AA"),1)
,(("BB","BB"),0)
,(("BB","CC"),1)
,(("BB","DD"),2)
,(("BB","EE"),3)
,(("BB","HH"),6)
,(("BB","JJ"),3)
,(("CC","AA"),2)
,(("CC","BB"),1)
,(("CC","CC"),0)
,(("CC","DD"),1)
,(("CC","EE"),2)
,(("CC","HH"),5)
,(("CC","JJ"),4)
,(("DD","AA"),1)
,(("DD","BB"),2)
,(("DD","CC"),1)
,(("DD","DD"),0)
,(("DD","EE"),1)
,(("DD","HH"),4)
,(("DD","JJ"),3)
,(("EE","AA"),2)
,(("EE","BB"),3)
,(("EE","CC"),2)
,(("EE","DD"),1)
,(("EE","EE"),0)
,(("EE","HH"),3)
,(("EE","JJ"),4)
,(("HH","AA"),5)
,(("HH","BB"),6)
,(("HH","CC"),5)
,(("HH","DD"),4)
,(("HH","EE"),3)
,(("HH","HH"),0)
,(("HH","JJ"),7)
,(("JJ","AA"),2)
,(("JJ","BB"),3)
,(("JJ","CC"),4)
,(("JJ","DD"),3)
,(("JJ","EE"),4)
,(("JJ","HH"),7)
,(("JJ","JJ"),0)
]

*-}

-- Not pretty, but it works.
elideZeroFlowRates :: Map String RoomConfig -> Map String RoomConfig
elideZeroFlowRates ss = foldlWithKey' elideZeroFlowRates' ss ss
  where elideZeroFlowRates' :: Map String RoomConfig -> String -> RoomConfig -> Map String RoomConfig
        elideZeroFlowRates' acc r (0, [_, _]) = attachRooms r (acc !? r) acc
        elideZeroFlowRates' acc _ _ = acc
        attachRooms :: String -> Maybe RoomConfig -> Map String RoomConfig -> Map String RoomConfig
        attachRooms _ Nothing acc = acc
        attachRooms r (Just (0, [(j1, d1), (j2, d2)])) acc =
          let (f1, js1) = acc ! j1
              (f2, js2) = acc ! j2
              js1' = map (updateDist' j1) js1
              js2' = map (updateDist' j2) js2
          in delete r . insert j2 (f2, js2') . insert j1 (f1, js1') $ acc
          where updateDist' rx jc@(jx, dx)
                  | jx == r && rx == j2 = (j1, dx + d1)
                  | jx == r && rx == j1 = (j2, dx + d2)
                  | otherwise = jc

{-* visits.dfs:

[("JJ",27,567,["JJ","AA"])
,("HH",24,528,["HH","AA"])
,("EE",27,81,["EE","AA"])
,("DD",28,560,["DD","AA"])
,("CC",27,54,["CC","AA"])
,("BB",28,364,["BB","AA"])
,("AA",30,0,["AA"])
]
*-}

type SearchQueue = (Int, (String, Int, [String])) -- (Rate calculated, (Current room, Time remaining, [Open-valves]))

visits :: Map String RoomConfig -> Map (String, String) Int -> Int
visits es fws = fst . maximum $ dfs (0, ("AA", 30, ["AA"])) []
  where dfs :: SearchQueue -> [SearchQueue] -> [SearchQueue]
        dfs q@(rt, (r, tm, js)) acc
          | tm >= 2 && size dests > 0 = foldlWithKey' (dfs' q) acc dests
          | otherwise                 = (rt, (r, tm - 1, js)):acc
          where dests = filterWithKey (\(src, dest) _ -> src == r && dest /= r && dest `notElem` js) fws
        dfs' :: SearchQueue -> [SearchQueue] -> (String, String) -> Int -> [SearchQueue]
        dfs' q@(rt, (r, tm, js)) acc (_, d1) dx =
          let tm' = tm - dx - 1
              (fl, _)  = es ! d1
          in dfs (rt + tm' * fl, (d1, tm', d1:js)) acc

solve :: Either a [Room] -> Int
solve (Right ss) = visits es fw
  where es = elideZeroFlowRates . fromList $ ss
        fw = floydWarshall es


main :: IO ()
main = interact $ show . solve . runParser rooms "<stdin>"

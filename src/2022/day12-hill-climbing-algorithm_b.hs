module Main where
import Debug.Trace (trace)
import Data.Array (Array, assocs, bounds, inRange, listArray, (!))
import Data.Set (Set, deleteFindMin, empty, insert, member, notMember, singleton)

--	As you walk up the hill, you suspect that the Elves will want to turn this into a hiking trail. The
--	beginning isn't very scenic, though; perhaps you can find a better starting point.
--
--	To maximize exercise while hiking, the trail should start as low as possible: elevation a. The goal is
--	still the square marked E. However, the trail should still be direct, taking the fewest steps to reach
--	its goal. So, you'll need to find the shortest path from any square at elevation a to the square
--	marked E.
--
--	... This path reaches the goal in only 29 steps, the fewest possible.
--
--	What is the fewest steps required to move starting from any square with elevation a to the location that should get the best signal?
--
-- Test:
--	% cd ../.. && echo -e 'Sabqponm\nabcryxxl\naccszExk\nacctuvwj\nabdefghi' | cabal run 2022_day12-hill-climbing-algorithm_b ## 29

-- Algorithm: https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way

mkGrid :: [String] -> (Point, Point, Array Point Char)
mkGrid cs = (b, e, listArray ((0, 0), (ly, lx)) $ reverse hs)
  where ((_, b, e), hs) = foldl' traverseYs (((0,0), (-10,-10), (-5, -5)), []) cs
        ly = pred . length $ cs
        lx = pred . length . head $ cs
        traverseYs acc xs =
          let (((y, _), s, e), map) = foldl' traverseXs acc xs
          in (((y + 1, 0), s, e), map)
        traverseXs ((pt@(y, x), s, e), map) ch =
          let (val, s', e')
                | 'S' == ch = ('a', pt, e)
                | 'E' == ch = ('z', s,  pt)
                | otherwise = (ch,  s,  e)
              acc = (((y, x + 1), s', e'), val:map)
          in acc

dijkstra :: (Point, Point, Array Point Char) -> Int
dijkstra (b, e, hmap) = dijkstra' empty $ singleton (0, b)
  where dijkstra' :: Set Point -> Set (Int, Point) -> Int
        dijkstra' vps costs | costs == empty = maxBound -- Some trails seem to hit a deadend, i.e. no next steps to take.
        dijkstra' vps costs = nextStep (r, pt) costs'
          where ((r, pt), costs') = deleteFindMin costs
                curHsucc = succ $ hmap ! pt
                nextStep (r, pt) costs
                  | pt == e           = r
                  | pt `member` vps = dijkstra' vps costs
                  | otherwise         = dijkstra' (insert pt vps) neighbors
                --
                -- Upto 4 neighbors per `dijkstra` visit point.
                neighbors = foldl' (flip insert) costs' [(r + 1, pt) | pt <- findNeighbors pt, pt `notMember` vps]
                findNeighbors (y, x) =
                  [ neighbor | neighbor <- [(y - 1, x), (y + 1, x), (y, x + 1), (y, x - 1)]
                             , inRange (bounds hmap) neighbor
                             , hmap ! neighbor <= curHsucc ]

solve :: (Point, Point, Array Point Char) -> Int
solve (b, e, hmap) = minimum . foldl' solve' [] $ assocs hmap
  where solve' acc (pt, 'a') = dijkstra (pt, e, hmap):acc -- 'a' marks a lowest point, a potential starting point
        solve' acc _ = acc -- skip heights > 'a'


main :: IO ()
main = interact $ show . solve . mkGrid . lines

{-# LANGUAGE MultiWayIf #-}
module Main where
import Data.Bool (bool)
import Data.Char (isDigit)
import Data.List (elemIndex, filter, map, sort)
import Data.Map.Strict ((!), Map, fromList, mapWithKey, toList)
import Data.Tuple (swap)

--	... First, you need to apply the decryption key, 811589153. Multiply each number by the decryption key
--	before you begin; this will produce the actual list of numbers to mix.
--
--	Second, you need to mix the list of numbers ten times. The order in which the numbers are mixed does
--	not change during mixing; the numbers are still moved in the order they appeared in the original,
--	pre-mixed list. (So, if -3 appears fourth in the original list of numbers to mix, -3 will be the
--	fourth number to move during each round of mixing.)
--
--	... The grove coordinates can still be found in the same way. Here, the 1000th number after 0 is
--	811589153, the 2000th is 2434767459, and the 3000th is -1623178306; adding these together produces
--	1623178306.
--
--	Apply the decryption key and mix your encrypted file ten times. What is the sum of the three numbers
--	that form the grove coordinates?
--
-- Test:
--	% cd ../.. && echo -e '1\n2\n-3\n3\n-2\n0\n4' | cabal run 2022_day20-grove-positioning-system_b # = 1623178306

str2int :: String -> Int
str2int = read . filter (\c -> isDigit c || '-' == c)

solve :: [Int] -> Int
solve is' = sum . map (\v -> snd $ ns' ! (((ns ! z0) + v) `mod` l)) $ [1000, 2000, 3000]
  where Just zo = elemIndex 0 is
        z0  = (zo, 0)
        is  = map (811589153 *) is' -- you need to apply the decryption key, 811589153
        l   = length is
        iso = zip [0..] is -- `is` tagged with original order (puzzle input has dupes, grrrr)
        ns  = foldl' move (fromList . zip iso $ [0..]) (take (l * 10) . cycle $ iso)
        ns' = fromList . map swap . toList $ ns
        --
        move :: Map (Int, Int) Int -> (Int, Int) -> Map (Int, Int) Int
        move acc i@(_, i') =
          let old  = acc ! i
              m    = i' `mod` (l - 1)
              new  = (old + m) `mod` (l - 1)
          in move' i old new acc
        move' i old new acc
          | old == new = acc
          | new > old  =
              mapWithKey (\k v -> if | k == i    -> new
                                     | v < old   -> v
                                     | v > new   -> v
                                     | otherwise -> v - 1) acc
          | new < old  =
              mapWithKey (\k v -> if | k == i    -> new
                                     | v < new   -> v
                                     | v >= old  -> v
                                     | otherwise -> v + 1) acc


main :: IO ()
main = interact $ show . solve . map str2int . words

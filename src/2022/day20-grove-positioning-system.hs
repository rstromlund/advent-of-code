{-# LANGUAGE MultiWayIf #-}
module Main where
import Data.Bool (bool)
import Data.Char (isDigit)
import Data.List (elemIndex, filter, map, sort)
import Data.Map.Strict ((!), Map, fromList, mapWithKey, toList)
import Data.Tuple (swap)

--	... The encrypted file is a list of numbers. To mix the file, move each number forward or backward in
--	the file a number of positions equal to the value of the number being moved. The list is circular, so
--	moving a number off one end of the list wraps back around to the other end as if the ends were
--	connected.
--
--	For example, to move the 1 in a sequence like 4, 5, 6, 1, 7, 8, 9, the 1 moves one position forward:
--	4, 5, 6, 7, 1, 8, 9. To move the -2 in a sequence like 4, -2, 5, 6, 7, 8, 9, the -2 moves two
--	positions backward, wrapping around: 4, 5, 6, 7, 8, -2, 9.
--
--	The numbers should be moved in the order they originally appear in the encrypted file. Numbers moving
--	around during the mixing process do not change the order in which the numbers are moved.
--
--	Consider this encrypted file:
--
--	1
--	2
--	-3
--	3
--	-2
--	0
--	4
--
--	Mixing this file proceeds as follows:
--
--	Initial arrangement:
--	1, 2, -3, 3, -2, 0, 4
--
--	1 moves between 2 and -3:
--	2, 1, -3, 3, -2, 0, 4
--
--	2 moves between -3 and 3:
--	1, -3, 2, 3, -2, 0, 4
--
--	-3 moves between -2 and 0:
--	1, 2, 3, -2, -3, 0, 4
--
--	3 moves between 0 and 4:
--	1, 2, -2, -3, 0, 3, 4
--
--	-2 moves between 4 and 1:
--	1, 2, -3, 0, 3, 4, -2
--
--	0 does not move:
--	1, 2, -3, 0, 3, 4, -2
--
--	4 moves between -3 and 0:
--	1, 2, -3, 4, 0, 3, -2
--
--	Then, the grove coordinates can be found by looking at the 1000th, 2000th, and 3000th numbers after
--	the value 0, wrapping around the list as necessary. In the above example, the 1000th number after 0 is
--	4, the 2000th is -3, and the 3000th is 2; adding these together produces 3.
--
--	Mix your encrypted file exactly once. What is the sum of the three numbers that form the grove coordinates?
--
-- Test:
--	% cd ../.. && echo -e '1\n2\n-3\n3\n-2\n0\n4' | cabal run 2022_day20-grove-positioning-system # = 3

str2int :: String -> Int
str2int = read . filter (\c -> isDigit c || '-' == c)

solve :: [Int] -> Int
solve is = sum . map (\v -> snd $ ns' ! (((ns ! z0) + v) `mod` l)) $ [1000, 2000, 3000]
  where Just zo = elemIndex 0 is
        z0  = (zo, 0)
        l   = length is
        iso = zip [0..] is -- `is` tagged with original order (puzzle input has dupes, grrrr)
        ns  = foldl' move (fromList . zip iso $ [0..]) iso
        ns' = fromList . map swap . toList $ ns
        --
        move :: Map (Int, Int) Int -> (Int, Int) -> Map (Int, Int) Int
        move acc i@(_, i') =
          let old  = acc ! i
              m    = i' `mod` (l - 1)
              new  = (old + m) `mod` (l - 1)
          in move' i old new acc
        move' i old new acc
          | old == new = acc
          | new > old  =
              mapWithKey (\k v -> if | k == i    -> new
                                     | v < old   -> v
                                     | v > new   -> v
                                     | otherwise -> v - 1) acc
          | new < old  =
              mapWithKey (\k v -> if | k == i    -> new
                                     | v < new   -> v
                                     | v >= old  -> v
                                     | otherwise -> v + 1) acc


main :: IO ()
main = interact $ show . solve . map str2int . words

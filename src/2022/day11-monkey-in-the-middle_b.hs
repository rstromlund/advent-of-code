module Main where
import Data.Bool (bool)
import Data.List (sortBy, splitAt)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, oneOf, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... You're worried you might not ever get your items back. So worried, in fact, that your relief that
--	a monkey's inspection didn't damage an item no longer causes your worry level to be divided by three.
--
--	Unfortunately, that relief was all that was keeping your worry levels from reaching ridiculous
--	levels. You'll need to find another way to keep your worry levels manageable.
--
--	At this rate, you might be putting up with these monkeys for a very long time - possibly 10000 rounds!
--
--	With these new rules, you can still figure out the monkey business after 10000 rounds. Using the same example above:
--	...
--	== After round 10000 ==
--	Monkey 0 inspected items 52166 times.
--	Monkey 1 inspected items 47830 times.
--	Monkey 2 inspected items 1938 times.
--	Monkey 3 inspected items 52013 times.
--
--	After 10000 rounds, the two most active monkeys inspected items 52166 and 52013 times. Multiplying
--	these together, the level of monkey business in this situation is now 2713310158.
--
--	Worry levels are no longer divided by three after each item is inspected; you'll need to find another
--	way to keep your worry levels manageable. Starting again from the initial state in your puzzle input,
--	what is the level of monkey business after 10000 rounds?
--
-- Test:
--	% cd ../.. && echo -e 'Monkey 0:\n  Starting items: 79, 98\n  Operation: new = old * 19\n  Test: divisible by 23\n    If true: throw to monkey 2\n    If false: throw to monkey 3\n\nMonkey 1:\n  Starting items: 54, 65, 75, 74\n  Operation: new = old + 6\n  Test: divisible by 19\n    If true: throw to monkey 2\n    If false: throw to monkey 0\n\nMonkey 2:\n  Starting items: 79, 60, 97\n  Operation: new = old * old\n  Test: divisible by 13\n    If true: throw to monkey 1\n    If false: throw to monkey 3\n\nMonkey 3:\n  Starting items: 74\n  Operation: new = old + 3\n  Test: divisible by 17\n    If true: throw to monkey 0\n    If false: throw to monkey 1' | cabal run 2022_day11-monkey-in-the-middle_b # = 2713310158

data Operation = Plus Int | Times Int | Squared deriving (Show)

calcOp (Plus x)  old = old + x
calcOp (Times x) old = old * x
calcOp Squared   old = old * old


data Note = Note
  {
    nMonkey :: Int
  , nItems :: [Int]
  , nOp :: Operation
  , nDiv :: Int
  , nTrue :: Int
  , nFalse :: Int
  , nState :: Int
  } deriving (Show)


type Parser = Parsec Void String

notes :: Parser [Note]
notes = note `sepBy` char '\n' <* optional newline <* eof

note :: Parser Note
note = do
  m  <- monkey
  is <- items
  o  <- op
  d  <- divisible
  t  <- ifexp
  f  <- ifexp
  return Note{nMonkey = m, nItems = is, nOp = o, nDiv = d, nTrue = t, nFalse = f, nState = 0}

monkey :: Parser Int
monkey = string "Monkey " *> decimal <* string ":\n"

items :: Parser [Int]
items = string "  Starting items:" *> (char ' ' *> decimal) `sepBy` char ',' <* newline

op :: Parser Operation
op = string "  Operation: new = old " *> choice
  [ Plus    <$> (string "+ " *> decimal)
  , Squared <$  string "* old"
  , Times   <$> (string "* " *> decimal)
  ] <* newline

divisible :: Parser Int
divisible = string "  Test: divisible by " *> decimal <* newline

ifexp :: Parser Int
ifexp = string "    If " *> (string "true" <|> string "false") *> string ": throw to monkey " *> decimal <* newline

--

rounds = 10000

solve :: Either a [Note] -> Int
solve (Right ns) = score . foldl' round ns $ [1..rounds]
  where round ns _ = foldl' mbusiness ns [0..length ns - 1]
        mbusiness :: [Note] -> Int -> [Note]
        mbusiness ns mnbr =
          let m = ns !! mnbr
              ns' = inspects m ns
          in ns'
        limiter = product . map nDiv $ ns -- worry limiter ... let's keep this in the Int range please!
        inspects m@Note{nMonkey = mnbr, nItems = []} ns = replaceAt mnbr m ns
        inspects m@Note{nMonkey = mnbr, nItems = (i:is), nOp = op, nDiv = d, nTrue = nT, nFalse = nF, nState = cnt} ns =
          let wl  = calcOp op i `mod` limiter
              rem = wl `mod` d
              nm  = bool nF nT $ 0 == rem
              t@Note{nItems = ti} = ns !! nm
              t'  = t{nItems = ti ++ [wl]}
          in inspects m{nItems = is, nState = cnt + 1} $ replaceAt nm t' ns
        replaceAt i x xs =
          let (b, a) = splitAt i xs
          in b ++ (x:tail a)
        score = product . take 2 . sortBy (flip compare) . map nState


main :: IO ()
main = interact $ show . solve . runParser notes "<stdin>"

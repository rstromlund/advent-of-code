module Main where
import Data.Bool (bool)
import Data.Char (isDigit, isSpace)
import Data.List as L (groupBy, length, map, span, takeWhile)
import Data.Maybe (fromMaybe)
import Data.Vector as V ((!), (!?), (//), Vector, findIndex, fromList, length, map, reverse, takeWhile)

--	... You approach the strange input device, but it isn't quite what the monkeys drew in their
--	notes. Instead, you are met with a large cube; each of its six faces is a square of 50x50 tiles.
--
--	To be fair, the monkeys' map does have six 50x50 regions on it. If you were to carefully fold the map, you should be able to shape it into a cube!
--
--	In the example above, the six (smaller, 4x4) faces of the cube are:
--
--	        1111
--	        1111
--	        1111
--	        1111
--	222233334444
--	222233334444
--	222233334444
--	222233334444
--	        55556666
--	        55556666
--	        55556666
--	        55556666
--
--	You still start in the same position and with the same facing as before, but the wrapping rules are
--	different. Now, if you would walk off the board, you instead proceed around the cube. From the
--	perspective of the map, this can look a little strange. In the above example, if you are at A and move
--	to the right, you would arrive at B facing down; if you are at C and move down, you would arrive at D
--	facing up:
--
--	        ...#
--	        .#..
--	        #...
--	        ....
--	...#.......#
--	........#..A
--	..#....#....
--	.D........#.
--	        ...#..B.
--	        .....#..
--	        .#......
--	        ..C...#.
--
--	Walls still block your path, even if they are on a different face of the cube. If you are at E facing
--	up, your movement is blocked by the wall marked by the arrow:
--
--	        ...#
--	        .#..
--	     -->#...
--	        ....
--	...#..E....#
--	........#...
--	..#....#....
--	..........#.
--	        ...#....
--	        .....#..
--	        .#......
--	        ......#.
--
--	Using the same method of drawing the last facing you had with an arrow on each tile you visit, the
--	full path taken by the above example now looks like this:
--
--	        >>v#
--	        .#v.
--	        #.v.
--	        ..v.
--	...#..^...v#
--	.>>>>>^.#.>>
--	.^#....#....
--	.^........#.
--	        ...#..v.
--	        .....#v.
--	        .#v<<<<.
--	        ..v...#.
--
--	The final password is still calculated from your final position and facing from the perspective of the
--	map. In this example, the final row is 5, the final column is 7, and the final facing is 3, so the
--	final password is 1000 * 5 + 4 * 7 + 3 = 5031.
--
--	Fold the map into a cube, then follow the path given in the monkeys' notes. What is the final password?
--
-- Test:
--	% cd ../.. && echo -e '        ...#\n        .#..\n        #...\n        ....\n...#.......#\n........#...\n..#....#....\n..........#.\n        ...#....\n        .....#..\n        .#......\n        ......#.\n\n10R5L5R10L4R5L5' | cabal run 2022_day22-monkey-map_b # = 5031

data Instruction = TurnRight | TurnLeft | Walk Int deriving (Show, Ord, Eq)

type State = (Int, ((Int, Int), (Int, Int))) -- Face, ((Y, X), (dY, dX))

dRight = ( 0,  1)
dLeft  = ( 0, -1)
dDown  = ( 1,  0)
dUp    = (-1,  0)

parse :: [Char] -> [Instruction]
parse ('L':is) = TurnLeft:parse is
parse ('R':is) = TurnRight:parse is
parse []       = []
parse is       = (\(n,is') -> (Walk . read $ n):parse is') . span isDigit $ is

warpFromLeft :: Vector Char -> Int
warpFromLeft = fromMaybe minBound . V.findIndex (not . isSpace)

warpFromRight :: Vector Char -> Int
warpFromRight = flip (-) 1 . V.length

warpFromTop :: Int -> Vector  (Vector Char) -> Int
warpFromTop x = warpFromLeft . V.map (\y -> fromMaybe ' ' $ y !? x)

warpFromBottom :: Int -> Vector  (Vector Char) -> Int
warpFromBottom x jm = (-) (V.length jm - 1) . warpFromLeft . V.map (\y -> fromMaybe ' ' $ y !? x) . V.reverse $ jm

turn TurnRight d
  | d == dRight = dDown
  | d == dDown  = dLeft
  | d == dLeft  = dUp
  | d == dUp    = dRight
turn TurnLeft d = turn TurnRight . turn TurnRight . turn TurnRight $ d

{-*
There's some kudgery/hackery going on here.  Instead of writing a generalized folding figure-out'er; I hard
coded the wrapping need for the test puzzle and *my* puzzle.  Unfortunately the 2d shape from test to puzzle
didn't match up ... did it for anyone?  Probably not.  :/

So if the vector matches the test data then 4x4 grids are used with the given 2d layout; else a 50x50 grid is
used with *my* puzzle 2d layout.  This may not work with your puzzle input, YMMV.

TODO: I don't know if I want to come back and generalize this solution or not.  Maybe there is a smarter
"modulus" way to do this. (?)

I wrote a test suite to check if all my wrapping code brought me back to the same place, not a slam dunk that
the code is correct; but it is a great sanity check. : )
*-}

sideDim jm = bool 50 4 $ (3 * 4) == V.length jm
face jm 1 = bool (0 * 4, 2 * 4) (0 * 50, 1 * 50) $ 50 == sideDim jm
face jm 2 = bool (1 * 4, 0 * 4) (0 * 50, 2 * 50) $ 50 == sideDim jm
face jm 3 = bool (1 * 4, 1 * 4) (1 * 50, 1 * 50) $ 50 == sideDim jm
face jm 4 = bool (1 * 4, 2 * 4) (2 * 50, 0 * 50) $ 50 == sideDim jm
face jm 5 = bool (2 * 4, 2 * 4) (2 * 50, 1 * 50) $ 50 == sideDim jm
face jm 6 = bool (2 * 4, 3 * 4) (3 * 50, 0 * 50) $ 50 == sideDim jm

getCh :: Vector  (Vector Char) -> State -> Char
getCh jm (f, ((y, x), _)) = let (yo, xo) = face jm f in fromMaybe ' ' $ (jm ! (y + yo)) !? (x + xo)

nextState'4 :: Vector  (Vector Char) -> State -> State
nextState'4 jm st@(f, (pt@(y, x), dpt@(dy, dx))) = warp (f, ((y + dy, x + dx), dpt))
  where -- face 1
        warp (1, ((y', x'), dRight)) | x' >= sideDim jm = (6, morphRightToLeft)
        warp (6, ((y', x'), dRight)) | x' >= sideDim jm = (1, morphRightToLeft)
        --
        warp (1, ((y', x'), dUp))    | y' < 0           = (2, morphUpToDown)
        warp (2, ((y', x'), dUp))    | y' < 0           = (1, morphUpToDown)
        --
        warp (1, ((y', x'), dLeft))  | x' < 0           = (3, morphLeftToDown)
        warp (3, ((y', x'), dUp))    | y' < 0           = (1, morphUpToRight)
        --
        warp (1, ((y', x'), dDown))  | y' >= sideDim jm = (4, morphDownToDown)
        warp (4, ((y', x'), dUp))    | y' < 0           = (1, morphUpToUp)
        -- face 2
        warp (2, ((y', x'), dDown))  | y' >= sideDim jm = (5, morphDownToUp)
        warp (5, ((y', x'), dDown))  | y' >= sideDim jm = (2, morphDownToUp)
        --
        warp (2, ((y', x'), dLeft))  | x' < 0           = (6, morphLeftToUp)
        warp (6, ((y', x'), dDown))  | y' >= sideDim jm = (2, morphDownToRight)
        --
        warp (2, ((y', x'), dRight)) | x' >= sideDim jm = (3, morphRightToRight)
        warp (3, ((y', x'), dLeft))  | x' < 0           = (2, morphLeftToLeft)
        -- face 3
        warp (3, ((y', x'), dDown))  | y' >= sideDim jm = (5, morphDownToRight)
        warp (5, ((y', x'), dLeft))  | x' < 0           = (3, morphLeftToUp)
        --
        warp (3, ((y', x'), dRight)) | x' >= sideDim jm = (4, morphRightToRight)
        warp (4, ((y', x'), dLeft))  | x' < 0           = (3, morphLeftToLeft)
        -- face 4
        warp (4, ((y', x'), dRight)) | x' >= sideDim jm = (6, morphRightToDown)
        warp (6, ((y', x'), dUp))    | y' < 0           = (4, morphUpToLeft)
        --
        warp (4, ((y', x'), dDown))  | y' >= sideDim jm = (5, morphDownToDown)
        warp (5, ((y', x'), dUp))    | y' < 0           = (4, morphUpToUp)
        -- face 5
        warp (5, ((y', x'), dRight)) | x' >= sideDim jm = (6, morphRightToRight)
        warp (6, ((y', x'), dLeft))  | x' < 0           = (5, morphLeftToLeft)
        --
        warp st' = st'
        --
        morphUpToDown     = ((                 0,                  x), dDown)  -- 1 to 2
        morphLeftToDown   = ((                 0,                  y), dDown)  -- 1 to 3
        morphDownToDown   = ((                 0,                  x), dDown)  -- 1 to 4
        morphRightToLeft  = ((sideDim jm - y - 1,     sideDim jm - 1), dLeft)  -- 1 to 6
        morphRightToRight = ((                 y,                  0), dRight) -- 2 to 3
        morphDownToUp     = ((    sideDim jm - 1, sideDim jm - x - 1), dUp)    -- 2 to 5
        morphLeftToUp     = ((    sideDim jm - 1, sideDim jm - y - 1), dUp)    -- 2 to 6
        morphUpToRight    = ((                 x,                  0), dRight) -- 3 to 1
        morphLeftToLeft   = ((                 y,     sideDim jm - 1), dLeft)  -- 3 to 2
        morphDownToRight  = ((sideDim jm - x - 1,                  0), dRight) -- 3 to 5
        morphUpToUp       = ((    sideDim jm - 1,                  x), dUp)    -- 4 to 1
        morphRightToDown  = ((                 0, sideDim jm - y - 1), dDown)  -- 4 to 6
        morphUpToLeft     = ((sideDim jm - x - 1,     sideDim jm - 1), dLeft)  -- 6 to 4

nextState'50 :: Vector  (Vector Char) -> State -> State
nextState'50 jm st@(f, (pt@(y, x), dpt@(dy, dx))) = warp (f, ((y + dy, x + dx), dpt))
  where -- face 1
        warp (1, ((y', x'), dRight)) | x' >= sideDim jm = (2, morphRightToRight)
        warp (2, ((y', x'), dLeft))  | x' < 0           = (1, morphLeftToLeft)
        --
        warp (1, ((y', x'), dDown))  | y' >= sideDim jm = (3, morphDownToDown)
        warp (3, ((y', x'), dUp))    | y' < 0           = (1, morphUpToUp)
        --
        warp (1, ((y', x'), dLeft))  | x' < 0           = (4, morphLeftToRight)
        warp (4, ((y', x'), dLeft))  | x' < 0           = (1, morphLeftToRight)
        --
        warp (1, ((y', x'), dUp))    | y' < 0           = (6, morphUpToRight)
        warp (6, ((y', x'), dLeft))  | x' < 0           = (1, morphLeftToDown)
        -- face 2
        warp (2, ((y', x'), dRight)) | x' >= sideDim jm = (5, morphRightToLeft)
        warp (5, ((y', x'), dRight)) | x' >= sideDim jm = (2, morphRightToLeft)
        --
        warp (2, ((y', x'), dDown))  | y' >= sideDim jm = (3, morphDownToLeft)
        warp (3, ((y', x'), dRight)) | x' >= sideDim jm = (2, morphRightToUp)
        --
        warp (2, ((y', x'), dUp))    | y' < 0           = (6, morphUpToUp)
        warp (6, ((y', x'), dDown))  | y' >= sideDim jm = (2, morphDownToDown)
        -- face 3
        warp (3, ((y', x'), dDown))  | y' >= sideDim jm = (5, morphDownToDown)
        warp (5, ((y', x'), dUp))    | y' < 0           = (3, morphUpToUp)
        --
        warp (3, ((y', x'), dLeft))  | x' < 0           = (4, morphLeftToDown)
        warp (4, ((y', x'), dUp))    | y' < 0           = (3, morphUpToRight)
        -- face 4
        warp (4, ((y', x'), dRight)) | x' >= sideDim jm = (5, morphRightToRight)
        warp (5, ((y', x'), dLeft))  | x' < 0           = (4, morphLeftToLeft)
        --
        warp (4, ((y', x'), dDown))  | y' >= sideDim jm = (6, morphDownToDown)
        warp (6, ((y', x'), dUp))    | y' < 0           = (4, morphUpToUp)
        -- face 5
        warp (5, ((y', x'), dDown))  | y' >= sideDim jm = (6, morphDownToLeft)
        warp (6, ((y', x'), dRight)) | x' >= sideDim jm = (5, morphRightToUp)
        --
        warp st' = st'
        --
        morphDownToDown   = ((                 0,                  x), dDown)
        morphDownToLeft   = ((                 x,     sideDim jm - 1), dLeft)  -- 2 to 3
        morphLeftToDown   = ((                 0,                  y), dDown)
        morphLeftToLeft   = ((                 y,     sideDim jm - 1), dLeft)
        morphLeftToRight  = ((sideDim jm - y - 1,                  0), dRight) -- 1 to 4
        morphRightToLeft  = ((sideDim jm - y - 1,     sideDim jm - 1), dLeft)
        morphRightToRight = ((                 y,                  0), dRight)
        morphRightToUp    = ((    sideDim jm - 1,                  y), dUp)    -- 3 to 2
        morphUpToRight    = ((                 x,                  0), dRight) -- 1 to 6
        morphUpToUp       = ((    sideDim jm - 1,                  x), dUp)

nextState :: Vector  (Vector Char) -> State -> State
nextState jm st@(f, (pt@(y, x), dpt@(dy, dx))) = bool (nextState'4 jm st) (nextState'50 jm st) $ 50 == sideDim jm

walk :: Vector  (Vector Char) -> [Instruction] -> State -> State
walk _  [] st = st
walk jm (TurnRight:is) st@(f, (pt, dpt)) = walk jm is (f, (pt, turn TurnRight dpt))
walk jm (TurnLeft:is)  st@(f, (pt, dpt)) = walk jm is (f, (pt, turn TurnLeft  dpt))
walk jm (Walk n:is)    st                = walk jm is . walk' n $ st
  where walk' 0 st = st
        walk' n st@(f, (pt, dpt)) =
          let st' = nextState jm st
              c   = getCh jm st'
          in if '#' == c then st else walk' (n - 1) st'

-- Facing is 0 for right (>), 1 for down (v), 2 for left (<), and 3 for up (^)
facing d
  | d == dRight = 0
  | d == dDown  = 1
  | d == dLeft  = 2
  | d == dUp    = 3

solve :: [[String]] -> Int
solve [ms, [_, is]] = score . walk jm (parse is) $ initState
  where jm        = V.fromList . L.map V.fromList $ ms -- jm (jungle map) is a vector of vectors
        initState = (1, ((0, 0), dRight))
        score st@(f, ((y, x), dpt)) = let (yo, xo) = face jm f in (1000 * (y + yo + 1)) + (4 * (x + xo + 1)) + facing dpt


main :: IO ()
main = interact $ show . solve . groupBy (\a b -> "" /= b) . lines

module Main where

--	... "The first column is what your opponent is going to play: A for Rock, B for Paper, and C for
--	Scissors. The second column--" Suddenly, the Elf is called away to help with someone's tent.
--
--	The second column, you reason, must be what you should play in response: X for Rock, Y for Paper, and
--	Z for Scissors. Winning every time would be suspicious, so the responses must have been carefully
--	chosen.
--
--	The winner of the whole tournament is the player with the highest score. Your total score is the sum
--	of your scores for each round. The score for a single round is the score for the shape you selected (1
--	for Rock, 2 for Paper, and 3 for Scissors) plus the score for the outcome of the round (0 if you lost,
--	3 if the round was a draw, and 6 if you won).
--
--	Since you can't be sure if the Elf is trying to help you or trick you, you should calculate the score
--	you would get if you were to follow the strategy guide.
--
--	For example, suppose you were given the following strategy guide:
--
--	A Y
--	B X
--	C Z
--
--	This strategy guide predicts and recommends the following:
--
--	* In the first round, your opponent will choose Rock (A), and you should choose Paper (Y). This ends
--	  in a win for you with a score of 8 (2 because you chose Paper + 6 because you won).
--	* In the second round, your opponent will choose Paper (B), and you should choose Rock (X). This ends
--	  in a loss for you with a score of 1 (1 + 0).
--	* The third round is a draw with both players choosing Scissors, giving you a score of 3 + 3 = 6.
--
--	In this example, if you were to follow the strategy guide, you would get a total score of 15 (8 + 1 + 6).
--
--	What would your total score be if everything goes exactly according to your strategy guide?
--
-- Test:
--	% cd ../.. && echo -e 'A Y\nB X\nC Z' | cabal run 2022_day02-rock-paper-scissors # = 15

data RPS = Rock | Paper | Scissors

selectP1 'A' = Rock
selectP1 'B' = Paper
selectP1 'C' = Scissors

selectP2 'X' = Rock
selectP2 'Y' = Paper
selectP2 'Z' = Scissors

myScore :: RPS -> Int
myScore Rock     = 1
myScore Paper    = 2
myScore Scissors = 3

play :: RPS -> RPS -> Int
play Scissors p2@Rock     = 6 + myScore p2
play Rock     p2@Paper    = 6 + myScore p2
play Paper    p2@Scissors = 6 + myScore p2

play Rock     p2@Rock     = 3 + myScore p2
play Paper    p2@Paper    = 3 + myScore p2
play Scissors p2@Scissors = 3 + myScore p2

play _        p2          = myScore p2

playRound [p1, _, p2] = play (selectP1 p1) $ selectP2 p2

main :: IO ()
main = interact $ show . sum . map playRound . lines

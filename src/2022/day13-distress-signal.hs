module Main where
import Data.Ord (Ordering(LT, EQ, GT))
import Data.List.Split (chunksOf)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your list consists of pairs of packets; pairs are separated by a blank line. You need to identify how many pairs of packets are in the right order.
--
--	For example:
--
--	[1,1,3,1,1]
--	[1,1,5,1,1]
--
--	[[1],[2,3,4]]
--	[[1],4]
--
--	[9]
--	[[8,7,6]]
--
--	[[4,4],4,4]
--	[[4,4],4,4,4]
--
--	[7,7,7,7]
--	[7,7,7]
--
--	[]
--	[3]
--
--	[[[]]]
--	[[]]
--
--	[1,[2,[3,[4,[5,6,7]]]],8,9]
--	[1,[2,[3,[4,[5,6,0]]]],8,9]
--
--	Packet data consists of lists and integers. Each list starts with [, ends with ], and contains zero or
--	more comma-separated values (either integers or other lists). Each packet is always a list and appears
--	on its own line.
--
--	When comparing two values, the first value is called left and the second value is called right. Then:
--
--	* If both values are integers, the lower integer should come first. If the left integer is lower than
--	  the right integer, the inputs are in the right order. If the left integer is higher than the right
--	  integer, the inputs are not in the right order. Otherwise, the inputs are the same integer; continue
--	  checking the next part of the input.
--	* If both values are lists, compare the first value of each list, then the second value, and so on. If
--	  the left list runs out of items first, the inputs are in the right order. If the right list runs out
--	  of items first, the inputs are not in the right order. If the lists are the same length and no
--	  comparison makes a decision about the order, continue checking the next part of the input.
--	* If exactly one value is an integer, convert the integer to a list which contains that integer as its
--	  only value, then retry the comparison. For example, if comparing [0,0,0] and 2, convert the right
--	  value to [2] (a list containing 2); the result is then found by instead comparing [0,0,0] and [2].
--
--	Using these rules, you can determine which of the pairs in the example are in the right order:
--
--	...
--
--	What are the indices of the pairs that are already in the right order? (The first pair has index 1,
--	the second pair has index 2, and so on.) In the above example, the pairs in the right order are 1, 2,
--	4, and 6; the sum of these indices is 13.
--
--	Determine which pairs of packets are already in the right order. What is the sum of the indices of
--	those pairs?
--
-- Test:
--	% cd ../.. && echo -e '[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]' | cabal run 2022_day13-distress-signal # = 13
--	% cd ../.. && echo -e '[[],[[[]],9,[],1],[1,0]]\n[[[10,8,0,[4,3],[1,4]],4,[7,5,[9,10,7,10],8]],[8,[[1,5,9,1],6,[10,5],5,[7,1]]],[[0,7,3],[[5],[8,3,0],4]],[4,2]]' | cabal run 2022_day13-distress-signal # = is True

data PuzzleList = PList [PuzzleList] | PInt Int deriving (Show)

type Parser = Parsec Void String

plists :: Parser [PuzzleList]
plists = many (plist <* space) <* eof

plist :: Parser PuzzleList
plist = choice
  [ char '[' *> (PList <$> (plist `sepBy` char ',')) <* char ']'
  , PInt <$> decimal
  ]

--

solve :: Either a [PuzzleList] -> Int
solve (Right ps) = score . map comp $ pairs
  where pairs = chunksOf 2 ps
        score = sum . map fst . filter ((==) LT . snd) . zip [1..]
        --
        -- FIXME: derive Ord and Eq to make PuzzleList a first class datatype?  And just use compare instead : )
        --
        comp [PList [], PList []] = EQ -- exhausted this list; continue on
        comp [PList [], PList (_:_)] = LT  -- If the left list runs out of items first, the inputs are in the right order
        comp [PList (_:_), PList []] = GT -- If the right list runs out of items first, the inputs are not in the right order
        comp [PList ((PInt a):as), PList ((PInt b):bs)] = -- If both values are integers, the lower integer should come first
          let rel = compare a b in if EQ == rel then comp [PList as, PList bs] else rel
        comp [a@(PInt _), b] = comp [PList [a], b] -- If exactly one value is an integer, convert the integer to a list
        comp [a, b@(PInt _)] = comp [a, PList [b]]
        comp [PList (a:as), PList (b:bs)] = -- loop thru lists; short circuit if LT or GT; continue comparing when EQ
          let rel = comp [a, b] in if EQ == rel then comp [PList as, PList bs] else rel


main :: IO ()
main = interact $ show . solve . runParser plists "<stdin>"

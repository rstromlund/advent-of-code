module Main where
import Data.Functor (($>))
import Data.List (transpose)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, noneOf, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... However, the action of moving three crates from stack 1 to stack 3 means that
--	those three moved crates stay in the same order, resulting in this new
--	configuration:
--
--	        [D]
--	        [N]
--	    [C] [Z]
--	    [M] [P]
--	 1   2   3
--
--	Next, as both crates are moved from stack 2 to stack 1, they retain their order as well:
--
--	        [D]
--	        [N]
--	[C]     [Z]
--	[M]     [P]
--	 1   2   3
--
--	Finally, a single crate is still moved from stack 1 to stack 2, but now it's crate C that gets moved:
--
--	        [D]
--	        [N]
--	        [Z]
--	[M] [C] [P]
--	 1   2   3
--
--	In this example, the CrateMover 9001 has put the crates in a totally different order: MCD.
--
--	Before the rearrangement process finishes, update your simulation so that the
--	Elves know where they should stand to be ready to unload the final supplies. After
--	the rearrangement procedure completes, what crate ends up on top of each stack?
--
-- Test:
--	% cd ../.. && echo -e '    [D]    \n[N] [C]    \n[Z] [M] [P]\n 1   2   3 \n\nmove 1 from 2 to 1\nmove 3 from 1 to 3\nmove 2 from 2 to 1\nmove 1 from 1 to 2' | cabal run 2022_day05-supply-stacks_b # = MCD

type Stack = [Char]

-- I'm kind of cheating here, I looked at my input and 9 stacks is the max. I could more flexibly put these
-- in a list or vector but mutating a 9x-tuple seems easier and in constant time/space w/o extra dependencies.
data Stacks = Stacks{ stack1 :: Stack
                    , stack2 :: Stack
                    , stack3 :: Stack
                    , stack4 :: Stack
                    , stack5 :: Stack
                    , stack6 :: Stack
                    , stack7 :: Stack
                    , stack8 :: Stack
                    , stack9 :: Stack
                    } deriving (Show)

mtStacks = Stacks {stack1 = [], stack2 = [], stack3 = [], stack4 = [], stack5 = [], stack6 = [], stack7 = [], stack8 = [], stack9 = []}

setStack 1 stk ss = ss{stack1 = stk}
setStack 2 stk ss = ss{stack2 = stk}
setStack 3 stk ss = ss{stack3 = stk}
setStack 4 stk ss = ss{stack4 = stk}
setStack 5 stk ss = ss{stack5 = stk}
setStack 6 stk ss = ss{stack6 = stk}
setStack 7 stk ss = ss{stack7 = stk}
setStack 8 stk ss = ss{stack8 = stk}
setStack 9 stk ss = ss{stack9 = stk}

getStack 1 ss = stack1 ss
getStack 2 ss = stack2 ss
getStack 3 ss = stack3 ss
getStack 4 ss = stack4 ss
getStack 5 ss = stack5 ss
getStack 6 ss = stack6 ss
getStack 7 ss = stack7 ss
getStack 8 ss = stack8 ss
getStack 9 ss = stack9 ss

mkStacks :: [[Stack]] -> Stacks
mkStacks = foldl' (\acc (i, cs) -> setStack i (concat cs) acc) mtStacks . zip [1..] . transpose

type Move = (Int, Int, Int) -- (moveCnt, fromStack, toStack)
type Moves = [Move]

data Puzzle = Puzzle{pzstacks :: Stacks, pzmoves :: Moves} deriving (Show)

mtPuzzle = Puzzle{pzstacks = mtStacks, pzmoves = []}


type Parser = Parsec Void String

puzzle :: Parser Puzzle
puzzle = do
  ss <- stacks
  _  <- many (noneOf "\n") -- don't need to parse stack numbers
  _  <- newline
  _  <- newline
  ms <- many move
  _  <- optional newline
  _  <- eof
  return Puzzle{pzstacks = ss, pzmoves = ms}

stacks :: Parser Stacks
stacks = mkStacks <$> many boxes

boxes :: Parser [String]
boxes = box `sepBy` char ' ' <* newline

box :: Parser String
box = choice
  [ (:[]) <$> (char '[' *> upperChar <* char ']')
  , string "   " $> []
  ]

move :: Parser Move
move = (,,) <$> (string "move " *> decimal) <*> (string " from " *> decimal) <*> (string " to " *> decimal) <* newline

--

solve :: Either a Puzzle -> String
solve (Right pz) = topBoxes . foldl' moveBox (pzstacks pz) $ pzmoves pz
  where moveBox ss (cnt, fromS, toS) =
          let f = getStack fromS ss
              t = getStack toS   ss
              (ms, f') = splitAt cnt f
          in setStack toS (ms ++ t) . setStack fromS f' $ ss
        topBoxes ss =
          let getStackHead acc n = acc ++ (take 1 . getStack n $ ss)
          in foldl' getStackHead "" [1..9]


main :: IO ()
main = interact $ show . solve . runParser puzzle "<stdin>"

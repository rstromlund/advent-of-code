module Main where
import Data.Char (isDigit, isSpace)
import Data.List as L (groupBy, map, span)
import Data.Maybe (fromMaybe)
import Data.Vector as V ((!), (!?), Vector, findIndex, fromList, length, map, reverse)

--	... For example:
--
--	        ...#
--	        .#..
--	        #...
--	        ....
--	...#.......#
--	........#...
--	..#....#....
--	..........#.
--	        ...#....
--	        .....#..
--	        .#......
--	        ......#.
--
--	10R5L5R10L4R5L5
--
--	The first half of the monkeys' notes is a map of the board. It is comprised of a set of open tiles (on
--	which you can move, drawn .) and solid walls (tiles which you cannot enter, drawn #).
--
--	The second half is a description of the path you must follow. It consists of alternating numbers and letters:
--
--	* A number indicates the number of tiles to move in the direction you are facing. If you run into a
--	  wall, you stop moving forward and continue with the next instruction.
--	* A letter indicates whether to turn 90 degrees clockwise (R) or counterclockwise (L). Turning happens
--	  in-place; it does not change your current tile.
--
--	So, a path like 10R5 means "go forward 10 tiles, then turn clockwise 90 degrees, then go forward 5 tiles".
--
--	You begin the path in the leftmost open tile of the top row of tiles. Initially, you are facing to the
--	right (from the perspective of how the map is drawn).
--
--	If a movement instruction would take you off of the map, you wrap around to the other side of the
--	board. In other words, if your next tile is off of the board, you should instead look in the direction
--	opposite of your current facing as far as you can until you find the opposite edge of the board, then
--	reappear there.
--
--	For example, if you are at A and facing to the right, the tile in front of you is marked B; if you are
--	at C and facing down, the tile in front of you is marked D:
--
--	        ...#
--	        .#..
--	        #...
--	        ....
--	...#.D.....#
--	........#...
--	B.#....#...A
--	.....C....#.
--	        ...#....
--	        .....#..
--	        .#......
--	        ......#.
--
--	It is possible for the next tile (after wrapping around) to be a wall; this still counts as there
--	being a wall in front of you, and so movement stops before you actually wrap to the other side of the
--	board.
--
--	By drawing the last facing you had with an arrow on each tile you visit, the full path taken by the above example looks like this:
--
--	        >>v#
--	        .#v.
--	        #.v.
--	        ..v.
--	...#...v..v#
--	>>>v...>#.>>
--	..#v...#....
--	...>>>>v..#.
--	        ...#....
--	        .....#..
--	        .#......
--	        ......#.
--
--	To finish providing the password to this strange input device, you need to determine numbers for your
--	final row, column, and facing as your final position appears from the perspective of the original
--	map. Rows start from 1 at the top and count downward; columns start from 1 at the left and count
--	rightward. (In the above example, row 1, column 1 refers to the empty space with no tile on it in the
--	top-left corner.) Facing is 0 for right (>), 1 for down (v), 2 for left (<), and 3 for up (^). The
--	final password is the sum of 1000 times the row, 4 times the column, and the facing.
--
--	In the above example, the final row is 6, the final column is 8, and the final facing is 0. So, the
--	final password is 1000 * 6 + 4 * 8 + 0: 6032.
--
--	Follow the path given in the monkeys' notes. What is the final password?
--
-- Test:
--	% cd ../.. && echo -e '        ...#\n        .#..\n        #...\n        ....\n...#.......#\n........#...\n..#....#....\n..........#.\n        ...#....\n        .....#..\n        .#......\n        ......#.\n\n10R5L5R10L4R5L5' | cabal run 2022_day22-monkey-map # = 6032

data Instruction = TurnRight | TurnLeft | Walk Int deriving (Show, Ord, Eq)

type State = ((Int, Int), (Int, Int)) -- (Y, X), (dY, dX)

parse :: [Char] -> [Instruction]
parse ('L':is) = TurnLeft:parse is
parse ('R':is) = TurnRight:parse is
parse []       = []
parse is       = (\(n,is') -> (Walk . read $ n):parse is') . span isDigit $ is

warpFromLeft :: Vector Char -> Int
warpFromLeft = fromMaybe minBound . V.findIndex (not . isSpace)

warpFromRight :: Vector Char -> Int
warpFromRight = flip (-) 1 . V.length

warpFromTop :: Int -> Vector  (Vector Char) -> Int
warpFromTop x = warpFromLeft . V.map (\y -> fromMaybe ' ' $ y !? x)

warpFromBottom :: Int -> Vector  (Vector Char) -> Int
warpFromBottom x jm = (-) (V.length jm - 1) . warpFromLeft . V.map (\y -> fromMaybe ' ' $ y !? x) . V.reverse $ jm

turn TurnRight ( 0,  1) = ( 1,  0)
turn TurnRight ( 1,  0) = ( 0, -1)
turn TurnRight ( 0, -1) = (-1,  0)
turn TurnRight (-1,  0) = ( 0,  1)
turn TurnLeft  d        = turn TurnRight . turn TurnRight . turn TurnRight $ d

getCh :: Vector  (Vector Char) -> (Int, Int) -> Char
getCh jm (y, x) = fromMaybe ' ' $ (jm ! y) !? x

nextPt :: Vector  (Vector Char) -> State -> (Int, Int)
nextPt jm st@(pt@(y, x), dpt@(dy, dx)) = warp (y + dy, x + dx) dpt
  where warp pt'@(y, x) ( 0,  1) | x >= V.length (jm ! y) = (y, warpFromLeft  (jm ! y))
        warp pt'@(y, x) ( 0, -1) | x < 0                  = (y, warpFromRight (jm ! y))
        warp pt'@(y, x) ( 1,  0) | y >= V.length jm       = (warpFromTop    x jm, x)
        warp pt'@(y, x) (-1,  0) | y <  0                 = (warpFromBottom x jm, x)
        warp pt'@(y, x) _ | ' ' == getCh jm pt'           = nextPt jm (pt', dpt)
                          | otherwise                     = pt'

walk :: Vector  (Vector Char) -> [Instruction] -> State -> State
walk _  [] st = st
walk jm (TurnRight:is) st@(pt, dpt) = walk jm is (pt, turn TurnRight dpt)
walk jm (TurnLeft:is)  st@(pt, dpt) = walk jm is (pt, turn TurnLeft  dpt)
walk jm (Walk n:is)    st = walk jm is . walk' n $ st
  where walk' 0 st = st
        walk' n st@(pt, dpt) =
          let pt' = nextPt jm st
              c = getCh jm pt'
          in if '#' == c then st else walk' (n - 1) (pt', dpt)

-- Facing is 0 for right (>), 1 for down (v), 2 for left (<), and 3 for up (^)
facing ( 0,  1) = 0
facing ( 1,  0) = 1
facing ( 0, -1) = 2
facing (-1,  0) = 3

solve :: [[String]] -> Int
solve [ms, [_, is]] = score . walk jm (parse is) $ initState
  where jm                  = V.fromList . L.map V.fromList $ ms -- jm (jungle map) is a vector of vectors
        initState           = ((0, warpFromLeft (jm ! 0)), (0, 1))
        score ((y, x), dpt) = (1000 * (y + 1)) + (4 * (x + 1)) + facing dpt


main :: IO ()
main = interact $ show . solve . groupBy (\a b -> "" /= b) . lines

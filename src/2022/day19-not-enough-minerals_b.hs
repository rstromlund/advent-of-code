module Main where
import Control.Parallel.Strategies (parList, rdeepseq, using)
import Data.Bifunctor (second)
import Data.Char (isDigit)
import Data.List as L (filter, map, take)
import Data.Set as S (Set, foldl', fromDistinctDescList, fromList, singleton, toAscList)

--	... While you were choosing the best blueprint, the elephants found some food on their own, so you're
--	not in as much of a hurry; you figure you probably have 32 minutes before the wind changes direction
--	again and you'll need to get out of range of the erupting volcano.
--
--	Unfortunately, one of the elephants ate most of your blueprint list! Now, only the first three
--	blueprints in your list are intact.
--
--	In 32 minutes, the largest number of geodes blueprint 1 (from the example above) can open is 56. One
--	way to achieve that is:
--
--	...
--
--	However, blueprint 2 from the example above is still better; using it, the largest number of geodes
--	you could open in 32 minutes is 62.
--
--	You no longer have enough blueprints to worry about quality levels. Instead, for each of the first
--	three blueprints, determine the largest number of geodes you could open; then, multiply these three
--	values together.
--
--	Don't worry about quality levels; instead, just determine the largest number of geodes you could open
--	using each of the first three blueprints. What do you get if you multiply these numbers together?
--
-- Test:
--	% cd ../.. && echo -e 'Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\nBlueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.' | cabal run 2022_day19-not-enough-minerals_b # (56, 62, _) = ?

totalMinutes = 32

type Minerals = (Int, Int, Int, Int) -- (geode, obsidian, clay, ore) -- I reversed these to have the Set prioritize geodes ... way back when I thought that'd help ...

data Blueprint = Blueprint{ blueprintNbr :: Int
                          , oreCost      :: Minerals
                          , clayCost     :: Minerals
                          , obsidianCost :: Minerals
                          , geodeCost    :: Minerals
                          , maxCost      :: Minerals
                          }
               deriving (Show, Eq, Ord)

str2int :: String -> Int
str2int = read . filter isDigit

na :: Int
na = minBound

parse :: String -> Blueprint
parse = mkBlueprint . words
  where mkBlueprint [ _, nbr -- Blueprint 1:
                    , _, _, _, _, oreRbtOre, _ -- Each ore robot costs 4 ore.
                    , _, _, _, _, clayRbtOre, _ -- Each clay robot costs 2 ore.
                    , _, _, _, _, obsidianRbtOre, _, _, obsidianRbtClay, _ -- Each obsidian robot costs 3 ore and 14 clay.
                    , _, _, _, _, geodeRbtOre, _, _, geodeRbtObsidian, _ -- Each geode robot costs 2 ore and 7 obsidian.
                    ]
          = Blueprint { blueprintNbr = str2int nbr
                      , oreCost      = (na, na, na, str2int oreRbtOre)
                      , clayCost     = (na, na, na, str2int clayRbtOre)
                      , obsidianCost = (na, na, str2int obsidianRbtClay, str2int obsidianRbtOre)
                      , geodeCost    = (na, str2int geodeRbtObsidian, na, str2int geodeRbtOre)
                      , maxCost      = (na, str2int geodeRbtObsidian, str2int obsidianRbtClay, maximum [str2int oreRbtOre, str2int clayRbtOre, str2int obsidianRbtOre, str2int geodeRbtOre])
                      }

data State = State{ minute   :: Int
                  , robots   :: Minerals -- Fortunately, you have exactly one ore-collecting robot; all other robots start at 0
                  , minerals :: Minerals -- All materials start at 0
                  }
           deriving (Show, Eq, Ord)

initialState = State{ minute   = 1
                    , robots   = (0, 0, 0, 1)
                    , minerals = (0, 0, 0, 0)
                    }

bfs :: Blueprint -> Set State -> Set State
bfs b@Blueprint{ blueprintNbr   = nbr
               , oreCost        = (_, _, _, ooc)
               , clayCost       = (_, _, _, coc)
               , obsidianCost   = (_, _, obcc, oboc)
               , geodeCost      = (_, gobc, _, goc)
               , maxCost        = (_, maxob, maxc, maxo)
               }
  = fromList . S.foldl' genStates []
  where genStates acc s@State{minute = mc}
          | mc > totalMinutes = acc
          | otherwise         = genGeodeState s ++ acc
        --
        genGeodeState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | obr > 0
                      = let tmo = (goc  - oa  + or  - 1) `div` or
                            tmb = (gobc - oba + obr - 1) `div` obr
                            tm  = maximum [0, tmo, tmb]
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr + 1, obr, cr, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr - gobc
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - goc
                                                )
                                   }
                        in if mc' > totalMinutes then genObsidianState s else s':genObsidianState s
          | otherwise = genObsidianState s
        genObsidianState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | obr < maxob && cr > 0
                      = let tmo = (oboc - oa + or - 1) `div` or
                            tmc = (obcc - ca + cr - 1) `div` cr
                            tm  = maximum [0, tmo, tmc]
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr + 1, cr, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr - obcc
                                                , oa  + or  * tm + or - oboc
                                                )
                                   }
                        in if mc' > totalMinutes then genClayState s else s':genClayState s
          | otherwise = genClayState s
        genClayState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | cr < maxc
                      = let tm  = max 0 $ (coc - oa + or - 1) `div` or
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr, cr + 1, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - coc
                                                )
                                   }
                        in if mc' > totalMinutes then genOreState s else s':genOreState s
          | otherwise = genOreState s
        genOreState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | or < maxo
                      = let tm  = max 0 $ (ooc - oa + or - 1) `div` or
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr, cr, or + 1)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - ooc
                                                )
                                   }
                        in [s' | mc' <= totalMinutes] -- if mc' > totalMinutes then [] else [s']
          | otherwise = []

-- solve :: [Blueprint] -> Int
solve bs = let xs = map (\b@Blueprint{blueprintNbr = nbr} -> fst . search (bfs b) $ (0, singleton initialState)) . take 3 $ bs
           in product (xs `using` parList rdeepseq)
  where search :: (Set State -> Set State) -> (Int, Set State) -> (Int, Set State)
        search f acc@(maxgeos, ss)
          | null ss   = acc
          | otherwise = search f . filterBestStates maxgeos . f $ ss
        --
        filterBestStates maxgeos = second fromDistinctDescList . L.foldl' isKeeper (maxgeos, []) . map calcMaxGeodes . toAscList
          where isKeeper (maxr, acc) (r, i, s) = let maxr' = max maxr r in (maxr', if i >= maxr' then s:acc else acc)
        --
        -- FIXME: this could be done in `bfs` instead; maybe do some short circuiting to not add to the set there?
        calcMaxGeodes s@State{minute = mc, robots = (gr, _, _, _), minerals = (ga, _, _, _)}
          -- Return realistic geodes; i.e. the current amount + robots * time remaining (this will improve in time w/ more construction, but realistic for this state).  And
          -- idealistic geodes; i.e. geodes already gathered, plus /(realistic) geodes that will be gathered, plus "what if we produce a geode robot in every remaining minute"
          = let tm = totalMinutes - mc
                r  = ga + gr * tm + gr
                i  = (tm + 1) * tm `div` 2
            in (r, r + i, s)


main :: IO ()
main = interact $ show . solve . map parse . lines

module Main where
import Data.Array as A ((!), Array, bounds, listArray)
import Data.List as L (concat, find, map)
import Data.Maybe (fromMaybe, isJust)
import Data.Set as S (Set, empty, foldl', insert, map, singleton)

--	... you find that strong, turbulent winds are pushing small blizzards of snow and sharp ice around the valley. It's a good thing everyone packed warm clothes! To make it across safely, you'll need to find a way to avoid them.
--
--	Fortunately, it's easy to see all of this from the entrance to the valley, so you make a map of the valley and the blizzards (your puzzle input). For example:
--
--	#.#####
--	#.....#
--	#>....#
--	#.....#
--	#...v.#
--	#.....#
--	#####.#
--	The walls of the valley are drawn as #; everything else is ground. Clear ground - where there is currently no blizzard - is drawn as .. Otherwise, blizzards are drawn with an arrow indicating their direction of motion: up (^), down (v), left (<), or right (>).
--
--	The above map includes two blizzards, one moving right (>) and one moving down (v). In one minute, each blizzard moves one position in the direction it is pointing:
--
--	#.#####
--	#.....#
--	#.>...#
--	#.....#
--	#.....#
--	#...v.#
--	#####.#
--	Due to conservation of blizzard energy, as a blizzard reaches the wall of the valley, a new blizzard forms on the opposite side of the valley moving in the same direction. After another minute, the bottom downward-moving blizzard has been replaced with a new downward-moving blizzard at the top of the valley instead:
--
--	#.#####
--	#...v.#
--	#..>..#
--	#.....#
--	#.....#
--	#.....#
--	#####.#
--	Because blizzards are made of tiny snowflakes, they pass right through each other. After another minute, both blizzards temporarily occupy the same position, marked 2:
--
--	#.#####
--	#.....#
--	#...2.#
--	#.....#
--	#.....#
--	#.....#
--	#####.#
--	After another minute, the situation resolves itself, giving each blizzard back its personal space:
--
--	#.#####
--	#.....#
--	#....>#
--	#...v.#
--	#.....#
--	#.....#
--	#####.#
--	Finally, after yet another minute, the rightward-facing blizzard on the right is replaced with a new one on the left facing the same direction:
--
--	#.#####
--	#.....#
--	#>....#
--	#.....#
--	#...v.#
--	#.....#
--	#####.#
--	This process repeats at least as long as you are observing it, but probably forever.
--
--	Here is a more complex example:
--
--	#.######
--	#>>.<^<#
--	#.<..<<#
--	#>v.><>#
--	#<^v^^>#
--	######.#
--	Your expedition begins in the only non-wall position in the top row and needs to reach the only non-wall position in the bottom row. On each minute, you can move up, down, left, or right, or you can wait in place. You and the blizzards act simultaneously, and you cannot share a position with a blizzard.
--
--	In the above example, the fastest way to reach your goal requires 18 steps. Drawing the position of the expedition as E, one way to achieve this is:
--
--	Initial state:
--	#E######
--	#>>.<^<#
--	#.<..<<#
--	#>v.><>#
--	#<^v^^>#
--	######.#
--
--	Minute 1, move down:
--	#.######
--	#E>3.<.#
--	#<..<<.#
--	#>2.22.#
--	#>v..^<#
--	######.#
--
--	Minute 2, move down:
--	#.######
--	#.2>2..#
--	#E^22^<#
--	#.>2.^>#
--	#.>..<.#
--	######.#
--
--	Minute 3, wait:
--	#.######
--	#<^<22.#
--	#E2<.2.#
--	#><2>..#
--	#..><..#
--	######.#
--
--	Minute 4, move up:
--	#.######
--	#E<..22#
--	#<<.<..#
--	#<2.>>.#
--	#.^22^.#
--	######.#
--
--	Minute 5, move right:
--	#.######
--	#2Ev.<>#
--	#<.<..<#
--	#.^>^22#
--	#.2..2.#
--	######.#
--
--	Minute 6, move right:
--	#.######
--	#>2E<.<#
--	#.2v^2<#
--	#>..>2>#
--	#<....>#
--	######.#
--
--	Minute 7, move down:
--	#.######
--	#.22^2.#
--	#<vE<2.#
--	#>>v<>.#
--	#>....<#
--	######.#
--
--	Minute 8, move left:
--	#.######
--	#.<>2^.#
--	#.E<<.<#
--	#.22..>#
--	#.2v^2.#
--	######.#
--
--	Minute 9, move up:
--	#.######
--	#<E2>>.#
--	#.<<.<.#
--	#>2>2^.#
--	#.v><^.#
--	######.#
--
--	Minute 10, move right:
--	#.######
--	#.2E.>2#
--	#<2v2^.#
--	#<>.>2.#
--	#..<>..#
--	######.#
--
--	Minute 11, wait:
--	#.######
--	#2^E^2>#
--	#<v<.^<#
--	#..2.>2#
--	#.<..>.#
--	######.#
--
--	Minute 12, move down:
--	#.######
--	#>>.<^<#
--	#.<E.<<#
--	#>v.><>#
--	#<^v^^>#
--	######.#
--
--	Minute 13, move down:
--	#.######
--	#.>3.<.#
--	#<..<<.#
--	#>2E22.#
--	#>v..^<#
--	######.#
--
--	Minute 14, move right:
--	#.######
--	#.2>2..#
--	#.^22^<#
--	#.>2E^>#
--	#.>..<.#
--	######.#
--
--	Minute 15, move right:
--	#.######
--	#<^<22.#
--	#.2<.2.#
--	#><2>E.#
--	#..><..#
--	######.#
--
--	Minute 16, move right:
--	#.######
--	#.<..22#
--	#<<.<..#
--	#<2.>>E#
--	#.^22^.#
--	######.#
--
--	Minute 17, move down:
--	#.######
--	#2.v.<>#
--	#<.<..<#
--	#.^>^22#
--	#.2..2E#
--	######.#
--
--	Minute 18, move down:
--	#.######
--	#>2.<.<#
--	#.2v^2<#
--	#>..>2>#
--	#<....>#
--	######E#
--	What is the fewest number of minutes required to avoid the blizzards and reach the goal?
--
-- Test:
--	% cd ../.. && echo -e '#.#####\n#.....#\n#>....#\n#.....#\n#...v.#\n#.....#\n#####.#' | cabal run 2022_day24-blizzard-basin
--	% cd ../.. && echo -e '#.######\n#>>.<^<#\n#.<..<<#\n#>v.><>#\n#<^v^^>#\n######.#' | cabal run 2022_day24-blizzard-basin # = 18

type ValleyMap = Array (Int, Int) Char
type State = (Int, (Int, Int), (Int, Int)) -- Steps/Minutes, (ExitY ExitX), (YouY, YouX)

gmround :: ValleyMap -> Set State -> State -> Set State
gmround vm acc s@(stps, e, u) = moveS acc s
  where stps' = stps + 1
        ((0, 0), (maxY, maxX)) = bounds vm
        --
        moveS :: Set State -> State -> Set State
        moveS acc s@(stps, e, (uY, uX))
          | uY >= maxY || '.' /= getCh stps' (uY + 1, uX) vm = moveN acc s
          | otherwise                                        = moveN (insert (stps', e, (uY + 1, uX)) acc) s
        moveN acc s@(stps, e, (uY, uX))
          | uY <= 1 || '.' /= getCh stps' (uY - 1, uX) vm    = moveE acc s
          | otherwise                                        = moveE (insert (stps', e, (uY - 1, uX)) acc) s
        moveE acc s@(stps, e, (uY, uX))
          | uX >= maxX || '.' /= getCh stps' (uY, uX + 1) vm = moveW acc s
          | otherwise                                        = moveW (insert (stps', e, (uY, uX + 1)) acc) s
        moveW acc s@(stps, e, (uY, uX))
          | uX <= 1 || '.' /= getCh stps' (uY, uX - 1) vm    = wait acc s
          | otherwise                                        = wait (insert (stps', e, (uY, uX - 1)) acc) s
        wait :: Set State -> State -> Set State
        wait acc s@(stps, e, u@(uY, uX))
          | '.' == getCh stps' (uY, uX) vm                   = insert (stps', e, u) acc
          | otherwise                                        = acc

gmstates :: ValleyMap -> Set State -> Set State
gmstates vm = S.foldl' (gmround vm) empty

-- Run until the first (i.e. fastest) winning state is found
run :: ValleyMap -> Set State -> State
run vm ss
  | null ss   = error "No game states"
  | otherwise = let win = find (\s@(_, e, u) -> e == u) ss
                in if isJust win then fromMaybe (0, (0,0), (0,0)) win else run vm . gmstates vm $ ss

{-* `getCh` uses modulo math to find the contents of the specified coordinates; no need to recalculate the board for just 4 or 5 characters. *-}
getCh :: Int -> (Int, Int) -> ValleyMap -> Char
getCh stps (y, x) vm = ch
  where ch
          | 0 == y || maxY == y || 0 == x || maxX == x = vm ! (y, x)
          | '^' == vm ! (newY $ y + stps, x) = '^'
          | 'v' == vm ! (newY $ y - stps, x) = 'v'
          | '<' == vm ! (y, newX $ x + stps) = '<'
          | '>' == vm ! (y, newX $ x - stps) = '>'
          | otherwise = '.'
        newY y = 1 + (y - 1) `mod` (maxY - 1)
        newX x = 1 + (x - 1) `mod` (maxX - 1)
        ((0, 0), (maxY, maxX)) = bounds vm

{-* `showmap` is useful for debugging/visualizing; leaving in final solution. *-}
showmap :: ValleyMap -> State -> [String]
showmap vm s@(stps, _, (uY, uX)) = L.map showXs [0..maxY]
  where showXs y = L.map (showCs y) [0..maxX]
        showCs y x | y == uY && x == uX = 'E'
                   | otherwise          = getCh stps (y, x) vm
        ((0, 0), (maxY, maxX)) = bounds vm

solve :: [String] -> Int
solve ys = score . run vm $ singleton s0
  where maxY  = length ys - 1 :: Int
        maxX  = (length . head $ ys) - 1 :: Int
        enter = (0, length . takeWhile ('#' ==) $ head ys) :: (Int, Int)
        exit  = (maxY, length . takeWhile ('#' ==) $ last ys) :: (Int, Int)
        s0 :: State
        s0 = (0, exit, enter)
        vm :: ValleyMap
        vm = listArray ((0, 0), (maxY, maxX)) . concat $ ys
        --
        score (stps, _, _) = stps


main :: IO ()
main = interact $ show . solve . lines

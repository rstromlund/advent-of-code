#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint .
typeset rc=${?}

### Run all puzzle solutions:

echo -e "\n\n==== day01"
time ./day01-calorie-counting.hs   < 01a.txt ; (( rc += ${?} ))
time ./day01-calorie-counting_b.hs < 01a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day02"
time ./day02-rock-paper-scissors.hs   < 02a.txt ; (( rc += ${?} ))
time ./day02-rock-paper-scissors_b.hs < 02a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day03"
time ./day03-rucksack-reorganization.hs   < 03a.txt ; (( rc += ${?} ))
time ./day03-rucksack-reorganization_b.hs < 03a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day04"
time ./day04-camp-cleanup.hs   < 04a.txt ; (( rc += ${?} ))
time ./day04-camp-cleanup_b.hs < 04a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day05"
time ./day05-supply-stacks.hs   < 05a.txt ; (( rc += ${?} ))
time ./day05-supply-stacks_b.hs < 05a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day06"
time ./day06-tuning-trouble.hs   < 06a.txt ; (( rc += ${?} ))
time ./day06-tuning-trouble_b.hs < 06a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day07"
time ./day07-no-space-left-on-device.hs   < 07a.txt ; (( rc += ${?} ))
time ./day07-no-space-left-on-device_b.hs < 07a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day08"
time ./day08-treetop-tree-house.hs   < 08a.txt ; (( rc += ${?} ))
time ./day08-treetop-tree-house_b.hs < 08a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day09"
time ./day09-rope-bridge.hs   < 09a.txt ; (( rc += ${?} ))
time ./day09-rope-bridge_b.hs < 09a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day10"
time ./day10-cathode-ray-tube.hs   < 10a.txt ; (( rc += ${?} ))
time ./day10-cathode-ray-tube_b.hs < 10a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day11"
time ./day11-monkey-in-the-middle.hs   < 11a.txt ; (( rc += ${?} ))
time ./day11-monkey-in-the-middle_b.hs < 11a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day12"
time ./day12-hill-climbing-algorithm.hs   < 12a.txt ; (( rc += ${?} ))
time ./day12-hill-climbing-algorithm_b.hs < 12a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day13"
time ./day13-distress-signal.hs   < 13a.txt ; (( rc += ${?} ))
time ./day13-distress-signal_b.hs < 13a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day14"
time ./day14-regolith-reservoir.hs   < 14a.txt ; (( rc += ${?} ))
time ./day14-regolith-reservoir_b.hs < 14a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day15"
time ./day15-beacon-exclusion-zone.hs   < 15a.txt ; (( rc += ${?} ))
time ./day15-beacon-exclusion-zone_b.hs < 15a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day16"
time ./day16-proboscidea-volcanium.hs   < 16a.txt ; (( rc += ${?} ))
time ./day16-proboscidea-volcanium_b.hs < 16a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day17"
time ./day17-pyroclastic-flow.hs   < 17a.txt ; (( rc += ${?} ))
time ./day17-pyroclastic-flow_b.hs < 17a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day18"
time ./day18-boiling-boulders.hs   < 18a.txt ; (( rc += ${?} ))
time ./day18-boiling-boulders_b.hs < 18a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day19"
time ./day19-not-enough-minerals.hs   < 19a.txt ; (( rc += ${?} ))
time ./day19-not-enough-minerals_b.hs < 19a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day20"
time ./day20-grove-positioning-system.hs   < 20a.txt ; (( rc += ${?} ))
time ./day20-grove-positioning-system_b.hs < 20a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day21"
time ./day21-monkey-math.hs   < 21a.txt ; (( rc += ${?} ))
time ./day21-monkey-math_b.hs < 21a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day22"
time ./day22-monkey-map.hs   < 22a.txt ; (( rc += ${?} ))
time ./day22-monkey-map_b.hs < 22a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day23"
time ./day23-unstable-diffusion.hs   < 23a.txt ; (( rc += ${?} ))
time ./day23-unstable-diffusion_b.hs < 23a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day24"
time ./day24-blizzard-basin.hs   < 24a.txt ; (( rc += ${?} ))
time ./day24-blizzard-basin_b.hs < 24a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-full-of-hot-air.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

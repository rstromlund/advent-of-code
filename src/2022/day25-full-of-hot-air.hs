module Main where

--	... For example:
--
--	1=-0-2
--	12111
--	2=0=
--	21
--	2=01
--	111
--	20012
--	112
--	1=-1=
--	1-12
--	12
--	1=
--	122
--
--	... "Okay, our Special Numeral-Analogue Fuel Units - SNAFU for short - are sort of like normal
--	numbers. You know how starting on the right, normal numbers have a ones place, a tens place, a
--	hundreds place, and so on, where the digit in each place tells you how many of that value you have?"
--
--	"SNAFU works the same way, except it uses powers of five instead of ten. Starting from the right, you
--	have a ones place, a fives place, a twenty-fives place, a one-hundred-and-twenty-fives place, and so
--	on. It's that easy!"
--
--	You ask why some of the digits look like - or = instead of "digits".
--
--	"You know, I never did ask the engineers why they did that. Instead of using digits four through zero,
--	the digits are 2, 1, 0, minus (written -), and double-minus (written =). Minus is worth -1, and
--	double-minus is worth -2."
--
--	"So, because ten (in normal numbers) is two fives and no ones, in SNAFU it is written 20. Since eight
--	(in normal numbers) is two fives minus two ones, it is written 2=."
--
--	"You can do it the other direction, too. Say you have the SNAFU number 2=-01. That's 2 in the 625s
--	place, = (double-minus) in the 125s place, - (minus) in the 25s place, 0 in the 5s place, and 1 in the
--	1s place. (2 times 625) plus (-2 times 125) plus (-1 times 25) plus (0 times 5) plus (1 times
--	1). That's 1250 plus -250 plus -25 plus 0 plus 1. 976!"
--
--	...
--
--	  Decimal          SNAFU
--	        1              1
--	        2              2
--	        3             1=
--	        4             1-
--	        5             10
--	        6             11
--	        7             12
--	        8             2=
--	        9             2-
--	       10             20
--	       15            1=0
--	       20            1-0
--	     2022         1=11-2
--	    12345        1-0---0
--	314159265  1121-1110-1=0
--
--	Based on this process, the SNAFU numbers in the example above can be converted to decimal numbers as follows:
--
--	 SNAFU  Decimal
--	1=-0-2     1747
--	 12111      906
--	  2=0=      198
--	    21       11
--	  2=01      201
--	   111       31
--	 20012     1257
--	   112       32
--	 1=-1=      353
--	  1-12      107
--	    12        7
--	    1=        3
--	   122       37
--
--	In decimal, the sum of these numbers is 4890.
--
--	As you go to input this number on Bob's console, you discover that some buttons you expected are
--	missing. Instead, you are met with buttons labeled =, -, 0, 1, and 2. Bob needs the input value
--	expressed as a SNAFU number, not in decimal.
--
--	Reversing the process, you can determine that for the decimal number 4890, the SNAFU number you need to supply to Bob's console is 2=-1=0.
--
--	The Elves are starting to get cold. What SNAFU number do you supply to Bob's console?
--
-- Test:
--	% cd ../.. && echo -e '1\n2\n1=\n1-\n10\n11\n12\n2=\n2-\n20\n1=0\n1-0\n1=11-2\n1-0---0\n1121-1110-1=0' | cabal run 2022_day25-full-of-hot-air # = (base SNAFU) ????? (base10) 314173722
--	% cd ../.. && echo -e '1=-0-2\n12111\n2=0=\n21\n2=01\n111\n20012\n112\n1=-1=\n1-12\n12\n1=\n122' | cabal run 2022_day25-full-of-hot-air # = (base SNAFU) 2=-1=0 (base10) 4890

snafu2int :: String -> Int
snafu2int = foldl' ch2int 0
  where ch2int acc '0' = acc * 5
        ch2int acc '1' = acc * 5 + 1
        ch2int acc '2' = acc * 5 + 2
        ch2int acc '=' = acc * 5 - 2
        ch2int acc '-' = acc * 5 - 1

int2snafu :: Int -> String
int2snafu x = reverse $ int2snafu' (x `divMod` 5) ""
  where int2snafu' (w, 0) acc = '0':cont w acc
        int2snafu' (w, 1) acc = '1':cont w acc
        int2snafu' (w, 2) acc = '2':cont w acc
        int2snafu' (w, 3) acc = '=':cont (w + 1) acc
        int2snafu' (w, 4) acc = '-':cont (w + 1) acc
        cont 0 acc = acc
        cont x acc = int2snafu' (x `divMod` 5) acc

solve :: [String] -> String
solve = int2snafu . sum . map snafu2int


main :: IO ()
main = interact $ solve . words

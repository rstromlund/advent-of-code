module Main where
import Data.Bool (bool)
import Data.List (intersect, notElem, sort)
import Data.Map.Strict ((!), (!?), Map, delete, empty, filterWithKey, foldlWithKey', fromList, insert, size)
import Data.Maybe (fromMaybe)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, sepBy, some)
import Text.Megaparsec.Char (char, newline, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... You're worried that even with an optimal approach, the pressure released won't be enough. What if you got one of the elephants to help you?
--
--	It would take you 4 minutes to teach an elephant how to open the right valves in the right order,
--	leaving you with only 26 minutes to actually execute your plan. Would having two of you working
--	together be better, even if it means having less time? (Assume that you teach the elephant before
--	opening any valves yourself, giving you both the same full 26 minutes.)
--
--	In the example above, you could teach the elephant to help you as follows: ...
--
--	With the elephant helping, after 26 minutes, the best you could do would release a total of 1707 pressure.
--
--	With you and an elephant working together for 26 minutes, what is the most pressure you could release?
--
-- Test:
--	% cd ../.. && echo -e 'Valve AA has flow rate=0; tunnels lead to valves DD, II, BB\nValve BB has flow rate=13; tunnels lead to valves CC, AA\nValve CC has flow rate=2; tunnels lead to valves DD, BB\nValve DD has flow rate=20; tunnels lead to valves CC, AA, EE\nValve EE has flow rate=3; tunnels lead to valves FF, DD\nValve FF has flow rate=0; tunnels lead to valves EE, GG\nValve GG has flow rate=0; tunnels lead to valves FF, HH\nValve HH has flow rate=22; tunnel leads to valve GG\nValve II has flow rate=0; tunnels lead to valves AA, JJ\nValve JJ has flow rate=21; tunnel leads to valve II' | cabal run 2022_day16-proboscidea-volcanium_b # 1707

type RoomConfig = (Int, [(String, Int)]) -- (Flow rate, [Connected rooms, Distance (1 to begin)])
type Room = (String, RoomConfig) -- Room name, Configuation

type Parser = Parsec Void String

rooms :: Parser [Room]
rooms = many room <* optional newline <* eof

room :: Parser Room
room = mkRoom <$> r <*> f <*> j <* newline
  where r = string "Valve " *> some upperChar
        f = string " has flow rate=" *> decimal
        j = string "; tunnel" *> optional (char 's') *> string " lead" *> optional (char 's') *> string " to valve" *> optional (char 's') *> (((,1) <$> (char ' ' *> some upperChar)) `sepBy` char ',')
        mkRoom r f j = (r, (f, j))

--

-- Floyd–Warshall algorithm
-- ref: https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm

getVertices :: Map String RoomConfig -> [String]
getVertices = sort . foldlWithKey' (\acc k _ -> k:acc) []

getEdges :: Map String RoomConfig -> [((String, String), Int)]
getEdges = foldlWithKey' getEdges' []
  where getEdges' :: [((String, String), Int)] -> String -> RoomConfig -> [((String, String), Int)]
        getEdges' acc r (_, js) = foldl' (\acc' (jx, dx) -> ((r, jx), dx):acc') acc js


-- Not the prettiest but it seems to work : )
floydWarshall :: Map String RoomConfig -> Map (String, String) Int
floydWarshall ss = fw
  where vs = getVertices ss
        dist = fromList $ getEdges ss ++ map (\r -> ((r, r), 0)) vs
        getW r1 r2 dist = fromMaybe (maxBound `div` 3) $ dist !? (r1, r2)
        fw = foldl' ks dist vs
          where ks dist' k = foldl' is dist' vs
                  where is dist' i = foldl' js dist' vs
                          where js dist' j =
                                  let dIJ = getW i j dist'
                                      dIK = getW i k dist'
                                      dKJ = getW k j dist'
                                  in if dIJ > dIK + dKJ then insert (i, j) (dIK + dKJ) dist' else dist'

--

-- Not pretty, but it works.
elideZeroFlowRates :: Map String RoomConfig -> Map String RoomConfig
elideZeroFlowRates ss = foldlWithKey' elideZeroFlowRates' ss ss
  where elideZeroFlowRates' :: Map String RoomConfig -> String -> RoomConfig -> Map String RoomConfig
        elideZeroFlowRates' acc r (0, [_, _]) = attachRooms r (acc !? r) acc
        elideZeroFlowRates' acc _ _ = acc
        attachRooms :: String -> Maybe RoomConfig -> Map String RoomConfig -> Map String RoomConfig
        attachRooms _ Nothing acc = acc
        attachRooms r (Just (0, [(j1, d1), (j2, d2)])) acc =
          let (f1, js1) = acc ! j1
              (f2, js2) = acc ! j2
              js1' = map (updateDist' j1) js1
              js2' = map (updateDist' j2) js2
          in delete r . insert j2 (f2, js2') . insert j1 (f1, js1') $ acc
          where updateDist' rx jc@(jx, dx)
                  | jx == r && rx == j2 = (j1, dx + d1)
                  | jx == r && rx == j1 = (j2, dx + d2)
                  | otherwise = jc

type SearchQueue = (Int, (String, Int, [String])) -- (Rate calculated, (Current room, Time remaining, [Open-valves]))

visits :: Map String RoomConfig -> Map (String, String) Int -> Map [String] Int
visits es fws = dfs (0, ("AA", 26, ["AA"])) empty
  where dfs :: SearchQueue -> Map [String] Int -> Map [String] Int
        dfs q@(rt, (r, tm, js)) acc
          | tm >= 2 && size dests > 0 = foldlWithKey' (dfs' q) acc dests
          | otherwise                 = createValveMap (rt, (r, tm - 1, js)) acc
          where dests = filterWithKey (\(src, dest) _ -> src == r && dest /= r && dest `notElem` js) fws
        dfs' :: SearchQueue -> Map [String] Int -> (String, String) -> Int -> Map [String] Int
        dfs' q@(rt, (r, tm, js)) acc (_, d1) dx =
          let tm'      = tm - dx - 1
              (fl, _)  = es ! d1
              q'       = (rt + tm' * fl, (d1, tm', d1:js))
          in dfs q' $ createValveMap q' acc
        createValveMap (rt, (_, _, js)) m =
          let js' = sort js
              rt' = fromMaybe 0 $ m !? js'
          in if rt > rt' then insert js' rt m else m

-- Search for 2 disjoint solutions (you and elephant open different valves) and find max flow rate released.
pairPlayers :: Map [String] Int -> Int
pairPlayers qs = foldlWithKey' joinPairs 0 qs
  where joinPairs acc js1 rt1 = foldlWithKey' (joinPairs' js1 rt1) acc qs
        joinPairs' js1 rt1 acc js2 rt2 =
          let cmn = js1 `intersect` js2
          in bool acc (max (rt1 + rt2) acc) $ rt2 >= rt1 && ["AA"] == cmn

solve :: Either a [Room] -> Int
solve (Right ss) = pairPlayers . visits es . filterWithKey (\(s, d) dx -> s /= d && d /= "AA") $ fw
  where es = elideZeroFlowRates . fromList $ ss
        fw = floydWarshall es


main :: IO ()
main = interact $ show . solve . runParser rooms "<stdin>"

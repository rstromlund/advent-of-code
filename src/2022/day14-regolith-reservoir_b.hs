module Main where
import qualified Data.Set as S (Set, empty, foldl', insert, lookupMax, lookupMin, member)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... the floor is an infinite horizontal line with a y coordinate equal to two plus the highest y coordinate of any point in your scan.
--
--	In the example above, the highest y coordinate of any point is 9, and so the floor is at y=11.
--
--	simulate falling sand until a unit of sand comes to rest at 500,0, blocking the source entirely and
--	stopping the flow of sand into the cave. In the example above, the situation finally looks like this
--	after 93 units of sand come to rest:
--
-- Test:
--	% cd ../.. && echo -e '498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9' | cabal run 2022_day14-regolith-reservoir_b # 93

type Point = (Int, Int)
type RockFormation = [Point]

type Parser = Parsec Void String

rforms :: Parser [RockFormation]
rforms = many rform <* optional newline <* eof

rform :: Parser RockFormation
rform = (point `sepBy` string " -> ") <* newline

point :: Parser Point
point = (,) <$> (decimal <* char ',') <*> decimal

--

drawFormations :: Either a [RockFormation] -> S.Set Point
drawFormations (Right rfs) = foldl' drawLines S.empty rfs
  where drawLines grid rf = fst . foldl' drawLine (grid, head rf) $ tail rf
        drawLine (grid, bpt@(bx, by)) ept@(ex, ey) = (draw bpt $ S.insert bpt grid, ept)
          where dx = signum (ex - bx)
                dy = signum (ey - by)
                draw cpt@(x, y) grid
                  | x == ex && y == ey = S.insert cpt grid
                  | otherwise          = draw (x + dx, y + dy) $ S.insert cpt grid


{-*
Rocks: [(494,9),(495,9),(496,6),(496,9),(497,6),(497,9),(498,4),(498,5),(498,6),(498,9),(499,9),(500,9),(501,9),(502,4),(502,5),(502,6),(502,7),(502,8),(502,9),(503,4)]

Sand:
                        500,2
                  499,3 500,3 501,3
                  499,4 500,4 501,4
      497,5       499,5 500,5 501,5
                  499,6 500,6 501,6
            498,7 499,7 500,7 501,7
495,8 497,8 498,8 499,8 500,8 501,8

          x=500
0 - ......+...
1 - ..........
2 - ......o...
3 - .....ooo..
4 - ....#ooo##
5 - ...o#ooo#.
6 - ..###ooo#.
7 - ....oooo#.
8 - .o.ooooo#.
9 - #########.
*-}

sourcePt = (500, 0)
rollVecs = [(0, 1), (-1, 1), (1, 1)]

solve :: S.Set Point -> Int
solve grid = dropSand 0 sourcePt grid rollVecs
  where Just (minX, _) = S.lookupMin grid
        Just (maxX, _) = S.lookupMax grid
        maxY = 2 + S.foldl' (\acc(_, y)  -> max y acc) 0 grid
        --
        dropSand :: Int -> Point -> S.Set Point -> [Point] -> Int
        dropSand cnt cpt@(x, y) grid rs@((dx, dy):rs')
          | y + dy >= maxY                   = dropSand (cnt + 1) sourcePt (S.insert cpt grid) rollVecs
          | (x + dx, y + dy) `S.member` grid = dropSand cnt cpt grid rs'
          | otherwise                        = dropSand cnt (x + dx, y + dy) grid rollVecs
        dropSand cnt cpt grid []
          | sourcePt == cpt                  = cnt + 1
          | otherwise                        = dropSand (cnt + 1) sourcePt (S.insert cpt grid) rollVecs


main :: IO ()
main = interact $ show . solve . drawFormations . runParser rforms "<stdin>"

module Main where
import Data.Bifunctor (first)
import Data.Char (isDigit)
import Data.Functor (($>))
import Data.List (dropWhile, groupBy, sortBy)
import Data.Ord (Ordering(LT, EQ, GT))
import Data.List.Split (chunksOf)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, lowerChar, newline, space, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Now, you just need to put all of the packets in the right order. Disregard the blank lines in your list of received packets.
--
--	The distress signal protocol also requires that you include two additional divider packets:
--
--	[[2]]
--	[[6]]
--
--	Using the same rules as before, organize all packets - the ones in your list of received packets as well as the two divider packets - into the correct order.
--
--	...
--
--	Afterward, locate the divider packets. To find the decoder key for this distress signal, you need to
--	determine the indices of the two divider packets and multiply them together. (The first packet is at
--	index 1, the second packet is at index 2, and so on.) In this example, the divider packets are 10th
--	and 14th, and so the decoder key is 140.
--
--	Organize all of the packets into the correct order. What is the decoder key for the distress signal?
--
-- Test:
--	% cd ../.. && echo -e '[1,1,3,1,1]\n[1,1,5,1,1]\n\n[[1],[2,3,4]]\n[[1],4]\n\n[9]\n[[8,7,6]]\n\n[[4,4],4,4]\n[[4,4],4,4,4]\n\n[7,7,7,7]\n[7,7,7]\n\n[]\n[3]\n\n[[[]]]\n[[]]\n\n[1,[2,[3,[4,[5,6,7]]]],8,9]\n[1,[2,[3,[4,[5,6,0]]]],8,9]' | cabal run 2022_day13-distress-signal_b # = 140

data PuzzleList = PList [PuzzleList] | PInt Int deriving (Show)

type Parser = Parsec Void String

plists :: Parser [PuzzleList]
plists = many (plist <* space) <* eof

plist :: Parser PuzzleList
plist = choice
  [ char '[' *> (PList <$> (plist `sepBy` char ',')) <* char ']'
  , PInt <$> decimal
  ]

--

dividerPackets = [PList [PList [PInt 2]],PList [PList [PInt 6]]]

solve :: Either a [PuzzleList] -> Int
solve (Right ps) = score . sortBy comp $ dividerPackets ++ ps
  where score = product . map fst . filter snd . zip [1..] . map (\p -> EQ == comp p (head dividerPackets) || EQ == comp p (dividerPackets !! 1))
        --
        -- FIXME: derive Ord and Eq to make PuzzleList a first class datatype?  And just use compare instead : )
        --
        comp :: PuzzleList -> PuzzleList -> Ordering
        comp (PList []) (PList [])     = EQ -- exhausted this list; continue on
        comp (PList []) (PList (_:_))  = LT  -- If the left list runs out of items first, the inputs are in the right order
        comp (PList (_:_)) (PList [])  = GT -- If the right list runs out of items first, the inputs are not in the right order
        comp (PList ((PInt a):as)) (PList ((PInt b):bs)) = -- If both values are integers, the lower integer should come first
          let rel = compare a b in if EQ == rel then comp (PList as) (PList bs) else rel
        comp a@(PInt _) b              = comp (PList [a]) b -- If exactly one value is an integer, convert the integer to a list
        comp a b@(PInt _)              = comp a $ PList [b]
        comp (PList (a:as)) (PList (b:bs)) = -- loop thru lists; short circuit if LT or GT; continue comparing when EQ
          let rel = comp a b in if EQ == rel then comp (PList as) (PList bs) else rel


main :: IO ()
main = interact $ show . solve . runParser plists "<stdin>"

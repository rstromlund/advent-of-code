module Main where
import Control.Parallel.Strategies (parList, rdeepseq, using)
import Data.Bifunctor (second)
import Data.Char (isDigit)
import Data.List as L (filter, map)
import Data.Set as S (Set, foldl', fromDistinctDescList, fromList, singleton, toAscList)

--	... The robot factory has many blueprints (your puzzle input) you can choose from, but once you've
--	configured it with a blueprint, you can't change it. You'll need to work out which blueprint is best.
--
--	For example:
--
--	Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.
--	Blueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.
--
--	The elephants are starting to look hungry, so you shouldn't take too long; you need to figure out
--	which blueprint would maximize the number of opened geodes after 24 minutes by figuring out which
--	robots to build and when to build them.
--
--	Using blueprint 1 in the example above, the largest number of geodes you could open in 24 minutes is
--	9. One way to achieve that is:
--
--	== Minute 1 ==
--	...
--
--	However, by using blueprint 2 in the example above, you could do even better: the largest number of
--	geodes you could open in 24 minutes is 12.
--
--	Determine the quality level of each blueprint by multiplying that blueprint's ID number with the
--	largest number of geodes that can be opened in 24 minutes using that blueprint. In this example, the
--	first blueprint has ID 1 and can open 9 geodes, so its quality level is 9. The second blueprint has ID
--	2 and can open 12 geodes, so its quality level is 24. Finally, if you add up the quality levels of all
--	of the blueprints in the list, you get 33.
--
--	Determine the quality level of each blueprint using the largest number of geodes it could produce in
--	24 minutes. What do you get if you add up the quality level of all of the blueprints in your list?
--
-- Test:
--	% cd ../.. && echo -e 'Blueprint 1: Each ore robot costs 4 ore. Each clay robot costs 2 ore. Each obsidian robot costs 3 ore and 14 clay. Each geode robot costs 2 ore and 7 obsidian.\nBlueprint 2: Each ore robot costs 2 ore. Each clay robot costs 3 ore. Each obsidian robot costs 3 ore and 8 clay. Each geode robot costs 3 ore and 12 obsidian.' | cabal run 2022_day19-not-enough-minerals # = 33

totalMinutes = 24

type Minerals = (Int, Int, Int, Int) -- (geode, obsidian, clay, ore) -- I reversed these to have the Set prioritize geodes ... way back when I thought that'd help ...

data Blueprint = Blueprint{ blueprintNbr :: Int
                          , oreCost      :: Minerals
                          , clayCost     :: Minerals
                          , obsidianCost :: Minerals
                          , geodeCost    :: Minerals
                          , maxCost      :: Minerals
                          }
               deriving (Show, Eq, Ord)

str2int :: String -> Int
str2int = read . filter isDigit

na :: Int
na = minBound

parse :: String -> Blueprint
parse = mkBlueprint . words
  where mkBlueprint [ _, nbr -- Blueprint 1:
                    , _, _, _, _, oreRbtOre, _ -- Each ore robot costs 4 ore.
                    , _, _, _, _, clayRbtOre, _ -- Each clay robot costs 2 ore.
                    , _, _, _, _, obsidianRbtOre, _, _, obsidianRbtClay, _ -- Each obsidian robot costs 3 ore and 14 clay.
                    , _, _, _, _, geodeRbtOre, _, _, geodeRbtObsidian, _ -- Each geode robot costs 2 ore and 7 obsidian.
                    ]
          = Blueprint { blueprintNbr = str2int nbr
                      , oreCost      = (na, na, na, str2int oreRbtOre)
                      , clayCost     = (na, na, na, str2int clayRbtOre)
                      , obsidianCost = (na, na, str2int obsidianRbtClay, str2int obsidianRbtOre)
                      , geodeCost    = (na, str2int geodeRbtObsidian, na, str2int geodeRbtOre)
                      , maxCost      = (na, str2int geodeRbtObsidian, str2int obsidianRbtClay, maximum [str2int oreRbtOre, str2int clayRbtOre, str2int obsidianRbtOre, str2int geodeRbtOre])
                      }

data State = State{ minute   :: Int
                  , robots   :: Minerals -- Fortunately, you have exactly one ore-collecting robot; all other robots start at 0
                  , minerals :: Minerals -- All materials start at 0
                  }
           deriving (Show, Eq, Ord)

initialState = State{ minute   = 1
                    , robots   = (0, 0, 0, 1)
                    , minerals = (0, 0, 0, 0)
                    }

bfs :: Blueprint -> Set State -> Set State
bfs b@Blueprint{ blueprintNbr   = nbr
               , oreCost        = (_, _, _, ooc)
               , clayCost       = (_, _, _, coc)
               , obsidianCost   = (_, _, obcc, oboc)
               , geodeCost      = (_, gobc, _, goc)
               , maxCost        = (_, maxob, maxc, maxo)
               }
  = fromList . S.foldl' genStates []
  where genStates acc s@State{minute = mc}
          | mc > totalMinutes = acc
          | otherwise         = genGeodeState s ++ acc
        --
        genGeodeState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | obr > 0
                      = let tmo = (goc  - oa  + or  - 1) `div` or
                            tmb = (gobc - oba + obr - 1) `div` obr
                            tm  = maximum [0, tmo, tmb]
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr + 1, obr, cr, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr - gobc
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - goc
                                                )
                                   }
                        in if mc' > totalMinutes then genObsidianState s else s':genObsidianState s
          | otherwise = genObsidianState s
        genObsidianState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | obr < maxob && cr > 0
                      = let tmo = (oboc - oa + or - 1) `div` or
                            tmc = (obcc - ca + cr - 1) `div` cr
                            tm  = maximum [0, tmo, tmc]
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr + 1, cr, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr - obcc
                                                , oa  + or  * tm + or - oboc
                                                )
                                   }
                        in if mc' > totalMinutes then genClayState s else s':genClayState s
          | otherwise = genClayState s
        genClayState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | cr < maxc
                      = let tm  = max 0 $ (coc - oa + or - 1) `div` or
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr, cr + 1, or)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - coc
                                                )
                                   }
                        in if mc' > totalMinutes then genOreState s else s':genOreState s
          | otherwise = genOreState s
        genOreState s@State{minute = mc, robots = (gr, obr, cr, or), minerals = (ga, oba, ca, oa)}
          | or < maxo
                      = let tm  = max 0 $ (ooc - oa + or - 1) `div` or
                            mc' = mc + 1 + tm
                            s'  = s{ minute = mc'
                                   , robots = (gr, obr, cr, or + 1)
                                   , minerals = ( ga  + gr  * tm + gr
                                                , oba + obr * tm + obr
                                                , ca  + cr  * tm + cr
                                                , oa  + or  * tm + or - ooc
                                                )
                                   }
                        in [s' | mc' <= totalMinutes] -- if mc' > totalMinutes then [] else [s']
          | otherwise = []

solve :: [Blueprint] -> Int
solve bs = let xs = map (\b@Blueprint{blueprintNbr = nbr} -> (*) nbr . fst . search (bfs b) $ (0, singleton initialState)) bs
           in sum (xs `using` parList rdeepseq) -- parallelism is probably not necessary now with the guard/filter-es below.
  where search :: (Set State -> Set State) -> (Int, Set State) -> (Int, Set State)
        search f acc@(maxgeos, ss)
          | null ss   = acc
          | otherwise = search f . filterBestStates maxgeos . f $ ss
        --
        filterBestStates maxgeos = second fromDistinctDescList . L.foldl' isKeeper (maxgeos, []) . map calcMaxGeodes . toAscList
          where isKeeper (maxr, acc) (r, i, s) =
                  let maxr' = max maxr r
                  in (maxr', if i >= maxr' then s:acc else acc)
        --
        -- FIXME: this could be done in `bfs` instead; maybe do some short circuiting to not add to the set there?
        calcMaxGeodes s@State{minute = mc, robots = (gr, _, _, _), minerals = (ga, _, _, _)}
          -- Return realistic geodes; i.e. the current amount + robots * time remaining (this will improve in time w/ more construction, but realistic for this state).  And
          -- idealistic geodes; i.e. geodes already gathered, plus /(realistic) geodes that will be gathered, plus "what if we produce a geode robot in every remaining minute"
          = let tm = totalMinutes - mc
                r  = ga + gr * tm + gr
                i  = (tm + 1) * tm `div` 2
            in (r, r + i, s)


main :: IO ()
main = interact $ show . solve . map parse . lines

module Main where
import Data.Bool (bool)
import Data.List (lookup)
import Data.Set (Set, fromList, insert, size)

--	... the X register controls the horizontal position of a sprite. Specifically, the sprite is 3 pixels wide, and the X register sets the horizontal position of the middle of that sprite. (In this system, there is no such thing as "vertical position": if the sprite's horizontal position puts its pixels where the CRT is currently drawing, then those pixels will be drawn.)
--
--	You count the pixels on the CRT: 40 wide and 6 high. This CRT screen draws the top row of pixels
--	left-to-right, then the row below that, and so on. The left-most pixel in each row is in position 0,
--	and the right-most pixel in each row is in position 39.
--
--	Like the CPU, the CRT is tied closely to the clock circuit: the CRT draws a single pixel during each
--	cycle. Representing each pixel of the screen as a #, here are the cycles during which the first and
--	last pixel in each row are drawn:
--
--	Cycle   1 -> ######################################## <- Cycle  40
--	Cycle  41 -> ######################################## <- Cycle  80
--	Cycle  81 -> ######################################## <- Cycle 120
--	Cycle 121 -> ######################################## <- Cycle 160
--	Cycle 161 -> ######################################## <- Cycle 200
--	Cycle 201 -> ######################################## <- Cycle 240
--
--	So, by carefully timing the CPU instructions and the CRT drawing operations, you should be able to
--	determine whether the sprite is visible the instant each pixel is drawn. If the sprite is positioned
--	such that one of its three pixels is the pixel currently being drawn, the screen produces a lit pixel
--	(#); otherwise, the screen leaves the pixel dark (.).
--
-- Test:
--	% cd ../.. && echo -e 'addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop' | cabal run 2022_day10-cathode-ray-tube_b # = (nonsense that matches the explanation)

type VM = (Int, Int, (Int, Int), [Char]) -- Cycles, Register X, (CRT X, CRT Y), CRT display

bootedVM = (1, 1, (0, 0), [])

solve :: [[String]] -> String -- let as an exercise to draw display in row/column format (or just manually split the output every 40 characters)
solve = (\(_, _, _, disp) -> reverse disp) . foldl' runPgm bootedVM
  where runPgm :: VM -> [String] -> VM
        runPgm state ['n':_]    = clockTick 0 state
        runPgm state ['a':_, i] = clockTick (read i) $ clockTick 0 state
        clockTick off (cyc, x, crt, disp) = (cyc + 1, x + off, advanceCRT crt, getChar x crt:disp)
        advanceCRT (col, row) =
          let col' = (col + 1) `mod` 40
          in (col', bool row (row + 1) $ col' < col)
        getChar x (col, _) = bool '.' '#' $ x >= (col - 1) && x <= (col + 1)


main :: IO ()
main = interact $ show . solve . map words . lines

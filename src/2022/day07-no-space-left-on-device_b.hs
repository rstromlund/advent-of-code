module Main where
import Data.Bifunctor (first)
import Data.Functor (($>))
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, lowerChar, newline, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The total disk space available to the filesystem is 70000000. To run the update, you need unused
--	space of at least 30000000. You need to find a directory you can delete that will free up enough space
--	to run the update.
--
--	In the example above, the total size of the outermost directory (and thus the total amount of used
--	space) is 48381165; this means that the size of the unused space must currently be 21618835, which
--	isn't quite the 30000000 required by the update. Therefore, the update still requires a directory with
--	total size of at least 8381165 to be deleted before it can run.
--
--	To achieve this, you have the following options:
--
--	Delete directory e, which would increase unused space by 584.
--	Delete directory a, which would increase unused space by 94853.
--	Delete directory d, which would increase unused space by 24933642.
--	Delete directory /, which would increase unused space by 48381165.
--
--	Directories e and a are both too small; deleting them would not free up enough space. However,
--	directories d and / are both big enough! Between these, choose the smallest: d, increasing unused
--	space by 24933642.
--
--	Find the smallest directory that, if deleted, would free up enough space on the filesystem to run the
--	update. What is the total size of that directory?
--
-- Test:
--	% cd ../.. && echo -e '$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k' | cabal run 2022_day07-no-space-left-on-device # 24933642


data DiskObj = Dir String | File Int String deriving (Show)
data DiskTree = DiskDir String [DiskTree] | DiskFiles (Int, [DiskObj]) deriving (Show)

data Command = CD String | LS deriving (Show)
data Terminal = CMD Command | LsOut DiskObj deriving (Show)

isLsOut :: Terminal -> Bool
isLsOut (LsOut _) = True
isLsOut _         = False

type Parser = Parsec Void String

parseLog :: Parser [Terminal]
parseLog = many term <* optional newline <* eof

filename :: Parser String
filename = many (choice
  [ upperChar
  , lowerChar
  , char '/'
  , char '.'
  ])

term :: Parser Terminal
term = CMD <$> cmd <|> LsOut <$> diskObj

cmd :: Parser Command
cmd =
  CD <$> (string "$ cd " *> filename) <* newline
  <|> string "$ ls" $> LS <* newline

diskObj :: Parser DiskObj
diskObj =
  Dir <$> (string "dir " *> filename) <* newline
  <|> File <$> (decimal <* char ' ') <*> filename <* newline

--

addDir :: DiskTree -> DiskTree -> DiskTree
addDir (DiskDir nm dls) dl = DiskDir nm (dls ++ [dl])

addFiles :: DiskTree -> [Terminal] -> DiskTree
addFiles (DiskDir nm dls) ts = DiskDir nm (dls ++ [DiskFiles (foldl' mkDiskFiles (0, []) ts)])
  where mkDiskFiles (sz, dls) (LsOut f@(File sz' _)) = (sz + sz', dls ++ [f])
        mkDiskFiles (sz, dls) (LsOut d@(Dir _)) = (sz, dls ++ [d])

calcSize :: DiskTree -> [(String, Int)]
calcSize (DiskDir nm dls) = fst . calcSize' $ ([], (0, dls))
  where calcSize' r@(szs, (acc, [])) = r
        calcSize' (szs, (acc, dl@(DiskDir nm subs):dls)) =
          let (szs', (acc', _)) = calcSize' (szs, (0, subs))
          in calcSize' ((nm, acc'):szs', (acc + acc', dls))
        calcSize' (szs, (acc, dl@(DiskFiles (sz', _)):dls)) = calcSize' (szs, (acc + sz', dls))

totalSize  = 70000000
neededSize = 30000000

solve :: Either a [Terminal] -> Int
solve (Right terms) = deleteWhich . calcSize . fst . traverse $ (DiskDir "" [], terms)
  where traverse :: (DiskTree, [Terminal]) -> (DiskTree, [Terminal])
        traverse r@(dl, []) = r
        traverse (dl, CMD (CD ".."):ts) = (dl, ts)
        traverse (dl, CMD (CD p):ts) = traverse . first (addDir dl) . traverse $ (DiskDir p [], ts)
        traverse (dl, CMD LS:ts) = let fls = takeWhile isLsOut ts
                                       ts' = drop (length fls) ts
                                   in traverse (addFiles dl fls, ts')
        deleteWhich szmap =
          let Just totalUsed = lookup "/" szmap
              unusedSize = totalSize - totalUsed
              toFreeSize = neededSize - unusedSize
          in minimum . map snd . filter (\(_, sz) -> sz >= toFreeSize) $ szmap


main :: IO ()
main = interact $ show . solve . runParser parseLog "<stdin>"

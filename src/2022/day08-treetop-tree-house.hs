module Main where
import Data.Bifunctor (second)
import Data.Bool (bool)
import Data.List (transpose)

--	... a map with the height of each tree (your puzzle input). For example:
--
--	30373
--	25512
--	65332
--	33549
--	35390
--
--	Each tree is represented as a single digit whose value is its height, where 0 is the shortest and 9 is the tallest.
--
--	A tree is visible if all of the other trees between it and an edge of the grid are shorter than
--	it. Only consider trees in the same row or column; that is, only look up, down, left, or right from
--	any given tree.
--
--	All of the trees around the edge of the grid are visible - since they are already on the edge, there
--	are no trees to block the view. In this example, that only leaves the interior nine trees to consider:
--
--	* The top-left 5 is visible from the left and top. (It isn't visible from the right or bottom since other trees of height 5 are in the way.)
--	* The top-middle 5 is visible from the top and right.
--	* The top-right 1 is not visible from any direction; for it to be visible, there would need to only be trees of height 0 between it and an edge.
--	* The left-middle 5 is visible, but only from the right.
--	* The center 3 is not visible from any direction; for it to be visible, there would need to be only trees of at most height 2 between it and an edge.
--	* The right-middle 3 is visible from the right.
--	* In the bottom row, the middle 5 is visible, but the 3 and 4 are not.
--
--	With 16 trees visible on the edge and another 5 visible in the interior, a total of 21 trees are visible in this arrangement.
--
--	Consider your map; how many trees are visible from outside the grid?
--
-- Test:
--	% cd ../.. && echo -e '30373\n25512\n65332\n33549\n35390' | cabal run 2022_day08-treetop-tree-house # = 21

solve :: [String] -> Int
solve ss = visibleTrees
  where maxX = (length . head $ ss) - 1
        maxY = length ss - 1
        tss  = transpose ss
        get x y ns = (ns !! y) !! x
        beforeAfter x y ns = second tail . splitAt x $ ns !! y
        visible x y =
          let h = get x y ss
              (hb, ha) = beforeAfter x y ss
              (vb, va) = beforeAfter y x tss
          in maximum hb < h || maximum ha < h || maximum vb < h || maximum va < h
        visibleTrees = foldl' (\acc y -> foldl' (\acc' x -> acc' + bool 0 1 (visible x y)) acc [1..maxX-1]) (maxX * 2 + maxY * 2) [1..maxY-1]


main :: IO ()
main = interact $ show . solve . lines

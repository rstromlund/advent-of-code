module Main where
import Data.Bool (bool)
import Data.List (lookup)
import Data.Set (Set, fromList, insert, size)

--	... The CPU has a single register, X, which starts with the value 1. It supports only two instructions:
--
--	* addx V takes two cycles to complete. After two cycles, the X register is increased by the value V. (V can be negative.)
--	* noop takes one cycle to complete. It has no other effect.
--
--	... Consider the following small program:
--
--	noop
--	addx 3
--	addx -5
--	Execution of this program proceeds as follows:
--
--	* At the start of the first cycle, the noop instruction begins execution. During the first cycle, X is 1.
--	  After the first cycle, the noop instruction finishes execution, doing nothing.
--	* At the start of the second cycle, the addx 3 instruction begins execution. During the second cycle, X is still 1.
--	* During the third cycle, X is still 1. After the third cycle, the addx 3 instruction finishes execution, setting X to 4.
--	* At the start of the fourth cycle, the addx -5 instruction begins execution. During the fourth cycle, X is still 4.
--	* During the fifth cycle, X is still 4. After the fifth cycle, the addx -5 instruction finishes execution, setting X to -1.
--
-- Test:
--	% cd ../.. && echo -e 'noop\naddx 3\naddx -5' | cabal run 2022_day10-cathode-ray-tube # = 0 (where x == -1)
--	% cd ../.. && echo -e 'addx 15\naddx -11\naddx 6\naddx -3\naddx 5\naddx -1\naddx -8\naddx 13\naddx 4\nnoop\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx 5\naddx -1\naddx -35\naddx 1\naddx 24\naddx -19\naddx 1\naddx 16\naddx -11\nnoop\nnoop\naddx 21\naddx -15\nnoop\nnoop\naddx -3\naddx 9\naddx 1\naddx -3\naddx 8\naddx 1\naddx 5\nnoop\nnoop\nnoop\nnoop\nnoop\naddx -36\nnoop\naddx 1\naddx 7\nnoop\nnoop\nnoop\naddx 2\naddx 6\nnoop\nnoop\nnoop\nnoop\nnoop\naddx 1\nnoop\nnoop\naddx 7\naddx 1\nnoop\naddx -13\naddx 13\naddx 7\nnoop\naddx 1\naddx -33\nnoop\nnoop\nnoop\naddx 2\nnoop\nnoop\nnoop\naddx 8\nnoop\naddx -1\naddx 2\naddx 1\nnoop\naddx 17\naddx -9\naddx 1\naddx 1\naddx -3\naddx 11\nnoop\nnoop\naddx 1\nnoop\naddx 1\nnoop\nnoop\naddx -13\naddx -19\naddx 1\naddx 3\naddx 26\naddx -30\naddx 12\naddx -1\naddx 3\naddx 1\nnoop\nnoop\nnoop\naddx -9\naddx 18\naddx 1\naddx 2\nnoop\nnoop\naddx 9\nnoop\nnoop\nnoop\naddx -1\naddx 2\naddx -37\naddx 1\naddx 3\nnoop\naddx 15\naddx -21\naddx 22\naddx -6\naddx 1\nnoop\naddx 2\naddx 1\nnoop\naddx -10\nnoop\nnoop\naddx 20\naddx 1\naddx 2\naddx 2\naddx -6\naddx -11\nnoop\nnoop\nnoop' | cabal run 2022_day10-cathode-ray-tube # = 13140

type Cpu = (Int, Int) -- Cycles, Register X

solve :: [[String]] -> Int
solve = sum . fst . foldl' runPgm ([], (1, 1))
  where runPgm :: ([Int], Cpu) -> [String] -> ([Int], Cpu)
        runPgm state ['n':_]    = clockTick 0 state
        runPgm state ['a':_, i] = clockTick (read i) $ clockTick 0 state
        clockTick off (sgs, (cyc, x)) =
          let sgs' = bool sgs (cyc * x:sgs) $ 0 == (cyc + 20) `mod` 40
          in (sgs', (cyc + 1, x + off))


main :: IO ()
main = interact $ show . solve . map words . lines

module Main where
import Data.Bool (bool)
import Data.List as L (elem, filter, )
import Data.Maybe (isJust, fromMaybe)
import Data.Set as S (Set, empty, foldl', fromList, insert, map, member, union)

--	... The tunnels eventually open into a very tall, narrow chamber. Large, oddly-shaped rocks are
--	falling into the chamber from above, presumably due to all the rumbling. If you can't work out where
--	the rocks will fall next, you might be crushed!
--
--	The five types of rocks have the following peculiar shapes, where # is rock and . is empty space:
--
--	####
--
--	.#.
--	###
--	.#.
--
--	..#
--	..#
--	###
--
--	#
--	#
--	#
--	#
--
--	##
--	##
--
--	The rocks fall in the order shown above: first the - shape, then the + shape, and so on. Once the end
--	of the list is reached, the same order repeats: the - shape falls first, sixth, 11th, 16th, etc.
--
--	The rocks don't spin, but they do get pushed around by jets of hot gas coming out of the walls
--	themselves. A quick scan reveals the effect the jets of hot gas will have on the rocks as they fall
--	(your puzzle input).
--
--	For example, suppose this was the jet pattern in your cave:
--
--	>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
--
--	In jet patterns, < means a push to the left, while > means a push to the right. The pattern above
--	means that the jets will push a falling rock right, then right, then right, then left, then left, then
--	right, and so on. If the end of the list is reached, it repeats.
--
--	The tall, vertical chamber is exactly seven units wide. Each rock appears so that its left edge is two
--	units away from the left wall and its bottom edge is three units above the highest rock in the room
--	(or the floor, if there isn't one).
--
--	After a rock appears, it alternates between being pushed by a jet of hot gas one unit (in the
--	direction indicated by the next symbol in the jet pattern) and then falling one unit down. If any
--	movement would cause any part of the rock to move into the walls, floor, or a stopped rock, the
--	movement instead does not occur. If a downward movement would have caused a falling rock to move into
--	the floor or an already-fallen rock, the falling rock stops where it is (having landed on something)
--	and a new rock immediately begins falling.
--
--	Drawing falling rocks with @ and stopped rocks with #, the jet pattern in the example above manifests
--	as follows: ...
--
--	To prove to the elephants your simulation is accurate, they want to know how tall the tower will get
--	after 2022 rocks have stopped (but before the 2023rd rock begins falling). In this example, the tower
--	of rocks will be 3068 units tall.
--
--	How many units tall will the tower of rocks be after 2022 rocks have stopped falling?
--
-- Test:
--	% cd ../.. && echo -e '>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>' | cabal run 2022_day17-pyroclastic-flow # 3068

rocks :: [Set (Int, Int)]
rocks =
  [
    fromList [(0, 0), (1, 0), (2, 0), (3, 0)],         -- shape "-"
    fromList [
                (1, 2),
        (0, 1),         (2, 1),
                (1, 0)],                               -- shape "+"
    fromList [(0, 0), (1, 0), (2, 0), (2, 1), (2, 2)], -- shape backwards "L"
    fromList [(0, 0), (0, 1), (0, 2), (0, 3)],         -- shape "|"
    fromList [(0, 0), (1, 0), (0, 1), (1, 1)]          -- shape "*"
  ]

-- +-------+
rockyFloor = S.fromList $ zip [0..width - 1] [-1,-1..]

width     = 7 :: Int
leftEdge  = 2 :: Int
bottomGap = 3 :: Int

puzzleIterations = 2022 :: Int -- ans: 3068 / 3232
-- puzzleIterations = 5555 :: Int -- ans: 8417 / 8825
-- puzzleIterations = 10000 :: Int -- ans: ? / 15851
-- puzzleIterations = 10 :: Int -- ans: 17 / 15

getHeight :: Set (Int, Int) -> Int
getHeight = S.foldl' (\acc (_, y) -> max acc y) (-1)

solve :: [Char] -> Int
solve jets = (+1) . getHeight . fst . L.foldl' dropRocks (rockyFloor, (cycle rocks, cycle jets)) $ [1..puzzleIterations]
  where dropRocks :: (Set (Int, Int), ([Set (Int, Int)], [Char])) -> Int -> (Set (Int, Int), ([Set (Int, Int)], [Char]))
        dropRocks (cavern, (rocks, jets)) _ = dropRock (cavern, (tail rocks, jets)) . startNewRock cavern . head $ rocks
        --
        startNewRock cavern rock =
          let h = getHeight cavern
          in S.map (\(x, y) -> (x + leftEdge, y + h + bottomGap + 1)) rock
        --
        dropRock :: (Set (Int, Int), ([Set (Int, Int)], [Char])) -> Set (Int, Int) -> (Set (Int, Int), ([Set (Int, Int)], [Char]))
        dropRock (cavern, (rocks, jets)) rock =
          let jets' = head jets
              rockJ = moveByJet jets' cavern rock
              rockD = moveDown cavern $ fromMaybe rock rockJ
          in if isJust rockD then dropRock (cavern, (rocks, tail jets)) $ fromMaybe empty rockD else (cavern `union` fromMaybe rock rockJ, (rocks, tail jets))
        --
        moveByJet :: Char -> Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveByJet '<' = moveRock (-1) 0
        moveByJet '>' = moveRock   1  0
        --
        moveDown :: Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveDown = moveRock 0 1
        --
        moveRock :: Int -> Int -> Set (Int, Int) -> Set (Int, Int) -> Maybe (Set (Int, Int))
        moveRock dx dy c = S.foldl' moveRock' (Just empty)
          where moveRock' Nothing _ = Nothing
                moveRock' (Just acc) (x, y) =
                  let pt'@(x', _) = (x + dx, y - dy)
                  in if pt' `member` c || x' < 0 || x' >= width then Nothing else Just $ insert pt' acc


main :: IO ()
main = interact $ show . solve . filter (`elem` "<>")

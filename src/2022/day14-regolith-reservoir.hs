module Main where
import Data.Set (Set, empty, insert, lookupMax, lookupMin, member)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Each path appears as a single line of text in your scan. After the first point of each path, each
--	point indicates the end of a straight horizontal or vertical line to be drawn from the previous
--	point. For example:
--
--	498,4 -> 498,6 -> 496,6
--	503,4 -> 502,4 -> 502,9 -> 494,9
--
--	This scan means that there are two paths of rock; the first path consists of two straight lines, and
--	the second path consists of three straight lines. (Specifically, the first path consists of a line of
--	rock from 498,4 through 498,6 and another line of rock from 498,6 through 496,6.)
--
--	The sand is pouring into the cave from point 500,0.
--
--	Drawing rock as #, air as ., and the source of the sand as +, this becomes:
--
--
--	  4     5  5
--	  9     0  0
--	  4     0  3
--	0 ......+...
--	1 ..........
--	2 ..........
--	3 ..........
--	4 ....#...##
--	5 ....#...#.
--	6 ..###...#.
--	7 ........#.
--	8 ........#.
--	9 #########.
--
--	Sand is produced one unit at a time, and the next unit of sand is not produced until the previous unit
--	of sand comes to rest. A unit of sand is large enough to fill one tile of air in your scan.
--
--	A unit of sand always falls down one step if possible. If the tile immediately below is blocked (by
--	rock or sand), the unit of sand attempts to instead move diagonally one step down and to the left. If
--	that tile is blocked, the unit of sand attempts to instead move diagonally one step down and to the
--	right. Sand keeps moving as long as it is able to do so, at each step trying to move down, then
--	down-left, then down-right. If all three possible destinations are blocked, the unit of sand comes to
--	rest and no longer moves, at which point the next unit of sand is created back at the source.
--
--	So, drawing sand that has come to rest as o, the first unit of sand simply falls straight down and then stops:
--
--	......+...
--	..........
--	..........
--	..........
--	....#...##
--	....#...#.
--	..###...#.
--	........#.
--	......o.#.
--	#########.
--
--	The second unit of sand then falls straight down, lands on the first one, and then comes to rest to its left:
--
--	......+...
--	..........
--	..........
--	..........
--	....#...##
--	....#...#.
--	..###...#.
--	........#.
--	.....oo.#.
--	#########.
--
--	After a total of five units of sand have come to rest, they form this pattern:
--
--	......+...
--	..........
--	..........
--	..........
--	....#...##
--	....#...#.
--	..###...#.
--	......o.#.
--	....oooo#.
--	#########.
--
--	After a total of 22 units of sand:
--
--	......+...
--	..........
--	......o...
--	.....ooo..
--	....#ooo##
--	....#ooo#.
--	..###ooo#.
--	....oooo#.
--	...ooooo#.
--	#########.
--
--	Finally, only two more units of sand can possibly come to rest:
--
--	......+...
--	..........
--	......o...
--	.....ooo..
--	....#ooo##
--	...o#ooo#.
--	..###ooo#.
--	....oooo#.
--	.o.ooooo#.
--	#########.
--
--	Once all 24 units of sand shown above have come to rest, all further sand flows out the bottom,
--	falling into the endless void. Just for fun, the path any new sand takes before falling forever is
--	shown here with ~:
--
--	.......+...
--	.......~...
--	......~o...
--	.....~ooo..
--	....~#ooo##
--	...~o#ooo#.
--	..~###ooo#.
--	..~..oooo#.
--	.~o.ooooo#.
--	~#########.
--	~..........
--	~..........
--	~..........
--
--	Using your scan, simulate the falling sand. How many units of sand come to rest before sand starts flowing into the abyss below?
--
-- Test:
--	% cd ../.. && echo -e '498,4 -> 498,6 -> 496,6\n503,4 -> 502,4 -> 502,9 -> 494,9' | cabal run 2022_day14-regolith-reservoir # 24

type Point = (Int, Int)
type RockFormation = [Point]

type Parser = Parsec Void String

rforms :: Parser [RockFormation]
rforms = many rform <* optional newline <* eof

rform :: Parser RockFormation
rform = (point `sepBy` string " -> ") <* newline

point :: Parser Point
point = (,) <$> (decimal <* char ',') <*> decimal

--

drawFormations :: Either a [RockFormation] -> Set Point
drawFormations (Right rfs) = foldl' drawLines empty rfs
  where drawLines grid rf = fst . foldl' drawLine (grid, head rf) $ tail rf
        drawLine (grid, bpt@(bx, by)) ept@(ex, ey) = (draw bpt $ insert bpt grid, ept)
          where dx = signum (ex - bx)
                dy = signum (ey - by)
                draw cpt@(x, y) grid
                  | x == ex && y == ey = insert cpt grid
                  | otherwise          = draw (x + dx, y + dy) $ insert cpt grid


{-*
Rocks: [(494,9),(495,9),(496,6),(496,9),(497,6),(497,9),(498,4),(498,5),(498,6),(498,9),(499,9),(500,9),(501,9),(502,4),(502,5),(502,6),(502,7),(502,8),(502,9),(503,4)]

Sand:
                        500,2
                  499,3 500,3 501,3
                  499,4 500,4 501,4
      497,5       499,5 500,5 501,5
                  499,6 500,6 501,6
            498,7 499,7 500,7 501,7
495,8 497,8 498,8 499,8 500,8 501,8

          x=500
0 - ......+...
1 - ..........
2 - ......o...
3 - .....ooo..
4 - ....#ooo##
5 - ...o#ooo#.
6 - ..###ooo#.
7 - ....oooo#.
8 - .o.ooooo#.
9 - #########.
*-}

sourcePt = (500, 0)
rollVecs = [(0, 1), (-1, 1), (1, 1)]

solve :: Set Point -> Int
solve grid = dropSand 0 sourcePt grid rollVecs
  where Just (minX, _) = lookupMin grid
        Just (maxX, _) = lookupMax grid
        --
        dropSand :: Int -> Point -> Set Point -> [Point] -> Int
        dropSand cnt (x, y) grid rs@((dx, dy):rs')
          | x < minX || x > maxX           = cnt
          | (x + dx, y + dy) `member` grid = dropSand cnt (x, y) grid rs'
          | otherwise                      = dropSand cnt (x + dx, y + dy) grid rollVecs
        dropSand cnt cpt grid []           = dropSand (cnt + 1) sourcePt (insert cpt grid) rollVecs


main :: IO ()
main = interact $ show . solve . drawFormations . runParser rforms "<stdin>"

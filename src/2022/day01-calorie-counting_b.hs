module Main where
import Data.List (groupBy, sort)

--	... By the time you calculate the answer to the Elves' question, they've already realized that the Elf
--	carrying the most Calories of food might eventually run out of snacks.
--
--	To avoid this unacceptable situation, the Elves would instead like to know the total Calories carried by the
--	top three Elves carrying the most Calories. That way, even if one of those Elves runs out of snacks, they
--	still have two backups.
--
--	In the example above, the top three Elves are the fourth Elf (with 24000 Calories), then the third Elf (with
--	11000 Calories), then the fifth Elf (with 10000 Calories). The sum of the Calories carried by these three
--	elves is 45000.
--
--	Find the top three Elves carrying the most Calories. How many Calories are those Elves carrying in total?
--
--
-- Test:
--	% cd ../.. && echo -e '1000\n2000\n3000\n\n4000\n\n5000\n6000\n\n7000\n8000\n9000\n\n10000' | cabal run 2022_day01-calorie-counting_b # = 45000


str2int :: String -> Int
str2int "" = 0
str2int s = read s


main :: IO ()
main = interact $ show . sum . take 3 . reverse . sort . map (sum . map str2int) . groupBy (\_ b -> "" /= b) . lines

module Main where
import Data.List (filter, map)
import Data.Map.Strict ((!), Map, adjust, fromList)

--	... Your job is to work out the number the monkey named root will yell before the monkeys figure it out themselves.
--
--	For example:
--
--	root: pppw + sjmn
--	dbpl: 5
--	cczh: sllz + lgvd
--	zczc: 2
--	ptdq: humn - dvpt
--	dvpt: 3
--	lfqf: 4
--	humn: 5
--	ljgn: 2
--	sjmn: drzm * dbpl
--	sllz: 4
--	pppw: cczh / lfqf
--	lgvd: ljgn * ptdq
--	drzm: hmdt - zczc
--	hmdt: 32
--
--	Each line contains the name of a monkey, a colon, and then the job of that monkey:
--
--	* A lone number means the monkey's job is simply to yell that number.
--	* A job like aaaa + bbbb means the monkey waits for monkeys aaaa and bbbb to yell each of their numbers; the monkey then yells the sum of those two numbers.
--	* aaaa - bbbb means the monkey yells aaaa's number minus bbbb's number.
--	* Job aaaa * bbbb will yell aaaa's number multiplied by bbbb's number.
--	* Job aaaa / bbbb will yell aaaa's number divided by bbbb's number.
--
--	So, in the above example, monkey drzm has to wait for monkeys hmdt and zczc to yell their
--	numbers. Fortunately, both hmdt and zczc have jobs that involve simply yelling a single number, so
--	they do this immediately: 32 and 2. Monkey drzm can then yell its number by finding 32 minus 2: 30.
--
--	Then, monkey sjmn has one of its numbers (30, from monkey drzm), and already has its other number, 5,
--	from dbpl. This allows it to yell its own number by finding 30 multiplied by 5: 150.
--
--	This process continues until root yells a number: 152.
--
--	However, your actual situation involves considerably more monkeys. What number will the monkey named root yell?
--
-- Test:
--	% cd ../.. && echo -e 'root: pppw + sjmn\ndbpl: 5\ncczh: sllz + lgvd\nzczc: 2\nptdq: humn - dvpt\ndvpt: 3\nlfqf: 4\nhumn: 5\nljgn: 2\nsjmn: drzm * dbpl\nsllz: 4\npppw: cczh / lfqf\nlgvd: ljgn * ptdq\ndrzm: hmdt - zczc\nhmdt: 32' | cabal run 2022_day21-monkey-math # = 152

data Expression = Constant Int | Op String Char String deriving (Show, Eq, Ord)

parse :: [String] -> (String, Expression)
parse [nm, val]            = (nm, Constant $ read val)
parse [nm, nm1, [op], nm2] = (nm, Op nm1 op nm2)

solve :: Map String Expression -> Expression
solve es = eval' es "root" (es ! "root") ! "root"
  where eval' :: Map String Expression -> String -> Expression -> Map String Expression
        eval' es _ (Constant i)    = es
        eval' es k (Op nm1 op nm2) =
          let es1 = eval' es  nm1 (es  ! nm1)
              es2 = eval' es1 nm2 (es1 ! nm2)
          in adjust (exec' es2) k es2
        --
        exec' :: Map String Expression -> Expression -> Expression
        exec' es (Op nm1 op nm2)
          | '+' == op = Constant $ getVal nm1 + getVal nm2
          | '-' == op = Constant $ getVal nm1 - getVal nm2
          | '*' == op = Constant $ getVal nm1 * getVal nm2
          | '/' == op = Constant $ getVal nm1 `div` getVal nm2
          where getVal k = let (Constant i) = es ! k in i


main :: IO ()
main = interact $ show . solve . fromList . map (parse . words) . lines . filter (':' /=)

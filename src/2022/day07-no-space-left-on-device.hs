module Main where
import Data.Bifunctor (first)
import Data.Functor (($>))
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, lowerChar, newline, string, upperChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Since the disk is full, your first step should probably be to find directories
--	that are good candidates for deletion. To do this, you need to determine the total
--	size of each directory. The total size of a directory is the sum of the sizes of
--	the files it contains, directly or indirectly. (Directories themselves do not
--	count as having any intrinsic size.)
--
--	The total sizes of the directories above can be found as follows:
--
--	* The total size of directory e is 584 because it contains a single file i of size 584 and no other directories.
--	* The directory a has total size 94853 because it contains files f (size 29116), g (size 2557),
--	  and h.lst (size 62596), plus file i indirectly (a contains e which contains i).
--	* Directory d has total size 24933642.
--	* As the outermost directory, / contains every file. Its total size is 48381165, the sum of the size of every file.
--
--	To begin, find all of the directories with a total size of at most 100000, then calculate the sum of
--	their total sizes. In the example above, these directories are a and e; the sum of their total sizes
--	is 95437 (94853 + 584). (As in this example, this process can count files more than once!)
--
--	Find all of the directories with a total size of at most 100000. What is the sum of the total sizes of those directories?
--
-- Test:
--	% cd ../.. && echo -e '$ cd /\n$ ls\ndir a\n14848514 b.txt\n8504156 c.dat\ndir d\n$ cd a\n$ ls\ndir e\n29116 f\n2557 g\n62596 h.lst\n$ cd e\n$ ls\n584 i\n$ cd ..\n$ cd ..\n$ cd d\n$ ls\n4060174 j\n8033020 d.log\n5626152 d.ext\n7214296 k' | cabal run 2022_day07-no-space-left-on-device # 95437


-- NOTE: should probably have used Data.Tree here (?).
data DiskObj = Dir String | File Int String deriving (Show)
data DiskTree = DiskDir String [DiskTree] | DiskFiles (Int, [DiskObj]) deriving (Show)

data Command = CD String | LS deriving (Show)
data Terminal = CMD Command | LsOut DiskObj deriving (Show)

isLsOut :: Terminal -> Bool
isLsOut (LsOut _) = True
isLsOut _         = False

type Parser = Parsec Void String

parseLog :: Parser [Terminal]
parseLog = many term <* optional newline <* eof

filename :: Parser String
filename = many (choice
  [ upperChar
  , lowerChar
  , char '/'
  , char '.'
  ])

term :: Parser Terminal
term = CMD <$> cmd <|> LsOut <$> diskObj

cmd :: Parser Command
cmd =
  CD <$> (string "$ cd " *> filename) <* newline
  <|> string "$ ls" $> LS <* newline

diskObj :: Parser DiskObj
diskObj =
  Dir <$> (string "dir " *> filename) <* newline
  <|> File <$> (decimal <* char ' ') <*> filename <* newline

--

addDir :: DiskTree -> DiskTree -> DiskTree
addDir (DiskDir nm dls) dl = DiskDir nm (dls ++ [dl])

addFiles :: DiskTree -> [Terminal] -> DiskTree
addFiles (DiskDir nm dls) ts = DiskDir nm (dls ++ [DiskFiles (foldl' mkDiskFiles (0, []) ts)])
  where mkDiskFiles (sz, dls) (LsOut f@(File sz' _)) = (sz + sz', dls ++ [f])
        mkDiskFiles (sz, dls) (LsOut d@(Dir _)) = (sz, dls ++ [d])

calcSize :: DiskTree -> [(String, Int)]
calcSize (DiskDir nm dls) = fst . calcSize' $ ([], (0, dls))
  where calcSize' r@(szs, (acc, [])) = r
        calcSize' (szs, (acc, dl@(DiskDir nm subs):dls)) =
          let (szs', (acc', _)) = calcSize' (szs, (0, subs))
          in calcSize' ((nm, acc'):szs', (acc + acc', dls))
        calcSize' (szs, (acc, dl@(DiskFiles (sz', _)):dls)) = calcSize' (szs, (acc + sz', dls))

atMost = 100000

solve :: Either a [Terminal] -> Int
solve (Right terms) = foldl' (\acc (_, sz) -> acc + sz) 0 . filter (\(_, sz) -> sz <= atMost) . calcSize . fst . traverse $ (DiskDir "" [], terms)
  where traverse :: (DiskTree, [Terminal]) -> (DiskTree, [Terminal])
        traverse r@(dl, []) = r
        traverse (dl, CMD (CD ".."):ts) = (dl, ts)
        traverse (dl, CMD (CD p):ts) = traverse . first (addDir dl) . traverse $ (DiskDir p [], ts)
        traverse (dl, CMD LS:ts) = let fls = takeWhile isLsOut ts
                                       ts' = drop (length fls) ts
                                   in traverse (addFiles dl fls, ts')


main :: IO ()
main = interact $ show . solve . runParser parseLog "<stdin>"

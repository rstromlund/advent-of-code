{-# LANGUAGE BangPatterns #-}

{-
=== To profile/debug:

% stack --profile exec --package split --package vector-sized -- ghc day15-rambunctious-recitation_part2.hs -rtsopts -fforce-recomp
% time cd ../.. && echo -e '3,1,2' | cabal run 2020_day15-rambunctious-recitation_part2 +RTS -s

% stack exec -- ghc --package split --package vector-sized day15-rambunctious-recitation_part2.hs -rtsopts -fforce-recomp -prof
% ./day15-rambunctious-recitation_part2 +RTS -p -hc <<<'12,20,0,6,1,17,7'
% stack exec -- hp2ps -e8in -c day15-rambunctious-recitation_part2.hp
% open day15-rambunctious-recitation_part2.ps
-}

module Main where
import Data.Char (isSpace)
import Data.List.Split (splitOn)
import Data.IntMap.Strict (IntMap, findWithDefault, fromList, insert)


--	... Impressed, the Elves issue you a challenge: determine the 30000000th number spoken. For example, given the same starting numbers as above:
--
--	Given 0,3,6, the 30000000th number spoken is 175594.
--	Given 1,3,2, the 30000000th number spoken is 2578.
--	Given 2,1,3, the 30000000th number spoken is 3544142.
--	Given 1,2,3, the 30000000th number spoken is 261214.
--	Given 2,3,1, the 30000000th number spoken is 6895259.
--	Given 3,2,1, the 30000000th number spoken is 18.
--	Given 3,1,2, the 30000000th number spoken is 362.
--
--	Given your starting numbers, what will be the 30,000,000th number spoken?
--
--
-- Test:
--	% cd ../.. && echo -e '0,3,6' | cabal run 2020_day15-rambunctious-recitation_part2 # 175594
--	% cd ../.. && echo -e '1,3,2' | cabal run 2020_day15-rambunctious-recitation_part2 # 2578
--	% cd ../.. && echo -e '2,1,3' | cabal run 2020_day15-rambunctious-recitation_part2 # 3544142
--	% cd ../.. && echo -e '1,2,3' | cabal run 2020_day15-rambunctious-recitation_part2 # 261214
--	% cd ../.. && echo -e '2,3,1' | cabal run 2020_day15-rambunctious-recitation_part2 # 6895259
--	% cd ../.. && echo -e '3,2,1' | cabal run 2020_day15-rambunctious-recitation_part2 # 18
--	% cd ../.. && echo -e '3,1,2' | cabal run 2020_day15-rambunctious-recitation_part2 # 362
--
-- Go:
--	% cd ../.. && time cabal run 2020_day15-rambunctious-recitation_part2 < src/2020/day15-rambunctious-recitation.input.txt
--
-- FIXME(?): 1-3 minutes for an immutable solution w/ 30mil iterations; not bad.


solve :: [Int] -> Int
solve ns = playGame (1 + length ns) startingBoard 0
  where startingBoard = fromList $ zip ns [1..] -- each number and round they were last spoken (using IntMap for speedup over just list)
        --
        playGame :: Int -> IntMap Int -> Int -> Int
        playGame 30000000 _ spokenN = spokenN
        playGame cnt !nbrMap spokenN =
          let nextN   = cnt - findWithDefault cnt spokenN nbrMap -- Zero if unfound (cnt-cnt), else distance to round last spoken
              nextCnt = 1 + cnt
          in playGame nextCnt (insert spokenN cnt nbrMap) nextN


main :: IO ()
main = interact $ show . solve . map read . splitOn "," . filter (not . isSpace)

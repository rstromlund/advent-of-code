module Main where
import Data.IntMap.Strict (IntMap, (!), delete, empty, findWithDefault, insert, size, toList)


--	... the floor is being renovated. You can't even reach the check-in desk until they've finished installing the *new tile floor*.
--
--	The tiles are all *hexagonal*; they need to be arranged in a hex grid with a very specific color pattern. Not in the mood to
--	wait, you offer to help figure out the pattern.
--
--	The tiles are all *white* on one side and *black* on the other. They start with the white side facing up. The lobby is large
--	enough to fit whatever pattern might need to appear there.
--
--	A member of the renovation crew gives you a *list of the tiles that need to be flipped over* (your puzzle input). Each line
--	in the list identifies a single tile that needs to be flipped by giving a series of steps starting from a *reference tile*
--	in the very center of the room. (Every line starts from the same reference tile.)
--
--	Because the tiles are hexagonal, every tile has *six neighbors*: east, southeast, southwest, west, northwest, and
--	northeast. These directions are given in your list, respectively, as e, se, sw, w, nw, and ne. A tile is identified by a
--	series of these directions with *no delimiters*; for example, "esenee" identifies the tile you land on if you start at the
--	reference tile and then move one tile east, one tile southeast, one tile northeast, and one tile east.
--
--	Each time a tile is identified, it flips from white to black or from black to white. Tiles might be flipped more than
--	once. For example, a line like "esew" flips a tile immediately adjacent to the reference tile, and a line like "nwwswee" flips
--	the reference tile itself.
--
--	Here is a larger example:
--
--	sesenwnenenewseeswwswswwnenewsewsw
--	neeenesenwnwwswnenewnwwsewnenwseswesw
--	seswneswswsenwwnwse
--	nwnwneseeswswnenewneswwnewseswneseene
--	swweswneswnenwsewnwneneseenw
--	eesenwseswswnenwswnwnwsewwnwsene
--	sewnenenenesenwsewnenwwwse
--	wenwwweseeeweswwwnwwe
--	wsweesenenewnwwnwsenewsenwwsesesenwne
--	neeswseenwwswnwswswnw
--	nenwswwsewswnenenewsenwsenwnesesenew
--	enewnwewneswsewnwswenweswnenwsenwsw
--	sweneswneswneneenwnewenewwneswswnese
--	swwesenesewenwneswnwwneseswwne
--	enesenwswwswneneswsenwnewswseenwsese
--	wnwnesenesenenwwnenwsewesewsesesew
--	nenewswnwewswnenesenwnesewesw
--	eneswnwswnwsenenwnwnwwseeswneewsenese
--	neswnwewnwnwseenwseesewsenwsweewe
--	wseweeenwnesenwwwswnew
--
--	In the above example, 10 tiles are flipped once (to black), and 5 more are flipped twice (to black, then back to
--	white). After all of these instructions have been followed, a total of 10 tiles are black.
--
--	Go through the renovation crew's list and determine which tiles they need to flip. After all of the instructions have been
--	followed, how many tiles are left with the black side up?
--
-- Test:
--	% cd ../.. && echo -e 'esenee' | cabal run 2020_day24-lobby-layout
--	% cd ../.. && echo -e 'esew' | cabal run 2020_day24-lobby-layout
--	% cd ../.. && echo -e 'nwwswee' | cabal run 2020_day24-lobby-layout
--	% cd ../.. && echo -e 'esenee\nesew\nnwwswee' | cabal run 2020_day24-lobby-layout
--	% cd ../.. && echo -e 'sesenwnenenewseeswwswswwnenewsewsw\nneeenesenwnwwswnenewnwwsewnenwseswesw\nseswneswswsenwwnwse\nnwnwneseeswswnenewneswwnewseswneseene\nswweswneswnenwsewnwneneseenw\neesenwseswswnenwswnwnwsewwnwsene\nsewnenenenesenwsewnenwwwse\nwenwwweseeeweswwwnwwe\nwsweesenenewnwwnwsenewsenwwsesesenwne\nneeswseenwwswnwswswnw\nnenwswwsewswnenenewsenwsenwnesesenew\nenewnwewneswsewnwswenweswnenwsenwsw\nsweneswneswneneenwnewenewwneswswnese\nswwesenesewenwneswnwwneseswwne\nenesenwswwswneneswsenwnewswseenwsese\nwnwnesenesenenwwnenwsewesewsesesew\nnenewswnwewswnenesenwnesewesw\neneswnwswnwsenenwnwnwwseeswneewsenese\nneswnwewnwnwseenwseesewsenwsweewe\nwseweeenwnesenwwwswnew' | cabal run 2020_day24-lobby-layout


data Tile = Black | White deriving Show


-- Loading both north and east coordinates into an integer (ref. day17-conway-cubes_part2.hs)
shiftAmt  :: Int
shiftAmt  = 0x1000000

nShift = shiftAmt
eShift = 1

coordMidpoint :: Int
coordMidpoint = shiftAmt `div` 2

mkCoords :: Int -> Int -> Int
mkCoords n e = (n + coordMidpoint) * nShift + (e + coordMidpoint) * eShift

getCoords :: Int -> (Int, Int)
getCoords key = (key `div` nShift `mod` shiftAmt - coordMidpoint,
                 key              `mod` shiftAmt - coordMidpoint)


solve = (\(_, _, ts) -> size ts) . foldl' move (0, 0, empty)
  where dbg :: (Int, Int, IntMap Tile) -> (Int, Int, IntMap Tile) -- decrypt mashed up coordinates for human consumption.
        dbg (n, e, ts) = (n, e, ts)
        --
        move :: (Int, Int, IntMap Tile) -> [Char] -> (Int, Int, IntMap Tile)
        move (n, e, ts) [] = let coords = mkCoords n e
                             in case findWithDefault White coords ts of
                                  White -> (0, 0, insert coords Black ts) -- flip color
                                  Black -> (0, 0, delete coords ts)       -- don't insert the default White, keep sparse map
        move (n, e, ts) ('e':rest)     = move (n,     e + 2, ts) rest
        move (n, e, ts) ('w':rest)     = move (n,     e - 2, ts) rest
        move (n, e, ts) ('n':'e':rest) = move (n + 1, e + 1, ts) rest
        move (n, e, ts) ('n':'w':rest) = move (n + 1, e - 1, ts) rest
        move (n, e, ts) ('s':'e':rest) = move (n - 1, e + 1, ts) rest
        move (n, e, ts) ('s':'w':rest) = move (n - 1, e - 1, ts) rest


main :: IO ()
main = interact $ show . solve . lines

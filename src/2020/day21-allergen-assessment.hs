module Main where
import Data.List ((\\), groupBy, intersect, nub)


--	... You don't speak the local language, so you can't read any ingredients lists. However, sometimes, allergens are listed
--	in a language you do understand. You should be able to use this information to determine which ingredient contains which
--	allergen and work out which foods are safe to take with you on your trip.
--
--	You start by compiling a list of foods (your puzzle input), one food per line. Each line includes that food's *ingredients
--	list* followed by some or all of the allergens the food contains.
--
--	Each allergen is found in exactly one ingredient. Each ingredient contains zero or one allergen. *Allergens aren't always
--	marked*; when they're listed (as in (contains nuts, shellfish) after an ingredients list), the ingredient that contains
--	each listed allergen will be *somewhere in the corresponding ingredients list*. However, even if an allergen isn't listed,
--	the ingredient that contains that allergen could still be present: maybe they forgot to label it, or maybe it was labeled
--	in a language you don't know.
--
--	For example, consider the following list of foods:
--
--	mxmxvkd kfcds sqjhc nhms (contains dairy, fish)
--	trh fvjkl sbzzf mxmxvkd (contains dairy)
--	sqjhc fvjkl (contains soy)
--	sqjhc mxmxvkd sbzzf (contains fish)
--
--	The first food in the list has four ingredients (written in a language you don't understand): mxmxvkd, kfcds, sqjhc, and
--	nhms. While the food might contain other allergens, a few allergens the food definitely contains are listed afterward:
--	dairy and fish.
--
--	The first step is to determine which ingredients can't possibly contain any of the allergens in any food in your list. In
--	the above example, none of the ingredients kfcds, nhms, sbzzf, or trh can contain an allergen. Counting the number of
--	times any of these ingredients appear in any ingredients list produces 5: they all appear once each except sbzzf, which
--	appears twice.
--
--	Determine which ingredients cannot possibly contain any of the allergens in your list. How many times do any of those
--	ingredients appear?
--
-- Test:
--	% cd ../.. && echo -e 'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)' | cabal run 2020_day21-allergen-assessment


solve :: [String] -> Int
solve ls = length okIs
  where grped = map (groupBy (\a b -> b /= "contains") . words . filter (`notElem` "(,)")) ls
        allergenMap :: [(String, [String])] -- [("dairy",["mxmxvkd","kfcds","sqjhc","nhms"]),("fish",["mxmxvkd","kfcds","sqjhc","nhms"]),...]
        allergenMap = concatMap (\(is:as) -> [(a, is) | a <- head as, "contains" /= a]) grped
        --
        allAs = nub . map fst $ allergenMap -- ["dairy","fish","soy"]
        -- "allergenAnsMap" very much like "findSlotNames" in 'day16-ticket-translation_part2.hs'
        -- Iterate on all non-1-length allergens and reduce by known allergen/ingredient matches.
        allergenAnsMap :: [(String, [String])]
        allergenAnsMap =
          until
            (all ((1 ==) . length . snd)) -- when all slots have only 1 possibility, then we're done
            (\ps -> map (trimIs ps) ps)   -- else eliminate possibilities by removing known answers
            $ map (\a -> (a, foldl1' intersect $ map snd . filter ((== a) . fst) $ allergenMap)) allAs
        trimIs :: [(String, [String])] -> (String, [String]) -> (String, [String])
        trimIs ps slot@(_, is)
          | 1 == length is = slot                  -- if a slot has 1 possibility, then it is the answer.
          | otherwise      = foldl' reduce slot ps -- else try and eliminate possibilies by all the other known answers.
        --
        reduce :: (String, [String])-> (String, [String])-> (String, [String])
        reduce (slotA, slotIs) (_, candidateIs)
          | 1 /= length candidateIs = (slotA, slotIs)                -- multiple possible answers in the "candidate"?  We can't use it.
          | otherwise               = (slotA, slotIs \\ candidateIs) -- the "candidate" only has 1 possible answer, assuming it is correct and remove from our slot possibilities.
        --
        allIs = concatMap head grped -- ["mxmxvkd","kfcds","sqjhc","nhms","trh","fvjkl","sbzzf","mxmxvkd","sqjhc","fvjkl","sqjhc","mxmxvkd","sbzzf"]
        knownAllergenIs = concatMap snd allergenAnsMap -- ["mxmxvkd","sqjhc","fvjkl"]
        okIs  = foldl' (\acc i -> filter (/= i) acc) allIs knownAllergenIs


main :: IO ()
main = interact $ show . solve . lines

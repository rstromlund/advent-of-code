module Main where
import Data.Function (on)
import Data.List ((\\), groupBy, intersect, intercalate, nub, sortBy)


--	... Now that you've isolated the inert ingredients, you should have enough information to figure out which ingredient contains which allergen.
--
--	In the above example:
--
--	mxmxvkd contains dairy.
--	sqjhc contains fish.
--	fvjkl contains soy.
--
--	Arrange the ingredients alphabetically by their allergen and separate them by commas to produce your canonical dangerous
--	ingredient list. (There should not be any spaces in your canonical dangerous ingredient list.) In the above example, this
--	would be mxmxvkd,sqjhc,fvjkl.
--
--	Time to stock your raft with supplies. What is your canonical dangerous ingredient list?
--
-- Test:
--	% cd ../.. && echo -e 'mxmxvkd kfcds sqjhc nhms (contains dairy, fish)\ntrh fvjkl sbzzf mxmxvkd (contains dairy)\nsqjhc fvjkl (contains soy)\nsqjhc mxmxvkd sbzzf (contains fish)' | cabal run 2020_day21-allergen-assessment_part2


solve :: [String] -> String
solve ls = knownAllergens
  where grped = map (groupBy (\a b -> b /= "contains") . words . filter (`notElem` "(,)")) ls
        allergenMap :: [(String, [String])] -- [("dairy",["mxmxvkd","kfcds","sqjhc","nhms"]),("fish",["mxmxvkd","kfcds","sqjhc","nhms"]),...]
        allergenMap = concatMap (\(is:as) -> [(a, is) | a <- head as, "contains" /= a]) grped
        --
        allAs = nub . map fst $ allergenMap -- ["dairy","fish","soy"]
        -- "allergenAnsMap" very much like "findSlotNames" in 'day16-ticket-translation_part2.hs'
        -- Iterate on all non-1-length allergens and reduce by known allergen/ingredient matches.
        allergenAnsMap :: [(String, [String])]
        allergenAnsMap =
          until
            (all ((1 ==) . length . snd)) -- when all slots have only 1 possibility, then we're done
            (\ps -> map (trimIs ps) ps)   -- else eliminate possibilities by removing known answers
            $ map (\a -> (a, foldl1' intersect $ map snd . filter ((== a) . fst) $ allergenMap)) allAs
        trimIs :: [(String, [String])] -> (String, [String]) -> (String, [String])
        trimIs ps slot@(_, is)
          | 1 == length is = slot                  -- if a slot has 1 possibility, then it is the answer.
          | otherwise      = foldl' reduce slot ps -- else try and eliminate possibilies by all the other known answers.
        --
        reduce :: (String, [String])-> (String, [String])-> (String, [String])
        reduce (slotA, slotIs) (_, candidateIs)
          | 1 /= length candidateIs = (slotA, slotIs)                -- multiple possible answers in the "candidate"?  We can't use it.
          | otherwise               = (slotA, slotIs \\ candidateIs) -- the "candidate" only has 1 possible answer, assuming it is correct and remove from our slot possibilities.
        --
        knownAllergens = intercalate "," . concatMap snd . sortBy (compare `on` fst) $ allergenAnsMap


main :: IO ()
main = interact $ show . solve . lines

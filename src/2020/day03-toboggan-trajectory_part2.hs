module Main where

--	Determine the number of trees you would encounter if, for each of the following slopes, you start at the top-left corner
--	and traverse the map all the way to the bottom:
--
--	Right 1, down 1.
--	Right 3, down 1. (This is the slope you already checked.)
--	Right 5, down 1.
--	Right 7, down 1.
--	Right 1, down 2.
--
--	In the above example, these slopes would find 2, 7, 3, 4, and 2 tree(s) respectively; multiplied together, these produce the answer 336.
--
--	What do you get if you multiply together the number of trees encountered on each of the listed slopes?
--
-- Test:
--	% cd ../.. && echo -e '..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#' | cabal run 2020_day03-toboggan-trajectory_part2

solve' :: (String, Int, Int, Int, Int) -> String -> (String, Int, Int, Int, Int)
solve' (lst, col, row, spdCol, spdRow) ts = (newPath, newCol, row + 1, spdCol, spdRow)
  where width  = length ts
        newCol
          | 0 == row `mod` spdRow = (col + spdCol) `mod` width
          | otherwise             = col
        newPath
          | 0 == row `mod` spdRow = ts!!col : lst
          | otherwise             = lst

solve :: [String] -> Int
solve hill = product $ map travs [(1, 1), (3, 1), (5, 1), (7, 1), (1, 2)]
  where width = length . head $ hill
        travs (r, d) = length . filter (== '#') . (\(ts, _, _, _, _) -> ts) . foldl solve' ("", 0, 0, r, d) $ hill

main :: IO ()
main = interact $ show . solve . words

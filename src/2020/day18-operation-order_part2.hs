module Main where
import Data.Bool (bool)


--	... Now, addition and multiplication have different precedence levels, but they're not the ones you're familiar
--	with. Instead, addition is evaluated before multiplication.
--
--	For example, the steps to evaluate the expression 1 + 2 * 3 + 4 * 5 + 6 are now as follows:
--
--	1 + 2 * 3 + 4 * 5 + 6
--	  3   * 3 + 4 * 5 + 6
--	  3   *   7   * 5 + 6
--	  3   *   7   *  11
--	     21       *  11
--	         231
--	Here are the other examples from above:
--
--	1 + (2 * 3) + (4 * (5 + 6)) still becomes 51.
--	2 * 3 + (4 * 5) becomes 46.
--	5 + (8 * 3 + 9 + 3 * 4 * 3) becomes 1445.
--	5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) becomes 669060.
--	((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 becomes 23340.
--
--	What do you get if you add up the results of evaluating the homework problems using these new rules?
--
-- Test:
--	% echo '1 + 2 * 3 + 4 * 5 + 6' | ./day18-operation-order_part2.hs # 231
--	% echo '1 + (2 * 3) + (4 * (5 + 6))' | ./day18-operation-order_part2.hs # 51
--	% echo '2 * 3 + (4 * 5)' | ./day18-operation-order_part2.hs # 46
--	% echo '5 + (8 * 3 + 9 + 3 * 4 * 3)' | ./day18-operation-order_part2.hs # 1445
--	% echo '5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))' | ./day18-operation-order_part2.hs # 669060
--	% echo '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2' | ./day18-operation-order_part2.hs # 23340


-- Move (Operation '-'/'*') into separate constructors, order of precedence set by order of Constructor.
data Expression = Multiply | Add | OpenParen | CloseParen | Operand Int deriving (Eq, Ord, Show)


parse :: String -> [Expression]
parse l = fst . parseExps $ ([], breakExpr l)
  where breakExpr = break (`elem` "+-*/() ")
        -- Terminating case first
        parseExps :: ([Expression], (String, String)) -> ([Expression], (String, String))
        parseExps (exps, ("", ""))         = (reverse exps, ("", ""))
        -- Ignore spaces
        parseExps (exps, ("", ' ':es))     = parseExps (exps, breakExpr es)
        -- Operations
        parseExps (exps, ("", '+':es))     = parseExps (Add:exps, breakExpr es)
        parseExps (exps, ("", '*':es))     = parseExps (Multiply:exps, breakExpr es)
        parseExps (exps, ("", '(':es))     = parseExps (OpenParen:exps, breakExpr es)
        parseExps (exps, ("", ')':es))     = parseExps (CloseParen:exps, breakExpr es)
        -- Operands, must be a number
        parseExps (exps, (nbr, es))        = parseExps (Operand (read nbr):exps, breakExpr es)


eval :: [Expression] -> Int
eval es = (\(_, [Operand i], []) -> i) . evalExps $ (es, [], [])
  where -- Evaluate operations
        reduce inp@([], [_], []) = inp -- Nothing to be done.
        --
        reduce inp@(exps, operands, OpenParen:operations) = inp -- OpenParen is "sticky", only CloseParen removes it
        reduce inp@(exps, (Operand v2):(Operand v1):operands, Add:operations)      = (exps, Operand (v1 + v2):operands, operations)
        reduce inp@(exps, (Operand v2):(Operand v1):operands, Multiply:operations) = (exps, Operand (v1 * v2):operands, operations)
        --
        evalExps :: ([Expression], [Expression], [Expression]) -> ([Expression], [Expression], [Expression])
        -- Push operand on the stack:
        evalExps (o@(Operand _):exps, operands, operations) = evalExps (exps, o:operands, operations)
        -- If not an operand, then perform an operation:
        evalExps (op:exps, operands, operations)            = evalExps . addOp op $ (exps, operands, operations)
        -- When no more expressions, reduce on final time and exit
        evalExps e@([], operands, operations)               = until
                                                                 (\(_, _, operations) -> null operations)
                                                                 reduce
                                                                 e
        --
        -- Reduce all expressions till we remove the matching OpenParen
        addOp CloseParen (exps, operands, operations)
          = let (exps', operands', OpenParen:operations') =
                  until
                    (\(_, _, op1:_) -> OpenParen == op1)
                    reduce
                    (exps, operands, operations)
            in (exps', operands', operations')
        -- Otherwise if this operation is less or equal precedence to the first pending op, reduce the stack first
        addOp op e@(_, _:_, op1:_) | op <= op1
          = let (exps', operands', operations') = reduce e
            in (exps', operands', op:operations')
        -- Operation has higher precedence (or is the first one), put on the top of stack and continue.
        addOp op (exps, operands, operations)
          = (exps, operands, op:operations)


solve :: String -> Int
solve = eval . parse


main :: IO ()
main = interact $ show . sum . map solve . lines

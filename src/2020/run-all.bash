#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
~/DocsAndSettings*/AppData/Roaming/local/bin/hlint . | dos2unix
typeset rc=${?}

### Run all puzzle solutions:

echo -e "\n\n==== day01"
time ./day01-report-repair.hs < day01-report-repair.input.txt ; (( rc += ${?} ))
time ./day01-report-repair_part2.hs < day01-report-repair.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day02"
time ./day02-password-philosophy.hs < day02-password-philosophy.input.txt ; (( rc += ${?} ))
time ./day02-password-philosophy_part2.hs < day02-password-philosophy.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day03"
time ./day03-toboggan-trajectory.hs < day03-toboggan-trajectory.input.txt ; (( rc += ${?} ))
time ./day03-toboggan-trajectory_part2.hs < day03-toboggan-trajectory.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day04"
time ./day04-passport-processing.hs < day04-passport-processing.input.txt ; (( rc += ${?} ))
time ./day04-passport-processing_part2.hs < day04-passport-processing.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day05"
time ./day05-binary-boarding.hs < day05-binary-boarding.input.txt ; (( rc += ${?} ))
time ./day05-binary-boarding_part2.hs < day05-binary-boarding.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day06"
time ./day06-custom-customs.hs < day06-custom-customs.input.txt ; (( rc += ${?} ))
time ./day06-custom-customs_part2.hs < day06-custom-customs.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day07"
time ./day07-handy-haversacks.hs < day07-handy-haversacks.input.txt ; (( rc += ${?} ))
time ./day07-handy-haversacks_part2.hs < day07-handy-haversacks.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day08"
time ./day08-handheld-halting.hs < day08-handheld-halting.input.txt ; (( rc += ${?} ))
time ./day08-handheld-halting_part2.hs < day08-handheld-halting.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day09"
time ./day09-encoding-error.hs < day09-encoding-error.input.txt ; (( rc += ${?} ))
time ./day09-encoding-error_part2.hs < day09-encoding-error.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day10"
time ./day10-adapter-array.hs < day10-adapter-array.input.txt ; (( rc += ${?} ))
time ./day10-adapter-array_part2.hs < day10-adapter-array.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day11"
time ./day11-seating-system.hs < day11-seating-system.input.txt ; (( rc += ${?} ))
time ./day11-seating-system_part2.hs < day11-seating-system.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day12"
time ./day12-rain-risk.hs < day12-rain-risk.input.txt ; (( rc += ${?} ))
time ./day12-rain-risk_part2.hs < day12-rain-risk.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day13"
time ./day13-shuttle-search.hs < day13-shuttle-search.input.txt ; (( rc += ${?} ))
time ./day13-shuttle-search_part2.hs < day13-shuttle-search.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day14"
time ./day14-docking-data.hs < day14-docking-data.input.txt ; (( rc += ${?} ))
time ./day14-docking-data_part2.hs < day14-docking-data.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day15"
time ./day15-rambunctious-recitation.hs < day15-rambunctious-recitation.input.txt ; (( rc += ${?} ))
time ./day15-rambunctious-recitation_part2.hs < day15-rambunctious-recitation.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day16"
time ./day16-ticket-translation.hs < day16-ticket-translation.input.txt ; (( rc += ${?} ))
time ./day16-ticket-translation_part2.hs < day16-ticket-translation.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day17"
time ./day17-conway-cubes.hs < day17-conway-cubes.input.txt ; (( rc += ${?} ))
time ./day17-conway-cubes_part2.hs < day17-conway-cubes.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day18"
time ./day18-operation-order.hs < day18-operation-order.input.txt ; (( rc += ${?} ))
time ./day18-operation-order_part2.hs < day18-operation-order.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day19"
time ./day19-monster-messages.hs < day19-monster-messages.input.txt ; (( rc += ${?} ))
time ./day19-monster-messages_part2.hs < day19-monster-messages.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day20"
time ./day20-jurassic-jigsaw.hs < day20-jurassic-jigsaw.input.txt ; (( rc += ${?} ))
time ./day20-jurassic-jigsaw_part2.hs < day20-jurassic-jigsaw.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day21"
time ./day21-allergen-assessment.hs < day21-allergen-assessment.input.txt ; (( rc += ${?} ))
time ./day21-allergen-assessment_part2.hs < day21-allergen-assessment.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day22"
time ./day22-crab-combat.hs < day22-crab-combat.input.txt ; (( rc += ${?} ))
time ./day22-crab-combat_part2.hs < day22-crab-combat.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day23"
time ./day23-crab-cups.hs < day23-crab-cups.input.txt ; (( rc += ${?} ))
time ./day23-crab-cups_part2.hs < day23-crab-cups.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day24"
time ./day24-lobby-layout.hs < day24-lobby-layout.input.txt ; (( rc += ${?} ))
time ./day24-lobby-layout_part2.hs < day24-lobby-layout.input.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-combo-breaker.hs < day25-combo-breaker.input.txt ; (( rc += ${?} ))

exit ${rc}

module Main where
import Data.Bool (bool)
import Data.List.Split (chunksOf)


--	... People don't just care about adjacent seats - they care about the first seat they can see in each of those eight directions!
--
--	Now, instead of considering just the eight immediately adjacent seats, consider the first seat in each of those eight
--	directions. For example, the empty seat below would see eight occupied seats:
--
--	.......#.
--	...#.....
--	.#.......
--	.........
--	..#L....#
--	....#....
--	.........
--	#........
--	...#.....
--
--	The leftmost empty seat below would only see one empty seat, but cannot see any of the occupied ones:
--
--	.............
--	.L.L.#.#.#.#.
--	.............
--	The empty seat below would see no occupied seats:
--
--	.##.##.
--	#.#.#.#
--	##...##
--	...L...
--	##...##
--	#.#.#.#
--	.##.##.
--
--	Also, people seem to be more tolerant than you expected: it now takes five or more visible occupied seats for an occupied
--	seat to become empty (rather than four or more from the previous rules). The other rules still apply: empty seats that see
--	no occupied seats become occupied, seats matching no rule don't change, and floor never changes.
--
--	Given the same starting layout as above, these new rules cause the seating area to shift around as follows:
--
--	L.LL.LL.LL
--	LLLLLLL.LL
--	L.L.L..L..
--	LLLL.LL.LL
--	L.LL.LL.LL
--	L.LLLLL.LL
--	..L.L.....
--	LLLLLLLLLL
--	L.LLLLLL.L
--	L.LLLLL.LL
--
--	#.##.##.##
--	#######.##
--	#.#.#..#..
--	####.##.##
--	#.##.##.##
--	#.#####.##
--	..#.#.....
--	##########
--	#.######.#
--	#.#####.##
--
--	#.LL.LL.L#
--	#LLLLLL.LL
--	L.L.L..L..
--	LLLL.LL.LL
--	L.LL.LL.LL
--	L.LLLLL.LL
--	..L.L.....
--	LLLLLLLLL#
--	#.LLLLLL.L
--	#.LLLLL.L#
--
--	#.L#.##.L#
--	#L#####.LL
--	L.#.#..#..
--	##L#.##.##
--	#.##.#L.##
--	#.#####.#L
--	..#.#.....
--	LLL####LL#
--	#.L#####.L
--	#.L####.L#
--
--	#.L#.L#.L#
--	#LLLLLL.LL
--	L.L.L..#..
--	##LL.LL.L#
--	L.LL.LL.L#
--	#.LLLLL.LL
--	..L.L.....
--	LLLLLLLLL#
--	#.LLLLL#.L
--	#.L#LL#.L#
--
--	#.L#.L#.L#
--	#LLLLLL.LL
--	L.L.L..#..
--	##L#.#L.L#
--	L.L#.#L.L#
--	#.L####.LL
--	..#.#.....
--	LLL###LLL#
--	#.LLLLL#.L
--	#.L#LL#.L#
--
--	#.L#.L#.L#
--	#LLLLLL.LL
--	L.L.L..#..
--	##L#.#L.L#
--	L.L#.LL.L#
--	#.LLLL#.LL
--	..#.L.....
--	LLL###LLL#
--	#.LLLLL#.L
--	#.L#LL#.L#
--
--	Again, at this point, people stop shifting around and the seating area reaches equilibrium. Once this occurs, you count 26
--	occupied seats.
--
--	Given the new visibility method and the rule change for occupied seats becoming empty, once equilibrium is reached, how
--	many seats end up occupied?
--
-- Test:
--	% cd ../.. && echo -e 'L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL' | cabal run 2020_day11-seating-system_part2


-- FIXME:
-- Maybe this isn't too bad, given all the extra calculations, this still runs in ~1 minute.  I moved width, height, and the list comprehension
-- to solve.  If haskell didn't already optimize those, they are invariants and can be moved outside the loop.


loadPeople :: Int -> Int -> [[(Int,Int)]] -> [String] -> [String]
loadPeople width height coordMap chart = map (foldr sitDown "") coordMap
  where getChar x y
          | x < 0       = 'L' -- If getting seat outside the chart, return 'L' an empty seat.  I.e. no one sitting there and stop looking further.
          | y < 0       = 'L'
          | x >= width  = 'L'
          | y >= height = 'L'
          | otherwise   = (chart!!y)!!x

        sitDown (x,y) acc = let s = getChar x y
                                o = adjacent x y
                            in determineSeat s o:acc
        determineSeat '.' _ = '.'
        determineSeat 'L' 0 = '#'
        determineSeat '#' o
          | o >= 5          = 'L'
          | otherwise       = '#'
        determineSeat s _   = s

        adjacent x y = scanDir (-1) (-1) x y + scanDir 0 (-1) x y + scanDir 1 (-1) x y +
                       scanDir (-1)   0  x y +                      scanDir 1   0  x y +
                       scanDir (-1)   1  x y + scanDir 0   1  x y + scanDir 1   1  x y
        scanDir xdir ydir x y = occupied xdir ydir (x + xdir) (y + ydir) $ getChar (x + xdir) (y + ydir)
        occupied _ _ _ _ 'L'     = 0
        occupied _ _ _ _ '#'     = 1
        occupied xdir ydir x y _ = scanDir xdir ydir x y


--solve :: [String] -> Int
solve chart = length . filter (== '#') . concat . loopUntilEq chart $ loadPeople width height coordMap chart
  where width    = length . head $ chart
        height   = length chart
        coordMap = chunksOf width [(x,y) | y <- [0..(height - 1)], x <- [0..(width - 1)]]
        loopUntilEq old new
          | old == new = old
          | otherwise  = loopUntilEq new $ loadPeople width height coordMap new


main :: IO ()
main = interact $ show . solve . lines

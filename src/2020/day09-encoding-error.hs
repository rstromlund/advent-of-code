module Main where


--	... Here is a larger example which only considers the previous 5 numbers (and has a preamble of length 5):
--
--	35
--	20
--	15
--	25
--	47
--	40
--	62
--	55
--	65
--	95
--	102
--	117
--	150
--	182
--	127
--	219
--	299
--	277
--	309
--	576
--
--	In this example, after the 5-number preamble, almost every number is the sum of two of the previous 5 numbers; the only
--	number that does not follow this rule is 127.
--
--	The first step of attacking the weakness in the XMAS data is to find the first number in the list (after the preamble)
--	which is not the sum of two of the 25 numbers before it. What is the first number that does not have this property?
--
-- Test:
--	% cd ../.. && echo -e '35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576' | cabal run 2020_day09-encoding-error

preamble = 25

findInvalid :: [Int] -> [Int] -> Int
findInvalid _  [] = -1
findInvalid hs ts = if valid chkNbr then findInvalid (tail hs ++ [head ts]) (tail ts) else chkNbr
  where chkNbr  = head ts
        valid n = n `elem` sums
        sums    = [x + y | x <- hs, y <- hs, x /= y]


solve :: [Int] -> Int
solve ns = findInvalid hs ts
  where (hs, ts)   = splitAt preamble ns


main :: IO ()
main = interact $ show . solve . map read . words

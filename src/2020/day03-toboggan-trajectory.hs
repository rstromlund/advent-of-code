module Main where

--	... The toboggan can only follow a few specific slopes (you opted for a cheaper
--	model that prefers rational numbers); start by counting all the trees you would
--	encounter for the slope right 3, down 1:
--
-- Test:
--	% cd ../.. && echo -e '..##.......\n#...#...#..\n.#....#..#.\n..#.#...#.#\n.#...##..#.\n..#.##.....\n.#.#.#....#\n.#........#\n#.##...#...\n#...##....#\n.#..#...#.#' | cabal run 2020_day03-toboggan-trajectory

solve :: (Int, String) -> String -> (Int, String)
solve (col, lst) ts = (newCol, ts!!col : lst)
  where width  = length ts
        newCol = (col + 3) `mod` width

main :: IO ()
main = interact $ show . length . filter (== '#') . snd . foldl solve (0, "") . words

module Main where
import Data.Char (isAlpha)
import Data.List.Split (chunksOf)
import Data.Maybe (fromMaybe)


--	... consider the following rules:
--
--	light red bags contain 1 bright white bag, 2 muted yellow bags.
--	dark orange bags contain 3 bright white bags, 4 muted yellow bags.
--	bright white bags contain 1 shiny gold bag.
--	muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
--	shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
--	dark olive bags contain 3 faded blue bags, 4 dotted black bags.
--	vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
--	faded blue bags contain no other bags.
--	dotted black bags contain no other bags.
--
--	These rules specify the required contents for 9 bag types. In this example, every faded blue bag is empty, every vibrant
--	plum bag contains 11 bags (5 faded blue and 6 dotted black), and so on.
--
--	You have a shiny gold bag. If you wanted to carry it in at least one other bag, how many different bag colors would be
--	valid for the outermost bag? (In other words: how many colors can, eventually, contain at least one shiny gold bag?)
--
--	In the above rules, the following options would be available to you:
--
--		A bright white bag, which can hold your shiny gold bag directly.
--		A muted yellow bag, which can hold your shiny gold bag directly, plus some other bags.
--		A dark orange bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
--		A light red bag, which can hold bright white and muted yellow bags, either of which could then hold your shiny gold bag.
--	So, in this example, the number of bag colors that can eventually contain at least one shiny gold bag is 4.
--
--	How many bag colors can eventually contain at least one shiny gold bag?
--
-- Test:
--	% time ./day07-handy-haversacks.hs < day07-handy-haversacks.input.test


myBag = "shiny gold"

parseDescr :: String -> (String, [String])
parseDescr descr = (container, contains)
  where (adj:color:_:_:rest) = words descr
        container = unwords [adj, color]
        contains = filter (/= "no other") . map unwords . chunksOf 2 . filter (all isAlpha) $ rest

-- ^^ generates: e.g. [("light red",["bright white","muted yellow"]),
--	("dark orange",["bright white","muted yellow"]),
--	("bright white",["shiny gold"]),
--	("muted yellow",["shiny gold","faded blue"]),
--	("shiny gold",["dark olive","vibrant plum"]),
--	("dark olive",["faded blue","dotted black"]),
--	("vibrant plum",["faded blue","dotted black"]),
--	("faded blue",[]),
--	("dotted black",[])]


solve :: [String] -> Int
solve descrs = length . filter id . map lookIn $ dict
  where dict          = map parseDescr descrs
        lookIn (c,cs) = myBag `elem` cs || digIn cs -- either here in the "contained" list or recursively ...
        digIn (b:bs)  = lookIn (b, fromMaybe [] . lookup b $ dict) || digIn bs -- open each bag and look at its contents too ...
        digIn []      = False

main :: IO ()
main = interact $ show . solve . lines

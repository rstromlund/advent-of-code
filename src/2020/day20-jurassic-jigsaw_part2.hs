module Main where
import Data.Bits ((.&.))
import Data.Bool (bool)
import Data.Function (on)
import Data.List (groupBy, sortBy, transpose)
import Data.List.Extra (groupOn)


--	... Now, you're ready to check the image for sea monsters.
--
--	The borders of each tile are not part of the actual image; start by removing them.
--
--	In the example above, the tiles become:
--
--	.#.#..#. ##...#.# #..#####
--	###....# .#....#. .#......
--	##.##.## #.#.#..# #####...
--	###.#### #...#.## ###.#..#
--	##.#.... #.##.### #...#.##
--	...##### ###.#... .#####.#
--	....#..# ...##..# .#.###..
--	.####... #..#.... .#......
--
--	#..#.##. .#..###. #.##....
--	#.####.. #.####.# .#.###..
--	###.#.#. ..#.#### ##.#..##
--	#.####.. ..##..## ######.#
--	##..##.# ...#...# .#.#.#..
--	...#..#. .#.#.##. .###.###
--	.#.#.... #.##.#.. .###.##.
--	###.#... #..#.##. ######..
--
--	.#.#.### .##.##.# ..#.##..
--	.####.## #.#...## #.#..#.#
--	..#.#..# ..#.#.#. ####.###
--	#..####. ..#.#.#. ###.###.
--	#####..# ####...# ##....##
--	#.##..#. .#...#.. ####...#
--	.#.###.. ##..##.. ####.##.
--	...###.. .##...#. ..#..###
--
--	Remove the gaps to form the actual image:
--
--	.#.#..#.##...#.##..#####
--	###....#.#....#..#......
--	##.##.###.#.#..######...
--	###.#####...#.#####.#..#
--	##.#....#.##.####...#.##
--	...########.#....#####.#
--	....#..#...##..#.#.###..
--	.####...#..#.....#......
--	#..#.##..#..###.#.##....
--	#.####..#.####.#.#.###..
--	###.#.#...#.######.#..##
--	#.####....##..########.#
--	##..##.#...#...#.#.#.#..
--	...#..#..#.#.##..###.###
--	.#.#....#.##.#...###.##.
--	###.#...#..#.##.######..
--	.#.#.###.##.##.#..#.##..
--	.####.###.#...###.#..#.#
--	..#.#..#..#.#.#.####.###
--	#..####...#.#.#.###.###.
--	#####..#####...###....##
--	#.##..#..#...#..####...#
--	.#.###..##..##..####.##.
--	...###...##...#...#..###
--
--	Now, you're ready to search for sea monsters! Because your image is monochrome, a sea monster will look like this:
--
--	                  #
--	#    ##    ##    ###
--	 #  #  #  #  #  #
--
--	When looking for this pattern in the image, the spaces can be anything; only the # need to match. Also, you might need to
--	rotate or flip your image before it's oriented correctly to find sea monsters. In the above image, after flipping and
--	rotating it to the appropriate orientation, there are two sea monsters (marked with O):
--
--	.####...#####..#...###..
--	#####..#..#.#.####..#.#.
--	.#.#...#.###...#.##.O#..
--	#.O.##.OO#.#.OO.##.OOO##
--	..#O.#O#.O##O..O.#O##.##
--	...#.#..##.##...#..#..##
--	#.##.#..#.#..#..##.#.#..
--	.###.##.....#...###.#...
--	#.####.#.#....##.#..#.#.
--	##...#..#....#..#...####
--	..#.##...###..#.#####..#
--	....#.##.#.#####....#...
--	..##.##.###.....#.##..#.
--	#...#...###..####....##.
--	.#.##...#.##.#.#.###...#
--	#.###.#..####...##..#...
--	#.###...#.##...#.##O###.
--	.O##.#OO.###OO##..OOO##.
--	..O#.O..O..O.#O##O##.###
--	#.#..##.########..#..##.
--	#.#####..#.#...##..#....
--	#....##..#.#########..##
--	#...#.....#..##...###.##
--	#..###....##.#...##.##.#
--
--	Determine how rough the waters are in the sea monsters' habitat by counting the number of # that are not part of a sea
--	monster. In the above example, the habitat's water roughness is 273.
--
--	How many # are not part of a sea monster?
--
-- Test:
--	% ./day20-jurassic-jigsaw_part2.hs < day20-jurassic-jigsaw.test.txt


monster = [
  "                  # "
 ,"#    ##    ##    ###"
 ," #  #  #  #  #  #   "
 ]


data TileImg = TileImg { nbr :: Int
                       , top :: String
                       , left :: String
                       , bottom :: String
                       , right :: String
                       , img :: [String]
                       } deriving Show


mkTileImg :: Int -> [String] -> TileImg
mkTileImg nbr img = TileImg nbr top left bottom right img
  where top    = head img
        bottom = reverse . last $ img
        timg   = transpose img
        left   = reverse . head $ timg
        right  = last timg


flipTileImg :: TileImg -> TileImg
flipTileImg (TileImg nbr t l b r img) = TileImg nbr ft fl fb fr fimg
  where fimg = reverse img
        ft   = reverse b
        fb   = reverse t
        fl   = reverse l
        fr   = reverse r


rightOf :: TileImg -> TileImg -> Bool
rightOf r@(TileImg rnbr _ rl _ _ _) l@(TileImg lnbr _ _ _ lr _) = rnbr /= lnbr && rl == reverse lr


isUnder :: TileImg -> TileImg -> Bool
isUnder (TileImg lnbr lt _ _ _ _) (TileImg unbr _ _ ub _ _) = lnbr /= unbr && lt == reverse ub


allRotations :: [(String, [String])] -> [TileImg]
allRotations (t:tiles) =
  let ('T':'i':'l':'e':' ':nbrStr, txtImg) = t
      nbr    = read . init $ nbrStr
      --
      rot0   = mkTileImg nbr txtImg
      rot1   = mkTileImg nbr $ transpose . reverse $ txtImg
      rot2   = mkTileImg nbr $ transpose . reverse $ img rot1
      rot3   = mkTileImg nbr $ transpose . reverse $ img rot2
  in -- all rotations
    rot0:rot1:rot2:rot3:
    -- all flips
    flipTileImg rot0:flipTileImg rot1:flipTileImg rot2:flipTileImg rot3:
    allRotations tiles
allRotations [] = []


findMonster :: [String] -> Int
findMonster img = iterRows img
  where rowsCols = length img
        mrows = length monster
        mcols = length . head $ monster
        mBits = calcBits monster -- ie. 2, 549255, 299592
        calcBits :: [String] -> [Int]
        calcBits strs = map (foldl' (\acc ch -> acc * 2 + bool 0 1 ('#' == ch)) 0) strs
        --
        iterRows (r1:r2:r3:rs) = iterCols 0 [r1,r2,r3] + iterRows (r2:r3:rs)
        iterRows _ = 0
        --
        iterCols cnt rows@[r1,r2,r3]
          | cnt + mcols > rowsCols = 0
          | otherwise              = ckMonster (map (take mcols) rows) + iterCols (cnt + 1) [drop 1 r1, drop 1 r2, drop 1 r3]
        ckMonster croppedRows      =
          let pzBits = calcBits croppedRows
              eureka = mBits == zipWith (.&.) mBits pzBits
          in bool 0 1 eureka


solve :: [(String, [String])] -> Int
solve tiles = pzHashes - monsterCnt * monsterHashes
  where tileCnt = length tiles
        allPossibleImgs = allRotations tiles
        -- uniqueEdges :: one tile w/ 4 uniques = corner; 2 uniques = side
        uniqueEdges :: [[TileImg]]
        uniqueEdges = filter ((==1) . length) . groupOn top . sortBy (compare `on` top) $ allPossibleImgs
        -- cornerTiles is a list of all corners of all valid rotations of the image (which is good b/c the monster may only show up on certain rotations)
        cornerTiles = concat . filter ((==4) . length) . groupOn nbr . sortBy (compare `on` nbr) . concat $ uniqueEdges
        -- look for tiles "under" a corner one until no more are found; this is the left side of the image
        firstCol :: [TileImg] -> [TileImg]
        firstCol (t1:ts) =
          case filter (`isUnder` t1) allPossibleImgs of
            [under] -> firstCol $ under:t1:ts
            _       -> reverse $ t1:ts
        -- given the left column tile, find tiles that match to the right until no more are found
        rightCol :: [TileImg] -> [TileImg]
        rightCol (t1:ts) =
          let rs = filter (`rightOf` t1) allPossibleImgs
          in case rs of
               [right] -> rightCol $ right:t1:ts
               _       -> reverse $ t1:ts
        --
        col1s = map (\t -> firstCol [t]) cornerTiles
        grids :: [[[TileImg]]]
        grids = map (map (\t -> rightCol [t])) col1s -- map all the resulting left hand sides and fill in the puzzle
        imgSols = filter ((== tileCnt) . sum . map length) grids -- all the reconstructed image solutions (images that use all tiles)
        --
        trimImg t = map (init . tail) $ init . tail $ img t
        justImgs = map (map (map trimImg)) imgSols
        joinedImgs :: [[String]]
        joinedImgs = map (concatMap (foldl1' (zipWith (++)))) justImgs
        --
        monsterCnt = maximum . map findMonster $ joinedImgs
        monsterHashes = length . filter ('#' ==) . concat $ monster
        pzHashes = length . filter ('#' ==) . concat $ head joinedImgs


main :: IO ()
main = interact $ show . solve . map ((\grp -> (head grp, tail grp)) . filter (/= "")) . groupBy (\a b -> "" /= b) . lines

module Main where
import Data.List (groupBy, transpose)


--	... The camera array consists of many cameras; rather than produce a single square image, they produce many smaller square
--	image tiles that need to be reassembled back into a single image.
--
--	Each camera in the camera array returns a single monochrome image tile with a random unique ID number. The tiles (your
--	puzzle input) arrived in a random order.
--
--	Worse yet, the camera array appears to be malfunctioning: each image tile has been rotated and flipped to a random
--	orientation. Your first task is to reassemble the original image by orienting the tiles so they fit together.
--
--	To show how the tiles should be reassembled, each tile's image data includes a border that should line up exactly with its
--	adjacent tiles. All tiles have this border, and the border lines up exactly when the tiles are both oriented
--	correctly. Tiles at the edge of the image also have this border, but the outermost edges won't line up with any other
--	tiles.
--
--	For example, suppose you have the following nine tiles:
--
--	Tile 2311:
--	..##.#..#.
--	##..#.....
--	#...##..#.
--	####.#...#
--	##.##.###.
--	##...#.###
--	.#.#.#..##
--	..#....#..
--	###...#.#.
--	..###..###
--
--	Tile 1951:
--	#.##...##.
--	#.####...#
--	.....#..##
--	#...######
--	.##.#....#
--	.###.#####
--	###.##.##.
--	.###....#.
--	..#.#..#.#
--	#...##.#..
--
--	Tile 1171:
--	####...##.
--	#..##.#..#
--	##.#..#.#.
--	.###.####.
--	..###.####
--	.##....##.
--	.#...####.
--	#.##.####.
--	####..#...
--	.....##...
--
--	Tile 1427:
--	###.##.#..
--	.#..#.##..
--	.#.##.#..#
--	#.#.#.##.#
--	....#...##
--	...##..##.
--	...#.#####
--	.#.####.#.
--	..#..###.#
--	..##.#..#.
--
--	Tile 1489:
--	##.#.#....
--	..##...#..
--	.##..##...
--	..#...#...
--	#####...#.
--	#..#.#.#.#
--	...#.#.#..
--	##.#...##.
--	..##.##.##
--	###.##.#..
--
--	Tile 2473:
--	#....####.
--	#..#.##...
--	#.##..#...
--	######.#.#
--	.#...#.#.#
--	.#########
--	.###.#..#.
--	########.#
--	##...##.#.
--	..###.#.#.
--
--	Tile 2971:
--	..#.#....#
--	#...###...
--	#.#.###...
--	##.##..#..
--	.#####..##
--	.#..####.#
--	#..#.#..#.
--	..####.###
--	..#.#.###.
--	...#.#.#.#
--
--	Tile 2729:
--	...#.#.#.#
--	####.#....
--	..#.#.....
--	....#..#.#
--	.##..##.#.
--	.#.####...
--	####.#.#..
--	##.####...
--	##..#.##..
--	#.##...##.
--
--	Tile 3079:
--	#.#.#####.
--	.#..######
--	..#.......
--	######....
--	####.#..#.
--	.#...#.##.
--	#.#####.##
--	..#.###...
--	..#.......
--	..#.###...
--
--	By rotating, flipping, and rearranging them, you can find a square arrangement that causes all adjacent borders to line up:
--
--	#...##.#.. ..###..### #.#.#####.
--	..#.#..#.# ###...#.#. .#..######
--	.###....#. ..#....#.. ..#.......
--	###.##.##. .#.#.#..## ######....
--	.###.##### ##...#.### ####.#..#.
--	.##.#....# ##.##.###. .#...#.##.
--	#...###### ####.#...# #.#####.##
--	.....#..## #...##..#. ..#.###...
--	#.####...# ##..#..... ..#.......
--	#.##...##. ..##.#..#. ..#.###...
--
--	#.##...##. ..##.#..#. ..#.###...
--	##..#.##.. ..#..###.# ##.##....#
--	##.####... .#.####.#. ..#.###..#
--	####.#.#.. ...#.##### ###.#..###
--	.#.####... ...##..##. .######.##
--	.##..##.#. ....#...## #.#.#.#...
--	....#..#.# #.#.#.##.# #.###.###.
--	..#.#..... .#.##.#..# #.###.##..
--	####.#.... .#..#.##.. .######...
--	...#.#.#.# ###.##.#.. .##...####
--
--	...#.#.#.# ###.##.#.. .##...####
--	..#.#.###. ..##.##.## #..#.##..#
--	..####.### ##.#...##. .#.#..#.##
--	#..#.#..#. ...#.#.#.. .####.###.
--	.#..####.# #..#.#.#.# ####.###..
--	.#####..## #####...#. .##....##.
--	##.##..#.. ..#...#... .####...#.
--	#.#.###... .##..##... .####.##.#
--	#...###... ..##...#.. ...#..####
--	..#.#....# ##.#.#.... ...##.....
--
--	For reference, the IDs of the above tiles are:
--
--	1951    2311    3079
--	2729    1427    2473
--	2971    1489    1171
--
--	To check that you've assembled the image correctly, multiply the IDs of the four corner tiles together. If you do this
--	with the assembled tiles from the example above, you get 1951 * 3079 * 2971 * 1171 = 20899048083289.
--
--	Assemble the tiles into an image. What do you get if you multiply together the IDs of the four corner tiles?
--
-- Test:
--	% cd ../.. && cabal run 2020_day20-jurassic-jigsaw < src/2020/day20-jurassic-jigsaw.test.txt
-- Go:
--	% cd ../.. && cabal run 2020_day20-jurassic-jigsaw < src/2020/day20-jurassic-jigsaw.input.txt


-- FIXME: Arrrrrrgh!  I started this list comprehension solution after I skipped reading the "produce a single square image" part.  I assumed a 3x3 answer.
--  I used macros to brute force the 3x3 solution into a huge 12x12 lc expression.  It is fast though.
--
-- Aaaaaand: since I only keep the 4 edges and drop the full "image", this won't work to find dragons in part 2.   : /


rightOf :: (Int, [String]) -> (Int, [String]) -> Bool
rightOf r@(rnm, [rt, rl, rb, rr]) l@(lnm, [lt, ll, lb, lr]) = rnm /= lnm && rl == reverse lr


isUnder :: (Int, [String]) -> (Int, [String]) -> Bool
isUnder l@(lnm, [lt, ll, lb, lr]) u@(unm, [ut, ul, ub, ur]) = lnm /= unm && lt == reverse ub


filterNbr :: Int -> [(Int,[String])] -> [(Int,[String])]
filterNbr key = filter (\(nbr, _) -> key == nbr)


solve :: [(String, [String])] -> Int
solve tiles = puzzleAns -- return 'testAns' for the 3x3 test input file.
  where testAns = head . map ((\a -> head a * a!!2 * a!!6 * a!!8) . map fst) $ combos3x3
        puzzleAns = head . map ((\a -> head a * a!!11 * a!!132 * a!!143) . map fst) $ combos
        --
        allPossibleImgs = rotate tiles
        --
        rotate (t:tiles) = let ('T':'i':'l':'e':' ':nm, img) = t
                               timg   = transpose img
                               nbr :: Int
                               nbr    = read . init $ nm
                               top    = head img
                               bottom = reverse . last $ img
                               left   = reverse . head $ timg
                               right  = last timg
                               dirs   = cycle [top, left, bottom, right]
                               flip [t, l, b, r] = [reverse b, reverse l, reverse t, reverse r]
                           in -- rotate
                               (nbr, take 4 dirs):
                               (nbr, take 4 $ drop 1 dirs):
                               (nbr, take 4 $ drop 2 dirs):
                               (nbr, take 4 $ drop 3 dirs):
                               -- flip
                               (nbr, flip . take 4 $ dirs):
                               (nbr, flip . take 4 . drop 1 $ dirs):
                               (nbr, flip . take 4 . drop 2 $ dirs):
                               (nbr, flip . take 4 . drop 3 $ dirs):
                               rotate tiles
        rotate []        = []
        -- (Whoops, see the FIXME above.  Keeping this for testing.)
        combos3x3 = [
          [i0, i1, i2, i3, i4, i5, i6, i7, i8]
          | i0 <- allPossibleImgs,
            i1 <- filter (`rightOf` i0) allPossibleImgs,
            i2 <- filter (`rightOf` i1) allPossibleImgs,

            i3 <- filter (        `isUnder` i0) allPossibleImgs,
            i4 <- filter (\i -> i `isUnder` i1 && i `rightOf` i3) allPossibleImgs,
            i5 <- filter (\i -> i `isUnder` i2 && i `rightOf` i4) allPossibleImgs,

            i6 <- filter (        `isUnder` i3) allPossibleImgs,
            i7 <- filter (\i -> i `isUnder` i4 && i `rightOf` i6) allPossibleImgs,
            i8 <- filter (\i -> i `isUnder` i5 && i `rightOf` i7) allPossibleImgs,
            ---- Ensure we haven't reused a tile# (despite rotation)
            fst i1 /= fst i0 &&
            fst i2 /= fst i0 && fst i2 /= fst i1 &&
            fst i3 /= fst i0 && fst i3 /= fst i1 && fst i3 /= fst i2 &&
            fst i4 /= fst i0 && fst i4 /= fst i1 && fst i4 /= fst i2 && fst i4 /= fst i3 &&
            fst i5 /= fst i0 && fst i5 /= fst i1 && fst i5 /= fst i2 && fst i5 /= fst i3 && fst i5 /= fst i4 &&
            fst i6 /= fst i0 && fst i6 /= fst i1 && fst i6 /= fst i2 && fst i6 /= fst i3 && fst i6 /= fst i4 && fst i6 /= fst i5 &&
            fst i7 /= fst i0 && fst i7 /= fst i1 && fst i7 /= fst i2 && fst i7 /= fst i3 && fst i7 /= fst i4 && fst i7 /= fst i5 && fst i7 /= fst i6 &&
            fst i8 /= fst i0 && fst i8 /= fst i1 && fst i8 /= fst i2 && fst i8 /= fst i3 && fst i8 /= fst i4 && fst i8 /= fst i5 && fst i8 /= fst i6 && fst i8 /= fst i7
          ]
        --
        -- 12x12 solution, not generalizable to NxN solutions : /
        combos = [
          [i00, i01, i02, i03, i04, i05, i06, i07, i08, i09, i0a, i0b,
           i10, i11, i12, i13, i14, i15, i16, i17, i18, i19, i1a, i1b,
           i20, i21, i22, i23, i24, i25, i26, i27, i28, i29, i2a, i2b,
           i30, i31, i32, i33, i34, i35, i36, i37, i38, i39, i3a, i3b,
           i40, i41, i42, i43, i44, i45, i46, i47, i48, i49, i4a, i4b,
           i50, i51, i52, i53, i54, i55, i56, i57, i58, i59, i5a, i5b,
           i60, i61, i62, i63, i64, i65, i66, i67, i68, i69, i6a, i6b,
           i70, i71, i72, i73, i74, i75, i76, i77, i78, i79, i7a, i7b,
           i80, i81, i82, i83, i84, i85, i86, i87, i88, i89, i8a, i8b,
           i90, i91, i92, i93, i94, i95, i96, i97, i98, i99, i9a, i9b,
           ia0, ia1, ia2, ia3, ia4, ia5, ia6, ia7, ia8, ia9, iaa, iab,
           ib0, ib1, ib2, ib3, ib4, ib5, ib6, ib7, ib8, ib9, iba, ibb
          ]
          | i00 <- allPossibleImgs,
            i01 <- filter ( `rightOf` i00) allPossibleImgs,
            i02 <- filter (                           `rightOf` i01) allPossibleImgs,
            i03 <- filter (                           `rightOf` i02) allPossibleImgs,
            i04 <- filter (                           `rightOf` i03) allPossibleImgs,
            i05 <- filter (                           `rightOf` i04) allPossibleImgs,
            i06 <- filter (                           `rightOf` i05) allPossibleImgs,
            i07 <- filter (                           `rightOf` i06) allPossibleImgs,
            i08 <- filter (                           `rightOf` i07) allPossibleImgs,
            i09 <- filter (                           `rightOf` i08) allPossibleImgs,
            i0a <- filter (                           `rightOf` i09) allPossibleImgs,
            i0b <- filter (                           `rightOf` i0a) allPossibleImgs,

            i10 <- filter (                           `isUnder` i00) allPossibleImgs,
            i11 <- filter (\i -> i `rightOf` i10 && i `isUnder` i01) allPossibleImgs,
            i12 <- filter (\i -> i `rightOf` i11 && i `isUnder` i02) allPossibleImgs,
            i13 <- filter (\i -> i `rightOf` i12 && i `isUnder` i03) allPossibleImgs,
            i14 <- filter (\i -> i `rightOf` i13 && i `isUnder` i04) allPossibleImgs,
            i15 <- filter (\i -> i `rightOf` i14 && i `isUnder` i05) allPossibleImgs,
            i16 <- filter (\i -> i `rightOf` i15 && i `isUnder` i06) allPossibleImgs,
            i17 <- filter (\i -> i `rightOf` i16 && i `isUnder` i07) allPossibleImgs,
            i18 <- filter (\i -> i `rightOf` i17 && i `isUnder` i08) allPossibleImgs,
            i19 <- filter (\i -> i `rightOf` i18 && i `isUnder` i09) allPossibleImgs,
            i1a <- filter (\i -> i `rightOf` i19 && i `isUnder` i0a) allPossibleImgs,
            i1b <- filter (\i -> i `rightOf` i1a && i `isUnder` i0b) allPossibleImgs,

            i20 <- filter (                           `isUnder` i10) allPossibleImgs,
            i21 <- filter (\i -> i `rightOf` i20 && i `isUnder` i11) allPossibleImgs,
            i22 <- filter (\i -> i `rightOf` i21 && i `isUnder` i12) allPossibleImgs,
            i23 <- filter (\i -> i `rightOf` i22 && i `isUnder` i13) allPossibleImgs,
            i24 <- filter (\i -> i `rightOf` i23 && i `isUnder` i14) allPossibleImgs,
            i25 <- filter (\i -> i `rightOf` i24 && i `isUnder` i15) allPossibleImgs,
            i26 <- filter (\i -> i `rightOf` i25 && i `isUnder` i16) allPossibleImgs,
            i27 <- filter (\i -> i `rightOf` i26 && i `isUnder` i17) allPossibleImgs,
            i28 <- filter (\i -> i `rightOf` i27 && i `isUnder` i18) allPossibleImgs,
            i29 <- filter (\i -> i `rightOf` i28 && i `isUnder` i19) allPossibleImgs,
            i2a <- filter (\i -> i `rightOf` i29 && i `isUnder` i1a) allPossibleImgs,
            i2b <- filter (\i -> i `rightOf` i2a && i `isUnder` i1b) allPossibleImgs,

            i30 <- filter (                           `isUnder` i20) allPossibleImgs,
            i31 <- filter (\i -> i `rightOf` i30 && i `isUnder` i21) allPossibleImgs,
            i32 <- filter (\i -> i `rightOf` i31 && i `isUnder` i22) allPossibleImgs,
            i33 <- filter (\i -> i `rightOf` i32 && i `isUnder` i23) allPossibleImgs,
            i34 <- filter (\i -> i `rightOf` i33 && i `isUnder` i24) allPossibleImgs,
            i35 <- filter (\i -> i `rightOf` i34 && i `isUnder` i25) allPossibleImgs,
            i36 <- filter (\i -> i `rightOf` i35 && i `isUnder` i26) allPossibleImgs,
            i37 <- filter (\i -> i `rightOf` i36 && i `isUnder` i27) allPossibleImgs,
            i38 <- filter (\i -> i `rightOf` i37 && i `isUnder` i28) allPossibleImgs,
            i39 <- filter (\i -> i `rightOf` i38 && i `isUnder` i29) allPossibleImgs,
            i3a <- filter (\i -> i `rightOf` i39 && i `isUnder` i2a) allPossibleImgs,
            i3b <- filter (\i -> i `rightOf` i3a && i `isUnder` i2b) allPossibleImgs,

            i40 <- filter (                           `isUnder` i30) allPossibleImgs,
            i41 <- filter (\i -> i `rightOf` i40 && i `isUnder` i31) allPossibleImgs,
            i42 <- filter (\i -> i `rightOf` i41 && i `isUnder` i32) allPossibleImgs,
            i43 <- filter (\i -> i `rightOf` i42 && i `isUnder` i33) allPossibleImgs,
            i44 <- filter (\i -> i `rightOf` i43 && i `isUnder` i34) allPossibleImgs,
            i45 <- filter (\i -> i `rightOf` i44 && i `isUnder` i35) allPossibleImgs,
            i46 <- filter (\i -> i `rightOf` i45 && i `isUnder` i36) allPossibleImgs,
            i47 <- filter (\i -> i `rightOf` i46 && i `isUnder` i37) allPossibleImgs,
            i48 <- filter (\i -> i `rightOf` i47 && i `isUnder` i38) allPossibleImgs,
            i49 <- filter (\i -> i `rightOf` i48 && i `isUnder` i39) allPossibleImgs,
            i4a <- filter (\i -> i `rightOf` i49 && i `isUnder` i3a) allPossibleImgs,
            i4b <- filter (\i -> i `rightOf` i4a && i `isUnder` i3b) allPossibleImgs,

            i50 <- filter (                           `isUnder` i40) allPossibleImgs,
            i51 <- filter (\i -> i `rightOf` i50 && i `isUnder` i41) allPossibleImgs,
            i52 <- filter (\i -> i `rightOf` i51 && i `isUnder` i42) allPossibleImgs,
            i53 <- filter (\i -> i `rightOf` i52 && i `isUnder` i43) allPossibleImgs,
            i54 <- filter (\i -> i `rightOf` i53 && i `isUnder` i44) allPossibleImgs,
            i55 <- filter (\i -> i `rightOf` i54 && i `isUnder` i45) allPossibleImgs,
            i56 <- filter (\i -> i `rightOf` i55 && i `isUnder` i46) allPossibleImgs,
            i57 <- filter (\i -> i `rightOf` i56 && i `isUnder` i47) allPossibleImgs,
            i58 <- filter (\i -> i `rightOf` i57 && i `isUnder` i48) allPossibleImgs,
            i59 <- filter (\i -> i `rightOf` i58 && i `isUnder` i49) allPossibleImgs,
            i5a <- filter (\i -> i `rightOf` i59 && i `isUnder` i4a) allPossibleImgs,
            i5b <- filter (\i -> i `rightOf` i5a && i `isUnder` i4b) allPossibleImgs,

            i60 <- filter (                           `isUnder` i50) allPossibleImgs,
            i61 <- filter (\i -> i `rightOf` i60 && i `isUnder` i51) allPossibleImgs,
            i62 <- filter (\i -> i `rightOf` i61 && i `isUnder` i52) allPossibleImgs,
            i63 <- filter (\i -> i `rightOf` i62 && i `isUnder` i53) allPossibleImgs,
            i64 <- filter (\i -> i `rightOf` i63 && i `isUnder` i54) allPossibleImgs,
            i65 <- filter (\i -> i `rightOf` i64 && i `isUnder` i55) allPossibleImgs,
            i66 <- filter (\i -> i `rightOf` i65 && i `isUnder` i56) allPossibleImgs,
            i67 <- filter (\i -> i `rightOf` i66 && i `isUnder` i57) allPossibleImgs,
            i68 <- filter (\i -> i `rightOf` i67 && i `isUnder` i58) allPossibleImgs,
            i69 <- filter (\i -> i `rightOf` i68 && i `isUnder` i59) allPossibleImgs,
            i6a <- filter (\i -> i `rightOf` i69 && i `isUnder` i5a) allPossibleImgs,
            i6b <- filter (\i -> i `rightOf` i6a && i `isUnder` i5b) allPossibleImgs,

            i70 <- filter (                           `isUnder` i60) allPossibleImgs,
            i71 <- filter (\i -> i `rightOf` i70 && i `isUnder` i61) allPossibleImgs,
            i72 <- filter (\i -> i `rightOf` i71 && i `isUnder` i62) allPossibleImgs,
            i73 <- filter (\i -> i `rightOf` i72 && i `isUnder` i63) allPossibleImgs,
            i74 <- filter (\i -> i `rightOf` i73 && i `isUnder` i64) allPossibleImgs,
            i75 <- filter (\i -> i `rightOf` i74 && i `isUnder` i65) allPossibleImgs,
            i76 <- filter (\i -> i `rightOf` i75 && i `isUnder` i66) allPossibleImgs,
            i77 <- filter (\i -> i `rightOf` i76 && i `isUnder` i67) allPossibleImgs,
            i78 <- filter (\i -> i `rightOf` i77 && i `isUnder` i68) allPossibleImgs,
            i79 <- filter (\i -> i `rightOf` i78 && i `isUnder` i69) allPossibleImgs,
            i7a <- filter (\i -> i `rightOf` i79 && i `isUnder` i6a) allPossibleImgs,
            i7b <- filter (\i -> i `rightOf` i7a && i `isUnder` i6b) allPossibleImgs,

            i80 <- filter ( `isUnder` i70) allPossibleImgs,
            i81 <- filter (\i -> i `rightOf` i80 && i `isUnder` i71) allPossibleImgs,
            i82 <- filter (\i -> i `rightOf` i81 && i `isUnder` i72) allPossibleImgs,
            i83 <- filter (\i -> i `rightOf` i82 && i `isUnder` i73) allPossibleImgs,
            i84 <- filter (\i -> i `rightOf` i83 && i `isUnder` i74) allPossibleImgs,
            i85 <- filter (\i -> i `rightOf` i84 && i `isUnder` i75) allPossibleImgs,
            i86 <- filter (\i -> i `rightOf` i85 && i `isUnder` i76) allPossibleImgs,
            i87 <- filter (\i -> i `rightOf` i86 && i `isUnder` i77) allPossibleImgs,
            i88 <- filter (\i -> i `rightOf` i87 && i `isUnder` i78) allPossibleImgs,
            i89 <- filter (\i -> i `rightOf` i88 && i `isUnder` i79) allPossibleImgs,
            i8a <- filter (\i -> i `rightOf` i89 && i `isUnder` i7a) allPossibleImgs,
            i8b <- filter (\i -> i `rightOf` i8a && i `isUnder` i7b) allPossibleImgs,

            i90 <- filter (                           `isUnder` i80) allPossibleImgs,
            i91 <- filter (\i -> i `rightOf` i90 && i `isUnder` i81) allPossibleImgs,
            i92 <- filter (\i -> i `rightOf` i91 && i `isUnder` i82) allPossibleImgs,
            i93 <- filter (\i -> i `rightOf` i92 && i `isUnder` i83) allPossibleImgs,
            i94 <- filter (\i -> i `rightOf` i93 && i `isUnder` i84) allPossibleImgs,
            i95 <- filter (\i -> i `rightOf` i94 && i `isUnder` i85) allPossibleImgs,
            i96 <- filter (\i -> i `rightOf` i95 && i `isUnder` i86) allPossibleImgs,
            i97 <- filter (\i -> i `rightOf` i96 && i `isUnder` i87) allPossibleImgs,
            i98 <- filter (\i -> i `rightOf` i97 && i `isUnder` i88) allPossibleImgs,
            i99 <- filter (\i -> i `rightOf` i98 && i `isUnder` i89) allPossibleImgs,
            i9a <- filter (\i -> i `rightOf` i99 && i `isUnder` i8a) allPossibleImgs,
            i9b <- filter (\i -> i `rightOf` i9a && i `isUnder` i8b) allPossibleImgs,

            ia0 <- filter (                           `isUnder` i90) allPossibleImgs,
            ia1 <- filter (\i -> i `rightOf` ia0 && i `isUnder` i91) allPossibleImgs,
            ia2 <- filter (\i -> i `rightOf` ia1 && i `isUnder` i92) allPossibleImgs,
            ia3 <- filter (\i -> i `rightOf` ia2 && i `isUnder` i93) allPossibleImgs,
            ia4 <- filter (\i -> i `rightOf` ia3 && i `isUnder` i94) allPossibleImgs,
            ia5 <- filter (\i -> i `rightOf` ia4 && i `isUnder` i95) allPossibleImgs,
            ia6 <- filter (\i -> i `rightOf` ia5 && i `isUnder` i96) allPossibleImgs,
            ia7 <- filter (\i -> i `rightOf` ia6 && i `isUnder` i97) allPossibleImgs,
            ia8 <- filter (\i -> i `rightOf` ia7 && i `isUnder` i98) allPossibleImgs,
            ia9 <- filter (\i -> i `rightOf` ia8 && i `isUnder` i99) allPossibleImgs,
            iaa <- filter (\i -> i `rightOf` ia9 && i `isUnder` i9a) allPossibleImgs,
            iab <- filter (\i -> i `rightOf` iaa && i `isUnder` i9b) allPossibleImgs,

            ib0 <- filter (                           `isUnder` ia0) allPossibleImgs,
            ib1 <- filter (\i -> i `rightOf` ib0 && i `isUnder` ia1) allPossibleImgs,
            ib2 <- filter (\i -> i `rightOf` ib1 && i `isUnder` ia2) allPossibleImgs,
            ib3 <- filter (\i -> i `rightOf` ib2 && i `isUnder` ia3) allPossibleImgs,
            ib4 <- filter (\i -> i `rightOf` ib3 && i `isUnder` ia4) allPossibleImgs,
            ib5 <- filter (\i -> i `rightOf` ib4 && i `isUnder` ia5) allPossibleImgs,
            ib6 <- filter (\i -> i `rightOf` ib5 && i `isUnder` ia6) allPossibleImgs,
            ib7 <- filter (\i -> i `rightOf` ib6 && i `isUnder` ia7) allPossibleImgs,
            ib8 <- filter (\i -> i `rightOf` ib7 && i `isUnder` ia8) allPossibleImgs,
            ib9 <- filter (\i -> i `rightOf` ib8 && i `isUnder` ia9) allPossibleImgs,
            iba <- filter (\i -> i `rightOf` ib9 && i `isUnder` iaa) allPossibleImgs,
            ibb <- filter (\i -> i `rightOf` iba && i `isUnder` iab) allPossibleImgs

            -- FIXME: Not replicating for 12x12, let `rightOf` and `isUnder` try unnecessary combos.
            --(fst i1) /= (fst i0) &&
            --(fst i2) /= (fst i0) && (fst i2) /= (fst i1) &&
            --(fst i3) /= (fst i0) && (fst i3) /= (fst i1) && (fst i3) /= (fst i2) &&
            --(fst i4) /= (fst i0) && (fst i4) /= (fst i1) && (fst i4) /= (fst i2) && (fst i4) /= (fst i3) &&
            --(fst i5) /= (fst i0) && (fst i5) /= (fst i1) && (fst i5) /= (fst i2) && (fst i5) /= (fst i3) && (fst i5) /= (fst i4) &&
            --(fst i6) /= (fst i0) && (fst i6) /= (fst i1) && (fst i6) /= (fst i2) && (fst i6) /= (fst i3) && (fst i6) /= (fst i4) && (fst i6) /= (fst i5) &&
            --(fst i7) /= (fst i0) && (fst i7) /= (fst i1) && (fst i7) /= (fst i2) && (fst i7) /= (fst i3) && (fst i7) /= (fst i4) && (fst i7) /= (fst i5) && (fst i7) /= (fst i6) &&
            --(fst i8) /= (fst i0) && (fst i8) /= (fst i1) && (fst i8) /= (fst i2) && (fst i8) /= (fst i3) && (fst i8) /= (fst i4) && (fst i8) /= (fst i5) && (fst i8) /= (fst i6) && (fst i8) /= (fst i7)
          ]


main :: IO ()
main = interact $ show . solve . map ((\grp -> (head grp, tail grp)) . filter (/= "")) . groupBy (\a b -> "" /= b) . lines

module Main where
import Data.List (sort, sortBy)

--	Your seat wasn't at the very front or back, though; the seats with IDs +1 and -1 from yours will be in your list.
--
--	What is the ID of your seat?
--
--	Here is an example batch file containing four passports:
--
-- Test:
--	% cd ../.. && echo -e 'FBFBBFFRLR\nBFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL' | cabal run 2020_day05-binary-boarding_part2 # (44, 5, 357) (70, 7, 567) (14, 7, 119) (102, 4, 820)

type SeatPos = Int

solve :: String -> SeatPos
solve seat = r * 8 + c
  where (rows, cols) = splitAt 7 seat
        r = fst . foldl binSearch (0, 127) $ rows
        c = fst . foldl binSearch (0,   7) $ cols

binSearch :: (Int, Int) -> Char -> (Int, Int)
binSearch (lb, ub) dir = newBounds
  where width = ub - lb
        half  = width `div` 2
        newBounds
          | 'F' == dir || 'L' == dir = (lb, lb + half)
          | 'B' == dir || 'R' == dir = (lb + half + 1, ub)


-- slow search by diff-ing all the seats w/ the next one and sorting (finds the biggest gap at least):
--	searchSeat :: [SeatPos] -> SeatPos
--	searchSeat ns = succ . (\(_, s1, _) -> s1) . head . sortBy (\(d1, _, _) (d2, _, _) -> compare d2 d1) . map (\(s1, s2) -> (s2 - s1, s1, s2)) . zip ns $ tail ns

-- Instead of diff-ing and sorting, this seems a bit faster (it should short-circuit if nothing else):
searchSeat :: [SeatPos] -> SeatPos
searchSeat (x:y:xs)
  | x + 1 < y = x + 1
  | otherwise = searchSeat (y:xs)
searchSeat _  = 0


main :: IO ()
main = interact $ show . searchSeat . sort . map solve . words

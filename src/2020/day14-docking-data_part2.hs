module Main where
import Data.Bits ((.&.), (.|.), complement)
import Data.Bool (bool)
import Data.Char (digitToInt)
import Data.IntMap.Strict (IntMap, empty, foldr', insert)
import Numeric (readInt)


--	... doesn't modify the values being written at all. Instead, it acts as a memory address decoder. Immediately before a
--	value is written to memory, each bit in the bitmask modifies the corresponding bit of the destination memory address in
--	the following way:
--
--	* If the bitmask bit is 0, the corresponding memory address bit is unchanged.
--	* If the bitmask bit is 1, the corresponding memory address bit is overwritten with 1.
--	* If the bitmask bit is X, the corresponding memory address bit is floating.
--
--	A floating bit is not connected to anything and instead fluctuates unpredictably. In practice, this means the floating
--	bits will take on all possible values, potentially causing many memory addresses to be written all at once!
--
--	For example, consider the following program:
--
--	mask = 000000000000000000000000000000X1001X
--	mem[42] = 100
--	mask = 00000000000000000000000000000000X0XX
--	mem[26] = 1
--	When this program goes to write to memory address 42, it first applies the bitmask:
--
--	address: 000000000000000000000000000000101010  (decimal 42)
--	mask:    000000000000000000000000000000X1001X
--	result:  000000000000000000000000000000X1101X
--
--	After applying the mask, four bits are overwritten, three of which are different, and two of which are floating. Floating
--	bits take on every possible combination of values; with two floating bits, four actual memory addresses are written:
--
--	000000000000000000000000000000011010  (decimal 26)
--	000000000000000000000000000000011011  (decimal 27)
--	000000000000000000000000000000111010  (decimal 58)
--	000000000000000000000000000000111011  (decimal 59)
--
--	Next, the program is about to write to memory address 26 with a different bitmask:
--
--	address: 000000000000000000000000000000011010  (decimal 26)
--	mask:    00000000000000000000000000000000X0XX
--	result:  00000000000000000000000000000001X0XX
--	This results in an address with three floating bits, causing writes to eight memory addresses:
--
--	000000000000000000000000000000010000  (decimal 16)
--	000000000000000000000000000000010001  (decimal 17)
--	000000000000000000000000000000010010  (decimal 18)
--	000000000000000000000000000000010011  (decimal 19)
--	000000000000000000000000000000011000  (decimal 24)
--	000000000000000000000000000000011001  (decimal 25)
--	000000000000000000000000000000011010  (decimal 26)
--	000000000000000000000000000000011011  (decimal 27)
--
--	The entire 36-bit address space still begins initialized to the value 0 at every address, and you still need the sum of
--	all values left in memory at the end of the program. In this example, the sum is 208.
--
--	Execute the initialization program using an emulator for a version 2 decoder chip. What is the sum of all values left in
--	memory after it completes?
--
-- Test:
--	% cd ../.. && echo -e 'mask = 000000000000000000000000000000X1001X\nmem[42] = 100\nmask = 00000000000000000000000000000000X0XX\nmem[26] = 1' | cabal run 2020_day14-docking-data_part2


-- FIXME:
--	Using an immutable assoc list causes a lot of copies and the memory locations run in the 10s of thousands (maybe more).
--	A mutable tree or something should easily run better.  Most of the time is spent (for some reason) in the sum loop.
--	This ran in ~ 18 minutes on a laptop.
-- Wow! From 15 minutes to 5 seconds w/ just IntMap instead of assoc lists. Read ur data structures people : )


genAllMasks :: String -> [String]
genAllMasks str = allMasks
  where allMasks = loop "" str
        loop acc ('0':xs) = loop (acc ++ "0") xs
        loop acc ('1':xs) = loop (acc ++ "1") xs
        loop acc ('X':xs) = loop (acc ++ "X") xs ++ loop (acc ++ "1") xs
        loop acc []       = [acc]


applyMasks :: Int -> [String] -> [Int]
applyMasks n = foldl' applyMask []
  where applyMask :: [Int] -> String -> [Int] -- strictly compute the new addresses to stop thunk buildup (attempt at optimization, maybe not worth it?)
        applyMask acc str = (n .&. andMask .|. orMask):acc
          where andMask = str2bin (bool 1 0 . ('X' ==)) -- "and" won't mask any bit except 'X'-es
                orMask  = str2bin (bool 0 1 . ('1' ==)) -- "or"  won't set  any bit except Ones
                str2bin char2int = fst . head . readInt 2 (`elem` "X01") char2int $ str


execute :: (IntMap Int, [String]) -> [String] -> (IntMap Int, [String])
execute (memMap, masks) ["mask", "=", maskStr] = (memMap, genAllMasks maskStr)

execute (memMap, masks) ['m':'e':'m':'[':locStr, "=", valStr] = (writeMem val $ applyMasks loc masks, masks)
  where [(loc, _)] = reads locStr :: [(Int, String)]
        [(val, _)] = reads valStr :: [(Int, String)]
        writeMem :: Int -> [Int] -> IntMap Int
        writeMem v ls = foldl' (\acc l -> insert l v acc) memMap ls

execute (memMap, masks) cmdList = (insert (-1) (-1) memMap, masks)


solve :: [String] -> Int
solve lines = let memMap = fst . foldl' execute (empty, ["1:* masks here"]) . map words $ lines
              in foldr' (+) 0 memMap


main :: IO ()
main = interact $ show . solve . lines

module Main where

--	Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.
--
--	For example, suppose your expense report contained the following:
--
--	1721
--	979
--	366
--	299
--	675
--	1456
--	In this list, the two entries that sum to 2020 are 1721 and 299. Multiplying them together produces 1721 * 299 = 514579, so the correct answer is 514579.
--
-- Test:
--	% cd ../.. && echo -e '1721\n979\n366\n299\n675\n1456' | cabal run 2020_day01-report-repair


solve :: [Int] -> Int
solve ns = head [x * y | x <- ns, y <- ns, x /= y, 2020 == x + y]


main :: IO ()
main = interact $ show . solve . map read . words

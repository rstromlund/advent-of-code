module Main where
import Data.List (minimumBy)

--	For example, consider just the first seven characters of FBFBBFFRLR:
--
--	Start by considering the whole range, rows 0 through 127.
--		F means to take the lower half, keeping rows 0 through 63.
--		B means to take the upper half, keeping rows 32 through 63.
--		F means to take the lower half, keeping rows 32 through 47.
--		B means to take the upper half, keeping rows 40 through 47.
--		B keeps rows 44 through 47.
--		F keeps rows 44 through 45.
--		The final F keeps the lower of the two, row 44.
--		The last three characters will be either L or R; these specify exactly one of the 8 columns of seats on the plane (numbered 0 through 7). The same process as above proceeds again, this time with only three steps. L means to keep the lower half, while R means to keep the upper half.
--
--	For example, consider just the last 3 characters of FBFBBFFRLR:
--
--	Start by considering the whole range, columns 0 through 7.
--		R means to take the upper half, keeping columns 4 through 7.
--		L means to take the lower half, keeping columns 4 through 5.
--		The final R keeps the upper of the two, column 5.
--	So, decoding FBFBBFFRLR reveals that it is the seat at row 44, column 5.
--
--	Every seat also has a unique seat ID: multiply the row by 8, then add the column. In this example, the seat has ID 44 * 8 + 5 = 357.
--
--	Here are some other boarding passes:
--
--		BFFFBBFRRR: row 70, column 7, seat ID 567.
--		FFFBBBFRRR: row 14, column 7, seat ID 119.
--		BBFFBBFRLL: row 102, column 4, seat ID 820.
--	As a sanity check, look through your list of boarding passes. What is the highest seat ID on a boarding pass?
--
-- Test:
--	% cd ../.. && echo -e 'FBFBBFFRLR\nBFFFBBFRRR\nFFFBBBFRRR\nBBFFBBFRLL' | cabal run 2020_day05-binary-boarding # (44, 5, 357) (70, 7, 567) (14, 7, 119) (102, 4, 820)


type SeatPos = Int


solve :: String -> SeatPos
solve seat = r * 8 + c
  where (rows, cols) = splitAt 7 seat
        r = fst . foldl binSearch (0, 127) $ rows
        c = fst . foldl binSearch (0,   7) $ cols


binSearch :: (Int, Int) -> Char -> (Int, Int)
binSearch (lb, ub) dir = newBounds
  where width = ub - lb
        half  = width `div` 2
        newBounds
          | 'F' == dir || 'L' == dir = (lb, lb + half)
          | 'B' == dir || 'R' == dir = (lb + half + 1, ub)


main :: IO ()
main = interact $ show . minimumBy (flip compare) . map solve . words

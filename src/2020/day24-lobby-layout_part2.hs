{-# LANGUAGE BangPatterns #-}
-- ^^ bang patterns didn't seem to speed up at all.  Remove, we had the space?
module Main where
import Data.Bool (bool)
import Data.IntMap.Strict (IntMap, (!), delete, empty, findWithDefault, foldlWithKey', insert, size, toList)


--	... The tile floor in the lobby is meant to be a living art exhibit. Every day, the tiles are all flipped according to the following rules:
--
--	* Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
--	* Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
--
--	Here, tiles immediately adjacent means the six tiles directly touching the tile in question.
--
--	The rules are applied simultaneously to every tile; put another way, it is first determined which tiles need to be
--	flipped, then they are all flipped at the same time.
--
--	In the above example, the number of black tiles that are facing up after the given number of days has passed is as follows:
--
--	Day 1: 15
--	Day 2: 12
--	Day 3: 25
--	Day 4: 14
--	Day 5: 23
--	Day 6: 28
--	Day 7: 41
--	Day 8: 37
--	Day 9: 49
--	Day 10: 37
--
--	Day 20: 132
--	Day 30: 259
--	Day 40: 406
--	Day 50: 566
--	Day 60: 788
--	Day 70: 1106
--	Day 80: 1373
--	Day 90: 1844
--	Day 100: 2208
--	After executing this process a total of 100 times, there would be 2208 black tiles facing up.
--
--	How many tiles will be black after 100 days?
--
-- Test:
--	% cd ../.. && echo -e 'esenee' | cabal run 2020_day24-lobby-layout_part2
--	% cd ../.. && echo -e 'esew' | cabal run 2020_day24-lobby-layout_part2
--	% cd ../.. && echo -e 'nwwswee' | cabal run 2020_day24-lobby-layout_part2
--	% cd ../.. && echo -e 'esenee\nesew\nnwwswee' | cabal run 2020_day24-lobby-layout_part2
--	% cd ../.. && echo -e 'sesenwnenenewseeswwswswwnenewsewsw\nneeenesenwnwwswnenewnwwsewnenwseswesw\nseswneswswsenwwnwse\nnwnwneseeswswnenewneswwnewseswneseene\nswweswneswnenwsewnwneneseenw\neesenwseswswnenwswnwnwsewwnwsene\nsewnenenenesenwsewnenwwwse\nwenwwweseeeweswwwnwwe\nwsweesenenewnwwnwsenewsenwwsesesenwne\nneeswseenwwswnwswswnw\nnenwswwsewswnenenewsenwsenwnesesenew\nenewnwewneswsewnwswenweswnenwsenwsw\nsweneswneswneneenwnewenewwneswswnese\nswwesenesewenwneswnwwneseswwne\nenesenwswwswneneswsenwnewswseenwsese\nwnwnesenesenenwwnenwsewesewsesesew\nnenewswnwewswnenesenwnesewesw\neneswnwswnwsenenwnwnwwseeswneewsenese\nneswnwewnwnwseenwseesewsenwsweewe\nwseweeenwnesenwwwswnew' | cabal run 2020_day24-lobby-layout_part2


-- FIXME: ran in under a minute, but there must be a better algorithm or data structure to speed this up.


data Tile = Black | White deriving (Eq, Show)


-- Packing both north and east coordinates into an integer (ref. day17-conway-cubes_part2.hs)
shiftAmt  :: Int
shiftAmt  = 0x1000000

nShift = shiftAmt
eShift = 1

coordMidpoint :: Int
coordMidpoint = shiftAmt `div` 2

mkCoords :: (Int, Int) -> Int
mkCoords (n, e) = (n + coordMidpoint) * nShift + (e + coordMidpoint)

getCoords :: Int -> (Int, Int)
getCoords key = (key `div` nShift `mod` shiftAmt - coordMidpoint,
                 key              `mod` shiftAmt - coordMidpoint)


east (n, e) = (n,     e + 2)
west (n, e) = (n,     e - 2)
ne   (n, e) = (n + 1, e + 1)
nw   (n, e) = (n + 1, e - 1)
se   (n, e) = (n - 1, e + 1)
sw   (n, e) = (n - 1, e - 1)

putTile :: (Int, Int) -> Tile -> IntMap Tile -> IntMap Tile
putTile (n, e) Black ts = insert (mkCoords (n, e)) Black ts
putTile (n, e) White ts = delete (mkCoords (n, e)) ts       -- don't insert the default White, keep map sparse

getTile :: (Int, Int) -> IntMap Tile -> Tile
getTile (n, e) = findWithDefault White (mkCoords (n, e))


builldFlr :: [String] -> IntMap Tile
builldFlr = snd . foldl' move ((0, 0), empty)
  where flipTile coords White ts = putTile coords Black ts
        flipTile coords Black ts = putTile coords White ts
        --
        move :: ((Int, Int), IntMap Tile) -> [Char] -> ((Int, Int), IntMap Tile)
        move ((n, e), ts) [] = ((0, 0), flipTile (n, e) (getTile (n, e) ts) ts)
        move ((n, e), ts) ('e':rest)     = move (east (n, e), ts) rest
        move ((n, e), ts) ('w':rest)     = move (west (n, e), ts) rest
        move ((n, e), ts) ('n':'e':rest) = move (ne   (n, e), ts) rest
        move ((n, e), ts) ('n':'w':rest) = move (nw   (n, e), ts) rest
        move ((n, e), ts) ('s':'e':rest) = move (se   (n, e), ts) rest
        move ((n, e), ts) ('s':'w':rest) = move (sw   (n, e), ts) rest


playLife :: Int -> IntMap Tile -> IntMap Tile
playLife 100 ts = ts
playLife !cnt !ts = playLife (cnt + 1) $ foldlWithKey' evalTile empty ts
  where evalTile :: IntMap Tile -> Int -> Tile -> IntMap Tile
        evalTile !acc k v = let coords = getCoords k -- unpack (n, e)
                                acc' =
                                  setTile coords .
                                  setTile (east coords) .
                                  setTile (west coords) .
                                  setTile (ne   coords) .
                                  setTile (nw   coords) .
                                  setTile (se   coords) .
                                  setTile (sw   coords) $ acc
                            in acc'
        --
        setTile  (n, e) acc = setTile' (n, e) (getTile (n, e) ts) acc
        -- * Any white tile with exactly 2 black tiles immediately adjacent to it is flipped to black.
        setTile' (n, e) White acc = let bs = countAdjacent (n, e)
                                    in putTile (n, e) (if 2 == bs then Black else White) acc
        -- * Any black tile with zero or more than 2 black tiles immediately adjacent to it is flipped to white.
        setTile' (n, e) Black acc = let bs = countAdjacent (n, e)
                                    in putTile (n, e) (if 0 == bs || bs > 2 then White else Black) acc
        countAdjacent (n, e) =
          bool 0 1 (Black == getTile (east (n, e)) ts) +
          bool 0 1 (Black == getTile (west (n, e)) ts) +
          bool 0 1 (Black == getTile (ne   (n, e)) ts) +
          bool 0 1 (Black == getTile (nw   (n, e)) ts) +
          bool 0 1 (Black == getTile (se   (n, e)) ts) +
          bool 0 1 (Black == getTile (sw   (n, e)) ts)


solve :: [String] -> Int
solve = size . playLife 0 . builldFlr


main :: IO ()
main = interact $ show . solve . lines

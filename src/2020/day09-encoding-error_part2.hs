module Main where
import Data.List (tails)


--	... Here is a larger example which only considers the previous 5 numbers (and has a preamble of length 5):
--
--	35
--	20
--	15
--	25
--	47
--	40
--	62
--	55
--	65
--	95
--	102
--	117
--	150
--	182
--	127
--	219
--	299
--	277
--	309
--	576
--
--	In this example, after the 5-number preamble, almost every number is the sum of two of the previous 5 numbers; the only
--	number that does not follow this rule is 127.
--
--	The first step of attacking the weakness in the XMAS data is to find the first number in the list (after the preamble)
--	which is not the sum of two of the 25 numbers before it. What is the first number that does not have this property?
---------------------
--
--	The final step in breaking the XMAS encryption relies on the invalid number you just found: you must find a contiguous set of at least two numbers in your list which sum to the invalid number from step 1.
--
--	Again consider the above example:
--
--	35
--	20
--	[15]
--	[25]
--	[47]
--	[40]
--	62
--	55
--	65
--	95
--	102
--	117
--	150
--	182
--	127
--	219
--	299
--	277
--	309
--	576
--
--	In this list, adding up all of the numbers from 15 through 40 produces the invalid
--	number from step 1, 127. (Of course, the contiguous set of numbers in your actual
--	list might be much longer.)
--
--	To find the encryption weakness, add together the smallest and largest number in this contiguous range; in this example,
--	these are 15 and 47, producing 62.
--
--	What is the encryption weakness in your XMAS-encrypted list of numbers?
--
-- Test:
--	% cd ../.. && echo -e '35\n20\n15\n25\n47\n40\n62\n55\n65\n95\n102\n117\n150\n182\n127\n219\n299\n277\n309\n576' | cabal run 2020_day09-encoding-error_part2

preamble = 25


findInvalid :: [Int] -> [Int] -> Int
findInvalid _ []  = -1
findInvalid hs ts = if valid chkNbr then findInvalid (tail hs ++ [head ts]) (tail ts) else chkNbr
  where chkNbr  = head ts
        valid n = n `elem` sums
        sums    = [x + y | x <- hs, y <- hs, x /= y] -- a combine like in day01 would probably perform better.


-- find the invalid number (as in part 1), search by generating all 'tails' of the number set (probably really inefficient),
--    adding up the first numbers until the invalid # is reached or surpassed, filter the answers find the min and max and add.
solve :: [Int] -> Int
solve ns = minimum ansNs + maximum ansNs
  where (hs, ts)   = splitAt preamble ns
        invld      = findInvalid hs ts
        allAns     = map (foldl searchSums (0, [])) $ tails ns -- search all possible tails
        searchSums (acc, ns) n = if acc < invld then (n + acc, n:ns) else (acc, ns) -- once we reach, or pass, 'invld' stop accumulating
        (_, ansNs) = head . filter (\(tryAns, ns) -> invld == tryAns && (not . null . tail $ ns)) $ allAns -- find the answer matching 'invld', return the combination nbrs


main :: IO ()
main = interact $ show . solve . map read . words

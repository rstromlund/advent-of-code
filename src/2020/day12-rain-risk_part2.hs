module Main where


--	... Almost all of the actions indicate how to move a waypoint which is relative to the ship's position:
--
--	* Action N means to move the waypoint north by the given value.
--	* Action S means to move the waypoint south by the given value.
--	* Action E means to move the waypoint east by the given value.
--	* Action W means to move the waypoint west by the given value.
--	* Action L means to rotate the waypoint around the ship left (counter-clockwise) the given number of degrees.
--	* Action R means to rotate the waypoint around the ship right (clockwise) the given number of degrees.
--	* Action F means to move forward to the waypoint a number of times equal to the given value.
--
--	The waypoint starts 10 units east and 1 unit north relative to the ship. The waypoint is relative to the ship; that is, if
--	the ship moves, the waypoint moves with it.
--
--	For example, using the same instructions as above:
--
--	* F10 moves the ship to the waypoint 10 times (a total of 100 units east and 10 units north), leaving the ship at east
--	  100, north 10. The waypoint stays 10 units east and 1 unit north of the ship.
--	* N3 moves the waypoint 3 units north to 10 units east and 4 units north of the ship. The ship remains at east 100, north 10.
--	* F7 moves the ship to the waypoint 7 times (a total of 70 units east and 28 units north), leaving the ship at east 170,
--	  north 38. The waypoint stays 10 units east and 4 units north of the ship.
--	* R90 rotates the waypoint around the ship clockwise 90 degrees, moving it to 4 units east and 10 units south of the
--	  ship. The ship remains at east 170, north 38.
--	* F11 moves the ship to the waypoint 11 times (a total of 44 units east and 110 units south), leaving the ship at east
--	  214, south 72. The waypoint stays 4 units east and 10 units south of the ship.
--
--	After these operations, the ship's Manhattan distance from its starting position is 214 + 72 = 286.
--
--	Figure out where the navigation instructions actually lead. What is the Manhattan distance between that location and the
--	ship's starting position?
--
-- Test:
--	% cd ../.. && echo -e 'F10\nN3\nF7\nR90\nF11' | cabal run 2020_day12-rain-risk_part2


solve :: [String] -> Int
solve = (\((ns, ew), _) -> abs ns + abs ew) . foldl' moveShip ((0, 0), (1, 10))
  where intVal str = read str :: Int
        --moveShip (shipPos@(ns,ew), wayPoint@(ns,ew)) cmd
        moveShip ((ns, ew), wp@(nsWP, ewWP)) ('F':val) = ((ns + nsWP * intVal val, ew + ewWP * intVal val), wp)
        moveShip ((ns, ew), (nsWP, ewWP)) ('W':val)    = ((ns, ew), (nsWP, ewWP - intVal val))
        moveShip ((ns, ew), (nsWP, ewWP)) ('S':val)    = ((ns, ew), (nsWP - intVal val, ewWP))
        moveShip ((ns, ew), (nsWP, ewWP)) ('N':val)    = ((ns, ew), (nsWP + intVal val, ewWP))
        moveShip ((ns, ew), (nsWP, ewWP)) ('E':val)    = ((ns, ew), (nsWP, ewWP + intVal val))
        moveShip ((ns, ew), (nsWP, ewWP)) "R90"        = ((ns, ew), (-ewWP, nsWP)) -- why figure out all the combos? Make all turns into Right90-es
        moveShip acc                      "R180"       = moveShip (moveShip acc "R90")  "R90"
        moveShip acc                      "R270"       = moveShip (moveShip acc "R180") "R90"
        moveShip acc                      "L90"        = moveShip acc "R270"
        moveShip acc                      "L180"       = moveShip acc "R180"
        moveShip acc                      "L270"       = moveShip acc "R90"


main :: IO ()
main = interact $ show . solve . words

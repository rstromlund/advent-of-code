module Main where
import Data.Bool (bool)
import Data.List (groupBy, intersect, transpose, isPrefixOf, (\\))
import Data.List.Split (splitOn)


--	... Now that you've identified which tickets contain invalid values, *discard those tickets entirely*. Use the remaining
--	valid tickets to determine which field is which.
--
--	Using the valid ranges for each field, determine what order the fields appear on the tickets. The order is consistent
--	between all tickets: if seat is the third field, it is the third field on every ticket, including your ticket.
--
--	For example, suppose you have the following notes:
--
--	class: 0-1 or 4-19
--	row: 0-5 or 8-19
--	seat: 0-13 or 16-19
--
--	your ticket:
--	11,12,13
--
--	nearby tickets:
--	3,9,18
--	15,1,5
--	5,14,9
--
--	Based on the nearby tickets in the above example, the first position must be row, the second position must be class, and
--	the third position must be seat; you can conclude that in your ticket, class is 12, row is 11, and seat is 13.
--
--	Once you work out which field is which, look for the six fields on your ticket that start with the word departure. What do
--	you get if you multiply those six values together?
--
-- Test:
--	% cd ../.. && echo -e 'departureclass: 0-1 or 4-19\nrow: 0-5 or 8-19\ndepartureseat: 0-13 or 16-19\n\nyour ticket:\n11,12,13\n\nnearby tickets:\n20,9,18\n3,9,20\n3,9,18\n15,1,5\n5,14,9' | cabal run 2020_day16-ticket-translation_part2
-- Go:
--	% cd ../.. && time cabal run 2020_day16-ticket-translation_part2 < src/2020/day16-ticket-translation.input.txt


type Name = String
type Boundary = (Int, Int)
type Rule = (Name, [Boundary])
type Ticket = [Int]
type TicketOwner = (Name, [Ticket])


-- e.g. "class: 1-3 or 5-7"
-- output: ("class", [(1,3), (5,7)])
parseRule :: String -> Rule
parseRule str =  (lbl, map parseBounds bndStr)
  where (lbl:bndsStr:_) = splitOn ":" str
        bndStr = filter (/= "or") . words $ bndsStr
        parseBounds :: String -> Boundary
        parseBounds = (\[a,b] -> (a,b)) . map read . splitOn "-"


-- e.g. ["your ticket:","7,1,14"] or ["nearby tickets:","7,3,47","40,4,50",...]
-- output ("your ticket:", [[7,1,14]]) or ("nearby tickets:", [[7,3,47],[40,4,50],...])
parseTicket :: [String] -> TicketOwner
parseTicket (nm:tkts) = (nm, map (map read . splitOn ",") tkts)


--output: validate each ticket# against all rule boundaries, return passing ruleNames per tkt#.
--[[["row","seat"],["class","row","seat"],["class","row","seat"]],
--[["class","row"],["class","row","seat"],["class","row","seat"]],
--[["class","row","seat"],["class","row"],["class","row","seat"]]]

validateTkts :: [Rule] -> TicketOwner -> [[[Name]]]
validateTkts rules tkts = map ckTkt $ snd tkts
  where ckTkt :: Ticket -> [[Name]] -- returns list of ruleNames per slot that pass bounds checks
        ckTkt = map ckTktNbr
        --
        ckTktNbr :: Int -> [Name] -- returns list of ruleNames this number passes bounds checks (remove the "" caused by non-passing rules)
        ckTktNbr n = filter (not . null) . map (ckRule n) $ rules
        --
        ckRule :: Int -> Rule -> Name -- returns ruleName if bound check passes, empty string otherwise
        ckRule n rule = bool "" (fst rule) $ any (\(lo, hi) -> n >= lo && n <= hi) (snd rule)


-- e.g. tkts
--[[["row","seat"],["class","row","seat"],["class","row","seat"]],
--[["class","row"],["class","row","seat"],["class","row","seat"]],
--[["class","row","seat"],["class","row"],["class","row","seat"]]]
--after intersect: intersect each column, then reduce those for the output.
--  [["row"],["class","row"],["class","row","seat"]]
-- output:
--  [["row"],["class"],["seat"]]

findSlotNames :: [[[Name]]] -> [[Name]]
findSlotNames tkts = until
                       (all (\slot -> 1 == length slot)) -- when all slots have only 1 possibility, then we're done
                       (\ps -> map (trimSlots ps) ps)    -- else eliminate possibilities by removing known answers
                       possibleNames
  where possibleNames :: [[Name]]
        possibleNames = map (foldl1 intersect) . transpose $ tkts -- intersect each "column" (or "slot") to get a list possible rule names.
        --
        trimSlots :: [[Name]] -> [Name] -> [Name]
        trimSlots ps slot
          | 1 == length slot = slot -- if a slot has 1 possibility, then it is the answer.
          | otherwise        = foldl' reduce slot ps -- else try and eliminate possibilies by all the other known answers.
        --
        reduce :: [Name] -> [Name] -> [Name]
        reduce slot candidate
          | 1 /= length candidate = slot              -- multiple possible answers in the "candidate"?  We can't use it.
          | otherwise             = slot \\ candidate -- the "candidate" only has 1 possible answer, assuming it is correct and remove from our slot possibilities.


solve :: [[String]] -> Int
solve [rulesIn, yourTicketIn, nearbyTicketsIn] = answer
  where -- parsers
        rules :: [Rule]
        rules = map parseRule rulesIn
        yourTicket :: TicketOwner
        yourTicket = parseTicket yourTicketIn
        nearbyTickets :: TicketOwner
        nearbyTickets = parseTicket nearbyTicketsIn
        -- transformers/reducers
        dropInavlids :: [[[Name]]] -> [[[Name]]]
        dropInavlids = filter (not . any null) -- every slot needs at least 1 possilbe answer, if not then it is invalid and removed.
        slotNames :: [Name]
        slotNames = concat . findSlotNames . dropInavlids . validateTkts rules $ nearbyTickets -- reduce all tickets down to just 1 ruleName per slot
        myTicketSolved :: [(Name, Int)]
        myTicketSolved = zip slotNames (head . snd $ yourTicket) -- combine the correct ruleName with my ticket #s
        answer :: Int
        answer = product . map snd . filter (\(nm, _) -> "departure" `isPrefixOf` nm) $ myTicketSolved -- filter to "departure" slots only and multiply #s


main :: IO ()
main = interact $ show . solve . map (filter (not . null)) . groupBy (\a b -> not . null $ b) . lines

{-# LANGUAGE LambdaCase #-}
module Main where
import Data.Bool (bool)
import Data.List (groupBy)
import Data.List.Split (splitOn)


--	... completely replace rules 8: 42 and 11: 42 31 with the following:
--
--	8: 42 | 42 8
--	11: 42 31 | 42 11 31
--
--	This small change has a big impact: now, the rules do contain loops, and the list of messages they could hypothetically
--	match is infinite. You'll need to determine how these changes affect which messages are valid.
--
--	Fortunately, many of the rules are unaffected by this change; it might help to start by looking at which rules always
--	match the same set of values and how those rules (especially rules 42 and 31) are used by the new versions of rules 8 and
--	11.
--
--	(Remember, you only need to handle the rules you have; building a solution that could handle any hypothetical combination
--	of rules would be significantly more difficult.)
--
--	For example:
--
--	0: 8 11
--	1: "a"
--	2: 1 24 | 14 4
--	3: 5 14 | 16 1
--	4: 1 1
--	5: 1 14 | 15 1
--	6: 14 14 | 1 14
--	7: 14 5 | 1 21
--	8: 42			{ 8: 42 | 42 8 }
--	9: 14 27 | 1 26
--	10: 23 14 | 28 1
--	11: 42 31		{ 11: 42 31 | 42 11 31 }
--	12: 24 14 | 19 1
--	13: 14 3 | 1 12
--	14: "b"
--	15: 1 | 14
--	16: 15 1 | 14 14
--	17: 14 2 | 1 7
--	18: 15 15
--	19: 14 1 | 14 14
--	20: 14 14 | 1 15
--	21: 14 1 | 1 14
--	22: 14 14
--	23: 25 1 | 22 14
--	24: 14 1
--	25: 1 1 | 1 14
--	26: 14 22 | 1 20
--	27: 1 6 | 14 18
--	28: 16 1
--	31: 14 17 | 1 13
--	42: 9 14 | 10 1
--
--	abbbbbabbbaaaababbaabbbbabababbbabbbbbbabaaaa
--	bbabbbbaabaabba
--	babbbbaabbbbbabbbbbbaabaaabaaa
--	aaabbbbbbaaaabaababaabababbabaaabbababababaaa
--	bbbbbbbaaaabbbbaaabbabaaa
--	bbbababbbbaaaaaaaabbababaaababaabab
--	ababaaaaaabaaab
--	ababaaaaabbbaba
--	baabbaaaabbaaaababbaababb
--	abbbbabbbbaaaababbbbbbaaaababb
--	aaaaabbaabaaaaababaa
--	aaaabbaaaabbaaa
--	aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
--	babaaabbbaaabaababbaabababaaab
--	aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
--
--	Without updating rules 8 and 11, these rules only match three messages: bbabbbbaabaabba, ababaaaaaabaaab, and ababaaaaabbbaba.
--
--	However, after updating rules 8 and 11, a total of 12 messages match:
--
--	* bbabbbbaabaabba
--	* babbbbaabbbbbabbbbbbaabaaabaaa
--	* aaabbbbbbaaaabaababaabababbabaaabbababababaaa
--	* bbbbbbbaaaabbbbaaabbabaaa
--	* bbbababbbbaaaaaaaabbababaaababaabab
--	* ababaaaaaabaaab
--	* ababaaaaabbbaba
--	* baabbaaaabbaaaababbaababb
--	* abbbbabbbbaaaababbbbbbaaaababb
--	* aaaaabbaabaaaaababaa
--	* aaaabbaabbaaaaaaabbbabbbaaabbaabaaa
--	* aabbbbbaabbbaaaaaabbbbbababaaaaabbaaabba
--
--	After updating rules 8 and 11, how many messages completely match rule 0?
--
-- Test:
--	% cd ../.. && cat src/2020/day19-monster-messages.test.txt | cabal run 2020_day19-monster-messages_part2 # b/f 3 matches: 2, 7, 8
--	% cd ../.. && (echo -e '8: 42 | 42 8\n11: 42 31 | 42 11 31' && grep -v -e '^8:' -e '^11:' < src/2020/day19-monster-messages.test.txt;) | cabal run 2020_day19-monster-messages_part2 # a/f 12 matches: 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 15
-- Go:
--	% cd ../.. && (echo -e '8: 42 | 42 8\n11: 42 31 | 42 11 31' && grep -v -e '^8:' -e '^11:' < src/2020/day19-monster-messages.input.txt;) | cabal run 2020_day19-monster-messages_part2


-- FIXME: I know parsers and monads would simplfy this a lot, I guess this is a "bare metal" solution.  I need to learn those coding skills and fix this later.

data Rule = RuleCombinator [[Int]] | RuleCharMatch Char deriving Show


parseRule :: String -> (Int, Rule)
parseRule str = let [nbrStr, defStr] = splitOn ": " str
                    nbr = read nbrStr
                in parseDef nbr defStr
  where parseDef n ('"':str) = (n, RuleCharMatch $ head str)
        parseDef n str       = (n, RuleCombinator . map (map read . words) . splitOn "|" $ str)


parseLines :: [(Int, Rule)] -> [[String]] -> ([(Int, Rule)], [String])
parseLines rs ["":ls]        = (rs, ls)
parseLines rs ([ruleStr]:ls) = parseLines (parseRule ruleStr:rs) ls


validate :: [(Int, Rule)] -> String -> Bool
validate rules msg = Just "" `elem` runRule [Just msg] (lookup 0 rules)
  where runRule :: [Maybe String] -> Maybe Rule -> [Maybe String]
        runRule [] _    = [Nothing]
        runRule msgs (Just (RuleCombinator combos))
                        = concatMap (validateRuleNbrs msgs) combos
        runRule msgs (Just (RuleCharMatch ch))
                        = map (\case
                                  Nothing  -> Nothing
                                  Just ""  -> Nothing
                                  Just str -> bool Nothing (Just (tail str)) $ ch == head str
                              ) msgs
        --
        validateRuleNbrs :: [Maybe String] -> [Int] -> [Maybe String]
        validateRuleNbrs msgs nbrs = foldl' validateRuleNbr msgs nbrs
        --
        validateRuleNbr :: [Maybe String] -> Int -> [Maybe String]
        validateRuleNbr msgs nbr  = filter (/= Nothing) . runRule msgs $ lookup nbr rules


solve :: [[String]] -> Int
solve ls = length . filter id . map (validate rules) $ msgs
  where (rules, msgs) = parseLines [] ls


main :: IO ()
main = interact $ show . solve . groupBy (\a _ -> "" == a) . lines

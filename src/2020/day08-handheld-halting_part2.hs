module Main where
import Data.List.Split (chunksOf)


--	... If you change the first instruction from nop +0 to jmp +0, it would create a single-instruction infinite loop, never
--	leaving that instruction. If you change almost any of the jmp instructions, the program will still eventually find another
--	jmp instruction and loop forever.
--
--	However, if you change the second-to-last instruction (from jmp -4 to nop -4), the program terminates! The instructions
--	are visited in this order:
--
--	nop +0  | 1
--	acc +1  | 2
--	jmp +4  | 3
--	acc +3  |
--	jmp -3  |
--	acc -99 |
--	acc +1  | 4
--	nop -4  | 5
--	acc +6  | 6
--
--	After the last instruction (acc +6), the program terminates by attempting to run the instruction below the last
--	instruction in the file. With this change, after the program terminates, the accumulator contains the value 8 (acc +1, acc
--	+1, acc +6).
--
--	Fix the program so that it terminates normally by changing exactly one jmp (to nop) or nop (to jmp). What is the value of
--	the accumulator after the program terminates?
--
-- Test:
--	% cd ../.. && echo -e 'nop +0\nacc +1\njmp +4\nacc +3\njmp -3\nacc -99\nacc +1\njmp -4\nacc +6' | cabal run 2020_day08-handheld-halting_part2


-- Return Nothing when an infinite loop is detected, Just Acc when it terminates
solve :: [(String, Int)] -> Maybe Int
solve pgm = step 0 [] 0
  where steps = length pgm
        step ip vistdIps acc
          | ip `elem` vistdIps = Nothing
          | ip >= steps        = Just acc
          | otherwise          = exec (pgm!!ip) ip vistdIps acc
        exec ("nop",_)  ip vistdIps acc = step (succ ip) (ip:vistdIps) acc
        exec ("acc",op) ip vistdIps acc = step (succ ip) (ip:vistdIps) (acc + op)
        exec ("jmp",op) ip vistdIps acc = step (ip + op) (ip:vistdIps) acc


-- Morph every "nop" to "jmp" and every "jmp" to "nop"; exhaustively search for a halting solution
makeMods :: [(String, Int)] -> [[(String, Int)]]
makeMods pgm = foldr morph [pgm] [0..pred . length $ pgm]
  where morph cnt acc = morph' (pgm!!cnt) cnt acc
        morph' ("nop",op) cnt acc = replace cnt ("jmp",op) pgm:acc
        morph' ("jmp",op) cnt acc = replace cnt ("nop",op) pgm:acc
        morph' _ _ acc            = acc
        replace pos newVal list   = let (hs, ts) = splitAt pos list in hs ++ newVal : tail ts


main :: IO ()
main = interact $ show . head . filter (/= Nothing) . map solve . makeMods . map (\(inst:op:_) -> (inst, opInt op)) . chunksOf 2 . words
  where opInt ('+':op) = read op :: Int
        opInt op       = read op :: Int

module Main where


--	... The homework (your puzzle input) consists of a series of expressions that consist of addition (+), multiplication (*), and parentheses ((...)). Just like normal math, parentheses indicate that the expression inside must be evaluated before it can be used by the surrounding expression. Addition still finds the sum of the numbers on both sides of the operator, and multiplication still finds the product.
--
--	However, the rules of operator precedence have changed. Rather than evaluating multiplication before addition, the operators have the same precedence, and are evaluated left-to-right regardless of the order in which they appear.
--
--	For example, the steps to evaluate the expression 1 + 2 * 3 + 4 * 5 + 6 are as follows:
--
--	1 + 2 * 3 + 4 * 5 + 6
--	  3   * 3 + 4 * 5 + 6
--	      9   + 4 * 5 + 6
--	         13   * 5 + 6
--	             65   + 6
--	                 71
--	Parentheses can override this order; for example, here is what happens if parentheses are added to form 1 + (2 * 3) + (4 * (5 + 6)):
--
--	1 + (2 * 3) + (4 * (5 + 6))
--	1 +    6    + (4 * (5 + 6))
--	     7      + (4 * (5 + 6))
--	     7      + (4 *   11   )
--	     7      +     44
--	            51
--	Here are a few more examples:
--
--	2 * 3 + (4 * 5) becomes 26.
--	5 + (8 * 3 + 9 + 3 * 4 * 3) becomes 437.
--	5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4)) becomes 12240.
--	((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2 becomes 13632.
--
--	Before you can help with the homework, you need to understand it yourself. Evaluate the expression on each line of the
--	homework; what is the sum of the resulting values?
--
-- Test:
--	% echo '1 + 2 * 3 + 4 * 5 + 6' | ./day18-operation-order.hs # 71
--	% echo '1 + (2 * 3) + (4 * (5 + 6))' | ./day18-operation-order.hs # 51
--	% echo '2 * 3 + (4 * 5)' | ./day18-operation-order.hs # 26
--	% echo '5 + (8 * 3 + 9 + 3 * 4 * 3)' | ./day18-operation-order.hs # 437
--	% echo '5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))' | ./day18-operation-order.hs # 12240
--	% echo '((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2' | ./day18-operation-order.hs # 13632


-- FIXME: parse and eval could be 1 pass instead of the intermediate representation.  Could use Text.Parser/Parsec too.

data Expression = Operation Char | Operand Int deriving Show


parse :: String -> [Expression]
parse l = fst . parseExps $ ([], breakExpr l)
  where breakExpr = break (`elem` "+-*/() ")
        -- Terminating case first
        parseExps :: ([Expression], (String, String)) -> ([Expression], (String, String))
        parseExps (exps, ("", ""))         = (reverse exps, ("", ""))
        -- Ignore spaces
        parseExps (exps, ("", ' ':es))     = parseExps (exps, breakExpr es)
        -- Operations
        parseExps (exps, ("", op:es)) | '+' == op || '-' == op || '*' == op || '/' == op || '(' == op || ')' == op
                                           = parseExps (Operation op:exps, breakExpr es)
        -- Operands, must be a number
        parseExps (exps, (nbr, es))        = parseExps (Operand (read nbr):exps, breakExpr es)


eval :: [Expression] -> Int
eval es = (\(_, [Operand i], []) -> i) . evalExps $ (es, [], [])
  where evalExps :: ([Expression], [Expression], [Expression]) -> ([Expression], [Expression], [Expression])
        -- First evaluate binary operations when at least 2 operands are present
        evalExps (exps, (Operand v2):(Operand v1):operands, (Operation '+'):operations) = evalExps (exps, Operand (v1 + v2):operands, operations)
        evalExps (exps, (Operand v2):(Operand v1):operands, (Operation '-'):operations) = evalExps (exps, Operand (v1 - v2):operands, operations)
        evalExps (exps, (Operand v2):(Operand v1):operands, (Operation '*'):operations) = evalExps (exps, Operand (v1 * v2):operands, operations)
        evalExps (exps, (Operand v2):(Operand v1):operands, (Operation '/'):operations) = evalExps (exps, Operand (v1 `div` v2):operands, operations)
        --
        -- Push operands on the stack, handle parenthesis operations first (recursively), any remaning operations get pushed on the stack
        evalExps (o@(Operand _):exps, operands, operations)   = evalExps (exps, o:operands, operations)
        evalExps (Operation '(':exps, operands, operations)   = let (exps', operands', operations') = evalExps (exps, [], [])
                                                                in evalExps (exps', operands' ++ operands, operations' ++ operations)
        evalExps (Operation ')':exps, operands, operations)   = (exps, operands, operations)
        evalExps (o@(Operation _):exps, operands, operations) = evalExps (exps, operands, o:operations)
        evalExps done@([], [_], [])                           = done


solve :: String -> Int
solve = eval . parse


main :: IO ()
main = interact $ show . sum . map solve . lines

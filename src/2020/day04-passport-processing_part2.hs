module Main where
import Data.List (groupBy, sort)
import Data.Char (isHexDigit, isDigit)

--	byr (Birth Year) - four digits; at least 1920 and at most 2002.
--	iyr (Issue Year) - four digits; at least 2010 and at most 2020.
--	eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
--	hgt (Height) - a number followed by either cm or in:
--		If cm, the number must be at least 150 and at most 193.
--		If in, the number must be at least 59 and at most 76.
--	hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
--	ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
--	pid (Passport ID) - a nine-digit number, including leading zeroes.
--	cid (Country ID) - ignored, missing or not.
--
-- Test:
--	% echo 'iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719' | ./day04-passport-processing_part2.hs
--	% ./day04-passport-processing_part2.hs < day04-passport-processing_part2.input.test

solve :: [String] -> Bool
solve = (["byr", "ecl", "eyr", "hcl", "hgt", "iyr", "pid"] ==) . sort . filter (/= "cid") . map (validate . splitAt 3)
  where test k b        = if b then k else "invlaid-" ++ k
        inRng mn mx val = let v = read val in v >= mn && v <= mx
        validate (k@"byr", ':':v)          = test k $ inRng 1920 2002 v
        validate (k@"iyr", ':':v)          = test k $ inRng 2010 2020 v
        validate (k@"eyr", ':':v)          = test k $ inRng 2020 2030 v
        validate (k@"hgt", ':':h:t:o:"cm") = test k $ inRng  150  193 [h, t, o]
        validate (k@"hgt", ':':t:o:"in")   = test k $ inRng   59   76 [t, o]
        validate (k@"hcl", ':':'#':hcl)    = test k $ 6 == length hcl && all isHexDigit hcl
        validate (k@"ecl", ":amb")         = k
        validate (k@"ecl", ":blu")         = k
        validate (k@"ecl", ":brn")         = k
        validate (k@"ecl", ":gry")         = k
        validate (k@"ecl", ":grn")         = k
        validate (k@"ecl", ":hzl")         = k
        validate (k@"ecl", ":oth")         = k
        validate (k@"pid", (':':pid))      = test k $ 9 == length pid && all isDigit pid
        validate (k@"cid", _)              = k
        validate (k, v)                    = "invalid-" ++ k ++ ":" ++ v

main :: IO ()
main = interact $ show . length . filter id . map (solve . words . unwords) . groupBy (\_ b -> "" /= b) . lines

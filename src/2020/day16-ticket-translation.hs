module Main where
import Data.List (groupBy)
import Data.List.Split (splitOn)


--	... You collect the rules for ticket fields, the numbers on your ticket, and the numbers on other nearby tickets for the
--	same train service (via the airport security cameras) together into a single document you can reference (your puzzle
--	input).
--
--	The rules for ticket fields specify a list of fields that exist somewhere on the ticket and the valid ranges of values for
--	each field. For example, a rule like class: 1-3 or 5-7 means that one of the fields in every ticket is named class and can
--	be any value in the ranges 1-3 or 5-7 (inclusive, such that 3 and 5 are both valid in this field, but 4 is not).
--
--	Each ticket is represented by a single line of comma-separated values. The values are the numbers on the ticket in the
--	order they appear; every ticket has the same format. For example, consider this ticket:
--
--	.--------------------------------------------------------.
--	| ????: 101    ?????: 102   ??????????: 103     ???: 104 |
--	|                                                        |
--	| ??: 301  ??: 302             ???????: 303      ??????? |
--	| ??: 401  ??: 402           ???? ????: 403    ????????? |
--	'--------------------------------------------------------'
--
--	Here, ? represents text in a language you don't understand. This ticket might be represented as
--	101,102,103,104,301,302,303,401,402,403; of course, the actual train tickets you're looking at are much more
--	complicated. In any case, you've extracted just the numbers in such a way that the first number is always the same
--	specific field, the second number is always a different specific field, and so on - you just don't know what each position
--	actually means!
--
--	Start by determining which tickets are completely invalid; these are tickets that contain values which aren't valid for
--	any field. Ignore your ticket for now.
--
--	For example, suppose you have the following notes:
--
--	class: 1-3 or 5-7
--	row: 6-11 or 33-44
--	seat: 13-40 or 45-50
--
--	your ticket:
--	7,1,14
--
--	nearby tickets:
--	7,3,47
--	40,4,50 <--  4 is invalid
--	55,2,20 <-- 55 is invalid
--	38,6,12 <-- 12 is invalid
--
--	It doesn't matter which position corresponds to which field; you can identify invalid nearby tickets by considering only
--	whether tickets contain values that are not valid for any field. In this example, the values on the first nearby ticket
--	are all valid for at least one field. This is not true of the other three nearby tickets: the values 4, 55, and 12 are are
--	not valid for any field. Adding together all of the invalid values produces your ticket scanning error rate: 4 + 55 + 12 =
--	71.
--
--	Consider the validity of the nearby tickets you scanned. What is your ticket scanning error rate?
--
-- Test:
--	% cd ../.. && time cabal run 2020_day16-ticket-translation < src/2020/day16-ticket-translation.test.txt
-- Go:
--	% cd ../.. && time cabal run 2020_day16-ticket-translation < src/2020/day16-ticket-translation.test.txt


type Name = String
type Boundary = (Int, Int)
type Rule = (Name, [Boundary])
type Ticket = [Int]
type TicketOwner = (Name, [Ticket])


-- e.g. "class: 1-3 or 5-7"
parseRule :: String -> Rule
parseRule str = (lbl, map parseBounds bndStr)
  where (lbl:bndsStr:_) = splitOn ":" str
        bndStr = filter (/= "or") . words $ bndsStr
        parseBounds :: String -> (Int, Int)
        parseBounds = (\[a,b] -> (a,b)) . map read . splitOn "-"


-- e.g. ["your ticket:","7,1,14"] or ["nearby tickets:","7,3,47","40,4,50",...]
parseTicket :: [String] -> TicketOwner
parseTicket (nm:tkts) = (nm, map (map read . splitOn ",") tkts)


validateTkts :: [Rule] -> TicketOwner -> [[Int]]
validateTkts rules = map ckTkt . snd
  where ckTkt :: [Int] -> [Int] -- returns list of numbers that do NOT pass any rule
        ckTkt = filter (not . ckTktNbr)
        --
        ckTktNbr :: Int -> Bool -- returns True if number passes any rule
        ckTktNbr n = any (ckRule n) rules
        --
        ckRule :: Int -> Rule -> Bool -- returns True if number passes any bounds check
        ckRule n = any (\(lo, hi) -> n >= lo && n <= hi) . snd


solve :: [[String]] -> Int
solve [rulesIn, yourTicketIn, nearbyTicketsIn] = sum . concat . validateTkts rules $ nearbyTickets
  where rules = map parseRule rulesIn
        yourTicket = parseTicket yourTicketIn
        nearbyTickets = parseTicket nearbyTicketsIn


main :: IO ()
main = interact $ show . solve . map (filter (not . null)) . groupBy (\a b -> not . null $ b) . lines

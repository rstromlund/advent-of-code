module Main where
import Data.Char (isAlpha)
import Data.List (groupBy, intersect)

--	As you finish the last group's customs declaration, you notice that you misread one word in the instructions:
--
--	You don't need to identify the questions to which anyone answered "yes"; you need to identify the questions to which
--	everyone answered "yes"!
--
--	For each of the people in their group, you write down the questions for which they answer "yes", one per line. For example:
--
-- Test:
--	% cd ../.. && echo -e 'abc\n\na\nb\nc\n\nab\nac\n\na\na\na\na\n\nb' | cabal run 2020_day06-custom-customs_part2 # [3,0,1,1,1] = 6


solve :: [String] -> Int
solve = length . foldl1 intersect . filter (not . null)

main :: IO ()
main = interact $ show . sum . map solve . groupBy (\_ b -> "" /= b) . lines

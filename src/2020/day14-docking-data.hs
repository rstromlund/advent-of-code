module Main where
import Debug.Trace (trace)
import Data.Bits ((.&.), (.|.))
import Data.Bool (bool)
import Numeric (readInt)


--	... The initialization program (your puzzle input) can either update the bitmask or write a value to memory. Values and
--	memory addresses are both 36-bit unsigned integers. For example, ignoring bitmasks for a moment, a line like mem[8] = 11
--	would write the value 11 to memory address 8.
--
--	The bitmask is always given as a string of 36 bits, written with the most significant bit (representing 2^35) on the left
--	and the least significant bit (2^0, that is, the 1s bit) on the right. The current bitmask is applied to values
--	immediately before they are written to memory: a 0 or 1 overwrites the corresponding bit in the value, while an X leaves
--	the bit in the value unchanged.
--
--	For example, consider the following program:
--
--	mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
--	mem[8] = 11
--	mem[7] = 101
--	mem[8] = 0
--
--	This program starts by specifying a bitmask (mask = ....). The mask it specifies will overwrite two bits in every written
--	value: the 2s bit is overwritten with 0, and the 64s bit is overwritten with 1.
--
--	The program then attempts to write the value 11 to memory address 8. By expanding everything out to individual bits, the
--	mask is applied as follows:
--
--	value:  000000000000000000000000000000001011  (decimal 11)
--	mask:   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
--	result: 000000000000000000000000000001001001  (decimal 73)
--
--	So, because of the mask, the value 73 is written to memory address 8 instead. Then, the program tries to write 101 to address 7:
--
--	value:  000000000000000000000000000001100101  (decimal 101)
--	mask:   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
--	result: 000000000000000000000000000001100101  (decimal 101)
--
--	This time, the mask has no effect, as the bits it overwrote were already the values the mask tried to set. Finally, the
--	program tries to write 0 to address 8:
--
--	value:  000000000000000000000000000000000000  (decimal 0)
--	mask:   XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
--	result: 000000000000000000000000000001000000  (decimal 64)
--
--	64 is written to address 8 instead, overwriting the value that was there previously.
--
--	To initialize your ferry's docking program, you need the sum of all values left in memory after the initialization program
--	completes. (The entire 36-bit address space begins initialized to the value 0 at every address.) In the above example,
--	only two values in memory are not zero - 101 (at address 7) and 64 (at address 8) - producing a sum of 165.
--
--	Execute the initialization program. What is the sum of all values left in memory after it completes? (Do not truncate the sum to 36 bits.)
--
-- Test:
--	% cd ../.. && echo -e 'mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X\nmem[8] = 11\nmem[7] = 101\nmem[8] = 0' | cabal run 2020_day14-docking-data


updateAssoc :: Eq k => k -> v -> [(k, v)] -> [(k, v)]
updateAssoc key value assoc = (key, value):filter ((key /=) . fst) assoc


execute :: ([(Int, Int)], (Int, Int)) -> [String] -> ([(Int, Int)], (Int, Int))
execute (memMap, (andMask, orMask)) ["mask", "=", maskStr] = (memMap, (newAndMask, newOrMask))
  where newAndMask = str2bin (bool 1 0 . ('0' ==)) -- "and" won't mask any bit except explicit Zeros
        newOrMask  = str2bin (bool 0 1 . ('1' ==)) -- "or"  won't set  any bit except explicit Ones
        str2bin char2int = fst . head . readInt 2 (`elem` "X01") char2int $ maskStr

execute (memMap, (andMask, orMask)) ['m':'e':'m':'[':locStr, "=", valStr] = (updateAssoc loc (val .&. andMask .|. orMask) memMap, (andMask, orMask))
  where [(loc, _)] = reads locStr :: [(Int, String)]
        [(val, _)] = reads valStr :: [(Int, String)]

execute (memMap, (andMask, orMask)) cmdList = ((-1, -1):memMap, (andMask, orMask))


solve :: [String] -> Int
solve = sum . map snd . fst . foldl' execute ([], (maxBound :: Int, 0)) . map words


main :: IO ()
main = interact $ show . solve . lines

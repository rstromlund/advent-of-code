module Main where

--	... find three numbers in your expense report that meet the same criteria.
--
--	Using the above example again, the three entries that sum to 2020 are 979, 366, and 675. Multiplying them together produces the answer, 241861950.
--
--	In your expense report, what is the product of the three entries that sum to 2020?
--
-- Test:
--	% cd ../.. && echo -e '1721\n979\n366\n299\n675\n1456' | cabal run 2020_day01-report-repair_part2


-- slower list comprehension:
solve1 :: [Int] -> Int
solve1 ns = head [x * y * z | x <- ns, y <- ns, x /= y, z <- ns, y /= z, x /= z, 2020 == (x + y + z)]


-- faster b/c combine only generates "downstream" triples and doesn't create duplicates from preceding numbers.
solve :: [Int] -> Maybe Int
solve = combine0
  where combine0 :: [Int] -> Maybe Int
        combine0 []         = undefined
        combine0 (n1:ns)    = case combine1 n1 ns of
                                Just n  -> Just n
                                Nothing -> combine0 ns
        combine1 :: Int -> [Int] -> Maybe Int
        combine1 _ []       = Nothing
        combine1 n1 (n2:ns) = case combine2 n1 n2 ns of
                                Just n  -> Just n
                                Nothing -> combine1 n1 ns
        combine2 :: Int -> Int -> [Int] -> Maybe Int
        combine2 _ _ []     = Nothing
        combine2 n1 n2 (n3:ns)
          | 2020 == n1 + n2 + n3
                            = Just $ n1 * n2 * n3
          | otherwise       = combine2 n1 n2 ns


main :: IO ()
main = interact $ show . solve . map read . words

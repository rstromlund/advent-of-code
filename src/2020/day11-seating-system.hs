module Main where
import Data.Bool (bool)
import Data.List.Split (chunksOf)


--	... The seat layout fits neatly on a grid. Each position is either floor (.), an empty seat (L), or an occupied seat
--	(#). For example, the initial seat layout might look like this:
--
--	L.LL.LL.LL
--	LLLLLLL.LL
--	L.L.L..L..
--	LLLL.LL.LL
--	L.LL.LL.LL
--	L.LLLLL.LL
--	..L.L.....
--	LLLLLLLLLL
--	L.LLLLLL.L
--	L.LLLLL.LL
--
--	Now, you just need to model the people who will be arriving shortly. Fortunately, people are entirely predictable and
--	always follow a simple set of rules. All decisions are based on the number of occupied seats adjacent to a given seat (one
--	of the eight positions immediately up, down, left, right, or diagonal from the seat). The following rules are applied to
--	every seat simultaneously:
--
--	* If a seat is empty (L) and there are no occupied seats adjacent to it, the seat becomes occupied.
--	* If a seat is occupied (#) and four or more seats adjacent to it are also occupied, the seat becomes empty.
--	* Otherwise, the seat's state does not change.
--
--	Floor (.) never changes; seats don't move, and nobody sits on the floor.
--
--	After one round of these rules, every seat in the example layout becomes occupied:
--
--	#.##.##.##
--	#######.##
--	#.#.#..#..
--	####.##.##
--	#.##.##.##
--	#.#####.##
--	..#.#.....
--	##########
--	#.######.#
--	#.#####.##
--
--	After a second round, the seats with four or more occupied adjacent seats become empty again:
--
--	#.LL.L#.##
--	#LLLLLL.L#
--	L.L.L..L..
--	#LLL.LL.L#
--	#.LL.LL.LL
--	#.LLLL#.##
--	..L.L.....
--	#LLLLLLLL#
--	#.LLLLLL.L
--	#.#LLLL.##
--
--	This process continues for three more rounds:
--
--	#.##.L#.##
--	#L###LL.L#
--	L.#.#..#..
--	#L##.##.L#
--	#.##.LL.LL
--	#.###L#.##
--	..#.#.....
--	#L######L#
--	#.LL###L.L
--	#.#L###.##
--
--	#.#L.L#.##
--	#LLL#LL.L#
--	L.L.L..#..
--	#LLL.##.L#
--	#.LL.LL.LL
--	#.LL#L#.##
--	..L.L.....
--	#L#LLLL#L#
--	#.LLLLLL.L
--	#.#L#L#.##
--
--	#.#L.L#.##
--	#LLL#LL.L#
--	L.#.L..#..
--	#L##.##.L#
--	#.#L.LL.LL
--	#.#L#L#.##
--	..L.L.....
--	#L#L##L#L#
--	#.LLLLLL.L
--	#.#L#L#.##
--
--	At this point, something interesting happens: the chaos stabilizes and further applications of these rules cause no seats
--	to change state! Once people stop moving around, you count 37 occupied seats.
--
--	Simulate your seating area by applying the seating rules repeatedly until no seats change state. How many seats end up occupied?
--
-- Test:
--	% cd ../.. && echo -e 'L.LL.LL.LL\nLLLLLLL.LL\nL.L.L..L..\nLLLL.LL.LL\nL.LL.LL.LL\nL.LLLLL.LL\n..L.L.....\nLLLLLLLLLL\nL.LLLLLL.L\nL.LLLLL.LL' | cabal run 2020_day11-seating-system


-- FIXME:
-- This isn't the best solution, lots of 2 dimensional list lookups.  It works in fairly good time though (under 1 minute on a fair powered laptop).
-- A vector or a 1 long packed string w/ array math to pull in the char should do much better.


loadPeople :: Int -> Int -> [[(Int,Int)]] -> [String] -> [String]
loadPeople width height coordMap chart = map (foldr sitDown "") coordMap
  where getChar x y
          | x < 0       = 'L' -- If getting seat outside the chart, return 'L' an empty seat.  I.e. no one sitting there and stop looking further.
          | y < 0       = 'L'
          | x >= width  = 'L'
          | y >= height = 'L'
          | otherwise   = (chart!!y)!!x

        sitDown (x,y) acc = let s = getChar x y
                                o = adjacent x y
                            in determineSeat s o:acc
        determineSeat '.' _ = '.'
        determineSeat 'L' 0 = '#'
        determineSeat '#' o
          | o >= 4          = 'L'
          | otherwise       = '#'
        determineSeat s _   = s

        adjacent x y = occupied (x - 1) (y - 1) + occupied x (y - 1) + occupied (x + 1) (y - 1) +
                       occupied (x - 1)  y      +                      occupied (x + 1)  y +
                       occupied (x - 1) (y + 1) + occupied x (y + 1) + occupied (x + 1) (y + 1)
        occupied x y = bool 0 1 $ '#' == getChar x y


solve :: [String] -> Int
solve chart = length . filter (== '#') . concat . loopUntilEq chart $ loadPeople width height coordMap chart
  where width    = length . head $ chart
        height   = length chart
        coordMap = chunksOf width [(x,y) | y <- [0..(height - 1)], x <- [0..(width - 1)]]
        loopUntilEq old new
          | old == new = old
          | otherwise  = loopUntilEq new $ loadPeople width height coordMap new


main :: IO ()
main = interact $ show . solve . lines

{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Bool (bool)
import Data.IntMap.Strict (IntMap, empty, delete, findWithDefault, foldlWithKey', insert, size)


--	For some reason, your simulated results don't match what the experimental energy source engineers expected. Apparently,
--	the pocket dimension actually has *four spatial dimensions*, not three.
--
--	The pocket dimension contains an infinite 4-dimensional grid. At every integer 4-dimensional coordinate (x,y,z,w),
--	there exists a single cube (really, a *hypercube*) which is still either active or inactive.
--
--	Each cube only ever considers its neighbors: any of the 80 other cubes where any of their coordinates differ by at most
--	1. For example, given the cube at x=1,y=2,z=3,w=4, its neighbors include the cube at x=2,y=2,z=3,w=3, the cube at
--	x=0,y=2,z=3,w=4, and so on.
--
--	The initial state of the pocket dimension still consists of a small flat region of cubes. Furthermore, the same rules for
--	cycle updating still apply: during each cycle, consider the number of active neighbors of each cube.
--
--	For example, consider the same initial state as in the example above. Even though the pocket dimension is 4-dimensional,
--	this initial state represents a small 2-dimensional slice of it. (In particular, this initial state defines a 3x3x1x1
--	region of the 4-dimensional space.)
--
--	Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is
--	shown layer-by-layer at each given z and w coordinate:
--
--	Before any cycles:
--
--	z=0, w=0
--	.#.
--	..#
--	###
--
--	After 1 cycle:
--
--	z=-1, w=-1
--	#..
--	..#
--	.#.
--
--	z=0, w=-1
--	#..
--	..#
--	.#.
--
--	z=1, w=-1
--	#..
--	..#
--	.#.
--
--	z=-1, w=0
--	#..
--	..#
--	.#.
--
--	z=0, w=0
--	#.#
--	.##
--	.#.
--
--	z=1, w=0
--	#..
--	..#
--	.#.
--
--	z=-1, w=1
--	#..
--	..#
--	.#.
--
--	z=0, w=1
--	#..
--	..#
--	.#.
--
--	z=1, w=1
--	#..
--	..#
--	.#.
--
--
--	After 2 cycles:
--
--	z=-2, w=-2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	z=-1, w=-2
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=0, w=-2
--	###..
--	##.##
--	#...#
--	.#..#
--	.###.
--
--	z=1, w=-2
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=2, w=-2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	z=-2, w=-1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=-1, w=-1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=0, w=-1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=1, w=-1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=2, w=-1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=-2, w=0
--	###..
--	##.##
--	#...#
--	.#..#
--	.###.
--
--	z=-1, w=0
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=0, w=0
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=1, w=0
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=2, w=0
--	###..
--	##.##
--	#...#
--	.#..#
--	.###.
--
--	z=-2, w=1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=-1, w=1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=0, w=1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=1, w=1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=2, w=1
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=-2, w=2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	z=-1, w=2
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=0, w=2
--	###..
--	##.##
--	#...#
--	.#..#
--	.###.
--
--	z=1, w=2
--	.....
--	.....
--	.....
--	.....
--	.....
--
--	z=2, w=2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	After the full six-cycle boot process completes, 848 cubes are left in the active state.
--
--	Starting with your given initial configuration, simulate six cycles in a 4-dimensional space. How many cubes are left in
--	the active state after the sixth cycle?
--
-- Test:
--	% cd ../.. && echo -e '.#.\n..#\n###' | cabal run 2020_day17-conway-cubes_part2


-- Using IntMap for efficient insert/lookup with Int key.  Packing (x,y,z,w) into an Int : 000xxxxyyyyzzzzwwww

shiftAmt  :: Int
shiftAmt  = 0x1000

xShift = shiftAmt * shiftAmt * shiftAmt
yShift = shiftAmt * shiftAmt
zShift = shiftAmt
wShift = 1

coordMidpoint :: Int
coordMidpoint = shiftAmt `div` 2

getCoords :: Int -> (Int, Int, Int, Int)
getCoords key = (key `div` xShift `mod` shiftAmt - coordMidpoint,
                 key `div` yShift `mod` shiftAmt - coordMidpoint,
                 key `div` zShift `mod` shiftAmt - coordMidpoint,
                 key              `mod` shiftAmt - coordMidpoint)

mkCoords :: Int -> Int -> Int -> Int -> Int
mkCoords x y z w = (x + coordMidpoint) * xShift + (y + coordMidpoint) * yShift + (z + coordMidpoint) * zShift + (w + coordMidpoint)


universeSet :: Int -> Int -> Int -> Int -> Char -> IntMap Char -> IntMap Char
universeSet x y z w '.' !uni = delete (mkCoords x y z w) uni -- Don't need to store inactive cubes, that's the default; keep map sparse
universeSet x y z w ch  !uni = insert (mkCoords x y z w) ch uni


universeGet :: Int -> Int -> Int -> Int -> IntMap Char -> Char
universeGet x y z w = findWithDefault '.' (mkCoords x y z w)


-- Offset to my 80 neighbors
myNeighbors = [(x,y,z,w) | x <- [-1..1], y <- [-1..1], z <- [-1..1], w <- [-1..1], 0 /= x || 0 /= y || 0 /= z || 0 /= w]

cntNeighbors :: Int -> Int -> Int -> Int -> IntMap Char -> Int
cntNeighbors x y z w uni = foldl' calcNeighbors 0 myNeighbors
  where calcNeighbors acc (ox, oy, oz, ow) = acc + bool 0 1 ('#' == universeGet (x + ox) (y + oy) (z + oz) (w + ow) uni)


-- Left as an exercise for the student: try and use occupied boundaries instead of assumed growing boundaries.  Which one is faster/more efficient?

universeBounds :: IntMap Char -> (Int, Int, Int, Int, Int, Int, Int, Int)
universeBounds = foldlWithKey'
                     (
                       \(minX, maxX, minY, maxY, minZ, maxZ, minW, maxW) k _ ->
                       let (x, y, z, w) = getCoords k
                       in (min x minX, max x maxX, min y minY, max y maxY, min z minZ, max z maxZ, min w minW, max w maxW)
                     )
                     (0, 0, 0, 0, 0, 0, 0, 0)


-- Assume universe grows, at max) 1 dimension each direction per cycle, cheaper than calculating the actual occupied boundaries.
-- But perhaps calculating the actual occupied boundaries would cut out a lot of unnecessary calculations.  What's the best trade off?

-- This function signature is sloppy, too big.  But it fits in the fold and passes min/max per coord.  Should be cleaner for a production app.

cycleUniverse :: (IntMap Char, Int, Int, Int, Int, Int, Int, Int, Int) -> Int -> (IntMap Char, Int, Int, Int, Int, Int, Int, Int, Int)
cycleUniverse (universe, minX, maxX, minY, maxY, minZ, maxZ, minW, maxW) _ =
  (\(uni', _, _, _) -> (uni', minX', maxX', minY', maxY', minZ', maxZ', minW', maxW')) .
  cycleW $ (empty :: IntMap Char)
  where minX' = minX - 1
        maxX' = maxX + 1
        minY' = minY - 1
        maxY' = maxY + 1
        minZ' = minZ - 1
        maxZ' = maxZ + 1
        minW' = minW - 1
        maxW' = maxW + 1
        cycleW uni              = foldl' cycleZ     (uni, 0, 0, 0) [minW'..maxW']
        cycleZ (uni, _, _, _) w = foldl' cycleY     (uni, 0, 0, w) [minZ'..maxZ']
        cycleY (uni, _, _, w) z = foldl' cycleX     (uni, 0, z, w) [minY'..maxY']
        cycleX (uni, _, z, w) y = foldl' calcStatus (uni, y, z, w) [minX'..maxX']
        --
        calcStatus (uni, y, z, w) x =
          let myStatus  = universeGet  x y z w universe
              neighbors = cntNeighbors x y z w universe
              !uni'
                 | '#' == myStatus && neighbors >= 2 && neighbors <= 3 = universeSet x y z w '#' uni -- survives
                 | '#' /= myStatus && 3 == neighbors                   = universeSet x y z w '#' uni -- becomes active
                 | '.' == myStatus                                     = uni -- no change (wasn't set, not setting)
                 | otherwise                                           = universeSet x y z w '.' uni -- else make inactive
          in (uni', y, z, w)


solve :: [String] -> Int
solve ls = size $ (\(uni, _, _, _, _, _, _, _, _) -> uni) looped
  where height = length ls
        width = length . head $ ls
        minY = (-height) `div` 2 + 1
        minX = (-width) `div` 2 + 1
        z = 0 -- Initializing a 2D plane in a 4D universe, Z=0 and W=0 for the starting plane.
        w = 0
        (maxX, maxY, universe)  = foldl' genY (0, minY - 1, empty :: IntMap Char) ls
        genY (x, y, uni) xs     = foldl' genX (minX - 1, y + 1, uni) xs
        genX (x, y, uni) ch     = (x + 1, y, universeSet (x + 1) y z w ch uni)
        --
        looped = foldl' cycleUniverse (universe, minX, maxX, minY, maxY, z, z, w, w) [1..6]


main :: IO ()
main = interact $ show . solve . lines

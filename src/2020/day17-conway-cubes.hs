module Main where
import Data.Bool (bool)
import qualified Data.IntMap.Strict as I


--	... In the initial state of the pocket dimension, almost all cubes start inactive. The only exception to this is a small
--	flat region of cubes (your puzzle input); the cubes in this region start in the specified active (#) or inactive (.)
--	state.
--
--	The energy source then proceeds to boot up by executing six cycles.
--
--	Each cube only ever considers its neighbors: any of the 26 other cubes where any of their coordinates differ by at most
--	1. For example, given the cube at x=1,y=2,z=3, its neighbors include the cube at x=2,y=2,z=2, the cube at x=0,y=2,z=3, and
--	so on.
--
--	During a cycle, all cubes simultaneously change their state according to the following rules:
--
--	* If a cube is active and exactly 2 or 3 of its neighbors are also active, the cube remains active. Otherwise, the cube becomes inactive.
--	* If a cube is inactive but exactly 3 of its neighbors are active, the cube becomes active. Otherwise, the cube remains inactive.
--
--	The engineers responsible for this experimental energy source would like you to simulate the pocket dimension and
--	determine what the configuration of cubes should be at the end of the six-cycle boot process.
--
--	For example, consider the following initial state:
--	.#.
--	..#
--	###
--
--	Even though the pocket dimension is 3-dimensional, this initial state represents a small 2-dimensional slice of it. (In
--	particular, this initial state defines a 3x3x1 region of the 3-dimensional space.)
--
--	Simulating a few cycles from this initial state produces the following configurations, where the result of each cycle is
--	shown layer-by-layer at each given z coordinate (and the frame of view follows the active cells in each cycle):
--
--	Before any cycles:
--
--	z=0
--	.#.
--	..#
--	###
--
--	After 1 cycle:
--
--	z=-1
--	#..
--	..#
--	.#.
--
--	z=0
--	#.#
--	.##
--	.#.
--
--	z=1
--	#..
--	..#
--	.#.
--
--	After 2 cycles:
--
--	z=-2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	z=-1
--	..#..
--	.#..#
--	....#
--	.#...
--	.....
--
--	z=0
--	##...
--	##...
--	#....
--	....#
--	.###.
--
--	z=1
--	..#..
--	.#..#
--	....#
--	.#...
--	.....
--
--	z=2
--	.....
--	.....
--	..#..
--	.....
--	.....
--
--	After 3 cycles:
--
--	z=-2
--	.......
--	.......
--	..##...
--	..###..
--	.......
--	.......
--	.......
--
--	z=-1
--	..#....
--	...#...
--	#......
--	.....##
--	.#...#.
--	..#.#..
--	...#...
--
--	z=0
--	...#...
--	.......
--	#......
--	.......
--	.....##
--	.##.#..
--	...#...
--
--	z=1
--	..#....
--	...#...
--	#......
--	.....##
--	.#...#.
--	..#.#..
--	...#...
--
--	z=2
--	.......
--	.......
--	..##...
--	..###..
--	.......
--	.......
--	.......
--
--	After the full six-cycle boot process completes, 112 cubes are left in the active state.
--
--	Starting with your given initial configuration, simulate six cycles. How many cubes are left in the active state after the sixth cycle?
--
-- Test:
--	% cd ../.. && echo -e '.#.\n..#\n###' | cabal run 2020_day17-conway-cubes


-- Masking (x,y,z) into an Int. 00xxxxxxyyyyyyzzzzzz

shiftAmt :: Int
shiftAmt = 10000

xShift = shiftAmt * shiftAmt
yShift = shiftAmt
zShift = 1

coordMidpoint :: Int
coordMidpoint = shiftAmt `div` 2

getCoords :: Int -> (Int, Int, Int)
getCoords key = (key `div` xShift `mod` shiftAmt - coordMidpoint, key `div` yShift `mod` shiftAmt - coordMidpoint, key `mod` shiftAmt - coordMidpoint)

mkCoords :: Int -> Int -> Int -> Int
mkCoords x y z = (x + coordMidpoint) * xShift + (y + coordMidpoint) * yShift + (z + coordMidpoint) * zShift

universeSet :: Int -> Int -> Int -> Char -> I.IntMap Char -> I.IntMap Char
universeSet x y z '.' uni = I.delete (mkCoords x y z) uni
universeSet x y z '#' uni = I.insert (mkCoords x y z) '#' uni
universeSet x y z _   uni = undefined

universeGet :: Int -> Int -> Int -> I.IntMap Char -> Char
universeGet x y z = I.findWithDefault '.' (mkCoords x y z)

cubeNeighbors :: Int -> Int -> Int -> I.IntMap Char -> Int
cubeNeighbors x y z uni = foldl' calcNeighbors 0 [(x,y,z) | x <- [x-1..x+1], y <- [y-1..y+1], z <- [z-1..z+1]]
  where calcNeighbors acc (nx, ny, nz)
          | nx == x && ny == y && nz == z = acc
          | otherwise                     = acc + bool 0 1 ('#' == universeGet nx ny nz uni)


cycleUniverse :: (I.IntMap Char, Int, Int, Int, Int, Int, Int) -> Int -> (I.IntMap Char, Int, Int, Int, Int, Int, Int)
cycleUniverse (universe, minX, maxX, minY, maxY, minZ, maxZ) _ = (\(newUni, _, _) -> (newUni, newMinX, newMaxX, newMinY, newMaxY, newMinZ, newMaxZ)) .
  cycleZ $ (I.empty :: I.IntMap Char)
  where newMinX = minX - 1
        newMaxX = maxX + 1
        newMinY = minY - 1
        newMaxY = maxY + 1
        newMinZ = minZ - 1
        newMaxZ = maxZ + 1
        cycleZ uni           = foldl' cycleY   (uni, 0, 0) [newMinZ..newMaxZ]
        cycleY (uni, _, _) z = foldl' cycleX   (uni, 0, z) [newMinY..newMaxY]
        cycleX (uni, _, z) y = foldl' calcNbrs (uni, y, z) [newMinX..newMaxX]
        --
        calcNbrs (uni, y, z) x = let myActivity = universeGet x y z universe
                                     neighbors  = cubeNeighbors x y z universe
                                     nextUni
                                       | '#' == myActivity && neighbors >= 2 && neighbors <= 3 = universeSet x y z '#' uni -- survives
                                       | '#' /= myActivity && 3 == neighbors                   = universeSet x y z '#' uni -- becomes active
                                       | '.' == myActivity                                     = uni -- no change (wasn't set, not setting)
                                       | otherwise                                             = universeSet x y z '.' uni -- else make inactive
                                 in (nextUni, y, z)


-- Only used in debugging, not used in final answer
printUniverse :: (I.IntMap Char, Int, Int, Int, Int, Int, Int) -> String
printUniverse (uni, minX, maxX, minY, maxY, minZ, maxZ) = (\(str, _, _) -> str) . printZ $ ("Active=" ++ show (I.size uni) ++ "\n")
  where printZ str           = foldl' printY (str, 0, 0) [minZ..maxZ]
        printY :: (String, Int, Int) -> Int -> (String, Int, Int)
        printY (str, _, _) z = foldl' printX (str ++ "\n\nz=" ++ show z ++ "\n", 0, z) [minY..maxY]
        printX :: (String, Int, Int) -> Int -> (String, Int, Int)
        printX (str, _, z) y = foldl' printV (str ++ "\n", y, z) [minX..maxX]
        --
        printV :: (String, Int, Int) -> Int -> (String, Int, Int)
        printV (str, y, z) x = let myActivity = universeGet x y z uni
                               in (str ++ [myActivity], y, z)


solve :: [String] -> Int
solve ls = I.size $ (\(uni, _, _, _, _, _, _) -> uni) looped
  where height = length ls
        width = length . head $ ls
        minY = (-height) `div` 2 + 1
        minX = (-width) `div` 2 + 1
        minZ = 0
        maxZ = 0
        (maxX, maxY, universe)  = foldl' genY (0, minY - 1, I.empty :: I.IntMap Char) ls
        genY (x, y, us) xs = foldl' genX (minX - 1, y + 1, us) xs
        genX (x, y, us) ch = (x + 1, y, universeSet (x + 1) y 0 ch us)
        looped = foldl' cycleUniverse (universe, minX, maxX, minY, maxY, minZ, maxZ) [1..6]


main :: IO ()
main = interact $ show . solve . lines

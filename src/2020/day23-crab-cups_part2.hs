{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Char (digitToInt)
import Data.IntMap.Strict (IntMap, (!), fromList, insert)


--	... you are quite surprised when the crab starts arranging *many* cups in a circle on your raft - *one million* (1000000) in total.
--
--	Your labeling is still correct for the first few cups; after that, the remaining cups are just numbered in an increasing
--	fashion starting from the number after the highest number in your list and proceeding one by one until one million is
--	reached. (For example, if your labeling were 54321, the cups would be numbered 5, 4, 3, 2, 1, and then start counting up
--	from 6 until one million is reached.) In this way, every number from one through one million is used exactly once.
--
--	After discovering where you made the mistake in translating Crab Numbers, you realize the small crab isn't going to do
--	merely 100 moves; the crab is going to do *ten million* (10000000) moves!
--
--	The crab is going to hide your stars - one each - under the *two cups that will end up immediately clockwise of cup 1*. You
--	can have them if you predict what the labels on those cups will be when the crab is finished.
--
--	In the above example (389125467), this would be 934001 and then 159792; multiplying these together produces 149245887792.
--
--	Determine which two cups will end up immediately clockwise of cup 1. What do you get if you multiply their labels together?
--
-- Test:
--	% cd ../.. && echo '389125467' | cabal run 2020_day23-crab-cups_part2
-- Go:
--	% cd ../.. && echo '389547612' | cabal run 2020_day23-crab-cups_part2


-- FIXME: (?) This takes a couple of minutes, but that isn't bad for an immutable solution.  A mutable vector in the state monad
-- or something else that's mutable would surely perform better.  But that is above my haskell paygrade for now.  Maybe I'll
-- come back and touch it up when I am more of a haskell-grown-up.


mx = 1000 * 1000
loops = 10 * 1000 * 1000

-- These next 2 will test against part1's puzzle for faster debugging/testing:
--mx = 9
--loops = 100


solve :: [Int] -> Int
solve cs = after1 . play 0 (head cs) $ startingCups
  where csxtra  = cs ++ [10..mx] -- cs plus the extra padding up to max
        startingCups = fromList $ (last csxtra, head csxtra):zip csxtra (tail csxtra)
        --
        after1 :: IntMap Int -> Int
        after1 is =
          let c1  = is ! 1
              c2  = is ! c1
          in c1 * c2
        findNextCup :: Int -> Int -> Int -> Int -> IntMap Int -> Int
        findNextCup cur c1 c2 c3 is
          | cur < 1   = findNextCup mx c1 c2 c3 is
          | cur == c1 || cur == c2 || cur == c3
                      = findNextCup (pred cur) c1 c2 c3 is
          | otherwise = cur
        insertAfter :: Int -> Int -> Int -> Int -> Int -> IntMap Int -> IntMap Int
        insertAfter cur c1 c3 nxt dst is =
          -- cur node points to next (splitting off the 3 cups)
          -- dst cup points to first of 3
          -- last of 3 points to what dst used to point to.
          insert c3 (is ! dst) . insert dst c1 . insert cur nxt $ is
        --
        -- One of these params caused a stack overflow, using bang to keep this in constant space.
        play :: Int -> Int -> IntMap Int -> IntMap Int
        play !cnt !cur !is
          | cnt >= loops = is
          | otherwise    =
              let c1  = is ! cur
                  c2  = is ! c1
                  c3  = is ! c2
                  nxt = is ! c3
                  dst = findNextCup (pred cur) c1 c2 c3 is
                  is' = insertAfter cur c1 c3 nxt dst is
              in play (1 + cnt) nxt is'


main :: IO ()
main = interact $ show . solve . map digitToInt . head . words

module Main where
import Data.List.Split (chunksOf)

--	Each policy actually describes two positions in the password, where 1 means the first character, 2 means the
--	second character, and so on. (Be careful; Toboggan Corporate Policies have no concept of "index zero"!) Exactly
--	one of these positions must contain the given letter. Other occurrences of the letter are irrelevant for the
--	purposes of policy enforcement.
--
--	Given the same example list from above:
--
--		1-3 a: abcde is valid: position 1 contains a and position 3 does not.
--		1-3 b: cdefg is invalid: neither position 1 nor position 3 contains b.
--		2-9 c: ccccccccc is invalid: both position 2 and position 9 contain c.
--	How many passwords are valid according to the new interpretation of the policies?
--
-- Test:
--	% cd ../.. && echo -e '1-3 a: abcde\n1-3 b: cdefg\n2-9 c: ccccccccc' | cabal run 2020_day02-password-philosophy_part2

solve :: [String] -> Bool
solve [rng, chs, pwd] = lowerPass /= upperPass
  -- ^^ Boolean xor is the same as (/=); i.e. if both are True xor == False, if both are False xor == False, else when different xor == True
  where (smn, smx) = span (/= '-') rng
        mn = read smn
        mx = read (drop 1 smx)
        cnt = length pwd
        lowerPass = mn <= cnt && pwd!!(mn - 1) == head chs
        upperPass = mx <= cnt && pwd!!(mx - 1) == head chs

main :: IO ()
main = interact $ show . length . filter id . map solve . chunksOf 3 . words

module Main where
import Data.Array (Array, assocs, bounds, inRange, listArray, (!))
import Data.Bifunctor (bimap)
import Data.Char (isAlpha, isSpace)

--	... starting point is just off the top of the diagram. Lines (drawn with |, -, and +) show the path it
--	needs to take, starting by going down onto the only line connected to the top of the diagram. It needs
--	to follow this path until it reaches the end (located somewhere within the diagram) and stop there.
--
--	Sometimes, the lines cross over each other; in these cases, it needs to continue going the same
--	direction, and only turn left or right when there's no other option. In addition, someone has left
--	letters on the line; these also don't change its direction, but it can use them to keep track of where
--	it's been. For example:
--
--	     |
--	     |  +--+
--	     A  |  C
--	 F---|----E|--+
--	     |  |  |  D
--	     +B-+  +--+
--
--	Given this diagram, the packet needs to take the following path:
--
--	* Starting at the only line touching the top of the diagram, it must go down, pass through A, and continue onward to the first +.
--	* Travel right, up, and right, passing through B in the process.
--	* Continue down (collecting C), right, and up (collecting D).
--	* Finally, go all the way left through E and stopping at F.
--
--	Following the path to the end, the letters it sees on its path are ABCDEF.
--
--	The little packet looks up at you, hoping you can help it find the way. What letters will it see (in
--	the order it would see them) if it follows the path? (The routing diagram is very wide; make sure you
--	view it without line wrapping.)
--
-- Test:
--	% cd ../.. && echo -e '     |          \n     |  +--+    \n     A  |  C    \n F---|----E|--+ \n     |  |  |  D \n     +B-+  +--+ ' | cabal run 2017_day19-a_series_of_tubes # = ABCDEF

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) cs
  where ls = lines cs
        ly = pred . length $ ls
        lx = length . head $ ls

cardinalDirections :: [Point]
cardinalDirections = [
  ( 1,  0), -- 1:down
  (-1,  0), -- 0:up
  ( 0, -1), -- 2:left
  ( 0,  1)  -- 3:right
  ]

getNeighbors :: PuzzleMap -> Point -> Point -> [Point]
getNeighbors net oldpt (y, x) = filter (\pt -> pt /= oldpt && inRange (bounds net) pt && (not . isSpace) (net ! pt)) . map (bimap (+ y) (+ x)) $ cardinalDirections

solve :: PuzzleMap -> String
solve net = walk (head cardinalDirections) start start '|' ""
  where start@(sy, sx) = fst . head . filter (('|' ==) . snd) . assocs $ net
        walk :: Point -> Point -> Point -> Char -> String -> String
        walk dir@(dy, dx) oldpt curpt ch acc
          | '+' == ch  = -- look around for the next step (but don't go backwards)
                         let [curpt'@(y', x')] = getNeighbors net oldpt curpt
                             ch' = net ! curpt'
                             _curpt@(y, x) = curpt
                         in walk (y' - y, x' - x) curpt curpt' ch' acc
          | isAlpha ch = walk dir curpt curpt' ch' (ch:acc) -- store the letter and move on
          | isSpace ch = reverse acc -- no track left, end the maze walking
          | otherwise  = walk dir curpt curpt' ch' acc -- nothing to see here, move along
          where curpt' = bimap (+ dy) (+ dx) curpt
                ch' = net ! curpt'


main :: IO ()
main = interact $ solve . parse

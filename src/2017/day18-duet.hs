module Main where
import Data.Array ((//), (!), Array, bounds, inRange, listArray)
import Data.HashMap.Strict ((!?), HashMap, empty, findWithDefault, insert, singleton)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... operate on a set of registers that are each named with a single letter and that can each hold a single integer. You suppose each register should start with a value of 0.
--
--	There aren't that many instructions, so it shouldn't be hard to figure out what they do. Here's what you determine:
--
--	* snd X plays a sound with a frequency equal to the value of X.
--	* set X Y sets register X to the value of Y.
--	* add X Y increases register X by the value of Y.
--	* mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
--	* mod X Y sets register X to the remainder of dividing the value contained in register X by the value of Y (that is, it sets X to the result of X modulo Y).
--	* rcv X recovers the frequency of the last sound played, but only when the value of X is not zero. (If it is zero, the command does nothing.)
--	* jgz X Y jumps with an offset of the value of Y, but only if the value of X is greater than zero. (An
--	  offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so
--	  on.)
--
--	Many of the instructions can take either a register (a single letter) or a number. The value of a
--	register is the integer it contains; the value of a number is that number.
--
--	After each jump instruction, the program continues with the instruction to which the jump
--	jumped. After any other instruction, the program continues with the next instruction. Continuing (or
--	jumping) off either end of the program terminates it.
--
--	For example:
--
--	set a 1
--	add a 2
--	mul a a
--	mod a 5
--	snd a
--	set a 0
--	rcv a
--	jgz a -1
--	set a 1
--	jgz a -2
--
--	* The first four instructions set a to 1, add 2 to it, square it, and then set it to itself modulo 5, resulting in a value of 4.
--	* Then, a sound with frequency 4 (the value of a) is played.
--	* After that, a is set to 0, causing the subsequent rcv and jgz instructions to both be skipped (rcv because a is 0, and jgz because a is not greater than 0).
--	* Finally, a is set to 1, causing the next jgz instruction to activate, jumping back two instructions
--	  to another jump, which jumps again to the rcv, which ultimately triggers the recover operation.
--
--	At the time the recover operation is executed, the frequency of the last sound played is 4.
--
--	What is the value of the recovered frequency (the value of the most recently played sound) the first
--	time a rcv instruction is executed with a non-zero value?
--
-- Test:
--	% cd ../.. && echo -e 'set a 1\nadd a 2\nmul a a\nmod a 5\nsnd a\nset a 0\nrcv a\njgz a -1\nset a 1\njgz a -2' | cabal run 2017_day18-duet # = 4

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Snd Memory
                 | Set Memory Memory
                 | Add Memory Memory
                 | Mul Memory Memory
                 | Mod Memory Memory
                 | Rcv Memory
                 | Jgz Memory Memory
                 deriving (Show, Eq, Ord)

type Parser = Parsec Void String

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

instructions :: Parser [Instruction]
instructions = many (instruction <* newline) <* eof

instruction :: Parser Instruction
instruction =
  string     "snd" *> space *> (Snd <$> mkMemory)
  <|> string "set" *> space *> (Set <$> mkMemory <*> (space *> mkMemory))
  <|> string "add" *> space *> (Add <$> mkMemory <*> (space *> mkMemory))
  <|> string "mul" *> space *> (Mul <$> mkMemory <*> (space *> mkMemory))
  <|> string "mod" *> space *> (Mod <$> mkMemory <*> (space *> mkMemory))
  <|> string "rcv" *> space *> (Rcv <$> mkMemory)
  <|> string "jgz" *> space *> (Jgz <$> mkMemory <*> (space *> mkMemory))

data CPU = CPU {iptr :: !Int, rsnd :: !Int, regs :: HashMap Char Int} deriving (Show)
type VM = (Array Int Instruction, CPU)

get (Constant i)        = const i
get (Register ch)       = findWithDefault 0 ch . regs
set (Register ch) r cpu = cpu{regs = insert ch r (regs cpu)}

exec :: VM -> CPU
exec vm@(is, cpu@CPU{iptr = ip})
  | inRange (bounds is) ip = exec . step vm $ is ! ip
  | otherwise              = cpu
  where step :: VM -> Instruction -> VM
        step (is, cpu) (Snd m1)    = (is, cpu{rsnd = get m1 cpu, iptr = ip + 1})
        step (is, cpu) (Set m1 m2) = (is, (set m1 (get m2 cpu) cpu){iptr = ip + 1})
        step (is, cpu) (Add m1 m2) = (is, (set m1 (get m1 cpu + get m2 cpu) cpu){iptr = ip + 1})
        step (is, cpu) (Mul m1 m2) = (is, (set m1 (get m1 cpu * get m2 cpu) cpu){iptr = ip + 1})
        step (is, cpu) (Mod m1 m2) = (is, (set m1 (get m1 cpu `mod` get m2 cpu) cpu){iptr = ip + 1})
        step (is, cpu) (Jgz m1 m2) = (is, cpu{iptr = ip + (if get m1 cpu > 0 then get m2 cpu else 1)})
        step (is, cpu) (Rcv m1)
          | 0 == get m1 cpu = (is, cpu{iptr = ip + 1})
          | otherwise       = (is, cpu{iptr = maxBound})

solve :: Either a [Instruction] -> Int
solve (Right instrs) = rsnd $ exec (is, CPU{iptr = 0, rsnd = 0, regs = empty})
  where is = listArray (0, length instrs - 1) instrs


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

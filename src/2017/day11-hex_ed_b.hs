module Main where
import Data.List.Split (splitOn)


--	How many steps away is the furthest he ever got from his starting position?
--
-- Test:
--	% cd ../.. && echo -e 'ne,ne,ne' | cabal run 2017_day11-hex_ed_b
--	% cd ../.. && echo -e 'ne,ne,sw,sw' | cabal run 2017_day11-hex_ed_b
--	% cd ../.. && echo -e 'ne,ne,s,s' | cabal run 2017_day11-hex_ed_b
--	% cd ../.. && echo -e 'se,sw,se,sw,sw' | cabal run 2017_day11-hex_ed_b


parse :: String -> [String]
parse = splitOn "," . concat . lines

solve :: [String] -> Int
solve = maximum . map score . move (0, 0)
  where move :: (Int, Int) -> [String] -> [(Int, Int)]
        move _ [] = []
        move (x, y) ("nw":ds) = let pt = (x - 1, y + 1) in pt:move pt ds
        move (x, y) ("n" :ds) = let pt = (x + 0, y + 2) in pt:move pt ds
        move (x, y) ("ne":ds) = let pt = (x + 1, y + 1) in pt:move pt ds
        move (x, y) ("sw":ds) = let pt = (x - 1, y - 1) in pt:move pt ds
        move (x, y) ("s" :ds) = let pt = (x + 0, y - 2) in pt:move pt ds
        move (x, y) ("se":ds) = let pt = (x + 1, y - 1) in pt:move pt ds
        --
        score :: (Int, Int) -> Int
        score (x, y)
          | abs y >= abs x = abs x + (abs y - abs x) `div` 2
          | otherwise      = abs x


main :: IO ()
main = interact $ show . solve . parse

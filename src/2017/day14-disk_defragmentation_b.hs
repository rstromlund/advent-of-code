{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Array ((!), (//), Array, bounds, indices, inRange, listArray)
import Data.Bits ((.&.), shiftR, xor)
import Data.List.Split (chunksOf)
import Data.Word (Word8)

--	Now, all the defragmenter needs to know is the number of regions. A region is a group of used squares
--	that are all adjacent, not including diagonals. Every used square is in exactly one region: lone used
--	squares form their own isolated regions, while several adjacent squares all count as a single region.
--
--	In the example above, the following nine regions are visible, each marked with a distinct digit:
--
--	11.2.3..-->
--	.1.2.3.4
--	....5.6.
--	7.8.55.9
--	.88.5...
--	88..5..8
--	.8...8..
--	88.8.88.-->
--	|      |
--	V      V
--
--	Of particular interest is the region marked 8; while it does not appear contiguous in this small view,
--	all of the squares marked 8 are connected when considering the whole 128x128 grid. In total, in this
--	example, 1242 regions are present.
--
--	How many regions are present given your key string?
--
-- Test:
--	% cd ../.. && echo -e 'flqrgnkx' | cabal run 2017_day14-disk_defragmentation_b # 1242

type Length = Int

parse :: String -> [Length]
parse = map fromEnum . safeHead . lines
  where safeHead []    = []
        safeHead (x:_) = x

listSize  = 256
maxRounds = 64
postfix   = [17, 31, 73, 47, 23] :: [Length]

knot :: [Length] -> [Word8]
knot !ls = map (foldl1' xor) . chunksOf 16 . knot' 0 0 ls' $ [0..fromIntegral (listSize - 1)]
  where ls' = concat . replicate maxRounds . (++ postfix) $ ls
        words2integer :: [Word8] -> Integer
        words2integer = foldl' (\acc x -> acc * 0x100 + fromIntegral x) 0
        --
        knot' :: Int -> Int -> [Length] -> [Word8] -> [Word8]
        knot' pos   _    []      is = take listSize . drop (listSize - (pos `mod` listSize)) . cycle $ is
        knot' !pos !skip (l:ls) !is = knot' (pos + l + skip) (skip + 1) ls . take listSize . drop (l + skip) . cycle $ is'
          where is' = let (bf, af) = splitAt l is
                      in reverse bf ++ af

-- number of bits in a nibble
nibble2int  0 = 0
nibble2int  1 = 1
nibble2int  2 = 1
nibble2int  3 = 2
nibble2int  4 = 1
nibble2int  5 = 2
nibble2int  6 = 2
nibble2int  7 = 3
nibble2int  8 = 1
nibble2int  9 = 2
nibble2int 10 = 2
nibble2int 11 = 3
nibble2int 12 = 2
nibble2int 13 = 3
nibble2int 14 = 3
nibble2int 15 = 4
nibble2int x = error $ show ("ERROR undefined context for 'nibble2int'", x)

-- number of bits in an 8bit word (2 nibbles)
word2int :: Word8 -> Int
word2int w = nibble2int ((w `shiftR` 4) .&. 15) + nibble2int (w .&. 15)

nibble2list :: Word8 -> [Word8] -> [Word8]
nibble2list i bs = snd . foldl' (\(i, bs) _ -> (i `shiftR` 1, (i .&. 1):bs)) (i, bs) $ [0..3]

word2list :: Word8 -> [Word8]
word2list i = nibble2list (i `shiftR` 4) [] ++ nibble2list i []

gridSize = 128
type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Word8

findRegions :: PuzzleMap -> (Int, PuzzleMap)
findRegions !hd = foldl' findRegion (0, hd) . indices $ hd
  where findRegion :: (Int, PuzzleMap) -> Point -> (Int, PuzzleMap)
        findRegion (cnt, hd) pt@(y, x)
          | (not . inRange (bounds hd) $ pt) || 0 == hd ! pt = (cnt, hd)
          | otherwise = let hd' = hd // [(pt, 0)]
                            (_, hdW) = findRegion (cnt, hd') (y, x - 1)
                            (_, hdE) = findRegion (cnt, hdW) (y, x + 1)
                            (_, hdN) = findRegion (cnt, hdE) (y - 1, x)
                            (_, hdS) = findRegion (cnt, hdN) (y + 1, x)
                        in (cnt + 1, hdS)

solve :: [Length] -> Int
solve ls = fst . findRegions $ hd
  where hashVal = map (knot . (ls ++) . map fromEnum . ("-" ++) . show) [0..(gridSize - 1)]
        bitmap  = concatMap (concatMap word2list) hashVal
        hd = listArray ((0, 0), (gridSize - 1, gridSize - 1)) bitmap
        --
        score :: [[Word8]] -> Int
        -- knot returns a list of 16 (128 / 4); but we `take` to be safe.
        score = sum . map (sum . map word2int . take (gridSize `div` 4))

main :: IO ()
main = interact $ show . solve . parse

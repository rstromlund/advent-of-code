module Main where
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	Now, you need to pass through the firewall without being caught - easier said than done.
--
--	You can't control the speed of the packet, but you can delay it any number of picoseconds. For each
--	picosecond you delay the packet before beginning your trip, all security scanners move one
--	step. You're not in the firewall during this time; you don't enter layer 0 until you stop delaying the
--	packet.
--
--	In the example above, if you delay 10 picoseconds (picoseconds 0 - 9), you won't get caught:
--
--	State after delaying:
--	 0   1   2   3   4   5   6
--	[ ] [S] ... ... [ ] ... [ ]
--	[ ] [ ]         [ ]     [ ]
--	[S]             [S]     [S]
--	                [ ]     [ ]
--
--	Picosecond 10:
--	 0   1   2   3   4   5   6
--	( ) [S] ... ... [ ] ... [ ]
--	[ ] [ ]         [ ]     [ ]
--	[S]             [S]     [S]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	( ) [ ] ... ... [ ] ... [ ]
--	[S] [S]         [S]     [S]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--
--	Picosecond 11:
--	 0   1   2   3   4   5   6
--	[ ] ( ) ... ... [ ] ... [ ]
--	[S] [S]         [S]     [S]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	[S] (S) ... ... [S] ... [S]
--	[ ] [ ]         [ ]     [ ]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--
--	Picosecond 12:
--	 0   1   2   3   4   5   6
--	[S] [S] (.) ... [S] ... [S]
--	[ ] [ ]         [ ]     [ ]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	[ ] [ ] (.) ... [ ] ... [ ]
--	[S] [S]         [S]     [S]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--
--	Picosecond 13:
--	 0   1   2   3   4   5   6
--	[ ] [ ] ... (.) [ ] ... [ ]
--	[S] [S]         [S]     [S]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	[ ] [S] ... (.) [ ] ... [ ]
--	[ ] [ ]         [ ]     [ ]
--	[S]             [S]     [S]
--	                [ ]     [ ]
--
--
--	Picosecond 14:
--	 0   1   2   3   4   5   6
--	[ ] [S] ... ... ( ) ... [ ]
--	[ ] [ ]         [ ]     [ ]
--	[S]             [S]     [S]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	[ ] [ ] ... ... ( ) ... [ ]
--	[S] [S]         [ ]     [ ]
--	[ ]             [ ]     [ ]
--	                [S]     [S]
--
--
--	Picosecond 15:
--	 0   1   2   3   4   5   6
--	[ ] [ ] ... ... [ ] (.) [ ]
--	[S] [S]         [ ]     [ ]
--	[ ]             [ ]     [ ]
--	                [S]     [S]
--
--	 0   1   2   3   4   5   6
--	[S] [S] ... ... [ ] (.) [ ]
--	[ ] [ ]         [ ]     [ ]
--	[ ]             [S]     [S]
--	                [ ]     [ ]
--
--
--	Picosecond 16:
--	 0   1   2   3   4   5   6
--	[S] [S] ... ... [ ] ... ( )
--	[ ] [ ]         [ ]     [ ]
--	[ ]             [S]     [S]
--	                [ ]     [ ]
--
--	 0   1   2   3   4   5   6
--	[ ] [ ] ... ... [ ] ... ( )
--	[S] [S]         [S]     [S]
--	[ ]             [ ]     [ ]
--	                [ ]     [ ]
--
--	Because all smaller delays would get you caught, the fewest number of picoseconds you would need to delay to get through safely is 10.
--
--	What is the fewest number of picoseconds that you need to delay the packet to pass through the firewall without being caught?
--
-- Test:
--	% cd ../.. && echo -e '0: 3\n1: 2\n4: 4\n6: 4' | cabal run 2017_day13-packet_scanners_b # 10

type Layer = (Int, Int) -- (layer, range)

type Parser = Parsec Void String

parse :: Parser [Layer]
parse = many (layer <* newline) <* eof

layer :: Parser Layer
layer = (,) <$> decimal <*> (string ": " *> decimal)

{-
Sieve out all multiples of the range period at the time offset; what is left is valid delays.  Take the first one.
Notice: the #10 is not in any of these lists; it is the first valid delay time for the example.

(0+t) mod 4 = 0  : 0, 4, 8, 12, 16, 20
(1+t) mod 2 = 0  : 1, 3, 5, 7, 9, 11, 13, 15, 17, 19
(4+t) mod 6 = 0  : 2, 8, 14, 20
(6+t) mod 6 = 0  : 6, 12, 18
-}

uncaught :: [Int] -> Layer -> [Int]
uncaught acc (l, r) = filter (\t -> ((t - f) `mod` r') /= 0) acc -- keep all valid delay times (remove the "caught" times)
  where r' = r * 2 - 2 -- range period
        f  = (r' - l) `mod` r' -- first "caught" delay time

solve :: Either a [Layer] -> Int
solve (Right ns) = head . foldl' uncaught [1..] $ ns


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

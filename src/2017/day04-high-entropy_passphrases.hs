module Main where
import Data.List (group, sort)

--	A new system policy has been put in place that requires all accounts to use a passphrase instead of
--	simply a password. A passphrase consists of a series of words (lowercase letters) separated by spaces.
--
--	To ensure security, a valid passphrase must contain no duplicate words.
--
--	For example:
--
--	* aa bb cc dd ee is valid.
--	* aa bb cc dd aa is not valid - the word aa appears more than once.
--	* aa bb cc dd aaa is valid - aa and aaa count as different words.
--
--	The system's full passphrase list is available as your puzzle input. How many passphrases are valid?
--
-- Test:
--	% cd ../.. && echo -e 'aa bb cc dd ee\naa bb cc dd aa\naa bb cc dd aaa' | cabal run 2017_day04-high-entropy_passphrases # = 2

type Passphrase = [[String]]

parse :: String -> Passphrase
parse = map words . lines

solve :: Passphrase -> Int
solve = length . filter id . map (all ((== 1) . length) . group . sort)


main :: IO ()
main = interact $ show . solve . parse

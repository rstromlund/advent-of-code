module Main where
import Data.Array ((!), Array, bounds, inRange, listArray)
import Data.Bifunctor (bimap, first)
import Data.HashMap.Strict (HashMap, empty, findWithDefault, insert)
import Data.Tuple (swap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The code it's running seems to be a variant of the kind you saw recently on that tablet. The
--	general functionality seems very similar, but some of the instructions are different:
--
--	* set X Y sets register X to the value of Y.
--	* sub X Y decreases register X by the value of Y.
--	* mul X Y sets register X to the result of multiplying the value contained in register X by the value of Y.
--	* jnz X Y jumps with an offset of the value of Y, but only if the value of X is not zero. (An offset of 2 skips the next instruction, an offset of -1 jumps to the previous instruction, and so on.)
--
--	Only the instructions listed above are used. The eight registers here, named a through h, all start at 0.
--
--	The coprocessor is currently set to some kind of debug mode, which allows for testing, but prevents it
--	from doing any meaningful work.
--
--	If you run the program (your puzzle input), how many times is the mul instruction invoked?
--
-- Test:
--	% cd ../.. && echo -e 'mul a 1' | cabal run 2017_day23-coprocessor_conflagration # = 1

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Set Memory Memory
                 | Sub Memory Memory
                 | Mul Memory Memory
                 | Jnz Memory Memory
                 deriving (Show, Eq, Ord)

type Parser = Parsec Void String

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

instructions :: Parser [Instruction]
instructions = many (instruction <* newline) <* eof

instruction :: Parser Instruction
instruction =
  string "set" *> space *> (Set <$> mkMemory <*> (space *> mkMemory))
  <|> string "sub" *> space *> (Sub <$> mkMemory <*> (space *> mkMemory))
  <|> string "mul" *> space *> (Mul <$> mkMemory <*> (space *> mkMemory))
  <|> string "jnz" *> space *> (Jnz <$> mkMemory <*> (space *> mkMemory))

data Thread = Thread {iptr :: !Int, regs :: HashMap Char Int, mulCnt :: Int} deriving (Show)

succIptr   cpu@Thread{iptr   = ip}  = cpu{iptr   = succ ip}
succMulCnt cpu@Thread{mulCnt = cnt} = cpu{mulCnt = succ cnt}
getMem     (Constant i)             = const i
getMem     (Register ch)            = findWithDefault 0 ch . regs
setReg     (Register ch) r cpu      = cpu{regs = insert ch r (regs cpu)}

step :: Array Int Instruction -> Thread -> Thread
step is t@Thread{iptr = ip}
  | not $ inRange (bounds is) ip = t
  | otherwise                    = step' t $ is ! ip
  where step' :: Thread -> Instruction -> Thread
        step' t (Set r m2) = succIptr . setReg r (getMem m2 t) $ t
        step' t (Sub r m2) = succIptr . setReg r (getMem r t - getMem m2 t) $ t
        step' t (Mul r m2) = succIptr . succMulCnt . setReg r (getMem r t * getMem m2 t) $ t
        step' t@Thread{iptr = ip} (Jnz r m2) = t{iptr = ip + (if getMem r t /= 0 then getMem m2 t else 1)}

solve :: Either a [Instruction] -> Int
solve (Right instrs) = mulCnt . exec $ Thread {iptr = 0, regs = empty, mulCnt = 0}
  where is  = listArray (0, length instrs - 1) instrs
        bis = bounds is
        --
        exec :: Thread -> Thread
        exec t@Thread{iptr = ip0}
          | not . inRange bis $ ip0 = t -- program exited
          | otherwise = exec . step is $ t


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

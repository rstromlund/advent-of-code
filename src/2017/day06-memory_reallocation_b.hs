module Main where
import Data.List (elemIndex, replicate)

--	Out of curiosity, the debugger would also like to know the size of the loop: starting from a state
--	that has already been seen, how many block redistribution cycles must be performed before that same
--	state is seen again?
--
--	In the example above, 2 4 1 2 is seen again after four cycles, and so the answer in that example would be 4.
--
--	How many cycles are in the infinite loop that arises from the configuration in your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e '0 2 7 0' | cabal run 2017_day06-memory_reallocation # 5

type Banks = [Int]

parse :: String -> Banks
parse = map read . words

redist :: Banks -> [Banks]
redist is = is':redist is'
  where l = length is
        --
        rotate :: Int -> [a] -> [a]
        rotate n xs = take l (drop (mod n l) (cycle xs))
        --
        i   = maximum is
        Just p = elemIndex i is
        (q, r) = i `divMod` l
        qr  = zipWith (+) (replicate l q) $ replicate r 1 ++ replicate (l - r) 0
        is' = rotate (l - p - 1) . zipWith (+) qr $ tail (rotate p is) ++ [0]

solve :: Banks -> Int
solve = findRepeat 0 [] . redist
  where findRepeat :: Int -> [Banks] -> [Banks] -> Int
        findRepeat cnt seen (xs:xss)
          | xs `elem` seen = let Just e = elemIndex xs $ reverse seen
                             in cnt - e
          | otherwise      = findRepeat (cnt + 1) (xs:seen) xss


main :: IO ()
main = interact $ show . solve . parse

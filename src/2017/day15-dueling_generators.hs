{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Bits ((.&.))
import Data.Word (Word32)

--	... The generators both work on the same principle. To create its next value, a generator will take
--	the previous value it produced, multiply it by a factor (generator A uses 16807; generator B uses
--	48271), and then keep the remainder of dividing that resulting product by 2147483647. That final
--	remainder is the value it produces next.
--
--	To calculate each generator's first value, it instead uses a specific starting value as its "previous
--	value" (as listed in your puzzle input).
--
--	For example, suppose that for starting values, generator A uses 65, while generator B uses 8921. Then,
--	the first five pairs of generated values are:
--
--	--Gen. A--  --Gen. B--
--	   1092455   430625591
--	1181022009  1233683848
--	 245556042  1431495498
--	1744312007   137874439
--	1352636452   285222916
--
--	In binary, these pairs are (with generator A's value first in each pair):
--
--	00000000000100001010101101100111
--	00011001101010101101001100110111
--
--	01000110011001001111011100111001
--	01001001100010001000010110001000
--
--	00001110101000101110001101001010
--	01010101010100101110001101001010
--
--	01100111111110000001011011000111
--	00001000001101111100110000000111
--
--	01010000100111111001100000100100
--	00010001000000000010100000000100
--
--	Here, you can see that the lowest (here, rightmost) 16 bits of the third value match:
--	1110001101001010. Because of this one match, after processing these five pairs, the judge would have
--	added only 1 to its total.
--
--	To get a significant sample, the judge would like to consider 40 million pairs. (In the example above,
--	the judge would eventually find a total of 588 pairs that match in their lowest 16 bits.)
--
--	After 40 million pairs, what is the judge's final count?
--
-- Test:
--	% cd ../.. && echo -e 'Generator A starts with 65\nGenerator B starts with 8921' | cabal run 2017_day15-dueling_generators # 588

factorA = 16807
factorB = 48271
modulus = 2147483647
sigBits = 0b1111111111111111
judgePairs = 40000000

parse :: String -> [Int]
parse = map (read . last . words) . lines

generator :: Int -> Int -> [Int]
generator f !s = s':generator f s'
  where !s' = (f * s) `rem` modulus

solve :: [Int] -> Int
solve [!startA, !startB] = foldl' (\acc same -> if same then acc + 1 else acc) 0 . take judgePairs $ vs
  where vs = zipWith (\a b -> a .&. sigBits == b .&. sigBits) genA genB
        genA = generator factorA startA
        genB = generator factorB startB


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.HashMap.Strict (HashMap, elems, empty, findWithDefault, insert)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string, symbolChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The registers all start at 0. The instructions look like this:
--
--	b inc 5 if a > 1
--	a inc 1 if b < 5
--	c dec -10 if a >= 1
--	c inc -20 if c == 10
--
--	These instructions would be processed as follows:
--
--	* Because a starts at 0, it is not greater than 1, and so b is not modified.
--	* a is increased by 1 (to 1) because b is less than 5 (it is 0).
--	* c is decreased by -10 (to 10) because a is now greater than or equal to 1 (it is 1).
--	* c is increased by -20 (to -10) because c is equal to 10.
--
--	After this process, the largest value in any register is 1.
--
--	You might also encounter <= (less than or equal to) or != (not equal to). However, the CPU doesn't
--	have the bandwidth to tell you what all the registers are named, and leaves that to you to determine.
--
--	What is the largest value in any register after completing the instructions in your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e 'b inc 5 if a > 1\na inc 1 if b < 5\nc dec -10 if a >= 1\nc inc -20 if c == 10' | cabal run 2017_day08-i_heard_you_like_registers # 1

type Command = (String, String, Int, String, String, Int) -- (Reg, Cmd, Operand, Reg, Comparison, Operand) e.g. "b inc 5 if a > 1"

type Parser = Parsec Void String

parse :: Parser [Command]
parse = many (command <* newline) <* eof

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

word :: Parser String
word = many (letterChar <|> symbolChar <|> char '!')

command :: Parser Command
command = (,,,,,) <$> (word <* space) <*> (word <* space) <*> (signed <* string " if ") <*> (word <* space) <*> (word <* space) <*> signed

run :: HashMap String Int -> Command -> HashMap String Int
run !rs (tgt, cmd, cmdop, src, cmp, cmpop) = insert tgt tgtend rs
  where tgtbeg = findWithDefault 0 tgt rs
        tgtend
          | not tst = tgtbeg
          | "inc" == cmd = tgtbeg + cmdop
          | "dec" == cmd = tgtbeg - cmdop
        srcbeg = findWithDefault 0 src rs
        tst
          | "==" == cmp = srcbeg == cmpop
          | "!=" == cmp = srcbeg /= cmpop
          | ">"  == cmp = srcbeg >  cmpop
          | ">=" == cmp = srcbeg >= cmpop
          | "<"  == cmp = srcbeg <  cmpop
          | "<=" == cmp = srcbeg <= cmpop

solve :: Either a [Command] -> Int
solve (Right cs) = maximum . elems . foldl' run empty $ cs


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

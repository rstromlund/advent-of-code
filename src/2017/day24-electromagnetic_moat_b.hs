module Main where
import Data.List (sortOn)
import Data.Ord (Down(..))
import Data.Tuple (swap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... In the example above, there are two longest bridges:
--
--	0/2--2/2--2/3--3/4
--	0/2--2/2--2/3--3/5
--
--	Of them, the one which uses the 3/5 component is stronger; its strength is 0+2 + 2+2 + 2+3 + 3+5 = 19.
--
--	What is the strength of the longest bridge you can make? If you can make multiple bridges of the
--	longest length, pick the strongest one.
--
-- Test:
--	% cd ../.. && echo -e '0/2\n2/2\n2/3\n3/4\n3/5\n0/1\n10/1\n9/10' | cabal run 2017_day24-electromagnetic_moat_b # = 19

type Component = (Int, Int) -- (port0, port1)
type Bridge = [Component]

type Parser = Parsec Void String

components :: Parser [Component]
components = many (component <* newline) <* eof

component :: Parser Component
component = (,) <$> (decimal <* char '/') <*> decimal

findPort :: (Bridge, ([Component], [Component])) -> [Bridge]
findPort (bs, (cs, ds)) = findMatch (bs, (cs ++ ds, []))
  where findMatch :: (Bridge, ([Component], [Component])) -> [Bridge]
        findMatch (bs@((_, bp):_), (c@(p0, p1):cs, ds))
          | bp == p0 = findMatch (c:bs, (cs ++ ds, []))      ++ findMatch (bs, (cs, c:ds))
          | bp == p1 = findMatch (swap c:bs, (cs ++ ds, [])) ++ findMatch (bs, (cs, c:ds))
          | otherwise = findMatch (bs, (cs, c:ds))
        findMatch (bs, ([], ds)) = [bs]

solve :: Either a [Component] -> Int
solve (Right cs) = score . findPort $ ([start], (cs, []))
  where start = (0,0) :: Component
        score bs = maximum . map strength . takeWhile ((==) mx . length) $ mbs
          where mbs = sortOn (Down . length) bs
                mx  = length . head $ mbs
        strength = sum . map (uncurry (+))


main :: IO ()
main = interact $ show . solve . runParser components "<stdin>"

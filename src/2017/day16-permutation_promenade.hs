module Main where
import Data.ByteString.Internal (c2w, w2c)
import Data.ByteString as BS (ByteString, append, concat, elemIndex, pack, splitAt, unpack)
import Data.List.Split (splitOn)

--	You come upon a very unusual sight; a group of programs here appear to be dancing.
--
--	There are sixteen programs in total, named a through p. They start by standing in a line: a stands in position 0, b stands in position 1, and so on until p, which stands in position 15.
--
--	The programs' dance consists of a sequence of dance moves:
--
--	Spin, written sX, makes X programs move from the end to the front, but maintain their order otherwise. (For example, s3 on abcde produces cdeab).
--	Exchange, written xA/B, makes the programs at positions A and B swap places.
--	Partner, written pA/B, makes the programs named A and B swap places.
--	For example, with only five programs standing in a line (abcde), they could do the following dance:
--
--	s1, a spin of size 1: eabcd.
--	x3/4, swapping the last two programs: eabdc.
--	pe/b, swapping programs e and b: baedc.
--
--	After finishing their dance, the programs end up in order baedc.
--
--	You watch the dance for a while and record their dance moves (your puzzle input). In what order are the programs standing after their dance?
--
-- Test:
--	% cd ../.. && echo -e 's1,x3/4,pe/b' | cabal run 2017_day16-permutation_promenade # = baedc

parse :: String -> [String]
parse = splitOn "," . head . lines

dancers    = ['a'..'p']
numDancers = length dancers

solve :: [String] -> String
solve = map w2c . unpack . foldl' move ps
  where ps = pack . map c2w $ dancers
        --
        move :: ByteString -> String -> ByteString
        move acc ('s':cnt) = uncurry (flip BS.append) . BS.splitAt (numDancers - read cnt) $ acc
        move acc ('x':poss)         = let [str1, str2] = splitOn "/" poss
                                          p1 = read str1 :: Int
                                          p2 = read str2 :: Int
                                      in swapChar (min p1 p2) (max p1 p2) acc
        move acc ['p', c1, '/', c2] = let Just p1 = elemIndex (c2w c1) acc
                                          Just p2 = elemIndex (c2w c2) acc
                                      in swapChar (min p1 p2) (max p1 p2) acc
        --
        swapChar p1 p2 acc = let -- in 'a[b]c[d]e' and (1,3) aBeg='a', aMid='c', and aEnd='e'; c0='b' and c1='d'
                                 (aBeg, tail0) = BS.splitAt p1 acc
                                 (c0,   tail1) = BS.splitAt 1 tail0
                                 (aMid, tail2) = BS.splitAt (p2 - p1 - 1) tail1
                                 (c1,   aEnd)  = BS.splitAt 1 tail2
                             in BS.concat [aBeg, c1, aMid, c0, aEnd]


main :: IO ()
main = interact $ solve . parse

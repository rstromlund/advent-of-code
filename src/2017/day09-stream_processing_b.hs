module Main where
import Data.Either (isRight)
import Data.Void (Void)
import Text.Megaparsec (Parsec, anySingle, choice, eof, many, noneOf, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)

--	Now, you're ready to remove the garbage.
--
--	To prove you've removed it, you need to count all of the characters within the garbage. The leading
--	and trailing < and > don't count, nor do any canceled characters or the ! doing the canceling.
--
--	<>, 0 characters.
--	<random characters>, 17 characters.
--	<<<<>, 3 characters.
--	<{!>}>, 2 characters.
--	<!!>, 0 characters.
--	<!!!>>, 0 characters.
--	<{o"i!a,<{i<a>, 10 characters.
--
--	How many non-canceled characters are within the garbage in your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e '<>' | cabal run 2017_day09-stream_processing_b # 0
--	% cd ../.. && echo -e '<random characters>' | cabal run 2017_day09-stream_processing_b # 17
--	% cd ../.. && echo -e '<<<<>' | cabal run 2017_day09-stream_processing_b # 3
--	% cd ../.. && echo -e '<{!>}>' | cabal run 2017_day09-stream_processing_b # 2
--	% cd ../.. && echo -e '<!!>' | cabal run 2017_day09-stream_processing_b # 0
--	% cd ../.. && echo -e '<!!!>>' | cabal run 2017_day09-stream_processing_b # 0
--	% cd ../.. && echo -e '<{o"i!a,<{i<a>' | cabal run 2017_day09-stream_processing_b # 10

data Group =
            Garbage [Either Char Char]
          | Groups [Group]
  deriving (Show)

type Parser = Parsec Void String

parse :: Parser Group
parse = group <* newline <* eof

group :: Parser Group
group = choice
  [
    char '<' *> garbage <* char '>'
  , char '{' *> groups <* char '}'
  ]

groups :: Parser Group
groups = Groups <$> (group `sepBy` char ',')

garbage :: Parser Group
garbage = Garbage <$> many garbage'
  where garbage' :: Parser (Either Char Char)
        garbage' = choice
          [
            char '!' *> (Left <$> anySingle)
          , Right <$> noneOf ">"
          ]

solve :: Either a Group -> Int
solve (Right g) = score g
  where score (Garbage cs) = length . filter isRight $ cs
        score (Groups gs) = sum . map score $ gs


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

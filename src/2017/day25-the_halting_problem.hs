{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Char (ord)
import Data.IntMap.Strict ((!), IntMap, fromList)
import Data.IntSet (IntSet, empty, insert, delete, member, size)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, runParser, sepBy)
import Text.Megaparsec.Char (char, letterChar, space, string)
import Text.Megaparsec.Char.Lexer (decimal)


--	... You find the Turing machine blueprints (your puzzle input) on a tablet in a nearby pile of
--	debris. Looking back up at the broken Turing machine above, you can start to identify its parts:
--
--	* A tape which contains 0 repeated infinitely to the left and right.
--	* A cursor, which can move left or right along the tape and read or write values at its current position.
--	* A set of states, each containing rules about what to do based on the current value under the cursor.
--
--	Each slot on the tape has two possible values: 0 (the starting value for all slots) and 1. Based on
--	whether the cursor is pointing at a 0 or a 1, the current state says what value to write at the
--	current position of the cursor, whether to move the cursor left or right one slot, and which state to
--	use next.
--
--	For example, suppose you found the following blueprint:
--
--	Begin in state A.
--	Perform a diagnostic checksum after 6 steps.
--
--	In state A:
--	  If the current value is 0:
--	    - Write the value 1.
--	    - Move one slot to the right.
--	    - Continue with state B.
--	  If the current value is 1:
--	    - Write the value 0.
--	    - Move one slot to the left.
--	    - Continue with state B.
--
--	In state B:
--	  If the current value is 0:
--	    - Write the value 1.
--	    - Move one slot to the left.
--	    - Continue with state A.
--	  If the current value is 1:
--	    - Write the value 1.
--	    - Move one slot to the right.
--	    - Continue with state A.
--
-- Test:
--	% cd ../.. && echo -e 'Begin in state A.\nPerform a diagnostic checksum after 6 steps.\n\nIn state A:\n  If the current value is 0:\n    - Write the value 1.\n    - Move one slot to the right.\n    - Continue with state B.\n  If the current value is 1:\n    - Write the value 0.\n    - Move one slot to the left.\n    - Continue with state B.\n\nIn state B:\n  If the current value is 0:\n    - Write the value 1.\n    - Move one slot to the left.\n    - Continue with state A.\n  If the current value is 1:\n    - Write the value 1.\n    - Move one slot to the right.\n    - Continue with state A.' | cabal run 2017_day25-the_halting_problem # = 3


type Cond = (Int, Int, Int) -- (cur val, write val, move amt, next state)
type State = (Int, (Cond, Cond)) -- (cur st, (cond0, cond1))
type Blueprint = (Int, Int, [State]) -- (begin state, checksum steps, [states])

type Parser = Parsec Void String

blueprint :: Parser Blueprint
blueprint = mkBlueprint
            <$> (string "Begin in state " *> letterChar <* char '.' <* space)
            <*> (string "Perform a diagnostic checksum after " *> decimal <* string " steps." <* space)
            <*> (string stateSep *> state `sepBy` string stateSep)
            <* eof
  where mkBlueprint st ckstps ss = (ord st, ckstps, ss)
        stateSep = "In state "

cond :: Parser Cond
cond = mkCond
        <$> (decimal <* char ':' <* space)
        <*> (string "- Write the value " *> decimal <* char '.' <* space)
        <*> (string "- Move one slot to the " *> (string "right" <|> string "left") <* char '.' <* space)
        <*> (string "- Continue with state " *> letterChar <* char '.' <* space)
  where mkCond _ifval wval mvamt nxtst = (wval, moveAmt mvamt, ord nxtst)
        moveAmt "right" =  1
        moveAmt "left"  = -1

state :: Parser State
state = mkState
        <$> (letterChar <* char ':' <* space)
        <*> (string condSep *> cond `sepBy` string condSep)
  where mkState st [cond0, cond1] = (ord st, (cond0, cond1))
        condSep = "If the current value is "

step :: IntMap (Cond, Cond) -> (IntSet, (Int, Int)) -> (IntSet, (Int, Int))
step mss (tape, (cst, cpos)) = (update wval tape, (nxtst, cpos + mvamt))
  where cval = if cpos `member` tape then 1 else 0
        (wval, mvamt, nxtst) = (if 0 == cval then fst else snd) $ mss ! cst
        update 0 tape = delete cpos tape
        update 1 tape = insert cpos tape

solve :: Either a Blueprint -> Int
solve (Right bp@(st, ckstps, ss)) = exec ckstps (tape, (ord 'A', 0))
  where mss  = fromList ss :: IntMap (Cond, Cond)
        tape = empty :: IntSet
        --
        exec :: Int -> (IntSet, (Int, Int)) -> Int
        exec 0 (tape, (_, _)) = size tape
        exec !ckstps !acc = exec (ckstps - 1) . step mss $ acc


main :: IO ()
main = interact $ show . solve . runParser blueprint "<stdin>"

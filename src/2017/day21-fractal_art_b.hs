{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Bifunctor (first)
import Data.HashMap.Strict ((!), HashMap, fromList)
import Data.List (transpose)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, oneOf, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)

--	How many pixels stay on after 18 iterations?

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = [String]

rotate90 = map reverse . transpose
rot0   = id
rot90  = rot0   . rotate90
rot180 = rot90  . rotate90
rot270 = rot180 . rotate90

type Parser = Parsec Void String

rules :: Parser [(PuzzleMap, PuzzleMap)]
rules = concat <$> (many (rule <* newline) <* eof)

rule :: Parser [(PuzzleMap, PuzzleMap)]
rule = mkRotsAndFlips <$> ((many (oneOf "#.") `sepBy` char '/') `sepBy` string " => ")
  where flp = map reverse
        mkRotsAndFlips [inp, outp] = rots ++ map (first flp) rots
          where rots = [(rot0 inp, outp), (rot90 inp, outp), (rot180 inp, outp), (rot270 inp, outp)]

getsq :: Int -> Int -> Int -> PuzzleMap -> PuzzleMap
getsq stp miny minx = map (slice minx) . slice miny
  where slice n = take stp . drop n

joinsq :: [PuzzleMap] -> PuzzleMap
joinsq sqs@((_:_):_) = top:joinsq nxt
  where top = concatMap head sqs
        nxt = map tail sqs
joinsq _ = []

enhance :: HashMap PuzzleMap PuzzleMap -> PuzzleMap -> Int -> PuzzleMap
enhance !rs !pzl _ = concatMap joinsq srch
  where siz = length pzl
        stp | even siz  = 2
            | otherwise = 3
        --
        sqrss = map (\y -> map (\x -> getsq stp y x pzl) [0,stp..siz-1]) [0,stp..siz-1]
        --
        srch :: [[PuzzleMap]]
        srch = map (map (rs !)) sqrss

start = [".#.", "..#", "###"]

solve :: Either a [(PuzzleMap, PuzzleMap)] -> Int
solve (Right rs) = cntHashes . foldl' enhance' start $ [1..18]
  where rs' = fromList rs
        enhance' = enhance rs'
        cntHashes :: PuzzleMap -> Int
        cntHashes = length . filter ('#' ==) . concat


main :: IO ()
main = interact $ show . solve . runParser rules "<stdin>"

module Main where
import Data.Tuple.Extra (thd3)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Each tick, all particles are updated simultaneously. A particle's properties are updated in the following order:
--
--	* Increase the X velocity by the X acceleration.
--	* Increase the Y velocity by the Y acceleration.
--	* Increase the Z velocity by the Z acceleration.
--	* Increase the X position by the X velocity.
--	* Increase the Y position by the Y velocity.
--	* Increase the Z position by the Z velocity.
--
--	Because of seemingly tenuous rationale involving z-buffering, the GPU would like to know which
--	particle will stay closest to position <0,0,0> in the long term. Measure this using the Manhattan
--	distance, which in this situation is simply the sum of the absolute values of a particle's X, Y, and Z
--	position.
--
--	For example, suppose you are only given two particles, both of which stay entirely on the X-axis (for
--	simplicity). Drawing the current states of particles 0 and 1 (in that order) with an adjacent a number
--	line and diagram of current X positions (marked in parentheses), the following would take place:
--
--	p=< 3,0,0>, v=< 2,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
--	p=< 4,0,0>, v=< 0,0,0>, a=<-2,0,0>                         (0)(1)
--
--	p=< 4,0,0>, v=< 1,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
--	p=< 2,0,0>, v=<-2,0,0>, a=<-2,0,0>                      (1)   (0)
--
--	p=< 4,0,0>, v=< 0,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
--	p=<-2,0,0>, v=<-4,0,0>, a=<-2,0,0>          (1)               (0)
--
--	p=< 3,0,0>, v=<-1,0,0>, a=<-1,0,0>    -4 -3 -2 -1  0  1  2  3  4
--	p=<-8,0,0>, v=<-6,0,0>, a=<-2,0,0>                         (0)
--	At this point, particle 1 will never be closer to <0,0,0> than particle 0, and so, in the long run, particle 0 will stay closest.
--
--	Which particle will stay closest to position <0,0,0> in the long term?
--
-- Test:
--	% cd ../.. && echo -e 'p=<3,0,0>, v=<2,0,0>, a=<-1,0,0>\np=<4,0,0>, v=<0,0,0>, a=<-2,0,0>' | cabal run 2017_day20-particle_swarm # = 0

type Vector = (Int, Int, Int) -- X, Y, Z
type Particle = ((Vector, Vector, Vector), Int) -- ((P, V, A), P#)

type Parser = Parsec Void String

particles :: Parser [Particle]
particles = zip <$> (many (particle <* newline) <* eof) <*> pure [0..]

particle :: Parser (Vector, Vector, Vector)
particle = (,,) <$>
  (string "p=" *> vector) <*>
  (string ", v=" *> vector) <*>
  (string ", a=" *> vector)

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

vector :: Parser Vector
vector = (,,) <$> (char '<' *> signed <* char ',') <*> (signed <* char ',') <*> (signed <* char '>')

manhattanDist :: Vector -> Vector -> Int
manhattanDist (x1, y1, z1) (x2, y2, z2) = abs (x2 - x1) + abs (y2 - y1) + abs (z2 - z1)

home = (0, 0, 0)

solve :: Either a [Particle] -> Int
solve (Right ps) = snd . minimum . map (\((_,_,a), n) -> (manhattanDist home a, n)) $ ps


main :: IO ()
main = interact $ show . solve . runParser particles "<stdin>"

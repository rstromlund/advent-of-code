module Main where

--	... Each square on the grid is allocated in a spiral pattern starting at a location marked 1 and then
--	counting up while spiraling outward. For example, the first few squares are allocated like this:
--
--	17  16  15  14  13
--	18   5   4   3  12
--	19   6   1   2  11
--	20   7   8   9  10
--	21  22  23---> ...
--
--	While this is very space-efficient (no squares are skipped), requested data must be carried back to
--	square 1 (the location of the only access port for this memory system) by programs that can only move
--	up, down, left, or right. They always take the shortest path: the Manhattan Distance between the
--	location of the data and square 1.
--
--	For example:
--
--	* Data from square 1 is carried 0 steps, since it's at the access port.
--	* Data from square 12 is carried 3 steps, such as: down, left, left.
--	* Data from square 23 is carried only 2 steps: up twice.
--	* Data from square 1024 must be carried 31 steps.
--
--	How many steps are required to carry the data from the square identified in your puzzle input all the way to the access port?
--
-- Test:
--	% cd ../.. && echo -e '1' | cabal run 2017_day03-spiral_memory # 0
--	% cd ../.. && echo -e '12' | cabal run 2017_day03-spiral_memory # 3
--	% cd ../.. && echo -e '23' | cabal run 2017_day03-spiral_memory # 2
--	% cd ../.. && echo -e '1024' | cabal run 2017_day03-spiral_memory # 31

parse :: String -> Int
parse = read

type Point = (Int, Int) -- x,y

manhattanDist :: (Point, Point) -> Int
manhattanDist ((y1, x1), (y2, x2)) = abs (y2 - y1) + abs (x2 - x1)

calcPt :: Int -> Point -> Point
calcPt l_2 (0, rmdr) = (  l_2,              rmdr - l_2 + 1)
calcPt l_2 (1, rmdr) = (  l_2 - rmdr - 1,   l_2)
calcPt l_2 (2, rmdr) = (- l_2,              l_2 - rmdr - 1)
calcPt l_2 (3, rmdr) = (  rmdr - l_2 + 1, - l_2)

solve :: Int -> Int
solve 1 = 0
solve n = manhattanDist ((0, 0), pt)
  where lvl :: Int -- Each level is a multiple of 2 + 1 (1, 3, 5, 7, ...)
        lvl = (ceiling(sqrt(fromIntegral n)) `div` 2) * 2 + 1
        beg = (lvl - 2) ^ 2 + 1
        pt = calcPt (lvl `div` 2) $ (n - beg) `divMod` (lvl - 1)


main :: IO ()
main = interact $ show . solve . parse

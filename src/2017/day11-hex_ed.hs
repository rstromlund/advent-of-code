module Main where
import Data.List.Split (splitOn)


--	... The hexagons ("hexes") in this grid are aligned such that adjacent hexes can be found to the
--	north, northeast, southeast, south, southwest, and northwest:
--
--	  \ n  /
--	nw +--+ ne
--	  /    \
--	-+      +-
--	  \    /
--	sw +--+ se
--	  / s  \
--
--	You have the path the child process took. Starting where he started, you need to determine the fewest
--	number of steps required to reach him. (A "step" means to move from the hex you are in to any adjacent
--	hex.)
--
--	For example:
--
--	* ne,ne,ne is 3 steps away.
--	* ne,ne,sw,sw is 0 steps away (back where you started).
--	* ne,ne,s,s is 2 steps away (se,se).
--	* se,sw,se,sw,sw is 3 steps away (s,s,sw).
--
-- Test:
--	% cd ../.. && echo -e 'ne,ne,ne' | cabal run 2017_day11-hex_ed # 3
--	% cd ../.. && echo -e 'ne,ne,sw,sw' | cabal run 2017_day11-hex_ed # 0
--	% cd ../.. && echo -e 'ne,ne,s,s' | cabal run 2017_day11-hex_ed # 2
--	% cd ../.. && echo -e 'se,sw,se,sw,sw' | cabal run 2017_day11-hex_ed # 3


parse :: String -> [String]
parse = splitOn "," . concat . lines

solve :: [String] -> Int
solve = score . move (0, 0)
  where move :: (Int, Int) -> [String] -> (Int, Int)
        move c [] = c
        move (x, y) ("nw":ds) = move (x - 1, y + 1) ds
        move (x, y) ("n" :ds) = move (x + 0, y + 2) ds
        move (x, y) ("ne":ds) = move (x + 1, y + 1) ds
        move (x, y) ("sw":ds) = move (x - 1, y - 1) ds
        move (x, y) ("s" :ds) = move (x + 0, y - 2) ds
        move (x, y) ("se":ds) = move (x + 1, y - 1) ds
        --
        score :: (Int, Int) -> Int
        score (x, y)
          | abs y >= abs x = abs x + (abs y - abs x) `div` 2
          | otherwise      = abs x


main :: IO ()
main = interact $ show . solve . parse

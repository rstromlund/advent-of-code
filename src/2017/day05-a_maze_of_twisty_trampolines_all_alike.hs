module Main where

--	.. The message includes a list of the offsets for each jump. Jumps are relative: -1 moves to the
--	previous instruction, and 2 skips the next one. Start at the first instruction in the list. The goal
--	is to follow the jumps until one leads outside the list.
--
--	In addition, these instructions are a little strange; after each jump, the offset of that instruction
--	increases by 1. So, if you come across an offset of 3, you would move three instructions forward, but
--	change it to a 4 for the next time it is encountered.
--
--	For example, consider the following list of jump offsets:
--
--	0
--	3
--	0
--	1
--	-3
--
--	Positive jumps ("forward") move downward; negative jumps move upward. For legibility in this example,
--	these offset values will be written all on one line, with the current instruction marked in
--	parentheses. The following steps would be taken before an exit is found:
--
--	* (0) 3  0  1  -3  - before we have taken any steps.
--	* (1) 3  0  1  -3  - jump with offset 0 (that is, don't jump at all). Fortunately, the instruction is then incremented to 1.
--	*  2 (3) 0  1  -3  - step forward because of the instruction we just modified. The first instruction is incremented again, now to 2.
--	*  2  4  0  1 (-3) - jump all the way to the end; leave a 4 behind.
--	*  2 (4) 0  1  -2  - go back to where we just were; increment -3 to -2.
--	*  2  5  0  1  -2  - jump 4 steps forward, escaping the maze.
--
--	In this example, the exit is reached in 5 steps.
--
--	How many steps does it take to reach the exit?
--
-- Test:
--	% cd ../.. && echo -e '0\n3\n0\n1\n-3' | cabal run 2017_day05-a_maze_of_twisty_trampolines_all_alike # 5

type Maze = [Int]

parse :: String -> Maze
parse = map read . words

-- Replace list `ls` slot `n` with new value `s`
updateList n s ls = let (l,_:ls') = splitAt n ls in l ++ s:ls'

solve :: Maze -> Int
solve is = walk 0 0 is
  where l = length is
        walk c n is
          | n >= l    = c
          | otherwise = let i = is !! n
                            n' = n + i
                            i' = i + 1
                        in walk (c + 1) n' $ updateList n i' is


main :: IO ()
main = interact $ show . solve . parse

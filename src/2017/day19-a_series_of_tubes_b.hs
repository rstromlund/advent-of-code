module Main where
import Data.Array (Array, assocs, bounds, inRange, listArray, (!))
import Data.Bifunctor (bimap)
import Data.Char (isAlpha, isSpace)

--	The packet is curious how many steps it needs to go.
--
--	For example, using the same routing diagram from the example above...
--
--	     |
--	     |  +--+
--	     A  |  C
--	 F---|--|-E---+
--	     |  |  |  D
--	     +B-+  +--+
--
--	...the packet would go:
--
--	6 steps down (including the first line at the top of the diagram).
--	3 steps right.
--	4 steps up.
--	3 steps right.
--	4 steps down.
--	3 steps right.
--	2 steps up.
--	13 steps left (including the F it stops on).
--	This would result in a total of 38 steps.
--
--	How many steps does the packet need to go?
--
-- Test:
--	% cd ../.. && echo -e '     |          \n     |  +--+    \n     A  |  C    \n F---|----E|--+ \n     |  |  |  D \n     +B-+  +--+ ' | cabal run 2017_day19-a_series_of_tubes_b # = 38

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = Array Point Char

parse :: String -> PuzzleMap
parse cs = listArray ((0, 0), (ly, lx)) cs
  where ls = lines cs
        ly = pred . length $ ls
        lx = length . head $ ls

cardinalDirections :: [Point]
cardinalDirections = [
  ( 1,  0), -- 1:down
  (-1,  0), -- 0:up
  ( 0, -1), -- 2:left
  ( 0,  1)  -- 3:right
  ]

getNeighbors :: PuzzleMap -> Point -> Point -> [Point]
getNeighbors net oldpt (y, x) = filter (\pt -> pt /= oldpt && inRange (bounds net) pt && (not . isSpace) (net ! pt)) . map (bimap (+ y) (+ x)) $ cardinalDirections

solve :: PuzzleMap -> Int
solve net = walk 0 (head cardinalDirections) start start '|'
  where start@(sy, sx) = fst . head . filter (('|' ==) . snd) . assocs $ net
        walk :: Int -> Point -> Point -> Point -> Char -> Int
        walk cnt dir@(dy, dx) oldpt curpt ch
          | '+' == ch  = -- look around for the next step (but don't go backwards)
                         let [curpt'@(y', x')] = getNeighbors net oldpt curpt
                             ch' = net ! curpt'
                             _curpt@(y, x) = curpt
                         in walk (cnt + 1) (y' - y, x' - x) curpt curpt' ch'
          | isSpace ch = cnt -- no track left, end the maze walking
          | otherwise  = walk (cnt + 1) dir curpt curpt' ch' -- nothing to see here, move along
          where curpt' = bimap (+ dy) (+ dx) curpt
                ch' = net ! curpt'


main :: IO ()
main = interact $ show . solve . parse

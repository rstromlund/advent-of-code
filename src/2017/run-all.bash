#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
time stack exec hlint *.hs
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-inverse_captcha.hs   < 01a.txt ; (( rc += ${?} ))
#time ./day01-inverse_captcha_b.hs < 01a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-corruption_checksum.hs   < 02a.txt ; (( rc += ${?} ))
#time ./day02-corruption_checksum_b.hs < 02a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-spiral_memory.hs   < 03a.txt ; (( rc += ${?} ))
#time ./day03-spiral_memory_b.hs < 03a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-high-entropy_passphrases.hs   < 04a.txt ; (( rc += ${?} ))
#time ./day04-high-entropy_passphrases_b.hs < 04a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-a_maze_of_twisty_trampolines_all_alike.hs   < 05a.txt ; (( rc += ${?} ))
#time ./day05-a_maze_of_twisty_trampolines_all_alike_b.hs < 05a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-memory_reallocation.hs   < 06a.txt ; (( rc += ${?} ))
#time ./day06-memory_reallocation_b.hs < 06a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-recursive_circus.hs   < 07a.txt ; (( rc += ${?} ))
#time ./day07-recursive_circus_b.hs < 07a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-i_heard_you_like_registers.hs   < 08a.txt ; (( rc += ${?} ))
#time ./day08-i_heard_you_like_registers_b.hs < 08a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-stream_processing.hs   < 09a.txt ; (( rc += ${?} ))
#time ./day09-stream_processing_b.hs < 09a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-knot_hash.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-knot_hash_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-hex_ed.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-hex_ed_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-digital_plumber.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-digital_plumber_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-packet_scanners.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-packet_scanners_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-disk_defragmentation.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-disk_defragmentation_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-dueling_generators.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-dueling_generators_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-permutation_promenade.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-permutation_promenade_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-spinlock.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-spinlock_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time ./day18-duet.hs   < 18a.txt ; (( rc += ${?} ))
#time ./day18-duet_b.hs < 18a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-a_series_of_tubes.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-a_series_of_tubes_b.hs < 19a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day20"
#time ./day20-particle_swarm.hs   < 20a.txt ; (( rc += ${?} ))
#time ./day20-particle_swarm_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-fractal_art.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-fractal_art_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-sporifica_virus.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-sporifica_virus_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time ./day23-coprocessor_conflagration.hs   < 23a.txt ; (( rc += ${?} ))
#time ./day23-coprocessor_conflagration_b.hs < 23a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-electromagnetic_moat.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-electromagnetic_moat_b.hs < 24a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-the_halting_problem.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

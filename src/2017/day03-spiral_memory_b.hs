module Main where
import Data.Bifunctor (bimap)
import Data.HashMap.Strict ((!?), HashMap, insert, singleton)
import Data.Maybe (mapMaybe)

--	... As a stress test on the system, the programs here clear the grid and then store the value 1 in
--	square 1. Then, in the same allocation order as shown above, they store the sum of the values in all
--	adjacent squares, including diagonals.
--
--	So, the first few squares' values are chosen as follows:
--
--	* Square 1 starts with the value 1.
--	* Square 2 has only one adjacent filled square (with value 1), so it also stores 1.
--	* Square 3 has both of the above squares as neighbors and stores the sum of their values, 2.
--	* Square 4 has all three of the aforementioned squares as neighbors and stores the sum of their values, 4.
--	* Square 5 only has the first and fourth squares as neighbors, so it gets the value 5.
--
--	Once a square is written, its value does not change. Therefore, the first few squares would receive the following values:
--
--	147  142  133  122   59
--	304    5    4    2   57
--	330   10    1    1   54
--	351   11   23   25   26
--	362  747  806--->   ...
--
--	What is the first value written that is larger than your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e '122' | cabal run 2017_day03-spiral_memory_b # = 133

type Point = (Int, Int) -- x,y
type Memory = HashMap Point Int

parse :: String -> Int
parse = read

cardinalDirections = [
  ( 0, -1), -- north
  ( 0,  1), -- south
  ( 1,  0), -- east
  (-1,  0), -- west

  (-1, -1), -- north-west
  ( 1, -1), -- north-east
  (-1,  1), -- south-west
  ( 1,  1)] :: [Point]  -- south-east

calcPt :: Int -> Point
calcPt 1 = (0, 0)
calcPt n = calcPt' (lvl `div` 2) $ (n - beg) `divMod` (lvl - 1)
  where lvl :: Int -- Each level is a multiple of 2 + 1 (1, 3, 5, 7, ...)
        lvl = (ceiling(sqrt(fromIntegral n)) `div` 2) * 2 + 1
        beg = (lvl - 2) ^ 2 + 1
        --
        calcPt' l_2 (0, rmdr) = (  l_2,              rmdr - l_2 + 1)
        calcPt' l_2 (1, rmdr) = (  l_2 - rmdr - 1,   l_2)
        calcPt' l_2 (2, rmdr) = (- l_2,              l_2 - rmdr - 1)
        calcPt' l_2 (3, rmdr) = (  rmdr - l_2 + 1, - l_2)

solve :: Int -> Int
solve n = head . dropWhile (<= n) . memSum 2 . singleton (0, 0) $ 1
  where memSum :: Int -> Memory -> [Int]
        memSum cnt mem = let pt@(x, y) = calcPt cnt
                             val = sum . mapMaybe ((mem !?) . bimap (+ x) (+ y)) $ cardinalDirections
                         in val:memSum (cnt + 1) (insert pt val mem)


main :: IO ()
main = interact $ show . solve . parse

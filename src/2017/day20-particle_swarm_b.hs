module Main where
import Data.List (nub, sort)
import Data.List.Extra (groupOn)
import Data.Ratio ((%), denominator)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	To simplify the problem further, the GPU would like to remove any particles that collide. Particles
--	collide if their positions ever exactly match. Because particles are updated simultaneously, more than
--	two particles can collide at the same time and place. Once particles collide, they are removed and
--	cannot collide with anything else after that tick.
--
--	For example:
--
--	p=<-6,0,0>, v=< 3,0,0>, a=< 0,0,0>
--	p=<-4,0,0>, v=< 2,0,0>, a=< 0,0,0>    -6 -5 -4 -3 -2 -1  0  1  2  3
--	p=<-2,0,0>, v=< 1,0,0>, a=< 0,0,0>    (0)   (1)   (2)            (3)
--	p=< 3,0,0>, v=<-1,0,0>, a=< 0,0,0>
--
--	p=<-3,0,0>, v=< 3,0,0>, a=< 0,0,0>
--	p=<-2,0,0>, v=< 2,0,0>, a=< 0,0,0>    -6 -5 -4 -3 -2 -1  0  1  2  3
--	p=<-1,0,0>, v=< 1,0,0>, a=< 0,0,0>             (0)(1)(2)      (3)
--	p=< 2,0,0>, v=<-1,0,0>, a=< 0,0,0>
--
--	p=< 0,0,0>, v=< 3,0,0>, a=< 0,0,0>
--	p=< 0,0,0>, v=< 2,0,0>, a=< 0,0,0>    -6 -5 -4 -3 -2 -1  0  1  2  3
--	p=< 0,0,0>, v=< 1,0,0>, a=< 0,0,0>                       X (3)
--	p=< 1,0,0>, v=<-1,0,0>, a=< 0,0,0>
--
--	------destroyed by collision------
--	------destroyed by collision------    -6 -5 -4 -3 -2 -1  0  1  2  3
--	------destroyed by collision------                      (3)
--	p=< 0,0,0>, v=<-1,0,0>, a=< 0,0,0>
--
--	In this example, particles 0, 1, and 2 are simultaneously destroyed at the time and place marked X. On the next tick, particle 3 passes through unharmed.
--
--	How many particles are left after all collisions are resolved?
--
-- Test:
--	% cd ../.. && echo -e 'p=<-6,0,0>, v=<3,0,0>, a=<0,0,0>\np=<-4,0,0>, v=<2,0,0>, a=<0,0,0>\np=<-2,0,0>, v=<1,0,0>, a=<0,0,0>\np=<3,0,0>, v=<-1,0,0>, a=<0,0,0>\np=<1,2,3>, v=<4,5,6>, a=<7,8,9>' | cabal run 2017_day20-particle_swarm_b # = 1

data Vector a = Vector   {x, y, z :: !a} deriving (Eq, Ord, Show)
data Particle = Particle {p, v, a :: !(Vector Int), nbr :: !Int} deriving (Eq, Ord, Show)

type Parser = Parsec Void String

particles :: Parser [Particle]
particles = zipWith (\pp n -> pp{nbr = n}) <$> (many (particle <* newline) <* eof) <*> pure [0..]

particle :: Parser Particle
particle = Particle <$>
  (string   "p=" *> vector) <*>
  (string ", v=" *> vector) <*>
  (string ", a=" *> vector) <*>
  pure 0

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

vector :: Parser (Vector Int)
vector = Vector <$> (char '<' *> signed <* char ',') <*> (signed <* char ',') <*> (signed <* char '>')

{-
https://www.reddit.com/r/adventofcode/comments/7kz6ik/2017_day_20_solutions/

Formula for position:
    pos = 1/2 a t^2 + (1/2 a + v) t + x

Formula for position (zero acceleration):
    pos = v t + x; t = -x / v

Quadratic formula (https://en.wikipedia.org/wiki/Quadratic_formula)
    x = (-b +/- sqrt(b^2 - 4ac)) / 2a
-}

quadForm :: Double -> Double -> Double -> [Double]
quadForm a b c
  | d >= 0.0  = [(-b + v) / (2.0 * a), (-b - v) / (2.0 * a)]
  | otherwise = []
  where d = b * b - 4 * a * c -- discriminant of the quadratic equation
        v = sqrt d -- value of the square root of the discriminant

intersect :: Particle -> Particle -> [(Double, [Particle])]
intersect p0 p1
  | nbr p0 == nbr p1 = [] -- same particle
  | p p0   == p p1   = [(0.0, [p0, p1])] -- already colliding
  | otherwise = map (, [p0, p1]) . intersect' (x (p p0) - x (p p1)) (x (v p0) - x (v p1)) $ (x (a p0) - x (a p1))
  where intersect' p v 0
          | v > 0     = [(- fromIntegral p) / fromIntegral v]
          | otherwise = []
        intersect' p v a = let a2 = fromIntegral a / 2.0
                           in quadForm a2 (a2 + fromIntegral v) (fromIntegral p)

posAtTm :: Int -> Particle -> Vector Rational
posAtTm t Particle{p = p', v = v', a = a'} = Vector (pos x) (pos y) (pos z)
  where tt = fromIntegral t
        pos :: (Vector Int -> Int) -> Rational
        pos cord = (1 % 2) * aa * tt * tt + ((1 % 2) * aa + vv) * tt + pp
          where pp = fromIntegral $ cord p'
                vv = fromIntegral $ cord v'
                aa = fromIntegral $ cord a'

discreet :: Vector Rational -> Bool
discreet Vector{x = xx, y = yy, z = zz} = 1 == denominator xx && 1 == denominator yy && 1 == denominator zz

wholeNumber :: Double -> Bool
wholeNumber = (0 ==) . round . (* 1000.0) . snd . properFraction

solve :: Either a [Particle] -> Int
solve (Right ps) = length . collide $ (ps, is)
  where -- ixs = list of times and particles that will collide at least in the x coordinate
        ixs :: [(Double, [Particle])]
        ixs = sort . concatMap (\p0 -> concatMap (filter (\(t, _) -> wholeNumber t && t >= 0.0) . intersect p0) ps) $ ps
        --
        is :: [[(Double, [Particle])]] -- e.g. [[(2.0,((((-6,0,0),(3,0,0),(0,0,0)),0),(((-4,0,0),(2,0,0),(0,0,0)),1))),...]]
        is = groupOn fst . filter (\(t, ps) ->
                                     let ps' = map (posAtTm $ truncate t) ps
                                     in all discreet ps' && head ps' == last ps'
                                  ) $ ixs
        --
        collide :: ([Particle], [[(Double, [Particle])]]) -> [Particle]
        collide (ps, [])    = ps
        collide (ps, []:is) = collide (ps, is)
        collide (ps, i:is) =
          let -- ns = list of particles colliding at this time; we eliminate these particles from the list
              -- and all future collisions since they are already disintegrated.
              ns = nub . sort . concatMap (map nbr . snd) $ i
          in collide (
                 -- remove all colliding particles from list
                 filter (\p' -> nbr p' `notElem` ns) ps,
                 -- remove particles from future collisions
                 map (filter (\(_, [p0,p1]) -> nbr p0 `notElem` ns && nbr p1 `notElem` ns)) is
             )


main :: IO ()
main = interact $ show . solve . runParser particles "<stdin>"

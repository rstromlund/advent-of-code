module Main where

--	... starts with a circular buffer containing only the value 0, which it marks as the current
--	position. It then steps forward through the circular buffer some number of steps (your puzzle input)
--	before inserting the first new value, 1, after the value it stopped on. The inserted value becomes the
--	current position. Then, it steps forward from there the same number of steps, and wherever it stops,
--	inserts after it the second new value, 2, and uses that as the new current position again.
--
--	It repeats this process of stepping forward, inserting a new value, and using the location of the
--	inserted value as the new current position a total of 2017 times, inserting 2017 as its final
--	operation, and ending with a total of 2018 values (including 0) in the circular buffer.
--
--	For example, if the spinlock were to step 3 times per insert, the circular buffer would begin to
--	evolve like this (using parentheses to mark the current position after each iteration of the
--	algorithm):
--
--	(0), the initial state before any insertions.
--	0 (1): the spinlock steps forward three times (0, 0, 0), and then inserts the first value, 1, after it. 1 becomes the current position.
--	0 (2) 1: the spinlock steps forward three times (0, 1, 0), and then inserts the second value, 2, after it. 2 becomes the current position.
--	0  2 (3) 1: the spinlock steps forward three times (1, 0, 2), and then inserts the third value, 3, after it. 3 becomes the current position.
--
--	And so on:
--
--	0  2 (4) 3  1
--	0 (5) 2  4  3  1
--	0  5  2  4  3 (6) 1
--	0  5 (7) 2  4  3  6  1
--	0  5  7  2  4  3 (8) 6  1
--	0 (9) 5  7  2  4  3  8  6  1
--
--	Eventually, after 2017 insertions, the section of the circular buffer near the last insertion looks like this:
--
--	1512  1134  151 (2017) 638  1513  851
--
--	Perhaps, if you can identify the value that will ultimately be after the last value written (2017),
--	you can short-circuit the spinlock. In this example, that would be 638.
--
--	What is the value after 2017 in your completed circular buffer?
--
-- Test:
--	% cd ../.. && echo -e '3' | cabal run 2017_day17-spinlock # = 638

parse :: String -> Int
parse = read . head . lines

insertions = 2017+1

solve :: Int -> Int
solve stepCnt = step 1 0 [0]
  where step len cpos buf
          | len == insertions = buf !! (cpos + 1) `mod` len
          | otherwise         = let p      = (cpos + stepCnt) `mod` len + 1
                                    (b, a) = splitAt p buf
                                in step (len + 1) p $ b ++ (len:a)


main :: IO ()
main = interact $ show . solve . parse

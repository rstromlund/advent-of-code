module Main where
import Data.Void (Void)
import Data.Word (Word8)
import Text.Megaparsec (Parsec, eof, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... begin with a list of numbers from 0 to 255, a current position which begins at 0 (the first
--	element in the list), a skip size (which starts at 0), and a sequence of lengths (your puzzle
--	input). Then, for each length:
--
--	* Reverse the order of that length of elements in the list, starting with the element at the current position.
--	* Move the current position forward by that length plus the skip size.
--	* Increase the skip size by one.
--
--	The list is circular; if the current position and the length try to reverse elements beyond the end of
--	the list, the operation reverses using as many extra elements as it needs from the front of the
--	list. If the current position moves past the end of the list, it wraps around to the front. Lengths
--	larger than the size of the list are invalid.
--
--	Here's an example using a smaller list:
--
--	Suppose we instead only had a circular list containing five elements, 0, 1, 2, 3, 4, and were given input lengths of 3, 4, 1, 5.
--
--	* The list begins as [0] 1 2 3 4 (where square brackets indicate the current position).
--	* The first length, 3, selects ([0] 1 2) 3 4 (where parentheses indicate the sublist to be reversed).
--	* After reversing that section (0 1 2 into 2 1 0), we get ([2] 1 0) 3 4.
--	* Then, the current position moves forward by the length, 3, plus the skip size, 0: 2 1 0 [3] 4. Finally, the skip size increases to 1.
--	* The second length, 4, selects a section which wraps: 2 1) 0 ([3] 4.
--	* The sublist 3 4 2 1 is reversed to form 1 2 4 3: 4 3) 0 ([1] 2.
--	* The current position moves forward by the length plus the skip size, a total of 5, causing it not to move because it wraps around: 4 3 0 [1] 2. The skip size increases to 2.
--	* The third length, 1, selects a sublist of a single element, and so reversing it has no effect.
--	* The current position moves forward by the length (1) plus the skip size (2): 4 [3] 0 1 2. The skip size increases to 3.
--	* The fourth length, 5, selects every element starting with the second: 4) ([3] 0 1 2. Reversing this sublist (3 0 1 2 4 into 4 2 1 0 3) produces: 3) ([4] 2 1 0.
--	* Finally, the current position moves forward by 8: 3 4 2 1 [0]. The skip size increases to 4.
--
--	In this example, the first two numbers in the list end up being 3 and 4; to check the process, you can
--	multiply them together to produce 12.
--
--	However, you should instead use the standard list size of 256 (with values 0 to 255) and the sequence
--	of lengths in your puzzle input. Once this process is complete, what is the result of multiplying the
--	first two numbers in the list?
--
-- Test:
--	% cd ../.. && echo -e '3,4,1,5' | cabal run 2017_day10-knot_hash # 12

type Length = Int

type Parser = Parsec Void String

parse :: Parser [Length]
parse = (decimal `sepBy` char ',') <* newline <* eof

-- listSize = 5 -- test size
listSize = 256

knot :: Int -> Int -> [Length] -> [Word8] -> [Word8]
knot pos _    []     is = take listSize . drop (listSize - (pos `mod` listSize)) . cycle $ is
knot pos skip (l:ls) is = knot (pos + l + skip) (skip + 1) ls . take listSize . drop (l + skip) . cycle $ is'
  where is' = let (bf, af) = splitAt l is
              in reverse bf ++ af

solve :: Either a [Length] -> Int
solve (Right ls) = product . map fromIntegral . take 2 . knot 0 0 ls $ [0..fromIntegral (listSize - 1)]


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

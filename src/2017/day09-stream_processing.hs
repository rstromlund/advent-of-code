module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, anySingle, choice, eof, many, noneOf, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)

--	... The characters represent groups - sequences that begin with { and end with }. Within a group,
--	there are zero or more other things, separated by commas: either another group or garbage. Since
--	groups can contain other groups, a } only closes the most-recently-opened unclosed group - that is,
--	they are nestable. Your puzzle input represents a single, large group which itself contains many
--	smaller ones.
--
--	Sometimes, instead of a group, you will find garbage. Garbage begins with < and ends with >. Between
--	those angle brackets, almost any character can appear, including { and }. Within garbage, < has no
--	special meaning.
--
--	In a futile attempt to clean up the garbage, some program has canceled some of the characters within
--	it using !: inside garbage, any character that comes after ! should be ignored, including <, >, and
--	even another !.
--
--	You don't see any characters that deviate from these rules. Outside garbage, you only find well-formed
--	groups, and garbage always terminates according to the rules above.
--
--	Here are some self-contained pieces of garbage:
--
--	* <>, empty garbage.
--	* <random characters>, garbage containing random characters.
--	* <<<<>, because the extra < are ignored.
--	* <{!>}>, because the first > is canceled.
--	* <!!>, because the second ! is canceled, allowing the > to terminate the garbage.
--	* <!!!>>, because the second ! and the first > are canceled.
--	* <{o"i!a,<{i<a>, which ends at the first >.
--
--	Here are some examples of whole streams and the number of groups they contain:
--
--	* {}, 1 group.
--	* {{{}}}, 3 groups.
--	* {{},{}}, also 3 groups.
--	* {{{},{},{{}}}}, 6 groups.
--	* {<{},{},{{}}>}, 1 group (which itself contains garbage).
--	* {<a>,<a>,<a>,<a>}, 1 group.
--	* {{<a>},{<a>},{<a>},{<a>}}, 5 groups.
--	* {{<!>},{<!>},{<!>},{<a>}}, 2 groups (since all but the last > are canceled).
--
--	Your goal is to find the total score for all groups in your input. Each group is assigned a score
--	which is one more than the score of the group that immediately contains it. (The outermost group gets
--	a score of 1.)
--
--	* {}, score of 1.
--	* {{{}}}, score of 1 + 2 + 3 = 6.
--	* {{},{}}, score of 1 + 2 + 2 = 5.
--	* {{{},{},{{}}}}, score of 1 + 2 + 3 + 3 + 3 + 4 = 16.
--	* {<a>,<a>,<a>,<a>}, score of 1.
--	* {{<ab>},{<ab>},{<ab>},{<ab>}}, score of 1 + 2 + 2 + 2 + 2 = 9.
--	* {{<!!>},{<!!>},{<!!>},{<!!>}}, score of 1 + 2 + 2 + 2 + 2 = 9.
--	* {{<a!>},{<a!>},{<a!>},{<ab>}}, score of 1 + 2 = 3.
--
--	What is the total score for all groups in your input?
--
-- Test:
--  Garbage tests:
--	% cd ../.. && echo -e '<>' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<random characters>' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<<<<> ' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<{!>}>' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<!!>' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<!!!>>' | cabal run 2017_day09-stream_processing # 0
--	% cd ../.. && echo -e '<{o"i!a,<{i<a>' | cabal run 2017_day09-stream_processing # 0
--  Group tests:
--	% cd ../.. && echo -e '{}' | cabal run 2017_day09-stream_processing # 1
--	% cd ../.. && echo -e '{{{}}}' | cabal run 2017_day09-stream_processing # 6
--	% cd ../.. && echo -e '{{},{}}' | cabal run 2017_day09-stream_processing # 5
--	% cd ../.. && echo -e '{{{},{},{{}}}}' | cabal run 2017_day09-stream_processing # 16
--	% cd ../.. && echo -e '{<a>,<a>,<a>,<a>}' | cabal run 2017_day09-stream_processing # 1
--	% cd ../.. && echo -e '{{<ab>},{<ab>},{<ab>},{<ab>}}' | cabal run 2017_day09-stream_processing # 9
--	% cd ../.. && echo -e '{{<!!>},{<!!>},{<!!>},{<!!>}}' | cabal run 2017_day09-stream_processing # 9
--	% cd ../.. && echo -e '{{<a!>},{<a!>},{<a!>},{<ab>}}' | cabal run 2017_day09-stream_processing # 3

data Group =
            Garbage String
          | Groups [Group]
  deriving (Show)

type Parser = Parsec Void String

parse :: Parser Group
parse = group <* newline <* eof

group :: Parser Group
group = choice
  [
    char '<' *> garbage <* char '>'
  , char '{' *> groups <* char '}'
  ]

groups :: Parser Group
groups = Groups <$> (group `sepBy` char ',')

garbage :: Parser Group
garbage = Garbage <$> many garbage'
  where garbage' :: Parser Char
        garbage' = choice
          [
            char '!' *> anySingle
          , noneOf ">"
          ]

solve :: Either a Group -> Int
solve (Right g) = score 1 g
  where score val (Garbage _) = 0
        score val (Groups gs) = val + (sum . map (score (val + 1)) $ gs)


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

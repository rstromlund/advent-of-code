module Main where
import Data.Graph.Inductive (UGr, reachable, mkUGraph)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Each program has one or more programs with which it can communicate, and these pipes are
--	bidirectional; if 8 says it can communicate with 11, then 11 will say it can communicate with 8.
--
--	You need to figure out how many programs are in the group that contains program ID 0.
--
--	For example, suppose you go door-to-door like a travelling salesman and record the following list:
--
--	0 <-> 2
--	1 <-> 1
--	2 <-> 0, 3, 4
--	3 <-> 2, 4
--	4 <-> 2, 3, 6
--	5 <-> 6
--	6 <-> 4, 5
--
--	In this example, the following programs are in the group that contains program ID 0:
--
--	* Program 0 by definition.
--	* Program 2, directly connected to program 0.
--	* Program 3 via program 2.
--	* Program 4 via program 2.
--	* Program 5 via programs 6, then 4, then 2.
--	* Program 6 via programs 4, then 2.
--
--	Therefore, a total of 6 programs are in this group; all but program 1, which has a pipe that connects it to itself.
--
--	How many programs are in the group that contains program ID 0?
--
-- Test:
--	% cd ../.. && echo -e '0 <-> 2\n1 <-> 1\n2 <-> 0, 3, 4\n3 <-> 2, 4\n4 <-> 2, 3, 6\n5 <-> 6\n6 <-> 4, 5' | cabal run 2017_day12-digital_plumber # 6

type Node = (Int, [Int]) -- (id, [vertices])

type Parser = Parsec Void String

parse :: Parser [Node]
parse = many (node <* newline) <* eof

node :: Parser Node
node = (,) <$> decimal <*> (string " <-> " *> (decimal `sepBy` string ", "))

solve :: Either a [Node] -> Int
solve (Right ns) = length . reachable 0 $ g
  where g :: UGr
        g = mkUGraph (fst <$> ns) (sequenceA =<< ns) -- nodes and edges
        --
        -- W/o library functions:
        -- size . walk empty $ 0
        --   where walk :: Set Int -> Int -> Set Int
        --         walk ps n = foldl' walk ps' vs'
        --           where ps'     = insert n ps -- visited programs (nodes)
        --                 Just vs = lookup n ns
        --                 vs'     = vs \\ ps' -- unvisited vertices


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

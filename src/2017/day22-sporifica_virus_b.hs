module Main where
import Data.HashMap.Strict ((!?), HashMap, delete, fromList, insert)

--	... Now, before it infects a clean node, it will weaken it to disable your defenses. If it encounters
--	an infected node, it will instead flag the node to be cleaned in the future. So:
--
--	* Clean nodes become weakened.
--	* Weakened nodes become infected.
--	* Infected nodes become flagged.
--	* Flagged nodes become clean.
--
--	Every node is always in exactly one of the above states.
--
--	The virus carrier still functions in a similar way, but now uses the following logic during its bursts of action:
--
--	* Decide which way to turn based on the current node:
--	  * If it is clean, it turns left.
--	  * If it is weakened, it does not turn, and will continue moving in the same direction.
--	  * If it is infected, it turns right.
--	  * If it is flagged, it reverses direction, and will go back the way it came.
--	* Modify the state of the current node, as described above.
--	* The virus carrier moves forward one node in the direction it is facing.
--	Start with the same map (still using . for clean and # for infected) and still with the virus carrier starting in the middle and facing up.
--
--	Using the same initial state as the previous example, and drawing weakened as W and flagged as F, the
--	middle of the infinite grid looks like this, with the virus carrier's position again marked with [ ]:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . # . . .
--	. . . #[.]. . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	This is the same as before, since no initial nodes are weakened or flagged. The virus carrier is on a
--	clean node, so it still turns left, instead weakens the node, and moves left:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . # . . .
--	. . .[#]W . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	The virus carrier is on an infected node, so it still turns right, instead flags the node, and moves up:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . .[.]. # . . .
--	. . . F W . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	This process repeats three more times, ending on the previously-flagged node and facing right:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . W W . # . . .
--	. . W[F]W . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	Finding a flagged node, it reverses direction and cleans the node:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . W W . # . . .
--	. .[W]. W . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	The weakened node becomes infected, and it continues in the same direction:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . W W . # . . .
--	.[.]# . W . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	Of the first 100 bursts, 26 will result in infection. Unfortunately, another feature of this evolved
--	virus is speed; of the first 10000000 bursts, 2511944 will result in infection.
--
--	Given your actual map, after 10000000 bursts of activity, how many bursts cause a node to become
--	infected? (Do not count nodes that begin infected.)
--
-- Test:
--	% cd ../.. && echo -e '..#\n#..\n...' | cabal run 2017_day22-sporifica_virus_b # = 2511944 (or 26 after 100 bursts)

type Point = (Int, Int) -- x,y
type Nodes = HashMap Point State

data State = Clean | Weakened | Infected | Flagged deriving (Enum, Eq, Ord, Show)

parse :: String -> [(Point, State)]
parse cs = zipWith (\pt ch -> (pt, if '#' == ch then Infected else Clean)) [(x,y) | y <- [0..ly], x <- [0..lx]] . concat $ ls
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls

cardinalDirections 0 = ( 0, -1) -- north  -- adding turns right (v)
cardinalDirections 1 = ( 1,  0) -- east
cardinalDirections 2 = ( 0,  1) -- south
cardinalDirections 3 = (-1,  0) -- west   -- subtracting turns left (^)

turnLeft   dir = (dir + 3) `mod` 4
turnRight  dir = (dir + 1) `mod` 4
turnAround dir = (dir + 2) `mod` 4

walk :: (Int, ((Int, Int), Int, HashMap Point State)) -> Int -> (Int, ((Int, Int), Int, HashMap Point State))
walk (cnt, (pt, dir, pm)) _ = act $ pm !? pt
  where act Nothing         = move (cnt,     (pt, turnLeft dir,   insert pt Weakened pm))
        act (Just Weakened) = move (cnt + 1, (pt, dir,            insert pt Infected pm))
        act (Just Infected) = move (cnt,     (pt, turnRight dir,  insert pt Flagged  pm))
        act (Just Flagged)  = move (cnt,     (pt, turnAround dir, delete pt pm))
        move (cnt, ((x, y), dir, pm)) = let (dx, dy) = cardinalDirections dir in (cnt, ((x + dx, y + dy), dir, pm))

solve :: [(Point, State)] -> Int -- HashMap Point State
solve ns = fst . foldl' walk (0, ((startx, starty), startd, ns')) $ [1..10000000]
  where ns' = fromList . filter ((Infected ==) . snd) $ ns
        (maxx, maxy) = maximum . map fst $ ns
        startx = maxx `div` 2
        starty = maxy `div` 2
        startd = 0 -- North/Up


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.HashMap.Strict ((!?), HashMap, elems, empty, findWithDefault, insert, singleton)
import Data.Maybe (fromMaybe)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string, symbolChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	To be safe, the CPU also needs to know the highest value held in any register during this process so
--	that it can decide how much memory to allocate to these operations. For example, in the above
--	instructions, the highest value ever held was 10 (in register c after the third instruction was
--	evaluated).
--
-- Test:
--	% cd ../.. && echo -e 'b inc 5 if a > 1\na inc 1 if b < 5\nc dec -10 if a >= 1\nc inc -20 if c == 10' | cabal run 2017_day08-i_heard_you_like_registers_b # 10

maxKey = "*"
type Command = (String, String, Int, String, String, Int) -- (Reg, Cmd, Operand, Reg, Comparison, Operand) e.g. "b inc 5 if a > 1"

type Parser = Parsec Void String

parse :: Parser [Command]
parse = many (command <* newline) <* eof

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

word :: Parser String
word = many (letterChar <|> symbolChar <|> char '!')

command :: Parser Command
command = (,,,,,) <$> (word <* space) <*> (word <* space) <*> (signed <* string " if ") <*> (word <* space) <*> (word <* space) <*> signed

run :: HashMap String Int -> Command -> HashMap String Int
run !rs (tgt, cmd, cmdop, src, cmp, cmpop) = insert maxKey (max maxbeg tgtend) (insert tgt tgtend rs)
  where Just maxbeg = rs !? maxKey
        tgtbeg = findWithDefault 0 tgt rs
        tgtend
          | not tst = tgtbeg
          | "inc" == cmd = tgtbeg + cmdop
          | "dec" == cmd = tgtbeg - cmdop
        srcbeg = findWithDefault 0 src rs
        tst
          | "==" == cmp = srcbeg == cmpop
          | "!=" == cmp = srcbeg /= cmpop
          | ">"  == cmp = srcbeg >  cmpop
          | ">=" == cmp = srcbeg >= cmpop
          | "<"  == cmp = srcbeg <  cmpop
          | "<=" == cmp = srcbeg <= cmpop

solve :: Either a [Command] -> Int
solve (Right cs) = fromMaybe minBound . (!? maxKey) . foldl' run (singleton maxKey minBound) $ cs


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

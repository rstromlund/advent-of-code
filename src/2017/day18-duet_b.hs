-- TODO: begging for lens : https://stackoverflow.com/questions/23942082/is-there-a-way-in-haskell-to-express-a-point-free-function-to-edit-the-property
module Main where
import Data.Array ((!), Array, bounds, inRange, listArray)
import Data.Bifunctor (bimap, first)
import Data.HashMap.Strict as M (HashMap, empty, findWithDefault, insert, singleton)
import Data.Sequence as S ((|>), Seq((:<|)), ViewL((:<)), empty, null, viewl)
import Data.Tuple (swap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	As you congratulate yourself for a job well done, you notice that the documentation has been on the
--	back of the tablet this entire time. While you actually got most of the instructions correct, there
--	are a few key differences. This assembly code isn't about sound at all - it's meant to be run twice at
--	the same time.
--
--	Each running copy of the program has its own set of registers and follows the code independently - in
--	fact, the programs don't even necessarily run at the same speed. To coordinate, they use the send
--	(snd) and receive (rcv) instructions:
--
--	* snd X sends the value of X to the other program. These values wait in a queue until that program is
--	  ready to receive them. Each program has its own message queue, so a program can never receive a
--	  message it sent.
--	* rcv X receives the next value and stores it in register X. If no values are in the queue, the
--	  program waits for a value to be sent to it. Programs do not continue to the next instruction until
--	  they have received a value. Values are received in the order they are sent.
--
--	Each program also has its own program ID (one 0 and the other 1); the register p should begin with this value.
--
--	For example:
--
--	snd 1
--	snd 2
--	snd p
--	rcv a
--	rcv b
--	rcv c
--	rcv d
--
--	Both programs begin by sending three values to the other. Program 0 sends 1, 2, 0; program 1 sends 1,
--	2, 1. Then, each program receives a value (both 1) and stores it in a, receives another value (both 2)
--	and stores it in b, and then each receives the program ID of the other program (program 0 receives 1;
--	program 1 receives 0) and stores it in c. Each program now sees a different value in its own copy of
--	register c.
--
--	Finally, both programs try to rcv a fourth time, but no data is waiting for either of them, and they
--	reach a deadlock. When this happens, both programs terminate.
--
--	It should be noted that it would be equally valid for the programs to run at different speeds; for
--	example, program 0 might have sent all three values and then stopped at the first rcv before program 1
--	executed even its first instruction.
--
--	Once both of your programs have terminated (regardless of what caused them to do so), how many times did program 1 send a value?
--
-- Test:
--	% cd ../.. && echo -e 'set a 1\nadd a 2\nmul a a\nmod a 5\nsnd a\nset a 0\nrcv a\njgz a -1\nset a 1\njgz a -2' | cabal run 2017_day18-duet_b # = 1
--	% cd ../.. && echo -e 'snd 1\nsnd 2\nsnd p\nrcv a\nrcv b\nrcv c\nrcv d' | cabal run 2017_day18-duet_b # = 3

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Snd Memory
                 | Set Memory Memory
                 | Add Memory Memory
                 | Mul Memory Memory
                 | Mod Memory Memory
                 | Rcv Memory
                 | Jgz Memory Memory
                 deriving (Show, Eq, Ord)

type Parser = Parsec Void String

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

instructions :: Parser [Instruction]
instructions = many (instruction <* newline) <* eof

instruction :: Parser Instruction
instruction =
  string     "snd" *> space *> (Snd <$> mkMemory)
  <|> string "set" *> space *> (Set <$> mkMemory <*> (space *> mkMemory))
  <|> string "add" *> space *> (Add <$> mkMemory <*> (space *> mkMemory))
  <|> string "mul" *> space *> (Mul <$> mkMemory <*> (space *> mkMemory))
  <|> string "mod" *> space *> (Mod <$> mkMemory <*> (space *> mkMemory))
  <|> string "rcv" *> space *> (Rcv <$> mkMemory)
  <|> string "jgz" *> space *> (Jgz <$> mkMemory <*> (space *> mkMemory))

data Thread = Thread {tid :: !Int, iptr :: !Int, blkd :: Bool, regs :: HashMap Char Int, que :: Seq Int, sndCnt :: Int} deriving (Show)
newThread = Thread {tid = 0, iptr = 0, blkd = False, regs = singleton 'p' 0, que = S.empty, sndCnt = 0}

type VM = (Array Int Instruction, Thread, Thread)

succIptr   cpu@Thread{iptr   = ip}  = cpu{iptr   = succ ip}
succSndCnt cpu@Thread{sndCnt = cnt} = cpu{sndCnt = succ cnt}
getMem     (Constant i)          = const i
getMem     (Register ch)         = findWithDefault 0 ch . regs
setReg     (Register ch) r cpu   = cpu{regs = insert ch r (regs cpu)}
setBlock   v cpu                 = cpu{blkd = v}
setQueue   q cpu                 = cpu{que = q}
enQueue    v cpu@Thread{que = q} = setQueue (q |> v) cpu -- Enque value to tail of queue
deQueue    cpu@Thread{que = q}   = viewl q
nullQueue  cpu@Thread{que = q}   = S.null q

step :: Array Int Instruction -> (Thread, Thread) -> (Thread, Thread)
step is ts@(_cpu@Thread{iptr = ip, blkd = blk}, _)
  | blk                          = ts -- waiting for a queue value
  | not $ inRange (bounds is) ip = first (setBlock True) ts -- exited program code
  | otherwise                    = step' ts $ is ! ip
  where step' :: (Thread, Thread) -> Instruction -> (Thread, Thread)
        step' ts (Set r m2) = first (\t -> succIptr . setReg r (getMem m2 t) $ t) ts
        step' ts (Add r m2) = first (\t -> succIptr . setReg r (getMem r t  +    getMem m2 t) $ t) ts
        step' ts (Mul r m2) = first (\t -> succIptr . setReg r (getMem r t  *    getMem m2 t) $ t) ts
        step' ts (Mod r m2) = first (\t -> succIptr . setReg r (getMem r t `mod` getMem m2 t) $ t) ts
        step' ts (Jgz r m2) = first (\t@Thread{iptr = ip} -> t{iptr = ip + (if getMem r t > 0 then getMem m2 t else 1)}) ts
        --
        step' ts@(t,_) (Snd r) = let val = getMem r t in bimap (succIptr . succSndCnt) (setBlock False . enQueue val) ts
        --
        step' ts@(t,_) (Rcv r)
          | nullQueue t = first (setBlock True) ts -- empty queue, block thread and don't advance iptr
          | otherwise   = let (hd :< q') = deQueue t -- Deque value from head of queue
                          in first (succIptr . setBlock False . setQueue q' . setReg r hd) ts

solve :: Either a [Instruction] -> Int
solve (Right instrs) = sndCnt . get1 . exec $ (newThread, newThread{tid = 1, regs = singleton 'p' 1})
  where is  = listArray (0, length instrs - 1) instrs
        bis = bounds is
        get1 (ta@Thread{tid = tida}, tb)
          | 1 == tida = ta
          | otherwise = tb
        --
        exec :: (Thread, Thread) -> (Thread, Thread)
        exec ts@(Thread{iptr = ip0, blkd = b0}, Thread{iptr = ip1, blkd = b1})
          | b0 && b1 = ts -- both threads deadlocked
          | not (inRange bis ip0 || inRange bis ip1) = ts -- both threads exited (neither in range)
          | otherwise = exec . swap . step is $ ts


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

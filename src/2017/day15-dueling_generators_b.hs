{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Bits ((.&.))
import Data.Word (Word32)

--	... They still generate values in the same way, but now they only hand a value to the judge when it meets their criteria:
--
--	Generator A looks for values that are multiples of 4.
--	Generator B looks for values that are multiples of 8.
--
-- Test:
--	% cd ../.. && echo -e 'Generator A starts with 65\nGenerator B starts with 8921' | cabal run 2017_day15-dueling_generators_b # 588

factorA   = 16807
factorB   = 48271
criteriaA = 4
criteriaB = 8
modulus   = 2147483647
sigBits   = 0b1111111111111111
judgePairs = 5000000

parse :: String -> [Int]
parse = map (read . last . words) . lines

generator :: Int -> Int -> Int -> [Int]
generator c f !s = next
  where !s' = (f * s) `rem` modulus
        next
          | s' `mod` c == 0 = s':generator c f s'
          | otherwise       = generator c f s'

solve :: [Int] -> Int
solve [!startA, !startB] = foldl' (\acc same -> if same then acc + 1 else acc) 0 . take judgePairs $ vs
  where vs = zipWith (\a b -> a .&. sigBits == b .&. sigBits) genA genB
        genA = generator criteriaA factorA startA
        genB = generator criteriaB factorB startB


main :: IO ()
main = interact $ show . solve . parse

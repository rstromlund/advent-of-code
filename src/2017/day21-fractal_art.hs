module Main where
import Data.Bifunctor (first)
import Data.HashMap.Strict ((!), HashMap, fromList)
import Data.List (transpose)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, oneOf, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)

--	... The program always begins with this pattern:
--
--	.#.
--	..#
--	###
--
--	Because the pattern is both 3 pixels wide and 3 pixels tall, it is said to have a size of 3.
--
--	Then, the program repeats the following process:
--
--	* If the size is evenly divisible by 2, break the pixels up into 2x2 squares, and convert each 2x2
--	  square into a 3x3 square by following the corresponding enhancement rule.
--	* Otherwise, the size is evenly divisible by 3; break the pixels up into 3x3 squares, and convert each
--	  3x3 square into a 4x4 square by following the corresponding enhancement rule.
--
--	Because each square of pixels is replaced by a larger one, the image gains pixels and so its size increases.
--
--	The artist's book of enhancement rules is nearby (your puzzle input); however, it seems to be missing
--	rules. The artist explains that sometimes, one must rotate or flip the input pattern to find a
--	match. (Never rotate or flip the output pattern, though.) Each pattern is written concisely: rows are
--	listed as single units, ordered top-down, and separated by slashes.
--
--	... How many pixels stay on after 5 iterations?
--
-- Test:
--	% cd ../.. && echo -e '../.# => ##./#../...\n.#./..#/### => #..#/..../..../#..#' | cabal run 2017_day21-fractal_art # = 12

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way
type PuzzleMap = [String]

rotate90 = map reverse . transpose
rot0   = id
rot90  = rot0   . rotate90
rot180 = rot90  . rotate90
rot270 = rot180 . rotate90

type Parser = Parsec Void String

rules :: Parser [(PuzzleMap, PuzzleMap)]
rules = concat <$> (many (rule <* newline) <* eof)

rule :: Parser [(PuzzleMap, PuzzleMap)]
rule = mkRotsAndFlips <$> ((many (oneOf "#.") `sepBy` char '/') `sepBy` string " => ")
  where flp = map reverse
        mkRotsAndFlips [inp, outp] = rots ++ map (first flp) rots
          where rots = [(rot0 inp, outp), (rot90 inp, outp), (rot180 inp, outp), (rot270 inp, outp)]

getsq :: Int -> Int -> Int -> PuzzleMap -> PuzzleMap
getsq stp miny minx = map (slice minx) . slice miny
  where slice n = take stp . drop n

joinsq :: [PuzzleMap] -> PuzzleMap
joinsq sqs@((_:_):_) = top:joinsq nxt
  where top = concatMap head sqs
        nxt = map tail sqs
joinsq _ = []

enhance :: HashMap PuzzleMap PuzzleMap -> PuzzleMap -> Int -> PuzzleMap
enhance rs pzl _ = concatMap joinsq srch
  where siz = length pzl
        stp | even siz  = 2
            | otherwise = 3
        --
        sqrss = map (\y -> map (\x -> getsq stp y x pzl) [0,stp..siz-1]) [0,stp..siz-1]
        --
        srch :: [[PuzzleMap]]
        srch = map (map (rs !)) sqrss

start = [".#.", "..#", "###"]

solve :: Either a [(PuzzleMap, PuzzleMap)] -> Int
solve (Right rs) = cntHashes . foldl' enhance' start $ [1..5]
  where rs' = fromList rs
        enhance' = enhance rs'
        cntHashes :: PuzzleMap -> Int
        cntHashes = length . filter ('#' ==) . concat


main :: IO ()
main = interact $ show . solve . runParser rules "<stdin>"

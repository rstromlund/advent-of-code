module Main where
import Data.HashMap.Strict ((!?), HashMap, delete, fromList, insert)

--	... To avoid detection, the virus carrier works in bursts; in each burst, it wakes up, does some work, and goes back to sleep. The following steps are all executed in order one time each burst:
--
--	* If the current node is infected, it turns to its right. Otherwise, it turns to its left. (Turning is done in-place; the current node does not change.)
--	* If the current node is clean, it becomes infected. Otherwise, it becomes cleaned. (This is done after the node is considered for the purposes of changing direction.)
--	* The virus carrier moves forward one node in the direction it is facing.
--
--	Diagnostics have also provided a map of the node infection status (your puzzle input). Clean nodes are
--	shown as .; infected nodes are shown as #. This map only shows the center of the grid; there are many
--	more nodes beyond those shown, but none of them are currently infected.
--
--	The virus carrier begins in the middle of the map facing up.
--
--	For example, suppose you are given a map like this:
--
--	..#
--	#..
--	...
--
--	Then, the middle of the infinite grid looks like this, with the virus carrier's position marked with [ ]:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . # . . .
--	. . . #[.]. . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	The virus carrier is on a clean node, so it turns left, infects the node, and moves left:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . # . . .
--	. . .[#]# . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
--	The virus carrier is on an infected node, so it turns right, cleans the node, and moves up:
--
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . .[.]. # . . .
--	. . . . # . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--	. . . . . . . . .
--
-- Test:
--	% cd ../.. && echo -e '..#\n#..\n...' | cabal run 2017_day22-sporifica_virus # = 5587

type Point = (Int, Int) -- x,y
type Nodes = HashMap Point Char

parse :: String -> [(Point, Char)]
parse cs = zip [(x,y) | y <- [0..ly], x <- [0..lx]] . concat $ ls
  where ls = lines cs
        ly = pred . length $ ls
        lx = pred . length . head $ ls

cardinalDirections 0 = ( 0, -1) -- north
cardinalDirections 1 = ( 1,  0) -- east
cardinalDirections 2 = ( 0,  1) -- south
cardinalDirections 3 = (-1,  0) -- west

walk :: (Int, ((Int, Int), Int, HashMap Point Char)) -> Int -> (Int, ((Int, Int), Int, HashMap Point Char))
walk (cnt, (pt, dir, pm)) _ = act $ pm !? pt
  where act Nothing    = move (cnt + 1, (pt, (dir + 3) `mod` 4, insert pt '#' pm))
        act (Just '#') = move (cnt,     (pt, (dir + 1) `mod` 4, delete pt     pm))
        move (cnt, ((x, y), dir, pm)) = let (dx, dy) = cardinalDirections dir in (cnt, ((x + dx, y + dy), dir, pm))

solve :: [(Point, Char)] -> Int -- HashMap Point Char
solve ns = fst . foldl' walk (0, ((startx, starty), startd, ns')) $ [1..10000]
  where ns' = fromList . filter (('#' ==) . snd) $ ns
        (maxx, maxy) = maximum . map fst $ ns
        startx = maxx `div` 2
        starty = maxy `div` 2
        startd = 0 -- North/Up


main :: IO ()
main = interact $ show . solve . parse

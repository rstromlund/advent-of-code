module Main where
import Data.Tuple (swap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The strength of a bridge is the sum of the port types in each component. For example, if your
--	bridge is made of components 0/3, 3/7, and 7/4, your bridge has a strength of 0+3 + 3+7 + 7+4 = 24.
--
--	For example, suppose you had the following components:
--
--	0/2
--	2/2
--	2/3
--	3/4
--	3/5
--	0/1
--	10/1
--	9/10
--
--	With them, you could make the following valid bridges:
--
--	0/1
--	0/1--10/1
--	0/1--10/1--9/10
--	0/2
--	0/2--2/3
--	0/2--2/3--3/4
--	0/2--2/3--3/5
--	0/2--2/2
--	0/2--2/2--2/3
--	0/2--2/2--2/3--3/4
--	0/2--2/2--2/3--3/5
--
--	(Note how, as shown by 10/1, order of ports within a component doesn't matter. However, you may only
--	use each port on a component once.)
--
--	Of these bridges, the strongest one is 0/1--10/1--9/10; it has a strength of 0+1 + 1+10 + 10+9 = 31.
--
--	What is the strength of the strongest bridge you can make with the components you have available?
--
-- Test:
--	% cd ../.. && echo -e '0/2\n2/2\n2/3\n3/4\n3/5\n0/1\n10/1\n9/10' | cabal run 2017_day24-electromagnetic_moat # = 31

type Component = (Int, Int) -- (port0, port1)
type Bridge = [Component]

type Parser = Parsec Void String

components :: Parser [Component]
components = many (component <* newline) <* eof

component :: Parser Component
component = (,) <$> (decimal <* char '/') <*> decimal

findPort :: (Bridge, ([Component], [Component])) -> [Bridge]
findPort (bs, (cs, ds)) = findMatch (bs, (cs ++ ds, []))
  where findMatch :: (Bridge, ([Component], [Component])) -> [Bridge]
        findMatch (bs@((_, bp):_), (c@(p0, p1):cs, ds))
          | bp == p0 = findMatch (c:bs, (cs ++ ds, []))      ++ findMatch (bs, (cs, c:ds))
          | bp == p1 = findMatch (swap c:bs, (cs ++ ds, [])) ++ findMatch (bs, (cs, c:ds))
          | otherwise = findMatch (bs, (cs, c:ds))
        findMatch (bs, ([], ds)) = [bs]

solve :: Either a [Component] -> Int
solve (Right cs) = maximum . map strength . findPort $ ([start], (cs, []))
  where start = (0,0) :: Component
        strength = sum . map (uncurry (+))


main :: IO ()
main = interact $ show . solve . runParser components "<stdin>"

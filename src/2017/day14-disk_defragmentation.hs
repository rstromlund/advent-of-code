{-# LANGUAGE BangPatterns #-}
-- TODO: Like in day 12, there is a cool graph based solution here.
module Main where
import Data.Bits ((.&.), shiftR, xor)
import Data.List.Split (chunksOf)
import Data.Word (Word8)

--	... the state of the grid is tracked by the bits in a sequence of knot hashes.
--
--	A total of 128 knot hashes are calculated, each corresponding to a single row in the grid; each hash
--	contains 128 bits which correspond to individual grid squares. Each bit of a hash indicates whether
--	that square is free (0) or used (1).
--
--	The hash inputs are a key string (your puzzle input), a dash, and a number from 0 to 127 corresponding
--	to the row. For example, if your key string were flqrgnkx, then the first row would be given by the
--	bits of the knot hash of flqrgnkx-0, the second row from the bits of the knot hash of flqrgnkx-1, and
--	so on until the last row, flqrgnkx-127.
--
--	The output of a knot hash is traditionally represented by 32 hexadecimal digits; each of these digits
--	correspond to 4 bits, for a total of 4 * 32 = 128 bits. To convert to bits, turn each hexadecimal
--	digit to its equivalent binary value, high-bit first: 0 becomes 0000, 1 becomes 0001, e becomes 1110,
--	f becomes 1111, and so on; a hash that begins with a0c2017... in hexadecimal would begin with
--	10100000110000100000000101110000... in binary.
--
--	Continuing this process, the first 8 rows and columns for key flqrgnkx appear as follows, using # to
--	denote used squares, and . to denote free ones:
--
--	##.#.#..-->
--	.#.#.#.#
--	....#.#.
--	#.#.##.#
--	.##.#...
--	##..#..#
--	.#...#..
--	##.#.##.-->
--	|      |
--	V      V
--
--	In this example, 8108 squares are used across the entire 128x128 grid.
--
--	Given your actual key string, how many squares are used?
--
-- Test:
--	% cd ../.. && echo -e 'flqrgnkx' | cabal run 2017_day14-disk_defragmentation # 8108

type Length = Int

parse :: String -> [Length]
parse = map fromEnum . safeHead . lines
  where safeHead []    = []
        safeHead (x:_) = x

listSize  = 256
maxRounds = 64
postfix   = [17, 31, 73, 47, 23] :: [Length]

knot :: [Length] -> [Word8]
knot !ls = map (foldl1' xor) . chunksOf 16 . knot' 0 0 ls' $ [0..fromIntegral (listSize - 1)]
  where ls' = concat . replicate maxRounds . (++ postfix) $ ls
        words2integer :: [Word8] -> Integer
        words2integer = foldl' (\acc x -> acc * 0x100 + fromIntegral x) 0
        --
        knot' :: Int -> Int -> [Length] -> [Word8] -> [Word8]
        knot' pos   _    []      is = take listSize . drop (listSize - (pos `mod` listSize)) . cycle $ is
        knot' !pos !skip (l:ls) !is = knot' (pos + l + skip) (skip + 1) ls . take listSize . drop (l + skip) . cycle $ is'
          where is' = let (bf, af) = splitAt l is
                      in reverse bf ++ af

-- number of bits in a nibble
nibble2int  0 = 0
nibble2int  1 = 1
nibble2int  2 = 1
nibble2int  3 = 2
nibble2int  4 = 1
nibble2int  5 = 2
nibble2int  6 = 2
nibble2int  7 = 3
nibble2int  8 = 1
nibble2int  9 = 2
nibble2int 10 = 2
nibble2int 11 = 3
nibble2int 12 = 2
nibble2int 13 = 3
nibble2int 14 = 3
nibble2int 15 = 4
nibble2int x = error $ show ("ERROR undefined context for 'nibble2int'", x)

-- number of bits in an 8bit word (2 nibbles)
word2int :: Word8 -> Int
word2int w = nibble2int ((w `shiftR` 4) .&. 15) + nibble2int (w .&. 15)

gridSize = 128

solve :: [Length] -> Int
solve ls = score . map (knot . (ls ++) . map fromEnum . ("-" ++) . show) $ [0..(gridSize - 1)]
  where score :: [[Word8]] -> Int
        score = sum . map (sum . map word2int . take (gridSize `div` 4))

main :: IO ()
main = interact $ show . solve . parse

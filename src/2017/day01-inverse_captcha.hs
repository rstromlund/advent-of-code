module Main where
import Data.Char (digitToInt)

--	... The captcha requires you to review a sequence of digits (your puzzle input) and find the sum of
--	all digits that match the next digit in the list. The list is circular, so the digit after the last
--	digit is the first digit in the list.
--
--	For example:
--
--	1122 produces a sum of 3 (1 + 2) because the first digit (1) matches the second digit and the third digit (2) matches the fourth digit.
--	1111 produces 4 because each digit (all 1) matches the next.
--	1234 produces 0 because no digit matches the next.
--	91212129 produces 9 because the only digit that matches the next one is the last digit, 9.
--	What is the solution to your captcha?
--
-- Test:
--	% cd ../.. && echo -e '1122' | cabal run 2017_day01-inverse_captcha # = 3
--	% cd ../.. && echo -e '1111' | cabal run 2017_day01-inverse_captcha # = 4
--	% cd ../.. && echo -e '1234' | cabal run 2017_day01-inverse_captcha # = 0
--	% cd ../.. && echo -e '91212129' | cabal run 2017_day01-inverse_captcha # = 9

type Captcha = [Int]

parse :: String -> Captcha
parse = map digitToInt . head . words

solve :: Captcha -> Int
solve is = sum [a | (a, b) <- zip is (drop 1 . cycle $ is), a == b]


main :: IO ()
main = interact $ show . solve . parse

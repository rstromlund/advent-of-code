{-# LANGUAGE BangPatterns #-}
-- FYI, to get a low level dump:
-- stack ghc -- -c day05-a_maze_of_twisty_trampolines_all_alike_b.hs -ddump-ds | sed -Ee 's!_[A-Za-z0-9]+!!g'
module Main where
import Data.Vector as V ((//), (!), Vector, fromList, length)

--	Now, the jumps are even stranger: after each jump, if the offset was three or more, instead decrease it by 1. Otherwise, increase it by 1 as before.
--
--	Using this rule with the above example, the process now takes 10 steps, and the offset values after finding the exit are left as 2 3 2 3 -1.
--
--	How many steps does it now take to reach the exit?
--
-- Test:
--	% cd ../.. && echo -e '0\n3\n0\n1\n-3' | cabal run 2017_day05-a_maze_of_twisty_trampolines_all_alike_b # 10

type Maze = [Int]

parse :: String -> Maze
parse = map read . words

walk :: Int -> Int -> Vector Int -> Int
walk !c !n !is
  | n < 0 || n >= V.length is = c
  | otherwise = walk (c + 1) (n + i) $ is // [(n, i')]
  where i  = is ! n
        i' = i + if i >= 3 then (-1) else 1

solve :: Maze -> Int
solve is = walk 0 0 $ fromList is


main :: IO ()
main = interact $ show . solve . parse

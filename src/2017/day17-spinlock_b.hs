{-# LANGUAGE BangPatterns #-}
module Main where

--	The spinlock does not short-circuit. Instead, it gets more angry. At least, you assume that's what
--	happened; it's spinning significantly faster than it was a moment ago.
--
--	You have good news and bad news.
--
--	The good news is that you have improved calculations for how to stop the spinlock. They indicate that
--	you actually need to identify the value after 0 in the current state of the circular buffer.
--
--	The bad news is that while you were determining this, the spinlock has just finished inserting its
--	fifty millionth value (50000000).
--
--	What is the value after 0 the moment 50000000 is inserted?
--
-- Test:
--	% cd ../.. && echo -e '3' | cabal run 2017_day17-spinlock_b # = <unknown from example>

parse :: String -> Int
parse = read . head . lines

insertions = 50000000

solve :: Int -> Int
solve stepCnt = fst . foldl' step (0, 0) $ [1..insertions]
  where step :: (Int, Int) -> Int -> (Int, Int)
        step (!ans, !cpos) !len =
          let p = (cpos + stepCnt) `mod` len + 1
              ans'
                | 1 == p    = len
                | otherwise = ans
          in (ans', p)
        {-# Inline step #-}


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.Either (fromRight, isLeft)
import Data.List (filter, lookup, sort)
import Data.List.Extra (sumOn')
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

-- ...
--	                gyxo
--	              /
--	         ugml - ebii
--	       /      \
--	      |         jptl
--	      |
--	      |         pbga
--	     /        /
--	tknk --- padx - havc
--	     \        \
--	      |         qoyq
--	      |
--	      |         ktlj
--	       \      /
--	         fwft - cntj
--	              \
--	                xhth
--
--	For any program holding a disc, each program standing on that disc forms a sub-tower. Each of those
--	sub-towers are supposed to be the same weight, or the disc itself isn't balanced. The weight of a
--	tower is the sum of the weights of the programs in that tower.
--
--	In the example above, this means that for ugml's disc to be balanced, gyxo, ebii, and jptl must all
--	have the same weight, and they do: 61.
--
--	However, for tknk to be balanced, each of the programs standing on its disc and all programs above it
--	must each match. This means that the following sums must all be the same:
--
--	ugml + (gyxo + ebii + jptl) = 68 + (61 + 61 + 61) = 251
--	padx + (pbga + havc + qoyq) = 45 + (66 + 66 + 66) = 243
--	fwft + (ktlj + cntj + xhth) = 72 + (57 + 57 + 57) = 243
--
--	As you can see, tknk's disc is unbalanced: ugml's stack is heavier than the other two. Even though the
--	nodes above ugml are balanced, ugml itself is too heavy: it needs to be 8 units lighter for its stack
--	to weigh 243 and keep the towers balanced. If this change were made, its weight would be 60.
--
--	Given that exactly one program is the wrong weight, what would its weight need to be to balance the
--	entire tower?
--
-- Test:
--	% cd ../.. && echo -e 'pbga (66)\nxhth (57)\nebii (61)\nhavc (66)\nktlj (57)\nfwft (72) -> ktlj, cntj, xhth\nqoyq (66)\npadx (45) -> pbga, havc, qoyq\ntknk (41) -> ugml, padx, fwft\njptl (61)\nugml (68) -> gyxo, ebii, jptl\ngyxo (61)\ncntj (57)' | cabal run 2017_day07-recursive_circus_b # 60
--	% cd ../.. && echo -e 'TKNK (0) -> tknk\npbga (86)\nxhth (77)\nebii (61)\nhavc (86)\nktlj (77)\nfwft (72) -> ktlj, cntj, xhth\nqoyq (86)\npadx (45) -> pbga, havc, qoyq\ntknk (41) -> ugml, padx, fwft\njptl (61)\nugml (68) -> gyxo, ebii, jptl\ngyxo (61)\ncntj (77)' | cabal run 2017_day07-recursive_circus_b # 16

type Node = (String, (Int, [String])) -- (name, weight, vertices)

type Parser = Parsec Void String

parse :: Parser [Node]
parse = many (node <* newline) <* eof

word :: Parser String
word = many letterChar

node :: Parser Node
node = (,) <$> word <*> ((,) <$> (string " (" *> decimal <* string ")") <*> choice
  [
    string " -> " *> (word `sepBy` string ", ")
  , pure []
  ])

-- Left = adjustment answer, Right = weight
getWeight :: [Node] -> String -> Either Int Int
getWeight ns v = bal ws
  where Just (w, vs) = lookup v ns
        ws :: [(Either Int Int, String)]
        ws = sort . map (\v -> (getWeight ns v, v)) $ vs
        --
        bal :: [(Either Int Int, String)] -> Either Int Int
        -- A leaf node's weight is just the node's weight
        bal [] = Right w
        -- If any weight is a Left, then we've found the adjustment already
        bal ws | any (isLeft . fst) ws = fst . head . filter (isLeft . fst) $ ws
        -- If the 2 smallest weights match, then the heaviest node is the outlier.  If not, then w0 must be the outlier.
        bal ws@((w0, v0):ws'@((w1, v1):_))
          -- All weights match, there is no out of balance tower
          | all ((w0 ==) . fst) ws' = Right $ w + sumOn' (fromRight 0 . fst) ws
          -- w0 and w1 match, then the last (heaviest) tower is out of balance
          | w0 == w1                = let (Right wx, vx) = last ws' -- wx is the total weight "tower" that is out of balance
                                          Just (wx', _) = lookup vx ns -- wx' is the weight of just the node
                                      in Left $ wx' - (wx - fromRight 0 w0) -- bring the weight down by the diff from min to max weight
          -- The first (lightest) tower is out of balance
          | otherwise               = let Just (w0', _) = lookup v0 ns -- w0' is the weight of just the node
                                      in Left $ w0' + (fromRight 0 w1 - fromRight 0 w0) -- bring the weight down by the diff from min to max weight (w1 matches all the max weights)

solve :: Either a [Node] -> Int
solve (Right ns) = let Left w = getWeight ns root in w
  where root = fst . head . filter (\(nm, _) -> not . any (\(_, (_, vs)) -> nm `elem` vs) $ ns) $ ns


main :: IO ()
main = interact $ show . solve . runParser parse "<stdin>"

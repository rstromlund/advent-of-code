module Main where
import Data.Char (digitToInt)

--	... Now, instead of considering the next digit, it wants you to consider the digit halfway around the
--	circular list. That is, if your list contains 10 items, only include a digit in your sum if the digit
--	10/2 = 5 steps forward matches it. Fortunately, your list has an even number of elements.
--
--	For example:
--
--	* 1212 produces 6: the list contains 4 items, and all four digits match the digit 2 items ahead.
--	* 1221 produces 0, because every comparison is between a 1 and a 2.
--	* 123425 produces 4, because both 2s match each other, but no other digit has a match.
--	* 123123 produces 12.
--	* 12131415 produces 4.
--
--	What is the solution to your new captcha?
--
-- Test:
--	% cd ../.. && echo -e '1212' | cabal run 2017_day01-inverse_captcha_b # = 6
--	% cd ../.. && echo -e '1221' | cabal run 2017_day01-inverse_captcha_b # = 0
--	% cd ../.. && echo -e '123425' | cabal run 2017_day01-inverse_captcha_b # = 4
--	% cd ../.. && echo -e '123123' | cabal run 2017_day01-inverse_captcha_b # = 12
--	% cd ../.. && echo -e '12131415' | cabal run 2017_day01-inverse_captcha_b # = 4

type Captcha = [Int]

parse :: String -> Captcha
parse = map digitToInt . head . words

solve :: Captcha -> Int
solve is = sum [a | (a, b) <- zip is (drop (length is `div` 2) . cycle $ is), a == b]


main :: IO ()
main = interact $ show . solve . parse

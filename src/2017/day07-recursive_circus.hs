module Main where
import Data.List (filter)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... For example, if your list is the following:
--
--	pbga (66)
--	xhth (57)
--	ebii (61)
--	havc (66)
--	ktlj (57)
--	fwft (72) -> ktlj, cntj, xhth
--	qoyq (66)
--	padx (45) -> pbga, havc, qoyq
--	tknk (41) -> ugml, padx, fwft
--	jptl (61)
--	ugml (68) -> gyxo, ebii, jptl
--	gyxo (61)
--	cntj (57)
--
--	... then you would be able to recreate the structure of the towers that looks like this:
--
--	                gyxo
--	              /
--	         ugml - ebii
--	       /      \
--	      |         jptl
--	      |
--	      |         pbga
--	     /        /
--	tknk --- padx - havc
--	     \        \
--	      |         qoyq
--	      |
--	      |         ktlj
--	       \      /
--	         fwft - cntj
--	              \
--	                xhth
--
--	Before you're ready to help them, you need to make sure your information is correct. What is the name of the bottom program?
--
-- Test:
--	% cd ../.. && echo -e 'pbga (66)\nxhth (57)\nebii (61)\nhavc (66)\nktlj (57)\nfwft (72) -> ktlj, cntj, xhth\nqoyq (66)\npadx (45) -> pbga, havc, qoyq\ntknk (41) -> ugml, padx, fwft\njptl (61)\nugml (68) -> gyxo, ebii, jptl\ngyxo (61)\ncntj (57)' | cabal run 2017_day07-recursive_circus # tknk

type Node = (String, (Int, [String])) -- (name, weight, vertices)

type Parser = Parsec Void String

parse :: Parser [Node]
parse = many (node <* newline) <* eof

word :: Parser String
word = many letterChar

node :: Parser Node
node = (,) <$> word <*> ((,) <$> (string " (" *> decimal <* string ")") <*> choice
  [
    string " -> " *> (word `sepBy` string ", ")
  , pure []
  ])

solve :: Either a [Node] -> String
solve (Right ns) = fst . head . filter (\(nm, _) -> not . any (\(_, (_, vs)) -> nm `elem` vs) $ ns) $ ns


main :: IO ()
main = interact $ solve . runParser parse "<stdin>"

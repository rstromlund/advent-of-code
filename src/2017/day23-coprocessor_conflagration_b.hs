module Main where

--	... The debug mode switch is wired directly to register a. You flip the switch, which makes register a
--	now start at 1 when the program is executed.
--
--	Immediately, the coprocessor begins to overheat. Whoever wrote this program obviously didn't choose a
--	very efficient implementation. You'll need to optimize the program if it has any hope of completing
--	before Santa needs that printer working.
--
--	The coprocessor's ultimate goal is to determine the final value left in register h once the program
--	completes. Technically, if it had that... it wouldn't even need to run the program.
--
--	After setting register a to 1, if the program were to run to completion, what value would be left in
--	register h?

{-
Boiling the assembler down gets the following c program (it is closer to assembly and I could reason it up
into higher level constructs this way).  Looks like we are counting the number of non-primes in every 17th
number between [106700..123700].  The beginning number `b` is calculated before any looping and the ending
number `c` just 17,000 more than `b`.

The assembly runs 2 inner loops looking for 2 numbers that multiply to `b`.  We can just run 1 loop and look for mod == 0.

YMMV! This solution doesn't run the program to determine the range, they were found by analysis.

\#include <stdio.h>

int main() {
   int a = 1;
   b = 67; /* 0x43 */
   c = b;
   b *= 100; /* 6,700 */
   b += 100000; /* 106,700 */
   c = b;
   c += 17000; /* 123,700 */

   for (int b = 106700; b <= 123700; b += 17) {
      printf("bf1: a:%d, b:%d, c:%d, d:%d, e:%d, f:%d, g:%d, h:%d\n", a, b, c, d, e, f, g, h);
      for (d = 2; d < b; d ++) {
         if (0 == b % d) {++ h; break;}
      }
   }
   printf("ans: h:%d\n", h);

}

-}


solve :: Int
solve = length . filter id . map (\b -> any (\d -> 0 == b `mod` d) [2..b - 1]) $ [106700,106717..123700]


main :: IO ()
main = interact $ show . const solve

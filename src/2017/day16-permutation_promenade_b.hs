module Main where
import Data.ByteString.Internal (c2w, w2c)
import Data.ByteString as BS (ByteString, append, concat, elemIndex, pack, splitAt, unpack)
import Data.HashMap.Strict ((!?), HashMap, elems, empty, filterWithKey, fromList, insert, keys, singleton, toList)
import Data.Maybe (isJust)
import Data.List.Split (splitOn)

--	Keeping the positions they ended up in from their previous dance, the programs perform it again and
--	again: including the first dance, a total of one billion (1000000000) times.
--
--	In the example above, their second dance would begin with the order baedc, and use the same dance moves:
--
--	s1, a spin of size 1: cbaed.
--	x3/4, swapping the last two programs: cbade.
--	pe/b, swapping programs e and b: ceadb.
--
--	In what order are the programs standing after their billion dances?
--
-- Test:
--	% cd ../.. && echo -e 's1,x3/4,pe/b' | cabal run 2017_day16-permutation_promenade_b # = baedc

parse :: String -> [String]
parse = splitOn "," . head . lines

dancers    = ['a'..'p']
numDancers = length dancers
numDances  = 1000000000

dance :: ByteString -> [String] -> ByteString
dance = foldl' move
  where move :: ByteString -> String -> ByteString
        move acc ('s':cnt) = uncurry (flip BS.append) . BS.splitAt (numDancers - read cnt) $ acc
                             -- uncurry BS.append . BS.splitAt (numDancers - (read cnt)) $ acc
        move acc ('x':poss)         = let [str1, str2] = splitOn "/" poss
                                          p1 = read str1 :: Int
                                          p2 = read str2 :: Int
                                      in swapChar (min p1 p2) (max p1 p2) acc
        move acc ['p', c1, '/', c2] = let Just p1 = elemIndex (c2w c1) acc
                                          Just p2 = elemIndex (c2w c2) acc
                                      in swapChar (min p1 p2) (max p1 p2) acc
        --
        swapChar p1 p2 acc = let -- in 'a[b]c[d]e' and (1,3) aBeg='a', aMid='c', and aEnd='e'; c0='b' and c1='d'
                                 (aBeg, tail0) = BS.splitAt p1 acc
                                 (c0,   tail1) = BS.splitAt 1 tail0
                                 (aMid, tail2) = BS.splitAt (p2 - p1 - 1) tail1
                                 (c1,   aEnd)  = BS.splitAt 1 tail2
                             in BS.concat [aBeg, c1, aMid, c0, aEnd]

solve :: [String] -> String
solve ms = map w2c . unpack . foldl' (\acc _ -> dance acc ms) ps' $ [1..remdr]
  where ps = pack . map c2w $ dancers
        --
        (rept, ps') = findLoop 0 empty ps
        remdr       = numDances `mod` rept
        --
        findLoop :: Int -> HashMap ByteString Int -> ByteString -> (Int, ByteString)
        findLoop cnt hist ps
          | isJust (hist !? ps) = (cnt, ps)
          | otherwise           = let hist' = insert ps cnt hist
                                      ps'   = dance ps ms
                                      cnt'  = cnt + 1
                                  in findLoop cnt' hist' ps'


main :: IO ()
main = interact $ solve . parse

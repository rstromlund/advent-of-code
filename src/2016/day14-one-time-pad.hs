module Main where
import Crypto.Hash.MD5 (hash)
import Data.ByteString (ByteString)
import Data.ByteString.Builder (byteStringHex)
import Data.ByteString.Builder.Extra (untrimmedStrategy, toLazyByteStringWith)
import Data.ByteString.Char8 (pack, unpack)
import Data.ByteString.Lazy (empty, toStrict)
import Data.Function (on)
import Data.List (group, isPrefixOf, tails)

--	... To generate keys, you first get a stream of random data by taking the MD5 of a pre-arranged salt
--	(your puzzle input) and an increasing integer index (starting with 0, and represented in decimal); the
--	resulting MD5 hash should be represented as a string of lowercase hexadecimal digits.
--
--	However, not all of these MD5 hashes are keys, and you need 64 new keys for your one-time pad. A hash is a key only if:
--
--	* It contains three of the same character in a row, like 777. Only consider the first such triplet in a hash.
--	* One of the next 1000 hashes in the stream contains that same character five times in a row, like 77777.
--
--	Considering future hashes for five-of-a-kind sequences does not cause those hashes to be skipped;
--	instead, regardless of whether the current hash is a key, always resume testing for keys starting with
--	the very next hash.
--
--	For example, if the pre-arranged salt is abc:
--
--	* The first index which produces a triple is 18, because the MD5 hash of abc18 contains
--	  ...cc38887a5.... However, index 18 does not count as a key for your one-time pad, because none of
--	  the next thousand hashes (index 19 through index 1018) contain 88888.
--	* The next index which produces a triple is 39; the hash of abc39 contains eee. It is also the first
--	  key: one of the next thousand hashes (the one at index 816) contains eeeee.
--	* None of the next six triples are keys, but the one after that, at index 92, is: it contains 999 and index 200 contains 99999.
--	* Eventually, index 22728 meets all of the criteria to generate the 64th key.
--
--	So, using our example salt of abc, index 22728 produces the 64th key.
--
--	Given the actual salt in your puzzle input, what index produces your 64th one-time pad key?
--
-- Test:
--	% cd ../.. && echo -e 'abc' | cabal run 2016_day14-one-time-pad # = 22728

keysToMath =   64
searchSize = 1000

md5hash :: ByteString -> ByteString
md5hash = toStrict
        . toLazyByteStringWith (untrimmedStrategy 32 32) empty
        . byteStringHex
        . hash

getThreepeat :: ByteString -> String
getThreepeat = safeHead . filter ((>= 3) . length) . group . unpack . md5hash
  where safeHead []    = ""
        safeHead (s:_) = s

search :: [String] -> [Int]
search hashes = [ n | (n, str3peat:hs) <- zip [0..] $ tails hashes
                    , str3peat /= ""
                    , let str5peat = replicate 5 . head $ str3peat
                    , any (str5peat `isPrefixOf`) . take searchSize $ hs
                    ]

solve :: String -> Int
solve salt = (!! (keysToMath - 1)) . search . map (getThreepeat . (salt' <>) . pack . show) $ [0..]
  where salt' = pack salt


main :: IO ()
main = interact $ show . solve . head . words

module Main where
import Data.List (intersect, lookup)
import Data.Map.Strict (Map, (!?), empty, foldlWithKey', insert)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... What do you get if you multiply together the values of one chip in each of outputs 0, 1, and 2?
--
-- Test:
--	% cd ../.. && echo -e 'value 5 goes to bot 2\nbot 2 gives low to bot 1 and high to bot 0\nvalue 3 goes to bot 1\nbot 1 gives low to output 1 and high to bot 0\nbot 0 gives low to output 2 and high to output 0\nvalue 2 goes to bot 2' | cabal run 2016_day10-balance-bots_b # = 30

data Destination = DestBot Int | DestOutput Int deriving (Show, Eq, Ord)

data Instruction = Value Int Int -- value, bot
                 | Bot Int Destination Destination -- bot# dest1 dest2
                 | NA
                 deriving (Show, Eq, Ord)

isValueIns :: Instruction -> Bool
isValueIns (Value _ _) = True
isValueIns _           = False

isBotIns = not . isValueIns

type FactoryState = (Instruction, Maybe Int, [[Int]]) -- instruction, val one, list of bot compared values

type Parser = Parsec Void String

instructions :: Parser [Instruction]
instructions = many instruction <* optional newline <* eof

instruction :: Parser Instruction
instruction =
  Value <$> (string "value " *> decimal) <*> (string " goes to bot " *> decimal) <* newline
  <|> Bot <$> (string "bot " *> decimal) <*> (mkDest <$> (string " gives low to " *> some letterChar <* char ' ') <*> decimal) <*> (mkDest <$> (string " and high to " *> some letterChar <* char ' ') <*> decimal) <* newline
  where
    mkDest "bot"    o = DestBot    o
    mkDest "output" o = DestOutput o

giveBot :: Instruction -> Map Int FactoryState -> Map Int FactoryState
giveBot (Value val bot) fs = give' (fs !? bot) fs
  where give' :: Maybe FactoryState -> Map Int FactoryState -> Map Int FactoryState
        give' (Just (i, Nothing, vs)) = insert bot (i, Just val, vs)
        give' (Just (i, Just v1, vs)) =
          let mn = min v1 val
              mx = max v1 val
          in disburse i mn mx . insert bot (i, Nothing, [mn, mx]:vs)
        --
        disburse :: Instruction -> Int -> Int -> Map Int FactoryState -> Map Int FactoryState
        disburse (Bot bot d1 d2) v1 v2 = disburseDest d2 v2 . disburseDest d1 v1
        --
        disburseDest (DestBot    o) v = giveBot (Value v o)
        -- Doing a gross thing, storing outputs as negatives instead of an output map :X
        disburseDest (DestOutput o) v = insert (minBound + o) (NA, Just v, [])

solve :: Either a [Instruction] -> Int
solve (Right ins) = product . getDestOutputs [0..2] . foldl' (flip giveBot) fs $ vs
  where mkFactoryState :: Map Int FactoryState -> Instruction -> Map Int FactoryState
        mkFactoryState fs i@(Bot bot _ _) = insert bot (i, Nothing, [[]]) fs
        fs = foldl' mkFactoryState empty . filter isBotIns $ ins
        vs = filter isValueIns ins
        getDestOutputs os fs = map getDestOutput os
          where getDestOutput o = let Just (_, Just v, _) = (fs !? (minBound + o)); in v


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

module Main where
import Data.List (intersect, lookup)
import Data.Map.Strict (Map, (!?), empty, foldlWithKey', insert)
import Data.Maybe (isNothing)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Some of the instructions specify that a specific-valued microchip should be given to a specific
--	bot; the rest of the instructions indicate what a given bot should do with its lower-value or
--	higher-value chip.
--
--	For example, consider the following instructions:
--
--	value 5 goes to bot 2
--	bot 2 gives low to bot 1 and high to bot 0
--	value 3 goes to bot 1
--	bot 1 gives low to output 1 and high to bot 0
--	bot 0 gives low to output 2 and high to output 0
--	value 2 goes to bot 2
--
--	* Initially, bot 1 starts with a value-3 chip, and bot 2 starts with a value-2 chip and a value-5 chip.
--	* Because bot 2 has two microchips, it gives its lower one (2) to bot 1 and its higher one (5) to bot 0.
--	* Then, bot 1 has two microchips; it puts the value-2 chip in output 1 and gives the value-3 chip to bot 0.
--	* Finally, bot 0 has two microchips; it puts the 3 in output 2 and the 5 in output 0.
--
--	In the end, output bin 0 contains a value-5 microchip, output bin 1 contains a value-2 microchip, and
--	output bin 2 contains a value-3 microchip. In this configuration, bot number 2 is responsible for
--	comparing value-5 microchips with value-2 microchips.
--
--	Based on your instructions, what is the number of the bot that is responsible for comparing value-61
--	microchips with value-17 microchips?
--
-- Test:
--	% cd ../.. && echo -e 'bot 0 gives low to bot 0 and high to bot 0\nbot 0 gives low to output 0 and high to bot 0\nbot 0 gives low to output 0 and high to output 0\nvalue 0 goes to bot 0' | cabal run 2016_day10-balance-bots # na, parse building
--	% cd ../.. && echo -e 'value 5 goes to bot 2\nbot 2 gives low to bot 1 and high to bot 0\nvalue 3 goes to bot 1\nbot 1 gives low to output 1 and high to bot 0\nbot 0 gives low to output 2 and high to output 0\nvalue 2 goes to bot 2' | cabal run 2016_day10-balance-bots # = 2

data Destination = DestBot Int | DestOutput Int deriving (Show, Eq, Ord)

data Instruction = Value Int Int -- value, bot
                 | Bot Int Destination Destination -- bot# dest1 dest2
                 | NA
                 deriving (Show, Eq, Ord)

isValueIns :: Instruction -> Bool
isValueIns (Value _ _) = True
isValueIns _           = False

isBotIns = not . isValueIns

type FactoryState = (Instruction, Maybe Int, [[Int]]) -- instruction, val one, list of bot compared values

type Parser = Parsec Void String

instructions :: Parser [Instruction]
instructions = many instruction <* optional newline <* eof

instruction :: Parser Instruction
instruction =
  Value <$> (string "value " *> decimal) <*> (string " goes to bot " *> decimal) <* newline
  <|> Bot <$> (string "bot " *> decimal) <*> (mkDest <$> (string " gives low to " *> some letterChar <* char ' ') <*> decimal) <*> (mkDest <$> (string " and high to " *> some letterChar <* char ' ') <*> decimal) <* newline
  where
    mkDest "bot"    o = DestBot    o
    mkDest "output" o = DestOutput o

giveBot :: Instruction -> Map Int FactoryState -> Map Int FactoryState
giveBot (Value val bot) fs = give' (fs !? bot) fs
  where give' :: Maybe FactoryState -> Map Int FactoryState -> Map Int FactoryState
        give' (Just (i, Nothing, vs)) = insert bot (i, Just val, vs)
        give' (Just (i, Just v1, vs)) =
          let mn = min v1 val
              mx = max v1 val
          in disburse i mn mx . insert bot (i, Nothing, [mn, mx]:vs)
        --
        disburse :: Instruction -> Int -> Int -> Map Int FactoryState -> Map Int FactoryState
        disburse (Bot _ d1 d2) v1 v2 = disburseDest d2 v2 . disburseDest d1 v1
        --
        disburseDest (DestBot o) v = giveBot (Value v o)
        disburseDest _           _ = id

-- Test:
compareValueOneT = 2
compareValueTwoT = 5
-- Puzzle
compareValueOne  = 17
compareValueTwo  = 61

compareAnswers = [[compareValueOne, compareValueTwo], [compareValueOneT, compareValueTwoT]]

solve :: Either a [Instruction] -> Int
solve (Right ins) = head . foldlWithKey' score [] . foldl' (flip giveBot) fs $ vs
  where mkFactoryState :: Map Int FactoryState -> Instruction -> Map Int FactoryState
        mkFactoryState fs i@(Bot bot _ _) = insert bot (i, Nothing, [[]]) fs
        fs = foldl' mkFactoryState empty . filter isBotIns $ ins
        vs = filter isValueIns ins
        score :: [Int] -> Int -> FactoryState -> [Int]
        score acc bot (_, _, vs)
          | null $ compareAnswers `intersect` vs = acc
          | otherwise = bot:acc


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

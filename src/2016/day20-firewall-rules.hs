module Main where
import Data.List (break, sort)
import Data.Tuple.Extra (both, second)

--	... For example, suppose only the values 0 through 9 were valid, and that you retrieved the following blacklist:
--
--	5-8
--	0-2
--	4-7
--
--	The blacklist specifies ranges of IPs (inclusive of both the start and end value) that are not
--	allowed. Then, the only IPs that this firewall allows are 3 and 9, since those are the only numbers
--	not in any range.
--
--	Given the list of blocked IPs you retrieved from the firewall (your puzzle input), what is the
--	lowest-valued IP that is not blocked?
--
-- Test:
--	% cd ../.. && echo -e '5-8\n0-2\n4-7' | cabal run 2016_day20-firewall-rules # = 3

type Range = (Int, Int) -- min, max

solve :: [Range] -> Int
solve rs = search 0 rs
  where search cur ((l, r):rs)
          | cur < l   = cur
          | cur > r   = search cur rs
          | otherwise = search (r + 1) rs


main :: IO ()
main = interact $ show . solve . sort . map (second abs . both read . break ('-' ==)) . lines

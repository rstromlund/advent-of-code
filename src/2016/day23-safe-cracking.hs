module Main where
import Data.Array ((//), (!), Array, bounds, inRange, listArray)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

-- Part 1:
--	... but with one new instruction:
--
--	tgl x toggles the instruction x away (pointing at instructions like jnz does: positive means forward; negative means backward):
--
--	* For one-argument instructions, inc becomes dec, and all other one-argument instructions become inc.
--	* For two-argument instructions, jnz becomes cpy, and all other two-instructions become jnz.
--	* The arguments of a toggled instruction are not affected.
--	* If an attempt is made to toggle an instruction outside the program, nothing happens.
--	* If toggling produces an invalid instruction (like cpy 1 2) and an attempt is later made to execute that instruction, skip it instead.
--	* If tgl toggles itself (for example, if a is 0, tgl a would target itself and become inc a), the
--	  resulting instruction is not executed until the next time it is reached.
--
--	For example, given this program:
--
--	cpy 2 a
--	tgl a
--	tgl a
--	tgl a
--	cpy 1 a
--	dec a
--	dec a
--
--	* cpy 2 a initializes register a to 2.
--	* The first tgl a toggles an instruction a (2) away from it, which changes the third tgl a into inc a.
--	* The second tgl a also modifies an instruction 2 away from it, which changes the cpy 1 a into jnz 1 a.
--	* The fourth line, which is now inc a, increments a to 3.
--	* Finally, the fifth line, which is now jnz 1 a, jumps a (3) instructions ahead, skipping the dec a instructions.
--
--	In this example, the final value in register a is 3.
--
--	The rest of the electronics seem to place the keypad entry (the number of eggs, 7) in register a, run the code, and then send the value left in register a to the safe.
--
--	What value should be sent to the safe?
--
-- Test:
--	% cd ../.. && echo -e '7\ncpy 2 a\ntgl a\ntgl a\ntgl a\ncpy 1 a\ndec a\ndec a' | cabal run 2016_day23-safe-cracking # = 3
--
-- Part 2:
--	... Now you count 12.
--
--	As you run the program with this new input, the prototype computer begins to overheat. You wonder
--	what's taking so long, and whether the lack of any instruction more powerful than "add one" has
--	anything to do with it. Don't bunnies usually multiply?
--
--	Anyway, what value should actually be sent to the safe?
--
-- Test:
--	% cd ../.. && echo -e '12\ncpy 2 a\ntgl a\ntgl a\ntgl a\ncpy 1 a\ndec a\ndec a' | cabal run 2016_day23-safe-cracking # = 3

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Copy Memory Memory
                 | Inc Memory
                 | Dec Memory
                 | JmpNot0 Memory Memory
                 | Tgl Memory
                 | Mul Memory Memory Memory Memory Memory -- dest, mul1, mul2, clr1, clr2
                 | Nop
                 deriving (Show, Eq, Ord)

type Parser = Parsec Void String

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

mkSingle :: (Memory -> Instruction) -> Parser Instruction
mkSingle c = c <$> mkMemory

instruction :: Parser Instruction
instruction = (
  string     "cpy " *> (Copy <$> (mkMemory <* space) <*> mkMemory)
  <|> string "inc " *> (Inc <$> mkMemory)
  <|> string "dec " *> (Dec <$> mkMemory)
  <|> string "tgl " *> (Tgl <$> mkMemory)
  <|> string "jnz " *> (JmpNot0 <$> (mkMemory <* space) <*> mkMemory)
  ) <* eof

toggleIns (Inc a) = Dec a
toggleIns (Dec a) = Inc a
toggleIns (Tgl a) = Inc a
toggleIns (JmpNot0 m1 m2) = Copy m1 m2
toggleIns (Copy m1 m2) = JmpNot0 m1 m2

{- Per the puzzle's hint; replace addition loop:
0: cpy b c
1: inc a
2: dec c
3: jnz c -2
4: dec d
5: jnz d -5

with "undocumented" mul and nop back to original size in case there are relative offsets that need to be kept in place:
0: mul a d b d c
1: nop
2: nop
3: nop
4: nop
5: nop
-}

patchMul is = patchMul' is []
  where patchMul' [] acc = reverse acc -- exit case
        --
        -- Find and patch the multiplication loop:
        -- (I know these multiplication registers for my input, but this should be generic in case others have a different arrangement.)
        patchMul' ((Copy m1 loopreg)
                  :(Inc dest)
                  :(Dec loopreg0)
                  :(JmpNot0 loopreg1 (Constant (-2)))
                  :(Dec m2)
                  :(JmpNot0 m2' (Constant (-5)))
                  :is
                  ) acc | loopreg == loopreg0 && loopreg == loopreg1 && m2 == m2'
          = patchMul' is (Nop:Nop:Nop:Nop:Nop:Mul dest m1 m2 loopreg m2:acc)
        --
        -- Not a mul loop, just copy the instruction unchanged:
        patchMul' (i:is) acc = patchMul' is (i:acc)

data CPU = CPU {regA, regB, regC, regD, iptr :: !Int} deriving (Show)
type VM = (Array Int Instruction, CPU)

get (Constant i)   = const i
get (Register 'a') = regA
get (Register 'b') = regB
get (Register 'c') = regC
get (Register 'd') = regD
set (Register 'a') r cpu = cpu{regA = r}
set (Register 'b') r cpu = cpu{regB = r}
set (Register 'c') r cpu = cpu{regC = r}
set (Register 'd') r cpu = cpu{regD = r}

exec :: VM -> CPU
exec vm@(is, cpu@CPU{iptr = ip})
  | inRange b ip = exec . step vm $ is ! ip
  | otherwise    = cpu
  where b = bounds is
        step :: VM -> Instruction -> VM
        step (is, cpu) (Copy m1 m2@(Register _)) = (is, (set m2 (get m1 cpu) cpu){iptr = ip + 1})
        step (is, cpu) (Inc m1@(Register _))     = (is, (set m1 (get m1 cpu + 1) cpu){iptr = ip + 1})
        step (is, cpu) (Dec m1@(Register _))     = (is, (set m1 (get m1 cpu - 1) cpu){iptr = ip + 1})
        step (is, cpu) (JmpNot0 m1 m2)           = (is, cpu{iptr = ip + (if get m1 cpu /= 0 then get m2 cpu else 1)})
        step (is, cpu) (Tgl m1)                  = let tgt = ip + get m1 cpu
                                                       is'
                                                         | inRange b tgt = is // [(tgt, toggleIns $ is ! tgt)]
                                                         | otherwise     = is
                                                   in (is', cpu{iptr = ip + 1})
        -- "undocumented" instructions:
        -- "mul" takes a destination, 2 operands, and 2 registers to clear (mimicking the side effects of the original loop):
        step (is, cpu) (Mul dest op1 op2 c1 c2)  = (is, (set c2 0 (set c1 0 (set dest (get op1 cpu * get op2 cpu) cpu))){iptr = ip + 1})
        step (is, cpu) _                         = (is, cpu{iptr = ip + 1}) -- Either a Nop or just plain nonsense from Tgl-ing

solve :: Int -> Either a [Instruction] -> Int
solve a (Right instrs) = regA $ exec (is, CPU{regA = a, regB = 0, regC = 0, regD = 0, iptr = 0})
  where is = listArray (0, length instrs - 1) . patchMul $ instrs


main :: IO ()
main = interact $ show . (\(a:ls) -> solve (read a) . mapM (runParser instruction "<stdin>") $ ls) . lines

module Main where
import Data.List (group, sort, sortOn, transpose)

--	... in each column and choose the least common letter to reconstruct the original message.
--
--	In the above example, the least common character in the first column is a; in the second, d, and so
--	on. Repeating this process for the remaining characters produces the original message, advent.
--
--	Given the recording in your puzzle input and this new decoding methodology, what is the original
--	message that Santa is trying to send?
--
-- Test:
--	% cd ../.. && echo -e 'eedadn\ndrvtee\neandsr\nraavrd\natevrs\ntsrnev\nsdttsa\nrasrtv\nnssdts\nntnada\nsvetve\ntesnvt\nvntsnd\nvrdear\ndvrsen\nenarar' | cabal run 2016_day06-signals-and-noise_b # = "advent"

solve :: [String] -> String
solve = map (head . head . sortOn length . group . sort)


main :: IO ()
main = interact $ show . solve . transpose . lines

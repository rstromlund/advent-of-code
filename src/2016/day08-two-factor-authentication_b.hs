module Main where
import Data.Bool (bool)
import Data.Char (isDigit)
import Data.List as L (break, map)
import Data.Set as S (Set, empty, insert, map, member, size)

--	You notice that the screen is only capable of displaying capital letters; in the font it uses, each letter is 5 pixels wide and 6 tall.
--
--	After you swipe your card, what code is the screen trying to display?
--
-- Test:
--	% cd ../.. && echo -e 'rect 3x2\nrotate column x=1 by 1\nrotate row y=0 by 4\nrotate column x=1 by 1' | cabal run 2016_day08-two-factor-authentication_b # = <visual of the screen>

screenW = 50 :: Int
screenH =  6 :: Int

type Point = (Int, Int) -- x,y
type Screen = Set Point

screen :: Screen
screen = empty

rotRow :: Screen -> Int -> Int -> Screen
rotRow scr y l = S.map mv scr
  where mv (x0, y0)
          | y == y0   = ((x0 + l) `mod` screenW, y0)
          | otherwise = (x0, y0)

rotCol :: Screen -> Int -> Int -> Screen
rotCol scr x l = S.map mv scr
  where mv (x0, y0)
          | x == x0   = (x0, (y0 + l) `mod` screenH)
          | otherwise = (x0, y0)

rect :: Screen -> Int -> Int -> Screen
rect scr w h = foldl' (\scr' x -> foldl' (\scr'' y -> insert (x, y) scr'') scr' [0..h-1]) scr [0..w-1]

showScr :: Screen -> [String]
showScr scr = L.map (\y -> L.map (\x -> bool '.' '#' $ (x, y) `member` scr) [0..screenW]) [0..screenH]

solve :: [String] -> [String]
solve = showScr . foldl' action screen . L.map words
  where action scr ["rotate", "row", y, "by", l]    = rotRow scr (read . filter isDigit $ y) (read l)
        action scr ["rotate", "column", x, "by", l] = rotCol scr (read . filter isDigit $ x) (read l)
        action scr ["rect", wh]                     = let (ws,hs) = break ('x' ==) wh
                                                      in rect scr (read ws) (read .tail $ hs)


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.Bool (bool)
import Data.List (intersect)

--	... An IP supports SSL if it has an Area-Broadcast Accessor, or ABA, anywhere in the supernet
--	sequences (outside any square bracketed sections), and a corresponding Byte Allocation Block, or BAB,
--	anywhere in the hypernet sequences. An ABA is any three-character sequence which consists of the same
--	character twice with a different character between them, such as xyx or aba. A corresponding BAB is
--	the same characters but in reversed positions: yxy and bab, respectively.
--
--	For example:
--
--	* aba[bab]xyz supports SSL (aba outside square brackets with corresponding bab within square brackets).
--	* xyx[xyx]xyx does not support SSL (xyx, but no corresponding yxy).
--	* aaa[kek]eke supports SSL (eke in supernet with corresponding kek in hypernet; the aaa sequence is not related, because the interior character must be different).
--	* zazbz[bzb]cdb supports SSL (zaz has no corresponding aza, but zbz has a corresponding bzb, even though zaz and zbz overlap).
--
--	How many IPs in your puzzle input support SSL?
--
-- Test:
--	% cd ../.. && echo -e 'aba[bab]xyz\nxyx[xyx]xyx\naaa[kek]eke\nzazbz[bzb]cdb' | cabal run 2016_day07-internet-protocol-version-7_b # = 3

solve :: [String] -> Int
solve = sum . map (cnt False [] [])
  where cnt :: Bool -> [String] -> [String] -> String -> Int
        cnt False ss hs [_,_]    = bool 1 0 $ null (ss `intersect` hs)
        --
        cnt False ss hs (_:'[':cs) = cnt True  ss hs cs -- stop things like "x[x" looking like ABA
        cnt False ss hs ('[':cs)   = cnt True  ss hs cs
        cnt True  ss hs (_:']':cs) = cnt False ss hs cs -- stop things like "y]y" looking like BAB
        cnt True  ss hs (']':cs)   = cnt False ss hs cs
        --
        cnt False ss hs (o1:cs@(i1:o2:_))
          | o1 == o2 && o1 /= i1 = cnt False ([o1,i1]:ss) hs cs
          | otherwise            = cnt False ss hs cs
        cnt True ss hs (o1:cs@(i1:o2:_))
          | o1 == o2 && o1 /= i1 = cnt True ss ([i1,o1]:hs) cs
          | otherwise            = cnt True ss hs cs


main :: IO ()
main = interact $ show . solve . lines

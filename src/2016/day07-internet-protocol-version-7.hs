module Main where

--	... An ABBA is any four-character sequence which consists of a pair of two different characters
--	followed by the reverse of that pair, such as xyyx or abba. However, the IP also must not have an ABBA
--	within any hypernet sequences, which are contained by square brackets.
--
--	For example:
--
--	* abba[mnop]qrst supports TLS (abba outside square brackets).
--	* abcd[bddb]xyyx does not support TLS (bddb is within square brackets, even though xyyx is outside square brackets).
--	* aaaa[qwer]tyui does not support TLS (aaaa is invalid; the interior characters must be different).
--	* ioxxoj[asdfgh]zxcvbn supports TLS (oxxo is outside square brackets, even though it's within a larger string).
--
--	How many IPs in your puzzle input support TLS?
--
-- Test:
--	% cd ../.. && echo -e 'abba[mnop]qrst\nabcd[bddb]xyyx\naaaa[qwer]tyui\nioxxoj[asdfgh]zxcvbn' | cabal run 2016_day07-internet-protocol-version-7 # = 2

solve :: [String] -> Int
solve = sum . map (cnt 0 False)
  where cnt :: Int -> Bool -> String -> Int
        cnt tls False [_,_,_]  = tls
        cnt tls False ('[':cs) = cnt tls True  cs
        cnt tls True (']':cs)  = cnt tls False cs
        cnt tls False (o1:cs@(i1:i2:o2:_))
          | o1 == o2 && i1 == i2 && o1 /= i1 = cnt 1   False cs
          | otherwise                        = cnt tls False cs
        cnt tls True (o1:cs@(i1:i2:o2:_))
          | o1 == o2 && i1 == i2 && o1 /= i1 = 0 -- an ABBA inside brackets stops the whole search
          | otherwise                        = cnt tls True cs


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.Char (isDigit)
import Data.List as L (break, map)
import Data.Set as S (Set, empty, insert, map, size)

--	... The screen is 50 pixels wide and 6 pixels tall, all of which start off, and is capable of three somewhat peculiar operations:
--
--	* rect AxB turns on all of the pixels in a rectangle at the top-left of the screen which is A wide and B tall.
--	* rotate row y=A by B shifts all of the pixels in row A (0 is the top row) right by B pixels. Pixels that would fall off the right end appear at the left end of the row.
--	* rotate column x=A by B shifts all of the pixels in column A (0 is the left column) down by B pixels. Pixels that would fall off the bottom appear at the top of the column.
--
--	For example, here is a simple sequence on a smaller screen:
--
--	rect 3x2 creates a small rectangle in the top-left corner:
--
--	###....
--	###....
--	.......
--
--	rotate column x=1 by 1 rotates the second column down by one pixel:
--
--	#.#....
--	###....
--	.#.....
--
--	rotate row y=0 by 4 rotates the top row right by four pixels:
--
--	....#.#
--	###....
--	.#.....
--
--	rotate column x=1 by 1 again rotates the second column down by one pixel, causing the bottom pixel to wrap back to the top:
--
--	.#..#.#
--	#.#....
--	.#.....
--
--	As you can see, this display technology is extremely powerful, and will soon dominate the
--	tiny-code-displaying-screen market. That's what the advertisement on the back of the display tries to
--	convince you, anyway.
--
--	There seems to be an intermediate check of the voltage used by the display: after you swipe your card, if the screen did work, how many pixels should be lit?
--
-- Test:
--	% cd ../.. && echo -e 'rect 3x2\nrotate column x=1 by 1\nrotate row y=0 by 4\nrotate column x=1 by 1' | cabal run 2016_day08-two-factor-authentication # = 6

screenW = 50 :: Int
screenH =  6 :: Int

type Point = (Int, Int) -- x,y
type Screen = Set Point

screen :: Screen
screen = empty

rotRow :: Screen -> Int -> Int -> Screen
rotRow scr y l = S.map mv scr
  where mv (x0, y0)
          | y == y0   = ((x0 + l) `mod` screenW, y0)
          | otherwise = (x0, y0)

rotCol :: Screen -> Int -> Int -> Screen
rotCol scr x l = S.map mv scr
  where mv (x0, y0)
          | x == x0   = (x0, (y0 + l) `mod` screenH)
          | otherwise = (x0, y0)

rect :: Screen -> Int -> Int -> Screen
rect scr w h = foldl' (\scr' x -> foldl' (\scr'' y -> insert (x, y) scr'') scr' [0..h-1]) scr [0..w-1]

solve :: [String] -> Int
solve = size . foldl' action screen . L.map words
  where action scr ["rotate", "row", y, "by", l]    = rotRow scr (read . filter isDigit $ y) (read l)
        action scr ["rotate", "column", x, "by", l] = rotCol scr (read . filter isDigit $ x) (read l)
        action scr ["rect", wh]                     = let (ws,hs) = break ('x' ==) wh
                                                      in rect scr (read ws) (read .tail $ hs)


main :: IO ()
main = interact $ show . solve . lines

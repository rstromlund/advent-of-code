module Main where
import Algorithm.Search (dijkstra, pruning)
import Data.Char (isDigit)
import Data.Array (Array, assocs, bounds, inRange, listArray, (!))
import Data.List (isPrefixOf, nub, permutations, sort, sortOn, tails)
import Data.Map.Strict (Map, (!?), fromList)
import Data.Set (Set, deleteFindMin, empty, insert, member, notMember, singleton)
import Data.Tuple (swap)

--	...
--
-- Test:
--	% cd ../.. && echo -e '###########\n#0.1.....2#\n#.#######.#\n#4.......3#\n###########' | cabal run 2016_day24-air-duct-spelunking # = 20

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way

mkGrid :: [String] -> Array Point Char
mkGrid cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

manhattanDist :: Point -> Point -> Int
manhattanDist (y1, x1) (y2, x2) = abs (y2 - y1) + abs (x2 - x1)

cardinalDirections :: [Point]
cardinalDirections = [
  (-1,  0),
  ( 1,  0),
  ( 0, -1),
  ( 0,  1)
  ]

solve :: Array Point Char -> Int
solve hmap = minimum . map sol . permutations . tail . map fst $ dpts -- ["01234","10234",...]
  where bnds  = bounds hmap
        dpts  = map swap . sortOn snd . filter (isDigit . snd) . assocs $ hmap -- [('0',(1,1)),('1',(1,3)),...]
        cnxs  = nub . map (sort . take 2) . permutations $ dpts -- e.g. [[('0',(1,1)),('1',(1,3))],[('1',(1,3)),('2',(1,9))],...]
        dists = fromList . map getDist $ cnxs -- e.g. fromList [(('0','1'),2),(('0','2'),8),...]
        --
        getDist [(d1, p1), (d2, p2)] =
          let Just (cost, _) = dijkstra (neighbors `pruning` isWall) manhattanDist (p2 ==) p1
          in ((d1, d2), cost)
        --
        -- Upto 4 neighbors per visit point.
        neighbors :: Point -> [Point]
        neighbors (y, x) =
          [ neighbor
          | (dy, dx) <- cardinalDirections
          , let neighbor = (y + dy, x + dx)
          , inRange bnds neighbor
          ]
        --
        isWall :: Point -> Bool
        isWall pt = '#' == (hmap ! pt)
        --
        -- Above uses dijkstra to find distances between all digits/points; below we brute force all combinations
        -- to find minimum combination.  Maybe anther application of dijkstra (or a*) would be better? : )
        sol cs = sum . map sol' . tails $ ("0" ++ cs ++ "0")
        sol' (d1:d2:_) = let pair = (min d1 d2, max d1 d2)
                             Just d = dists !? pair
                         in d
        sol' _         = 0


main :: IO ()
main = interact $ show . solve . mkGrid . lines

module Main where

--	... a new disc with 11 positions and starting at position 0 has appeared exactly one second below the previously-bottom disc.
--
--	With this new disc, and counting again starting from time=0 with the configuration in your puzzle input, what is the first time you
--
-- Test:
--	% cd ../.. && echo -e 'Disc #1 has 5 positions; at time=0, it is at position 4.\nDisc #2 has 2 positions; at time=0, it is at position 1.' | cabal run 2016_day15-timing-is-everything_b # = ??

type Disk = (Int, Int, Int) -- disk #, tot positions, at time=0 position

parse :: [String] -> Disk
parse ["Disc", dNbrStr, "has", totPosStr, "positions;", "at", "time=0,", "it", "is", "at", "position", time0PosStr] = (read . tail $ dNbrStr, read totPosStr, read . init $ time0PosStr)

align :: Int -> Disk -> Bool
align t (n, tot, t0) = 0 == (t + n + t0) `mod` tot

solve :: [Disk] -> Int
solve ds' = fst . head . filter snd . map (\t -> (t, all (align t) ds)) $ [try0, (try0 + tot_1) ..]
  where ds = ds' ++ [(length ds' + 1, 11, 0)]
        ((n_1, tot_1, t0_1):_) = ds'
        try0 = (tot_1 - (t0_1 + n_1)) `mod` tot_1


main :: IO ()
main = interact $ show . solve . map (parse . words) . lines

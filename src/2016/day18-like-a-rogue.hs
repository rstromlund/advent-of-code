module Main where
import Debug.Trace (trace)
import Data.List (isPrefixOf, tails)

-- Part 1:
--	... suppose you know the first row (with tiles marked by letters) and want to determine the next row (with tiles marked by numbers):
--
--	ABCDE
--	12345
--
--	The type of tile 2 is based on the types of tiles A, B, and C; the type of tile 5 is based on tiles D, E, and an imaginary "safe" tile. Let's call these three tiles from the previous row the left, center, and right tiles, respectively. Then, a new tile is a trap only in one of the following situations:
--
--	* Its left and center tiles are traps, but its right tile is not.
--	* Its center and right tiles are traps, but its left tile is not.
--	* Only its left tile is a trap.
--	* Only its right tile is a trap.
--
--	In any other situation, the new tile is safe.
--
--	Then, starting with the row ..^^., you can determine the next row by applying those rules to each new tile:
--
--	* The leftmost character on the next row considers the left (nonexistent, so we assume "safe"), center
--	  (the first ., which means "safe"), and right (the second ., also "safe") tiles on the previous
--	  row. Because all of the trap rules require a trap in at least one of the previous three tiles, the
--	  first tile on this new row is also safe, ..
--	* The second character on the next row considers its left (.), center (.), and right (^) tiles from
--	  the previous row. This matches the fourth rule: only the right tile is a trap. Therefore, the next
--	  tile in this new row is a trap, ^.
--	* The third character considers .^^, which matches the second trap rule: its center and right tiles
--	  are traps, but its left tile is not. Therefore, this tile is also a trap, ^.
--	* The last two characters in this new row match the first and third rules, respectively, and so they
--	  are both also traps, ^.
--
--	After these steps, we now know the next row of tiles in the room: .^^^^. Then, we continue on to the
--	next row, using the same rules, and get ^^..^. After determining two new rows, our map looks like
--	this:
--
--	..^^.
--	.^^^^
--	^^..^
--
--	Here's a larger example with ten tiles per row and ten rows:
--
--	.^^.^.^^^^
--	^^^...^..^
--	^.^^.^.^^.
--	..^^...^^^
--	.^^^^.^^.^
--	^^..^.^^..
--	^^^^..^^^.
--	^..^^^^.^^
--	.^^^..^.^^
--	^^.^^^..^^
--
--	In ten rows, this larger example has 38 safe tiles.
--
--	Starting with the map in your puzzle input, in a total of 40 rows (including the starting row), how many safe tiles are there?
--
-- Part 2:
--	How many safe tiles are there in a total of 400000 rows?
--
-- Test:
--	% cd ../.. && echo -e '3 ..^^.' | cabal run 2016_day18-like-a-rogue # = 6
--	% cd ../.. && echo -e '10 .^^.^.^^^^' | cabal run 2016_day18-like-a-rogue # = 38

score = length . filter (== '.')

mkRow :: Int -> (String, Int) -> Int -> (String, Int)
mkRow isl (is, safeCnt) _ = (is', score is' + safeCnt)
  where is' = map mkCol . take isl . tails $ ('.':is)
        mkCol is
          |
            "^^." `isPrefixOf` is ||
            ".^^" `isPrefixOf` is ||
            "^.." `isPrefixOf` is ||
            "..^" `isPrefixOf` is ||
            "^^" == is ||
            "^." == is = '^'
          | otherwise  = '.'

solve :: [String] -> Int
solve [rs, is] = snd . foldl' (mkRow $ length is) (is, score is) $ [1..read rs - 1]


main :: IO ()
main = interact $ show . solve . words

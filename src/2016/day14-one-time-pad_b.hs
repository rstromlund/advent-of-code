module Main where
import Crypto.Hash.MD5 (hash)
import Data.ByteString (ByteString)
import Data.ByteString.Builder (byteStringHex)
import Data.ByteString.Builder.Extra (untrimmedStrategy, toLazyByteStringWith)
import Data.ByteString.Char8 (pack, unpack)
import Data.ByteString.Lazy (empty, toStrict)
import Data.List (group, isPrefixOf, tails)

--	... To implement key stretching, whenever you generate a hash, before you use it, you first find the
--	MD5 hash of that hash, then the MD5 hash of that hash, and so on, a total of 2016 additional
--	hashings. Always use lowercase hexadecimal representations of hashes.

keysToMath =   64
searchSize = 1000

md5hash :: ByteString -> ByteString
md5hash = toStrict
        . toLazyByteStringWith (untrimmedStrategy 32 32) empty
        . byteStringHex
        . hash

md52016 :: ByteString -> ByteString
md52016 = (!! 2017) . iterate md5hash

getThreepeat :: ByteString -> String
getThreepeat = safeHead . filter ((>= 3) . length) . group . unpack . md52016
  where safeHead []    = ""
        safeHead (s:_) = s

search :: [String] -> [Int]
search hashes = [ n | (n, str3peat:hs) <- zip [0..] $ tails hashes
                    , str3peat /= ""
                    , let str5peat = replicate 5 . head $ str3peat
                    , any (str5peat `isPrefixOf`) . take searchSize $ hs
                    ]

solve :: String -> Int
solve salt = (!! (keysToMath - 1)) . search . map (getThreepeat . (salt' <>) . pack . show) $ [0..]
  where salt' = pack salt


main :: IO ()
main = interact $ show . solve . head . words

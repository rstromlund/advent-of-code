module Main where
import Debug.Trace (trace)
import Data.Bifunctor (second)
import Data.Bits ((.&.), (.|.), complement, shift, xor)
import Data.Bool (bool)
import Data.Functor (($>))
import Data.List ((\\), map, nub, permutations, take)
import Data.Void (Void)
import Data.Word (Word16, Word64)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)

--	... For example, suppose the isolated area has the following arrangement:
--
--	The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.
--	The second floor contains a hydrogen generator.
--	The third floor contains a lithium generator.
--	The fourth floor contains nothing relevant.
--
-- Test:
--	% cd ../.. && echo -e 'The first floor contains a hydrogen-compatible microchip and a lithium-compatible microchip.\nThe second floor contains a hydrogen generator.\nThe third floor contains a lithium generator.\nThe fourth floor contains nothing relevant.' | cabal run 2016_day11-radioisotope-thermoelectric-generators # = 11

iStrontium = 0x01 :: Word16
iPlutonium = 0x02 :: Word16
iThulium   = 0x04 :: Word16
iRuthenium = 0x08 :: Word16
iCurium    = 0x10 :: Word16

type Object  = Word16 -- 0xMMGG => top 8 bits mapped to microchips, lower 8 bits mapped to generators

type Floor  = Word16
type Floors = Word64

iChipMask      = 0xFF00 :: Word16
iChipShift     = 8
iGeneratorMask = 0x00FF :: Word16

iFloorMask     = 0xFFFF :: Word64
iFloorShift    = 16

iEmptyFloor    = 0 :: Floor
iEmptyFloors   = 0 :: Floors

elements    = [iStrontium,  iPlutonium,  iThulium,  iRuthenium,  iCurium]
elementsStr = ["strontium", "plutonium", "thulium", "ruthenium", "curium"]
elementMap  = zip elementsStr elements
allelements = elements ++ map (`shift` iChipShift) elements

topFloor = 3 :: Int

type Parser = Parsec Void String

setFloor :: Int -> Floor -> Floors -> Floors
setFloor i _ _ | i < 0 || i > topFloor = error ("setFloor -- No floor: " ++ show i)
setFloor i f fs = complement (iFloorMask `shift` shftCnt) .&. fs .|. (fromIntegral f :: Floors) `shift` shftCnt
  where shftCnt = i * iFloorShift

getFloor :: Int -> Floors -> Floor
getFloor i _ | i < 0 || i > topFloor = error ("getFloor -- No floor: " ++ show i)
getFloor i fs = fromIntegral ((iFloorMask `shift` shftCnt .&. fs) `shift` (- shftCnt)) :: Floor
  where shftCnt = i * iFloorShift

floors :: Parser Floors
floors = mkFloors <$> many floor_ <* eof
  where mkFloors oss = foldl' (\fs i -> setFloor i (oss !! i) fs) iEmptyFloors [0..topFloor]

floor_ :: Parser Floor
floor_ = mkFloor <$> (string "The " *> some letterChar *> string " floor contains" *> ((string " nothing relevant" $> [0]) <|> many object) <* char '.' <* newline)
  where mkFloor = foldl' (.|.) 0

object :: Parser Object
object =
  mkObject <$> (optional (char ',') *> char ' ' *> optional (string "and ") *> string "a " *> (mkElement <$> some letterChar)) <*> (string " generator" <|> string "-compatible microchip")
  where
    mkElement "hydrogen" = iStrontium -- hydrogen and lithium are only used in the test example, not my input.
    mkElement "lithium"  = iPlutonium
    mkElement e = let Just i = lookup e elementMap in i
    --
    mkObject e " generator"            = e
    mkObject e "-compatible microchip" = e `shift` iChipShift

type GameState = (Int, Floors) -- elevator, floors/objects

ppFloor :: Floor -> String
ppFloor os = concatMap(\(nm, i) ->
                          bool "" (nm ++ " generator ") (0 /= os .&. i) ++
                          bool "" (nm ++ " chip ")      (0 /= os `shift` (- iChipShift) .&. i)) elementMap

ppGS (e, fs) = "elevator " ++ show e ++ concatMap (\i -> ", floor " ++ show i ++ ": " ++ ppFloor (getFloor i fs)) [0..topFloor]

-- Any chip on a floor with generators *must* have its protector generator
vfloor :: Floor -> Bool
vfloor os = nogen || allMatch
  where nogen    = 0 == (os .&. iGeneratorMask)
        chps     = os `shift` (- iChipShift)
        allMatch = chps == (chps .&. os)

validate :: Floors -> Bool
validate fs = all (\i -> vfloor . getFloor i $ fs) [0..topFloor]

generateCandidates :: Floor -> ([Floor], [Floor]) -- (multiple mv's, single mv's)
generateCandidates flr = (os2, os1)
  where os1 = filter (/= 0) . map (flr .&.) $ allelements -- all elements moving by themself
        cs = nub . map (foldl' (.|.) 0 . take 2) . permutations $ os1
        ps = filter paired cs
        os2 = cs \\ safeTail ps -- multiple items to move (but only 1 generator+chip pair)
        paired os1 = let c = os1 `shift` (- iChipShift)
                     in os1 > iGeneratorMask && c == c .&. os1
        safeTail []    = []
        safeTail (_:t) = t

{-
Canonicalize each game state:
   ALL PAIRS ARE INTERCHANGEABLE - The following two states are EQUIVALENT:
      (HGen@floor0, HChip@floor1, LGen@floor2, LChip@floor2),
      (LGen@floor0, LChip@floor1, HGen@floor2, HChip@floor2)
   - prune any state EQUIVALENT TO (not just exactly equal to) a state you have already seen!
-}

canonicalize fs = fs'
  where fsos :: [[Object]]
        fsos   = map (\i -> let flr = getFloor i fs in filter (/= 0) . map (flr .&.) $ allelements) [0..topFloor]
        osmap  = zip (filter (<= iGeneratorMask) . concat $ fsos) elements
        --
        newmap = map (map xlate) fsos
        xlate o
          | o <= iGeneratorMask = let Just o' = lookup o osmap in o'
          | otherwise           = let Just o' = lookup (o `shift` (- iChipShift)) osmap in (o' `shift` iChipShift)
        --
        fs' = foldl' (\acc (i, flr) -> setFloor i flr acc) iEmptyFloors . zipWith (\i os -> (i, foldl' (.|.) iEmptyFloor os)) [0..topFloor] $ newmap

genStates :: GameState -> [GameState]
genStates (e0, fs) = validgs
  where (mcs, scs) = generateCandidates (getFloor e0 fs) -- multi object, and single object move candidates
        upgs' = filter (validate . snd) . foldl' (mv (e0 + 1)) [] $ mcs
        upgs | null upgs' = filter (validate . snd) . foldl' (mv (e0 + 1)) [] $ scs
             | otherwise  = upgs' -- If you can move 2 objects up, then skip moving 1 object
        dngs' = filter (validate . snd) . foldl' (mv (e0 - 1)) [] $ scs
        dngs | null dngs' = filter (validate . snd) . foldl' (mv (e0 - 1)) [] $ mcs
             | otherwise  = dngs' -- If you can move 1 object down, then skip moving 2 objects
        --
        validgs = nub . map (second canonicalize) $ upgs ++ dngs
        --
        mv :: Int -> [GameState] -> Floor -> [GameState]
        mv e1 acc _
          -- Don't check impossible elevator floors
          | e1 < 0 || e1 > topFloor = acc
          -- If this floor and all the floors below you are empty, don't move anything down (we're moving on up)
          | all ((0 ==) . flip getFloor fs) [0..e1] = acc
        --
        mv e1 acc os =
          let fe0 = getFloor e0 fs `xor` os
              fe1 = getFloor e1 fs .|. os
          in (e1, setFloor e1 fe1 . setFloor e0 fe0 $ fs):acc

search :: [GameState] -> [GameState]
search = nub . concatMap genStates

winner :: GameState -> Bool
winner (_, fs) = 0 == complement (iFloorMask `shift` (topFloor * iFloorShift)) .&. fs && 0 /= getFloor topFloor fs

solve :: Either a Floors -> Int
solve (Right fs) = untilWinner 0 [] [(0, canonicalize fs)]
  where untilWinner stps pastgs gs
          | any winner gs = trace "!" stps
          | otherwise     = trace (show (".", "pastgs", length pastgs, "gs", length gs)) $
                            untilWinner (stps + 1) (nub $ pastgs ++ gs) . search $ gs \\ pastgs


main :: IO ()
main = interact $ show . solve . runParser floors "<stdin>"

module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser)
import Text.Megaparsec.Char (char, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... count the number of viable pairs of nodes. A viable pair is any two nodes (A,B), regardless of whether they are directly connected, such that:
--
--	Node A is not empty (its Used is not zero).
--	Nodes A and B are not the same node.
--	The data on node A (its Used) would fit on node B (its Avail).
--
--	How many viable pairs of nodes are there?
--
-- Test:
--	% cd ../.. && echo -e 'root@ebhq-gridcenter# df -h\nFilesystem              Size  Used  Avail  Use%\n/dev/grid/node-x0-y0     85T    0T    13T   84%\n/dev/grid/node-x0-y1     89T   73T    16T   82%\n/dev/grid/node-x0-y2     85T   68T    17T   80%\n/dev/grid/node-x0-y3     90T   66T    99T   73%' | cabal run 2016_day22-grid-computing # = 2

type Point = (Int, Int) -- x, y

data Node = Node {used, avail :: !Int} deriving (Show, Eq, Ord)

-- e.g. /dev/grid/node-x0-y0     85T   72T    13T   84%

type Parser = Parsec Void String

node :: Parser (Point, Node)
node = do
  x      <- string "/dev/grid/node-x" *> decimal
  y      <- string "-y" *> decimal <* space
  _size  <- decimal <* char 'T' <* space
  used   <- decimal <* char 'T' <* space
  avail  <- decimal <* char 'T' <* space
  _pcnt  <- decimal <* char '%' <* eof
  return ((x, y), Node used avail)

solve :: Either a [(Point, Node)] -> Int
solve (Right ns) = length pairs
  where pairs = [
          (na, nb) | na@(apt, Node{used = au}) <- ns
                   , au /= 0
                   , nb@(bpt, Node{avail = ba}) <- ns
                   , apt /= bpt
                   , au <= ba
          ]


main :: IO ()
main = interact $ show . solve . mapM (runParser node "<stdin>") . drop 2 . lines

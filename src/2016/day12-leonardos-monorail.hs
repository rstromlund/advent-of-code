module Main where
import Data.Array (Array, bounds, listArray, (!))
import Data.Bool (bool)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The assembunny code you've extracted operates on four registers (a, b, c, and d) that start at 0
--	and can hold any integer. However, it seems to make use of only a few instructions:
--
--	* cpy x y copies x (either an integer or the value of a register) into register y.
--	* inc x increases the value of register x by one.
--	* dec x decreases the value of register x by one.
--	* jnz x y jumps to an instruction y away (positive means forward; negative means backward), but only if x is not zero.
--
--	The jnz instruction moves relative to itself: an offset of -1 would continue at the previous instruction, while an offset of 2 would skip over the next instruction.
--
--	For example:
--	cpy 41 a
--	inc a
--	inc a
--	dec a
--	jnz a 2
--	dec a
--
--	The above code would set register a to 41, increase its value by 2, decrease its value by 1, and then
--	skip the last dec a (because a is not zero, so the jnz a 2 skips it), leaving register a at 42. When
--	you move past the last instruction, the program halts.
--
--	After executing the assembunny code in your puzzle input, what value is left in register a?
--
-- Test:
--	% cd ../.. && echo -e 'cpy 41 a\ninc a\ninc a\ndec a\njnz a 2\ndec a' | cabal run 2016_day12-leonardos-monorail # = 42

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Copy Memory Memory
                 | Inc Memory
                 | Dec Memory
                 | JmpNot0 Memory Int
                 deriving (Show, Eq, Ord)

data CPU = CPU { regA :: Int
               , regB :: Int
               , regC :: Int
               , regD :: Int
               , iptr :: Int
               } deriving (Show)

type Parser = Parsec Void String

instructions :: Parser (Array Int Instruction)
instructions = mkra <$> many instruction <* eof
  where mkra :: [Instruction] -> Array Int Instruction
        mkra is = listArray (0, length is - 1) is

instruction :: Parser Instruction
instruction = (
  string "cpy " *> mkCopy
  <|> string "inc " *> mkInc
  <|> string "dec " *> mkDec
  <|> string "jnz " *> mkJmpNot0
  ) <* newline

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

mkCopy :: Parser Instruction
mkCopy = Copy <$> (mkMemory <* space) <*> mkMemory

mkInc :: Parser Instruction
mkInc = Inc <$> mkMemory

mkDec :: Parser Instruction
mkDec = Dec <$> mkMemory

mkJmpNot0 :: Parser Instruction
mkJmpNot0 = JmpNot0 <$> (mkMemory <* space) <*> signed

solve :: Either a (Array Int Instruction) -> Int
solve (Right instrs) = regA $ exec CPU{regA = 0, regB = 0, regC = 0, regD = 0, iptr = 0}
  where (imin, imax) = bounds instrs
        get (Constant i)   = const i
        get (Register 'a') = regA
        get (Register 'b') = regB
        get (Register 'c') = regC
        get (Register 'd') = regD
        set (Register 'a') r cpu = cpu{regA = r}
        set (Register 'b') r cpu = cpu{regB = r}
        set (Register 'c') r cpu = cpu{regC = r}
        set (Register 'd') r cpu = cpu{regD = r}
        exec :: CPU -> CPU
        exec cpu@CPU{iptr = ip}
          | ip > imax = cpu
          | otherwise = exec . exec' $ instrs ! ip
          where exec' :: Instruction -> CPU
                exec' (Copy m1 m2)     = (set m2 (get m1 cpu) cpu){iptr = ip + 1}
                exec' (Inc m1)         = (set m1 (get m1 cpu + 1) cpu){iptr = ip + 1}
                exec' (Dec m1)         = (set m1 (get m1 cpu - 1) cpu){iptr = ip + 1}
                exec' (JmpNot0 m1 off) = let i = get m1 cpu in cpu{iptr = ip + bool 1 off (0 /= i)}


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

module Main where

--	... but... 5 10 25? Some of these aren't triangles. You can't help but mark the impossible ones.
--
--	In a valid triangle, the sum of any two sides must be larger than the remaining side. For example, the
--	"triangle" given above is impossible, because 5 + 10 is not larger than 25.
--
--	In your puzzle input, how many of the listed triangles are possible?
--
-- Test:
--	% cd ../.. && echo -e '5 10 25\n    4   21  894' | cabal run 2016_day03-squares-with-three-sides # = 0

maybeTriangle :: [Int] -> Bool
maybeTriangle [x, y, z] = (x + y > z) && (x + z > y) && (y + z > x)


main :: IO ()
main = interact $ show . length . filter id . map (maybeTriangle . map read . words) . lines

module Main where
import Data.Bits (popCount)
import Data.Bool (bool)
import Data.List as L ()
import Data.Map.Strict as M (Map, (!?), empty, insert)
import Data.Set as S (Set, deleteFindMin, empty, insert, member, null, singleton)

--	... You can determine whether a given x,y coordinate will be a wall or an open space using a simple system:
--
--	* Find x*x + 3*x + 2*x*y + y + y*y.
--	* Add the office designer's favorite number (your puzzle input).
--	* Find the binary representation of that sum; count the number of bits that are 1.
--	  * If the number of bits that are 1 is even, it's an open space.
--	  * If the number of bits that are 1 is odd, it's a wall.
--
--	For example, if the office designer's favorite number were 10, drawing walls as # and open spaces as
--	., the corner of the building containing 0,0 would look like this:
--
--	  0123456789
--	0 .#.####.##
--	1 ..#..#...#
--	2 #....##...
--	3 ###.#.###.
--	4 .##..#..#.
--	5 ..##....#.
--	6 #...##.###
--
--	Now, suppose you wanted to reach 7,4. The shortest route you could take is marked as O:
--
--	  0123456789
--	0 .#.####.##
--	1 .O#..#...#
--	2 #OOO.##...
--	3 ###O#.###.
--	4 .##OO#OO#.
--	5 ..##OOO.#.
--	6 #...##.###
--
--	Thus, reaching 7,4 would take a minimum of 11 steps (starting from your current location, 1,1).
--
--	What is the fewest number of steps required for you to reach 31,39?
--
-- Test:
--	% cd ../.. && echo -e '10' | cabal run 2016_day13-a-maze-of-twisty-little-cubicles # = 11

data Cell = Wall | Space deriving (Show, Eq, Ord)
type Point = (Int, Int)

spaceOrWall :: Int -> Point -> Cell
spaceOrWall f (x, y) = bool Space Wall (odd . popCount $ z)
  where z = x*x + 3*x + 2*x*y + y + y*y + f

getCell :: Int -> Point -> Map Point Cell -> (Cell, Map Point Cell)
getCell f pt@(x, y) office
  | x < 0 || y < 0 = (Wall, office)
  | otherwise = get (office !? pt)
  where get (Just c) = (c, office)
        get Nothing  = let c = spaceOrWall f pt in (c, M.insert pt c office)

pStartPoint     = (1, 1)
pTestEndPoint   = ( 7,  4)
pPuzzleEndPoint = (31, 39)

dirs = [( 0, -1),  -- N
        ( 0,  1),  -- S
        (-1,  0),  -- E
        ( 1,  0)]  -- W

solve :: Int -> Int
solve favNbr = dijkstra M.empty S.empty (singleton (0, pStartPoint))
  where pEndPoint = bool pPuzzleEndPoint pTestEndPoint $ 10 == favNbr
        dijkstra :: Map Point Cell -> Set Point -> Set (Int, Point) -> Int
        dijkstra office vps paths
          | S.null paths = error "Cannot find the ending point!"
          | otherwise    = nextStep nxtCell paths'
          where (nxtCell, paths') = deleteFindMin paths
                nextStep (s, pt@(x, y)) paths
                  | pEndPoint == pt = s -- Found our end point!
                  | pt `member` vps = dijkstra office vps paths -- already tried this point
                  | otherwise = let neighbors               = L.foldl' (\ps pt -> S.insert (s + 1, pt) ps) paths' adjcent
                                    (adjcent, office')      = L.foldl' valid ([], office) dirs -- adjcent cells that aren't walls (with an office map cache update)
                                    valid (vs, os) (dx, dy) = let pt' = (x + dx, y + dy)
                                                                  (c, os') = getCell favNbr pt' os
                                                              in bool (pt':vs, os') (vs, os') $ Wall == c
                                in dijkstra office' (S.insert pt vps) neighbors


main :: IO ()
main = interact $ show . solve . read

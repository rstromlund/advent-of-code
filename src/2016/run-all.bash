#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint .
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-no-time-for-a-taxicab.hs   < 01a.txt ; (( rc += ${?} ))
#time ./day01-no-time-for-a-taxicab_b.hs < 01a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-bathroom-security.hs   < 02a.txt ; (( rc += ${?} ))
#time ./day02-bathroom-security_b.hs < 02a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-squares-with-three-sides.hs   < 03a.txt ; (( rc += ${?} ))
#time ./day03-squares-with-three-sides_b.hs < 03a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-security-through-obscurity.hs   < 04a.txt ; (( rc += ${?} ))
#time ./day04-security-through-obscurity_b.hs < 04a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-how-about-a-nice-game-of-chess.hs   < 05a.txt ; (( rc += ${?} ))
#time ./day05-how-about-a-nice-game-of-chess_b.hs < 05a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-signals-and-noise.hs   < 06a.txt ; (( rc += ${?} ))
#time ./day06-signals-and-noise_b.hs < 06a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-internet-protocol-version-7.hs   < 07a.txt ; (( rc += ${?} ))
#time ./day07-internet-protocol-version-7_b.hs < 07a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-two-factor-authentication.hs   < 08a.txt ; (( rc += ${?} ))
#time ./day08-two-factor-authentication_b.hs < 08a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-explosives-in-cyberspace.hs   < 09a.txt ; (( rc += ${?} ))
#time ./day09-explosives-in-cyberspace_b.hs < 09a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-balance-bots.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-balance-bots_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-radioisotope-thermoelectric-generators.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-radioisotope-thermoelectric-generators_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-leonardos-monorail.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-leonardos-monorail_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-a-maze-of-twisty-little-cubicles.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-a-maze-of-twisty-little-cubicles_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-one-time-pad.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-one-time-pad_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-timing-is-everything.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-timing-is-everything_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-dragon-checksum.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-dragon-checksum_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-two-steps-forward.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-two-steps-forward_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time echo     "40 $(< 18a.txt)" | ./day18-like-a-rogue.hs ; (( rc += ${?} ))
#time echo "400000 $(< 18a.txt)" | ./day18-like-a-rogue.hs ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-an-elephant-named-joseph.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-an-elephant-named-joseph_b.hs < 19a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day20"
#time ./day20-firewall-rules.hs   < 20a.txt ; (( rc += ${?} ))
#time ./day20-firewall-rules_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-scrambled-letters-and-hash.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-scrambled-letters-and-hash_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-grid-computing.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-grid-computing_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time echo -e  "7\n$(< 23a.txt)" | ./day23-safe-cracking.hs ; (( rc += ${?} ))
#time echo -e "12\n$(< 23a.txt)" | ./day23-safe-cracking.hs ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-air-duct-spelunking.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-air-duct-spelunking_b.hs < 24a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-clock-signal.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

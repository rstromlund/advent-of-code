module Main where
import Data.List (group, sort, sortOn, transpose)

--	... All you need to do is figure out which character is most frequent for each position. For example, suppose you had recorded the following messages:
--
--	eedadn
--	drvtee
--	eandsr
--	raavrd
--	atevrs
--	tsrnev
--	sdttsa
--	rasrtv
--	nssdts
--	ntnada
--	svetve
--	tesnvt
--	vntsnd
--	vrdear
--	dvrsen
--	enarar
--
--	The most common character in the first column is e; in the second, a; in the third, s, and so on. Combining these characters returns the error-corrected message, easter.
--
--	Given the recording in your puzzle input, what is the error-corrected version of the message being sent?
--
-- Test:
--	% cd ../.. && echo -e 'eedadn\ndrvtee\neandsr\nraavrd\natevrs\ntsrnev\nsdttsa\nrasrtv\nnssdts\nntnada\nsvetve\ntesnvt\nvntsnd\nvrdear\ndvrsen\nenarar' | cabal run 2016_day06-signals-and-noise # = "easter"

-- Sort the letters, group, sort on group length, pick last group (the largest), take 1 letter from that group -> map this across all input lines for the answer
solve :: [String] -> String
solve = map (head . last . sortOn length . group . sort)


main :: IO ()
main = interact $ show . solve . transpose . lines

module Main where

--	... You picture a keypad like this:
--	1 2 3
--	4 5 6
--	7 8 9
--
--	Suppose your instructions are:
--
--	ULL
--	RRDDD
--	LURDL
--	UUUUD
--
--	* You start at "5" and move up (to "2"), left (to "1"), and left (you can't, and stay on "1"), so the first button is 1.
--	* Starting from the previous button ("1"), you move right twice (to "3") and then down three times
--		(stopping at "9" after two moves and ignoring the third), ending up with 9.
--	* Continuing from "9", you move left, up, right, down, and left, ending with 8.
--	* Finally, you move up four times (stopping at "2"), then down once, ending with 5.
--
--	So, in this example, the bathroom code is 1985.
--
--	Your puzzle input is the instructions from the document you found at the front desk. What is the bathroom code?
--
-- Test:
--	% cd ../.. && echo -e 'ULL\nRRDDD\nLURDL\nUUUUD' | cabal run 2016_day02-bathroom-security # = 1985

type Point = (Int, Int) -- X, Y

dMoveX 'U' = 0
dMoveX 'D' = 0
dMoveX 'L' = -1
dMoveX 'R' = 1

dMoveY 'U' = -1
dMoveY 'D' = 1
dMoveY 'L' = 0
dMoveY 'R' = 0

point5 = (1, 1)

move (x, y) c = (min (max (x + dMoveX c) 0) 2, min (max (y + dMoveY c) 0) 2)

solve :: [String] -> [Int]
solve cs = reverse . snd $ foldl' cmd (point5, []) cs
  where cmd (p, ans) ds =
          let p'@(x', y') = foldl' move p ds
          in (p', y' * 3 + x' + 1:ans)


main :: IO ()
main = interact $ show . solve . lines

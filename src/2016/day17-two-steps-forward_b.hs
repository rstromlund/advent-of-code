module Main where
import Crypto.Hash.MD5 (hash)
import Data.ByteString as B (ByteString, length)
import Data.ByteString.Builder (byteStringHex)
import Data.ByteString.Builder.Extra (untrimmedStrategy, toLazyByteStringWith)
import Data.ByteString.Char8 as C8 (empty, pack, singleton, unpack)
import Data.ByteString.Lazy as L (empty, toStrict)
import Data.List (partition)

--	... The doors in your current room are either open or closed (and locked) based on the hexadecimal MD5
--	hash of a passcode (your puzzle input) followed by a sequence of uppercase characters representing the
--	path you have taken so far (U for up, D for down, L for left, and R for right).
--
--	Only the first four characters of the hash are used; they represent, respectively, the doors up, down,
--	left, and right from your current position. Any b, c, d, e, or f means that the corresponding door is
--	open; any other character (any number or a) means that the corresponding door is closed and locked.
--
--	... For example, suppose the passcode is hijkl. Initially, you have taken no steps, and so your path
--	is empty: you simply find the MD5 hash of hijkl alone. The first four characters of this hash are
--	ced9, which indicate that up is open (c), down is open (e), left is open (d), and right is closed and
--	locked (9). Because you start in the top-left corner, there are no "up" or "left" doors to be open, so
--	your only choice is down.
--
--	Next, having gone only one step (down, or D), you find the hash of hijklD. This produces f2bc, which
--	indicates that you can go back up, left (but that's a wall), or right. Going right means hashing
--	hijklDR to get 5745 - all doors closed and locked. However, going up instead is worthwhile: even
--	though it returns you to the room you started in, your path would then be DU, opening a different set
--	of doors.
--
--	After going DU (and then hashing hijklDU to get 528e), only the right door is open; after going DUR,
--	all doors lock. (Fortunately, your actual passcode is not hijkl).
--
--	Passcodes actually used by Easter Bunny Vault Security do allow access to the vault if you know the
--	right path. For example:
--
--	* If your passcode were ihgpwlah, the shortest path would be DDRRRD.
--	* With kglvqrro, the shortest path would be DDUDRLRRUDRD.
--	* With ulqzkmiv, the shortest would be DRURDRUDDLLDLUURRDULRLDUUDDDRR.
--
--	Given your vault's passcode, what is the shortest path (the actual path, not just the length) to reach the vault?
--
-- Test:
--	% cd ../.. && echo -e 'ihgpwlah' | cabal run 2016_day17-two-steps-forward_b # = 370
--	% cd ../.. && echo -e 'kglvqrro' | cabal run 2016_day17-two-steps-forward_b # = 492
--	% cd ../.. && echo -e 'ulqzkmiv' | cabal run 2016_day17-two-steps-forward_b # = 830

type Point = (Int, Int) -- x, y

vaultX = 3
vaultY = 3

isOpen c = c >= 'b' && c <= 'f'

md5hash :: ByteString -> ByteString
md5hash = toStrict
        . toLazyByteStringWith (untrimmedStrategy 32 32) L.empty
        . byteStringHex
        . hash

genRs ibs ((x, y), path) = let hs = md5hash (ibs <> path)
                               (u:d:l:r:_) = unpack hs
                           in up u ++ dn d ++ lf l ++ rt r
  where mv c p (dx, dy)
          | isOpen c && x + dx >= 0 && y + dy >= 0 && x + dx <= vaultX && y + dy <= vaultY = [((x + dx, y + dy), path <> singleton p)]
          | otherwise = []
        up c = mv c 'U' ( 0, -1)
        dn c = mv c 'D' ( 0,  1)
        lf c = mv c 'L' (-1,  0)
        rt c = mv c 'R' ( 1,  0)

search :: ByteString -> [(Point, ByteString)] -> [(Point, ByteString)]
search ibs rs = next ([], rs)
  where next (ws, []) = ws
        next (ws, rs) = let (ws', rs') = partition (((vaultX, vaultY) ==) . fst) . concatMap (genRs ibs) $ rs
                        in next (ws ++ ws', rs')

solve :: ByteString -> Int
solve ibs = maximum . map (B.length . snd) . search ibs $ [((0, 0), C8.empty)]


main :: IO ()
main = interact $ show . solve . pack . head . words

module Main where
import Data.Word (Word8)
import Data.ByteString (unpack)
import Data.ByteString.UTF8 (fromString)
import Crypto.Hash.MD5 as MD5 (finalize, init, update)

--	... You still look for hashes that begin with five zeroes; however, now, the sixth character represents the position (0-7), and the seventh character is the character to put in that position.
--
--	A hash result of 000001f means that f is the second character in the password. Use only the first result for each position, and ignore invalid positions.
--
--	For example, if the Door ID is abc:
--
--	* The first interesting hash is from abc3231929, which produces 0000015...; so, 5 goes in position 1: _5______.
--	* In the previous method, 5017308 produced an interesting hash; however, it is ignored, because it specifies an invalid position (8).
--	* The second interesting hash is at index 5357525, which produces 000004e...; so, e goes in position 4: _5__e___.
--
--	You almost choke on your popcorn as the final character falls into place, producing the password 05ace8e3.
--
--	Given the actual Door ID and this new method, what is the password?
--
-- Test:
--	% cd ../.. && echo -e 'abc' | cabal run 2016_day05-how-about-a-nice-game-of-chess_b # = [0,5,10,12,14,8,14,3]

md5s :: String -> [Word8]
md5s str = unpack $ finalize ctx
  where ctx  = update ctx0 (fromString str)
        ctx0 = MD5.init

solve :: [String] -> [Int]
solve [doorid] = accum (replicate 8 (-1)) ps
  where ms = map (md5s . (doorid ++) . show) [0..]
        as = filter (\(i0:i1:i2:_) -> i0 == 0 && i1 == 0 && i2 < 0x10) ms
        ps = filter (\(p, _) -> p <= 7) . map (\ws -> let p = ws !! 2; c = (ws !! 3) `div` 16 in (fromIntegral p, fromIntegral c)) $ as
        -- You could probably sort on position and use assoc functions to pull 0..7 instead of `accum` here.  Better?  Or worse?
        accum :: [Int] -> [(Int, Int)] -> [Int]
        accum ns ((p, c):ts)
          | (-1) `notElem` ns = ns
          | -1 == (ns !! p)   = accum (let (ys,zs) = splitAt p ns in ys ++ (c:tail zs)) ts
          | otherwise         = accum ns ts


main :: IO ()
main = interact $ show . solve . words

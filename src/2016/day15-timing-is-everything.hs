module Main where

--	... For example, at time=0, suppose you see the following arrangement:
--
--	Disc #1 has 5 positions; at time=0, it is at position 4.
--	Disc #2 has 2 positions; at time=0, it is at position 1.
--
--	If you press the button exactly at time=0, the capsule would start to fall; it would reach the first
--	disc at time=1. Since the first disc was at position 4 at time=0, by time=1 it has ticked one position
--	forward. As a five-position disc, the next position is 0, and the capsule falls through the slot.
--
--	Then, at time=2, the capsule reaches the second disc. The second disc has ticked forward two positions
--	at this point: it started at position 1, then continued to position 0, and finally ended up at
--	position 1 again. Because there's only a slot at position 0, the capsule bounces away.
--
--	If, however, you wait until time=5 to push the button, then when the capsule reaches each disc, the
--	first disc will have ticked forward 5+1 = 6 times (to position 0), and the second disc will have
--	ticked forward 5+2 = 7 times (also to position 0). In this case, the capsule would fall through the
--	discs and come out of the machine.
--
--	However, your situation has more than two discs; you've noted their positions in your puzzle
--	input. What is the first time you can press the button to get a capsule?
--
-- Test:
--	% cd ../.. && echo -e 'Disc #1 has 5 positions; at time=0, it is at position 4.\nDisc #2 has 2 positions; at time=0, it is at position 1.' | cabal run 2016_day15-timing-is-everything # = 5

type Disk = (Int, Int, Int) -- disk #, tot positions, at time=0 position

parse :: [String] -> Disk
parse ["Disc", dNbrStr, "has", totPosStr, "positions;", "at", "time=0,", "it", "is", "at", "position", time0PosStr] = (read . tail $ dNbrStr, read totPosStr, read . init $ time0PosStr)

align :: Int -> Disk -> Bool
align t (n, tot, t0) = 0 == (t + n + t0) `mod` tot

solve :: [Disk] -> Int
solve ds = fst . head . filter snd . map (\t -> (t, all (align t) ds)) $ [try0, (try0 + tot_1) ..]
  where ((n_1, tot_1, t0_1):_) = ds
        try0 = (tot_1 - (t0_1 + n_1)) `mod` tot_1


main :: IO ()
main = interact $ show . solve . map (parse . words) . lines

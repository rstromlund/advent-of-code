module Main where
import Data.List as L (map)
import Data.Vector.Unboxed as V ((!), Vector, fromList, generate, length, map, reverse, singleton, take, toList)

--	The second disk you have to fill has length 35651584. Again using the initial state in your puzzle
--	input, what is the correct checksum for this disk?

-- NOTE: Switched to vector to make this fast enough to complete.  Other than that, this is just like part 1.

diskLength = 35651584 :: Int

curve :: Vector Bool -> Vector Bool
curve a = a <> singleton False <> (V.map not . V.reverse $ a)

cksum :: Vector Bool -> Vector Bool
cksum v = V.generate (V.length v `div` 2) (\i -> v ! (i * 2) == v ! (i * 2 + 1))

toBool '0' = False
toBool '1' = True

fromBool False = '0'
fromBool True  = '1'

solve :: String -> String
solve = L.map fromBool . V.toList . getCksum . getCurve . fromList . L.map toBool
  where getCurve = V.take diskLength . head . filter ((>= diskLength) . V.length) . iterate curve
        getCksum = head . filter (odd . V.length) . iterate cksum


main :: IO ()
main = interact $ show . solve . head . words

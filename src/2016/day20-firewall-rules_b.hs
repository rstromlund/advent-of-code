module Main where
import Data.List (break, sort)
import Data.Tuple.Extra (both, second)

--	How many IPs are allowed by the blacklist?
--
-- Test:
--	% cd ../.. && echo -e '5-8\n0-2\n4-7' | cabal run 2016_day20-firewall-rules_b # = 2

type Range = (Int, Int) -- min, max

-- maxIP = 9 :: Int -- example max
maxIP = 4294967295 :: Int -- puzzle max

solve :: [Range] -> Int
solve rs = search 0 0 rs
  where search tot cur [] = tot + (maxIP - cur + 1)
        search tot cur ((l, r):rs)
          | cur < l   = search (tot + 1) (cur + 1) ((l, r):rs)
          | cur > r   = search tot cur rs
          | otherwise = search tot (r + 1) rs


main :: IO ()
main = interact $ show . solve . sort . map (second abs . both read . break ('-' ==)) . lines

module Main where
import Data.Char (chr, ord)
import Data.Foldable (toList)
import Data.List as L (find, foldr)
import Data.Sequence as S ((><), Seq, deleteAt, elemIndexL, fromList, index, insertAt, length, reverse, splitAt, update)

--	You scrambled the password correctly, but you discover that you can't actually modify the password
--	file on the system. You'll need to un-scramble one of the existing passwords by reversing the
--	scrambling process.
--
--	What is the un-scrambled version of the scrambled password fbgdceah?
--
-- Test:
--	% cd ../.. && echo -e 'swap position 4 with position 0\nswap letter d with letter b\nreverse positions 0 through 4\nrotate left 1 step\nmove position 1 to position 4\nmove position 3 to position 0\nrotate based on position of letter b\nrotate based on position of letter d' | cabal run 2016_day21-scrambled-letters-and-hash_b # = abcde

-- seed = "decab" -- example
seed = "fbgdceah" -- puzzle
v0 = fromList . map ord $ seed

move :: Seq Int -> [String] -> Seq Int
move v ["swap", "position", x, "with", "position", y] =
  let p1 = read x
      p2 = read y
      c1 = v `index` p1
      c2 = v `index` p2
  in update p1 c2 . update p2 c1 $ v

move v ["swap", "letter", x, "with", "letter", y] =
  let (Just p1) = elemIndexL (ord . head $ x) v
      (Just p2) = elemIndexL (ord . head $ y) v
      c1 = v `index` p1
      c2 = v `index` p2
  in update p1 c2 . update p2 c1 $ v

move v ["rotate", "left", x, _steps] =
  let (l, r) = S.splitAt (read x) v
  in r >< l

move v ["rotate", "right", x, _steps] =
  let (l, r) = S.splitAt (S.length v - read x) v
  in r >< l

move v ["rotate", "based", "on", "position", "of", "letter", x] =
  let (Just p1) = elemIndexL (ord . head $ x) v
      rs  = p1 + 1 + (if p1 >= 4 then 1 else 0)
  in foldl' (\v' _ -> move v' ["rotate", "right", "1", "step"]) v [1..rs]

move v ["reverse", "positions", x, "through", y] =
  let p1 = read x
      p2 = read y
      (l, m') = S.splitAt p1 v
      (m, r) = S.splitAt (p2 - p1 + 1) m'
  in (l >< S.reverse m) >< r

move v ["move", "position", x, "to", "position", y] =
  let p1 = read x
      p2 = read y
      c1 = v `index` p1
  in insertAt p2 c1 . deleteAt p1 $ v

unmove :: [String] -> Seq Int -> Seq Int
unmove ["swap", "position", x, "with", "position", y] v = move v ["swap", "position", y, "with", "position", x]
unmove ["swap", "letter", x, "with", "letter", y] v = move v ["swap", "letter", x, "with", "letter", y]
unmove ["rotate", "left", x, steps] v = move v ["rotate", "right", x, steps]
unmove ["rotate", "right", x, steps] v = move v ["rotate", "left", x, steps]
unmove ["rotate", "based", "on", "position", "of", "letter", x] v =
  -- Start rotating left and see which source vector can explain a "rotate based on" our letter:
  let vs     = map (\n -> move v ["rotate", "left", show n, "steps"]) [0..S.length v]
      Just f = find (\v' -> v == move v' ["rotate", "based", "on", "position", "of", "letter", x]) vs
  in f
unmove ["reverse", "positions", x, "through", y] v = move v ["reverse", "positions", x, "through", y]
unmove ["move", "position", x, "to", "position", y] v = move v ["move", "position", y, "to", "position", x]

solve :: [[String]] -> String
solve = map chr . toList . foldr unmove v0


main :: IO ()
main = interact $ show . solve . map words . lines

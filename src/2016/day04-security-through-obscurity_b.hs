module Main where
import Data.Char (chr, isAlpha, isDigit, isAsciiLower, ord)
import Data.List (break, filter, group, groupBy, sort, span)

--	... To decrypt a room name, rotate each letter forward through the alphabet a number of times equal to the room's sector ID. A becomes B, B becomes C, Z becomes A, and so on. Dashes become spaces.
--
--	For example, the real name for qzmt-zixmtkozy-ivhz-343 is very encrypted name.
--
--	What is the sector ID of the room where North Pole objects are stored?
--
-- Test:
--	% cd ../.. && echo -e 'qzmt-zixmtkozy-ivhz-343[aaaaa]' | cabal run 2016_day04-security-through-obscurity_b # = very encrypted name
--	% cd ../.. && echo -e 'aaaaa-bbb-z-y-x-123[abxyz]\na-b-c-d-e-f-g-h-987[abcde]\nnot-a-real-room-404[oarel]\ntotally-real-room-200[decoy]' | cabal run 2016_day04-security-through-obscurity_b

type Room = (Int, (String, String)) -- Id, (Code, Cksum)

parse :: String -> Room
parse cs = let p0 = break isDigit cs -- e.g. ("aczupnetwp-dnlgpyrpc-sfye-dstaatyr-","561[patyc]")
               p1 = span ('[' /=) (snd p0) -- e.g. ("561","[patyc]")
           in (read . fst $ p1, (fst p0, filter isAlpha . snd $ p1)) -- e.g. (123,("aaaaabbbzyx","abxyz"))

decode :: Room -> (Int, String)
decode (i, (code, _)) = (i, map decodech code)
  where decodech '-' = ' '
        decodech ' ' = ' '
        decodech ch | isAsciiLower ch =
                      let o   = ord ch - ord 'a'
                          ch' = chr (ord 'a' + ((o + i) `mod` 26))
                      in ch'


main :: IO ()
main = interact $ show . fst . head . filter ((==) "northpole object storage " . snd) . map (decode . parse) . lines

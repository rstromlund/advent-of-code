module Main where
import Debug.Trace (trace)
import Data.Bool (bool)
import Data.List as L (map)
import Data.Vector.Unboxed as V ((!), Vector, fromList, generate, length, map, reverse, singleton, take, toList)

--	... For example, with five Elves (numbered 1 to 5):
--
--	  1
--	5   2
--	 4 3
--
--	* Elf 1 takes Elf 2's present.
--	* Elf 2 has no presents and is skipped.
--	* Elf 3 takes Elf 4's present.
--	* Elf 4 has no presents and is also skipped.
--	* Elf 5 takes Elf 1's two presents.
--	* Neither Elf 1 nor Elf 2 have any presents, so both are skipped.
--	* Elf 3 takes Elf 5's three presents.
--
--	So, with five Elves, the Elf that sits starting in position 3 gets all the presents.
--
--	With the number of Elves given in your puzzle input, which Elf gets all the presents?
--
-- Test:
--	% cd ../.. && echo -e '5' | cabal run 2016_day19-an-elephant-named-joseph # = 3

eliminate :: Vector Int -> Vector Int
eliminate v = v'
  where vl  = V.length v
        off = bool 0 2 $ odd vl
        vl' = V.length v `div` 2
        v'  = V.generate vl' (\i -> v ! (i * 2 + off))

solve :: Int -> Int
solve e = (! 0) . solve' . fromList $ [1..e]
  where solve' v | V.length v == 1 = v
                 | otherwise       = solve' . eliminate $ v


main :: IO ()
main = interact $ show . solve . read . head . words

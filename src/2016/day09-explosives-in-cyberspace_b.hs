module Main where
import Data.List (break, splitAt)

--	... markers within decompressed data are decompressed. This, the documentation explains, provides much more substantial compression capabilities, allowing many-gigabyte files to be stored in only a few kilobytes.
--
--	For example:
--
--	* (3x3)XYZ still becomes XYZXYZXYZ, as the decompressed section contains no markers.
--	* X(8x2)(3x3)ABCY becomes XABCABCABCABCABCABCY, because the decompressed data from the (8x2) marker is
--	  then further decompressed, thus triggering the (3x3) marker twice for a total of six ABC sequences.
--	* (27x12)(20x12)(13x14)(7x10)(1x12)A decompresses into a string of A repeated 241920 times.
--	* (25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN becomes 445 characters long.
--
--	Unfortunately, the computer you brought probably doesn't have enough memory to actually decompress the file; you'll have to come up with another way to get its decompressed length.
--
--	What is the decompressed length of the file using this improved format?
--
-- Test:
--	% cd ../.. && echo -e '(3x3)XYZ' | cabal run 2016_day09-explosives-in-cyberspace_b # = 9
--	% cd ../.. && echo -e 'X(8x2)(3x3)ABCY' | cabal run 2016_day09-explosives-in-cyberspace_b # = 20
--	% cd ../.. && echo -e '(27x12)(20x12)(13x14)(7x10)(1x12)A' | cabal run 2016_day09-explosives-in-cyberspace_b # = 241920
--	% cd ../.. && echo -e '(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN' | cabal run 2016_day09-explosives-in-cyberspace_b # = 445
--	% cd ../.. && echo -e '(3x3)XYZ\nX(8x2)(3x3)ABCY\n(27x12)(20x12)(13x14)(7x10)(1x12)A\n(25x3)(3x3)ABC(2x3)XY(5x2)PQRSTX(18x9)(3x2)TWO(5x7)SEVEN' | cabal run 2016_day09-explosives-in-cyberspace_b # = 242394

solve :: String -> Int
solve = walk 0
  where walk acc []        = acc
        walk acc (' ':cs)  = walk acc cs
        walk acc ('\n':cs) = walk acc cs
        walk acc ('(':cs)  =
          let (lxw, ')':cs') = break (')' ==) cs
              (ls, 'x':ws)   = break ('x' ==) lxw
              l              = read ls
              w              = read ws
              (sub, cs'')    = splitAt l cs'
              lsub           = walk 0 sub
          in walk (acc + lsub * w) cs''
        walk acc (_:cs)    = walk (acc + 1) cs


main :: IO ()
main = interact $ show . solve

module Main where
import Data.Bits (popCount)
import Data.Bool (bool)
import Data.List as L ()
import Data.Map.Strict as M (Map, (!?), empty, insert)
import Data.Set as S (Set, deleteFindMin, empty, insert, member, null, singleton, size)

--	How many locations (distinct x,y coordinates, including your starting location) can you reach in at most 50 steps?

data Cell = Wall | Space deriving (Show, Eq, Ord)
type Point = (Int, Int)

spaceOrWall :: Int -> Point -> Cell
spaceOrWall f (x, y) = bool Space Wall (odd . popCount $ z)
  where z = x*x + 3*x + 2*x*y + y + y*y + f


-- Turns out caching this calculation wasn't necessary ... but I've seen AoC puzzles that tricked us like that.  Oh well, didn't hurt.

getCell :: Int -> Point -> Map Point Cell -> (Cell, Map Point Cell)
getCell f pt@(x, y) office
  | x < 0 || y < 0 = (Wall, office)
  | otherwise = get (office !? pt)
  where get (Just c) = (c, office)
        get Nothing  = let c = spaceOrWall f pt in (c, M.insert pt c office)

pStartPoint = (1, 1)
maxSteps    = 50

dirs = [( 0, -1),  -- N
        ( 0,  1),  -- S
        (-1,  0),  -- E
        ( 1,  0)]  -- W

solve :: Int -> Int
solve favNbr = dijkstra M.empty S.empty (singleton (0, pStartPoint))
  where dijkstra :: Map Point Cell -> Set Point -> Set (Int, Point) -> Int
        dijkstra office vps paths
          | S.null paths = size vps
          | otherwise    = nextStep nxtCell paths'
          where (nxtCell, paths') = deleteFindMin paths
                nextStep (s, pt@(x, y)) paths
                  | s == maxSteps = dijkstra office (S.insert pt vps) paths -- keep going till we exhaust all < 50 step paths
                  | s >  maxSteps = size vps -- done, count the visited points.
                  | pt `member` vps = dijkstra office vps paths
                  | otherwise = let neighbors               = L.foldl' (\ps pt -> S.insert (s + 1, pt) ps) paths' adjcent
                                    (adjcent, office')      = L.foldl' valid ([], office) dirs -- adjcent cells that aren't walls (with an office map cache update)
                                    valid (vs, os) (dx, dy) = let pt' = (x + dx, y + dy)
                                                                  (c, os') = getCell favNbr pt' os
                                                              in (bool (pt':vs) vs $ Wall == c, os')
                                in dijkstra office' (S.insert pt vps) neighbors


main :: IO ()
main = interact $ show . solve . read

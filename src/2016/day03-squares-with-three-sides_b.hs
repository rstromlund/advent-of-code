module Main where

--	... triangles are specified in groups of three vertically. Each set of three numbers in a column
--	specifies a triangle. Rows are unrelated.
--
--	For example, given the following specification, numbers with the same hundreds digit would be part of
--	the same triangle:
--
--	101 301 501
--	102 302 502
--	103 303 503
--	201 401 601
--	202 402 602
--	203 403 603
--
--	In your puzzle input, and instead reading by columns, how many of the listed triangles are possible?

maybeTriangle :: Int -> Int -> Int -> Bool
maybeTriangle x y z = (x + y > z) && (x + z > y) && (y + z > x)

maybeTriangles :: [Int] -> [Bool]
maybeTriangles [] = []
maybeTriangles (x0:x1:x2:y0:y1:y2:z0:z1:z2:ts) = maybeTriangle x0 y0 z0 : maybeTriangle x1 y1 z1 : maybeTriangle x2 y2 z2 : maybeTriangles ts

main :: IO ()
main = interact $ show . length . filter id . maybeTriangles . map read . words

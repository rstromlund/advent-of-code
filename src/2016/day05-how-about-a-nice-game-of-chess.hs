module Main where
import Data.List (take)
import Data.Word (Word8)
import Data.ByteString (unpack)
import Data.ByteString.UTF8 (fromString)
import Crypto.Hash.MD5 as MD5 (finalize, init, update)

--	... finding the MD5 hash of some Door ID (your puzzle input) and an increasing integer index (starting with 0).
--
--	A hash indicates the next character in the password if its hexadecimal representation starts with five
--	zeroes. If it does, the sixth character in the hash is the next character of the password.
--
--	For example, if the Door ID is abc:
--
--	* The first index which produces a hash that starts with five zeroes is 3231929, which we find by
--		hashing abc3231929; the sixth character of the hash, and thus the first character of the password,
--		is 1.
--	* 5017308 produces the next interesting hash, which starts with 000008f82..., so the second character of the password is 8.
--	* The third time a hash starts with five zeroes is for abc5278568, discovering the character f.
--
--	In this example, after continuing this search a total of eight times, the password is 18f47a30.
--
--	Given the actual Door ID, what is the password?
--
-- Test:
--	% cd ../.. && echo -e 'abc' | cabal run 2016_day05-how-about-a-nice-game-of-chess # = [1,8,15,4,7,10,3,0]

md5s :: String -> [Word8]
md5s str = unpack $ finalize ctx
  where ctx  = update ctx0 (fromString str)
        ctx0 = MD5.init

solve :: [String] -> [Word8]
solve [doorid] = map (!! 2) . take 8 $ as
  where ms = map (md5s . (doorid ++) . show) [0..]
        as = filter (\(i0:i1:i2:_) -> i0 == 0 && i1 == 0 && i2 < 0x10) ms


main :: IO ()
main = interact $ show . solve . words

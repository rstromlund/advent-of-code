{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Maybe (isNothing)

--	Easter Bunny HQ is actually at the first location you visit twice.
--
--	For example, if your instructions are R8, R4, R4, R8, the first location you visit twice is 4 blocks away, due East.
--
--	How many blocks away is the first location you visit twice?
--
-- Test:
--	% cd ../.. && echo -e 'R8, R4, R4, R8' | cabal run 2016_day01-no-time-for-a-taxicab_b # = 4

type Point = (Int, Int) -- X, Y

dNorth :: Point
dNorth = ( 0, -1)
dSouth ::Point
dSouth = ( 0,  1)
dEast  ::Point
dEast  = ( 1,  0)
dWest  ::Point
dWest  = (-1,  0)

ds :: [Point]
ds = [dNorth, dEast, dSouth, dWest]

rot :: Char -> Int
rot 'R'  =  1
rot 'L'  = -1

walk :: (Int, Point, [(Point, Point)]) -> String -> (Int, Point, [(Point, Point)])
walk (d, (x, y), ls) (c:ns) = (d', p', (p0', p'):ls)
  where d'       = (d + rot c) `mod` 4
        p0'      = (x + dx, y + dy)
        p'       = (x + dx * dist, y + dy * dist)
        dist     = read ns
        (dx, dy) = ds!!d'

solve :: (Int, Point, [(Point, Point)]) -> Int
solve (_, _, ls) = score . head . dropWhile isNothing . search $ ls'
  where ls' = reverse ls
        score (Just (x, y)) = abs x + abs y
        search [_] = []
        search (l@((_sx1, _sy1), (_sx2, _sy2)):ls'') = calcPt l (filter (cross l) ls'') : search ls''
        calcPt ((_sx1, _sy1), (_sx2, _sy2)) [] = Nothing
        calcPt ((sx1, sy1), (sx2, sy2)) [((ex1, ey1), (ex2, ey2))] =
          let -- Normalize points:
              sx1' = min sx1 sx2
              sy1' = min sy1 sy2
              --
              ex1' = min ex1 ex2
              ey1' = min ey1 ey2
              --
              x' = max sx1' ex1'
              y' = max sy1' ey1'
          in Just (x', y')
        cross :: (Point, Point) -> (Point, Point) -> Bool
        cross ((sx1, sy1), (sx2, sy2)) ((ex1, ey1), (ex2, ey2)) =
          ((sx1 <= ex2 && sx2 >= ex1) || (sx1 <= ex1 && sx2 >= ex2)) &&
          ((sy1 <= ey2 && sy2 >= ey1) || (sy1 <= ey1 && sy2 >= ey2))


main :: IO ()
main = interact $ show . solve . foldl' walk (0, (0, 0), [((0, 0), (0, 0))]) . words . filter (/= ',')

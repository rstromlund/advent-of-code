module Main where
import Data.Char (isAlpha, isDigit)
import Data.Function (on)
import Data.List (break, filter, group, groupBy, nub, sort, sortOn, span)
import Data.Ord (Down(..))

--	... A room is real (not a decoy) if the checksum is the five most common letters in the encrypted name, in order, with ties broken by alphabetization. For example:
--
--	aaaaa-bbb-z-y-x-123[abxyz] is a real room because the most common letters are a (5), b (3), and then a tie between x, y, and z, which are listed alphabetically.
--	a-b-c-d-e-f-g-h-987[abcde] is a real room because although the letters are all tied (1 of each), the first five are listed alphabetically.
--	not-a-real-room-404[oarel] is a real room.
--	totally-real-room-200[decoy] is not.
--	Of the real rooms from the list above, the sum of their sector IDs is 1514.
--
--	What is the sum of the sector IDs of the real rooms?
--
-- Test:
--	% cd ../.. && echo -e 'aaaaa-bbb-z-y-x-123[abxyz]\na-b-c-d-e-f-g-h-987[abcde]\nnot-a-real-room-404[oarel]\ntotally-real-room-200[decoy]' | cabal run 2016_day04-security-through-obscurity # = 1514

type Room = (Int, (String, String)) -- Id, (Code, Cksum)

parse :: String -> Room
parse cs = let p0 = break isDigit cs -- e.g. ("aczupnetwp-dnlgpyrpc-sfye-dstaatyr-","561[patyc]")
               p1 = span ('[' /=) (snd p0) -- e.g. ("561","[patyc]")
           in (read . fst $ p1, (filter isAlpha . fst $ p0, filter isAlpha . snd $ p1)) -- e.g. (123,("aaaaabbbzyx","abxyz"))

solve :: [Room] -> Int
solve = sum . map maybeRoom
  where maybeRoom r@(_, (code, _)) =
          let gs = group . sort $ code -- e.g. ["aaaaa","bbb","x","y","z"]
              ls = sortOn (Down . length) gs -- e.g. ["aaaaa","bbb","z","y","x"]
              gl = groupBy ((==) `on` length) ls -- e.g. [["aaaaa"],["bbb"],["z","y","x"]]
              ll = take 5 . concatMap (nub . sort . concat) $ gl -- e.g. "abxyz"
          in tst r ll
        tst (i, (_, cksum)) ts
          | ts == cksum = i
          | otherwise   = 0


main :: IO ()
main = interact $ show . solve . map parse . lines

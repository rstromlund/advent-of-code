module Main where
import Data.Char (chr, ord)
import Data.Foldable (toList)
import Data.Sequence as S ((><), Seq, deleteAt, elemIndexL, fromList, index, insertAt, length, mapWithIndex, reverse, splitAt, update)

--	... The scrambling function is a series of operations (the exact list is provided in your puzzle
--	input). Starting with the password to be scrambled, apply each operation in succession to the
--	string. The individual operations behave as follows:
--
--	* swap position X with position Y means that the letters at indexes X and Y (counting from 0) should be swapped.
--	* swap letter X with letter Y means that the letters X and Y should be swapped (regardless of where
--	  they appear in the string).
--	* rotate left/right X steps means that the whole string should be rotated; for example, one right
--	  rotation would turn abcd into dabc.
--	* rotate based on position of letter X means that the whole string should be rotated to the right
--	  based on the index of letter X (counting from 0) as determined before this instruction does any
--	  rotations. Once the index is determined, rotate the string to the right one time, plus a number of
--	  times equal to that index, plus one additional time if the index was at least 4.
--	* reverse positions X through Y means that the span of letters at indexes X through Y (including the
--	  letters at X and Y) should be reversed in order.
--	* move position X to position Y means that the letter which is at index X should be removed from the
--	 string, then inserted such that it ends up at index Y.
--
--	For example, suppose you start with abcde and perform the following operations:
--
--	* swap position 4 with position 0 swaps the first and last letters, producing the input for the next step, ebcda.
--	* swap letter d with letter b swaps the positions of d and b: edcba.
--	* reverse positions 0 through 4 causes the entire string to be reversed, producing abcde.
--	* rotate left 1 step shifts all letters left one position, causing the first letter to wrap to the end of the string: bcdea.
--	* move position 1 to position 4 removes the letter at position 1 (c), then inserts it at position 4 (the end of the string): bdeac.
--	* move position 3 to position 0 removes the letter at position 3 (a), then inserts it at position 0 (the front of the string): abdec.
--	* rotate based on position of letter b finds the index of letter b (1), then rotates the string right
--	  once plus a number of times equal to that index (2): ecabd.
--	* rotate based on position of letter d finds the index of letter d (4), then rotates the string right
--	  once, plus a number of times equal to that index, plus an additional time because the index was at
--	  least 4, for a total of 6 right rotations: decab.
--
--	After these steps, the resulting scrambled password is decab.
--
--	Now, you just need to generate a new scrambled password and you can access the system. Given the list
--	of scrambling operations in your puzzle input, what is the result of scrambling abcdefgh?
--
-- Test:
--	% cd ../.. && echo -e 'swap position 4 with position 0\nswap letter d with letter b\nreverse positions 0 through 4\nrotate left 1 step\nmove position 1 to position 4\nmove position 3 to position 0\nrotate based on position of letter b\nrotate based on position of letter d' | cabal run 2016_day21-scrambled-letters-and-hash # = decab

-- seed = "abcde" -- example
seed = "abcdefgh" -- puzzle
v0 = fromList . map ord $ seed

move :: Seq Int -> [String] -> Seq Int
move v ["swap", "position", x, "with", "position", y] =
  let p1 = read x
      p2 = read y
      c1 = v `index` p1
      c2 = v `index` p2
  in update p1 c2 . update p2 c1 $ v

move v ["swap", "letter", x, "with", "letter", y] =
  let cx = ord . head $ x
      cy = ord . head $ y
  in S.mapWithIndex (\_ cv -> if cv == cx then cy else if cv == cy then cx else cv) v

move v ["rotate", "left", x, _steps] = let (l, r) = S.splitAt (read x) v in r >< l

move v ["rotate", "right", x, _steps] = let (l, r) = S.splitAt (S.length v - read x) v in r >< l

move v ["rotate", "based", "on", "position", "of", "letter", x] =
  let (Just p1) = elemIndexL (ord . head $ x) v
      cnt = p1 + 1 + (if p1 >= 4 then 1 else 0)
  in foldl' (\v' _ -> move v' ["rotate", "right", "1", "step"]) v [1..(cnt `mod` S.length v)]

move v ["reverse", "positions", x, "through", y] =
  let p1 = read x
      p2 = read y
      (l, m') = S.splitAt p1 v
      (m, r) = S.splitAt (p2 - p1 + 1) m'
  in (l >< S.reverse m) >< r

move v ["move", "position", x, "to", "position", y] =
  let p1 = read x
      p2 = read y
      c1 = v `index` p1
  in insertAt p2 c1 . deleteAt p1 $ v

solve :: [[String]] -> String
solve = map chr . toList . foldl' move v0


main :: IO ()
main = interact $ show . solve . map words . lines

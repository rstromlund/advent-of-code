module Main where
import Algorithm.Search (aStar)
import Data.Map.Strict (Map, (!?), fromList)
import Data.Maybe (isJust)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser)
import Text.Megaparsec.Char (char, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your goal is to gain access to the data which begins in the node with y=0 and the highest x (that is, the node in the top-right corner).
--
--	... So, after 7 steps, we've accessed the data we want. Unfortunately, each of these moves takes time, and we need to be efficient:
--
--	What is the fewest number of steps required to move your goal data to node-x0-y0?
--
-- Test:
--	% cd ../.. && echo -e 'root@ebhq-gridcenter# df -h\nFilesystem              Size  Used  Avail  Use%\n/dev/grid/node-x0-y0   10T    8T     2T   80%\n/dev/grid/node-x0-y1   11T    6T     5T   54%\n/dev/grid/node-x0-y2   32T   28T     4T   87%\n/dev/grid/node-x1-y0    9T    7T     2T   77%\n/dev/grid/node-x1-y1    8T    0T     8T    0%\n/dev/grid/node-x1-y2   11T    7T     4T   63%\n/dev/grid/node-x2-y0   10T    6T     4T   60%\n/dev/grid/node-x2-y1    9T    8T     1T   88%\n/dev/grid/node-x2-y2    9T    6T     3T   66%' | cabal run 2016_day22-grid-computing_c # = 7

type Point = (Int, Int) -- x, y
data Node = Node {size, used :: !Int} deriving (Show, Eq, Ord)
type Parser = Parsec Void String

node :: Parser (Point, Node)
node = do
  x      <- string "/dev/grid/node-x" *> decimal
  y      <- string "-y" *> decimal <* space
  size   <- decimal <* char 'T' <* space
  used   <- decimal <* char 'T' <* space
  _avail <- decimal <* char 'T' <* space
  _pcnt  <- decimal <* char '%' <* eof
  return ((x, y), Node size used)

type SearchState = (Point, Point) -- curr pt, goal pt

cardinalDirections :: [Point]
cardinalDirections = [
  ( 1,  0),
  (-1,  0),
  ( 0,  1),
  ( 0, -1)
  ]

exitPt = (0, 0)

manhattanDist :: SearchState -> Int
manhattanDist (pos, goal) = dist goal exitPt + dist goal pos
  where dist (x1, y1) (x2, y2) = abs (y2 - y1) + abs (x2 - x1)

neighbors :: Int -> Map Point Node -> SearchState -> [SearchState]
neighbors maxX ms (pt@(x, y), gl) =
  [ (pt', gl')
  | (dx, dy) <- cardinalDirections
  , let x'  = x + dx
        y'  = y + dy
        pt' = (x', y')  -- candidate point
        gl' | gl == pt' = pt
            | otherwise = gl
  , x' >= 0 && x' <= maxX && y' >= 0
  , Just cn <- [ms !? pt'] -- candidate node
  , used cn <= size n
  ]
  where (Just n) = ms !? pt -- 'n' is invariant in the cardinal directions

solve :: Either a [(Point, Node)] -> Maybe Int
solve (Right ns) = (fst <$>) $ aStar
                     (neighbors maxX ms)     -- find all neighbors in the 4 cardinal directions (pruning walls and invalid states)
                     (const . const $ 1)     -- it only costs 1 step between neighbors
                     manhattanDist           -- cost of this state
                     ((exitPt ==) . snd)     -- done when goalPoint is at the exit
                     (emptyPoint, (maxX, 0)) -- our start/current point; and tracking the current goal point position too
  where ms         = fromList ns
        emptyPoint = fst . head . filter ((0 ==) . used . snd) $ ns
        maxX       = maximum . map (fst . fst) $ ns


main :: IO ()
main = interact $ show . solve . mapM (runParser node "<stdin>") . drop 2 . lines

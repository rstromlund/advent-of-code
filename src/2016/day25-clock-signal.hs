module Main where
import Data.Array ((!), Array, bounds, elems, inRange, listArray)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... This antenna code, being a signal generator, uses one extra instruction:
--
--	* out x transmits x (either an integer or the value of a register) as the next value for the clock signal.
--
--	The code takes a value (via register a) that describes the signal to generate, but you're not sure how
--	it's used. You'll have to find the input to produce the right signal through experimentation.
--
--	What is the lowest positive integer that can be used to initialize register a and cause the code to
--	output a clock signal of 0, 1, 0, 1... repeating forever?

{- scratch pad:

Note: this is as far as I got; these formulas and psuedo code are probably not entirely correct.  I decided to
initialize register d with the multiplication (that part is correct) and run just the inner loop.  That
generates the first 12 cycles; then I brute force search until I got the right answer.  No doubt there is a
simple math equation that would be more elegant ... but it worked.

 0: cpy a d   # move puzzle input to d -- minimize this
 1: cpy 9 c   # c = 9
 2: cpy 282 b # loop while c /= 0 do
              #   b = 282
 3: inc d     #   loop while b /= 0 do
              #     d ++
 4: dec b     #     b --
 5: jnz b -2  #   done
 6: dec c     #   c --
 7: jnz c -5  # done
              # ^^^^ d = a + 9 * 282

 8: cpy d a   # while forever do
              #   a = d
 9: jnz 0 0   #   do
10: cpy a b   #     b = a
11: cpy 0 a   #     a = 0
12: cpy 2 c   #     x: c = 2
13: jnz b 2   #     while b /= 0 do
14: jnz 1 6   #       (break / goto z)
15: dec b     #       b --
16: dec c     #       c --
17: jnz c -4  #       if c /= 0 continue
18: inc a     #       a ++
19: jnz 1 -7  #       goto x
20: cpy 2 b   #     done
              #     ^^^^ a = d `div` 2; c = 2 - (b `mod` 2)
              #     z: b = 2
21: jnz c 2   #     y: if c == 0 goto w ## (rem 0?)
22: jnz 1 4   #
23: dec b     #     b --
24: dec c     #     c --
25: jnz 1 -4  #     goto y
26: jnz 0 0   #     w:
27: out b     #     print b
28: jnz a -19 #   while a /= 0
29: jnz 1 -21 # done

a = 1
d = a + 9 * 282 (2539)

while forever do
  a = d (2538)
  do
    b = a (2538)
    a = b `div` 2 (1269)
    c = 2 - (b `mod` 2) (2)
    b = 2 - c
    print b
  while a /= 0
done

-}

data Memory = Constant Int | Register Char deriving (Show, Eq, Ord)
data Instruction = Copy Memory Memory
                 | Inc Memory
                 | Dec Memory
                 | JmpNot0 Memory Memory
                 | Out Memory
                 deriving (Show, Eq, Ord)

type Parser = Parsec Void String

signed :: Parser Int
signed = f <$> optional (char '-') <*> decimal
  where f (Just '-') x = negate x
        f _          x = x

mkMemory :: Parser Memory
mkMemory = (Constant <$> signed) <|> (Register <$> letterChar)

mkSingle :: (Memory -> Instruction) -> Parser Instruction
mkSingle c = c <$> mkMemory

instructions :: Parser (Array Int Instruction)
instructions = mkra <$> many instruction <* eof
  where mkra :: [Instruction] -> Array Int Instruction
        mkra is = listArray (0, length is - 1) is

instruction :: Parser Instruction
instruction = (
  string     "cpy " *> (Copy <$> (mkMemory <* space) <*> mkMemory)
  <|> string "inc " *> (Inc <$> mkMemory)
  <|> string "dec " *> (Dec <$> mkMemory)
  <|> string "out " *> (Out <$> mkMemory)
  <|> string "jnz " *> (JmpNot0 <$> (mkMemory <* space) <*> mkMemory)
  ) <* newline

data CPU = CPU {regA, regB, regC, regD, iptr :: !Int} deriving (Show)
type VM = ([Int], (Array Int Instruction, CPU))

get (Constant i)   = const i
get (Register 'a') = regA
get (Register 'b') = regB
get (Register 'c') = regC
get (Register 'd') = regD
set (Register 'a') r cpu = cpu{regA = r}
set (Register 'b') r cpu = cpu{regB = r}
set (Register 'c') r cpu = cpu{regC = r}
set (Register 'd') r cpu = cpu{regD = r}

-- TODO: cpu changes all the time, is does not change, and os rarely changes.  I should define VM as (cpu, (is,os)); I think.
-- Or better yet, use a state monad.

exec :: VM -> VM
exec vm@(_, (is, cpu@CPU{iptr = ip}))
  | 29 == ip     = vm -- only execute the outer loop, not the "while forever" loop
  | inRange b ip = exec . step vm $ is ! ip
  | otherwise    = vm
  where b = bounds is
        step :: VM -> Instruction -> VM
        step (os, (is, cpu)) (Copy m1 m2)    = (os, (is, (set m2 (get m1 cpu) cpu){iptr = ip + 1}))
        step (os, (is, cpu)) (Inc m1)        = (os, (is, (set m1 (get m1 cpu + 1) cpu){iptr = ip + 1}))
        step (os, (is, cpu)) (Dec m1)        = (os, (is, (set m1 (get m1 cpu - 1) cpu){iptr = ip + 1}))
        step (os, (is, cpu)) (JmpNot0 m1 m2) = (os, (is, cpu{iptr = ip + (if get m1 cpu /= 0 then get m2 cpu else 1)}))
        step (os, (is, cpu)) (Out m1)        = (get m1 cpu:os, (is, cpu{iptr = ip + 1}))

solve :: Either a (Array Int Instruction) -> Int
solve (Right is) = fst . head . filter ((win ==) . snd) . map run $ [0..]
  where run i    = (i, fst . exec $ ([], (is, CPU{regA = 0, regB = 0, regC = 0, regD = i + mult, iptr = 8})))
        win      = reverse . take 12 . cycle $ [0,1]
        mult     = product . take 2 . filter (/= 0) . map getCpyConst . elems $ is
        --
        getCpyConst (Copy (Constant i) _) = i
        getCpyConst _ = 0


main :: IO ()
main = interact $ show . solve . runParser instructions "<stdin>"

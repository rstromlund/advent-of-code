module Main where
import Data.List (sortBy)
import Data.List.Extra (replace)
import Data.List.Split (chunksOf)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser)
import Text.Megaparsec.Char (char, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your goal is to gain access to the data which begins in the node with y=0 and the highest x (that is, the node in the top-right corner).
--
--	... So, after 7 steps, we've accessed the data we want. Unfortunately, each of these moves takes time, and we need to be efficient:
--
--	What is the fewest number of steps required to move your goal data to node-x0-y0?
--
-- Note:
--	This app pretty prints a "sliding puzzle" looking grid and instructions to hand solve part 2.  It is
--	faster to hand solve than even an A* solution takes.  We need to be efficient after all. : p

type Point = (Int, Int) -- x, y

data Node = Node {size, used :: !Int} deriving (Show, Eq, Ord)

-- e.g. /dev/grid/node-x0-y0     85T   72T    13T   84%

type Parser = Parsec Void String

node :: Parser (Point, Node)
node = do
  x      <- string "/dev/grid/node-x" *> decimal
  y      <- string "-y" *> decimal <* space
  size   <- decimal <* char 'T' <* space
  used   <- decimal <* char 'T' <* space
  _avail <- decimal <* char 'T' <* space
  _pcnt  <- decimal <* char '%' <* eof
  return ((x, y), Node size used)

solve :: Either a [(Point, Node)] -> String
solve (Right ns) = show . chunksOf ((1 +) . fst $ goalPoint) . map getChar . sortBy (\((ax, ay), _) ((bx, by), _) -> compare (ay * 128 + ax) (by * 128 + bx)) $ ns
  where emptyServer :: (Point, Node)
        emptyServer = head . filter ((0 ==) . used . snd) $ ns
        emptyPoint  = fst emptyServer
        emptySize   = size . snd $ emptyServer
        --
        goalPoint :: Point
        goalPoint = maximum . map fst . filter ((0 ==) . snd . fst) $ ns
        --
        getChar (pt, Node{used = u})
          -- FIXME? this wall '|' guard only uses the empty node size and doesn't really check all pairs of servers (part 1 anyone?).  But it worked for me.
          | u > emptySize    = '|'
          | pt == emptyPoint = '_'
          | pt == goalPoint  = 'G'
          | pt == (0, 0)     = 'E'
          | otherwise        = '.'

{- my grid:
E..............................G
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
................................
...................|||||||||||||
................................
................................
........................_.......
................................
................................
................................

44 moves up around wall (keep one "." to your right) to just E of G
30 * 5 to move G just W of E
1 more move pops G to E

(+ 44 (* 30 5) 1)
195

-}

main :: IO ()
main = interact $ replace "\",\"" "\"\n,\"" . ("Note: a little manual work saves a lot of compute time and coding trouble.\nCount the minimum moves to get '_' to just east of 'G' (maneuvering around the wall),\nthen 5 moves gets 'G' one step closer to 'E', and one more move gets us home.  My moves == 195, YMMV.\n" ++ )
  . solve . mapM (runParser node "<stdin>") . drop 2 . lines

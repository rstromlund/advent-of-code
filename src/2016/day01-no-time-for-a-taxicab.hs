{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where

--	Following R2, L3 leaves you 2 blocks East and 3 blocks North, or 5 blocks away.
--	R2, R2, R2 leaves you 2 blocks due South of your starting position, which is 2 blocks away.
--	R5, L5, R5, R3 leaves you 12 blocks away.
--	How many blocks away is Easter Bunny HQ?
--
-- Test:
--	% cd ../.. && echo -e 'R2, L3' | cabal run 2016_day01-no-time-for-a-taxicab # = 5
--	% cd ../.. && echo -e 'R2, R2, R2' | cabal run 2016_day01-no-time-for-a-taxicab # = 2
--	% cd ../.. && echo -e 'R5, L5, R5, R3' | cabal run 2016_day01-no-time-for-a-taxicab # = 12

type Point = (Int, Int) -- X, Y
type Pos = (Int, Point) -- Dir, (X, Y)

dNorth :: Point
dNorth = ( 0, -1)
dSouth ::Point
dSouth = ( 0,  1)
dEast  ::Point
dEast  = ( 1,  0)
dWest  ::Point
dWest  = (-1,  0)

ds :: [Point]
ds = [dNorth, dEast, dSouth, dWest]

walk :: Pos -> String -> Pos
walk (d, (x, y)) (c:ns) = (d', (x + dx * dist, y + dy * dist))
  where rot 'R'  =  1
        rot 'L'  = -1
        d'       = (d + rot c) `mod` 4
        dist     = read ns
        (dx, dy) = ds!!d'

score :: Pos -> Int
score (_, (x, y)) = abs x + abs y


main :: IO ()
main = interact $ show . score . foldl' walk (0, (0, 0)) . words . filter (/= ',')

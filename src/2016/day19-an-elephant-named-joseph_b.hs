module Main where
import Data.Sequence as S ((><), (|>), Seq, ViewL(..), drop, fromList, length, null, singleton, splitAt, viewl)

--	... the Elves agree to instead steal presents from the Elf directly across the circle. If two Elves
--	are across the circle, the one on the left (from the perspective of the stealer) is stolen from. The
--	other rules remain unchanged: Elves with no presents are removed from the circle entirely, and the
--	other elves move in slightly to keep the circle evenly spaced.
--
--	... So, with five Elves, the Elf that sits starting in position 2 gets all the presents.
--
--	With the number of Elves given in your puzzle input, which Elf now gets all the presents?
--
-- Test:
--	% cd ../.. && echo -e '5' | cabal run 2016_day19-an-elephant-named-joseph_b # = 2

solve :: Int -> Int
solve e = eliminate . fromList $ [1..e]
  where eliminate :: Seq Int -> Int
        eliminate xs =
          case viewl xs of
            EmptyL -> error "no answer?"
            x :< xs'
              | S.null xs' -> x
              | otherwise  -> let o5 = (S.length xs' - 1) `div` 2 -- 0.5 == half way point
                                  (l, r) = S.splitAt o5 xs'
                              in eliminate $ (l >< S.drop 1 r) |> x


main :: IO ()
main = interact $ show . solve . read . head . words

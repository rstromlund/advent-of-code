module Main where

--	... you notice some extra parts on the first floor that weren't listed on the record outside:
--
--	An elerium generator.
--	An elerium-compatible microchip.
--	A dilithium generator.
--	A dilithium-compatible microchip.
--
--	These work just like the other generators and microchips. You'll have to get them up to assembly as well.
--
--	What is the minimum number of steps required to bring all of the objects, including these four new ones, to the fourth floor?
--
-- Test:
--	% cd ../.. && echo -e 'The first floor contains nothing relevant.\nThe second floor contains a hydrogen-compatible microchip and a hydrogen generator.\nThe third floor contains a lithium-compatible microchip and a lithium generator.\nThe fourth floor contains nothing relevant.' | cabal run 2016_day11-radioisotope-thermoelectric-generators_b # = 27
--	% cd ../.. && echo -e 'The first floor contains a strontium generator, a strontium-compatible microchip, a plutonium generator, and a plutonium-compatible microchip.\nThe second floor contains a thulium-compatible microchip and a thulium generator.\nThe third floor contains nothing relevant.\nThe fourth floor contains nothing relevant.' | cabal run 2016_day11-radioisotope-thermoelectric-generators_b # = experimenting

-- Update this number with your answer from part1:
iPartA = 37

-- 4 elements from the bottom to the top add 24 moves
iAdjust = 24

solve :: String -> Int
solve _ = iPartA + iAdjust

main :: IO ()
main = interact $ show . solve

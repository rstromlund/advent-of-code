module Main where
import Data.Bool (bool)

--	... the keypad is not at all like you imagined it. Instead, you are confronted with the result of hundreds of man-hours of bathroom-keypad-design meetings:
--
--	    1
--	  2 3 4
--	5 6 7 8 9
--	  A B C
--	    D
--
--	You still start at "5" and stop when you're at an edge, but given the same instructions as above, the outcome is very different:
--
--	* You start at "5" and don't move at all (up and left are both edges), ending at 5.
--	* Continuing from "5", you move right twice and down three times (through "6", "7", "B", "D", "D"), ending at D.
--	* Then, from "D", you move five more times (through "D", "B", "C", "C", "B"), ending at B.
--	* Finally, after five more moves, you end at 3.
--
--	So, given the actual keypad layout, the code would be 5DB3.
--
--	Using the same instructions in your puzzle input, what is the correct bathroom code?
--
-- Test:
--	% cd ../.. && echo -e 'ULL\nRRDDD\nLURDL\nUUUUD' | cabal run 2016_day02-bathroom-security_b # = 5DB3

type Point = (Int, Int) -- X, Y

pad = [
  "       ",
  "   1   ",
  "  234  ",
  " 56789 ",
  "  ABC  ",
  "   D   ",
  "       "
  ]

point5 = (1, 3)

dMoveX 'U' = 0
dMoveX 'D' = 0
dMoveX 'L' = -1
dMoveX 'R' = 1

dMoveY 'U' = -1
dMoveY 'D' = 1
dMoveY 'L' = 0
dMoveY 'R' = 0

getPadChar (x, y) = (pad !! y) !! x

move (x, y) c =
  let x' = x + dMoveX c
      y' = y + dMoveY c
      d  = getPadChar (x', y')
  in bool (x', y') (x, y) $ ' ' == d

cmd (p, ans) ds =
  let p' = foldl' move p ds
  in (p', getPadChar p' : ans)

solve :: [String] -> String
solve = reverse . snd . foldl' cmd (point5, "")


main :: IO ()
main = interact $ show . solve . lines

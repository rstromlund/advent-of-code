module Main where
import Debug.Trace (trace)
import Algorithm.Search (dijkstra, pruning)
import Data.Char (isDigit)
import Data.Array (Array, assocs, bounds, inRange, listArray, (!))
import Data.List (isPrefixOf, nub, permutations, sort, sortOn, tails)
import Data.Map.Strict (Map, (!?), fromList)
import Data.Set (Set, deleteFindMin, empty, insert, member, notMember, singleton)
import Data.Tuple (swap)

--	... you acquired and create a map with the relevant locations marked (your puzzle input). 0 is your
--	current location, from which the cleaning robot embarks; the other numbers are (in no particular
--	order) the locations the robot needs to visit at least once each. Walls are marked as '#', and open
--	passages are marked as '.'. Numbers behave like open passages.
--
--	For example, suppose you have a map like the following:
--
--	###########
--	#0.1.....2#
--	#.#######.#
--	#4.......3#
--	###########
--
--	To reach all of the points of interest as quickly as possible, you would have the robot take the following path:
--
--	* 0 to 4 (2 steps)
--	* 4 to 1 (4 steps; it can't move diagonally)
--	* 1 to 2 (6 steps)
--	* 2 to 3 (2 steps)
--
--	Since the robot isn't very fast, you need to find it the shortest route. This path is the fewest steps
--	(in the above example, a total of 14) required to start at 0 and then visit every other location at
--	least once.
--
--	Given your actual map, and starting from location 0, what is the fewest number of steps required to
--	visit every non-0 number marked on the map at least once?
--
-- Test:
--	% cd ../.. && echo -e '###########\n#0.1.....2#\n#.#######.#\n#4.......3#\n###########' | cabal run 2016_day24-air-duct-spelunking # = 14

type Point = (Int, Int) -- Y,X; y first is important b/c listArray builds it that way

mkGrid :: [String] -> Array Point Char
mkGrid cs = listArray ((0, 0), (ly, lx)) . concat $ cs
  where ly = pred . length $ cs
        lx = pred . length . head $ cs

manhattanDist :: Point -> Point -> Int
manhattanDist (y1, x1) (y2, x2) = abs (y2 - y1) + abs (x2 - x1)

cardinalDirections :: [Point]
cardinalDirections = [
  (-1,  0),
  ( 1,  0),
  ( 0, -1),
  ( 0,  1)
  ]

solve :: Array Point Char -> Int
solve hmap = minimum . map sol . filter ("0" `isPrefixOf`) . permutations . map fst $ dpts -- ["01234","10234",...]
  where bnds  = bounds hmap
        dpts  = map swap . sortOn snd . filter (isDigit . snd) . assocs $ hmap -- [('0',(1,1)),('1',(1,3)),...]
        cnxs  = nub . map (sort . take 2) . permutations $ dpts -- e.g. [[('0',(1,1)),('1',(1,3))],[('1',(1,3)),('2',(1,9))],...]
        dists = fromList . map getDist $ cnxs -- e.g. fromList [(('0','1'),2),(('0','2'),8),...]
        --
        getDist [(d1, p1), (d2, p2)] =
          let Just (cost, _) = dijkstra (neighbors `pruning` isWall) manhattanDist (p2 ==) p1
          in ((d1, d2), cost)
        --
        -- Upto 4 neighbors per visit point.
        neighbors :: Point -> [Point]
        neighbors (y, x) =
          [ neighbor
          | (dy, dx) <- cardinalDirections
          , let neighbor = (y + dy, x + dx)
          , inRange bnds neighbor
          ]
        --
        isWall :: Point -> Bool
        isWall pt = '#' == (hmap ! pt)
        --
        -- Above uses dijkstra to find distances between all digits/points; below we brute force all combinations
        -- to find minimum combination.  Maybe anther application of dijkstra (or a*) would be better? : )
        sol = sum . map sol' . tails
        sol' (d1:d2:_) = let pair = (min d1 d2, max d1 d2)
                             Just d = dists !? pair
                         in d
        sol' _         = 0


main :: IO ()
main = interact $ show . solve . mkGrid . lines

#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint *.hs
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-xyz.hs   < 01a.txt ; (( rc += ${?} ))
#time ./day01-xyz_b.hs < 01a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-xyz.hs   < 02a.txt ; (( rc += ${?} ))
#time ./day02-xyz_b.hs < 02a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-xyz.hs   < 03a.txt ; (( rc += ${?} ))
#time ./day03-xyz_b.hs < 03a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-xyz.hs   < 04a.txt ; (( rc += ${?} ))
#time ./day04-xyz_b.hs < 04a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-xyz.hs   < 05a.txt ; (( rc += ${?} ))
#time ./day05-xyz_b.hs < 05a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-xyz.hs   < 06a.txt ; (( rc += ${?} ))
#time ./day06-xyz_b.hs < 06a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-xyz.hs   < 07a.txt ; (( rc += ${?} ))
#time ./day07-xyz_b.hs < 07a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-xyz.hs   < 08a.txt ; (( rc += ${?} ))
#time ./day08-xyz_b.hs < 08a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-xyz.hs   < 09a.txt ; (( rc += ${?} ))
#time ./day09-xyz_b.hs < 09a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-xyz.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-xyz_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-xyz.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-xyz_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-xyz.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-xyz_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-xyz.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-xyz_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-xyz.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-xyz_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-xyz.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-xyz_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-xyz.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-xyz_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-xyz.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-xyz_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time ./day18-xyz.hs   < 18a.txt ; (( rc += ${?} ))
#time ./day18-xyz_b.hs < 18a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-xyz.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-xyz_b.hs < 19a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day20"
#time ./day20-xyz.hs   < 20a.txt ; (( rc += ${?} ))
#time ./day20-xyz_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-xyz.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-xyz_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-xyz.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-xyz_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time ./day23-xyz.hs   < 23a.txt ; (( rc += ${?} ))
#time ./day23-xyz_b.hs < 23a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-xyz.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-xyz_b.hs < 24a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day25"
#time ./day25-snowverload.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

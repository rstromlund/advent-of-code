module Main where
import Data.Char (digitToInt)
import Control.Monad.ST
import qualified Data.List   as L (foldl')
import qualified Data.Set    as S (empty, insert, member, size)
import qualified Data.Vector as V (Vector, fromList, length, map, (!), (//))

--	... your submarine can remotely measure the energy level of each octopus (your puzzle input). For example:

--	5483143223
--	2745854711
--	5264556173
--	6141336146
--	6357385478
--	4167524645
--	2176841721
--	6882881134
--	4846848554
--	5283751526
--
--	The energy level of each octopus is a value between 0 and 9. Here, the top-left octopus has an energy level of 5, the
--	bottom-right one has an energy level of 6, and so on.
--
--	You can model the energy levels and flashes of light in steps. During a single step, the following occurs:
--
--	* First, the energy level of each octopus increases by 1.
--	* Then, any octopus with an energy level greater than 9 flashes. This increases the energy level of all adjacent octopuses
--	  by 1, including octopuses that are diagonally adjacent. If this causes an octopus to have an energy level greater than 9,
--	  it also flashes. This process continues as long as new octopuses keep having their energy level increased beyond 9. (An
--	  octopus can only flash at most once per step.)
--	* Finally, any octopus that flashed during this step has its energy level
--	  set to 0, as it used all of its energy to flash.
--
--	... After 100 steps, there have been a total of 1656 flashes.
--
-- Test:
--	% cd ../.. && echo -e '11111\n19991\n19191\n19991\n11111' | cabal run 2021_day11-dumbo-octopus ## 9 after 1 step, 259 after 100
--	% cd ../.. && echo -e '5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526' | cabal run 2021_day11-dumbo-octopus ## 1656

nbrDirs = [
  (-1, -1), (0, -1), (1, -1),
  (-1,  0),          (1,  0),
  (-1,  1), (0,  1), (1,  1)
  ]

applyN :: Int -> (b -> b) -> b -> b
applyN n = foldr (.) id . replicate n

solve :: [String] -> Int
solve ls = snd . applyN 100 step $ (om, 0)
  where om = V.fromList . map (V.fromList . map digitToInt) $ ls -- om (octopus map) is a vector of vectors

step :: (V.Vector (V.Vector Int), Int) -> (V.Vector (V.Vector Int), Int)
step (v, cnt) = (\(v', s') -> (v', cnt + S.size s')) . L.foldl' (\acc y -> fst . L.foldl' xpld (acc, y) $ [0..(lx - 1)]) (next v, S.empty) $ [0..(ly - 1)]
  where ly = V.length v
        lx = V.length $ v V.! 0
        getEnergy x y om
          | x >= 0 && y >= 0 && x < lx && y < ly = om V.! y V.! x
          | otherwise = -999
        setEnergy x y e om
          | x >= 0 && y >= 0 && x < lx && y < ly = om V.// [(y, om V.! y V.// [(x, e)])]
          | otherwise = om
        next v  = V.map (V.map succ) v
        xpld ((om, es), y) x = xpld' ((om, es), y) x $ getEnergy x y om
        xpld' ((om, es), y) x e
          | e <= 9 = ((om, es), y) -- not exploding
          | S.member (x, y) es = ((om, es), y) -- already exploded
          | otherwise = L.foldl' (\((omf, esf), _) (dx, dy) ->
                                    -- Light up all our neighbors
                                    let x'   = x + dx
                                        y'   = y + dy
                                        e'   = 1 + getEnergy x' y' omf
                                        omf' = setEnergy x' y' e' omf
                                    in if S.member (x', y') esf then ((omf, esf), y) else (\(acc, _) -> (acc, y)) . xpld' ((omf', esf), y') x' $ e'
                                    --  Curious thing, w/o the function in front of "xpld'"; this breaks out of fold!!! ^^^^ tail recursion breaks from fold?! Or updates "y"?!
              ) ((setEnergy x y 0 om, S.insert (x, y) es), y) nbrDirs


main :: IO ()
main = interact $ show . solve . lines

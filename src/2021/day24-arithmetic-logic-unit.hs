module Main where
import Data.List (dropWhile, reverse)

--	Move all players home
--
-- Test:
--	% cd ../.. && echo -e '*na-no test*' | cabal run 2021_day24-arithmetic-logic-unit ## 91699394894995

{-
Type	z-es
t1	0 * 26 + w(14) + 3
t1	z * 26 + w(13) + 12
t1	z * 26 + w(12) + 9
t2	26 * (z / 26) + w(11) + 12 or z/26 -- (-6)
t1	z * 26 + w(10) + 2
t2	26 * (z / 26) + w(9) + 1 or z/26 -- (-8)
t2	26 * (z / 26) + w(8) + 1 or z/26 -- (-4)
t1	z * 26 + w(7) + 13
t1	z * 26 + w(6) + 1
t1	z * 26 + w(5) + 6
t2	26 * (z / 26) + w(4) + 2 or z/26 -- (-11)
t2	26 * (z / 26) + w(3) + 11 or z/26 -- (0)
t2	26 * (z / 26) + w(2) + 10 or z/26 -- (-8)
t2	26 * (z / 26) + w(1) + 3 or z/26 -- (-7)
7	<- t1	14,13,12,10,7,6,5
7	<- t2	11,9,8,4,3,2,1
-}

-- Left = increments, Right = fixed point
algoTypes :: [Either Int Int]

algoTypes = [
  Left   3, -- 14
  Left  12, -- 13
  Left   9, -- 12
  Right  6, -- 11
  Left   2, -- 10
  Right  8, -- 9
  Right  4, -- 8
  Left  13, -- 7
  Left   1, -- 6
  Left   6, -- 5
  Right 11, -- 4
  Right  0, -- 3
  Right  8, -- 2
  Right  7  -- 1
  ]

solve :: [String] -> [Int]
solve _ = head . dropWhile (== []) . map srch $ ps
  where ns = [9,8..1]
        ps = [[w14, w13, w12, w10, w7, w6, w5] | w14 <- ns, w13 <- ns, w12 <- ns, w10 <- ns, w7 <- ns, w6 <- ns, w5 <- ns] -- possibilities

srch :: [Int] -> [Int]
srch p = srch' [] 0 p algoTypes
  where -- t1: e.g. 0 * 26 + w + i
        srch' mns z (w:ws) ((Left i):as) = srch' (w:mns) z' ws as
          where z' = z * 26 + w + i
        -- t2: e.g. 26 * (z / 26) + w + 12 or z/26 -- (-6)
        srch' mns z ws ((Right f):as)
          | 1 <= w && w <= 9 = srch' (w:mns) z' ws as
          | otherwise = []
          where w = z `mod` 26 - f
                z' = z `quot` 26
        -- exit cases
        srch' mns 0 _ [] = reverse mns
        srch' mns _ _ [] = [] -- should be pruned b/f getting here


main :: IO ()
main = interact $ show . solve . lines

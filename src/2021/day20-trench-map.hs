module Main where
import Data.Maybe (fromMaybe)
import qualified Data.Vector as V (Vector, concatMap, filter, fromList, length, (!), (!?))

--	ref: https://adventofcode.com/2021/day/20
--
-- Test:
--	% cd ../.. && time cabal run 2021_day20-trench-map < src/2021/20a.tst ## long-ish test set

solve :: [String] -> Int
solve (inalgo:_:inpxs) = count . (!! 2) . iterate (enhance algo) $ pxs
  where algo = V.fromList inalgo
        pxs = V.fromList . map V.fromList $ (replicate (length . head $ inpxs) '.':inpxs) -- input pxs (pixels)
        count = V.length . V.concatMap (V.filter (== '#'))

enhance :: V.Vector Char -> V.Vector (V.Vector Char) -> V.Vector (V.Vector Char)
enhance algo pxs = V.fromList [V.fromList [algo V.! getNbr x y pxs | x <- [-1..lx]] | y <- [-2..ly]] -- start y at -2 to keep the "canary" pixels between enhancements
  where ly = V.length pxs
        lx = V.length $ pxs V.! 0
        def = pxs V.! 0 V.! 0 -- we replicated in a "canary" line of pixels that will always be the "background color" of the "universe"
        convert = foldl' (\a ch -> a * 2 + (if '.' == ch then 0 else 1)) 0
        --
        getNbr :: Int -> Int -> V.Vector (V.Vector Char) -> Int
        getNbr x y pxs = convert [getPx x y | y <- [y-1..y+1], x <- [x-1..x+1]]
        --
        getPx x y = fromMaybe def $ do
          r <- pxs V.!? y
          r V.!? x


main :: IO ()
main = interact $ show . solve . lines

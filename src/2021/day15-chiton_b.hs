module Main where
import qualified Data.Array as A (Array, bounds, inRange, listArray, range, (!))
import Data.Char (digitToInt)
import qualified Data.Set as S (Set, deleteFindMin, empty, insert, member, notMember, singleton)

--	... The entire cave is actually five times larger in both dimensions than you thought; the area you originally scanned is
--	just one tile in a 5x5 tile area that forms the full map. Your original map tile repeats to the right and downward; each
--	time the tile repeats to the right or downward, all of its risk levels are 1 higher than the tile immediately up or left
--	of it. However, risk levels above 9 wrap back around to 1.
--
-- Test:
--	% cd ../.. && echo -e '1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581' | cabal run 2021_day15-chiton_b ## 315

-- Algorithm: https://en.wikipedia.org/wiki/Pathfinding
-- *and*  https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm

expand :: [String] -> [[Int]]
expand ls = concatMap (\n -> map (quint . map (msucc n)) dls) [0..4]
  where dls = map (map digitToInt) ls
        msucc n i = let n' = n + i
                    in if n' <= 9 then n' else n' - 9
        quint = concat . take 5 . iterate (map (`msucc` 1))

solve :: [[Int]] -> Int
solve ls = dijkstra S.empty $ S.singleton (0, begPt)
  where cm :: A.Array (Int, Int) Int -- cm (cave map) is an array of points (y,x) of risk (int)
        cm = A.listArray (begPt, endPt) . concat $ ls
        ly = pred . length $ ls
        lx = pred . length . head $ ls
        begPt = (0, 0)
        endPt = (ly, lx)
        --
        -- Visit surrounding cave "square"s.
        dijkstra :: S.Set (Int, Int) -> S.Set (Int, (Int, Int)) -> Int
        dijkstra vps ps = nextStep (r, pt) ps'
          where ((r, pt), ps') = S.deleteFindMin ps
                nextStep (r, pt) ns
                  | pt == endPt       = r
                  | pt `S.member` vps = dijkstra vps ns
                  | otherwise         = dijkstra (S.insert pt vps) nbrs
                --
                -- Upto 4 neighbors per `dijkstra` visit point.
                nbrs = foldl' (flip S.insert) ps' [(r + cm A.! n, n) | n <- findNbrs pt, n `S.notMember` vps]
                findNbrs (y, x) = [nbr | nbr <- [(y - 1, x), (y, x + 1), (y + 1, x), (y, x - 1)], A.inRange (A.bounds cm) nbr]


main :: IO ()
main = interact $ show . solve . expand . lines

module Main where
import Data.Bool (bool)
import Data.List (find, sort, (\\))
import Data.Maybe (isNothing)
import qualified Data.Set as S (Set, deleteFindMin, empty, fromList, insert, member, union)

--	Move all players home
--
-- Test:
--	% cd ../.. && echo -e '#############\n#...........#\n###B#C#B#D###\n  #A#D#C#A#\n  #########' | cabal run 2021_day23-amphipod_b ## 44169

-- Note: reuse 'dijkstra' algorithm from day15; I didn't polish the code once it ran correctly on test data.

{-
Amber amphipods require 1 energy per step,
Bronze amphipods require 10 energy,
Copper amphipods require 100, and
Desert ones require 1000. The
-}

maxDepth = 4

energy 'A' = 1
energy 'B' = 10
energy 'C' = 100
energy 'D' = 1000
energy _   = 0

roomEnergy 2 = 1
roomEnergy 4 = 10
roomEnergy 6 = 100
roomEnergy 8 = 1000

energyRoom 1    = 2
energyRoom 10   = 4
energyRoom 100  = 6
energyRoom 1000 = 8

hallway = [0..10] \\ [2,4,6,8] -- all hallway points except the "no-parking" zones

type Point = (Int, Int)
type Player = (Point, Int, Int) -- Location, Wandering? (1=yes, 0=home), Cost

-- Note: players are sorted in State so permutations match, maybe a Map would compare and be more efficient?
type State = (Int, [Player]) -- Cost of this move, Player's potential move


solve :: [String] -> Int
solve ls = dijkstra S.empty . nextStates 0 . parse . patch $ ls
  where parse :: [String] -> [Player]
        parse = filter (\(_, _, e) -> e > 0) . -- filter down to just the players
                concatMap (\(r, cs) -> map (\(c, ch) -> let pe = energy ch in ((c, r), bool 1 0 $ maxDepth == r && pe == roomEnergy c, pe)) cs) . -- make cells for everything and flatten
                zip [-1..] . -- add row numbers
                map (zip [-1..]) -- add col numbers
        patch ls = take 3 ls ++ lines "  #D#C#B#A#\n  #D#B#A#C#\n" ++ drop 3 ls

win :: [Player] -> Bool
win = (== 0) . foldl' (\acc (_, h, _) -> acc + h) 0

-- Find all hallway moves and all moves into home position and how much that would cost
nextStates :: Int -> [Player] -> S.Set State
nextStates c ps = S.fromList ((concatMap allPoints . filter canMove $ ps) ++ concatMap allHomeMoves ps)
  where canMove :: Player -> Bool -- Can move if (*) in the hallway or top of room; or (*) in bottom and above spaces are empty
        canMove p@((_, y), h, _) = h /= 0 && (y == 1 || (y >= 2 && canMoveOut p))
        canMoveOut ((x, y), h, _) = isNothing (find (\((x', y'), _, _) -> x == x' && y' < y) ps)
        --
        allPoints :: Player -> [State]
        allPoints p@((x,_), h, e) = map (\x' -> (cost p (x', 0), sort $ ((x', 0), h, e):ps')) . filter (clearPath p ps') $ (hallway \\ [x])
          where ps' = ps \\ [p]
        --
        allHomeMoves :: Player -> [State]
        allHomeMoves (_, 0, _) = [] -- already home, no more moves :)
        allHomeMoves p@(_, _, e) = -- in the hallway, looking for a home
          let ps' = ps \\ [p] -- players except "me"
              rx  = energyRoom e -- room #
              rs  = filter (\((x, y), _, _) -> x == rx && y > 0) ps -- players in the room
              opn = canMoveOut p && -- nothing above us?
                isNothing (find (\(_, h', e') -> h' /= 0 || e' /= e) rs) && -- a wandering amphipod or a different color blocking home?
                clearPath p ps' rx -- clear path thru the hallway?
              hpt = (rx, maxDepth - length rs) -- point open in home room
          in bool [] [(cost p hpt, sort $ (hpt, 0, e):ps')] opn
        --
        cost :: Player -> Point -> Int
        cost ((x, y), _, e) (x', y') = c + e * (abs (x' - x) + y + y')
        --
        clearPath :: Player -> [Player] -> Int -> Bool -- is another player in the way?
        clearPath ((x, _), _, _) ps hx = isNothing $ find (\((bx, by), _, _) -> 0 == by && ((hx <= bx && bx <= x) || (x <= bx && bx <= hx))) ps

dijkstra :: S.Set [Player] -> S.Set State -> Int
dijkstra vps ss
  | win ps            = c
  | ps `S.member` vps = dijkstra vps ss'
  | otherwise         = dijkstra (S.insert ps vps) (S.union ss' (nextStates c ps))
  where ((c, ps), ss') = S.deleteFindMin ss

main :: IO ()
main = interact $ show . solve . lines

{-
Dijkstra with breadcrumbs to print solution step-by-step; helped me debug:

Invoke:
dijkstra (M.empty, M.empty) S.empty . nextStates 0 . parse

-- Crude printout w/ trace; but with a simple keyboard macro in emacs, this did the trick:
printBc :: Int -> [Player] -> (M.Map [Player] [Player], M.Map [Player] Int) -> Int
printBc c ps (bc, cc) = tracePs (Just ps)
  where tracePs Nothing = c
        tracePs (Just ps) = trace (show ("tracePs", cc M.!? ps, str ps, homeStr ps, ps)) (tracePs $ bc M.!? ps)
        ltr (Just (_, _, 1))    = 'A'
        ltr (Just (_, _, 10))   = 'B'
        ltr (Just (_, _, 100))  = 'C'
        ltr (Just (_, _, 1000)) = 'D'
        str ps = map (\hx ->
                     let p = find (\((x, y), _, _) -> x == hx && y == 0) ps
                     in if (isNothing p) then '.' else ltr p
                  ) [0..10]
        homeStr ps = map (\(hx,hy) ->
                            let p = find (\((x, y), _, _) -> x == hx && y == hy) ps
                            in if (isNothing p) then '.' else ltr p
                         ) [(2,1),(4,1),(6,1),(8,1),(2,2),(4,2),(6,2),(8,2),(2,3),(4,3),(6,3),(8,3),(2,4),(4,4),(6,4),(8,4)]

dijkstra :: (M.Map [Player] [Player], M.Map [Player] Int) -> S.Set [Player] -> S.Set State -> Int
dijkstra (bc, cc) vps ss
  | win ps            = trace (show ("a", ps)) printBc c ps (bc, cc)
  | ps `S.member` vps = {-trace "b" $-} dijkstra (bc, cc) vps ss'
  | otherwise         = -- {-trace (show ("c", c, ps)) $-} (if c `mod` 1000 < 10 then trace (show (c, ps)) else id) $
                        dijkstra (bc', cc') (S.insert ps vps) (S.union ss' nss)
  where ((c, ps), ss') = S.deleteFindMin ss
        nss = nextStates c ps
        bc' = S.foldl' (\bc' (_, ns') -> M.insert ns' ps bc') bc nss
        cc' = M.insert ps c cc
-}

module Main where
import Control.Arrow ((&&&))
import Data.List (group, sort)
import Data.List.Extra (groupOn)
import Data.List.Split (splitOn)

--	... lanternfish that creates a new fish resets its timer to 6, not 7 (because 0 is included as a valid timer value). The
--	new lanternfish starts with an internal timer of 8 and does not start counting down until the next day.
--
--	3,4,3,1,2
--
--	This list means that the first fish has an internal timer of 3, the second fish has an internal timer of 4, and so on
--	until the fifth fish, which has an internal timer of 2.
--
-- Test:
--	% cd ../.. && echo -e '3,4,3,1,2' | cabal run 2021_day06-lanternfish_b # 5934

applyN n = foldr (.) id . replicate n

-- optimize space usage: group fish into "timer groups" (all fish on the same reproductive cycle) w/ count.
sortFish :: [Int] -> [(Int, Int)]
sortFish = map (head &&& length) . group . sort

solve :: [Int] -> Int
solve = sum . map snd . applyN 256 sim' . sortFish
  where sim newFs (0, cnt) = (6, cnt):(8, cnt):newFs -- reset cnt fish and spawn cnt fish
        sim newFs (t, cnt) = (t - 1, cnt):newFs      -- decrement cnt fish
        sim' = map (foldl1' (\(t, c1) (_, c2) -> (t, c1 + c2))) . groupOn fst . sort . foldl' sim [] -- create new generation and combine fish on same cycle again.


main :: IO ()
main = interact $ show . solve . map (read :: String -> Int) . splitOn ","

module Main where
import Data.Bifunctor (second)
import Data.List (group, groupBy, last, length, sort)
import qualified Data.Map.Strict as M (fromList, lookup)
import Data.Maybe (fromMaybe)

--	... You just need to work out what polymer would result after repeating the pair insertion process a few times.
--
--	For example:
--
--	NNCB
--
--	CH -> B
--	HH -> N
--	CB -> H
--	NH -> C
--	HB -> C
--	HC -> B
--	HN -> C
--	NN -> C
--	BH -> H
--	NC -> B
--	NB -> B
--	BN -> B
--	BB -> N
--	BC -> B
--	CC -> N
--	CN -> C
--
--	A rule like AB -> C means that when elements A and B are immediately adjacent, element C should be inserted between them. These insertions all happen simultaneously.
--
--	So, starting with the polymer template NNCB, the first step simultaneously considers all three pairs:
--
--	* The first pair (NN) matches the rule NN -> C, so element C is inserted between the first N and the second N.
--	* The second pair (NC) matches the rule NC -> B, so element B is inserted between the N and the C.
--	* The third pair (CB) matches the rule CB -> H, so element H is inserted between the C and the B.
--
--	Note that these pairs overlap: the second element of one pair is the first element of the next pair. Also, because all
--	pairs are considered simultaneously, inserted elements are not considered to be part of a pair until the next step.
--
--	After the first step of this process, the polymer becomes NCNBCHB.
--
--	Here are the results of a few steps using the above rules:
--
--	Template:     NNCB
--	After step 1: NCNBCHB
--	After step 2: NBCCNBBBCBHCB
--	After step 3: NBBBCNCCNBBNBNBBCHBHHBCHB
--	After step 4: NBBNBNBBCCNBCNCCNBBNBBNBBBNBBNBBCBHCBHHNHCBBCBHCB
--
--	Apply 10 steps of pair insertion to the polymer template and find the most and least common elements in the result. *What
--	do you get if you take the quantity of the most common element and subtract the quantity of the least common element?*
--
--	... taking the quantity of the most common element (B, 1749) and subtracting the quantity of the least common element (H, 161)
--	produces 1749 - 161 = *1588*.
--
-- Test:
--	% cd ../.. && echo -e 'NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C' | cabal run 2021_day14-extended-polymerization ## 1588

applyN n = foldr (.) id . replicate n

solve :: (String, [(String, Char)]) -> Int
solve (tmplt, rs) = (\ns -> last ns - head ns) . sort . map length . group . sort . applyN 10 insertion $ tmplt
  where ms = M.fromList rs
        insertion [e1]       = [e1]
        insertion (e1:e2:es) = [e1, fromMaybe '#' $ M.lookup [e1, e2] ms] ++ insertion (e2:es)


main :: IO ()
main = interact $ show . solve . (\[tmplt, rs] -> (head tmplt, map (second (!! 4) . span (/= ' ')) . tail $ rs)) . groupBy (\a b -> "" /= b) . lines

module Main where
import Data.Bool (bool)
import Data.Char (isDigit)

--	Turn on and off cuboids, how many are on in the -50..50^3 initialization grid?
--
-- Test:
--	% cd ../.. && echo -e 'on x=10..12,y=10..12,z=10..12\non x=11..13,y=11..13,z=11..13\noff x=9..11,y=9..11,z=9..11\non x=10..10,y=10..10,z=10..10\non x=-54112..-39298,y=-85059..-49293,z=-27449..7877' | cabal run 2021_day22-reactor-reboot_b ## !39
--	% cd ../.. && cabal run 2021_day22-reactor-reboot_b < src/2021/22b.tst # 2,758,514,936,282,235

{-
find overlapping 3d cube intersection
https://gamedev.stackexchange.com/questions/130408/what-is-the-fastest-algorithm-to-check-if-two-cubes-intersect-where-the-cubes-a
https://stackoverflow.com/questions/5009526/overlapping-cubes
https://github.com/iskyd/advent-of-code-julia/blob/main/2021/day22/main.jl
-}

type Cube = [Int] -- [xmin, xmax, ymin, ymax, zmin, zmax]
type Step = (Int, Cube) -- (1=on|0=off, Cube)

solve :: [String] -> Int
solve = sum . map volume . solution [] . map parse
  where parse :: String -> Step
        parse ('o':dir:_:_:cs) = (bool (-1) 1 $ 'n' == dir, map read . words . map (\c -> bool ' ' c $ isDigit c || '-' == c) $ cs)
        volume (val, [x0, x1, y0, y1, z0, z1]) = val * (x1 - x0 + 1) * (y1 - y0 + 1) * (z1 - z0 + 1)

solution :: [Step] -> [Step] -> [Step]
solution acc [] = acc
solution acc (c@(val, [x0, x1, y0, y1, z0, z1]):cs) = let is = concatMap findIntersect acc
                                                      in solution (acc ++ bool is (c:is) (1 == val)) cs
  where findIntersect :: Step -> [Step]
        findIntersect (aval, [ax0, ax1, ay0, ay1, az0, az1])
          | x0 > ax1 || x1 < ax0 ||
            y0 > ay1 || y1 < ay0 ||
            z0 > az1 || z1 < az0 = [] -- no overlap, just report c
          | otherwise = let x0' = max x0 ax0
                            x1' = min x1 ax1
                            y0' = max y0 ay0
                            y1' = min y1 ay1
                            z0' = max z0 az0
                            z1' = min z1 az1
                            -- If both are "on" or "off", then this intersection is negated (don't double count the intersection, 2 "on"s or 2 "off"s)
                            -- If this one is "on" and that one is "off", then positive value (the "on" overrides the previous "off")
                            -- Else on*off = toggle
                            val'
                              | val == aval = -val
                              | val == 1 && aval == -1 = 1
                              | otherwise = val * aval
                        in [(val', [x0', x1', y0', y1', z0', z1'])]


main :: IO ()
main = interact $ show . solve . lines

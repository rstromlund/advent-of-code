module Main where
--	...
--	forward 5
--	down 5
--	forward 8
--	up 3
--	down 8
--	forward 2
--
--	...After following these instructions, you would have a horizontal position of 15 and a depth of 10. (Multiplying these together produces 150.)
--
-- Test:
--	% cd ../.. && echo -e 'forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2' | cabal run 2021_day02-dive # = 150


solve :: [[String]] -> Int
solve = (\(d, f, _) -> d * f) . foldl' move (0, 0, 0)
  where move (d, f, a) ["forward", n] = let x = read n in (d + x * a, f + x, a)
        move (d, f, a) ["down", n]    = (d, f, a + read n)
        move (d, f, a) ["up", n]      = (d, f, a - read n)

main :: IO ()
main = interact $ show . solve . map words . lines

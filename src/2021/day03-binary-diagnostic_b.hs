module Main where
import Data.List (transpose)

--	...Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:
--	00100
--	11110
--	10110
--	10111
--	10101
--	01111
--	00111
--	11100
--	10000
--	11001
--	00010
--	01010
--
--	oxygen generator rating:
--	* Start with all 12 numbers and consider only the first bit of each number. There are more 1 bits (7) than 0 bits (5), so
--	keep only the 7 numbers with a 1 in the first position: 11110, 10110, 10111, 10101, 11100, 10000, and 11001.
--
--	* Then, consider the second bit of the 7 remaining numbers: there are more 0 bits (4) than 1 bits (3), so keep only the 4
--	numbers with a 0 in the second position: 10110, 10111, 10101, and 10000.
--
--	* In the third position, three of the four numbers have a 1, so keep those three: 10110, 10111, and 10101.
--
--	* In the fourth position, two of the three numbers have a 1, so keep those two: 10110 and 10111.
--
--	* In the fifth position, there are an equal number of 0 bits and 1 bits (one each). So, to find the oxygen generator rating,
--	keep the number with a 1 in that position: 10111.
--
--	* As there is only one number left, stop; the oxygen generator rating is 10111, or 23 in decimal.
--

--	CO2 scrubber rating:
--
--	* Start again with all 12 numbers and consider only the first bit of each number. There are fewer 0 bits (5) than 1 bits
--	(7), so keep only the 5 numbers with a 0 in the first position: 00100, 01111, 00111, 00010, and 01010.
--
--	* Then, consider the second bit of the 5 remaining numbers: there are fewer 1 bits (2) than 0 bits (3), so keep only the 2
--	numbers with a 1 in the second position: 01111 and 01010.
--
--	* In the third position, there are an equal number of 0 bits and 1 bits (one each). So, to find the CO2 scrubber rating,
--	keep the number with a 0 in that position: 01010.
--
--	* As there is only one number left, stop; the CO2 scrubber rating is 01010, or 10 in decimal.
--
-- Test:
--	% cd ../.. && echo -e '00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010' | cabal run 2021_day03-binary-diagnostic_b # = 23*10=230


solve :: [String] -> Int
solve ns = bintoint 0 gamma * bintoint 0 epsilon
  where bs = map (map ('1' ==)) ns
        gamma   = fltr mostCmn         [] bs
        epsilon = fltr (not . mostCmn) [] bs
        mostCmn str = let zeros = count False str
                          ones  = length str - zeros
                      in ones >= zeros
        count digit str = length . filter (digit ==) $ str
        fltr _ acc [r]  = acc ++ r
        fltr f acc rs   = let a    = f . head . transpose $ rs
                              acc' = acc ++ [a]
                              rs'  = map tail $ filter ((== a) . head) rs
                          in fltr f acc' rs'

bintoint :: Int -> [Bool] -> Int
bintoint acc []         = acc
bintoint acc (False:ds) = bintoint (2 * acc) ds
bintoint acc (True:ds)  = bintoint (2 * acc + 1) ds


main :: IO ()
main = interact $ show . solve . words

module Main where
import Data.Char (isDigit)
import Data.List (filter, group, sort)
import Data.List.Split (wordsBy)

--	...your list will only ever be horizontal, vertical, or a diagonal line at exactly 45 degrees
--	* An entry like 1,1 -> 3,3 covers points 1,1, 2,2, and 3,3.
--	* An entry like 9,7 -> 7,9 covers points 9,7, 8,8, and 7,9.
--
-- Test:
--	% cd ../.. && echo -e '0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2' | cabal run 2021_day05-hydrothermal-venture_b # 12

solve :: [String] -> Int
solve = length . filter (\g -> length g > 1) . group . sort . concatMap (draw . readPt)
  where draw ((x1, y1), (x2, y2))
          | x1 == x2  = [(x1, y) | y <- [(min y1 y2)..(max y1 y2)]]
          | y1 == y2  = [(x, y1) | x <- [(min x1 x2)..(max x1 x2)]]
          | otherwise = let l  = abs $ x2 - x1
                            dx = signum $ x2 - x1
                            dy = signum $ y2 - y1
                        in [(x1 + i * dx, y1 + i * dy) | i <- [0..l]]

readPt :: String -> ((Int, Int), (Int, Int))
readPt = (\[x1, y1, x2, y2] -> ((x1, y1), (x2, y2))) . map read . wordsBy (not . isDigit)


main :: IO ()
main = interact $ show . solve . lines

module Main where

--	... each line is a measurement of the sea floor depth as the sweep looks further and further away from the submarine.
--
--	For example, suppose you had the following report:
--
--	199
--	200
--	208
--	210
--	200
--	207
--	240
--	269
--	260
--	263
--
--	...count the number of times a depth measurement increases from the previous measurement
--
-- Test:
--	% cd ../.. && echo -e '199\n200\n208\n210\n200\n207\n240\n269\n260\n263' | cabal run 2021_day01-sonar_sweep_b # = 5


solve :: [Int] -> Int
solve ns = length . filter (== True) $ zipWith (<) ss (tail ss)
  where ss = zipWith3 (\a b c -> a + b + c) ns (tail ns) (tail $ tail ns)


main :: IO ()
main = interact $ show . solve . map read . words

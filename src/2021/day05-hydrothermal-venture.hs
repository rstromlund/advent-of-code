module Main where
import Data.Char (isDigit)
import Data.List (filter, group, sort)
import Data.List.Split (wordsBy)

--	...
--	0,9 -> 5,9
--	8,0 -> 0,8
--	9,4 -> 3,4
--	2,2 -> 2,1
--	7,0 -> 7,4
--	6,4 -> 2,0
--	0,9 -> 2,9
--	3,4 -> 1,4
--	0,0 -> 8,8
--	5,5 -> 8,2
--
--	...For now, only consider horizontal and vertical lines: lines where either x1 = x2 or y1 = y2.
--	...Consider only horizontal and vertical lines. At how many points do at least two lines overlap?
--
-- Test:
--	% cd ../.. && echo -e '0,9 -> 5,9\n8,0 -> 0,8\n9,4 -> 3,4\n2,2 -> 2,1\n7,0 -> 7,4\n6,4 -> 2,0\n0,9 -> 2,9\n3,4 -> 1,4\n0,0 -> 8,8\n5,5 -> 8,2' | cabal run 2021_day05-hydrothermal-venture # 5

solve :: [String] -> Int
solve = length . filter (\g -> length g > 1) . group . sort . concatMap (draw . readPt)
  where draw ((x1, y1), (x2, y2))
          | x1 == x2  = [(x1, y) | y <- [(min y1 y2)..(max y1 y2)]]
          | y1 == y2  = [(x, y1) | x <- [(min x1 x2)..(max x1 x2)]]
          | otherwise = []

readPt :: String -> ((Int, Int), (Int, Int))
readPt = (\[x1, y1, x2, y2] -> ((x1, y1), (x2, y2))) . map read . wordsBy (not . isDigit)


main :: IO ()
main = interact $ show . solve . lines

module Main where
import qualified Data.List   as L (all, foldl', product, sortBy)
import qualified Data.Set    as S (fromList, insert, member, size)
import qualified Data.Vector as V (foldl', fromList, length, map, (!))

--	Next, you need to find the largest basins so you know what areas are most important to avoid.
--
--	A basin is all locations that eventually flow downward to a single low point. Therefore, every low point has a basin,
--	although some basins are very small. Locations of height 9 do not count as being in any basin, and all other locations
--	will always be part of exactly one basin.
--
--	The size of a basin is the number of locations within the basin, including the low point. The example above has four basins.
--
--	Find the three largest basins and multiply their sizes together. In the above example, this is 9 * 14 * 9 = 1134.
--
-- Test:
--	% cd ../.. && echo -e '2199943210\n3987894921\n9856789892\n8767896789\n9899965678' | cabal run 2021_day09-smoke-basin_b ## 1134

nbrDirs = [(0, -1), (-1, 0), (1, 0), (0, 1)]

solve :: [String] -> Int
solve ls = L.product . take 3 . L.sortBy (flip compare) . map S.size $ ms
  where hm = V.fromList $ map V.fromList ls -- hm (heightmap) is a vector of vectors
        ly = V.length hm
        lx = V.length $ hm V.! 0
        -- fold over the rows and columns looking for basins
        (_, ms) = V.foldl' (\(y, mins) row -> (\(_, _, mins') -> (y + 1, mins')) . V.foldl' f (0, y, mins) $ row) (0, []) hm
        getHeight x y
          | x >= 0 && y >= 0 && x < lx && y < ly = hm V.! y V.! x
          | otherwise = '9' -- put a "barrier" around the perimeter
        f (x, y, mins) ch = let ns = map (\(dx, dy) -> getHeight (x + dx) (y + dy)) nbrDirs
                                -- ns = the neighboring heights
                                mins'
                                  | L.all (ch <) ns = crawl x y (S.fromList [(x, y)]):mins
                                  | otherwise     = mins
                            in (x + 1, y, mins')
        crawl x y ns = L.foldl' g ns nbrDirs
          where g ns (dx, dy) = let x' = x + dx
                                    y' = y + dy
                                    ch = getHeight x' y'
                                    s  = (x', y')
                                in if '9' == ch || S.member s ns then ns else crawl x' y' $ S.insert s ns


main :: IO ()
main = interact $ show . solve . lines

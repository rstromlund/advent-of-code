module Main where
import Data.List (transpose)

--	...Each bit in the gamma rate can be determined by finding the most common bit in the corresponding position of all numbers in the diagnostic report. For example, given the following diagnostic report:
--	00100
--	11110
--	10110
--	10111
--	10101
--	01111
--	00111
--	11100
--	10000
--	11001
--	00010
--	01010
--
--	Considering only the first bit of each number, there are five 0 bits and seven 1 bits. Since the most common bit is 1, the first bit of the gamma rate is 1.
--
-- Test:
--	% cd ../.. && echo -e '00100\n11110\n10110\n10111\n10101\n01111\n00111\n11100\n10000\n11001\n00010\n01010' | cabal run 2021_day03-binary-diagnostic # = 10110*10110, (22*9), 198


solve :: [String] -> Int
solve ns = bintoint 0 gamma * bintoint 0 epsilon
  where gamma   = map mostCmn $ transpose ns
        epsilon = map not gamma
        mostCmn str = count '1' str > count '0' str
        count digit str = length . filter (digit ==) $ str

bintoint :: Int -> [Bool] -> Int
bintoint acc []         = acc
bintoint acc (False:ds) = bintoint (2 * acc) ds
bintoint acc (True:ds)  = bintoint (2 * acc + 1) ds


main :: IO ()
main = interact $ show . solve . words

module Main where
import qualified Data.List as L (all)
import qualified Data.Vector as V (foldl', fromList, length, (!))

--	... heightmap:
--
--	2199943210 [low @ 1 and 0]
--	3987894921
--	9856789892 [low @ 5]
--	8767896789
--	9899965678 [low @ 5]
--
--	Your first goal is to find the low points - the locations that are lower than any of its adjacent locations.
--	(Diagonal locations do not count as adjacent.)
--
--	The risk level of a low point is 1 plus its height. In the above example, the risk levels of the low points are 2, 1, 6,
--	and 6. The sum of the risk levels of all low points in the heightmap is therefore 15.
--
-- Test:
--	% cd ../.. && echo -e '2199943210\n3987894921\n9856789892\n8767896789\n9899965678' | cabal run 2021_day09-smoke-basin ## 15

solve :: [String] -> Int
solve ls = sum . map (\ch -> fromEnum ch - fromEnum '0' + 1) $ ms
  where hm = V.fromList $ map V.fromList ls -- hm (heightmap) is a vector of vectors
        ly = V.length hm
        lx = V.length $ hm V.! 0
        -- fold over the rows and columns looking for basins
        (_, ms) = V.foldl' (\(y, mins) row -> (\(_, _, mins') -> (y + 1, mins')) . V.foldl' f (0, y, mins) $ row) (0, []) hm
        getHeight x y
          | x >= 0 && y >= 0 && x < lx && y < ly = hm V.! y V.! x
          | otherwise = '9' -- put a "barrier" around the perimeter
        f (x, y, mins) ch = let -- ns = the neighboring heights
                                ns = getHeight x (y - 1):getHeight (x - 1) y:getHeight (x + 1) y:[getHeight x (y + 1)]
                                mins'
                                  | L.all (ch <) ns = ch:mins
                                  | otherwise       = mins
                            in (x + 1, y, mins')


main :: IO ()
main = interact $ show . solve . lines

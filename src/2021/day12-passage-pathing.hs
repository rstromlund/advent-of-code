module Main where
import Data.Char (isUpper)
import Data.List (concatMap, elem, filter, length)
import Data.List.Split (splitOn)

--	For example:
--
--	start-A
--	start-b
--	A-c
--	A-b
--	b-d
--	A-end
--	b-end
--	This is a list of how all of the caves are connected. You start in the cave named start, and your destination is the cave named end. An entry like b-d means that cave b is connected to cave d - that is, you can move between them.
--
--	So, the above cave system looks roughly like this:
--
--	    start
--	    /   \
--	c--A-----b--d
--	    \   /
--	     end

--	... Your goal is to find the number of distinct paths that start at start, end at end, and don't visit small caves more
--	than once. There are two types of caves: big caves (written in uppercase, like A) and small caves (written in lowercase,
--	like b). It would be a waste of time to visit any small cave more than once, but big caves are large enough that it might
--	be worth visiting them multiple times. So, all paths you find should visit small caves at most once, and can visit big
--	caves any number of times.
--
--	Given these rules, there are 10 paths through this example cave system:
--
--	start,A,b,A,c,A,end
--	start,A,b,A,end
--	start,A,b,end
--	start,A,c,A,b,A,end
--	start,A,c,A,b,end
--	start,A,c,A,end
--	start,A,end
--	start,b,A,c,A,end
--	start,b,A,end
--	start,b,end
--
--	(Each line in the above list corresponds to a single path; the caves visited by that path are listed in the order they are visited and separated by commas.)
--
--	Note that in this cave system, cave d is never visited by any path: to do so, cave b would need to be visited twice (once
--	on the way to cave d and a second time when returning from cave d), and since cave b is small, this is not allowed.
--
-- Test:
--	% cd ../.. && echo -e 'start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end' | cabal run 2021_day12-passage-pathing ## 10
--	% cd ../.. && echo -e 'dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc' | cabal run 2021_day12-passage-pathing ## 19

solve :: [[String]] -> Int
solve lns = length . visit $ ["start"]
  where ns = [(a, b) | [a, b] <- lns] ++ [(a, b) | [b, a] <- lns]
        candidates node = map snd . filter ((==) node . fst) $ ns
        -- small caves
        skipCaves :: [String] -> String -> Bool
        skipCaves _ (ch:_) | isUpper ch = False -- big? go there.
        skipCaves ps c = c `elem` ps -- been there?
        -- recursive path builder
        visit :: [String] -> [[String]]
        visit ps@("end":_) = [ps]
        visit ps@(p:_)     = concatMap (\c -> visit (c:ps)) cs
          where cs = filter (not . skipCaves ps) $ candidates p


main :: IO ()
main = interact $ show . solve . map (splitOn "-") . lines

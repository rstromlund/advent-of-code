module Main where
import Data.Char (digitToInt)
import qualified Data.List   as L (foldl')
import qualified Data.Set    as S (empty, insert, member, size)
import qualified Data.Vector as V (Vector, fromList, length, map, (!), (//))

--	... If you can calculate the exact moments when the octopuses will all flash simultaneously, you should be able to
--	navigate through the cavern. What is the first step during which all octopuses flash?
--
-- Test:
--	% cd ../.. && echo -e '5483143223\n2745854711\n5264556173\n6141336146\n6357385478\n4167524645\n2176841721\n6882881134\n4846848554\n5283751526' | cabal run 2021_day11-dumbo-octopus ## 195

nbrDirs = [
  (-1, -1), (0, -1), (1, -1),
  (-1,  0),          (1,  0),
  (-1,  1), (0,  1), (1,  1)
  ]

solve :: [String] -> Int
solve ls = search (V.length om * V.length (om V.! 0)) om
  where om = V.fromList . map (V.fromList . map digitToInt) $ ls -- om (octopus map) is a vector of vectors

search :: Int -> V.Vector (V.Vector Int) -> Int
search tot v = search' 1 $ step (v, 0)
  where search' scnt (v, fcnt)
          | tot == fcnt = scnt
          | otherwise   = search' (succ scnt) $ step (v, 0)

step :: (V.Vector (V.Vector Int), Int) -> (V.Vector (V.Vector Int), Int)
step (v, cnt) = (\(v', s') -> (v', cnt + S.size s')) . L.foldl' (\acc y -> fst . L.foldl' xpld (acc, y) $ [0..(lx - 1)]) (next v, S.empty) $ [0..(ly - 1)]
  where ly = V.length v
        lx = V.length $ v V.! 0
        getEnergy x y om
          | x >= 0 && y >= 0 && x < lx && y < ly = om V.! y V.! x
          | otherwise = -999
        setEnergy x y e om
          | x >= 0 && y >= 0 && x < lx && y < ly = om V.// [(y, om V.! y V.// [(x, e)])]
          | otherwise = om
        next v  = V.map (V.map succ) v
        xpld ((om, es), y) x = xpld' ((om, es), y) x $ getEnergy x y om
        xpld' ((om, es), y) x e
          | e <= 9 = ((om, es), y) -- not exploding
          | S.member (x, y) es = ((om, es), y) -- already exploded
          | otherwise = L.foldl' (\((omf, esf), _) (dx, dy) ->
                                    -- Light up all our neighbors
                                    let x'   = x + dx
                                        y'   = y + dy
                                        e'   = 1 + getEnergy x' y' omf
                                        omf' = setEnergy x' y' e' omf
                                    in if S.member (x', y') esf then ((omf, esf), y) else (\(acc, _) -> (acc, y)) . xpld' ((omf', esf), y') x' $ e'
                                    --  Curious thing, w/o the function in front of "xpld'"; this breaks out of fold!!! ^^^^ tail recursion breaks from fold?! Or updates "y"?!
              ) ((setEnergy x y 0 om, S.insert (x, y) es), y) nbrDirs


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Control.Arrow ((&&&))
import Data.List (group, sort)
import Data.List.Split (splitOn)

--	... For example, consider the following horizontal positions:
--
--	As it turns out, crab submarine engines don't burn fuel at a constant rate. Instead, each change of 1 step in horizontal
--	position costs 1 more unit of fuel than the last: the first step costs 1, the second step costs 2, the third step costs 3,
--	and so on.

--	As each crab moves, moving further becomes more expensive. This changes the best horizontal position to align them all on; in the example above, this becomes 5:
--
--	This costs a total of 168 fuel. This is the new cheapest possible outcome; the old alignment position (2) now costs 206 fuel instead.
--
-- Test:
--	% cd ../.. && echo -e '16,1,2,0,4,2,7,1,2,14' | cabal run 2021_day07-treachery-of-whales_b # 168

type CrabBoat = (Int, Int) -- Pos, Cnt

solve :: [CrabBoat] -> Int
solve cs = fst . minimum . map fuelCosts $ [minPos..maxPos]
  where minPos   = fst . minimum $ cs
        maxPos   = fst . maximum $ cs
        sum_ns n = n * (n + 1) `quot` 2
        fuelCosts apos = foldl' fuelCost (0, apos) cs
        fuelCost (cost, tpos) (pos, cnt)
          | tpos == pos = (cost, tpos) -- no cost b/c we don't have to move crabs to "this posistion".
          | otherwise   = (cost + (sum_ns . abs $ tpos - pos) * cnt, tpos) -- move "cnt" crabs to "this posistion".

main :: IO ()
main = interact $ show . solve . map (head &&& length) . group . sort . map (read :: String -> Int) . splitOn ","

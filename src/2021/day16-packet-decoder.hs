module Main where
import Data.Bifunctor (first)
import Data.Char (digitToInt)
import Data.List (lookup, splitAt)

--	[Various packet decoding scenarios; ref https://adventofcode.com/2021/day/16]
--
--	part 1 version distribution:
--		23 0
--		24 7
--		33 2
--		33 5
--		38 1
--		38 3
--		38 4
--		41 6
--
--	part 1 operator distribution:
--		26  0 : sum
--		31  1 : product
--		12  2 : minimum
--		10  3 : maximum
--		169 4 : literal values
--		8   5 : greater than - value is 1 if the value of the first sub-packet is greater than the value of the second sub-packet; otherwise, their value is 0
--		8   6 : less than - same as above but "less than"; these packets always have exactly two sub-packets
--		4   7 : equal to - same as above but "equal to"
--
-- Test:
--	% cd ../.. && echo -e 'D2FE28' | cabal run 2021_day16-packet-decoder ## 6
--	% cd ../.. && echo -e '38006F45291200' | cabal run 2021_day16-packet-decoder ## v1[v6, v2] = 9
--	% cd ../.. && echo -e 'EE00D40C823060' | cabal run 2021_day16-packet-decoder ## v7[v2, v4, v1] = 14
--
--	% cd ../.. && echo -e '8A004A801A8002F478' | cabal run 2021_day16-packet-decoder ## v4[v1[v5[v6]]] = 16
--	% cd ../.. && echo -e '620080001611562C8802118E34' | cabal run 2021_day16-packet-decoder ## v3[pk1[lit1, lit2], pk2[lit1, lit2]] = 12
--	% cd ../.. && echo -e 'C0015000016115A2E0802F182340' | cabal run 2021_day16-packet-decoder ## v3[pk1[lit1, lit2], pk2[lit1, lit2]] = 23
--	% cd ../.. && echo -e 'A0016C880162017C3686B18A3D4780' | cabal run 2021_day16-packet-decoder ## o[o[o[lit1, lit2, lit3, lit4, lit5]]] = 31

hexdecoder = [
  ('0', "0000"), ('1', "0001"), ('2', "0010"), ('3', "0011"), ('4', "0100"), ('5', "0101"), ('6', "0110"), ('7', "0111"),
  ('8', "1000"), ('9', "1001"), ('A', "1010"), ('B', "1011"), ('C', "1100"), ('D', "1101"), ('E', "1110"), ('F', "1111"),
  ('\n', "")
  ]

convert :: [Char] -> Int
convert = foldl' (\acc d -> 2 * acc + digitToInt d) 0

splitNbr :: Int -> [Char] -> (Int, [Char])
splitNbr n = first convert . splitAt n

solve :: [Char] -> Int
solve = fst . parsePacket

parsePacket :: [Char] -> (Int, [Char]) -- (ver-sum, bits)
parsePacket bs = parseType v t bs'
  where (v, bs1) = splitNbr 3 bs
        (t, bs') = splitNbr 3 bs1

-- '4' means *literal value* in repeating 5bit sequences
parseType :: Int -> Int -> [Char] -> (Int, [Char])
parseType v 4 bs = while1s bs
  where while1s bs = let (cont:_, bs') = splitAt 5 bs
                     in if '0' == cont then (v, bs') else while1s bs'

-- '0' means *total length in bits* given in the first 15 bits.
parseType v t ('0':bs) = (whileBits v pkts, bs')
  where (len, bs1)  = splitNbr 15 bs
        (pkts, bs') = splitAt len bs1
        whileBits vsum [] = vsum
        whileBits vsum bs = let (vsum', bs') = parsePacket bs
                            in whileBits (vsum + vsum') bs'

-- '1' means *number of sub-packets immediately contained* given in the first 11 bits.
parseType v t ('1':bs) = whileCnt cnt v bs'
  where (cnt, bs') = splitNbr 11 bs
        whileCnt 0   vsum bs = (vsum, bs)
        whileCnt cnt vsum bs = let (vsum', bs') = parsePacket bs
                               in whileCnt (cnt - 1) (vsum + vsum') bs'


main :: IO ()
main = interact $ show . solve . concatMap (\ch -> let Just xs = lookup ch hexdecoder in xs)

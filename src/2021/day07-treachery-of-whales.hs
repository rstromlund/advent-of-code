module Main where
import Control.Arrow ((&&&))
import Data.List (group, sort)
import Data.List.Split (splitOn)

--	... For example, consider the following horizontal positions:
--
--	16,1,2,0,4,2,7,1,2,14
--
--	This means there's a crab with horizontal position 16, a crab with horizontal position 1, and so on.
--
--	Each change of 1 step in horizontal position of a single crab costs 1 fuel. You could choose any horizontal position to
--	align them all on, but the one that costs the least fuel is horizontal position 2
--
--	This costs a total of 37 fuel. This is the cheapest possible outcome; more expensive outcomes include aligning at position
--	1 (41 fuel), position 3 (39 fuel), or position 10 (71 fuel).
--
-- Test:
--	% cd ../.. && echo -e '16,1,2,0,4,2,7,1,2,14' | cabal run 2021_day07-treachery-of-whales # 37

type CrabBoat = (Int, Int) -- Pos, Cnt

solve :: [CrabBoat] -> Int
solve cs = fst . minimum . map fuelCosts $ [minPos..maxPos]
  where minPos = fst . minimum $ cs
        maxPos = fst . maximum $ cs
        fuelCosts apos = foldl' fuelCost (0, apos) cs
        fuelCost (cost, tpos) (pos, cnt)
          | tpos == pos = (cost, tpos) -- no cost b/c we don't have to move crabs to "this posistion".
          | otherwise   = (cost + abs (tpos - pos) * cnt, tpos) -- move "cnt" crabs to "this posistion".

main :: IO ()
main = interact $ show . solve . map (head &&& length) . group . sort . map (read :: String -> Int) . splitOn ","

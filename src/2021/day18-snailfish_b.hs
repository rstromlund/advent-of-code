module Main where
import Data.Char (digitToInt, isHexDigit)
import Data.Tree (Tree(Node), subForest)
import Data.Maybe (Maybe, isJust)

--	... What is the largest magnitude you can get from adding only two of the snailfish numbers?
--	Note that snailfish addition is not commutative - that is, x + y and y + x can produce different results.
--
-- Test:
--	% cd ../.. && echo -e '[[[0,[5,8]],[[1,7],[9,6]]],[[4,[1,2]],[[1,4],2]]]\n[[[5,[2,8]],4],[5,[[9,9],0]]]\n[6,[[[6,2],[5,6]],[[7,6],[4,7]]]]\n[[[6,[0,7]],[0,9]],[4,[9,[9,0]]]]\n[[[7,[6,4]],[3,[1,3]]],[[[5,5],1],9]]\n[[6,[[7,3],[3,2]]],[[[3,8],[5,7]],4]]\n[[[[5,4],[7,7]],8],[[8,3],8]]\n[[9,3],[[9,9],[6,[4,9]]]]\n[[2,[[7,7],7]],[[5,8],[[9,3],[0,2]]]]\n[[[[5,2],5],[8,[3,7]]],[[5,[7,5]],[4,4]]]' | cabal run 2021_day18-snailfish_b ## 3993

-- FIXME: designating a number as "parent" is totally kludgy, need a data type Pair/Val
parent = -9999

-- The 'converge' function repeatedly applies f until there's no change
converge :: Eq a => (a -> a) -> a -> a
converge f x = let x' = f x in if x' == x then x else converge f x'

solve :: [String] -> Int
solve = maximum . adds . ms
  where ms = map parse
        adds (p:ps) = map (\p2 -> magnitude . reduce $ Node parent [p, p2]) ps ++ map (\p2 -> magnitude . reduce $ Node parent [p2, p]) ps ++ adds ps
        adds [] = []
        reduce = converge (split . converge explode)

explode :: Tree Int -> Tree Int
explode t = snd . explode' 1 $ ((False, Nothing, Nothing), t)
  where addDeepRight n t@(Node _ [c0, c1])  = t {subForest = [c0, addDeepRight n c1]}
        addDeepRight (Just n) (Node val []) = Node (val + n) []
        --
        explode' :: Int -> ((Bool, Maybe Int, Maybe Int), Tree Int) -> ((Bool, Maybe Int, Maybe Int), Tree Int)
                 -- level (exploded, carry-left, carry-right, tree) = return tuple
        explode' 5 ((False, Nothing, Nothing), Node _ [Node val0 [], Node val1 []]) = ((True, Just val0, Just val1), Node 0 [])
                 -- ^^ explode node: push left-val back up, carry right-val forward, replace pair w/ 0 value
        explode' lvl ((x, cl, cr), p@(Node _ [c0, c1])) =
          let ((x0, cl0, cr0), c0') = explode' (1 + lvl) ((x, cl, cr), c0)
              ((x1, cl1, cr1), c1') = explode' (1 + lvl) ((x0, Nothing, cr0), c1) -- do not pass "carry-left" down the right side.
          in if isJust cl1 -- is there a carry-left explosion from the right side?  Add it to the left (deep down right #)
             then ((x1, Nothing, cr1), p {subForest = [addDeepRight cl1 c0', c1']})
             else ((x1, cl0, cr1), p {subForest = [c0', c1']})
        explode' _ ((True, Nothing, Just cr), Node val []) = ((True, Nothing, Nothing), Node (val + cr) []) -- this is the first value since an explosion, add the carry-right
        explode' _ othr = othr

split :: Tree Int -> Tree Int
split t = snd . split' $ (False, t)
  where split' (False, t@(Node _ [c0, c1])) -- split parents
          = let (s0, c0') = split' (False,  c0)
                (s', c1') = split' (s0, c1)
            in  (s', t {subForest = [c0', c1']})
        split' (False, Node val []) | val >= 10 -- split the #
          = let (val', r) = val `quotRem` 2
            in (True, Node parent [Node val' [], Node (val' + r) []])
        split' othr = othr

magnitude :: Tree Int -> Int
magnitude (Node _ [c0, c1]) = 3 * magnitude c0 + 2 * magnitude c1
magnitude (Node val []) = val

parse :: String -> Tree Int
parse s = fst . parse' $ (Node 0 [] {-unused, placeholder-}, s)
  where peel ch (s:ss) | ch == s = ss
        --
        parse' (t, ch:s)
          | '[' == ch = let (c0, s0) = parse' (t, s)
                            (c1, s') = parse' (t, peel ',' s0)
                        in (Node parent [c0, c1], peel ']' s')
          | isHexDigit ch = (Node (digitToInt ch) [], s)


main :: IO ()
main = interact $ show . solve . lines

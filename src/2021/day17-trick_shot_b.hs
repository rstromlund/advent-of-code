module Main where
import Data.Bool (bool)
import Data.Char (isDigit)
import Data.List (filter)

--	... count of trajectories to target zone.
--
-- Test:
--	% cd ../.. && echo -e 'target area: x=20..30, y=-10..-5' | cabal run 2021_day17-trick_shot_b ## 112

solve :: [Int] -> Int
solve is = length . filter (shoot ((x1, y1), (x2, y2)) (0, 0)) $ [(dx, dy) | dx <- [0..x2], dy <- [y2..abs y2]]
  where [x1', x2', y1', y2'] = is
        x1 = min x1' x2'
        x2 = max x1' x2'
        y1 = max y1' y2'
        y2 = min y1' y2'

inTarget :: ((Int, Int), (Int, Int)) -> (Int, Int) -> Bool
inTarget ((x1, y1), (x2, y2)) (x, y) = x >= x1 && x <= x2 && y <= y1 && y >= y2

shoot :: ((Int, Int), (Int, Int)) -> (Int, Int) -> (Int, Int) -> Bool
shoot b@(_, (x2, y2)) p@(x, y) (dx, dy)
  | inTarget b p     = True
  | x > x2 || y < y2 = False
  | otherwise        = shoot b (x + dx, y + dy) (dx', dy')
  where dx' = if dx > 0 then dx - 1 else dx
        dy' = dy - 1


main :: IO ()
main = interact $ show . solve . map read . words . map (\ch -> bool ' ' ch $ isDigit ch || '-' == ch)

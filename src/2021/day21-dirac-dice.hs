module Main where
import Data.Bool (bool)
import Data.List (dropWhile, )
import qualified Data.Vector as V (Vector, concatMap, filter, fromList, length, (!))
import Debug.Trace (trace)

--	ref: https://adventofcode.com/2021/day/20
--
-- Test:
--	% cd ../.. && echo -e 'Player 1 starting position: 4\nPlayer 2 starting position: 8' | cabal run 2021_day21-dirac-dice ## 745 * 993 = 739785

solve (p1:p2:_) = score . iterate play $ ((parse p1 - 1, parse p2 - 1), (0, 0, 1, 0))
  -- {-(\((p1, p2), (s1, s2, die, cnt)) -> cnt ) .-} head . dropWhile (\(_, (s1, s2, _, _)) -> s1 < 1000 && s2 < 1000) . iterate play $ ((parse p1 - 1, parse p2 - 1), (0, 0, 1, 0))
  where parse :: String -> Int
        parse = read . drop 2 . dropWhile (/= ':')
        --
        score (r1@((lp1, lp2), (ls1, ls2, ldie, lcnt)):r2@((p1, p2), (s1, s2, die, cnt)):rs)
          | s1 >= 1000 && s2 <  1000 = ls2 * (lcnt + 3)
          | s2 >= 1000 && s1 <  1000 = ls2 * (lcnt + 3)
          | s1 >= 1000 && s2 >= 1000 = if s1 > s2 then s2 * lcnt else s1 * lcnt
          | otherwise = score (r2:rs)

play ((p1, p2), (s1, s2, die, cnt)) =
  let r1  = getDie die       + getDie (1 + die) + getDie (2 + die)
      r2  = getDie (die + 3) + getDie (4 + die) + getDie (5 + die)
      p1' = (p1 + r1) `mod` 10
      p2' = (p2 + r2) `mod` 10
      s1' = s1 + p1' + 1
      s2' = s2 + p2' + 1
      die' = getDie (die + 6)
      cnt' = cnt + 6
  in {-trace (show ("play", p1', p2', s1', s2', die', cnt'))-} ((p1', p2'), (s1', s2', die', cnt'))
  where getDie n = bool n (n - 100) $ n > 100

main :: IO ()
main = interact $ show . solve . lines

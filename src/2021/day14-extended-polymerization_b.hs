module Main where
import Data.Bifunctor (bimap)
import Data.List (groupBy, last, sort)
import qualified Data.Map.Strict as M (elems, foldlWithKey, fromList, fromListWith, lookup)

--	... 40 steps should do it.
--
--	In the above example, the most common element is B (occurring 2,192,039,569,602 times) and the least common element is H
--	(occurring 3849876073 times); subtracting these produces *2188189693529*.
--
--	You just need to work out what polymer would result after repeating the pair insertion process a few times.
--
-- Test:
--	% cd ../.. && echo -e 'NNCB\n\nCH -> B\nHH -> N\nCB -> H\nNH -> C\nHB -> C\nHC -> B\nHN -> C\nNN -> C\nBH -> H\nNC -> B\nNB -> B\nBN -> B\nBB -> N\nBC -> B\nCC -> N\nCN -> C' | cabal run 2021_day14-extended-polymerization_b ## 2188189693529

-- Coding is mine, got insight to the problem from : https://xn--sant-epa.ti-pun.ch/posts/2021-12-aoc/day14.html

solve :: (String, [((Char, Char), Char)]) -> Int
solve (tmplt, rs) = account . M.fromListWith (+) . M.foldlWithKey score [] . (!! 40) . iterate step . M.fromListWith (+) . pairs $ tmplt
  where ms = M.fromList rs
        pairs (e1:es@(e2:_)) = ((e1, e2), 1):pairs es
        pairs [e] = []
        -- Forget traversing a string and building a string; concentrate on the pairs and building new pairs ...
        step = M.fromListWith (+) . M.foldlWithKey step' []
        step' ps (a, b) n | Just c <- M.lookup (a, b) ms = ((a, c), n):((c, b), n):ps
        score ss (a, b) n = (a, n):(b, n):ss
        account ss = let ns = sort . M.elems $ ss
                         mx = (last ns + 1) `quot` 2
                         mn = (head ns + 1) `quot` 2
                     in mx - mn


main :: IO ()
main = interact $ show . solve . (\[tmplt, rs] -> (head tmplt, map (bimap (\ps -> (head ps, ps !! 1)) (!! 4) . span (/= ' ')) . tail $ rs)) . groupBy (\a b -> "" /= b) . lines

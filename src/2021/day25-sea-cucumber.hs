module Main where

--	ref: https://adventofcode.com/2021/day/25
--
-- Test:
--	% cd ../.. && echo -e 'v...>>.vv>\n.vv>>.vv..\n>>.>v>...v\n>>v>>.>.v.\nv>v.vv.v..\n>.>>..v...\n.vv..>.>v.\nv.v..>>v.v\n....v..v.>' | time cabal run 2021_day25-sea-cucumber ## 58

-- Note: this is a bit hastily done, lists and multiple lookups don't perform well ... but we did it! : ))
-- TODO: use transpose to make 'v' more like '>'?  Faster (probably)?

-- The 'converge' function repeatedly applies f until there's no change
converge :: Eq a => Int -> (a -> a) -> a -> (Int, a)
converge cnt f x = let x' = f x in if x' == x then (cnt, x) else converge (cnt + 1) f x'

solve :: [String] -> Int
solve ls = fst . converge 1 pass $ ls
  where ly = length ls
        lx = length $ head ls
        getCh x y ms
          | x >= lx = getCh (x - lx) y ms
          | y >= ly = getCh x (y - ly) ms
          | y <  0  = getCh x (y + ly) ms
          | otherwise = (ms !! y) !! x
        --
        pass = step 'v' . step '>'
        step passCh ms = mapY 0 ms
          where mapY y (l:ls) = {-trace (show ("mapY", l)) $-} mapX 0 y l:mapY (y + 1) ls
                mapY y [] = []
                --
                mapX :: Int -> Int -> String -> String
                mapX x y ('>':cs'@('.':cs))
                  | '>' == passCh = '.':'>':mapX (x + 2) y cs -- advance a '>'
                  | otherwise     = '>':mapX (x + 1) y cs'
                mapX x y ['>']
                  | '>' == passCh =
                      let ch0 = getCh (x + 1) y ms
                      in [if '.' == ch0 then '.' else '>'] -- wrap around a '>'
                  | otherwise = ['>']
                --
                mapX x y ('v':cs)
                  | 'v' == passCh && '.' == getCh x (y + 1) ms = '.':mapX (x + 1) y cs -- advance a 'v'
                  | otherwise = 'v':mapX (x + 1) y cs
                --
                mapX x y ('.':cs)
                  | '>' == passCh && 0 == x && '>' == getCh (lx - 1) y ms = '>':mapX (x + 1) y cs -- wrap around a '>'
                  | 'v' == passCh && 'v' == getCh x (y - 1) ms = 'v':mapX (x + 1) y cs -- advance/wrap around a 'v'
                  | otherwise = '.':mapX (x + 1) y cs
                mapX x y (ch:cs) = ch:mapX (x + 1) y cs
                mapX _ _ [] = []


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.Bifunctor (first)
import Data.Bool (bool)
import Data.Char (digitToInt)
import Data.List (lookup, splitAt)

--	[Various packet decoding scenarios; ref https://adventofcode.com/2021/day/16]
--
--	part 1 version distribution:
--		23 0
--		24 7
--		33 2
--		33 5
--		38 1
--		38 3
--		38 4
--		41 6
--
--	part 1 operator distribution:
--		26  0 : sum
--		31  1 : product
--		12  2 : minimum
--		10  3 : maximum
--		169 4 : literal values
--		8   5 : greater than - value is 1 if the value of the first sub-packet is greater than the value of the second sub-packet; otherwise, their value is 0
--		8   6 : less than - same as above but "less than"; these packets always have exactly two sub-packets
--		4   7 : equal to - same as above but "equal to"
--
-- Test:
--	% cd ../.. && echo -e 'C200B40A82' | cabal run 2021_day16-packet-decoder_b ## 3
--	% cd ../.. && echo -e '04005AC33890' | cabal run 2021_day16-packet-decoder_b ## 54
--	% cd ../.. && echo -e '880086C3E88112' | cabal run 2021_day16-packet-decoder_b ## 7
--	% cd ../.. && echo -e 'CE00C43D881120' | cabal run 2021_day16-packet-decoder_b ## 9
--	% cd ../.. && echo -e 'D8005AC2A8F0' | cabal run 2021_day16-packet-decoder_b ## 1
--	% cd ../.. && echo -e 'F600BC2D8F' | cabal run 2021_day16-packet-decoder_b ## 0
--	% cd ../.. && echo -e '9C005AC2F8F0' | cabal run 2021_day16-packet-decoder_b ## 0
--	% cd ../.. && echo -e '9C0141080250320F1802104A08' | cabal run 2021_day16-packet-decoder_b ## 1

hexdecoder = [
  ('0', "0000"), ('1', "0001"), ('2', "0010"), ('3', "0011"), ('4', "0100"), ('5', "0101"), ('6', "0110"), ('7', "0111"),
  ('8', "1000"), ('9', "1001"), ('A', "1010"), ('B', "1011"), ('C', "1100"), ('D', "1101"), ('E', "1110"), ('F', "1111"),
  ('\n', "")
  ]

convert :: [Char] -> Int
convert = foldl' (\acc d -> 2 * acc + digitToInt d) 0

splitNbr :: Int -> [Char] -> (Int, [Char])
splitNbr n = first convert . splitAt n

solve :: [Char] -> Int
solve = fst . parsePacket

parsePacket :: [Char] -> (Int, [Char]) -- (op-val, bits)
parsePacket bs = parseType v t bs'
  where (v, bs1) = splitNbr 3 bs
        (t, bs') = splitNbr 3 bs1

-- '4' means *literal value* in repeating 5bit sequences
parseType :: Int -> Int -> [Char] -> (Int, [Char])
parseType v 4 bs = while1s 0 bs
  where while1s val bs = let (cont:ds, bs') = splitAt 5 bs
                             val' = (val * 16) + convert ds
                         in if '0' == cont then (val', bs') else while1s val' bs'

-- '0' means *total length in bits* given in the first 15 bits.
parseType v t ('0':bs) = (op t $ whileBits [] pkts, bs')
  where (len, bs1)  = splitNbr 15 bs
        (pkts, bs') = splitAt len bs1
        whileBits ps [] = ps -- note: ps is in reverse order which only matters to (>) and (<) operation below.
        whileBits ps bs = let (val, bs') = parsePacket bs
                          in whileBits (val:ps) bs'

-- '1' means *number of sub-packets immediately contained* given in the first 11 bits.
parseType v t ('1':bs) = first (op t) $ whileCnt cnt [] bs1
  where (cnt, bs1) = splitNbr 11 bs
        whileCnt 0   ps bs = (ps, bs)
        whileCnt cnt ps bs = let (val, bs') = parsePacket bs
                             in whileCnt (cnt - 1) (val:ps) bs'

op :: Int -> [Int] -> Int -- type -> [op-val] -> val
op 0 = sum
op 1 = product
op 2 = minimum
op 3 = maximum
op 5 = \[v2, v1] -> bool 0 1 (v1 > v2)
op 6 = \[v2, v1] -> bool 0 1 (v1 < v2)
op 7 = \[v2, v1] -> bool 0 1 (v1 == v2)


main :: IO ()
main = interact $ show . solve . concatMap (\ch -> let Just xs = lookup ch hexdecoder in xs)

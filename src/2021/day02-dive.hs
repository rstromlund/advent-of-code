module Main where
import Data.Function (on)
import Data.List (groupBy, sort)

--	...
--	forward 5
--	down 5
--	forward 8
--	up 3
--	down 8
--	forward 2
--
--	...After following these instructions, you would have a horizontal position of 15 and a depth of 10. (Multiplying these together produces 150.)
--
-- Test:
--	% cd ../.. && echo -e 'forward 5\ndown 5\nforward 8\nup 3\ndown 8\nforward 2' | cabal run 2021_day02-dive # = 150


solve :: [(String, Int)] -> Int
solve = final . map (comb . unzip) . groupBy ((==) `on` fst) . sort
  where comb (d:_, ns) = (d, sum ns)
        final [("down", d), ("forward", f), ("up",u)] = f * (d - u)

totuple :: [String] -> (String, Int)
totuple [wd, val] = (wd, read val)


main :: IO ()
main = interact $ show . solve . map (totuple . words) . lines

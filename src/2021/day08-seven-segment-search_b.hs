module Main where
import Control.Arrow ((&&&))
import Data.List (intersect, sort, (\\))
import Data.List.Split (splitOn)
import Data.Maybe (fromMaybe)

--	be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe
--	edbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc
--	fgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg
--	fbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb
--	aecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea
--	fgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb
--	dbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe
--	bdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef
--	egadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb
--	gcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce
--

--	Consider again the first example above:
--	After some careful analysis, the mapping between signal wires and segments only make sense in the following configuration:
--
--	 dddd
--	e    a
--	e    a
--	 ffff
--	g    b
--	g    b
--	 cccc
--
--	So, the unique signal patterns would correspond to the following digits:
--	acedgfb: 8
--	cdfbe: 5
--	gcdfa: 2
--	fbcad: 3
--	dab: 7
--	cefabd: 9
--	cdfgeb: 6
--	eafb: 4
--	cagedb: 0
--	ab: 1
--
--	Then, the four digits of the output value can be decoded:
--	cdfeb: 5
--	fcadb: 3
--	cdfeb: 5
--	cdbaf: 3
--	Therefore, the output value for this entry is 5353.
--
--	Following this same process for each entry in the second, larger example above, the output value of each entry can be determined:
--
--	fdgacbe cefdb cefbgd gcbe: 8394
--	fcgedb cgb dgebacf gc: 9781
--	cg cg fdcagb cbg: 1197
--	efabcd cedba gadfec cb: 9361
--	gecf egdcabf bgf bfgea: 4873
--	gebdcfa ecba ca fadegcb: 8418
--	cefg dcbef fcge gbcadfe: 4548
--	ed bcgafe cdgba cbgef: 1625
--	gbdfcae bgc cg cgb: 8717
--	fgae cfgab fg bagce: 4315
--
--	Adding all of the output values in this larger example produces 61229.
--
--	For each entry, determine all of the wire/segment connections and decode the four-digit output values. What do you get if
--	you add up all of the output values?
--
-- Test:
--	% cd ../.. && echo -e 'be cfbegad cbdgef fgaecd cgeb fdcge agebfd fecdb fabcd edb | fdgacbe cefdb cefbgd gcbe\nedbfga begcd cbg gc gcadebf fbgde acbgfd abcde gfcbed gfec | fcgedb cgb dgebacf gc\nfgaebd cg bdaec gdafb agbcfd gdcbef bgcad gfac gcb cdgabef | cg cg fdcagb cbg\nfbegcd cbd adcefb dageb afcb bc aefdc ecdab fgdeca fcdbega | efabcd cedba gadfec cb\naecbfdg fbg gf bafeg dbefa fcge gcbea fcaegb dgceab fcbdga | gecf egdcabf bgf bfgea\nfgeab ca afcebg bdacfeg cfaedg gcfdb baec bfadeg bafgc acf | gebdcfa ecba ca fadegcb\ndbcfg fgd bdegcaf fgec aegbdf ecdfab fbedc dacgb gdcebf gf | cefg dcbef fcge gbcadfe\nbdfegc cbegaf gecbf dfcage bdacg ed bedf ced adcbefg gebcd | ed bcgafe cdgba cbgef\negadfb cdbfeg cegd fecab cgb gbdefca cg fgcdab egfdb bfceg | gbdfcae bgc cg cgb\ngcafb gcf dcaebfg ecagb gf abcdeg gaef cafbge fdbac fegbdc | fgae cfgab fg bagce' | cabal run 2021_day08-seven-segment-search_b ## 61229

solve :: [[[String]]] -> Int -- lines of 2 columns of words [ten-patters, code]
solve = sum . map solve1

solve1 :: [[String]] -> Int -- 2 columns of words [ten-patters, code]
solve1 [ten, code4] = foldl' (\a (Just n) -> a * 10 + n) 0 . map ((`lookup` brkn) . sort) $ code4
  where tal = map ((length &&& id) . sort) ten -- An assoc list of (len, code-str)
        fnd n = fromMaybe "*err" . lookup n $ tal
        --
        s1  = fnd 2
        s4  = fnd 4
        s7  = fnd 3
        s8  = fnd 7
        -- solve for l5s; digit 2, take away middle top, left top, middle middle, and right top/bottom leaves just left bottom, middle bottom (which we have observed)
        s2  = fst . head . filter ((==) leftBot_MidBot . snd) . zip l5s . map (\v -> ((v \\ midTop) \\ leftTop_MidMid) \\ s1) $ l5s
        s5  = fst . head . filter ((==) 2 . snd) . zip s3or5 . map (\v -> length $ v \\ s2) $ s3or5 -- digit 5 minus digit 2-segments leaves just 2 segments (3 would leave 1)
        s3  = head $ s3or5 \\ [s5]
        s3or5 = l5s \\ [s2]
        -- solve for l6s; digit 8, take away digit 9-segments and intersect with left-bottom+middle-bottom leaves just left-bottom (digits 6 and 0 leave nothing)
        s9  = fst . head . filter ((==) 1 . snd) . zip l6s . map (\v -> length $ (s8 \\ v) `intersect` leftBot_MidBot) $ l6s
        s6  = fst . head . filter ((==) 1 . snd) . zip s6or0 . map (\v -> length $ (s8 \\ v) `intersect` s1) $ s6or0 -- digit 8 minus digit 6-segments (leaves left-top), intersect 1-segments still leaves l-t (digit 0 would leave nothing)
        s0  = head $ s6or0 \\ [s6]
        s6or0 = l6s \\ [s9]
        --
        brkn = [(s0, 0), (s1, 1), (s2, 2), (s3, 3), (s4, 4), (s5, 5), (s6, 6), (s7, 7), (s8, 8), (s9, 9)] -- we broke the code; assoc list of (codes, value)
        -- observations:
        l5s            = map snd . filter (\(c, _) -> 5 == c) $ tal -- all digits w/ 5 segments
        l6s            = map snd . filter (\(c, _) -> 6 == c) $ tal -- all digits w/ 6 segments
        leftTop_MidMid = s4 \\ s1
        midTop         = s7 \\ s1
        leftBot_MidBot = ((s8 \\ s4) \\ s1) \\ s7


main :: IO ()
main = interact $ show . solve . map (map words . splitOn "|") . lines

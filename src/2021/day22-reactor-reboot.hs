module Main where
import Data.Bool (bool)
import Data.Char (isDigit)
import qualified Data.IntMap.Strict as I (IntMap, delete, empty, findWithDefault, insert, size)

--	Turn on and off cuboids, how many are on in the -50..50^3 initialization grid?
--
-- Test:
--	% cd ../.. && echo -e 'on x=10..12,y=10..12,z=10..12\non x=11..13,y=11..13,z=11..13\noff x=9..11,y=9..11,z=9..11\non x=10..10,y=10..10,z=10..10\non x=-54112..-39298,y=-85059..-49293,z=-27449..7877' | cabal run 2021_day22-reactor-reboot ## 39
--	% cd ../.. && cabal run 2021_day22-reactor-reboot < src/2021/22a.tst # 590784


-- Note: The naive way, I am sure part 2 will make this untenable.

initMax = 50

-- Kludgy?  Maybe, but stuff (x, y, z) into an Int and use IntMap for compactness.  (Copied from last year b/c the hint in the story.)
-- Masking (x,y,z) into an Int. xxxxxxyyyyyyzzzzzz

shiftAmt :: Int
shiftAmt = 1000000

xShift = shiftAmt * shiftAmt
yShift = shiftAmt
zShift = 1

coordMidpoint :: Int
coordMidpoint = shiftAmt `div` 2

getCoords :: Int -> (Int, Int, Int)
getCoords key = (key `div` xShift `mod` shiftAmt - coordMidpoint, key `div` yShift `mod` shiftAmt - coordMidpoint, key `mod` shiftAmt - coordMidpoint)

mkCoords :: (Int, Int, Int) -> Int
mkCoords (x, y, z) = (x + coordMidpoint) * xShift + (y + coordMidpoint) * yShift + (z + coordMidpoint) * zShift

cubeSet :: Bool -> I.IntMap Bool -> (Int, Int, Int) -> I.IntMap Bool
cubeSet False cs pt = I.delete (mkCoords pt) cs
cubeSet True  cs pt = I.insert (mkCoords pt) True cs

cubeGet :: (Int, Int, Int) -> I.IntMap Bool -> Bool
cubeGet pt = I.findWithDefault False $ mkCoords pt


solve = I.size . foldl' plot I.empty . filter isInit . map parse
  where parse :: String -> (Bool, [Int])
        parse ('o':dir:_:_:cs) = ('n' == dir, map read . words . map (\c -> bool ' ' c $ isDigit c || '-' == c) $ cs)
        isInit :: (Bool, [Int]) -> Bool
        isInit (b, [x0, x1, y0, y1, z0, z1]) = x0 <= initMax && x1 >= -initMax && y0 <= initMax && y1 >= -initMax && z0 <= initMax && z1 >= -initMax

plot :: I.IntMap Bool -> (Bool, [Int]) -> I.IntMap Bool
plot cs (b, [x0, x1, y0, y1, z0, z1]) = foldl' (cubeSet b) cs [(x, y, z) | x <- [x0..x1], y <- [y0..y1], z <- [z0..z1]]


main :: IO ()
main = interact $ show . solve . lines

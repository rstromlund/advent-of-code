module Main where
import Control.Arrow ((&&&))
import Data.List (group, sort)
import qualified Data.MemoTrie as M (memo)

--	ref: https://adventofcode.com/2021/day/20
--
-- Test:
--	% cd ../.. && echo -e 'Player 1 starting position: 4\nPlayer 2 starting position: 8' | cabal run 2021_day21-dirac-dice_b ## 444356092776315

-- Note: I used reddit megasolution for algorithm and hints.

-- possible die rolls plus frequency
dieHisto :: [(Int, Int)]
dieHisto = map (head &&& length) . group . sort $ [i + j + k | i <- [1..3], j <- [1..3], k <- [1..3]] -- aka. [(3,1),(4,3),(5,6),(6,7),(7,6),(8,3),(9,1)]

solve (p1:p2:_) = uncurry max . play $ ((parse p1, 0), (parse p2, 0))
  where parse :: String -> Int
        parse = read . last . words

play :: ((Int, Int), (Int, Int)) -> (Int, Int)
play = M.memo play'
  where play' :: ((Int, Int), (Int, Int)) -> (Int, Int)
        play' ((p1, s1), (p2, s2)) = foldl' (\(a1, a2) d -> let (w1, w2) = step d in (a1 + w1, a2 + w2)) (0,0) dieHisto
          where step :: (Int, Int) -> (Int, Int)
                step (k, ct) = let p1' = mod1 10 $ p1 + k
                                   s1' = s1 + p1'
                               in if s1' >= 21 then (ct, 0) else (\(w2, w1) -> (w1 * ct, w2 * ct)) (play ((p2, s2), (p1', s1')))

mod1 :: Int -> Int -> Int
mod1 m n = ((n - 1) `mod` m) + 1


main :: IO ()
main = interact $ show . solve . lines

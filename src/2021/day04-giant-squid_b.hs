module Main where
import Data.List (all, any, find, filter, groupBy, sum, transpose)
import Data.List.Split (splitOn)

--	...bingo subsystem
--
--	7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1
--
--	22 13 17 11  0
--	 8  2 23  4 24
--	21  9 14 16  7
--	 6 10  3 18  5
--	 1 12 20 15 19
--
--	 3 15  0  2 22
--	 9 18 13 17  5
--	19  8  7 25 23
--	20 11 10 24  4
--	14 21 16 12  6
--
--	14 21 17 24  4
--	10 16 15  9 19
--	18  8 23 26 20
--	22 11 13  6  5
--	 2  0 12  3  7
--
--	...Finally, 24 is drawn ...At this point, the third board wins because it has at least one complete row or column of
--	marked numbers (in this case, the entire top row is marked: 14 21 17 24 4).
--
--	The score of the winning board can now be calculated. Start by finding the sum of all unmarked numbers on that board; in
--	this case, the sum is 188. Then, multiply that sum by the number that was just called when the board won, 24, to get the
--	final score, 188 * 24 = 4512.
--
-- Test:
--	% cd ../.. && echo -e '7,4,9,5,11,17,23,2,0,14,21,24,10,16,13,6,15,25,12,22,18,20,8,19,3,26,1\n\n22 13 17 11  0\n8  2 23  4 24\n21  9 14 16  7\n6 10  3 18  5\n1 12 20 15 19\n\n3 15  0  2 22\n9 18 13 17  5\n19  8  7 25 23\n20 11 10 24  4\n14 21 16 12  6\n\n14 21 17 24  4\n10 16 15  9 19\n18  8 23 26 20\n22 11 13  6  5\n2  0 12  3  7' | cabal run 2021_day04-giant-squid_b # 148 * 13 = 1924


type BingoCard = [[Maybe Int]] -- 5 rows x 5 columns of Maybe Int's

solve :: [[[String]]] -> Int
solve (bs:bds) = play 999 cs ns cs
  where ns = map read . splitOn "," . head . head $ bs
        cs = map (map (map (Just . read)) . tail) bds

winnerByRows :: BingoCard -> Bool -- from 1 card return if winning (by ROW) card
winnerByRows = any winRow
  where winRow r = all (Nothing ==) r

winner :: BingoCard -> Bool -- from 1 card return if winning (by ROW or by COL) card
winner c = winnerByRows c || (winnerByRows . transpose $ c)

score :: Int -> BingoCard -> Int
score n c = n * (maybe 0 sum . sequence . filter (Nothing /=) . concat $ c)

play :: Int -> [BingoCard] -> [Int] -> [BingoCard] -> Int
play oldN [lastc] _ []      = score oldN lastc
play oldN oldCS (n:ns) cs = play n newCs ns . filter (not . winner) $ newCs
  where newCs    = map playCard cs
        playCard = map playRow
        playRow  = map playCol
        playCol Nothing = Nothing
        playCol (Just x)
          | n == x    = Nothing
          | otherwise = Just x


main :: IO ()
main = interact $ show . solve . groupBy (\a b -> [] /= b) . map words . lines

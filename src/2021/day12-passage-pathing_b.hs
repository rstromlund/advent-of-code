module Main where
import Control.Arrow ((&&&))
import Data.Char (isLower, isUpper)
import Data.List (filter, group, length, sort)
import Data.List.Split (splitOn)
import qualified Data.Map.Strict as M (Map, elemAt, filter, fromList, null, insertWith, size)

--	After reviewing the available paths, you realize you might have time to visit a single small cave *twice*. Specifically, big
--	caves can be visited any number of times, a single small cave can be visited at most twice, and the remaining small caves
--	can be visited at most once. However, the caves named start and end can only be visited *exactly once each*: once you leave
--	the start cave, you may not return to it, and once you reach the end cave, the path must end immediately.
--
-- Test:
--	% cd ../.. && echo -e 'start-A\nstart-b\nA-c\nA-b\nb-d\nA-end\nb-end' | cabal run 2021_day12-passage-pathing_b ## 36
--	% cd ../.. && echo -e 'dc-end\nHN-start\nstart-kj\ndc-start\ndc-HN\nLN-dc\nHN-end\nkj-sa\nkj-HN\nkj-dc' | cabal run 2021_day12-passage-pathing_b ## 103

solve :: [[String]] -> Int
solve lns = length . visit $ ["start"]
  where ns = [(a, b) | [a, b] <- lns, "end" /= a && "start" /= b] ++ [(a, b) | [b, a] <- lns, "end" /= a && "start" /= b]
        candidates node = map snd . filter ((==) node . fst) $ ns
        -- small caves
        skipCaves :: M.Map String Int -> String -> Bool
        skipCaves _ (ch:_) | isUpper ch = False -- big? go there.
        skipCaves freq c = let freq' = M.insertWith (+) c 1 freq
                               dbls  = M.filter (> 1) freq'
                           in (not . M.null $ dbls) && (M.size dbls > 1 || (snd . M.elemAt 0 $ dbls) > 2) -- been there? 2 small rooms? twice? thrice? skip...
        -- recursive path builder
        visit :: [String] -> [[String]]
        visit ps@("end":_) = [ps]
        visit ps@(p:_) = concatMap (\c -> visit (c:ps)) cs
          where freq = M.fromList . map (head &&& length) . group . sort . filter (isLower . head) $ ps -- frequency visit map
                cs = filter (not . skipCaves freq) $ candidates p


main :: IO ()
main = interact $ show . solve . map (splitOn "-") . lines

module Main where
import Data.Bifunctor (bimap, second)
import Data.List (groupBy)
import qualified Data.Set as S (fromList, map, size)

--	The transparent paper is marked with random dots and includes instructions on how to fold it up (your puzzle input). For example:
--
--	6,10
--	0,14
--	9,10
--	0,3
--	10,4
--	4,11
--	6,0
--	6,12
--	4,1
--	0,13
--	10,12
--	3,4
--	3,0
--	8,4
--	1,10
--	2,14
--	8,10
--	9,0
--
--	fold along y=7
--	fold along x=5
--
--	The first section is a list of dots on the transparent paper. 0,0 represents the top-left coordinate. The first value, x,
--	increases to the right. The second value, y, increases downward. So, the coordinate 3,0 is to the right of 0,0, and the
--	coordinate 0,7 is below 0,0. The coordinates in this example form the following pattern, where # is a dot on the paper and
--	. is an empty, unmarked position:

-- Test:
--	% cd ../.. && echo -e '6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5' | cabal run 2021_day13-transparent-oragami ## 17

solve :: ([(Int, Int)], [(String, Int)]) -> Int
solve (cs, is) = S.size . step $ is
  where ss = S.fromList cs
        step (("fold along y", yf):_) = S.map (\(x, y) -> if y <= yf then (x, y) else (x, yf - y + yf)) ss
        step (("fold along x", xf):_) = S.map (\(x, y) -> if x <= xf then (x, y) else (xf - x + xf, y)) ss


main :: IO ()
main = interact $ show . solve
       . (\[pts, flds] ->
             (map (bimap read (read . tail) . span (/= ',')) pts,
              map (second (read . tail) . span (/= '=')) . tail $ flds))
       . groupBy (\a b -> "" /= b) . lines

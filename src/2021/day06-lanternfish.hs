module Main where
import Debug.Trace (trace)
import Data.Char (isDigit)
import Data.List.Split (wordsBy)

--	... lanternfish that creates a new fish resets its timer to 6, not 7 (because 0 is included as a valid timer value). The
--	new lanternfish starts with an internal timer of 8 and does not start counting down until the next day.
--
--	3,4,3,1,2
--
--	This list means that the first fish has an internal timer of 3, the second fish has an internal timer of 4, and so on
--	until the fifth fish, which has an internal timer of 2.
--
-- Test:
--	% cd ../.. && echo -e '3,4,3,1,2' | cabal run 2021_day06-lanternfish # 5934

applyN n = foldr (.) id . replicate n

solve :: [Int] -> Int
solve = length . applyN 80 (foldl' sim [])
  where sim newFs f
          | f == 0    = 6:8:newFs
          | otherwise = (f - 1):newFs


main :: IO ()
main = interact $ show . solve . map read . wordsBy (not . isDigit)

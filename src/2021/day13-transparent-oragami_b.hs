module Main where
import Data.Bifunctor (bimap, second)
import Data.Bool (bool)
import Data.List (groupBy)
import qualified Data.Set as S (Set, findMax, fromList, map, member)

--	Finish folding the transparent paper according to the instructions. The manual says the code is always eight capital letters.
--	What code do you use to activate the infrared thermal imaging camera system?
--
-- Test:
--	% cd ../.. && echo -e '6,10\n0,14\n9,10\n0,3\n10,4\n4,11\n6,0\n6,12\n4,1\n0,13\n10,12\n3,4\n3,0\n8,4\n1,10\n2,14\n8,10\n9,0\n\nfold along y=7\nfold along x=5' | cabal run 2021_day13-transparent-oragami_b ## 0 (O?)

solve :: ([(Int, Int)], [(String, Int)]) -> String
solve (cs, is) = showLtrs . foldl' step ss $ is
  where ss = S.fromList cs
        step ss ("fold along y", yf) = S.map (\(x, y) -> if y <= yf then (x, y) else (x, yf - y + yf)) ss
        step ss ("fold along x", xf) = S.map (\(x, y) -> if x <= xf then (x, y) else (xf - x + xf, y)) ss

-- A bit of a cheat, just printing out the set and reading it.  : p
showLtrs :: S.Set (Int, Int) -> String
showLtrs ss = unlines $ map (\y -> map (\x -> bool ' ' '#' $ S.member (x, y) ss) [0..(fst . S.findMax $ ss)]) [0..(S.findMax . S.map snd $ ss)]


main :: IO ()
main = interact $ solve
       . (\[pts, flds] ->
             (map (bimap read (read . tail) . span (/= ',')) pts,
              map (second (read . tail) . span (/= '=')) . tail $ flds))
       . groupBy (\a b -> "" /= b) . lines

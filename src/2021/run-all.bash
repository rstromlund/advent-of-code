#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint .
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-sonar_sweep.hs   < 1a.txt ; (( rc += ${?} ))
#time ./day01-sonar_sweep_b.hs < 1a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-dive.hs   < 2a.txt ; (( rc += ${?} ))
#time ./day02-dive_b.hs < 2a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-binary-diagnostic.hs   < 3a.txt ; (( rc += ${?} ))
#time ./day03-binary-diagnostic_b.hs < 3a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-giant-squid.hs   < 4a.txt ; (( rc += ${?} ))
#time ./day04-giant-squid_b.hs < 4a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-hydrothermal-venture.hs   < 5a.txt ; (( rc += ${?} ))
#time ./day05-hydrothermal-venture_b.hs < 5a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-lanternfish.hs   < 6a.txt ; (( rc += ${?} ))
#time ./day06-lanternfish_b.hs < 6a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-treachery-of-whales.hs   < 7a.txt ; (( rc += ${?} ))
#time ./day07-treachery-of-whales_b.hs < 7a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-seven-segment-search.hs   < 8a.txt ; (( rc += ${?} ))
#time ./day08-seven-segment-search_b.hs < 8a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-smoke-basin.hs   < 9a.txt ; (( rc += ${?} ))
#time ./day09-smoke-basin_b.hs < 9a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-syntax-scoring.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-syntax-scoring_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-dumbo-octopus.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-dumbo-octopus_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-passage-pathing.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-passage-pathing_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-transparent-oragami.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-transparent-oragami_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-extended-polymerization.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-extended-polymerization_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-chiton.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-chiton_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-packet-decoder.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-packet-decoder_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-trick_shot.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-trick_shot_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time ./day18-snailfish.hs   < 18a.txt ; (( rc += ${?} ))
#time ./day18-snailfish_b.hs < 18a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-beacon-scanner.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-beacon-scanner_b.hs < 19a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day20"
#time ./day20-trench-map.hs   < 20a.txt ; (( rc += ${?} ))
#time ./day20-trench-map_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-dirac-dice.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-dirac-dice_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-reactor-reboot.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-reactor-reboot_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time ./day23-amphipod.hs   < 23a.txt ; (( rc += ${?} ))
#time ./day23-amphipod_b.hs < 23a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-arithmetic-logic-unit.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-arithmetic-logic-unit_b.hs < 24a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-sea-cucumber.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

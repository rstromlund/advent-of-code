module Main where
import Data.Char (digitToInt)
import Data.List (dropWhile, )
import qualified Data.Map.Strict as M (Map, empty, foldlWithKey', insertWith, lookup, member, singleton)
import Data.Maybe (fromMaybe, isJust)
import qualified Data.Vector as V (Vector, fromList, length, (!), (//))

--	... For example:
--
--	1163751742
--	1381373672
--	2136511328
--	3694931569
--	7463417111
--	1319128137
--	1359912421
--	3125421639
--	1293138521
--	2311944581
--
--	You start in the top left position, your destination is the bottom right position, and you cannot move diagonally. The
--	number at each position is its risk level; to determine the total risk of an entire path, add up the risk levels of each
--	position you enter (that is, don't count the risk level of your starting position unless you enter it; leaving it adds no
--	risk to your total).
--
--	Your goal is to find a path with the lowest total risk. In this example, a path with the lowest total risk is highlighted
--	here:
--
--	.163751742
--	.381373672
--	.......328
--	369493..69
--	7463417.11
--	1319128..7
--	13599124.1
--	31254216.9
--	12931385..
--	231194458.
--
--	The total risk of this path is 40 (the starting position is never entered, so its risk is not counted).
--
-- Test:
--	% cd ../.. && echo -e '1163751742\n1381373672\n2136511328\n3694931569\n7463417111\n1319128137\n1359912421\n3125421639\n1293138521\n2311944581' | cabal run 2021_day15-chiton ## 40

--  Algorithm: https://en.wikipedia.org/wiki/Pathfinding

-- WARNING!  This is not Dijkstra's algorithm and can fail "turning corners" and obliterating shorter routes by longer ones.  This
-- algorithm is probably better at mazes with walls like in the wiki article.  It works for part1 of today's problem but not part2.

visitDirs = [
         (0, -1),
  (-1, 0),      (1, 0),
         (0,  1)
  ]

solve :: [String] -> Int
solve ls = (\(ps', _) -> fromMaybe 9999 $ M.lookup (0, 0) ps') . (!! 0) . dropWhile (\(ps', _) -> not . M.member (0, 0) $ ps') .
  iterate visit $ (M.singleton (lx - 1, ly - 1) 0, setRisk (lx - 1, ly - 1) Nothing cm) -- start in lower right corner and search up and left
  where cm :: V.Vector (V.Vector (Maybe Int))
        cm = V.fromList . map (V.fromList . map (Just . digitToInt)) $ ls -- cm (cave map) is a vector of vectors of Maybe Ints
        ly = V.length cm
        lx = V.length $ cm V.! 0
        --
        getRisk (x, y) cm
          | x >= 0 && y >= 0 && x < lx && y < ly = cm V.! y V.! x
          | otherwise = Nothing
        setRisk (x, y) n cm
          | x >= 0 && y >= 0 && x < lx && y < ly = cm V.// [(y, cm V.! y V.// [(x, n)])]
          | otherwise = cm
        --
        -- Visit surrounding cave "square"s.
        visit :: (M.Map (Int, Int) Int, V.Vector (V.Vector (Maybe Int))) -> (M.Map (Int, Int) Int, V.Vector (V.Vector (Maybe Int)))
        visit (ps, cm) =
          let vs = M.foldlWithKey' (\ps' (x, y) r -> visit' ps' cm (x, y) r) [] ps -- all the new visit points/risks
          in foldl' (\(ps', cm') (cord, r) -> (M.insertWith min cord r ps', setRisk cord Nothing cm')) (M.empty, cm) vs -- combine to lowest risk and erase trail
        visit' ps cm (x, y) r =
          foldl' (\ns (dx, dy) ->
                     let nx = x + dx
                         ny = y + dy
                         nr = getRisk (nx, ny) cm
                     in if isJust nr then ((nx, ny), r + fromMaybe 9999 nr):ns else ns
                 ) ps visitDirs


main :: IO ()
main = interact $ show . solve . lines

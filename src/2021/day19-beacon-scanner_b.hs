module Main where
import qualified Data.Map.Strict as M (Map, fromList, (!))
import Data.Maybe (Maybe, fromMaybe, isJust, isNothing)
import Data.List (concatMap, dropWhile, groupBy, intersect, takeWhile)
import Data.List.Extra (groupOn)
import Data.List.Split (splitOn)
import qualified Data.Set as S (empty, fromList, size, union)

--	ref: https://adventofcode.com/2021/day/19
--
-- Test:
--	% cd ../.. && time cabal run 2021_day19-beacon-scanner_b < src/2021/19a.tst ## long-ish test set

-- permutations: https://www.euclideanspace.com/maths/algebra/matrix/transforms/examples/index.htm
-- distance: https://en.wikipedia.org/wiki/Taxicab_geometry

-- min overlapping beacons between two scanners if they overlap
minMatches = 12

-- min matches when pairing all beacons -- the number of edges in a complete graph of 12 nodes
pairMatches = (minMatches * (minMatches - 1)) `quot` 2

type Position = [Int]

data Scanner = Scanner {
  nbr :: Int,
  beacons :: [Position],
  distanceMap :: M.Map Int (Position, Position),
  distances :: [Int],
  position :: Maybe Position
} deriving (Eq, Ord, Read, Show)

-- The 'converge' function repeatedly applies f until there's no change
converge :: Eq a => (a -> a) -> a -> a
converge f x = let x' = f x in if x' == x then x else converge f x'

manhattan :: Scanner -> Scanner -> Int
manhattan s0 s1 = sum . map abs $ zipWith (-) (fromMaybe [] $ position s0) (fromMaybe [] $ position s1)

solve :: [[String]] -> Int
solve = maximum . manhattans . converge searchLoop . map (parse . dropWhile (== ""))
  where manhattans (s0:ss') = map (manhattan s0) ss' ++ manhattans ss'
        manhattans [] = []
        --
        parse (s:ss) = mkScanner (parseScanner s) (map parseCoords ss) Nothing
        parseScanner :: String -> Int
        parseScanner ('-':'-':'-':' ':'s':'c':'a':'n':'n':'e':'r':' ':s) = read . takeWhile (/= ' ') $ s
        parseCoords :: String -> Position
        parseCoords = map read . splitOn ","

searchLoop :: [Scanner] -> [Scanner]
searchLoop ss = foldl' searchLoop' ss ss
  where searchLoop' ss' s0 | isNothing (position s0) = ss' -- find a completed 1st scanner
        searchLoop' ss' s0 = upd ss' . concatMap (\s1 -> if nbr s0 == nbr s1 || isJust (position s1) then [] -- skip self and already found scanners
                                                         else findPos s0 s1) $ ss' -- find uncompleted 2nd scanner's w/ overlap
        upd ss' [] = ss'
        upd ss' (Nothing:xs) = upd ss' xs
        upd ss' (fnd:_) = updScanner fnd ss'
        --
        findPos :: Scanner -> Scanner -> [Maybe (Scanner, Int, Position)]
        findPos s0 s1 = concatMap (
          \d -> let d0 = distanceMap s0 M.! d
                    d1 = distanceMap s1 M.! d
                    dm1r0 = map (\f -> f $ fst d1) permute
                    dm1r1 = map (\f -> f $ snd d1) permute
                    dm1rs = zip [0..] $ zip dm1r0 dm1r1
                in map (\(n, pt) -> scannerLocation s1 n d0 pt) dm1rs
          ) $ distMatches s0 s1
        -- FIXME(?) maybe only need to rotate and search the first intersecting point?
        distMatches s0 s1 = let ds = distances s0 `intersect` distances s1
                            in if length ds >= pairMatches then ds else []

distPairs :: [Position] -> [(Int, (Position, Position))]
distPairs (b:bs) = map (\b' -> (beaconDistance b b', (b, b'))) bs ++ distPairs bs
distPairs []     = []

mkScanner :: Int -> [Position] -> Maybe Position -> Scanner
mkScanner nbr bs p = let ds = distPairs bs
                     in build nbr bs (M.fromList ds) (map fst ds) Nothing
  where build 0   beacons distanceMap distances _ = Scanner 0 beacons distanceMap distances (Just [0,0,0])
        build nbr beacons distanceMap distances position = Scanner nbr beacons distanceMap distances p

updScanner :: Maybe (Scanner, Int, Position) -> [Scanner] -> [Scanner]
updScanner (Just (s1, n, pt)) ss =
  let p   = permute !! n
      bs  = map (zipWith (+) pt . p) (beacons s1)
      s1' = mkScanner (nbr s1) bs (Just pt)
      (s',_:ss') = splitAt (nbr s1) ss
  in s' ++ s1':ss'

scannerLocation :: Scanner -> Int -> (Position, Position) -> (Position, Position) -> Maybe (Scanner, Int, Position)
scannerLocation s1 n (ba0, ba1) (bb0, bb1) =
  let loc1 = zipWith (-) ba0 bb0
      loc2 = zipWith (-) ba1 bb1
      loc3 = zipWith (-) ba0 bb1
      loc4 = zipWith (-) ba1 bb0
  in if loc1 == loc2 then Just (s1, n, loc1) else if loc3 == loc4 then Just (s1, n, loc3) else Nothing

beaconDistance :: Position -> Position -> Int
beaconDistance b0 = sum . map ((\n -> n * n) . abs) . zipWith (-) b0 -- don't bother taking square root, magnitude will do.

permute :: [Position -> Position]
permute = [
  \[x,y,z] -> [ x,  y,  z], -- 00
  \[x,y,z] -> [ x,  z, -y], -- 01
  \[x,y,z] -> [ x, -y, -z], -- 02
  \[x,y,z] -> [ x, -z,  y], -- 03
  \[x,y,z] -> [ y, -x,  z], -- 04
  \[x,y,z] -> [ y,  z,  x], -- 05
  \[x,y,z] -> [ y,  x, -z], -- 06
  \[x,y,z] -> [ y, -z, -x], -- 07
  \[x,y,z] -> [-x, -y,  z], -- 08
  \[x,y,z] -> [-x, -z, -y], -- 09
  \[x,y,z] -> [-x,  y, -z], -- 10
  \[x,y,z] -> [-x,  z,  y], -- 11
  \[x,y,z] -> [-y,  x,  z], -- 12
  \[x,y,z] -> [-y, -z,  x], -- 13
  \[x,y,z] -> [-y, -x, -z], -- 14
  \[x,y,z] -> [-y,  z, -x], -- 15
  \[x,y,z] -> [ z,  y, -x], -- 16
  \[x,y,z] -> [ z,  x,  y], -- 17
  \[x,y,z] -> [ z, -y,  x], -- 18
  \[x,y,z] -> [ z, -x, -y], -- 19
  \[x,y,z] -> [-z, -y, -x], -- 20
  \[x,y,z] -> [-z, -x,  y], -- 21
  \[x,y,z] -> [-z,  y,  x], -- 22
  \[x,y,z] -> [-z,  x, -y]  -- 23
  ]

main :: IO ()
main = interact $ show . solve . groupBy (\_ b -> "" /= b) . lines

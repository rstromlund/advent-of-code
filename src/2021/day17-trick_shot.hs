module Main where
import Data.List (dropWhile, takeWhile)

--	... highest trajectory to target zone.
--
-- Test:
--	% cd ../.. && echo -e 'target area: x=20..30, y=-10..-5' | cabal run 2021_day17-trick_shot # 45

solve :: Int -> Int
solve y = sum [1..abs y - 1]


main :: IO ()
main = interact $ show . solve . read . takeWhile (/= '.') . tail . dropWhile (/= '=') . last . words

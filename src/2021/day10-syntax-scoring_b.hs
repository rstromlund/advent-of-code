module Main where
import Data.List (sort)
import Debug.Trace (trace)

--	Incomplete lines don't have any incorrect characters - instead, they're missing some closing characters at the end of the
--	line. To repair the navigation subsystem, you just need to figure out the sequence of closing characters that complete all
--	open chunks in the line.
--
--	The score is determined by considering the completion string character-by-character. Start with a total score of 0. Then,
--	for each character, multiply the total score by 5 and then increase the total score by the point value given for the
--	character in the following table:
--
--	* ): 1 point.
--	* ]: 2 points.
--	* }: 3 points.
--	* >: 4 points.
--
--	Autocomplete tools are an odd bunch: the winner is found by sorting all of the scores and then taking the middle
--	score. (There will always be an odd number of scores to consider.) In this example, the middle score is 288957 because
--	there are the same number of scores smaller and larger than it.
--
-- Test:
--	% cd ../.. && echo -e '[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]' | cabal run 2021_day10-syntax-scoring_b ## 288957

solve :: [String] -> Int
solve = head . (\ss -> drop (length ss `quot` 2) ss) . sort . filter (/= 0) . map (foldl' score 0 . parse)
  where score acc ch = (5 * acc) + val ch
        val ')' = 1
        val ']' = 2
        val '}' = 3
        val '>' = 4

parse :: String -> String
parse = walk ""
  where match '(' = ')'
        match '[' = ']'
        match '{' = '}'
        match '<' = '>'
        --
        walk stk [] = map match stk -- reached end-of-string; incomplete but not invalid. Return the necessary closing sigils ...
        walk stk (c:cs) | c `elem` "([{<" = walk (c:stk) cs -- Opening sigil?  Push onto stack & continue ...
        -- Closing sigil? Check for matching opener on the stack ...
        walk (s:stk) (c:cs) = if c == match s then walk stk cs else "" -- stop parsing if sigils mismatch


main :: IO ()
main = interact $ show . solve . lines

module Main where

--	* If a chunk opens with (, it must close with ).
--	* If a chunk opens with [, it must close with ].
--	* If a chunk opens with {, it must close with }.
--	* If a chunk opens with <, it must close with >.
--
--	Some lines are incomplete, but others are corrupted. Find and discard the corrupted lines first.
--

--	For example, consider the following navigation subsystem:
--
--	[({(<(())[]>[[{[]{<()<>>
--	[(()[<>])]({[<{<<[]>>(
--	{([(<{}[<>[]}>{[]{[(<()>
--	(((({<>}<{<{<>}{[]{[]{}
--	[[<[([]))<([[{}[[()]]]
--	[{[{({}]{}}([{[{{{}}([]
--	{<[[]]>}<{[{[{[]{()[[[]
--	[<(<(<(<{}))><([]([]()
--	<{([([[(<>()){}]>(<<{{
--	<{([{{}}[<[[[<>{}]]]>[]]
--
--	Some of the lines aren't corrupted, just incomplete; you can ignore these lines for now. The remaining five lines are corrupted:
--
--	{([(<{}[<>[]}>{[]{[(<()> - Expected ], but found } instead.
--	[[<[([]))<([[{}[[()]]] - Expected ], but found ) instead.
--	[{[{({}]{}}([{[{{{}}([] - Expected ), but found ] instead.
--	[<(<(<(<{}))><([]([]() - Expected >, but found ) instead.
--	<{([([[(<>()){}]>(<<{{ - Expected ], but found > instead.
--
--	... take the first illegal character on the line and look it up in the following table:
--	): 3 points.
--	]: 57 points.
--	}: 1197 points.
--	>: 25137 points.
--
--	In the above example, an illegal ) was found twice (2*3 = 6 points), an illegal ] was found once (57 points), an illegal }
--	was found once (1197 points), and an illegal > was found once (25137 points). So, the total syntax error score for this
--	file is 6+57+1197+25137 = 26397 points!
--
-- Test:
--	% cd ../.. && echo -e '[({(<(())[]>[[{[]{<()<>>\n[(()[<>])]({[<{<<[]>>(\n{([(<{}[<>[]}>{[]{[(<()>\n(((({<>}<{<{<>}{[]{[]{}\n[[<[([]))<([[{}[[()]]]\n[{[{({}]{}}([{[{{{}}([]\n{<[[]]>}<{[{[{[]{()[[[]\n[<(<(<(<{}))><([]([]()\n<{([([[(<>()){}]>(<<{{\n<{([{{}}[<[[[<>{}]]]>[]]' | cabal run 2021_day10-syntax-scoring ## 26397

solve :: [String] -> Int
solve = sum . map parse

parse :: String -> Int
parse = walk ""
  where walk stk [] = 0 -- exhausted the input string?  Discard ...
        walk stk (c:cs) | c `elem` "([{<" = walk (c:stk) cs -- Opening?  Push onto stack & continue ...
        --
        walk (t:stk) (')':cs) = if t /= '(' then     3 else walk stk cs
        walk (t:stk) (']':cs) = if t /= '[' then    57 else walk stk cs
        walk (t:stk) ('}':cs) = if t /= '{' then  1197 else walk stk cs
        walk (t:stk) ('>':cs) = if t /= '<' then 25137 else walk stk cs


main :: IO ()
main = interact $ show . solve . lines

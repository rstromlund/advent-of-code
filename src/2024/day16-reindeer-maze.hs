{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed ((!?), assocs)
import Data.Bool (bool)
import Point (Point(..), PuzzleMap, parseMap, turnLeft, turnRight, east)

{-
... The Reindeer start on the Start Tile (marked S) facing East and need to reach the End Tile (marked E). They
can move forward one tile at a time (increasing their score by 1 point), but never into a wall (#). They can
also rotate clockwise or counterclockwise 90 degrees at a time (increasing their score by 1000 points).

To figure out the best place to sit, you start by grabbing a map (your puzzle input) from a nearby kiosk. For example:

	....maze w/o solution...

There are many paths through this maze, but taking any of the best paths would incur a score of only
7036. This can be achieved by taking a total of 36 steps forward and turning 90 degrees a total of 7 times:


	###############
	#.......#....E#
	#.#.###.#.###^#
	#.....#.#...#^#
	#.###.#####.#^#
	#.#.#.......#^#
	#.#.#####.###^#
	#..>>>>>>>>v#^#
	###^#.#####v#^#
	#>>^#.....#v#^#
	#^#.#.###.#v#^#
	#^....#...#v#^#
	#^###.#.#.#v#^#
	#S..#.....#>>^#
	###############

Here's a second example:

	....maze w/o solution...

In this maze, the best paths cost 11048 points; following one such path would look like this:

	#################
	#...#...#...#..E#
	#.#.#.#.#.#.#.#^#
	#.#.#.#...#...#^#
	#.#.#.#.###.#.#^#
	#>>v#.#.#.....#^#
	#^#v#.#.#.#####^#
	#^#v..#.#.#>>>>^#
	#^#v#####.#^###.#
	#^#v#..>>>>^#...#
	#^#v###^#####.###
	#^#v#>>^#.....#.#
	#^#v#^#####.###.#
	#^#v#^........#.#
	#^#v#^#########.#
	#S#>>^..........#
	#################

Note that the path shown above includes one 90 degree turn as the very first move, rotating the Reindeer from
facing East to facing North.

Analyze your map carefully. What is the lowest score a Reindeer could possibly get?

Test:
	% cd ../.. && echo -e '###############\n#.......#....E#\n#.#.###.#.###.#\n#.....#.#...#.#\n#.###.#####.#.#\n#.#.#.......#.#\n#.#.#####.###.#\n#...........#.#\n###.#.#####.#.#\n#...#.....#.#.#\n#.#.#.###.#.#.#\n#.....#...#.#.#\n#.###.#.#.#.#.#\n#S..#.....#...#\n###############' | cabal run 2024_day16-reindeer-maze # = 7036
	% cd ../.. && echo -e '#################\n#...#...#...#..E#\n#.#.#.#.#.#.#.#.#\n#.#.#.#...#...#.#\n#.#.#.#.###.#.#.#\n#...#.#.#.....#.#\n#.#.#.#.#.#####.#\n#.#...#.#.#.....#\n#.#.#####.#.###.#\n#.#.#.......#...#\n#.#.###.#####.###\n#.#.#...#.....#.#\n#.#.#.#####.###.#\n#.#.#.........#.#\n#.#.#.#########.#\n#S#.............#\n#################' | cabal run 2024_day16-reindeer-maze # = 11048
-}

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where pm    = parseMap cs
        start = head [i | (i, 'S') <- assocs pm]
        end   = head [i | (i, 'E') <- assocs pm]

type State = (Point, Point) -- Position, Direction

solveMaze :: (PuzzleMap, Point, Point) -> Maybe (Int, [State])
solveMaze (pm, start, end) = dijkstra
    getNeighbors
    (\(p0, d0) (p1, d1) -> bool 1 0 (p0 == p1) + bool 1000 0 (d0 == d1))
    ((end ==) . fst)
    (start, east)
  where getNeighbors :: State -> [State]
        getNeighbors (pos, dir) = [
          (i, d) | (i, d) <- [(pos + dir, dir), (pos, r), (pos, l)],
                   Just '#' /= pm !? i
          ]
          where r = turnRight dir
                l = turnLeft dir

solve :: (PuzzleMap, Point, Point) -> Int
solve = score . solveMaze
  where score (Just (cost, _)) = cost


main :: IO ()
main = interact $ show . solve . parse

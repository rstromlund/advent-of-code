module Main where
import Data.List (inits, tails)

--	... The Problem Dampener is a reactor-mounted module that lets the reactor safety systems tolerate a
--	single bad level in what would otherwise be a safe report. It's like the bad level never happened!
--
--	Now, the same rules apply as before, except if removing a single level from an unsafe report would
--	make it safe, the report instead counts as safe.
--
--	More of the above example's reports are now safe:
--
--	* 7 6 4 2 1: Safe without removing any level.
--	* 1 2 7 8 9: Unsafe regardless of which level is removed.
--	* 9 7 6 2 1: Unsafe regardless of which level is removed.
--	* 1 3 2 4 5: Safe by removing the second level, 3.
--	* 8 6 4 4 1: Safe by removing the third level, 4.
--	* 1 3 6 7 9: Safe without removing any level.
--
--	Thanks to the Problem Dampener, 4 reports are actually safe!
--
--	Update your analysis by handling situations where the Problem Dampener can remove a single level from
--	unsafe reports. How many reports are now safe?
--
-- Test:
--	% cd ../.. && echo -e '7 6 4 2 1\n1 2 7 8 9\n9 7 6 2 1\n1 3 2 4 5\n8 6 4 4 1\n1 3 6 7 9' | cabal run 2024_day02-red-nosed-reports_b # = 4

parse :: String -> [[Int]]
parse = map (map (read :: String -> Int) . words) . lines

solve :: [[Int]] -> Int
solve = length . filter safeDampener
  where safeDampener = any safe . subReports
          where -- `subReports` creates reports by dropping 1 number at a time
                -- We brute force search `subReports` to find the first (if any) safe report
                subReports r = zipWith (++) (inits r) (tails . tail $ r)
        safe r = all (`between` (1, 3)) ds || all (`between` (-3, -1)) ds
          where ds = zipWith (-) r $ tail r
        between x (l, h) = l <= x && x <= h


main :: IO ()
main = interact $ show . solve . parse

{-# OPTIONS_GHC -Wno-incomplete-patterns -Wno-incomplete-uni-patterns #-}
module Main where
import Data.Array.Unboxed ((!?), assocs)
import Data.Maybe (isNothing)
import Data.Set (Set, insert, singleton, size)
import Point (Point(..), PuzzleMap, north, parseMap, turnRight)

--	... a single guard is patrolling this part of the lab.
--
--	Maybe you can work out where the guard will go ahead of time so that The Historians can search safely?
--
--	You start by making a map (your puzzle input) of the situation. For example:
--
--	....#.....
--	.........#
--	..........
--	..#.......
--	.......#..
--	..........
--	.#..^.....
--	........#.
--	#.........
--	......#...
--
--	The map shows the current position of the guard with ^ (to indicate the guard is currently facing up
--	from the perspective of the map). Any obstructions - crates, desks, alchemical reactors, etc. - are
--	shown as #.
--
--	Lab guards in 1518 follow a very strict patrol protocol which involves repeatedly following these steps:
--
--	* If there is something directly in front of you, turn right 90 degrees.
--	* Otherwise, take a step forward.
--
--	Following the above protocol, the guard moves up several times until she reaches an obstacle (in this case, a pile of failed suit prototypes):
--
--	....#.....
--	....^....#
--	..........
--	..#.......
--	.......#..
--	..........
--	.#........
--	........#.
--	#.........
--	......#...
--
--	Because there is now an obstacle in front of the guard, she turns right before continuing straight in her new facing direction:
--
--	....#.....
--	........>#
--	..........
--	..#.......
--	.......#..
--	..........
--	.#........
--	........#.
--	#.........
--	......#...
--
--	... By predicting the guard's route, you can determine which specific positions in the lab will be in
--	the patrol path. Including the guard's starting position, the positions visited by the guard before
--	leaving the area are marked with an X:
--
--	....#.....
--	....XXXXX#
--	....X...X.
--	..#.X...X.
--	..XXXXX#X.
--	..X.X.X.X.
--	.#XXXXXXX.
--	.XXXXXXX#.
--	#XXXXXXX..
--	......#X..
--
--	In this example, the guard will visit 41 distinct positions on your map.
--
--	Predict the path of the guard. How many distinct positions will the guard visit before leaving the mapped area?
--
-- Test:
--	% cd ../.. && echo -e '....#.....\n.........#\n..........\n..#.......\n.......#..\n..........\n.#..^.....\n........#.\n#.........\n......#...' | cabal run 2024_day06-guard-gallivant # = 41

walk :: PuzzleMap -> Set Point -> Point -> Point -> Set Point
walk pm path pt dir
  | isNothing c'   = path
  | Just '#' /= c' = walk pm (insert i' path) i' dir
  | otherwise      = walk pm path pt . turnRight $ dir
  where i' = pt + dir
        c' = pm !? i'

solve :: PuzzleMap -> Int
solve pm = size . walk pm (singleton start) start $ north
  where [start] = [i | (i, '^') <- assocs pm]


main :: IO ()
main = interact $ show . solve . parseMap

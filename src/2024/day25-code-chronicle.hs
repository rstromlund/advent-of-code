{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.List (groupBy, transpose)

{-
... "The virtual system is complicated, but part of it really is a crude simulation of a five-pin tumbler
lock, mostly for marketing reasons. If you look at the schematics, you can figure out whether a key could
possibly fit in a lock."

He transmits you some example schematics:

	#####
	.####
	.####
	.####
	.#.#.
	.#...
	.....

	#####
	##.##
	.#.##
	...##
	...#.
	...#.
	.....

	.....
	#....
	#....
	#...#
	#.#.#
	#.###
	#####

	.....
	.....
	#.#..
	###..
	###.#
	###.#
	#####

	.....
	.....
	.....
	#....
	#.#..
	#.#.#
	#####

"The locks are schematics that have the top row filled (#) and the bottom row empty (.); the keys have the top
row empty and the bottom row filled. If you look closely, you'll see that each schematic is actually a set of
columns of various heights, either extending downward from the top (for locks) or upward from the bottom (for
keys)."

"For locks, those are the pins themselves; you can convert the pins in schematics to a list of heights, one
per column. For keys, the columns make up the shape of the key where it aligns with pins; those can also be
converted to a list of heights."

"So, you could say the first lock has pin heights 0,5,3,4,3:"

	#####
	.####
	.####
	.####
	.#.#.
	.#...
	.....

"Or, that the first key has heights 5,0,2,1,3:"

	.....
	#....
	#....
	#...#
	#.#.#
	#.###
	#####

"These seem like they should fit together; in the first four columns, the pins and key don't overlap. However,
this key cannot be for this lock: in the rightmost column, the lock's pin overlaps with the key, which you
know because in that column the sum of the lock height and key height is more than the available space."

"So anyway, you can narrow down the keys you'd need to try by just testing each key with each lock, which
means you would have to check... wait, you have how many locks? But the only installation that size is at the
North--" You disconnect the call.

In this example, converting both locks to pin heights produces:

	0,5,3,4,3
	1,2,0,5,3

Converting all three keys to heights produces:

	5,0,2,1,3
	4,3,4,0,2
	3,0,2,0,1

Then, you can try every key with every lock:

	* Lock 0,5,3,4,3 and key 5,0,2,1,3: overlap in the last column.
	* Lock 0,5,3,4,3 and key 4,3,4,0,2: overlap in the second column.
	* Lock 0,5,3,4,3 and key 3,0,2,0,1: all columns fit!
	* Lock 1,2,0,5,3 and key 5,0,2,1,3: overlap in the first column.
	* Lock 1,2,0,5,3 and key 4,3,4,0,2: all columns fit!
	* Lock 1,2,0,5,3 and key 3,0,2,0,1: all columns fit!

So, in this example, the number of unique lock/key pairs that fit together without overlapping in any column
is 3.

Analyze your lock and key schematics. How many unique lock/key pairs fit together without overlapping in any
column?

Test:
	% cd ../.. && echo -e '#####\n.####\n.####\n.####\n.#.#.\n.#...\n.....\n\n#####\n##.##\n.#.##\n...##\n...#.\n...#.\n.....\n\n.....\n#....\n#....\n#...#\n#.#.#\n#.###\n#####\n\n.....\n.....\n#.#..\n###..\n###.#\n###.#\n#####\n\n.....\n.....\n.....\n#....\n#.#..\n#.#.#\n#####' | cabal run 2024_day25-code-chronicle # = 3
-}

parse :: String -> (Int, [(Bool, [Int])])
parse inp = (h, map count ss)
  where ss {-schematics-} = map (dropWhile null) . groupBy (\_ b -> "" /= b) . lines $ inp
        h = length . head . head $ ss
        cnt f = map (pred . length . takeWhile ('#' ==)) . transpose . f
        count :: [String] -> (Bool, [Int])
        count gs@(('#':_):_) = (True,  cnt id gs)
        count gs@(('.':_):_) = (False, cnt reverse gs)

solve :: (Int, [(Bool, [Int])]) -> Int
solve (h, gs) = length [() | k <- ks, l <- ls, not . any (> h) . zipWith (+) k $ l]
  where ks = [is | (True,  is) <- gs]
        ls = [is | (False, is) <- gs]


main :: IO ()
main = interact $ show . solve . parse

{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... some well-hidden elephants are holding a third type of operator.
--
--	The concatenation operator (||) combines the digits from its left and right inputs into a single
--	number. For example, 12 || 345 would become 12345. All operators are still evaluated left-to-right.
--
--	Now, apart from the three equations that could be made true using only addition and multiplication,
--	the above example has three more equations that can be made true by inserting operators:
--
--	* 156: 15 6 can be made true through a single concatenation: 15 || 6 = 156.
--	* 7290: 6 8 6 15 can be made true using 6 * 8 || 6 * 15.
--	* 192: 17 8 14 can be made true using 17 || 8 + 14.
--
--	Adding up all six test values (the three that could be made before using only + and * plus the new
--	three that can now be made by also using ||) produces the new total calibration result of 11387.
--
--	Using your new knowledge of elephant hiding spots, determine which equations could possibly be
--	true. What is their total calibration result?
--
-- Test:
--	% cd ../.. && echo -e '190: 10 19\n3267: 81 40 27\n83: 17 5\n156: 15 6\n7290: 6 8 6 15\n161011: 16 10 13\n192: 17 8 14\n21037: 9 7 18 13\n292: 11 6 16 20' | cabal run 2024_day07-bridge-repair_b # = 11387

type PuzzleList = (Int, [Int])

type Parser = Parsec Void String

plists :: Parser [PuzzleList]
plists = many (plist <* newline) <* eof

plist :: Parser PuzzleList
plist = (,) <$> (decimal <* string ": ") <*> (decimal `sepBy` char ' ')

-- | Concatenate numbers
(||.) :: Integral a => a -> a -> a
(||.) a b = shift' a b + b
  where shift' x 0 = x
        shift' x y = shift' (x * 10) (y `div` 10)

-- | Build all possible combinations of applying (*), (+) and (||.) to solve the equation
calc :: [Int] -> [Int]
calc (f0:fs) =
  foldl (\lhss f ->
            foldl (\acc lhs ->
                     (lhs ||. f):(lhs + f):(lhs * f):acc
                  ) [] lhss
        ) [f0] fs

solve :: Either a [PuzzleList] -> Int
solve (Right ps) = sum [ans | (ans, fs) <- ps, ans `elem` calc fs]


main :: IO ()
main = interact $ show . solve . runParser plists "<stdin>"

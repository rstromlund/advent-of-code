{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.List (isPrefixOf)
import Data.MemoTrie (memo2)

{-
... give them every possible option.

Here are all of the different ways the above example's designs can be made:

brwrr can be made in two different ways: b, r, wr, r or br, wr, r.

bggr can only be made with b, g, g, and r.

gbbr can be made 4 different ways:

	* g, b, b, r
	* g, b, br
	* gb, b, r
	* gb, br

rrbgbr can be made 6 different ways:

	* r, r, b, g, b, r
	* r, r, b, g, br
	* r, r, b, gb, r
	* r, rb, g, b, r
	* r, rb, g, br
	* r, rb, gb, r

bwurrg can only be made with bwu, r, r, and g.

brgr can be made in two different ways: b, r, g, r or br, g, r.

ubwu and bbrgwb are still impossible.

Adding up all of the ways the towels in this example could be arranged into the desired designs yields 16
(2 + 1 + 4 + 6 + 1 + 2).

They'll let you into the onsen as soon as you have the list. What do you get if you add up the number of
different ways you could make each design?

Test:
	% cd ../.. && echo -e 'r, wr, b, g, bwu, rb, gb, br\n\nbrwrr\nbggr\ngbbr\nrrbgbr\nubwu\nbwurrg\nbrgr\nbbrgwb' | cabal run 2024_day19-linen-layout_b # = 16
-}

parse :: String -> ([String], [String])
parse = go . lines
  where go (ts:"":ds) = (words . filter (/= ',') $ ts, ds)

type State = String

findAllPaths :: [String] -> State -> Int
findAllPaths = memo2 findAllPaths'
  where findAllPaths' :: [String] -> State -> Int
        findAllPaths' _ "" = 1
        findAllPaths' ts s = sum . map (findAllPaths ts) $ nextStates s
          where nextStates d = [drop (length t) d | t <- ts, t `isPrefixOf` d]

solve :: [String] -> [String] -> Int
solve ts = sum . map (findAllPaths ts)


main :: IO ()
main = interact $ show . uncurry solve . parse

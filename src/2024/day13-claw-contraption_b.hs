{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Char (isDigit)
import Point (Point(..))

{-
... the position of every prize is actually 10000000000000 higher on both the X and Y axis!

Add 10000000000000 to the X and Y position of every prize. After making this change, the example above would now look like this:

	Button A: X+94, Y+34
	Button B: X+22, Y+67
	Prize: X=10000000008400, Y=10000000005400

	Button A: X+26, Y+66
	Button B: X+67, Y+21
	Prize: X=10000000012748, Y=10000000012176

	Button A: X+17, Y+86
	Button B: X+84, Y+37
	Prize: X=10000000007870, Y=10000000006450

	Button A: X+69, Y+23
	Button B: X+27, Y+71
	Prize: X=10000000018641, Y=10000000010279

Now, it is only possible to win a prize on the second and fourth claw machines. Unfortunately, it will take
many more than 100 presses to do so.

Using the corrected prize coordinates, figure out how to win as many prizes as possible. What is the fewest
tokens you would have to spend to win all possible prizes?

Test:
	% cd ../.. && echo -e 'Button A: X+94, Y+34\nButton B: X+22, Y+67\nPrize: X=8400, Y=5400\n\nButton A: X+26, Y+66\nButton B: X+67, Y+21\nPrize: X=12748, Y=12176\n\nButton A: X+17, Y+86\nButton B: X+84, Y+37\nPrize: X=7870, Y=6450\n\nButton A: X+69, Y+23\nButton B: X+27, Y+71\nPrize: X=18641, Y=10279' | cabal run 2024_day13-claw-contraption_b # = 8,753,186,089,080
-}


{-
NOTES:

a = button A presses
b = button B presses
3a + b = tokens (aka price) <- minimize

E.g.
Button A: X+94, Y+34
Button B: X+22, Y+67
Prize: X=8400, Y=5400

94a + 22b = 8400
34a + 67b = 5400

Minimum iterations now: 106,382,978,813. Would take to long w/ part 1 solution.
Ignoring the new offset for the equation simplification ...

94a + 22b = 8400
34a + 67b = 5400

  94a        + 22b        = 8400
- (94/34)34a + (94/34)67b = (94/34)5400
  =====================================
       22b - (94/34)67b = 8400 - (94/34)5400

b(22 - (94/34)67) = 8400 - (94/34)5400
b = (8400 - (94/34)5400) / (22 - (94/34)67)

a = (8400 - 22b)/94 or
a = (5400 - 67b)/34

So:
  b = (px - (ax / ay) * py) / (bx - (ax / ay) * by)
  a = (py - by * b) / ay
-}

prizeOffset :: Int
prizeOffset = 10000000000000

parse :: String -> [(Point, Point, Point)]
parse = parse' . words
  where parse' [] = []
        parse' ("Button": "A:": ('X':'+':ax): ('Y':'+':ay):
                "Button": "B:": ('X':'+':bx): ('Y':'+':by):
                "Prize:":       ('X':'=':px): ('Y':'=':py):
                cs) =
          (mkPt ay ax, mkPt by bx, Point (toint py + prizeOffset) (toint px + prizeOffset)):parse' cs
        mkPt sy sx = Point (toint sy) (toint sx)
        toint :: String -> Int
        toint = read . takeWhile isDigit

solve :: [(Point, Point, Point)] -> Int
solve = sum . map findSolution
  where findSolution :: (Point, Point, Point) -> Int
        findSolution (Point ay ax, Point by bx, Point py px)
          | px == guessX &&
            py == guessY = a * 3 + b
          | otherwise    = 0
          where ay' = fromIntegral ay :: Double
                ax' = fromIntegral ax :: Double
                by' = fromIntegral by :: Double
                bx' = fromIntegral bx :: Double
                py' = fromIntegral py :: Double
                px' = fromIntegral px :: Double
                --
                b = round $ (px' - (ax' / ay') * py') / (bx' - (ax' / ay') * by')
                a = (py - by * b) `div` ay
                guessX = ax * a + bx * b
                guessY = ay * a + by * b


main :: IO ()
main = interact $ show . solve . parse

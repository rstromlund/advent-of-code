{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Data.Char (digitToInt)
import Data.List (delete)

--	... try compacting the files on his disk by moving whole files instead.
--
--	This time, attempt to move whole files to the leftmost span of free space blocks that could fit the
--	file. Attempt to move each file exactly once in order of decreasing file ID number starting with the
--	file with the highest file ID number. If there is no span of free space to the left of a file that is
--	large enough to fit the file, the file does not move.
--
--	The first example from above now proceeds differently:
--
--	00...111...2...333.44.5555.6666.777.888899
--	0099.111...2...333.44.5555.6666.777.8888..
--	0099.1117772...333.44.5555.6666.....8888..
--	0099.111777244.333....5555.6666.....8888..
--	00992111777.44.333....5555.6666.....8888..
--
--	The process of updating the filesystem checksum is the same; now, this example's checksum would be 2858.
--
--	Start over, now compacting the amphipod's hard drive using this new method instead. What is the resulting filesystem checksum?
--
-- Test:
--	% cd ../.. && echo -e '2333133121414131402' | cabal run 2024_day09-disk-fragmenter_b # = 2858

type Pos  = Int
type Id   = Int
type Size = Int

data Node = File Pos Size Id | Space Pos Size deriving (Eq, Show)
isSpace :: Node -> Bool
isSpace (Space _ _) = True
isSpace _           = False

parse :: String -> [Node]
parse = fileDescr [] (0 :: Pos) (0 :: Id) . map digitToInt . head . lines
  where fileDescr :: [Node] -> Pos -> Id -> [Int] -> [Node]
        fileDescr  acc pos fileid (blks:cs')  = spaceDescr (acc' blks (File pos blks fileid) acc) (pos + blks) (fileid + 1) cs'
        fileDescr  acc _ _ []                 = reverse acc
        spaceDescr acc _ _ []                 = reverse acc
        spaceDescr acc pos fileid (blks:cs')  = fileDescr (acc' blks (Space pos blks) acc) (pos + blks) fileid cs'
        acc' 0 _ acc = acc
        acc' _ n acc = n:acc

compact :: [Node] -> [Node]
compact ns = compact' ns . reverse . filter (not . isSpace) $ ns
  where compact' :: [Node] -> [Node] -> [Node]
        compact' cs (mvF@(File posF szF idF):ds) =
          case [x | x@(Space pos sz) <- cs, sz >= szF, pos < posF] of
            (s:_) ->  compact' (moveFile s . delete mvF $ cs) ds
            []    ->  compact' cs ds
          where moveFile :: Node -> [Node] -> [Node]
                moveFile s@(Space posS szS) (s':cs') | s == s' =
                  let f'    = File posS szF idF
                      spRem = szS - szF
                      cs''  | spRem > 0 = f':Space (posS + szF) spRem:cs'
                            | otherwise = f':cs'
                  in cs''
                moveFile s (n:cs') = n:moveFile s cs'
        compact' cs [] = cs

fsCksum :: [Node] -> Int
fsCksum ns = sum [id' * pos' | File pos sz id' <- ns, pos' <- [pos..(pos + sz - 1)]]

solve :: [Node] -> Int
solve = fsCksum . compact


main :: IO ()
main = interact $ show . solve . parse

x00: 1
x01: 0
x02: 0
x03: 1
x04: 1
x05: 1
x06: 0
x07: 0
x08: 0
x09: 1
x10: 0
x11: 0
x12: 0
x13: 0
x14: 1
x15: 0
x16: 0
x17: 0
x18: 0
x19: 0
x20: 0
x21: 0
x22: 0
x23: 1
x24: 0
x25: 1
x26: 0
x27: 0
x28: 0
x29: 0
x30: 1
x31: 0
x32: 1
x33: 0
x34: 0
x35: 0
x36: 1
x37: 0
x38: 1
x39: 1
x40: 0
x41: 1
x42: 1
x43: 1
x44: 1
y00: 1
y01: 1
y02: 1
y03: 1
y04: 0
y05: 1
y06: 0
y07: 1
y08: 0
y09: 1
y10: 1
y11: 1
y12: 1
y13: 1
y14: 0
y15: 0
y16: 0
y17: 0
y18: 0
y19: 0
y20: 0
y21: 0
y22: 1
y23: 0
y24: 0
y25: 1
y26: 0
y27: 1
y28: 1
y29: 1
y30: 0
y31: 0
y32: 0
y33: 1
y34: 0
y35: 0
y36: 0
y37: 1
y38: 0
y39: 1
y40: 1
y41: 1
y42: 0
y43: 0
y44: 1

x00 XOR y00 -> z00
x00 AND y00 -> cout00(bdj)
#
x01 XOR y01 -> sumxy01(twd)
sumxy01(twd) XOR cout00(bdj) -> z01
x01 AND y01 -> carryab01(gwd)
sumxy01(twd) AND cout00(bdj) -> carryfwd01(cbq)
carryab01(gwd) OR carryfwd01(cbq) -> cout01(rhr)
#
x02 XOR y02 -> sumxy02(rsk)
sumxy02(rsk) XOR cout01(rhr) -> z02
x02 AND y02 -> carryab02(fph)
sumxy02(rsk) AND cout01(rhr) -> carryfwd02(nkm)
carryab02(fph) OR carryfwd02(nkm) -> cout02(twj)
#
x03 XOR y03 -> sumxy03(jfr)
sumxy03(jfr) XOR cout02(twj) -> z03
x03 AND y03 -> carryab03(rnw)
sumxy03(jfr) AND cout02(twj) -> carryfwd03(nbj)
carryab03(rnw) OR carryfwd03(nbj) -> cout03(dft)
#
x04 XOR y04 -> sumxy04(vwf)
sumxy04(vwf) XOR cout03(dft) -> z04
x04 AND y04 -> carryab04(hwt)
sumxy04(vwf) AND cout03(dft) -> carryfwd04(mfj)
carryab04(hwt) OR carryfwd04(mfj) -> cout04(str)
#
x05 XOR y05 -> sumxy05(gtt)
sumxy05(gtt) XOR cout04(str) -> z05
x05 AND y05 -> carryab05(vth)
sumxy05(gtt) AND cout04(str) -> carryfwd05(jvv)
carryab05(vth) OR carryfwd05(jvv) -> cout05(rjb)
#
x06 XOR y06 -> sumxy06(jrg)
sumxy06(jrg) XOR cout05(rjb) -> z06
x06 AND y06 -> carryab06(dbc)
sumxy06(jrg) AND cout05(rjb) -> carryfwd06(mjs)
carryab06(dbc) OR carryfwd06(mjs) -> cout06(mwg)
#
x07 XOR y07 -> sumxy07(wnd)
sumxy07(wnd) XOR cout06(mwg) -> z07
x07 AND y07 -> carryab07(vqk)
sumxy07(wnd) AND cout06(mwg) -> carryfwd07(hnm)
carryab07(vqk) OR carryfwd07(hnm) -> cout07(whc)
#
x08 XOR y08 -> sumxy08(ndh)
sumxy08(ndh) XOR cout07(whc) -> z08
x08 AND y08 -> carryab08(gth)
sumxy08(ndh) AND cout07(whc) -> carryfwd08(rbb)
carryab08(gth) OR carryfwd08(rbb) -> cout08(bpc)
#
x09 XOR y09 -> sumxy09(hsq)
sumxy09(hsq) XOR cout08(bpc) -> z09
x09 AND y09 -> carryab09(qsm)
sumxy09(hsq) AND cout08(bpc) -> carryfwd09(rjj)
carryab09(qsm) OR carryfwd09(rjj) -> cout09(kqs)
#
x10 XOR y10 -> sumxy10(fnm)
sumxy10(fnm) XOR cout09(kqs) -> z10
x10 AND y10 -> carryab10(mjb)
sumxy10(fnm) AND cout09(kqs) -> carryfwd10(knt)
carryab10(mjb) OR carryfwd10(knt) -> cout10(tnr)
#
x11 XOR y11 -> sumxy11(kdw)
sumxy11(kdw) XOR cout10(tnr) -> z11
x11 AND y11 -> carryab11(mdq)
sumxy11(kdw) AND cout10(tnr) -> carryfwd11(fkw)
carryab11(mdq) OR carryfwd11(fkw) -> cout11(cdq)
#!
x12 XOR y12 -> sumxy12(nhb)
sumxy12(nhb) XOR cout11(cdq) -> kth ! (swap kth with cout12(z12))
x12 AND y12 -> carryab12(psw)
sumxy12(nhb) AND cout11(cdq) -> carryfwd12(nng)
carryab12(psw) OR carryfwd12(nng) -> cout12(z12)
#!
x13 XOR y13 -> sumxy13(mtp)
sumxy13(mtp) XOR kth -> z13 ! (same as above, not a new solution: swap kth with cout12(z12))
x13 AND y13 -> carryab13(qrs)
sumxy13(mtp) AND kth -> carryfwd13(cns)
carryab13(qrs) OR carryfwd13(cns) -> cout13(rpt)
#
x14 XOR y14 -> sumxy14(hbh)
sumxy14(hbh) XOR cout13(rpt) -> z14
x14 AND y14 -> carryab14(skj)
sumxy14(hbh) AND cout13(rpt) -> carryfwd14(ctc)
carryab14(skj) OR carryfwd14(ctc) -> cout14(sqb)
#
x15 XOR y15 -> sumxy15(ckk)
sumxy15(ckk) XOR cout14(sqb) -> z15
x15 AND y15 -> carryab15(jhq)
sumxy15(ckk) AND cout14(sqb) -> carryfwd15(dgn)
carryab15(jhq) OR carryfwd15(dgn) -> cout15(msm)
#
x16 XOR y16 -> sumxy16(qdt)
sumxy16(qdt) XOR cout15(msm) -> z16
x16 AND y16 -> carryab16(hjd)
sumxy16(qdt) AND cout15(msm) -> carryfwd16(bjj)
carryab16(hjd) OR carryfwd16(bjj) -> cout16(qjv)
#
x17 XOR y17 -> sumxy17(pts)
sumxy17(pts) XOR cout16(qjv) -> z17
x17 AND y17 -> carryab17(tkm)
sumxy17(pts) AND cout16(qjv) -> carryfwd17(mmm)
carryab17(tkm) OR carryfwd17(mmm) -> cout17(fhb)
#
x18 XOR y18 -> sumxy18(pbj)
sumxy18(pbj) XOR cout17(fhb) -> z18
x18 AND y18 -> carryab18(pbd)
sumxy18(pbj) AND cout17(fhb) -> carryfwd18(wfr)
carryab18(pbd) OR carryfwd18(wfr) -> cout18(spq)
#
x19 XOR y19 -> sumxy19(jvg)
sumxy19(jvg) XOR cout18(spq) -> z19
x19 AND y19 -> carryab19(hgk)
sumxy19(jvg) AND cout18(spq) -> carryfwd19(gfv)
carryab19(hgk) OR carryfwd19(gfv) -> cout19(crt)
#
x20 XOR y20 -> sumxy20(vhk)
sumxy20(vhk) XOR cout19(crt) -> z20
x20 AND y20 -> carryab20(khd)
sumxy20(vhk) AND cout19(crt) -> carryfwd20(qgh)
carryab20(khd) OR carryfwd20(qgh) -> cout20(gjg)
#
x21 XOR y21 -> sumxy21(nkr)
sumxy21(nkr) XOR cout20(gjg) -> z21
x21 AND y21 -> carryab21(mtd)
sumxy21(nkr) AND cout20(gjg) -> carryfwd21(dvg)
carryab21(mtd) OR carryfwd21(dvg) -> cout21(rtk)
#
x22 XOR y22 -> sumxy22(kqc)
sumxy22(kqc) XOR cout21(rtk) -> z22
x22 AND y22 -> carryab22(hjf)
sumxy22(kqc) AND cout21(rtk) -> carryfwd22(ssd)
carryab22(hjf) OR carryfwd22(ssd) -> cout22(gvs)
#
x23 XOR y23 -> sumxy23(tmj)
sumxy23(tmj) XOR cout22(gvs) -> z23
x23 AND y23 -> carryab23(frm)
sumxy23(tmj) AND cout22(gvs) -> carryfwd23(psk)
carryab23(frm) OR carryfwd23(psk) -> cout23(wfg)
#
x24 XOR y24 -> sumxy24(qbp)
sumxy24(qbp) XOR cout23(wfg) -> z24
x24 AND y24 -> carryab24(qcb)
sumxy24(qbp) AND cout23(wfg) -> carryfwd24(fws)
carryab24(qcb) OR carryfwd24(fws) -> cout24(pmm)
#
x25 XOR y25 -> sumxy25(msc)
sumxy25(msc) XOR cout24(pmm) -> z25
x25 AND y25 -> carryab25(qmv)
sumxy25(msc) AND cout24(pmm) -> carryfwd25(ftq)
carryab25(qmv) OR carryfwd25(ftq) -> cout25(mbg)
#!
x26 XOR y26 -> sumxy26(dfp)
sumxy26(dfp) XOR cout25(mbg) -> gsd ! (swap gsd with z26)
x26 AND y26 -> carryab26(z26)
sumxy26(dfp) AND cout25(mbg) -> carryfwd26(kbg)
gsd OR carryfwd26(kbg) -> cout26(cmf)
#
x27 XOR y27 -> sumxy27(swt)
sumxy27(swt) XOR cout26(cmf) -> z27
x27 AND y27 -> carryab27(tbc)
sumxy27(swt) AND cout26(cmf) -> carryfwd27(nvm)
carryab27(tbc) OR carryfwd27(nvm) -> cout27(ghk)
#
x28 XOR y28 -> sumxy28(pgc)
sumxy28(pgc) XOR cout27(ghk) -> z28
x28 AND y28 -> carryab28(crb)
sumxy28(pgc) AND cout27(ghk) -> carryfwd28(qhw)
carryab28(crb) OR carryfwd28(qhw) -> cout28(vbp)
#
x29 XOR y29 -> sumxy29(fht)
sumxy29(fht) XOR cout28(vbp) -> z29
x29 AND y29 -> carryab29(drg)
sumxy29(fht) AND cout28(vbp) -> carryfwd29(wbb)
carryab29(drg) OR carryfwd29(wbb) -> cout29(nvq)
#
x30 XOR y30 -> sumxy30(gtd)
sumxy30(gtd) XOR cout29(nvq) -> z30
x30 AND y30 -> carryab30(rkn)
sumxy30(gtd) AND cout29(nvq) -> carryfwd30(mck)
carryab30(rkn) OR carryfwd30(mck) -> cout30(dtj)
#
x31 XOR y31 -> sumxy31(trd)
sumxy31(trd) XOR cout30(dtj) -> z31
x31 AND y31 -> carryab31(nhq)
sumxy31(trd) AND cout30(dtj) -> carryfwd31(bjf)
carryab31(nhq) OR carryfwd31(bjf) -> cout31(vtg)
#!
x32 XOR y32 -> sumxy32(bkh)
sumxy32(bkh) XOR cout31(vtg) -> tbt ! (swap tbt with z32)
x32 AND y32 -> carryab32(skt)
sumxy32(bkh) AND cout31(vtg) -> carryfwd32(z32)
carryab32(skt) OR tbt -> cout32(nwm)
#
x33 XOR y33 -> sumxy33(rpb)
sumxy33(rpb) XOR cout32(nwm) -> z33
x33 AND y33 -> carryab33(jtq)
sumxy33(rpb) AND cout32(nwm) -> carryfwd33(kmb)
carryab33(jtq) OR carryfwd33(kmb) -> cout33(ksf)
#
x34 XOR y34 -> sumxy34(bhh)
sumxy34(bhh) XOR cout33(ksf) -> z34
x34 AND y34 -> carryab34(vkh)
sumxy34(bhh) AND cout33(ksf) -> carryfwd34(dcc)
carryab34(vkh) OR carryfwd34(dcc) -> cout34(brg)
#
x35 XOR y35 -> sumxy35(jng)
sumxy35(jng) XOR cout34(brg) -> z35
x35 AND y35 -> carryab35(cnp)
sumxy35(jng) AND cout34(brg) -> carryfwd35(bbb)
carryab35(cnp) OR carryfwd35(bbb) -> cout35(htb)
#!
x36 XOR y36 -> sumxy36(vpm)           ! (swap sumxy36(vpm) with carryab36(qnf))
carryab36(qnf) XOR cout35(htb) -> z36
x36 AND y36 -> carryab36(qnf)
carryab36(qnf) AND cout35(htb) -> carryfwd36(thg)
sumxy36(vpm) OR carryfwd36(thg) -> cout36(wkk)
#
x37 XOR y37 -> sumxy37(hpp)
sumxy37(hpp) XOR cout36(wkk) -> z37
x37 AND y37 -> carryab37(ggw)
sumxy37(hpp) AND cout36(wkk) -> carryfwd37(gpv)
carryab37(ggw) OR carryfwd37(gpv) -> cout37(ths)
#
x38 XOR y38 -> sumxy38(phr)
sumxy38(phr) XOR cout37(ths) -> z38
x38 AND y38 -> carryab38(kmg)
sumxy38(phr) AND cout37(ths) -> carryfwd38(bdg)
carryab38(kmg) OR carryfwd38(bdg) -> cout38(hhd)
#
x39 XOR y39 -> sumxy39(qnv)
sumxy39(qnv) XOR cout38(hhd) -> z39
x39 AND y39 -> carryab39(bdr)
sumxy39(qnv) AND cout38(hhd) -> carryfwd39(tct)
carryab39(bdr) OR carryfwd39(tct) -> cout39(pbt)
#
x40 XOR y40 -> sumxy40(ttt)
sumxy40(ttt) XOR cout39(pbt) -> z40
x40 AND y40 -> carryab40(fhh)
sumxy40(ttt) AND cout39(pbt) -> carryfwd40(hkv)
carryab40(fhh) OR carryfwd40(hkv) -> cout40(ttv)
#
x41 XOR y41 -> sumxy41(jkb)
sumxy41(jkb) XOR cout40(ttv) -> z41
x41 AND y41 -> carryab41(rgv)
sumxy41(jkb) AND cout40(ttv) -> carryfwd41(bvs)
carryab41(rgv) OR carryfwd41(bvs) -> cout41(mdd)
#
x42 XOR y42 -> sumxy42(spr)
sumxy42(spr) XOR cout41(mdd) -> z42
x42 AND y42 -> carryab42(rcn)
sumxy42(spr) AND cout41(mdd) -> carryfwd42(pdt)
carryab42(rcn) OR carryfwd42(pdt) -> cout42(dpg)
#
x43 XOR y43 -> sumxy43(gnj)
sumxy43(gnj) XOR cout42(dpg) -> z43
x43 AND y43 -> carryab43(hsd)
sumxy43(gnj) AND cout42(dpg) -> carryfwd43(nsf)
carryab43(hsd) OR carryfwd43(nsf) -> cout43(jnj)
#
x44 XOR y44 -> sumxy44(nnt)
sumxy44(nnt) XOR cout43(jnj) -> z44
x44 AND y44 -> carryab44(vdn)
sumxy44(nnt) AND cout43(jnj) -> carryfwd44(qtn)
carryab44(vdn) OR carryfwd44(qtn) -> z45

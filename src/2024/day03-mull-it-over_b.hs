{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Functor (($>))
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, anySingle, many, runParser, try)
import Text.Megaparsec.Char (char, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	As you scan through the corrupted memory, you notice that some of the conditional statements are also
--	still intact. If you handle some of the uncorrupted conditional statements in the program, you might
--	be able to get an even more accurate result.
--
--	There are two new instructions you'll need to handle:
--
--	* The do() instruction enables future mul instructions.
--	* The don't() instruction disables future mul instructions.
--
--	Only the most recent do() or don't() instruction applies. At the beginning of the program, mul
--	instructions are enabled.
--
--	For example:
--
--	`xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))`
--
--	This corrupted memory is similar to the example from before, but this time the mul(5,5) and mul(11,8)
--	instructions are disabled because there is a don't() instruction before them. The other mul
--	instructions function normally, including the one at the end that gets re-enabled by a do()
--	instruction.
--
--	This time, the sum of the results is 48 (2*4 + 8*5).
--
--	Handle the new instructions; what do you get if you add up all of the results of just the enabled multiplications?
--
-- Test:
--	% cd ../.. && echo -e "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))" | cabal run 2024_day03-mull-it-over_b # = 48

data Instruction = MulInstr Int Int | DO | DONT deriving (Show)

type Parser = Parsec Void String

instrs :: Parser [Instruction]
instrs = many instr

mulinstr :: Parser Instruction
mulinstr = MulInstr <$ string "mul(" <*> decimal <* char ',' <*> decimal <* char ')'

instr :: Parser Instruction
instr = try mulinstr
        <|> string "do()"    $> DO
        <|> string "don't()" $> DONT
        <|> try (anySingle *> instr)

solve :: Either a [Instruction] -> Int
solve (Right is) = fst . foldl' step (0, True) $ is
  where step (a, _) DO   = (a, True)
        step (a, _) DONT = (a, False)
        step (a, True) (MulInstr op1 op2) = (a + op1 * op2, True)
        step acc    _    = acc


main :: IO ()
main = interact $ show . solve . runParser instrs "<stdin>"

{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns -Wno-name-shadowing #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed as AU ((!?), assocs)
import Data.Bool (bool)
import Data.List (nub)
import Data.Map.Strict as MS ((!), (!?), Map, assocs, delete, empty, filterWithKey, foldlWithKey', fromList, insert, size)
import Data.Maybe (fromMaybe)
import Point (Point(..), PuzzleMap, cardinalDirections, parseMap, turnLeft, turnRight, north, east)
import Debug.Trace (trace)
-- trace_ = trace
trace_ _ x = x

{-
Now that you know what the best paths look like, you can figure out the best spot to sit.

Every non-wall tile (S, ., or E) is equipped with places to sit along the edges of the tile. While determining
which of these tiles would be the best spot to sit depends on a whole bunch of factors (how comfortable the
seats are, how far away the bathrooms are, whether there's a pillar blocking your view, etc.), the most
important factor is whether the tile is on one of the best paths through the maze. If you sit somewhere else,
you'd miss all the action!

So, you'll need to determine which tiles are part of any best path through the maze, including the S and E tiles.

In the first example, there are 45 tiles (marked O) that are part of at least one of the various best paths
through the maze:

	###############
	#.......#....O#
	#.#.###.#.###O#
	#.....#.#...#O#
	#.###.#####.#O#
	#.#.#.......#O#
	#.#.#####.###O#
	#..OOOOOOOOO#O#
	###O#O#####O#O#
	#OOO#O....#O#O#
	#O#O#O###.#O#O#
	#OOOOO#...#O#O#
	#O###.#.#.#O#O#
	#O..#.....#OOO#
	###############

In the second example, there are 64 tiles that are part of at least one of the best paths:

	#################
	#...#...#...#..O#
	#.#.#.#.#.#.#.#O#
	#.#.#.#...#...#O#
	#.#.#.#.###.#.#O#
	#OOO#.#.#.....#O#
	#O#O#.#.#.#####O#
	#O#O..#.#.#OOOOO#
	#O#O#####.#O###O#
	#O#O#..OOOOO#OOO#
	#O#O###O#####O###
	#O#O#OOO#..OOO#.#
	#O#O#O#####O###.#
	#O#O#OOOOOOO..#.#
	#O#O#O#########.#
	#O#OOO..........#
	#################

Analyze your map further. How many tiles are part of at least one of the best paths through the maze?

Test:
	% cd ../.. && echo -e '###############\n#.......#....E#\n#.#.###.#.###.#\n#.....#.#...#.#\n#.###.#####.#.#\n#.#.#.......#.#\n#.#.#####.###.#\n#...........#.#\n###.#.#####.#.#\n#...#.....#.#.#\n#.#.#.###.#.#.#\n#.....#...#.#.#\n#.###.#.#.#.#.#\n#S..#.....#...#\n###############' | cabal run 2024_day16-reindeer-maze_b # = 45
	% cd ../.. && echo -e '#################\n#...#...#...#..E#\n#.#.#.#.#.#.#.#.#\n#.#.#.#...#...#.#\n#.#.#.#.###.#.#.#\n#...#.#.#.....#.#\n#.#.#.#.#.#####.#\n#.#...#.#.#.....#\n#.#.#####.#.###.#\n#.#.#.......#...#\n#.#.###.#####.###\n#.#.#...#.....#.#\n#.#.#.#####.###.#\n#.#.#.........#.#\n#.#.#.#########.#\n#S#.............#\n#################' | cabal run 2024_day16-reindeer-maze_b # = 64
-}

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where pm    = parseMap cs
        start = head [i | (i, 'S') <- AU.assocs pm]
        end   = head [i | (i, 'E') <- AU.assocs pm]

{-
type State = (Point, Point) -- Position, Direction

solveMaze :: PuzzleMap -> State -> Point -> Maybe (Int, [State])
solveMaze pm st0 end = dijkstra
    getNeighbors
    (\(p0, d0) (p1, d1) -> bool 1 0 (p0 == p1) + bool 1000 0 (d0 == d1))
    ((end ==) . fst)
    st0
  where getNeighbors :: State -> [State]
        getNeighbors (pos, dir) = [
          (i, d) | (i, d) <- [(pos + dir, dir), (pos, r), (pos, l)],
                   Just '#' /= pm !? i
          ]
          where r = turnRight dir
                l = turnLeft dir

-- Ugly brute force; Floyd-Warshall would no doubt be better here.  But we
-- force Dijkstra's algorithm to go thru every intersection in the maze; filtering
-- cheapest unique routes.

solveMidPt :: PuzzleMap -> Point -> Point -> Point -> Int -> Maybe (Int, [State])
solveMidPt pm start end mpt minc =
  let ans = if c0 < minc && c1 < minc && c0 + c1 == minc then Just (c0 + c1, ts0 ++ ts1) else Nothing
  in trace_ (show ("solveMidPt", start, end, mpt, "ans", ans)) $ ans
  where Just (c0, ts0) = solveMaze pm (start, east) mpt
        Just (c1, ts1) = solveMaze pm (last ts0) end

-- 669 is too low :/
-- 670 is the right answer!  Wuh?
-- But this took 19+ minutes.

solve :: (PuzzleMap, Point, Point) -> Int
solve (pm, start, end) = score . nub $ [
  ts
    | (i, '.') <- AU.assocs pm,
      -- only consider intersections where decisions need to be made
      let ns = [i' | d <- cardinalDirections, let i' = i + d, Just '.' == pm !? i'],
      length ns > 2,
      t <- i:ns,
      Just (c, ts) <- [solveMidPt pm start end t minc],
      c == minc
  ]
  where Just (minc, _) = solveMaze pm (start, east) end
        score :: [[State]] -> Int
        score = length . nub . concatMap (map fst)


main :: IO ()
main = interact $ show . solve . parse
-}

-- Floyd–Warshall algorithm
-- ref: https://en.wikipedia.org/wiki/Floyd%E2%80%93Warshall_algorithm
-- ref: 2022/day16-proboscidea-volcanium.hs

type State = (Point, Point) -- Position, Direction

getVertices :: PuzzleMap -> [State]
getVertices pm = [
  (i, d)
    | (i, ch) <- AU.assocs pm,
      '#' /= ch,
      d <- cardinalDirections
  ]

getEdges :: PuzzleMap -> [State] -> [((State, State), Int)]
getEdges pm vs = concatMap getEdges' vs
  where getEdges' s@(pos, dir) = [
          ((s, s'), 1000)
            | s'@(pos', dir') <- [v | v@(p, d) <- vs, p == pos && d /= dir],
              -- make edge to all other 1 turn orientations in the same spot
              let dplus = dir + dir',
              dplus /= Point 0 0
          ] ++ [
          ((s, s'), 1)
            | s' <- [v | v@(p, d) <- vs, d == dir && p == pos + d]
          ]

floydWarshall :: [State] -> [((State, State), Int)] -> (Map (State, State) Int, Map (State, State) State)
floydWarshall vs es = fw
  where es' = fromList $ es ++ map (\r -> ((r, r), 0)) vs
        prev :: Map (State, State) State
        prev = fromList [((u, v), u) | ((u, v), _) <- MS.assocs es']
        --
        getW :: State -> State -> Map (State, State) Int -> Int
        getW r1 r2 edgs = fromMaybe (maxBound `div` 3) $ edgs MS.!? (r1, r2)
        --
        fw = foldl' kloop (es', prev) vs
          where kloop :: (Map (State, State) Int, Map (State, State) State) -> State -> (Map (State, State) Int, Map (State, State) State)
                kloop (dist, prev) k = foldl' iloop (dist, prev) vs
                  where iloop (dist, prev) i = foldl' jloop (dist, prev) vs
                          where jloop (dist, prev) j =
                                  let dIJ = getW i j dist
                                      dIK = getW i k dist
                                      dKJ = getW k j dist
                                      pKJ = case prev MS.!? (k, j) of
                                              Just pt -> trace_ (show ("set prev", i, j, '=', k, j, pt)) $ pt
                                              _       -> error (show ("no k,j", i, j, '=', k, j, dKJ))
                                  in if dIJ > dIK + dKJ then (insert (i, j) (dIK + dKJ) dist, insert (i, j) pKJ prev) else (dist, prev)

getPath :: State -> State -> Map (State, State) State -> [State]
getPath u v prev = getPath' v [v]
  where getPath' v' path
          | u == v'   = path
          | otherwise = let (Just v) = prev MS.!? (u, v')
                        in getPath' v (v:path)

solve :: (PuzzleMap, Point, Point) -> String -- Int
solve (pm, start, end) = show $ dist MS.!? (sstate, estate)
  -- show $ (dist MS.!? (sstate, estate), getPath sstate estate prev, (vs, es))
  where sstate = (start, east)
        estate = (end,   north)
        vs = getVertices pm
        es = getEdges pm vs
        (dist, prev) = floydWarshall vs es


main :: IO ()
main = interact $ show . solve . parse

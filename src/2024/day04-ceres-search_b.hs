{-# LANGUAGE ParallelListComp #-}
module Main where
import Data.Array.Unboxed ((!?), assocs)
import Point (Point(..), PuzzleMap, diagonalDirections, parseMap)

--	... it's an X-MAS puzzle in which you're supposed to find two MAS in the shape of an X. One way to achieve that is like this:
--
--	M.S
--	.A.
--	M.S
--
--	Irrelevant characters have again been replaced with . in the above diagram. Within the X, each MAS can be written forwards or backwards.
--
--	Here's the same example from before, but this time all of the X-MASes have been kept instead:
--
--	.M.S......
--	..A..MSMS.
--	.M.S.MAA..
--	..A.ASMSM.
--	.M.S.M....
--	..........
--	S.S.S.S.S.
--	.A.A.A.A..
--	M.M.M.M.M.
--	..........
--
--	In this example, an X-MAS appears 9 times.
--
--	... How many times does an X-MAS appear?
--
-- Test:
--	% cd ../.. && echo -e 'MMMSXXMASM\nMSAMXMSMSA\nAMXSXMAAMM\nMSAMASMSMX\nXMASAMXAMM\nXXAMMXXAMA\nSMSMSASXSS\nSAXAMASAAA\nMAMMMXMMMM\nMXMXAXMASX' | cabal run 2024_day04-ceres-search_b # = 9

wordSearch :: String
wordSearch = "MAS"
wordLen    :: Int
wordLen    = length wordSearch

-- `crossDir` takes ordinal direction of first match and finds the 2 possible cross match direction and
-- location offset from first match.  This gives a more general solution (e.g. finding SANTA) than looking
-- specifically for 'A's and then matching 'M's and 'S's.
crossDir :: Point -> [(Point, Point)]
crossDir (Point dy dx) = [
    (Point 0 ((wordLen - 1) * dx), Point dy (- dx))
  , (Point ((wordLen - 1) * dy) 0, Point (- dy) dx)
  ]

solve :: PuzzleMap -> Int
solve ps = length [
  ()
  | (ix, m) <- assocs ps
  , m == head wordSearch
  , dir <- diagonalDirections
  -- Find first match
  , match ix dir

  -- Find cross match
  , (ix', dir') <- crossDir dir
  , match (ix' + ix) dir'
  ] `div` 2 -- we found both 'M's and matched from both sides, so div by 2.
  where match :: Point -> Point -> Bool
        match ix dir = and [Just ch == ps !? i | ch <- wordSearch | i <- iterate (dir +) ix]


main :: IO ()
main = interact $ show . solve . parseMap

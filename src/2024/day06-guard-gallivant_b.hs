{-# OPTIONS_GHC -Wno-incomplete-patterns -Wno-incomplete-uni-patterns #-}
module Main where
import Data.Array.Unboxed ((!?), (//), assocs)
import Data.Maybe (isNothing)
import Data.Set (Set, insert, member, singleton)
import Point (Point(..), PuzzleMap, north, parseMap, turnRight)

--	... They'd like to place the new obstruction in such a way that the guard will get stuck in a loop,
--	making the rest of the lab safe to search.
--
--	To have the lowest chance of creating a time paradox, The Historians would like to know all of the
--	possible positions for such an obstruction. The new obstruction can't be placed at the guard's
--	starting position - the guard is there right now and would notice.
--
--	In the above example, there are only 6 different positions where a new obstruction would cause the
--	guard to get stuck in a loop. The diagrams of these six situations use O to mark the new obstruction,
--	| to show a position where the guard moves up/down, - to show a position where the guard moves
--	left/right, and + to show a position where the guard moves both up/down and left/right.
--
--	Option one, put a printing press next to the guard's starting position:
--
--	....#.....
--	....+---+#
--	....|...|.
--	..#.|...|.
--	....|..#|.
--	....|...|.
--	.#.O^---+.
--	........#.
--	#.........
--	......#...
--
--	... It doesn't really matter what you choose to use as an obstacle so long as you and The Historians
--	can put it into position without the guard noticing. The important thing is having enough options that
--	you can find one that minimizes time paradoxes, and in this example, there are 6 different positions
--	you could choose.
--
--	You need to get the guard stuck in a loop by adding a single new obstruction. How many different
--	positions could you choose for this obstruction?
--
-- Test:
--	% cd ../.. && echo -e '....#.....\n.........#\n..........\n..#.......\n.......#..\n..........\n.#..^.....\n........#.\n#.........\n......#...' | cabal run 2024_day06-guard-gallivant_b # = 6

walk :: PuzzleMap -> Set (Point, Point) -> Point -> Point -> Bool
walk pm path pt dir
  | isNothing c'   = False
  | Just '#' /= c' = let pos = (i', dir)
                     in pos `member` path || walk pm (insert pos path) i' dir
  | otherwise      = walk pm path pt . turnRight $ dir
  where i' = pt + dir
        c' = pm !? i'

solve :: PuzzleMap -> Int
solve pm = length [
  ()
    | (i, '.') <- assocs pm
    , let pm' = pm // [(i, '#')]
    , walk pm' (singleton (start, north)) start north
  ]
  where [start] = [i | (i, '^') <- assocs pm]


main :: IO ()
main = interact $ show . solve . parseMap

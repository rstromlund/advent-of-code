{-# OPTIONS_GHC -Wno-name-shadowing #-}
module Main where
import Data.Array.Unboxed ((!?), assocs)
import Data.Set as S (Set, deleteAt, elemAt, elems, empty, fromList, insert, member, notMember, singleton, size, null)
import Point (Point(..), PuzzleMap, cardinalDirections, neighbors, parseMap, turnLeft)

{-
... instead of using the perimeter to calculate the price, you need to use the number of sides each region
has. Each straight section of fence counts as a side, regardless of how long it is.

Consider this example again:

	AAAA
	BBCD
	BBCC
	EEEC

The region containing type A plants has 4 sides, as does each of the regions containing plants of type B, D,
and E. However, the more complex region containing the plants of type C has 8 sides!

Using the new method of calculating the per-region price by multiplying the region's area by its number of
sides, regions A through E have prices 16, 16, 32, 4, and 12, respectively, for a total price of 80.

The second example above (full of type X and O plants) would have a total price of 436.

Here's a map that includes an E-shaped region full of type E plants:

	EEEEE
	EXXXX
	EEEEE
	EXXXX
	EEEEE

The E-shaped region has an area of 17 and 12 sides for a price of 204. Including the two regions full of type
X plants, this map has a total price of 236.

This map has a total price of 368:

	AAAAAA
	AAABBA
	AAABBA
	ABBAAA
	ABBAAA
	AAAAAA

It includes two regions full of type B plants (each with 4 sides) and a single region full of type A plants
(with 4 sides on the outside and 8 more sides on the inside, a total of 12 sides). Be especially careful when
counting the fence around regions like the one full of type A plants; in particular, each section of fence has
an in-side and an out-side, so the fence does not connect across the middle of the region (where the two B
regions touch diagonally). (The Elves would have used the Möbius Fencing Company instead, but their contract
terms were too one-sided.)

The larger example from before now has the following updated prices:

	* A region of R plants with price 12 * 10 = 120.
	* A region of I plants with price 4 * 4 = 16.
	* A region of C plants with price 14 * 22 = 308.
	* A region of F plants with price 10 * 12 = 120.
	* A region of V plants with price 13 * 10 = 130.
	* A region of J plants with price 11 * 12 = 132.
	* A region of C plants with price 1 * 4 = 4.
	* A region of E plants with price 13 * 8 = 104.
	* A region of I plants with price 14 * 16 = 224.
	* A region of M plants with price 5 * 6 = 30.
	* A region of S plants with price 3 * 6 = 18.

Adding these together produces its new total price of 1206.

What is the new total price of fencing all regions on your map?

Test:
	% cd ../.. && echo -e 'AAAA\nBBCD\nBBCC\nEEEC' | cabal run 2024_day12-garden-groups_b # = 80
	% cd ../.. && echo -e 'EEEEE\nEXXXX\nEEEEE\nEXXXX\nEEEEE' | cabal run 2024_day12-garden-groups_b # = 236
	% cd ../.. && echo -e 'AAAAAA\nAAABBA\nAAABBA\nABBAAA\nABBAAA\nAAAAAA' | cabal run 2024_day12-garden-groups_b # = 368
	% cd ../.. && echo -e 'RRRRIICCFF\nRRRRIICCCF\nVVRRRCCFFF\nVVRCCCJFFF\nVVVVCJJCFE\nVVIVCCJJEE\nVVIIICJJEE\nMIIIIIJJEE\nMIIISIJEEE\nMMMISSJEEE' | cabal run 2024_day12-garden-groups_b # = 1206
-}

type Area = Int
type Sides = Int
type RegionId = (Area, Sides)
type Region = (RegionId, Set Point)

-- I ripped off 'dijkstra's algorithm and took out the cost and finished functions; basically "flood-fill" the
-- current plant and return the points visited.
mapRegion :: (Point -> [Point]) -- Function to generate list of neighboring states given the current state
          -> Point -- Initial state
          -> Set Point -- list of steps
mapRegion next initial = mapRegion' empty $ singleton initial
  where mapRegion' :: Set Point -> Set Point -> Set Point
        mapRegion' vst nss
          | S.null nss = vst
          | otherwise  = nextStep st nss'
          where st   = elemAt 0 nss
                nss' = deleteAt 0 nss
                --
                nextStep st nss
                  | st `member` vst = mapRegion' vst nss -- seen already
                  | otherwise       = mapRegion' (insert st vst) nss'
                  where nss' = foldl' (flip insert) nss (next st)

{-
E.g. if the top C is at (0,0); then
  +-+
  |C|
  + +-+
  |C C|
  +-+ +
    |C|
    +-+

edges ('es') would have [
  (Point 0 0, Point (-1) 0)
 ,(Point 0 0, Point 0 (-1))
 ,(Point 0 0, Point 0 1)
]

I.e. the point with up, left, and right as directions *not* in the C region.  Taking each in turn, "Can We Turn Left And Move?":
  (1) pointing up, can we go left?    No = vertex
  (2) pointing left, can we go down?  Yes = not a vertex (note: we can go down and that cell has a left boundary too)
  (3) pointing right, can we go up?   No = vertex

Giving 2 vertices for the top C cell.

For the remaining edges = [
  (Point 1 0, Point 0 (-1)) -- No = vertex
 ,(Point 1 0, Point 1 0)    -- No = vertex! From pointing down we can turn and move left; but this
       next cell doesn't have a "down" perimeter too.  This is a corner and counts for a vertex!
], [
  (Point 1 1, Point (-1) 0) -- No = vertex, this is another corner (see above)
 ,(Point 1 1, Point 0 1)    -- No = vertex
], [
  (Point 2 1, Point 0 (-1)) -- No = vertex
 ,(Point 2 1, Point 0 1)    -- Yes = not a vertex
 ,(Point 2 1, Point 1 0)    -- No = vertex; for 2 vertices
]
For a total of 8 vertices
-}
sides :: Set (Point, Point) -> Int
sides es = length [(ix, dir) | (ix, dir) <- elems es, (ix + turnLeft dir, dir) `notMember` es]

buildRegion :: PuzzleMap -> [Region] -> (Point, Char) -> [Region]
buildRegion pm regs (ix, plant)
  | any ((ix `member`) . snd) regs = regs
  | otherwise = ((area, sides es), reg):regs
  where reg = mapRegion getNeighbors ix
        --
        getNeighbors :: Point -> [Point]
        getNeighbors ix = [
          n | n <- neighbors cardinalDirections ix
            , Just plant == pm !? n
          ]
        --
        area = size reg
        es   = fromList [ {-edges-}
          (p, n - p) -- (plant, direction to outside the region)
            | p <- elems reg
            , n <- neighbors cardinalDirections p
            , n `notMember` reg
          ]

solve :: PuzzleMap -> Int
solve pm = sum . map score . foldl' (buildRegion pm) [] . assocs $ pm
  where score = uncurry (*) . fst


main :: IO ()
main = interact $ show . solve . parseMap

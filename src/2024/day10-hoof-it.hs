module Main where
import Data.Array.Unboxed ((!?), assocs)
import Data.List (nub)
import Point (Point(..), PuzzleMap, cardinalDirections, neighbors, parseMap)

--	... The topographic map indicates the height at each position using a scale from 0 (lowest) to 9 (highest). For example:
--
--	0123
--	1234
--	8765
--	9876
--
--	Based on un-scorched scraps of the book, you determine that a good hiking trail is as long as possible
--	and has an even, gradual, uphill slope. For all practical purposes, this means that a hiking trail is
--	any path that starts at height 0, ends at height 9, and always increases by a height of exactly 1 at
--	each step. Hiking trails never include diagonal steps - only up, down, left, or right (from the
--	perspective of the map).
--
--	You look up from the map and notice that the reindeer has helpfully begun to construct a small pile of
--	pencils, markers, rulers, compasses, stickers, and other equipment you might need to update the map
--	with hiking trails.
--
--	A trailhead is any position that starts one or more hiking trails - here, these positions will always
--	have height 0. Assembling more fragments of pages, you establish that a trailhead's score is the
--	number of 9-height positions reachable from that trailhead via a hiking trail. In the above example,
--	the single trailhead in the top left corner has a score of 1 because it can reach a single 9 (the one
--	in the bottom left).
--
--	This trailhead has a score of 2:
--
--	...0...
--	...1...
--	...2...
--	6543456
--	7.....7
--	8.....8
--	9.....9
--
--	... Here's a larger example:
--
--	89010123
--	78121874
--	87430965
--	96549874
--	45678903
--	32019012
--	01329801
--	10456732
--
--	This larger example has 9 trailheads. Considering the trailheads in reading order, they have scores of
--	5, 6, 5, 3, 1, 3, 5, 3, and 5. Adding these scores together, the sum of the scores of all trailheads
--	is 36.
--
--	The reindeer gleefully carries over a protractor and adds it to the pile. What is the sum of the
--	scores of all trailheads on your topographic map?
--
-- Test:
--	% cd ../.. && echo -e '...0...\n...1...\n...2...\n6543456\n7.....7\n8.....8\n9.....9' | cabal run 2024_day10-hoof-it # = 2
--	% cd ../.. && echo -e '..90..9\n...1.98\n...2..7\n6543456\n765.987\n876....\n987....' | cabal run 2024_day10-hoof-it # = 4
--	% cd ../.. && echo -e '10..9..\n2...8..\n3...7..\n4567654\n...8..3\n...9..2\n.....01' | cabal run 2024_day10-hoof-it # = 3
--	% cd ../.. && echo -e '89010123\n78121874\n87430965\n96549874\n45678903\n32019012\n01329801\n10456732' | cabal run 2024_day10-hoof-it # = 36

validTrail :: String
validTrail = "0123456789"

findPaths :: PuzzleMap -> String -> [Point] -> Int
findPaths _  ""        hikers = length . nub $ hikers
findPaths pm (t:trail) hikers = findPaths pm trail
             [ n
             | h <- hikers
             , n <- neighbors cardinalDirections h
             , Just t == pm !? n
             ]

solve :: PuzzleMap -> Int
solve pm = sum [findPaths pm (tail validTrail) [ix] | (ix, ch) <- assocs pm, ch == head validTrail]


main :: IO ()
main = interact $ show . solve . parseMap

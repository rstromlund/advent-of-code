{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Bifunctor (second)
import Data.List (sort)

{-
... trying to produce a number through some boolean logic gates. Each gate has two inputs and one output. The
gates all operate on values that are either true (1) or false (0).

	* AND gates output 1 if both inputs are 1; if either input is 0, these gates output 0.
	* OR gates output 1 if one or both inputs is 1; if both inputs are 0, these gates output 0.
	* XOR gates output 1 if the inputs are different; if the inputs are the same, these gates output 0.

Gates wait until both inputs are received before producing output; wires can carry 0, 1 or no value at
all. There are no loops; once a gate has determined its output, the output will not change until the whole
system is reset. Each wire is connected to at most one gate output, but can be connected to many gate inputs.

Rather than risk getting shocked while tinkering with the live system, you write down all of the gate
connections and initial wire values (your puzzle input) so you can consider them in relative safety. For
example:

	x00: 1
	x01: 1
	x02: 1
	y00: 0
	y01: 1
	y02: 0

	x00 AND y00 -> z00
	x01 XOR y01 -> z01
	x02 OR y02 -> z02

Because gates wait for input, some wires need to start with a value (as inputs to the entire system). The
first section specifies these values. For example, x00: 1 means that the wire named x00 starts with the value
1 (as if a gate is already outputting that value onto that wire).

The second section lists all of the gates and the wires connected to them. For example, x00 AND y00 -> z00
describes an instance of an AND gate which has wires x00 and y00 connected to its inputs and which will write
its output to wire z00.

In this example, simulating these gates eventually causes 0 to appear on wire z00, 0 to appear on wire z01,
and 1 to appear on wire z02.

Ultimately, the system is trying to produce a number by combining the bits on all wires starting with z. z00
is the least significant bit, then z01, then z02, and so on.

In this example, the three output bits form the binary number 100 which is equal to the decimal number 4.

Here's a larger example:

	x00: 1
	x01: 0
	x02: 1
	x03: 1
	x04: 0
	y00: 1
	y01: 1
	y02: 1
	y03: 1
	y04: 1

	ntg XOR fgs -> mjb
	y02 OR x01 -> tnw
	kwq OR kpj -> z05
	x00 OR x03 -> fst
	tgd XOR rvg -> z01
	vdt OR tnw -> bfw
	bfw AND frj -> z10
	ffh OR nrd -> bqk
	y00 AND y03 -> djm
	y03 OR y00 -> psh
	bqk OR frj -> z08
	tnw OR fst -> frj
	gnj AND tgd -> z11
	bfw XOR mjb -> z00
	x03 OR x00 -> vdt
	gnj AND wpb -> z02
	x04 AND y00 -> kjc
	djm OR pbm -> qhw
	nrd AND vdt -> hwm
	kjc AND fst -> rvg
	y04 OR y02 -> fgs
	y01 AND x02 -> pbm
	ntg OR kjc -> kwq
	psh XOR fgs -> tgd
	qhw XOR tgd -> z09
	pbm OR djm -> kpj
	x03 XOR y03 -> ffh
	x00 XOR y04 -> ntg
	bfw OR bqk -> z06
	nrd XOR fgs -> wpb
	frj XOR qhw -> z04
	bqk OR frj -> z07
	y03 OR x01 -> nrd
	hwm AND bqk -> z03
	tgd XOR rvg -> z12
	tnw OR pbm -> gnj

After waiting for values on all wires starting with z, the wires in this system have the following values:

	bfw: 1
	bqk: 1
	djm: 1
	ffh: 0
	fgs: 1
	frj: 1
	fst: 1
	gnj: 1
	hwm: 1
	kjc: 0
	kpj: 1
	kwq: 0
	mjb: 1
	nrd: 1
	ntg: 0
	pbm: 1
	psh: 1
	qhw: 1
	rvg: 0
	tgd: 0
	tnw: 1
	vdt: 1
	wpb: 0
	z00: 0
	z01: 0
	z02: 0
	z03: 1
	z04: 0
	z05: 1
	z06: 1
	z07: 1
	z08: 1
	z09: 1
	z10: 1
	z11: 0
	z12: 0

Combining the bits from all wires starting with z produces the binary number 0011111101000. Converting this
number to decimal produces 2024.

Simulate the system of gates and wires. What decimal number does it output on the wires starting with z?

Test:
	% cd ../.. && echo -e 'x00: 1\nx01: 1\nx02: 1\ny00: 0\ny01: 1\ny02: 0\n\nx00 AND y00 -> z00\nx01 XOR y01 -> z01\nx02 OR y02 -> z02' | cabal run 2024_day24-crossed-wires # = 4
	% cd ../.. && echo -e 'x00: 1\nx01: 0\nx02: 1\nx03: 1\nx04: 0\ny00: 1\ny01: 1\ny02: 1\ny03: 1\ny04: 1\n\nntg XOR fgs -> mjb\ny02 OR x01 -> tnw\nkwq OR kpj -> z05\nx00 OR x03 -> fst\ntgd XOR rvg -> z01\nvdt OR tnw -> bfw\nbfw AND frj -> z10\nffh OR nrd -> bqk\ny00 AND y03 -> djm\ny03 OR y00 -> psh\nbqk OR frj -> z08\ntnw OR fst -> frj\ngnj AND tgd -> z11\nbfw XOR mjb -> z00\nx03 OR x00 -> vdt\ngnj AND wpb -> z02\nx04 AND y00 -> kjc\ndjm OR pbm -> qhw\nnrd AND vdt -> hwm\nkjc AND fst -> rvg\ny04 OR y02 -> fgs\ny01 AND x02 -> pbm\nntg OR kjc -> kwq\npsh XOR fgs -> tgd\nqhw XOR tgd -> z09\npbm OR djm -> kpj\nx03 XOR y03 -> ffh\nx00 XOR y04 -> ntg\nbfw OR bqk -> z06\nnrd XOR fgs -> wpb\nfrj XOR qhw -> z04\nbqk OR frj -> z07\ny03 OR x01 -> nrd\nhwm AND bqk -> z03\ntgd XOR rvg -> z12\ntnw OR pbm -> gnj' | cabal run 2024_day24-crossed-wires # = 2024
-}

type Wire = String
type Value = Bool
type Line = (Wire, Value)

data Input = Lit Value | Var Wire   deriving (Eq, Ord, Show)
data Op    = AND | XOR | OR         deriving (Eq, Ord, Show)
data Gate  = GOp  Wire Op Input Input
           | GLit Wire Value        deriving (Eq, Ord, Show)

parse :: String -> ([Line], [Gate])
parse = second mkGate . mkLine [] . lines
  where mkLine acc ((a:b:c:':':' ':[f]):ls) = mkLine (([a,b,c], '1' == f):acc) ls
        mkLine acc ("":ls) = (reverse acc, ls)
        mkGate ((a:b:c:' ':'A':'N':'D':' ':d:e:f:' ':'-':'>':' ':g):gs) = GOp g AND (Var [a,b,c]) (Var [d,e,f]):mkGate gs
        mkGate ((a:b:c:' ':'X':'O':'R':' ':d:e:f:' ':'-':'>':' ':g):gs) = GOp g XOR (Var [a,b,c]) (Var [d,e,f]):mkGate gs
        mkGate ((a:b:c:' ':'O':'R':' ':d:e:f:' ':'-':'>':' ':g):gs)     = GOp g OR  (Var [a,b,c]) (Var [d,e,f]):mkGate gs
        mkGate [] = []

compute :: [Line] -> [Gate] -> [Gate]
compute (ln:lns) = compute lns . next . run ln
  where run :: Line -> [Gate] -> [Gate]
        run (w, v) = run' []
          where run' gs (GOp out op (Var w') op2:gs') | w == w' = run' (GOp out op (Lit v) op2:gs) gs'
                run' gs (GOp out op op1 (Var w'):gs') | w == w' = run' (GOp out op op1 (Lit v):gs) gs'
                run' gs (g:gs') = run' (g:gs) gs' -- wire not involved
                run' gs []      = gs
        --
        next = next' []
          where next' gs (GOp out op (Lit v0) (Lit v1):gs') = g:next' [] hs
                  where v' = exec op v0 v1
                        g  = GLit out v'
                        hs = run (out, v') (gs ++ gs')
                next' gs (g:gs') = next' (g:gs) gs'
                next' gs []      = gs
        --
        exec AND v0 v1 = v0 && v1
        exec XOR v0 v1 = v0 /= v1
        exec OR  v0 v1 = v0 || v1
compute [] = id

solve :: ([Line], [Gate]) -> Int
solve = foldr score 0 . sort . uncurry compute
  where score (GLit ('z':_) v) acc = acc * 2 + (if v then 1 else 0)
        score _ acc = acc


main :: IO ()
main = interact $ show . solve . parse

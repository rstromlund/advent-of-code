{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Data.List (intersect, nub, sort, sortOn, subsequences)
import Data.Maybe (fromMaybe)
import Data.Map.Strict ((!?), Map, empty, insertWith)
import Data.Ord (Down(..))

{-
... the LAN party will be the largest set of computers that are all connected to each other. That is, for each
computer at the LAN party, that computer will have a connection to every other computer at the LAN party.

In the above example, the largest set of computers that are all connected to each other is made up of co, de,
ka, and ta. Each computer in this set has a connection to every other computer in the set:

	ka-co
	ta-co
	de-co
	ta-ka
	de-ta
	ka-de

The LAN party posters say that the password to get into the LAN party is the name of every computer at the LAN
party, sorted alphabetically, then joined together with commas. (The people running the LAN party are clearly
a bunch of nerds.) In this example, the password would be co,de,ka,ta.

What is the password to get into the LAN party?

Test:
	% cd ../.. && echo -e 'kh-tc\nqp-kh\nde-cg\nka-co\nyn-aq\nqp-ub\ncg-tb\nvc-aq\ntb-ka\nwh-tc\nyn-cg\nkh-ub\nta-co\nde-co\ntc-td\ntb-wq\nwh-td\nta-ka\ntd-qp\naq-cg\nwq-ub\nub-vc\nde-ta\nwq-aq\nwq-vc\nwh-yn\nka-de\nkh-ta\nco-tc\nwh-qp\ntb-vc\ntd-yn' | cabal run 2024_day23-lan-party_b # = co,de,ka,ta
-}

type LAN = Map String [String]

parse :: String -> [(String, String)]
parse = map parseNodes . lines
  where parseNodes :: String -> (String, String)
        parseNodes (a0:a1:'-':bs) = ([a0,a1], bs)

mkLAN :: [(String, String)] -> LAN
mkLAN = foldl' (\acc (n0, n1) -> insertWith (++) n1 [n0] (insertWith (++) n0 [n1] acc)) empty

findConnected :: Map String [String] -> String -> [String]
findConnected !lan !n0 = head . sortOn (Down . length) $ ds
  where Just cs = lan !? n0
        cs' = n0:cs
        ds = [ cmn
             | sq@(_:_) <- subsequences cs
             , let tgt = n0:sq
             , let ns = [n1:fromMaybe [] (lan !? n1) | n1 <- sq]
             , let cmn = foldl' intersect cs' ns
             , length cmn == length (cmn `intersect` tgt)
             ]

solve :: [(String, String)] -> [String]
solve ns = sort . head . sortOn (Down . length) $ cs
  where lan = mkLAN ns
        cs = map (findConnected lan) . nub . map fst $ ns


main :: IO ()
main = interact $ show . solve . parse

module Main where
import Data.Array.Unboxed (assocs, bounds, inRange)
import Data.List (nub, sortOn, tails)
import Data.List.Extra (groupOn)
import Point (Point(..), PuzzleMap, parseMap)

--	Watching over your shoulder as you work, one of The Historians asks if you took the effects of
--	resonant harmonics into your calculations.
--
--	Whoops!
--
--	After updating your model, it turns out that an antinode occurs at any grid position exactly in line
--	with at least two antennas of the same frequency, regardless of distance. This means that some of the
--	new antinodes will occur at the position of each antenna (unless that antenna is the only one of its
--	frequency).
--
--	So, these three T-frequency antennas now create many antinodes:
--
--	T....#....
--	...T......
--	.T....#...
--	.........#
--	..#.......
--	..........
--	...#......
--	..........
--	....#.....
--	..........
--
--	In fact, the three T-frequency antennas are all exactly in line with two antennas, so they are all
--	also antinodes! This brings the total number of antinodes in the above example to 9.
--
--	The original example now has 34 antinodes, including the antinodes that appear on every antenna:
--
--	##....#....#
--	.#.#....0...
--	..#.#0....#.
--	..##...0....
--	....0....#..
--	.#...#A....#
--	...#..#.....
--	#....#.#....
--	..#.....A...
--	....#....A..
--	.#........#.
--	...#......##
--
--	Calculate the impact of the signal using this updated model. How many unique locations within the
--	bounds of the map contain an antinode?
--
-- Test:
--	% cd ../.. && echo -e 'T.........\n...T......\n.T........\n..........\n..........\n..........\n..........\n..........\n..........\n..........' | cabal run 2024_day08-resonant-collinearity_b # = 9
--	% cd ../.. && echo -e '............\n........0...\n.....0......\n.......0....\n....0.......\n......A.....\n............\n............\n........A...\n.........A..\n............\n............' | cabal run 2024_day08-resonant-collinearity_b # = 34

solve :: PuzzleMap -> Int
solve ps = length rs
  where b = bounds ps
        --
        -- Group antenna types together:
        gs :: [[(Point, Char)]]
        gs = groupOn snd . sortOn snd $ [(i, c) | (i, c) <- assocs ps, '.' /= c]
        --
        -- Every antenna paired with all the others of its type w/o dupes like (a,b) and (b,a):
        as :: [((Point, Char), (Point, Char))]
        as = [ (a0, a1)
             | ag <- gs
             , (a0:ag'@(_:_)) <- tails ag
             , a1 <- ag'
             ]
        --
        -- Every antinode from every antenna pair:
        rs = nub
          [ i
          | ((i0, _), (i1, _)) <- as
          , let dir = i0 - i1
          , i <- (takeWhile (inRange b) . iterate (+ dir) $ i0)
              ++ (takeWhile (inRange b) . iterate (subtract dir) $ i0)
          ]


main :: IO ()
main = interact $ show . solve . parseMap

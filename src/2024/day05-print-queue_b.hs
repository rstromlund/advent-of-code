{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.List (inits, sortBy)
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (char, newline)
import Text.Megaparsec.Char.Lexer (decimal)

--	... For each of the incorrectly-ordered updates, use the page ordering rules to put the page numbers
--	in the right order. For the above example, here are the three incorrectly-ordered updates and their
--	correct orderings:
--
--	* 75,97,47,61,53 becomes 97,75,47,61,53.
--	* 61,13,29 becomes 61,29,13.
--	* 97,13,75,29,47 becomes 97,75,47,29,13.
--
--	After taking only the incorrectly-ordered updates and ordering them correctly, their middle page
--	numbers are 47, 29, and 47. Adding these together produces 123.
--
--	Find the updates which are not in the correct order. What do you get if you add up the middle page
--	numbers after correctly ordering just those updates?
--
-- Test:
--	% cd ../.. && echo -e '47|53\n97|13\n97|61\n97|47\n75|29\n61|13\n75|53\n29|13\n97|29\n53|29\n61|53\n97|53\n61|29\n47|13\n75|47\n97|75\n47|61\n75|61\n47|29\n75|13\n53|13\n\n75,47,61,53,29\n97,61,53,29,13\n75,29,13\n75,97,47,61,53\n61,13,29\n97,13,75,29,47' | cabal run 2024_day05-print-queue_b # = 123

type Rule = (Int, Int)
type Update = [Int]

type Parser = Parsec Void String

rules :: Parser ([Rule], [Update])
rules = (,) <$> many rule <*> (newline *> many pages) <* eof

rule :: Parser Rule
rule = (,) <$> decimal <* char '|' <*> decimal <* newline

pages :: Parser Update
pages = decimal `sepBy` char ',' <* newline

validOrder :: [Rule] -> Update -> Bool
validOrder rs ps = null
  [ ()
  | ps'@(_:_:_) <- inits ps
  -- ^^ skip empty or singleton inits
  , let r = last ps'
  -- each page must come to the right of the preceeding pages
  , any (\l -> (r, l) `elem` rs) . init $ ps'
  -- ^^ is there any rule that puts our "right" page before the left ones? This is a rule violation!
  ]

solve :: Either a ([Rule], [Update]) -> Int
solve (Right (rs, us)) = sum
  [ gs !! mid
  | bs <- [ps | ps <- us, not $ validOrder rs ps] -- bad updates
  , let gs = sortBy (\l r -> if (l, r) `elem` rs then LT else GT) bs -- made good
  -- Sort page numbers by needing to be on the left side of another number
  , let mid = length gs `div` 2
  ]


main :: IO ()
main = interact $ show . solve . runParser rules "<stdin>"

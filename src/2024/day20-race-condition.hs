{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed as A ((!?), assocs)
import Data.Map.Strict as M ((!?), Map, fromList)
import Point (Point(..), PuzzleMap, cardinalDirections, neighbors, parseMap)

{-
... compete to see who can finish in the fewest picoseconds ... They hand you a map of the racetrack (your
puzzle input). For example:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#######.#.#.###
	#######.#.#...#
	#######.#.###.#
	###..E#...#...#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

The map consists of track (.) - including the start (S) and end (E) positions (both of which also count as
track) - and walls (#).

When a program runs through the racetrack, it starts at the start position. Then, it is allowed to move up,
down, left, or right; each such move takes 1 picosecond. The goal is to reach the end position as quickly as
possible. In this example racetrack, the fastest time is 84 picoseconds.

Because there is only a single path from the start to the end and the programs all go the same speed, the
races used to be pretty boring. To make things more interesting, they introduced a new rule to the races:
programs are allowed to cheat.

The rules for cheating are very strict. Exactly once during a race, a program may disable collision for up to
2 picoseconds. This allows the program to pass through walls as if they were regular track. At the end of the
cheat, the program must be back on normal track again; otherwise, it will receive a segmentation fault and get
disqualified.

So, a program could complete the course in 72 picoseconds (saving 12 picoseconds) by cheating for the two
moves marked 1 and 2:

	###############
	#...#...12....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#######.#.#.###
	#######.#.#...#
	#######.#.###.#
	###..E#...#...#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

Or, a program could complete the course in 64 picoseconds (saving 20 picoseconds) by cheating for the two
moves marked 1 and 2:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#######.#.#.###
	#######.#.#...#
	#######.#.###.#
	###..E#...12..#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

This cheat saves 38 picoseconds:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#######.#.#.###
	#######.#.#...#
	#######.#.###.#
	###..E#...#...#
	###.####1##.###
	#...###.2.#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

This cheat saves 64 picoseconds and takes the program directly to the end:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#######.#.#.###
	#######.#.#...#
	#######.#.###.#
	###..21...#...#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

Each cheat has a distinct start position (the position where the cheat is activated, just before the first
move that is allowed to go through walls) and end position; cheats are uniquely identified by their start
position and end position.

In this example, the total number of cheats (grouped by the amount of time they save) are as follows:

	* There are 14 cheats that save 2 picoseconds.
	* There are 14 cheats that save 4 picoseconds.
	* There are 2 cheats that save 6 picoseconds.
	* There are 4 cheats that save 8 picoseconds.
	* There are 2 cheats that save 10 picoseconds.
	* There are 3 cheats that save 12 picoseconds.
	* There is one cheat that saves 20 picoseconds.
	* There is one cheat that saves 36 picoseconds.
	* There is one cheat that saves 38 picoseconds.
	* There is one cheat that saves 40 picoseconds.
	* There is one cheat that saves 64 picoseconds.

You aren't sure what the conditions of the racetrack will be like, so to give yourself as many options as
possible, you'll need a list of the best cheats. How many cheats would save you at least 100 picoseconds?

Test:
	% cd ../.. && echo -e '###############\n#...#...#.....#\n#.#.#.#.#.###.#\n#S#...#.#.#...#\n#######.#.#.###\n#######.#.#...#\n#######.#.###.#\n###..E#...#...#\n###.#######.###\n#...###...#...#\n#.#####.#.###.#\n#.#...#.#.#...#\n#.#.#.#.#.#.###\n#...#...#...###\n###############' | cabal run 2024_day20-race-condition # = 2
-}

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where pm    = parseMap cs
        start = head [i | (i, 'S') <- assocs pm]
        end   = head [i | (i, 'E') <- assocs pm]

savesSeconds :: Int
savesSeconds = 100 -- 40 for test; 100 for prod

cheat :: PuzzleMap -> [Point] -> Map Point Int -> Int
cheat pm ts soln = length
  [
    sav |
      (c0, t) <- zip [0..] ts,
      d <- cardinalDirections,
      let w  = t + d, -- wall
      let tc = w + d, -- track(sub cheat)
      Just '#' == pm A.!? w,
      Just ch  <- [pm A.!? tc],
      ch /= '#',
      let Just c1 = soln M.!? tc,
      let sav = c1 - c0 - 2,
      sav >= savesSeconds
  ]

solve :: (PuzzleMap, Point, Point) -> Int
solve (pm, start, end) = cheat pm (start:ts) (fromList . zip (start:ts) $ [0..])
  where Just (_, ts) = dijkstra getNeighbors (const . const (1 :: Int)) (end ==) start
        getNeighbors :: Point -> [Point]
        getNeighbors pos = [i | i <- neighbors cardinalDirections pos, Just '#' /= pm A.!? i]


main :: IO ()
main = interact $ show . solve . parse

{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed as A ((!?), assocs)
import Data.List (nub)
import Data.Map.Strict as M ((!?), Map, fromList)
import Point (Point(..), PuzzleMap, cardinalDirections, manhattan, neighbors, parseMap)

{-
... latest version of the cheating rule permits a single cheat that instead lasts at most 20 picoseconds.

Now, in addition to all the cheats that were possible in just two picoseconds, many more cheats are
possible. This six-picosecond cheat saves 76 picoseconds:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S#...#.#.#...#
	#1#####.#.#.###
	#2#####.#.#...#
	#3#####.#.###.#
	#456.E#...#...#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

Because this cheat has the same start and end positions as the one above, it's the same cheat, even though the
path taken during the cheat is different:

	###############
	#...#...#.....#
	#.#.#.#.#.###.#
	#S12..#.#.#...#
	###3###.#.#.###
	###4###.#.#...#
	###5###.#.###.#
	###6.E#...#...#
	###.#######.###
	#...###...#...#
	#.#####.#.###.#
	#.#...#.#.#...#
	#.#.#.#.#.#.###
	#...#...#...###
	###############

Cheats don't need to use all 20 picoseconds; cheats can last any amount of time up to and including 20
picoseconds (but can still only end when the program is on normal track). Any cheat time not used is lost; it
can't be saved for another cheat later.

You'll still need a list of the best cheats, but now there are even more to choose between. Here are the
quantities of cheats in this example that save 50 picoseconds or more:

	* There are 32 cheats that save 50 picoseconds.
	* There are 31 cheats that save 52 picoseconds.
	* There are 29 cheats that save 54 picoseconds.
	* There are 39 cheats that save 56 picoseconds.
	* There are 25 cheats that save 58 picoseconds.
	* There are 23 cheats that save 60 picoseconds.
	* There are 20 cheats that save 62 picoseconds.
	* There are 19 cheats that save 64 picoseconds.
	* There are 12 cheats that save 66 picoseconds.
	* There are 14 cheats that save 68 picoseconds.
	* There are 12 cheats that save 70 picoseconds.
	* There are 22 cheats that save 72 picoseconds.
	* There are 4 cheats that save 74 picoseconds.
	* There are 3 cheats that save 76 picoseconds.

Find the best cheats using the updated cheating rules. How many cheats would save you at least 100
picoseconds?

Test:
	% cd ../.. && echo -e '###############\n#...#...#.....#\n#.#.#.#.#.###.#\n#S#...#.#.#...#\n#######.#.#.###\n#######.#.#...#\n#######.#.###.#\n###..E#...#...#\n###.#######.###\n#...###...#...#\n#.#####.#.###.#\n#.#...#.#.#...#\n#.#.#.#.#.#.###\n#...#...#...###\n###############' | cabal run 2024_day20-race-condition_b # = 285
-}

parse :: String -> (PuzzleMap, Point, Point)
parse cs = (pm, start, end)
  where pm    = parseMap cs
        start = head [i | (i, 'S') <- assocs pm]
        end   = head [i | (i, 'E') <- assocs pm]

savesSeconds :: Int
savesSeconds = 100 -- 50 for test; 100 for prod
zoneSize :: Int
zoneSize = 20

-- | Find all possible track "landing" positions from a cheat in a 20 pico radius
cheatZone :: PuzzleMap -> Point -> [(Point, Point)]
cheatZone pm pt = nub
  [ (pt, i)
  | y  <- [0..zoneSize]
  , x  <- [0..zoneSize - y]
  , not (0 == y && 0 == x)
  , y' <- [y, -y]
  , x' <- [x, -x]
  , let i = pt + Point y' x'
  , Just ch <- [pm A.!? i]
  , ch /= '#'
  ]

-- | Calculate the cost of all the possible cheats
cheat :: PuzzleMap -> [Point] -> Map Point Int -> Int
cheat pm ts soln = length
  [ sav -- (sav, (t, tc))
    | (c0, t) <- zip [0..] ts
    , (_, tc) <- cheatZone pm t -- track(sub cheat)
    , let Just c1 = soln M.!? tc
    , let sav = c1 - c0 - manhattan t tc
    , sav >= savesSeconds
  ]

solve :: (PuzzleMap, Point, Point) -> Int
solve (pm, start, end) = cheat pm (start:ts) (fromList . zip (start:ts) $ [0..])
  where -- | Use Dijkstra as an easy way to find our 1 path thru the race.
        -- It might be overkill (since there is 1 solution) but it's easier than [re]coding a maze search here.
        Just (_, ts) = dijkstra getNeighbors (const . const (1 :: Int)) (end ==) start
        getNeighbors :: Point -> [Point]
        getNeighbors pos = [i | i <- neighbors cardinalDirections pos, Just '#' /= pm A.!? i]


main :: IO ()
main = interact $ show . solve . parse

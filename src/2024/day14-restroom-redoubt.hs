{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)
import Point (Point(..))

{-
... To get The Historian safely to the bathroom, you'll need a way to predict where the robots will be in the
future. Fortunately, they all seem to be moving on the tile floor in predictable straight lines.

You make a list (your puzzle input) of all of the robots' current positions (p) and velocities (v), one robot
per line. For example:

	p=0,4 v=3,-3
	p=6,3 v=-1,-3
	p=10,3 v=-1,2
	p=2,0 v=2,-1
	p=0,0 v=1,3
	p=3,0 v=-2,-2
	p=7,6 v=-1,-3
	p=3,0 v=-1,-2
	p=9,3 v=2,3
	p=7,3 v=-1,2
	p=2,4 v=2,-3
	p=9,5 v=-3,-3

Each robot's position is given as p=x,y where x represents the number of tiles the robot is from the left wall
and y represents the number of tiles from the top wall (when viewed from above). So, a position of p=0,0 means
the robot is all the way in the top-left corner.

Each robot's velocity is given as v=x,y where x and y are given in tiles per second. Positive x means the
robot is moving to the right, and positive y means the robot is moving down. So, a velocity of v=1,-2 means
that each second, the robot moves 1 tile to the right and 2 tiles up.

The robots outside the actual bathroom are in a space which is 101 tiles wide and 103 tiles tall (when viewed
from above). However, in this example, the robots are in a space which is only 11 tiles wide and 7 tiles tall.

... These robots have a unique feature for maximum bathroom security: they can teleport. When a robot would
run into an edge of the space they're in, they instead teleport to the other side, effectively wrapping around
the edges. Here is what robot p=2,4 v=2,-3 does for the first few seconds ...

	After 5 seconds:
	...........
	...........
	...........
	.1.........
	...........
	...........
	...........

... the number of robots on each tile after 100 seconds has elapsed looks like this:

	......2..1.
	...........
	1..........
	.11........
	.....1.....
	...12......
	.1....1....

To determine the safest area, count the number of robots in each quadrant after 100 seconds. Robots that are
exactly in the middle (horizontally or vertically) don't count as being in any quadrant, so the only relevant
robots are:

	..... 2..1.
	..... .....
	1.... .....

	..... .....
	...12 .....
	.1... 1....

In this example, the quadrants contain 1, 3, 4, and 1 robot. Multiplying these together gives a total safety
factor of 12.

Predict the motion of the robots in your list within a space which is 101 tiles wide and 103 tiles tall. What
will the safety factor be after exactly 100 seconds have elapsed?

Test:
	% cd ../.. && echo -e 'p=0,4 v=3,-3\np=6,3 v=-1,-3\np=10,3 v=-1,2\np=2,0 v=2,-1\np=0,0 v=1,3\np=3,0 v=-2,-2\np=7,6 v=-1,-3\np=3,0 v=-1,-2\np=9,3 v=2,3\np=7,3 v=-1,2\np=2,4 v=2,-3\np=9,5 v=-3,-3' | cabal run 2024_day14-restroom-redoubt # = 12
-}

data SecGuard = SecGuard Point Point deriving (Show) -- Location and Velocity

type Parser = Parsec Void String

secGuards :: Parser [SecGuard]
secGuards = many secGuard <* eof

nbr :: Parser Int
nbr = f <$> optional (char '-') <*> decimal
  where f Nothing x = x
        f _       x = negate x

secGuard :: Parser SecGuard
secGuard = mkSecGuard <$>
             (string  "p=" *> (nbr `sepBy` char ',')) <*>
             (string " v=" *> (nbr `sepBy` char ',')) <* newline
  where mkSecGuard [x,y] [dx,dy] = SecGuard (Point y x) (Point dy dx)

prodPuzzleSize :: Point
prodPuzzleSize = Point 103 101
testPuzzleSize :: Point
testPuzzleSize = Point   7  11
puzzleSize :: Point
puzzleSize = prodPuzzleSize -- testPuzzleSize

simulateTime :: Point
simulateTime = 100

solve :: Either a [SecGuard] -> Int
solve (Right is) = product . quadGroup . map simulate $ is
  where (Point sizeY sizeX) = puzzleSize
        quad2Y = 1 + sizeY `div` 2
        quad2X = 1 + sizeX `div` 2
        quads = [ (Point      0      0, Point (quad2Y - 2) (quad2X - 2))
                , (Point      0 quad2X, Point (quad2Y - 2)  (sizeX - 1))
                , (Point quad2Y      0, Point  (sizeY - 1) (quad2X - 2))
                , (Point quad2Y quad2X, Point  (sizeY - 1)  (sizeX - 1))
                ]
        simulate (SecGuard pos vel) = warp $ pos + vel * simulateTime
        warp (Point y x) = Point (y `mod` sizeY) (x `mod` sizeX)
        quadGroup gs = map (length . inQuad) quads
          where inQuad (Point loy lox, Point hiy hix) = filter (\(Point gy gx) ->
                  gy >= loy && gy <= hiy &&
                  gx >= lox && gx <= hix) gs


main :: IO ()
main = interact $ show . solve . runParser secGuards "<stdin>"

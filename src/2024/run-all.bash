#!/bin/bash

cd ../../ && cabal exec hlint $(git ls-files --others --modified --exclude-standard -- "***.hs")
typeset rc=${?}

### Run all puzzle solutions:

test -r "advent-of-code.cabal" || exit 99

#echo -e "\n\n==== day01"
#time cabal run 2024_day01-historian-hysteria   < "src/2024/01a.txt" ; (( rc += 0 ))
#time cabal run 2024_day01-historian-hysteria_b < "src/2024/01a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day02"
#time cabal run 2024_day02-red-nosed-reports   < "src/2024/02a.txt" ; (( rc += 0 ))
#time cabal run 2024_day02-red-nosed-reports_b < "src/2024/02a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day03"
#time cabal run 2024_day03-mull-it-over   < "src/2024/03a.txt" ; (( rc += 0 ))
#time cabal run 2024_day03-mull-it-over_b < "src/2024/03a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day04"
#time cabal run 2024_day04-ceres-search   < "src/2024/04a.txt" ; (( rc += 0 ))
#time cabal run 2024_day04-ceres-search_b < "src/2024/04a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day05"
#time cabal run 2024_day05-print-queue   < "src/2024/05a.txt" ; (( rc += 0 ))
#time cabal run 2024_day05-print-queue_b < "src/2024/05a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day06"
#time cabal run 2024_day06-guard-gallivant   < "src/2024/06a.txt" ; (( rc += 0 ))
#time cabal run 2024_day06-guard-gallivant_b < "src/2024/06a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day07"
#time cabal run 2024_day07-bridge-repair   < "src/2024/07a.txt" ; (( rc += 0 ))
#time cabal run 2024_day07-bridge-repair_b < "src/2024/07a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day08"
#time cabal run 2024_day08-resonant-collinearity   < "src/2024/08a.txt" ; (( rc += 0 ))
#time cabal run 2024_day08-resonant-collinearity_b < "src/2024/08a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day09"
#time cabal run 2024_day09-disk-fragmenter   < "src/2024/09a.txt" ; (( rc += 0 ))
#time cabal run 2024_day09-disk-fragmenter_b < "src/2024/09a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day10"
#time cabal run 2024_day10-hoof-it   < "src/2024/10a.txt" ; (( rc += 0 ))
#time cabal run 2024_day10-hoof-it_b < "src/2024/10a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day11"
#time cabal run 2024_day11-plutonian-pebbles   < "src/2024/11a.txt" ; (( rc += 0 ))
#time cabal run 2024_day11-plutonian-pebbles_b < "src/2024/11a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day12"
#time cabal run 2024_day12-garden-groups   < "src/2024/12a.txt" ; (( rc += 0 ))
#time cabal run 2024_day12-garden-groups_b < "src/2024/12a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day13"
#time cabal run 2024_day13-claw-contraption   < "src/2024/13a.txt" ; (( rc += 0 ))
#time cabal run 2024_day13-claw-contraption_b < "src/2024/13a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day14"
#time cabal run 2024_day14-restroom-redoubt   < "src/2024/14a.txt" ; (( rc += 0 ))
#time cabal run 2024_day14-restroom-redoubt_b < "src/2024/14a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day15"
#time cabal run 2024_day15-warehouse-woes   < "src/2024/15a.txt" ; (( rc += 0 ))
#time cabal run 2024_day15-warehouse-woes_b < "src/2024/15a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day16"
#time cabal run 2024_day16-reindeer-maze   < "src/2024/16a.txt" ; (( rc += 0 ))
#time cabal run 2024_day16-reindeer-maze_b < "src/2024/16a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day17"
#time cabal run 2024_day17-chronospatial-computer   < "src/2024/17a.txt" ; (( rc += 0 ))
#time cabal run 2024_day17-chronospatial-computer_b < "src/2024/17a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day18"
#time cabal run 2024_day18-ram-run   < "src/2024/18a.txt" ; (( rc += 0 ))
#time cabal run 2024_day18-ram-run_b < "src/2024/18a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day19"
#time cabal run 2024_day19-linen-layout   < "src/2024/19a.txt" ; (( rc += 0 ))
#time cabal run 2024_day19-linen-layout_b < "src/2024/19a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day20"
#time cabal run 2024_day20-race-condition   < "src/2024/20a.txt" ; (( rc += 0 ))
#time cabal run 2024_day20-race-condition_b < "src/2024/20a.txt" ; (( rc += 0 ))

echo -e "\n\n==== day21"
#time cabal run 2024_day21-keypad-conundrum   < "src/2024/21a.txt" ; (( rc += 0 ))
time cabal run 2024_day21-keypad-conundrum_b < "src/2024/21a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day22"
#time cabal run 2024_day22-monkey-market   < "src/2024/22a.txt" ; (( rc += 0 ))
#time cabal run 2024_day22-monkey-market_b < "src/2024/22a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day23"
#time cabal run 2024_day23-lan-party   < "src/2024/23a.txt" ; (( rc += 0 ))
#time cabal run 2024_day23-lan-party_b < "src/2024/23a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day24"
#time cabal run 2024_day24-crossed-wires   < "src/2024/24a.txt" ; (( rc += 0 ))
#time cabal run 2024_day24-crossed-wires_b < "src/2024/24a.txt" ; (( rc += 0 ))

#echo -e "\n\n==== day25"
#time cabal run 2024_day25-code-chronicle   < "src/2024/25a.txt" ; (( rc += 0 ))

exit ${rc}

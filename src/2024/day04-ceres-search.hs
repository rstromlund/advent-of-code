{-# LANGUAGE ParallelListComp #-}
module Main where
import Data.Array.Unboxed ((!?), assocs)
import Point (PuzzleMap, ordinalDirections, parseMap)

--	... The actual word search will be full of letters instead. For example:
--
--	MMMSXXMASM
--	MSAMXMSMSA
--	AMXSXMAAMM
--	MSAMASMSMX
--	XMASAMXAMM
--	XXAMMXXAMA
--	SMSMSASXSS
--	SAXAMASAAA
--	MAMMMXMMMM
--	MXMXAXMASX
--
--	In this word search, XMAS occurs a total of 18 times; here's the same word search again, but where
--	letters not involved in any XMAS have been replaced with .:
--
--	....XXMAS.
--	.SAMXMS...
--	...S..A...
--	..A.A.MS.X
--	XMASAMX.MM
--	X.....XA.A
--	S.S.S.S.SS
--	.A.A.A.A.A
--	..M.M.M.MM
--	.X.X.XMASX
--
--	Take a look at the little Elf's word search. How many times does XMAS appear?
--
-- Test:
--	% cd ../.. && echo -e 'MMMSXXMASM\nMSAMXMSMSA\nAMXSXMAAMM\nMSAMASMSMX\nXMASAMXAMM\nXXAMMXXAMA\nSMSMSASXSS\nSAXAMASAAA\nMAMMMXMMMM\nMXMXAXMASX' | cabal run 2024_day04-ceres-search # = 18

wordSearch :: String
wordSearch = "XMAS"

solve :: PuzzleMap -> Int
solve cw = length [
  ()
  | (ix, x) <- assocs cw
  , x == head wordSearch
  , dir <- ordinalDirections
  , and [Just ch == cw !? i | ch <- wordSearch | i <- iterate (dir +) ix]
  ]


main :: IO ()
main = interact $ show . solve . parseMap

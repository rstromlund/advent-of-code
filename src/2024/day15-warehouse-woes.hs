{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns #-}
module Main where
import Data.Array.Unboxed ((!?), (//), assocs)
import Data.List.Split (splitOn)
import Point (Point(..), PuzzleMap, north, east, south, west, parseMap)

{-
... These lanternfish seem so anxious because they have lost control of the robot that operates one of their
most important warehouses! It is currently running amok, pushing around boxes in the warehouse with no regard
for lanternfish logistics or lanternfish inventory management strategies.

... The lanternfish already have a map of the warehouse and a list of movements the robot will attempt to make
(your puzzle input). The problem is that the movements will sometimes fail as boxes are shifted around, making
the actual movements of the robot difficult to predict.

For example:

	##########
	#..O..O.O#
	#......O.#
	#.OO..O.O#
	#..O@..O.#
	#O#..O...#
	#O..O..O.#
	#.OO.O.OO#
	#....O...#
	##########

	<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^vvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v
	><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^
	^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^
	>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>
	^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>v^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^

As the robot (@) attempts to move, if there are any boxes (O) in the way, the robot will also attempt to push
those boxes. However, if this action would cause the robot or a box to move into a wall (#), nothing moves
instead, including the robot. The initial positions of these are shown on the map at the top of the document
the lanternfish gave you.

The rest of the document describes the moves (^ for up, v for down, < for left, > for right) that the robot
will attempt to make, in order. (The moves form a single giant sequence; they are broken into multiple lines
just to make copy-pasting easier. Newlines within the move sequence should be ignored.)

Here is a smaller example to get started:

	########
	#..O.O.#
	##@.O..#
	#...O..#
	#.#.O..#
	#...O..#
	#......#
	########

	<^^>>>vv<v>>v<<

Were the robot to attempt the given sequence of moves, it would push around the boxes as follows:
... last ...

	Move <:
	########
	#....OO#
	##.....#
	#.....O#
	#.#O@..#
	#...O..#
	#...O..#
	########

The larger example has many more moves; after the robot has finished those moves, the warehouse would look
like this:

	##########
	#.O.O.OOO#
	#........#
	#OO......#
	#OO@.....#
	#O#.....O#
	#O.....OO#
	#O.....OO#
	#OO....OO#
	##########

The lanternfish use their own custom Goods Positioning System (GPS for short) to track the locations of the
boxes. The GPS coordinate of a box is equal to 100 times its distance from the top edge of the map plus its
distance from the left edge of the map. (This process does not stop at wall tiles; measure all the way to the
edges of the map.)

So, the box shown below has a distance of 1 from the top edge of the map and 4 from the left edge of the map,
resulting in a GPS coordinate of 100 * 1 + 4 = 104.

	#######
	#...O..
	#......

The lanternfish would like to know the sum of all boxes' GPS coordinates after the robot finishes moving. In
the larger example, the sum of all boxes' GPS coordinates is 10092. In the smaller example, the sum is 2028.

Predict the motion of the robot and boxes in the warehouse. After the robot is finished moving, what is the
sum of all boxes' GPS coordinates?

Test:
	% # solved puzzles, test scoring first:
	% cd ../.. && echo -e '########\n#....OO#\n##.....#\n#.....O#\n#.#O@..#\n#...O..#\n#...O..#\n########\n\n<' | cabal run 2024_day15-warehouse-woes # = 2028
	% cd ../.. && echo -e '##########\n#.O.O.OOO#\n#........#\n#OO......#\n#OO@.....#\n#O#.....O#\n#O.....OO#\n#O.....OO#\n#OO....OO#\n##########\n\n<' | cabal run 2024_day15-warehouse-woes # = 10092

	% cd ../.. && echo -e '########\n#..O.O.#\n##@.O..#\n#...O..#\n#.#.O..#\n#...O..#\n#......#\n########\n\n<^^>>>vv<v>>v<<' | cabal run 2024_day15-warehouse-woes # = 2028
	% cd ../.. && echo -e '##########\n#..O..O.O#\n#......O.#\n#.OO..O.O#\n#..O@..O.#\n#O#..O...#\n#O..O..O.#\n#.OO.O.OO#\n#....O...#\n##########\n\n<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^\nvvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v\n><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<\n<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^\n^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><\n^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^\n>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^\n<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>\n^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>\nv^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^' | cabal run 2024_day15-warehouse-woes # = 10092
-}

parse :: String -> (PuzzleMap, String)
parse = (\[pm, ds] -> (parseMap pm, filter ('\n' /=) ds)) . splitOn "\n\n"

heading :: Char -> Point
heading '^' = north
heading 'v' = south
heading '<' = west
heading '>' = east

walk :: PuzzleMap -> Point -> [Char] -> PuzzleMap
walk pm _ [] = pm
walk pm pos (d:ds)
  | 'O' == ch = moveBox pm pos' ds dir -- need to push boxes to move
  | '#' == ch = walk pm pos  ds -- can't move
  | otherwise = walk pm pos' ds -- can move
  where dir  = heading d
        pos' = pos + dir
        Just ch = pm !? pos'

moveBox :: PuzzleMap -> Point -> [Char] -> Point -> PuzzleMap
moveBox pm pos ds dir = walk pm' pos' ds
  -- ^^ pos is where the robot has already stepped into the space of the first box
  -- we need to push the boxes out of the way or put the robot back
  where bs   = [
          -- Shift box in case we can really move them
          (i + dir, 'O')
            | i <- takeWhile ((Just 'O' ==) . (!?) pm) . iterate (dir +) $ pos
          ]
        -- last-character past the boxes, is it a wall or empty space?
        (Just lch) = pm !? (fst . last $ bs)
        --
        (pm', pos')
          | '#' == lch = (pm, pos - dir) -- can't push into the wall
          | otherwise  = (pm // ((pos, lch):bs), pos) -- move boxes

score :: PuzzleMap -> Int
score pm = sum [y * 100 + x | (Point y x, 'O') <- assocs pm]

solve :: (PuzzleMap, String) -> Int
solve (pm, ds) = score . walk pm start $ ds
  where start = head [i | (i, '@') <- assocs pm]


main :: IO ()
main = interact $ show . solve . parse

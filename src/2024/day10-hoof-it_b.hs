module Main where
import Data.Array.Unboxed ((!?), assocs)
import Point (Point(..), PuzzleMap, cardinalDirections, neighbors, parseMap)

--	... a second way to measure a trailhead called its rating. A trailhead's rating is the number of
--	distinct hiking trails which begin at that trailhead. For example:
--
--	.....0.
--	..4321.
--	..5..2.
--	..6543.
--	..7..4.
--	..8765.
--	..9....
--
--	The above map has a single trailhead; its rating is 3 because there are exactly three distinct hiking trails which begin at that position:
--
--	.....0.   .....0.   .....0.
--	..4321.   .....1.   .....1.
--	..5....   .....2.   .....2.
--	..6....   ..6543.   .....3.
--	..7....   ..7....   .....4.
--	..8....   ..8....   ..8765.
--	..9....   ..9....   ..9....
--
--	... Here's the larger example from before:
--
--	89010123
--	78121874
--	87430965
--	96549874
--	45678903
--	32019012
--	01329801
--	10456732
--
--	Considering its trailheads in reading order, they have ratings of 20, 24, 10, 4, 1, 4, 5, 8, and
--	5. The sum of all trailhead ratings in this larger example topographic map is 81.
--
--	You're not sure how, but the reindeer seems to have crafted some tiny flags out of toothpicks and bits
--	of paper and is using them to mark trailheads on your topographic map. What is the sum of the ratings
--	of all trailheads?
--
-- Test:
--	% cd ../.. && echo -e '.....0.\n..4321.\n..5..2.\n..6543.\n..7..4.\n..8765.\n..9....' | cabal run 2024_day10-hoof-it_b # = 3
--	% cd ../.. && echo -e '..90..9\n...1.98\n...2..7\n6543456\n765.987\n876....\n987....' | cabal run 2024_day10-hoof-it_b # = 13
--	% cd ../.. && echo -e '012345\n123456\n234567\n345678\n4.6789\n56789.' | cabal run 2024_day10-hoof-it_b # = 227
--	% cd ../.. && echo -e '89010123\n78121874\n87430965\n96549874\n45678903\n32019012\n01329801\n10456732' | cabal run 2024_day10-hoof-it_b # = 81

validTrail :: String
validTrail = "0123456789"

findPaths :: PuzzleMap -> String -> [Point] -> Int
findPaths _  ""        hikers = length hikers
findPaths pm (t:trail) hikers = findPaths pm trail
             [ n
             | h <- hikers
             , n <- neighbors cardinalDirections h
             , Just t == pm !? n
             ]

solve :: PuzzleMap -> Int
solve pm = sum [findPaths pm (tail validTrail) [ix] | (ix, ch) <- assocs pm, ch == head validTrail]


main :: IO ()
main = interact $ show . solve . parseMap

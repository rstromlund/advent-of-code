{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.List (sort)
import Data.List.Extra (groupOn, sumOn')
import Math.NumberTheory.Logarithms (integerLog10)

--	... How many stones would you have after blinking a total of 75 times?
--
-- Test:
--	% cd ../.. && echo -e '125 17' | cabal run 2024_day11-plutonian-pebbles_b # = 55312

type Count = Int
type Rock  = Int
type GroupRock = (Rock, Count)

parse :: String -> [Rock]
parse = map read . words

blinkRock :: Rock -> [Rock]
blinkRock 0 = [1]
blinkRock 1 = [2024]
blinkRock n | 0 == r2 = [l, r]
  where (half, r2) = (`quotRem` 2) . succ . integerLog10 . fromIntegral $ n
        (l, r)     = n `divMod` (10 ^ half)
blinkRock n = [n * 2024]


blinks :: Int
blinks = 75

blink :: Int -> [GroupRock] -> Count
blink 0 = sumOn' snd
blink b = blink (b - 1) . mergeGroups . blinkRocks
  where blinkRocks = concatMap (\(r, cnt) -> map (,cnt) . blinkRock $ r)
        mergeGroups grs = [(r', sumOn' snd gs) | gs@((r',_):_) <- groupOn fst . sort $ grs]

solve :: [Rock] -> Count
solve = blink blinks . map (,1)


main :: IO ()
main = interact $ show . solve . parse

{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.List (group, sort)
import Data.Map.Strict ((!?), Map, empty, insertWith)

{-
... The network map provides a list of every connection between two computers. For example:

	kh-tc
	qp-kh
	de-cg
	ka-co
	yn-aq
	qp-ub
	cg-tb
	vc-aq
	tb-ka
	wh-tc
	yn-cg
	kh-ub
	ta-co
	de-co
	tc-td
	tb-wq
	wh-td
	ta-ka
	td-qp
	aq-cg
	wq-ub
	ub-vc
	de-ta
	wq-aq
	wq-vc
	wh-yn
	ka-de
	kh-ta
	co-tc
	wh-qp
	tb-vc
	td-yn

Each line of text in the network map represents a single connection; the line kh-tc represents a connection
between the computer named kh and the computer named tc. Connections aren't directional; tc-kh would mean
exactly the same thing.

LAN parties typically involve multiplayer games, so maybe you can locate it by finding groups of connected
computers. Start by looking for sets of three computers where each computer in the set is connected to the
other two computers.

In this example, there are 12 such sets of three inter-connected computers:

	aq,cg,yn
	aq,vc,wq
	co,de,ka
	co,de,ta
	co,ka,ta
	de,ka,ta
	kh,qp,ub
	qp,td,wh
	tb,vc,wq
	tc,td,wh
	td,wh,yn
	ub,vc,wq

If the Chief Historian is here, and he's at the LAN party, it would be best to know that right away. You're
pretty sure his computer's name starts with t, so consider only sets of three computers where at least one
computer's name starts with t. That narrows the list down to 7 sets of three inter-connected computers:

	co,de,ta
	co,ka,ta
	de,ka,ta
	qp,td,wh
	tb,vc,wq
	tc,td,wh
	td,wh,yn

Find all the sets of three inter-connected computers. How many contain at least one computer with a name that starts with t?

Test:
	% cd ../.. && echo -e 'kh-tc\nqp-kh\nde-cg\nka-co\nyn-aq\nqp-ub\ncg-tb\nvc-aq\ntb-ka\nwh-tc\nyn-cg\nkh-ub\nta-co\nde-co\ntc-td\ntb-wq\nwh-td\nta-ka\ntd-qp\naq-cg\nwq-ub\nub-vc\nde-ta\nwq-aq\nwq-vc\nwh-yn\nka-de\nkh-ta\nco-tc\nwh-qp\ntb-vc\ntd-yn' | cabal run 2024_day23-lan-party # = 7
-}

type LAN = Map String [String]

parse :: String -> [(String, String)]
parse = map parseNodes . lines
  where parseNodes :: String -> (String, String)
        parseNodes (a0:a1:'-':bs) = ([a0,a1], bs)

mkLAN :: [(String, String)] -> LAN
mkLAN = foldl' (\acc (n0, n1) -> insertWith (++) n1 [n0] (insertWith (++) n0 [n1] acc)) empty

mkThree :: [(String, String)] -> [[String]]
mkThree ns =
  [ a0
  | [a0@[c0:_,c1:_,c2:_],_a1,_a2] <- group . sort . foldl' find3rd [] $ ns
  , 't' == c0 || 't' == c1 || 't' == c2
  ]
  where find3rd :: [[String]] -> (String, String) -> [[String]]
        find3rd acc (n0, n1) =
          case lan !? n1 of
            Just ms -> [sort [n0,n1,n2] | n2 <- ms, n0 /= n2] ++ acc
            _       -> acc
        lan = mkLAN ns

solve :: [(String, String)] -> Int
solve = length . mkThree


main :: IO ()
main = interact $ show . solve . parse

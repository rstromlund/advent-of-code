{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns -Wno-name-shadowing #-}
module Main where
import Algorithm.Search (dijkstra)
import Data.Array.Unboxed ((//), (!?), Array, array)
import Data.List.Split (splitOn)
import Point (Point(..), cardinalDirections, neighbors)

{-
... you need to determine the first byte that will cut off the path to the exit.

In the above example, after the byte at 1,1 falls, there is still a path to the exit:

	O..#OOO
	O##OO#O
	O#OO#OO
	OOO#OO#
	###OO##
	.##O###
	#.#OOOO

However, after adding the very next byte (at 6,1), there is no longer a path to the exit:

	...#...
	.##..##
	.#..#..
	...#..#
	###..##
	.##.###
	#.#....

So, in this example, the coordinates of the first byte that prevents the exit from being reachable are 6,1.

Simulate more of the bytes that are about to corrupt your memory space. What are the coordinates of the first
byte that will prevent the exit from being reachable from your starting position? (Provide the answer as two
integers separated by a comma with no other characters.)

Test:
	% cd ../.. && echo -e '5,4\n4,2\n4,5\n3,0\n2,1\n6,3\n2,4\n1,5\n0,6\n3,3\n2,6\n5,1\n1,2\n5,5\n2,5\n6,5\n1,4\n0,4\n6,4\n1,1\n6,1\n1,0\n0,5\n1,6\n2,0' | cabal run 2024_day18-ram-run_b # = "6,1"
-}

parse :: String -> [Point]
parse = map ((\[xs, ys] -> Point (read ys) (read xs)) . splitOn ",") . lines

puzzleConf :: (Int, Int)
-- puzzleConf = (6, 12) -- Test
puzzleConf = (70, 1024) -- Prod

type State = Point

solve :: [Point] -> Point
solve ps = search pm start bs $ Just (0, [])
  where (pwidth, bytesFallen) = puzzleConf
        start = Point 0 0
        end   = Point pwidth pwidth
        (ps', bs) = splitAt bytesFallen ps
        --
        pm :: Array Point Bool
        pm = array (start, end) [(i, i `elem` ps') | x <- [0..pwidth], y <- [0..pwidth], let i = Point y x]
        --
        -- | Kind of a brute force solution, add each corrupt coordinate one at a time and try and solve w/ dijkstra's algorithm.
        --   return the coordinate that caused no solution to be found.  How else could this be solved?  By finding all possible
        --   paths and knocking them out one coordinate at a time?  IDK?  But this took < 30 seconds.
        search :: Array Point Bool -> Point -> [Point] -> Maybe (Int, [State]) -> Point
        search _ b' _ Nothing = b'
        search pm _ (b:bs) _  = search pm' b bs sol
          where pm' = pm `seq` pm // [(b, True)]
                sol = dijkstra getNeighbors (const . const 1) (end ==) start
                getNeighbors :: State -> [State]
                getNeighbors pos = [
                  i | i <- neighbors cardinalDirections pos,
                      Just False == pm' !? i
                  ]


main :: IO ()
main = interact $ show . solve . parse

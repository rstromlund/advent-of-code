{-# OPTIONS_GHC -Wno-incomplete-uni-patterns -Wno-incomplete-patterns -Wno-name-shadowing #-}
module Main where
import Data.Array.Unboxed ((!?), (//), assocs)
import Data.List.Split (splitOn)
import Point (Point(..), PuzzleMap, north, east, south, west, parseMap)

{-
... There is one key difference: everything except the robot is twice as wide! The robot's list of movements
doesn't change.

To get the wider warehouse's map, start with your original map and, for each tile, make the following changes:

	* If the tile is #, the new map contains ## instead.
	* If the tile is O, the new map contains [] instead.
	* If the tile is ., the new map contains .. instead.
	* If the tile is @, the new map contains @. instead.

This will produce a new warehouse map which is twice as wide and with wide boxes that are represented by
[]. (The robot does not change size.)

The larger example from before would now look like this:

	####################
	##....[]....[]..[]##
	##............[]..##
	##..[][]....[]..[]##
	##....[]@.....[]..##
	##[]##....[]......##
	##[]....[]....[]..##
	##..[][]..[]..[][]##
	##........[]......##
	####################

Because boxes are now twice as wide but the robot is still the same size and speed, boxes can be aligned such
that they directly push two other boxes at once. For example, consider this situation:

	#######
	#...#.#
	#.....#
	#..OO@#
	#..O..#
	#.....#
	#######
	<vv<<^^<<^^

After appropriately resizing this map, the robot would push around these boxes as follows:

	Initial state:
	##############
	##......##..##
	##..........##
	##....[][]@.##
	##....[]....##
	##..........##
	##############

	Move <:
	##############
	##......##..##
	##..........##
	##...[][]@..##
	##....[]....##
	##..........##
	##############
...
	Move <:
	##############0
	##......##..##1
	##..........##2
	##...[][]...##3
	##....[]....##4
	##.....@....##5
	##############6
	0123456789abcd

	Move ^:
	##############
	##......##..##
	##...[][]...##
	##....[]....##
	##.....@....##
	##..........##
	##############

	Move ^:
	##############
	##......##..##
	##...[][]...##
	##....[]....##
	##.....@....##
	##..........##
	##############
...last...
	Move ^:
	##############
	##...[].##..##
	##...@.[]...##
	##....[]....##
	##..........##
	##..........##
	##############

This warehouse also uses GPS to locate the boxes. For these larger boxes, distances are measured from the edge
of the map to the closest edge of the box in question. So, the box shown below has a distance of 1 from the
top edge of the map and 5 from the left edge of the map, resulting in a GPS coordinate of 100 * 1 + 5 = 105.

	##########
	##...[]...
	##........

In the scaled-up version of the larger example from above, after the robot has finished all of its moves, the
warehouse would look like this:

	####################
	##[].......[].[][]##
	##[]...........[].##
	##[]........[][][]##
	##[]......[]....[]##
	##..##......[]....##
	##..[]............##
	##..@......[].[][]##
	##......[][]..[]..##
	####################

The sum of these boxes' GPS coordinates is 9021.

Predict the motion of the robot and boxes in this new, scaled-up warehouse. What is the sum of all boxes'
final GPS coordinates?

Test:
	% # solved puzzle, test scoring % cd ../.. && echo -e '##############\n##...[].##..##\n##...@.[]...##\n##....[]....##\n##..........##\n##..........##\n##############' | cabal run 2024_day15-warehouse-woes_b # = 618
	% cd ../.. && echo -e '####################\n##[].......[].[][]##\n##[]...........[].##\n##[]........[][][]##\n##[]......[]....[]##\n##..##......[]....##\n##..[]............##\n##..@......[].[][]##\n##......[][]..[]..##\n####################' | cabal run 2024_day15-warehouse-woes_b # = 9021

	% cd ../.. && echo -e '#######\n#...#.#\n#.....#\n#..OO@#\n#..O..#\n#.....#\n#######\n\n<vv<<^^<<^^' | cabal run 2024_day15-warehouse-woes_b # = 618
	% cd ../.. && echo -e '##########\n#..O..O.O#\n#......O.#\n#.OO..O.O#\n#..O@..O.#\n#O#..O...#\n#O..O..O.#\n#.OO.O.OO#\n#....O...#\n##########\n\n<vv>^<v^>v>^vv^v>v<>v^v<v<^vv<<<^><<><>>v<vvv<>^v^>^<<<><<v<<<v^vv^v>^\nvvv<<^>^v^^><<>>><>^<<><^vv^^<>vvv<>><^^v>^>vv<>v<<<<v<^v>^<^^>>>^<v<v\n><>vv>v^v^<>><>>>><^^>vv>v<^^^>>v^v^<^^>v^^>v^<^v>v<>>v^v^<v>v^^<^^vv<\n<<v<^>>^^^^>>>v^<>vvv^><v<<<>^^^vv^<vvv>^>v<^^^^v<>^>vvvv><>>v^<<^^^^^\n^><^><>>><>^^<<^^v>>><^<v>^<vv>>v>>>^v><>^v><<<<v>>v<v<v>vvv>^<><<>^><\n^>><>^v<><^vvv<^^<><v<<<<<><^v<<<><<<^^<v<^^^><^>>^<v^><<<^>>^v<v^v<v^\n>^>>^v>vv>^<<^v<>><<><<v<<v><>v<^vv<<<>^^v^>^^>>><<^v>>v^v><^^>>^<>vv^\n<><^^>^^^<><vvvvv^v<v<<>^v<v>v<<^><<><<><<<^^<<<^<<>><<><^^^>^^<>^>v<>\n^^>vv<^v^v<vv>^<><v<^v>^^^>>>^^vvv^>vvv<>>>^<^>>>>>^<<^v>^vvv<>^<><<v>\nv^^>>><<^^<>>^v^<v^vv<>v^<<>^<^v^v><^<<<><<^<v><v<>vv>>v><v^<vv<>v^<<^' | cabal run 2024_day15-warehouse-woes_b # = 9021
-}

parse :: String -> (PuzzleMap, String)
parse = (\[pm, ds] -> (parseMap . concatMap dbl $ pm, filter ('\n' /=) ds)) . splitOn "\n\n"
  where dbl '#' = "##"
        dbl 'O' = "[]"
        dbl '.' = ".."
        dbl '@' = "@."
        dbl '\n' = "\n"

heading :: Char -> Point
heading '^' = north
heading 'v' = south
heading '<' = west
heading '>' = east

getOtherSide :: [(Point, Char)] -> [(Point, Char)]
getOtherSide qs@((pt, '['):_) = (pt + east, ']'):qs
getOtherSide qs@((pt, ']'):_) = (pt + west, '['):qs
getOtherSide qs               = qs

-- | Keep a list of boxes visited (with '.' floor marking), list of moved boxes, and a queue of boxes to analyze
moveBoxes :: PuzzleMap -> Point -> Point -> Char -> Maybe PuzzleMap
moveBoxes pm dir pos ch = moveBoxes' [] [] (getOtherSide [(pos, ch)])
  where moveBoxes' :: [(Point, Char)] -> [(Point, Char)] -> [(Point, Char)] -> Maybe PuzzleMap
        moveBoxes' vs bs []            = Just $ (pm // vs) // bs -- analyzed all, ok to push
        moveBoxes' _ _ ((_, '#'):_qs)  = Nothing -- hit a wall, no boxes/players can move
        moveBoxes' vs bs ((_, '.'):qs) = moveBoxes' vs bs qs -- pushed into empty space, that's ok

        moveBoxes' vs bs ((pt, ch):qs)
          | (pt, '.') `elem` vs = moveBoxes' vs bs qs -- already analyzed
          | otherwise           = moveBoxes' (v:vs) (b:bs) (getOtherSide ((pt', ch'):qs))
          where pt' = pt + dir
                (Just ch') = pm !? pt'
                v = (pt, '.')
                b = (pt', ch)

walk :: PuzzleMap -> Point -> [Char] -> PuzzleMap
walk pm _ [] = pm
walk pm pos (d:ds)
  | '#' == ch' = walk pm  pos  ds -- can't move
  | '[' == ch' ||
    ']' == ch' = push -- need to push boxes to move
  | otherwise  = walk pm  pos' ds -- can move freely
  where dir  = heading d
        pos' = pos + dir
        Just ch' = pm !? pos'
        --
        push = case moveBoxes pm dir pos' ch' of
                 (Just pm') -> walk pm' pos' ds
                 _          -> walk pm  pos  ds

score :: PuzzleMap -> Int
score pm = sum [y * 100 + x | (Point y x, '[') <- assocs pm]

solve :: (PuzzleMap, String) -> Int
solve (pm, ds) = score . walk pm' start $ ds
  where start = head [i | (i, '@') <- assocs pm]
        pm' = pm // [(start, '.')]


main :: IO ()
main = interact $ show . solve . parse

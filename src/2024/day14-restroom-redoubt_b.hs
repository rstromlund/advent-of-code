{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, optional, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)
import Point (Point(..), cardinalDirections)

{-
During the bathroom break, someone notices that these robots seem awfully similar to ones built and used at
the North Pole. If they're the same type of robots, they should have a hard-coded Easter egg: very rarely,
most of the robots should arrange themselves into a picture of a Christmas tree.

What is the fewest number of seconds that must elapse for the robots to display the Easter egg?


Test:
	% cd ../.. && echo -e 'p=0,4 v=3,-3\np=6,3 v=-1,-3\np=10,3 v=-1,2\np=2,0 v=2,-1\np=0,0 v=1,3\np=3,0 v=-2,-2\np=7,6 v=-1,-3\np=3,0 v=-1,-2\np=9,3 v=2,3\np=7,3 v=-1,2\np=2,4 v=2,-3\np=9,5 v=-3,-3' | cabal run 2024_day14-restroom-redoubt_b
-}

data SecGuard = SecGuard Point Point deriving (Show) -- Location and Velocity

type Parser = Parsec Void String

secGuards :: Parser [SecGuard]
secGuards = many secGuard <* eof

nbr :: Parser Int
nbr = f <$> optional (char '-') <*> decimal
  where f Nothing x = x
        f _       x = negate x

secGuard :: Parser SecGuard
secGuard = mkSecGuard <$>
             (string  "p=" *> (nbr `sepBy` char ',')) <*>
             (string " v=" *> (nbr `sepBy` char ',')) <* newline
  where mkSecGuard [x,y] [dx,dy] = SecGuard (Point y x) (Point dy dx)

prodPuzzleSize :: Point
prodPuzzleSize = Point 103 101
testPuzzleSize :: Point
testPuzzleSize = Point   7  11
puzzleSize :: Point
puzzleSize = prodPuzzleSize -- testPuzzleSize

{-
Here's the image snipped from the winning time generation (lovely isn't it):

(7037,[
...
 .....*******************************...
 .....*.............................*...
 .....*.............................*...
 *....*.............................*...
 .....*.............................*...
 .....*..............*..............*..*
 .....*.............***.............*...
 .....*............*****............*...
 .....*...........*******...........*...
 .....*..........*********..........*...
 .....*............*****............*...
 .....*...........*******...........*...
 .....*..........*********..........*...
 .....*.........***********.........*...
 .....*........*************........*...
 .....*..........*********..........*...
 .....*.........***********.........*...
 .....*........*************........*...
 .....*.......***************.......*...
 ..*..*......*****************......*...
 ...*.*........*************........*...
 .....*.......***************.......*...
 .....*......*****************......*...
 .....*.....*******************.....*...
 .....*....*********************....*...
 .....*.............***.............*...
 .....*.............***.............*...
 .....*.............***.............*...
 .....*.............................*..*
 .....*.............................*...
 .....*.............................*...
 .....*.............................*...
 .....*******************************...
 .......................................
...
])
-}

solve :: Either a [SecGuard] -> (Int, [String])
solve (Right is) =
  -- find the first generation that makes a "big" jump in the neighbor count, it should be the tree
  -- (most generations have < 300-ish neighbors; found by manually looking :)
  draw . head . filter ((> 500) . snd . fst) .
  -- count the number of guards with neighbors (a Christmas tree shape should have *lots* of neighbors)
  zipWith (\tm g -> ((tm, neighborCnt g), g)) [0..] .
  -- generate "generations" of security guard positions
  iterate (map simulate) $ is
  where (Point sizeY sizeX) = puzzleSize
        simulate :: SecGuard -> SecGuard
        simulate (SecGuard pos vel) = SecGuard (warp $ pos + vel) vel
        warp (Point y x) = Point (y `mod` sizeY) (x `mod` sizeX)
        --
        neighborCnt ps = sum [
          length [n | SecGuard n _ <- ps, d <- cardinalDirections, p + d == n]
            | SecGuard p _ <- ps
          ]
        --
        draw ((tm, _), gs) = (tm,) $ [
            [ if Point y x `elem` ps then '*' else '.'
              | x <- [0..sizeX-1]
            ]
            | y <- [0..sizeY-1]
          ]
          where ps = [p | SecGuard p _ <- gs]


main :: IO ()
main = interact $ show . solve . runParser secGuards "<stdin>"

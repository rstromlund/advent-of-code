{-# OPTIONS_GHC -Wno-incomplete-patterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, runParser, sepBy)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... some young elephants were playing nearby and stole all the operators from their calibration
--	equations! They could finish the calibrations if only someone could determine which test values could
--	possibly be produced by placing any combination of operators into their calibration equations (your
--	puzzle input).
--
--	For example:
--
--	190: 10 19
--	3267: 81 40 27
--	83: 17 5
--	156: 15 6
--	7290: 6 8 6 15
--	161011: 16 10 13
--	192: 17 8 14
--	21037: 9 7 18 13
--	292: 11 6 16 20
--
--	Each line represents a single equation. The test value appears before the colon on each line; it is
--	your job to determine whether the remaining numbers can be combined with operators to produce the test
--	value.
--
--	Operators are always evaluated left-to-right, not according to precedence rules. Furthermore, numbers
--	in the equations cannot be rearranged. Glancing into the jungle, you can see elephants holding two
--	different types of operators: add (+) and multiply (*).
--
--	Only three of the above equations can be made true by inserting operators:
--
--	* 190: 10 19 has only one position that accepts an operator: between 10 and 19. Choosing + would give
--	  29, but choosing * would give the test value (10 * 19 = 190).
--	* 3267: 81 40 27 has two positions for operators. Of the four possible configurations of the
--	  operators, two cause the right side to match the test value: 81 + 40 * 27 and 81 * 40 + 27 both
--	  equal 3267 (when evaluated left-to-right)!
--	* 292: 11 6 16 20 can be solved in exactly one way: 11 + 6 * 16 + 20.
--
--	The engineers just need the total calibration result, which is the sum of the test values from just
--	the equations that could possibly be true. In the above example, the sum of the test values for the
--	three equations listed above is 3749.
--
--	Determine which equations could possibly be true. What is their total calibration result?
--
-- Test:
--	% cd ../.. && echo -e '190: 10 19\n3267: 81 40 27\n83: 17 5\n156: 15 6\n7290: 6 8 6 15\n161011: 16 10 13\n192: 17 8 14\n21037: 9 7 18 13\n292: 11 6 16 20' | cabal run 2024_day07-bridge-repair # = 3749

type PuzzleList = (Int, [Int]) -- (Test Value, Test Parameters)

type Parser = Parsec Void String

plists :: Parser [PuzzleList]
plists = many (plist <* newline) <* eof

plist :: Parser PuzzleList
plist = (,) <$> (decimal <* string ": ") <*> (decimal `sepBy` char ' ')

-- | Build all possible combinations of applying (*) and (+) to solve the equation
calc :: [Int] -> [Int]
calc (f0:fs) =
  foldl (\lhss f ->
            foldl (\acc lhs ->
                     (lhs + f):(lhs * f):acc
                  ) [] lhss
        ) [f0] fs

solve :: Either a [PuzzleList] -> Int
solve (Right ps) = sum [ans | (ans, fs) <- ps, ans `elem` calc fs]


main :: IO ()
main = interact $ show . solve . runParser plists "<stdin>"

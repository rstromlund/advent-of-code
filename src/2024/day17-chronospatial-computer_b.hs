{-# OPTIONS_GHC -Wno-incomplete-patterns -Wno-incomplete-uni-patterns #-}
module Main where
import Algorithm.Search (dfs)
import Data.Array.IArray (Array, elems, listArray)
import Data.Bits (xor, shiftL, shiftR)
import Data.List.Split (chunksOf, splitOn)

{-
... this program is supposed to output another copy of the program! Unfortunately, the value in register A
seems to have been corrupted. You'll need to find a new value to which you can initialize register A so that
the program's output instructions produce an exact copy of the program itself.

For example:
	Register A: 2024
	Register B: 0
	Register C: 0

	Program: 0,3,5,4,3,0

This program outputs a copy of itself if register A is instead initialized to 117440. (The original initial
value of register A, 2024, is ignored.)

What is the lowest positive initial value for register A that causes the program to output a copy of itself?

Test:
	% cd ../.. && echo -e 'Register A: 2024\nRegister B: 0\nRegister C: 0\n\nProgram: 0,3,5,4,3,0' | cabal run 2024_day17-chronospatial-computer_b # = 117440

	% cd ../.. && echo -e 'Register A: 7\nRegister B: 0\nRegister C: 0\n\nProgram: 2,4,1,2,7,5,4,7,1,3,5,5,0,3' | cabal run 2024_day17-chronospatial-computer_b # no loop
-}

data CPU = CPU {regA, regB, regC, iptr :: !Int, outNs :: [Int], instrs :: Array Int [Int]} deriving (Show)

parse :: String -> CPU
parse = mkCPU . words
  where mkCPU :: [String] -> CPU
        mkCPU [
          "Register", "A:", stra,
          "Register", "B:", strb,
          "Register", "C:", strc,
          "Program:", strpgm] = CPU{
            regA = read stra,
            regB = read strb,
            regC = read strc,
            iptr = 0,
            outNs = [],
            instrs = mkIS . chunksOf 2 . map read . splitOn "," $ strpgm
            }
        mkIS is = listArray (0, length is - 1) is

{-
loop: -- Assmblr     | C-ish
	2,4 BST regA | regB = regA mod 8 -- i.e. regA & 7
	1,2 BXL 2    | regB = regB xor 2
	7,5 CDV regB | regC = regA >> regB -- was: div (2**regB)
	4,7 BXC 7    | regB = regB xor regC
	1,3 BXL 3    | regB = regB xor 3
	5,5 OUT regB | print(regB mod 8)
	0,3 ADV 3    | regA = regA >> 3 -- was: div (2**3)
3,0 JNZ 0 | if regA != 0 goto loop

PGM:
lso         ...         mso
2,4,1,2,7,5,4,7,1,3,5,5,0,3,3,0

Thanks to https://www.reddit.com/r/adventofcode/comments/1hg38ah/comment/m5mv8o4/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button
for insights.
-}

-- | *My* puzzle input creates this function (useful to inspect the problem space):
-- exec :: CPU -> CPU
-- exec CPU{regA = a, instrs = is} = loop [] a
--   where loop acc 0 = CPU{regA = a, regB = 0, regC = 0, iptr = 0, outNs = acc, instrs = is}
--         loop acc a = let b0 = (a `mod` 8) `xor` 2
--                          c  = a `shiftR` b0
--                          b1 = (b0 `xor` c) `xor` 3
--                      in loop ((b1 `mod` 8):acc) (a `shiftR` 3)

type State = (Int, [Int]) -- (regA, remaining octals)

solve :: CPU -> Int
solve CPU{instrs = is} = fst . last $ ss
  where Just ss = dfs nextStates (null . snd) (0, reverse . concat . elems $ is)
        nextStates (a, o:os) = [
          (a3 + i, os)
            | let a3 = a `shiftL` 3,
              i <- [7,6..0], -- the "last" state in the Foldable will be the first one visited; so put 0 in last
              let guess = (((i `xor` 2) `xor` ((a3 + i) `shiftR` (i `xor` 2))) `xor` 3) `mod` 8, -- specific to my puzzle input!
              o == guess
          ]

main :: IO ()
main = interact $ show . solve . parse

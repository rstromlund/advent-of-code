module Main where
import Data.List (minimum, sort)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, optional, runParser)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The player always goes first. Each attack reduces the opponent's hit points by at least 1. The first character at or
--	below 0 hit points loses.
--
--	Damage dealt by an attacker each turn is equal to the attacker's damage score minus the defender's armor score. An
--	attacker always does at least 1 damage. So, if the attacker has a damage score of 8, and the defender has an armor score
--	of 3, the defender loses 5 hit points. If the defender had an armor score of 300, the defender would still lose 1 hit
--	point.
--
--	Your damage score and armor score both start at zero. They can be increased by buying items in exchange for gold. You
--	start with no items and have as much gold as you need. Your total damage or armor is equal to the sum of those stats from
--	all of your items. You have 100 hit points.
--
--	Here is what the item shop is selling:
--
--	Weapons:    Cost  Damage  Armor
--	Dagger            4       0
--	Shortsword        5       0
--	Warhammer         6       0
--	Longsword         7       0
--	Greataxe          8       0
--
--	Armor:      Cost  Damage  Armor
--	Leather          0       1
--	Chainmail        0       2
--	Splintmail       0       3
--	Bandedmail       0       4
--	Platemail        0       5
--
--	Rings:      Cost  Damage  Armor
--	Damage +1    25     1       0
--	Damage +2    50     2       0
--	Damage +3   100     3       0
--	Defense +1   20     0       1
--	Defense +2   40     0       2
--	Defense +3   80     0       3
--
--	You must buy exactly one weapon; no dual-wielding. Armor is optional, but you can't use more than one. You can buy 0-2
--	rings (at most one for each hand). You must use any items you buy. The shop only has one of each item, so you can't buy,
--	for example, two rings of Damage +3.
--
--	For example, suppose you have 8 hit points, 5 damage, and 5 armor, and that the boss has 12 hit points, 7 damage, and 2 armor:
--	* The boss deals 7-5 = 2 damage; the player goes down to 6 hit points.
--	* The player deals 5-2 = 3 damage; the boss goes down to 6 hit points.
--	* The boss deals 7-5 = 2 damage; the player goes down to 4 hit points.
--	* The player deals 5-2 = 3 damage; the boss goes down to 3 hit points.
--	* The boss deals 7-5 = 2 damage; the player goes down to 2 hit points.
--	* The player deals 5-2 = 3 damage; the boss goes down to 0 hit points.
--
--	In this scenario, the player wins! (Barely.)
--
--	You have 100 hit points. The boss's actual stats are in your puzzle input. What is the least amount of gold you can spend and still win the fight?
--
-- Test:
--	% cd ../.. && echo -e 'Hit Points: 12\nDamage: 7\nArmor: 2' | cabal run 2015_day21-rpg-simulator-20xx < 21a.txt # ?

type Combatant = (Int, Int, Int)
-- you = (8, 5, 5) -- test you
you = (100, 0, 0)

type Parser = Parsec Void String

boss :: Parser Combatant
boss = string "Hit Points: " >> (,,) <$> decimal <* string "\nDamage: " <*> decimal <* string "\nArmor: " <*> decimal <* optional newline <* eof


-- Store [(Name, Cost, Damage, Armor)]
weapons = [ ("Dagger",      8, 4, 0)
          , ("Shortsword", 10, 5, 0)
          , ("Warhammer",  25, 6, 0)
          , ("Longsword",  40, 7, 0)
          , ("Greataxe",   74, 8, 0)
          ]

armor   = [ ("NoArmor",      0, 0, 0)
          , ("Leather",     13, 0, 1)
          , ("Chainmail",   31, 0, 2)
          , ("Splintmail",  53, 0, 3)
          , ("Bandedmail",  75, 0, 4)
          , ("Platemail",  102, 0, 5)
          ]

rings   = [ ("NoRing1",      0, 0, 0)
          , ("NoRing2",      0, 0, 0)
          , ("Damage +1",   25, 1, 0)
          , ("Damage +2",   50, 2, 0)
          , ("Damage +3",  100, 3, 0)
          , ("Defense +1",  20, 0, 1)
          , ("Defense +2",  40, 0, 2)
          , ("Defense +3",  80, 0, 3)
          ]


solve :: Either a Combatant -> Int
solve (Right boss) = fst . minimum . filter snd . map match $ [(w, a, r1, r2) | w <- weapons, a <- armor, r1 <- rings, r2 <- rings, r1 /= r2]
  where match ((w, wc, wd, wa), (a, ac, ad, aa), (r1, r1c, r1d, r1a), (r2, r2c, r2d, r2a)) =
          let c' = wc + ac + r1c + r2c
              d' = wd + ad + r1d + r2d
              a' = wa + aa + r1a + r2a
              (yh, yd, ya) = you
          in (c', fight (yh, yd + d', ya + a') boss)
        --
        -- Note: the damage each turn is constant, this could just be a calculation instead of a simulation.  But it's fast enough.
        fight :: Combatant -> Combatant -> Bool
        fight y@(yh, _, _) b@(bh, _, _)
          | bh <= 0   = True
          | yh <= 0   = False
          | otherwise = turn y b
        --
        turn :: Combatant -> Combatant -> Bool
        turn y@(yh, yd, ya) b@(bh, bd, ba) =
          let yt  = max (yd - ba) 1
              bh' = bh - yt
              bt  = max (bd - ya) 1
              yh' = yh - bt
          in fight (yh', yd, ya) (bh', bd, ba)


main :: IO ()
main = interact $ show . solve . runParser boss "<stdin>"

{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, runParser, sepBy)
import Text.Megaparsec.Char (char, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	You just finish implementing your winning light pattern when you realize you mistranslated Santa's message from Ancient Nordic Elvish.
--
--	The light grid you bought actually has individual brightness controls; each light can have a brightness of zero or more. The lights all start at zero.
--
--	The phrase turn on actually means that you should increase the brightness of those lights by 1.
--
--	The phrase turn off actually means that you should decrease the brightness of those lights by 1, to a minimum of zero.
--
--	The phrase toggle actually means that you should increase the brightness of those lights by 2.
--
--	What is the total brightness of all lights combined after following Santa's instructions?
--
--	For example:
--	* turn on 0,0 through 0,0 would increase the total brightness by 1.
--	* toggle 0,0 through 999,999 would increase the total brightness by 2000000.
--
-- Test:
--	% cd ../.. && echo -e 'turn on 0,0 through 0,0' | cabal run 2015_day06-probably-a-fire-hazard_b ## 1
--	% cd ../.. && echo -e 'toggle 0,0 through 999,999' | cabal run 2015_day06-probably-a-fire-hazard_b ## 2000000

data Action = TurnOn | TurnOff | Toggle deriving (Show, Ord, Eq)
type Square = (Int, Int, Int, Int) -- (xmin, xmax, ymin, ymax)
type Step = (Action, Square) -- (1=on|0=off, Square)

actionVal TurnOn  = 1
actionVal TurnOff = -1
actionVal Toggle  = 0

type Parser = Parsec Void String

instructions :: Parser Step
instructions = do
  a <- action
  _ <- space
  [x0, y0] <- coords
  _ <- string " through "
  [x1, y1] <- coords
  eof
  return (a, (x0, y0, x1, y1))

action :: Parser Action
action = choice
  [ TurnOn  <$ string "turn on"
  , TurnOff <$ string "turn off"
  , Toggle  <$ string "toggle" ]

coords :: Parser [Int]
coords = decimal `sepBy` char ','

-- Just run the x-es and y-es and calculate each pixel in the instruction "stack" (just 300 lines); seems simpler than allocating
-- a huge vector and running in a mutable vector monad; or trying to stack and intersect rectangles. (?)
solve :: Either a [Step] -> Int
solve (Right is) = foldl' stepX 0 [0..999]
  where stepX !acc !x = foldl' stepY acc [0..999]
          where stepY !acc !y = acc + getVal x y
        getVal !x !y = foldl' getVal' 0 is
          where getVal' acc (act, (x0, y0, x1, y1))
                  | x > x1 || x < x0 || y > y1 || y < y0 = acc -- doesnt apply
                  | TurnOn  == act = acc + 1
                  | TurnOff == act = max 0 $ acc - 1
                  | Toggle  == act = acc + 2

main :: IO ()
main = interact $ show . solve . mapM (runParser instructions "<stdin>") . lines

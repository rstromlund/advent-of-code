module Main where
import Data.List (last, maximum, nub, permutations, tails, take)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... You're going to find the optimal seating arrangement and avoid all those awkward conversations.
--
--	You start by writing up a list of everyone invited and the amount their happiness would increase or decrease if they were
--	to find themselves sitting next to each other person. You have a circular table that will be just big enough to fit
--	everyone comfortably, and so each person will have exactly two neighbors.
--
--	For example, suppose you have only four attendees planned, and you calculate their potential happiness as follows:
--	* Alice would gain 54 happiness units by sitting next to Bob.
--	* Alice would lose 79 happiness units by sitting next to Carol.
--	* Alice would lose 2 happiness units by sitting next to David.
--	* Bob would gain 83 happiness units by sitting next to Alice.
--	* Bob would lose 7 happiness units by sitting next to Carol.
--	* Bob would lose 63 happiness units by sitting next to David.
--	* Carol would lose 62 happiness units by sitting next to Alice.
--	* Carol would gain 60 happiness units by sitting next to Bob.
--	* Carol would gain 55 happiness units by sitting next to David.
--	* David would gain 46 happiness units by sitting next to Alice.
--	* David would lose 7 happiness units by sitting next to Bob.
--	* David would gain 41 happiness units by sitting next to Carol.
--
--	Then, if you seat Alice next to David, Alice would lose 2 happiness units (because David talks so much), but David would
--	gain 46 happiness units (because Alice is such a good listener), for a total change of 44.
--
--	If you continue around the table, you could then seat Bob next to Alice (Bob gains 83, Alice gains 54). Finally, seat
--	Carol, who sits next to Bob (Carol gains 60, Bob loses 7) and David (Carol gains 55, David gains 41). The arrangement
--	looks like this:
--
--	     +41 +46
--	+55   David    -2
--	Carol       Alice
--	+60    Bob    +54
--	     -7  +83
--
--	After trying every other seating arrangement in this hypothetical scenario, you find that this one is the most optimal, with a total change in happiness of 330.
--
--	What is the total change in happiness for the optimal seating arrangement of the actual guest list?
--
-- Test:
--	% ./day13-knights-of-the-dinner-table.hs < 13a.tst # 330

{- actors:
Alice
Bob
Carol
David
Eric
Frank
George
Mallory

would

lose/gain

#99

happiness units by sitting next to

{actor}
.
-}

type Scenario = (String, Int, String)

type Parser = Parsec Void String

preferences :: Parser [Scenario]
preferences = many preference <* eof

preference :: Parser Scenario
preference = (,,) <$> (actor <* string " would ") <*> score <* string " happiness units by sitting next to " <*> actor <* char '.' <* optional newline
  where actor = choice [ string "Alice"
                       , string "Bob"
                       , string "Carol"
                       , string "David"
                       , string "Eric"
                       , string "Frank"
                       , string "George"
                       , string "Mallory"
                       ]
        score = f <$> choice [string "lose ", string "gain "] <*> decimal
          where f ('l':_) s = -s
                f ('g':_) s = s

solve :: Either a [Scenario] -> Int
solve (Right ss) = maximum . map (foldl' (\acc (_, s, _) -> acc + s) 0 . mkScenarios . filter ((==) 2 . length) . map (take 2) . tails) . permutations $ actors
  where actors = nub . map (\(a1, _, _) -> a1) $ ss
        mkScenarios :: [[String]] -> [Scenario]
        mkScenarios as = findScenario [head . head $ as, last . last $ as] ++ concatMap findScenario as
        findScenario [a1, a2] = filter (\(s1, _, s2) -> (a1 == s1 && a2 == s2) || (a1 == s2 && a2 == s1)) ss


main :: IO ()
main = interact $ show . solve . runParser preferences "<stdin>"

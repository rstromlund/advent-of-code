{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}
module Main where
import Data.List (scanl')
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline)

--	Now, given the same instructions, find the position of the first character that causes him to enter the basement
--	(floor -1). The first character in the instructions has position 1, the second character has position 2, and so on.
--
--	For example:
--	* ) causes him to enter the basement at character position 1.
--	* ()()) causes him to enter the basement at character position 5.
--
-- Test:
--	% cd ../.. && echo -e ')' | cabal run 2015_day01-not-quite-lisp_b ## 1
--	% cd ../.. && echo -e '()())' | cabal run 2015_day01-not-quite-lisp_b ## 5

type Parser = Parsec Void String

elevate :: Parser [Int]
elevate = many move <* optional newline <* eof

move :: Parser Int
move = choice
  [ 1    <$ char '('
  , (-1) <$ char ')' ]

solve :: String -> Int
solve ps = length . takeWhile (>= 0) . scanl' (+) 0 $ ns
  where Right ns = runParser elevate "<stdin>" ps


main :: IO ()
main = interact $ show . solve

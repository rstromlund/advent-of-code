#!/bin/bash

## FIXME: is there a "pretty" way (via stack maybe) to run hlint?  I installed it via stack ... :/
stack exec hlint .
typeset rc=${?}

### Run all puzzle solutions:

#echo -e "\n\n==== day01"
#time ./day01-not-quite-lisp.hs   < 1a.txt ; (( rc += ${?} ))
#time ./day01-not-quite-lisp_b.hs < 1a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day02"
#time ./day02-i-was-told-there-would-be-no-math.hs   < 2a.txt ; (( rc += ${?} ))
#time ./day02-i-was-told-there-would-be-no-math_b.hs < 2a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day03"
#time ./day03-perfectly-spherical-houses-in-a-vacuum.hs   < 3a.txt ; (( rc += ${?} ))
#time ./day03-perfectly-spherical-houses-in-a-vacuum_b.hs < 3a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day04"
#time ./day04-the-ideal-stocking-stuffer.hs   < 4a.txt ; (( rc += ${?} ))
#time ./day04-the-ideal-stocking-stuffer_b.hs < 4a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day05"
#time ./day05-doesnt-he-have-intern_elves-for-this.hs   < 5a.txt ; (( rc += ${?} ))
#time ./day05-doesnt-he-have-intern_elves-for-this_b.hs < 5a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day06"
#time ./day06-probably-a-fire-hazard.hs   < 6a.txt ; (( rc += ${?} ))
#time ./day06-probably-a-fire-hazard_b.hs < 6a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day07"
#time ./day07-some-assembly-required.hs < 7a.txt ; (( rc += ${?} ))
#	## Part 2 is odd -> "Now, take the signal you got on wire a, override wire b to that signal, and reset the other wires (including
#	## wire a). What new signal is ultimately provided to wire a?"  Wow, there's a clump of words; this seems to satify tho:
#sed -e 's!^14146 -> b$!956 -> b!'      < 7a.txt | time ./day07-some-assembly-required.hs ; (( rc += ${?} ))

#echo -e "\n\n==== day08"
#time ./day08-matchsticks.hs   < 8a.txt ; (( rc += ${?} ))
#time ./day08-matchsticks_b.hs < 8a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day09"
#time ./day09-all-in-a-single-night.hs   < 9a.txt ; (( rc += ${?} ))
#time ./day09-all-in-a-single-night_b.hs < 9a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day10"
#time ./day10-elves-look-elves-say.hs   < 10a.txt ; (( rc += ${?} ))
#time ./day10-elves-look-elves-say_b.hs < 10a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day11"
#time ./day11-corporate-policy.hs   < 11a.txt ; (( rc += ${?} ))
#time ./day11-corporate-policy_b.hs < 11a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day12"
#time ./day12-jsabacusframework-io.hs   < 12a.txt ; (( rc += ${?} ))
#time ./day12-jsabacusframework-io_b.hs < 12a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day13"
#time ./day13-knights-of-the-dinner-table.hs   < 13a.txt ; (( rc += ${?} ))
#time ./day13-knights-of-the-dinner-table_b.hs < 13a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day14"
#time ./day14-reindeer-olympics.hs   < 14a.txt ; (( rc += ${?} ))
#time ./day14-reindeer-olympics_b.hs < 14a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day15"
#time ./day15-science-for-hungry-people.hs   < 15a.txt ; (( rc += ${?} ))
#time ./day15-science-for-hungry-people_b.hs < 15a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day16"
#time ./day16-aunt-sue.hs   < 16a.txt ; (( rc += ${?} ))
#time ./day16-aunt-sue_b.hs < 16a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day17"
#time ./day17-no-such-thing-as-too-much.hs   < 17a.txt ; (( rc += ${?} ))
#time ./day17-no-such-thing-as-too-much_b.hs < 17a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day18"
#time ./day18-like-a-gif-for-your-yard.hs   < 18a.txt ; (( rc += ${?} ))
#time ./day18-like-a-gif-for-your-yard_b.hs < 18a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day19"
#time ./day19-medicine-for-rudolph.hs   < 19a.txt ; (( rc += ${?} ))
#time ./day19-medicine-for-rudolph_b.hs < 19a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day20"
#time ./day20-infinite-elves-and-infinite-houses.hs   < 20a.txt ; (( rc += ${?} ))
#time ./day20-infinite-elves-and-infinite-houses_b.hs < 20a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day21"
#time ./day21-rpg-simulator-20xx.hs   < 21a.txt ; (( rc += ${?} ))
#time ./day21-rpg-simulator-20xx_b.hs < 21a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day22"
#time ./day22-wizard-simulator-20xx.hs   < 22a.txt ; (( rc += ${?} ))
#time ./day22-wizard-simulator-20xx_b.hs < 22a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day23"
#time ./day23-opening-the-turing-lock.hs   < 23a.txt ; (( rc += ${?} ))
#time ./day23-opening-the-turing-lock_b.hs < 23a.txt ; (( rc += ${?} ))

#echo -e "\n\n==== day24"
#time ./day24-it-hangs-in-the-balance.hs   < 24a.txt ; (( rc += ${?} ))
#time ./day24-it-hangs-in-the-balance_b.hs < 24a.txt ; (( rc += ${?} ))

echo -e "\n\n==== day25"
time ./day25-let-it-snow.hs < 25a.txt ; (( rc += ${?} ))

exit ${rc}

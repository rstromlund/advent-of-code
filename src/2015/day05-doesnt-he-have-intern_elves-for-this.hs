module Main where
import Data.List (elem, group, isInfixOf, length)

--	Santa needs help figuring out which strings in his text file are naughty or nice.
--
--	A nice string is one with all of the following properties:
--	* It contains at least three vowels (aeiou only), like aei, xazegov, or aeiouaeiouaeiou.
--	* It contains at least one letter that appears twice in a row, like xx, abcdde (dd), or aabbccdd (aa, bb, cc, or dd).
--	* It does not contain the strings ab, cd, pq, or xy, even if they are part of one of the other requirements.
--
--	For example:
--	* ugknbfddgicrmopn is nice because it has at least three vowels (u...i...o...), a double letter (...dd...), and none of the disallowed substrings.
--	* aaa is nice because it has at least three vowels and a double letter, even though the letters used by different rules overlap.
--	* jchzalrnumimnmhp is naughty because it has no double letter.
--	* haegwjzuvuyypxyu is naughty because it contains the string xy.
--	* dvszwmarrgswjxmb is naughty because it contains only one vowel.
--
--	How many strings are nice?
--
-- Test:
--	% cd ../.. && echo -e 'ugknbfddgicrmopn' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this ## 1
--	% cd ../.. && echo -e 'aaa' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this ## 1
--	% cd ../.. && echo -e 'jchzalrnumimnmhp' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this ## 0
--	% cd ../.. && echo -e 'haegwjzuvuyypxyu' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this ## 0
--	% cd ../.. && echo -e 'dvszwmarrgswjxmb' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this ## 0

solve :: [String] -> Int
solve = length . filter isNice
  where vowels          = "aeiou"
        forbidden       = ["ab", "cd", "pq", "xy"]
        hasThreeVowels  = (>= 3) . length . filter (`elem` vowels)
        hasDoubleLetter = any ((>= 2) . length) . group
        hasNoForbidden wd = not . any (`isInfixOf` wd) $ forbidden
        isNice wd       = hasThreeVowels wd && hasDoubleLetter wd && hasNoForbidden wd


main :: IO ()
main = interact $ show . solve . lines

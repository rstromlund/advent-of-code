module Main where
import Data.Bool (bool)
import Data.List (maximum)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Instead, at the end of each second, he awards one point to the reindeer currently in the lead. (If there are multiple
--	reindeer tied for the lead, they each get one point.) He keeps the traditional 2503 second time limit, of course, as doing
--	otherwise would be entirely ridiculous.
--
--	Given the example reindeer from above, after the first second, Dancer is in the lead and gets one point. He stays in the
--	lead until several seconds into Comet's second burst: after the 140th second, Comet pulls into the lead and gets his first
--	point. Of course, since Dancer had been in the lead for the 139 seconds before that, he has accumulated 139 points by the
--	140th second.
--
--	After the 1000th second, Dancer has accumulated 689 points, while poor Comet, our old champion, only has 312. So, with the
--	new scoring system, Dancer would win (if the race ended at 1000 seconds).
--
--	Again given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, how many points does the
--	winning reindeer have?
--
-- Test:
--	% cd ../.. && echo -e 'Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\nDancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.' | cabal run 2015_day14-reindeer-olympics_b # 689 (for 1000 seconds)

type ReindeerStat = (String, Int, Int, Int) -- reindeer, speed, dur, rest
type RaceStat = ((Int, Int, Bool, Int), ReindeerStat) -- (dist flown, tm remaining, flying?, points), rstats

type Parser = Parsec Void String

totDur = 2503
--totDur = 140 -- tests per description
--totDur = 1000 -- for example above.

stats :: Parser [ReindeerStat]
stats = many stat <* eof

stat :: Parser ReindeerStat
stat = (,,,) <$> reindeer <* string " can fly " <*> decimal <* string " km/s for " <*> decimal <* string " seconds, but then must rest for " <*> decimal <* string " seconds." <* optional newline
  where reindeer = choice [ string "Dancer",  string "Cupid",   string "Rudolph", string "Donner", string "Dasher"
                          , string "Blitzen", string "Prancer", string "Comet",   string "Vixen"]

solve :: Either a [ReindeerStat] -> Int
solve (Right ss) = maxPts . (!! totDur) . iterate (score . map fly) $ map (\r@(_, spd, dur, rest) -> ((0, dur, True, 0), r)) ss
  where fly :: RaceStat -> RaceStat
        fly ((dist, rem, flying, pts), r@(_, spd, dur, rest))
          | flying && 0 == rem = ((dist, rest - 1, False, pts), r)     -- switched to/and rested on this clock tick.
          | flying             = ((dist + spd, rem - 1, True, pts), r) -- otherwise just flying
          | 0 == rem           = ((dist + spd, dur - 1, True, pts), r) -- switched to/and flew again on this clock tick.
          | otherwise          = ((dist, rem - 1, False, pts), r)      -- otherwise just resting
        score :: [RaceStat] -> [RaceStat]
        score rs = map (\i@((dist, rem, flying, pts), r) -> bool i ((dist, rem, flying, pts + 1), r) $ dist == m) rs
          where m = maximum . map (\((dist, _, _, _), _) -> dist) $ rs
        maxPts :: [RaceStat] -> Int
        maxPts = maximum . map (\((_, _, _, pts), _) -> pts)


main :: IO ()
main = interact $ show . solve . runParser stats "<stdin>"

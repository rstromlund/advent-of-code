module Main where
import Data.Bifunctor (bimap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, count, eof, many, noneOf, oneOf, runParser)
import Text.Megaparsec.Char (char, hexDigitChar)

--	... it is important to realize the difference between the number of characters in the code representation of the string
--	literal and the number of characters in the in-memory string itself.
--
--	For example:
--	* "" is 2 characters of code (the two double quotes), but the string contains zero characters.
--	* "abc" is 5 characters of code, but 3 characters in the string data.
--	* "aaa\"aaa" is 10 characters of code, but the string itself contains six "a" characters and a single, escaped quote character, for a total of 7 characters in the string data.
--	* "\x27" is 6 characters of code, but the string itself contains just one - an apostrophe ('), escaped using hexadecimal notation.
--
--	Santa's list is a file that contains many double-quoted string literals, one on each line. The only escape sequences used
--	are \\ (which represents a single backslash), \" (which represents a lone double-quote character), and \x plus two
--	hexadecimal characters (which represents a single character with that ASCII code).
--
--	Disregarding the whitespace in the file, what is the number of characters of code for string literals minus the number of
--	characters in memory for the values of the strings in total for the entire file?
--
--	For example, given the four strings above, the total number of characters of string code (2 + 5 + 10 + 6 = 23) minus the
--	total number of characters in memory for string values (0 + 3 + 7 + 1 = 11) is 23 - 11 = 12.
--
-- Test:
--	% cd ../.. && echo -e '""\n"abc"\n"aaa\\"aaa"\n"\\x27"' | cabal run 2015_day08-matchsticks ## 12

type Parser = Parsec Void String

aocString :: Parser String
aocString = do
  char '"'
  content <- many escapedChar
  char '"'
  eof
  return content

escapedChar :: Parser Char
escapedChar = noneOf "\"\\"
             <|> char '\\' *> escape
  where escape :: Parser Char
        escape = do
          ch <- oneOf "x\\\""
          if ch /= 'x'
            then pure ch
            else do _ <- count 2 hexDigitChar -- just burn the next 2 hex chars
                    return '.' -- we don't need to actually compute the char in part1.

parse ss = zip ss <$> mapM (runParser aocString "<stdin>") ss

solve :: Either a [(String, String)] -> Int
solve (Right ps) = sum . map (uncurry (-) . bimap length length) $ ps


main :: IO ()
main = interact $ show . solve . parse . lines

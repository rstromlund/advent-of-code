module Main where
import Data.List (subsequences)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, noneOf, runParser, some)
import Text.Megaparsec.Char.Lexer (decimal)

--	... The elves bought too much eggnog again - 150 liters this time. To fit it all into your refrigerator, you'll need to
--	move it into smaller containers. You take an inventory of the capacities of the available containers.
--
--	For example, suppose you have containers of size 20, 15, 10, 5, and 5 liters. If you need to store 25 liters, there are
--	four ways to do it:
--	* 15 and 10
--	* 20 and 5 (the first 5)
--	* 20 and 5 (the second 5)
--	* 15, 5, and 5
--
--	Filling all containers entirely, how many different combinations of containers can exactly fit all 150 liters of eggnog?
--
-- Test:
--	% cd ../.. && echo -e '20\n15\n10\n5\n5' | cabal run 2015_day17-no-such-thing-as-too-much # 4

type Parser = Parsec Void String

containers :: Parser [Int]
containers = some (decimal <* many (noneOf "0123456789")) <* eof

--targetLiters = 25 -- test example
targetLiters = 150

solve :: Either a [Int] -> Int
solve (Right cs) = length . filter ((targetLiters ==) . sum) . subsequences $ cs


main :: IO ()
main = interact $ show . solve . runParser containers "<stdin>"

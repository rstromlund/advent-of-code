module Main where
import Data.List (maximum, sort)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, optional, runParser)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	Turns out the shopkeeper is working with the boss, and can persuade you to buy whatever items he wants. The other rules
--	still apply, and he still only has one of each item.
--
--	What is the most amount of gold you can spend and still lose the fight?

type Combatant = (Int, Int, Int)
you = (100, 0, 0)

type Parser = Parsec Void String

boss :: Parser Combatant
boss = string "Hit Points: " >> (,,) <$> decimal <* string "\nDamage: " <*> decimal <* string "\nArmor: " <*> decimal <* optional newline <* eof


-- Store [(Name, Cost, Damage, Armor)]
weapons = [ ("Dagger",      8, 4, 0)
          , ("Shortsword", 10, 5, 0)
          , ("Warhammer",  25, 6, 0)
          , ("Longsword",  40, 7, 0)
          , ("Greataxe",   74, 8, 0)
          ]

armor   = [ ("NoArmor",      0, 0, 0)
          , ("Leather",     13, 0, 1)
          , ("Chainmail",   31, 0, 2)
          , ("Splintmail",  53, 0, 3)
          , ("Bandedmail",  75, 0, 4)
          , ("Platemail",  102, 0, 5)
          ]

rings   = [ ("NoRing1",      0, 0, 0)
          , ("NoRing2",      0, 0, 0)
          , ("Damage +1",   25, 1, 0)
          , ("Damage +2",   50, 2, 0)
          , ("Damage +3",  100, 3, 0)
          , ("Defense +1",  20, 0, 1)
          , ("Defense +2",  40, 0, 2)
          , ("Defense +3",  80, 0, 3)
          ]


solve :: Either a Combatant -> Int
solve (Right boss) = fst . maximum . filter (not . snd) . map match $ [(w, a, r1, r2) | w <- weapons, a <- armor, r1 <- rings, r2 <- rings, r1 /= r2]
  where match ((w, wc, wd, wa), (a, ac, ad, aa), (r1, r1c, r1d, r1a), (r2, r2c, r2d, r2a)) =
          let c' = wc + ac + r1c + r2c
              d' = wd + ad + r1d + r2d
              a' = wa + aa + r1a + r2a
              (yh, yd, ya) = you
          in (c', fight (yh, yd + d', ya + a') boss)
        --
        -- Note: the damage each turn is constant, this could just be a calculation instead of a simulation.  But it's fast enough.
        fight :: Combatant -> Combatant -> Bool
        fight y@(yh, _, _) b@(bh, _, _)
          | bh <= 0   = True
          | yh <= 0   = False
          | otherwise = turn y b
        --
        turn :: Combatant -> Combatant -> Bool
        turn y@(yh, yd, ya) b@(bh, bd, ba) =
          let yt  = max (yd - ba) 1
              bh' = bh - yt
              bt  = max (bd - ya) 1
              yh' = yh - bt
          in fight (yh', yd, ya) (bh', bd, ba)


main :: IO ()
main = interact $ show . solve . runParser boss "<stdin>"

module Main where
import Data.Array (Array, bounds, listArray, (!))
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, oneOf, optional, runParser, (<|>))
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	The manual explains that the computer supports two registers and six instructions
--	(truly, it goes on to remind the reader, a state-of-the-art technology). The
--	registers are named a and b, can hold any non-negative integer, and begin with a
--	value of 0. The instructions are as follows:
--	* hlf r sets register r to half its current value, then continues with the next instruction.
--	* tpl r sets register r to triple its current value, then continues with the next instruction.
--	* inc r increments register r, adding 1 to it, then continues with the next instruction.
--	* jmp offset is a jump; it continues with the instruction offset away relative to itself.
--	* jie r, offset is like jmp, but only jumps if register r is even ("jump if even").
--	* jio r, offset is like jmp, but only jumps if register r is 1 ("jump if one", not odd).
--
--	All three jump instructions work with an offset relative to that instruction. The
--	offset is always written with a prefix + or - to indicate the direction of the
--	jump (forward or backward, respectively). For example, jmp +1 would simply
--	continue with the next instruction, while jmp +0 would continuously jump back to
--	itself forever.
--
--	The program exits when it tries to run an instruction beyond the ones defined.
--
--	For example, this program sets a to 2, because the jio instruction causes it to skip the tpl instruction:
--
--	inc a
--	jio a, +2
--	tpl a
--	inc a
--
--	What is the value in register b when the program in your puzzle input is finished executing?
--
-- Test:
--	% cd ../.. && echo -e 'inc a\njio a, +2\ntpl a\ninc a' | cabal run 2015_day23-opening-the-turing-lock ## ?

data IS -- Instruction Set
  = Hlf Char
  | Tpl Char
  | Inc Char
  | Jmp Int
  | Jie Char Int
  | Jio Char Int
  deriving (Show)

data CPU = CPU { regA :: !Int
               , regB :: !Int
               , iptr   :: !Int
               }
           deriving (Show)

type Parser = Parsec Void String

instrs :: Parser (Array Int IS)
instrs = mkra <$> many (instr <* optional newline) <* eof
  where mkra :: [IS] -> Array Int IS
        mkra iss = listArray (0, length iss - 1) iss

instr :: Parser IS
instr = (string "hlf " >> Hlf <$> oneOf "ab")
    <|> (string "tpl " >> Tpl <$> oneOf "ab")
    <|> (string "inc " >> Inc <$> oneOf "ab")
    <|> (string "jmp " >> Jmp <$> signed    )
    <|> (string "jie " >> Jie <$> oneOf "ab" <* string ", " <*> signed)
    <|> (string "jio " >> Jio <$> oneOf "ab" <* string ", " <*> signed)
  where signed :: Parser Int
        signed = f <$> oneOf "+-" <*> decimal
          where f '+' x = x
                f _   x = negate x

solve :: Either a (Array Int IS) -> Int
solve (Right instrs) = regB $ exec CPU{regA = 0, regB = 0, iptr = 0}
  where (imin, imax) = bounds instrs
        get 'a' = regA
        get 'b' = regB
        set 'a' r cpu = cpu{regA = max 0 r}
        set 'b' r cpu = cpu{regB = max 0 r}
        exec :: CPU -> CPU
        exec cpu@CPU{iptr = ip}
          | ip > imax = cpu
          | otherwise = exec . exec' $ instrs ! ip
          where exec' :: IS -> CPU
                exec' (Hlf reg)     = (set reg (get reg cpu `div` 2) cpu){iptr = ip + 1}
                exec' (Tpl reg)     = (set reg (get reg cpu *     3) cpu){iptr = ip + 1}
                exec' (Inc reg)     = (set reg (get reg cpu +     1) cpu){iptr = ip + 1}
                exec' (Jmp off)     = cpu{iptr = ip + off}
                exec' (Jie reg off) = if even $ get reg cpu then cpu{iptr = ip + off} else cpu{iptr = ip + 1}
                exec' (Jio reg off) = if   1 == get reg cpu then cpu{iptr = ip + off} else cpu{iptr = ip + 1}


main :: IO ()
main = interact $ show . solve . runParser instrs "<stdin>"

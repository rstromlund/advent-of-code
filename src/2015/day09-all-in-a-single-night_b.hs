module Main where
import Data.List (find, nub, permutations)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser)
import Text.Megaparsec.Char (letterChar, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... What is the distance of the longest route?
--
-- Test:
--	% cd ../.. && echo -e 'London to Dublin = 464\nLondon to Belfast = 518\nDublin to Belfast = 141' | cabal run 2015_day09-all-in-a-single-night_b ## 982

type Parser = Parsec Void String

route :: Parser ((String, String), Int)
route = do
  l1 <- location
  string " to "
  l2 <- location
  string " = "
  dist <- decimal
  eof
  return ((l1, l2), dist)

location :: Parser String
location = many letterChar

solve :: Either a [((String, String), Int)] -> Int
solve (Right rs) = maximum . map distance . filter (/= Nothing) . map sequence $ roads
  where distinctLocs :: [String] -- Each city to visit
        distinctLocs = nub . foldl' (\acc ((l1, l2), _) -> l1:l2:acc) [] $ rs
        roads :: [[Maybe ((String, String), Int)]] -- All permutations of roads to our cities
        roads = map makeRoad . permutations $ distinctLocs
          where makeRoad (p1:ps@(p2:_)) = find (\((r1, r2), _) -> (p1 == r1 && p2 == r2) || (p1 == r2 && p2 == r1)) rs:makeRoad ps
                makeRoad [_] = []
        distance (Just rs') = sum . map snd $ rs'


main :: IO ()
main = interact $ show . solve . mapM (runParser route "<stdin>") . lines

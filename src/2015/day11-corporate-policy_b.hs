module Main where
import Data.List (any, drop, elem, filter)

--	Santa's password expired again. What's the next one?

nextPassCandidate :: String -> String
nextPassCandidate = reverse . inc . reverse
  where inc ('z':ps) = 'a':inc ps
        inc (p:ps) = succ p:ps

solve :: String -> String
solve = (!! 1) . filter validPass . drop 1 . iterate nextPassCandidate
  where validPass p = notConfusing p && findStraight p && countDoubles 0 p >= 2
        --
        notConfusing = not . any (`elem` "iol")
        --
        findStraight (p1:ps@(p2:p3:_))
          | succ p1 == p2 && succ p2 == p3 = True
          | otherwise = findStraight ps
        findStraight _ = False
        --
        countDoubles :: Int -> String -> Int -- TODO? could short circuit at cnt==2 and return a Bool instead.
        countDoubles cnt (p1:p2:ps)
          | p1 == p2  = countDoubles (cnt + 1) ps
          | otherwise = countDoubles cnt (p2:ps)
        countDoubles cnt _ = cnt


main :: IO ()
main = interact $ solve . head . lines

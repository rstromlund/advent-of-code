module Main where
import Data.List (takeWhile)
import Data.Set (Set, fromList, intersection)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, some, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... you need to figure out which Aunt Sue (which you conveniently number 1 to 500, for sanity) gave you the gift. You open
--	the present and, as luck would have it, good ol' Aunt Sue got you a My First Crime Scene Analysis Machine! Just what you
--	wanted. Or needed, as the case may be.
--
--	The My First Crime Scene Analysis Machine (MFCSAM for short) can detect a few specific compounds in a given sample, as
--	well as how many distinct kinds of those compounds there are. According to the instructions, these are what the MFCSAM can
--	detect:
--	* children, by human DNA age analysis.
--	* cats. It doesn't differentiate individual breeds.
--	* Several seemingly random breeds of dog: samoyeds, pomeranians, akitas, and vizslas.
--	* goldfish. No other kinds of fish.
--	* trees, all in one group.
--	* cars, presumably by exhaust or gasoline or something.
--	* perfumes, which is handy, since many of your Aunts Sue wear a few kinds.
--
--	In fact, many of your Aunts Sue have many of these. You put the wrapping from the gift into the MFCSAM. It beeps
--	inquisitively at you a few times and then prints out a message on ticker tape:
--	* children: 3
--	* cats: 7
--	* samoyeds: 2
--	* pomeranians: 3
--	* akitas: 0
--	* vizslas: 0
--	* goldfish: 5
--	* trees: 3
--	* cars: 2
--	* perfumes: 1
--
--	You make a list of the things you can remember about each Aunt Sue. Things missing from your list aren't zero - you simply don't remember the value.
--
--	What is the number of the Sue that got you the gift?
--
-- Test:
--	% cd ../.. && echo -e 'Sue 7: pomeranians: 5, samoyeds: 0, perfumes: 10' | cabal run 2015_day16-aunt-sue # debug parse

type Aunt = Set (String, Int) -- e.g. cars: 9
type Parser = Parsec Void String

aunts :: Parser [Aunt]
aunts = some aunt <* eof

aunt :: Parser Aunt
aunt = word *> char ' ' *> decimal *> string ": " *> (fromList <$> some items) <* optional newline
  where word = some letterChar
        items :: Parser (String, Int)
        items = (,) <$> word <* string ": " <*> decimal <* optional (string ", ")

mysteryAunt = fromList [("children", 3), ("cats", 7), ("samoyeds", 2), ("pomeranians", 3), ("akitas", 0), ("vizslas", 0), ("goldfish", 5), ("trees", 3), ("cars", 2), ("perfumes", 1)]

solve :: Either a [Aunt] -> Int
solve (Right as) = (1 +) . length . takeWhile (False ==) . map (\a -> mysteryAunt `intersection` a == a) $ as


main :: IO ()
main = interact $ show . solve . runParser aunts "<stdin>"

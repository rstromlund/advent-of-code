module Main where
import Data.List (any, drop, elem, filter)

--	... passwords must be exactly eight lowercase letters (for security reasons), so he finds his new password by incrementing
--	his old password string repeatedly until it is valid.
--
--	Incrementing is just like counting with numbers: xx, xy, xz, ya, yb, and so on. Increase the rightmost letter one step; if
--	it was z, it wraps around to a, and repeat with the next letter to the left until one doesn't wrap around.
--
--	Unfortunately for Santa, a new Security-Elf recently started, and he has imposed some additional password requirements:
--	* Passwords must include one increasing straight of at least three letters, like abc, bcd, cde, and so on, up to xyz. They cannot skip letters; abd doesn't count.
--	* Passwords may not contain the letters i, o, or l, as these letters can be mistaken for other characters and are therefore confusing.
--	* Passwords must contain at least two different, non-overlapping pairs of letters, like aa, bb, or zz.
--
--	For example:
--	* hijklmmn meets the first requirement (because it contains the straight hij) but fails the second requirement requirement (because it contains i and l).
--	* abbceffg meets the third requirement (because it repeats bb and ff) but fails the first requirement.
--	* abbcegjk fails the third requirement, because it only has one double letter (bb).
--	* The next password after abcdefgh is abcdffaa.
--	* The next password after ghijklmn is ghjaabcc, because you eventually skip all the passwords that start with ghi..., since i is not allowed.
--
--	Given Santa's current password (your puzzle input), what should his next password be?
--
-- Test:
--	% cd ../.. && echo -e 'hijklmmm' | cabal run 2015_day11-corporate-policy ## fails second req
--	% cd ../.. && echo -e 'abbcefff' | cabal run 2015_day11-corporate-policy ## fails first req
--	% cd ../.. && echo -e 'abbcegjj' | cabal run 2015_day11-corporate-policy ## fails third req
--	% cd ../.. && echo -e 'abcdefgg' | cabal run 2015_day11-corporate-policy ## abcdffaa
--	% cd ../.. && echo -e 'ghijklmm' | cabal run 2015_day11-corporate-policy ## ghjaabcc

nextPassCandidate :: String -> String
nextPassCandidate = reverse . inc . reverse
  where inc ('z':ps) = 'a':inc ps
        inc (p:ps) = succ p:ps

solve :: String -> String
solve = (!! 0) . filter validPass . drop 1 . iterate nextPassCandidate
  where validPass p = notConfusing p && findStraight p && countDoubles 0 p >= 2
        --
        notConfusing = not . any (`elem` "iol")
        --
        findStraight (p1:ps@(p2:p3:_))
          | succ p1 == p2 && succ p2 == p3 = True
          | otherwise = findStraight ps
        findStraight _ = False
        --
        countDoubles :: Int -> String -> Int
        countDoubles cnt (p1:p2:ps)
          | p1 == p2  = countDoubles (cnt + 1) ps
          | otherwise = countDoubles cnt (p2:ps)
        countDoubles cnt _ = cnt


main :: IO ()
main = interact $ solve . head . lines

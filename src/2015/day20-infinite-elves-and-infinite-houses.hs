module Main where
import Data.List (group, takeWhile)

--	... He sends them down a street with infinite houses numbered sequentially: 1, 2, 3, 4, 5, and so on.
--
--	Each Elf is assigned a number, too, and delivers presents to houses based on that number:
--	* The first Elf (number 1) delivers presents to every house: 1, 2, 3, 4, 5, ....
--	* The second Elf (number 2) delivers presents to every second house: 2, 4, 6, 8, 10, ....
--	* Elf number 3 delivers presents to every third house: 3, 6, 9, 12, 15, ....
--
--	There are infinitely many Elves, numbered starting with 1. Each Elf delivers presents equal to ten times his or her number at each house.
--
--	So, the first nine houses on the street end up like this:
--
--	House 1 got 10 presents.
--	House 2 got 30 presents.
--	House 3 got 40 presents.
--	House 4 got 70 presents.
--	House 5 got 60 presents.
--	House 6 got 120 presents.
--	House 7 got 80 presents.
--	House 8 got 150 presents.
--	House 9 got 130 presents.
--
--	The first house gets 10 presents: it is visited only by Elf 1, which delivers 1 * 10 = 10 presents. The fourth house gets
--	70 presents, because it is visited by Elves 1, 2, and 4, for a total of 10 + 20 + 40 = 70 presents.
--
--	What is the lowest house number of the house to get at least as many presents as the number in your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e '100' | cabal run 2015_day20-infinite-elves-and-infinite-houses # 6
--	% cd ../.. && echo -e '33100000' | cabal run 2015_day20-infinite-elves-and-infinite-houses # puzzle ans

factors :: Int -> [Int]
factors m = f m (head primes) (tail primes)
  where f m n ns
          | m < 2 = []
          | m < n ^ 2 = [m]   -- stop early
          | m `mod` n == 0 = n : f (m `div` n) n ns
          | otherwise = f m (head ns) (tail ns)

primes :: [Int]
primes = 2 : filter (\n-> head (factors n) == n) [3,5..]

solve :: Int -> Int
solve t' = (+ 2) . length . takeWhile (< t) . map (deliveries . factors) $ [2..]
  where t = t' `div` 10
        deliveries :: [Int] -> Int
        deliveries = foldl' (\acc ps -> let p = head ps
                                            cnt = length ps
                                        in acc * (p ^ (cnt + 1) - 1) `div` (p - 1)) 1 . group


main :: IO ()
main = interact $ show . solve . (read :: String -> Int) . head . words

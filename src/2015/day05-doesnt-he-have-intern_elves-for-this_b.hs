module Main where
import Data.List (isInfixOf, length)

--	Realizing the error of his ways, Santa has switched to a better model of determining whether a string is naughty or
--	nice. None of the old rules apply, as they are all clearly ridiculous.
--
--	Now, a nice string is one with all of the following properties:
--	* It contains a pair of any two letters that appears at least twice in the string without overlapping, like xyxy (xy) or
--		aabcdefgaa (aa), but not like aaa (aa, but it overlaps).
--	* It contains at least one letter which repeats with exactly one letter between them, like xyx, abcdefeghi (efe), or even aaa.
--
--	For example:
--	* qjhvhtzxzqqjkmpb is nice because is has a pair that appears twice (qj) and a letter that repeats with exactly one letter between them (zxz).
--	* xxyxx is nice because it has a pair that appears twice and a letter that repeats with one between, even though the letters used by each rule overlap.
--	* uurcxstgmygtbstg is naughty because it has a pair (tg) but no repeat with a single letter between them.
--	* ieodomkazucvgmuy is naughty because it has a repeating letter with one between (odo), but no pair that appears twice.
--
--	How many strings are nice under these new rules?
--
-- Test:
--	% cd ../.. && echo -e 'qjhvhtzxzqqjkmpb' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this_b ## 1
--	% cd ../.. && echo -e 'xxyxx' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this_b ## 1
--	% cd ../.. && echo -e 'uurcxstgmygtbstg' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this_b ## 0
--	% cd ../.. && echo -e 'ieodomkazucvgmuy' | cabal run 2015_day05-doesnt-he-have-intern_elves-for-this_b ## 0

solve :: [String] -> Int
solve = length . filter isNice
  where isNice wd              = hasTwoLtrPair wd && hasSandwich wd
        hasTwoLtrPair (m:n:ws) | [m,n] `isInfixOf` ws = True
        hasTwoLtrPair (_:ws)   = hasTwoLtrPair ws
        hasTwoLtrPair _        = False
        hasSandwich (w:_:w':_) | w == w' = True
        hasSandwich (_:ws)     = hasSandwich ws
        hasSandwich _          = False


main :: IO ()
main = interact $ show . solve . lines

module Main where
import Data.List (group, sort, subsequences)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, noneOf, runParser, some)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Find the minimum number of containers that can exactly fit all 150 liters of eggnog. How many different ways can you
--	fill that number of containers and still hold exactly 150 litres?
--
--	In the example above, the minimum number of containers was two. There were three ways to use that many containers, and so
--	the answer there would be 3.
--
-- Test:
--	% cd ../.. && echo -e '20\n15\n10\n5\n5' | cabal run 2015_day17-no-such-thing-as-too-much_b # 3

type Parser = Parsec Void String

containers :: Parser [Int]
containers = some (decimal <* many (noneOf "0123456789")) <* eof

--targetLiters = 25 -- test example
targetLiters = 150

solve :: Either a [Int] -> Int
solve (Right cs) = length . head . group . sort . map length . filter ((targetLiters ==) . sum) . subsequences $ cs


main :: IO ()
main = interact $ show . solve . runParser containers "<stdin>"

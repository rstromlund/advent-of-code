module Main where
import Data.List (nub)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline)

--	He begins by delivering a present to the house at his starting location, and then an elf at the North Pole calls him via
--	radio and tells him where to move next. Moves are always exactly one house to the north (^), south (v), east (>), or west
--	(<). After each move, he delivers another present to the house at his new location.
--
--	However, the elf back at the north pole has had a little too much eggnog, and so his directions are a little off, and
--	Santa ends up visiting some houses more than once. How many houses receive at least one present?
--
--	For example:
--	* > delivers presents to 2 houses: one at the starting location, and one to the east.
--	* ^>v< delivers presents to 4 houses in a square, including twice to the house at his starting/ending location.
--	* ^v^v^v^v^v delivers a bunch of presents to some very lucky children at only 2 houses.

-- Test:
--	% cd ../.. && echo -e '>' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum ## 2
--	% cd ../.. && echo -e '^>v<' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum ## 4
--	% cd ../.. && echo -e '^v^v^v^v^v' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum ## 2

type Parser = Parsec Void String

deliver :: Parser [(Int, Int)] -- (x-dir, y-dir)
deliver = many move <* optional newline <* eof

move :: Parser (Int, Int)
move = choice
  [ (0, 1) <$ char '^'
  , (0,-1) <$ char 'v'
  , (1, 0) <$ char '>'
  , (-1,0) <$ char '<' ]

solve :: Either e [(Int, Int)] -> Int
solve (Right ms) = length . nub . foldl' santa [(0,0)] $ ms
  where santa l@((x,y):_) (xd,yd) = (x+xd, y+yd):l


main :: IO ()
main = interact $ show . solve . runParser deliver "<stdin>"

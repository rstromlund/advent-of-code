{-# OPTIONS_GHC -Wno-incomplete-uni-patterns #-}
module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline)

--	An opening parenthesis, (, means he should go up one floor, and a closing parenthesis, ), means he should go down one floor.
--
--	The apartment building is very tall, and the basement is very deep; he will never find the top or bottom floors.
--
--	For example:
--	* (()) and ()() both result in floor 0.
--	* ((( and (()(()( both result in floor 3.
--	* ))((((( also results in floor 3.
--	* ()) and ))( both result in floor -1 (the first basement level).
--	* ))) and )())()) both result in floor -3.
--
--	To what floor do the instructions take Santa?
--
-- Test:
--	% cd ../.. && echo -e '(())' | cabal run 2015_day01-not-quite-lisp ## 0
--	% cd ../.. && echo -e '()()' | cabal run 2015_day01-not-quite-lisp ## 0
--	% cd ../.. && echo -e '(((' | cabal run 2015_day01-not-quite-lisp ## 3
--	% cd ../.. && echo -e '(()(()(' | cabal run 2015_day01-not-quite-lisp ## 3
--	% cd ../.. && echo -e '())' | cabal run 2015_day01-not-quite-lisp ## -1
--	% cd ../.. && echo -e '))(' | cabal run 2015_day01-not-quite-lisp ## -1
--	% cd ../.. && echo -e ')))' | cabal run 2015_day01-not-quite-lisp ## -3
--	% cd ../.. && echo -e ')())())' | cabal run 2015_day01-not-quite-lisp ## -3

type Parser = Parsec Void String

elevate :: Parser [Int]
elevate = many move <* optional newline <* eof

move :: Parser Int
move = choice
  [ 1    <$ char '('
  , (-1) <$ char ')' ]

solve :: String -> Int
solve ps = sum ns
  where Right ns = runParser elevate "<stdin>" ps


main :: IO ()
main = interact $ show . solve

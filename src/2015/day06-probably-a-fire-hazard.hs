{-# LANGUAGE BangPatterns #-}
module Main where
import Data.Bool (bool)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, runParser, sepBy)
import Text.Megaparsec.Char (char, space, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... you've decided to deploy one million lights in a 1000x1000 grid
--
--	Lights in your grid are numbered from 0 to 999 in each direction; the lights at each corner are at 0,0, 0,999, 999,999,
--	and 999,0. The instructions include whether to turn on, turn off, or toggle various inclusive ranges given as coordinate
--	pairs. Each coordinate pair represents opposite corners of a rectangle, inclusive; a coordinate pair like 0,0 through 2,2
--	therefore refers to 9 lights in a 3x3 square. The lights all start turned off.
--
--	To defeat your neighbors this year, all you have to do is set up your lights by doing the instructions Santa sent you in order.
--
--	For example:
--	* "turn on 0,0 through 999,999"      would turn on (or leave on) every light.
--	* "toggle 0,0 through 999,0"         would toggle the first line of 1000 lights, turning off the ones that were on, and turning on the ones that were off.
--	* "turn off 499,499 through 500,500" would turn off (or leave off) the middle four lights.
--
--	After following the instructions, how many lights are lit?
--
-- Test:
--	% cd ../.. && echo -e 'turn on 0,0 through 999,999' | cabal run 2015_day06-probably-a-fire-hazard ## 1000000
--	% cd ../.. && echo -e 'turn on 0,0 through 999,999\ntoggle 0,0 through 999,0' | cabal run 2015_day06-probably-a-fire-hazard ## 999000
--	% cd ../.. && echo -e 'turn on 0,0 through 999,999\ntoggle 0,0 through 999,0\nturn off 499,499 through 500,500' | cabal run 2015_day06-probably-a-fire-hazard ## 998996

data Action = TurnOn | TurnOff | Toggle deriving (Show, Ord, Eq)
type Square = (Int, Int, Int, Int) -- (xmin, xmax, ymin, ymax)
type Step = (Action, Square) -- (1=on|0=off, Square)

actionVal TurnOn  = 1
actionVal TurnOff = -1
actionVal Toggle  = 0

type Parser = Parsec Void String

instructions :: Parser Step
instructions = do
  a <- action
  _ <- space
  [x0, y0] <- coords
  _ <- string " through "
  [x1, y1] <- coords
  eof
  return (a, (x0, y0, x1, y1))

action :: Parser Action
action = choice
  [ TurnOn  <$ string "turn on"
  , TurnOff <$ string "turn off"
  , Toggle  <$ string "toggle" ]

coords :: Parser [Int]
coords = decimal `sepBy` char ','

-- Just run the x-es and y-es and calculate each pixel in the instruction "stack" (just 300 lines); seems simpler than allocating
-- a huge vector and running in a mutable vector monad; or trying to stack and intersect rectangles. (?)
solve :: Either a [Step] -> Int
solve (Right is) = foldl' stepX 0 [0..999]
  where stepX !acc !x = foldl' stepY acc [0..999]
          where stepY !acc !y = acc + getVal x y
        getVal !x !y = foldl' getVal' 0 is
          where getVal' acc (act, (x0, y0, x1, y1))
                  | x > x1 || x < x0 || y > y1 || y < y0 = acc -- doesnt apply
                  | TurnOn  == act = 1
                  | TurnOff == act = 0
                  | Toggle  == act = bool 0 1 $ 0 == acc


main :: IO ()
main = interact $ show . solve . mapM (runParser instructions "<stdin>") . lines

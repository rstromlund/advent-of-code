module Main where
import Data.Array.ST (newArray, readArray, runSTUArray, writeArray)
import Data.Array.Unboxed ((!))

--	... The Elves decide they don't want to visit an infinite number of houses. Instead, each Elf will stop after delivering
--	presents to 50 houses. To make up for it, they decide to deliver presents equal to eleven times their number at each
--	house.
--
--	With these changes, what is the new lowest house number of the house to get at least as many presents as the number in
--	your puzzle input?
--
-- Test:
--	% cd ../.. && echo -e '100' | cabal run 2015_day20-infinite-elves-and-infinite-houses_b # 6
--	% cd ../.. && echo -e '33100000' | cabal run 2015_day20-infinite-elves-and-infinite-houses_b # puzzle ans

maxHouse = 1000000 :: Int

solve t = runSTUArray (
  do
    arr <- newArray (0, maxHouse) (0 :: Int)
    search arr 1
    return arr
  ) ! 0
  where search arr n = do
          deliver 50
          val <- readArray arr n
          if val >= t then writeArray arr 0 n else search arr (n + 1)
          --
          where
            deliver 0 = pure ()
            deliver h = do
              let pos = n * h
              if pos <= 1000000
                then do val <- readArray arr pos
                        writeArray arr pos (n * 11 + val)
                else pure ()
              deliver (h - 1)


main :: IO ()
main = interact $ show . solve . (read :: String -> Int) . head . words

module Main where
import Data.Array (Array, bounds, listArray, (!))
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, many, oneOf, optional, runParser, (<|>))
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	The unknown benefactor is very thankful for releasi-- er, helping little Jane
--	Marie with her computer. Definitely not to distract you, what is the value in
--	register b after the program is finished executing if register a starts as 1
--	instead?
--
-- Test:
--	% cd ../.. && echo -e 'inc a\njio a, +2\ntpl a\ninc a' | cabal run 2015_day23-opening-the-turing-lock_b ## ?

data IS -- Instruction Set
  = Hlf Char
  | Tpl Char
  | Inc Char
  | Jmp Int
  | Jie Char Int
  | Jio Char Int
  deriving (Show)

data CPU = CPU { regA :: !Int
               , regB :: !Int
               , iptr   :: !Int
               }
           deriving (Show)

type Parser = Parsec Void String

instrs :: Parser (Array Int IS)
instrs = mkra <$> many (instr <* optional newline) <* eof
  where mkra :: [IS] -> Array Int IS
        mkra iss = listArray (0, length iss - 1) iss

instr :: Parser IS
instr = (string "hlf " >> Hlf <$> oneOf "ab")
    <|> (string "tpl " >> Tpl <$> oneOf "ab")
    <|> (string "inc " >> Inc <$> oneOf "ab")
    <|> (string "jmp " >> Jmp <$> signed    )
    <|> (string "jie " >> Jie <$> oneOf "ab" <* string ", " <*> signed)
    <|> (string "jio " >> Jio <$> oneOf "ab" <* string ", " <*> signed)
  where signed :: Parser Int
        signed = f <$> oneOf "+-" <*> decimal
          where f '+' x = x
                f _   x = negate x

--solve :: Either a (Array Int IS) -> Int
solve (Right instrs) = regB $ exec CPU{regA = 1, regB = 0, iptr = 0}
  where (imin, imax) = bounds instrs
        get 'a' = regA
        get 'b' = regB
        set 'a' r cpu = cpu{regA = max 0 r}
        set 'b' r cpu = cpu{regB = max 0 r}
        exec :: CPU -> CPU
        exec cpu@CPU{iptr = ip}
          | ip > imax = cpu
          | otherwise = exec . exec' $ instrs ! ip
          where exec' :: IS -> CPU
                exec' (Hlf reg)     = (set reg (get reg cpu `div` 2) cpu){iptr = ip + 1}
                exec' (Tpl reg)     = (set reg (get reg cpu *     3) cpu){iptr = ip + 1}
                exec' (Inc reg)     = (set reg (get reg cpu +     1) cpu){iptr = ip + 1}
                exec' (Jmp off)     = cpu{iptr = ip + off}
                exec' (Jie reg off) = if even $ get reg cpu then cpu{iptr = ip + off} else cpu{iptr = ip + 1}
                exec' (Jio reg off) = if   1 == get reg cpu then cpu{iptr = ip + off} else cpu{iptr = ip + 1}


main :: IO ()
main = interact $ show . solve . runParser instrs "<stdin>"

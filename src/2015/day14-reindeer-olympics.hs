module Main where
import Data.List (maximum)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Reindeer can only either be flying (always at their top speed) or resting (not moving at all), and always spend whole seconds in either state.
--
--	For example, suppose you have the following Reindeer:
--	* Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.
--	* Dancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.
--
--	After one second, Comet has gone 14 km, while Dancer has gone 16 km. After ten seconds, Comet has gone 140 km, while
--	Dancer has gone 160 km. On the eleventh second, Comet begins resting (staying at 140 km), and Dancer continues on for a
--	total distance of 176 km. On the 12th second, both reindeer are resting. They continue to rest until the 138th second,
--	when Comet flies for another ten seconds. On the 174th second, Dancer flies for another 11 seconds.
--
--	In this example, after the 1000th second, both reindeer are resting, and Comet is in the lead at 1120 km (poor Dancer has
--	only gotten 1056 km by that point). So, in this situation, Comet would win (if the race ended at 1000 seconds).
--
--	Given the descriptions of each reindeer (in your puzzle input), after exactly 2503 seconds, what distance has the winning
--	reindeer traveled?
--
-- Test:
--	% cd ../.. && echo -e 'Comet can fly 14 km/s for 10 seconds, but then must rest for 127 seconds.\nDancer can fly 16 km/s for 11 seconds, but then must rest for 162 seconds.' | cabal run 2015_day14-reindeer-olympics # 1120 (for 1000 seconds)

type Stat = (String, Int, Int, Int) -- reindeer, speed, dur, rest

type Parser = Parsec Void String

totDur = 2503
--totDur = 1000 -- for example above.
--totDur =    1 -- for example debugging: [14,16]
--totDur =   12 -- for example debugging: [140,176]
--totDur =  138 -- for example debugging: [154,176]
--totDur =  174 -- for example debugging: [280,192]

stats :: Parser [Stat]
stats = many stat <* eof

stat :: Parser Stat
stat = (,,,) <$> reindeer <* string " can fly " <*> decimal <* string " km/s for " <*> decimal <* string " seconds, but then must rest for " <*> decimal <* string " seconds." <* optional newline
  where reindeer = choice [ string "Dancer",  string "Cupid",   string "Rudolph", string "Donner", string "Dasher"
                          , string "Blitzen", string "Prancer", string "Comet",   string "Vixen"]

solve :: Either a [Stat] -> Int
solve (Right ss) = maximum . map (fly 0 1 True) $ ss
  where fly :: Int -> Int -> Bool -> Stat -> Int
        fly dist tm _ _
          | tm > totDur = dist
        fly dist tm flying r@(_, spd, dur, rest)
          | flying     = let x = min dur (totDur - tm + 1)
                         in fly (dist + spd * x) (tm + x) False r
          | not flying = fly dist (tm + rest) True r


main :: IO ()
main = interact $ show . solve . runParser stats "<stdin>"

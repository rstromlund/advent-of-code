module Main where
import Data.Bifunctor (bimap)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, noneOf, oneOf, runParser)

--	Now, let's go the other way. In addition to finding the number of characters of code, you should now encode each code
--	representation as a new string and find the number of characters of the new encoded representation, including the
--	surrounding double quotes.
--
--	For example:
--	* "" encodes to "\"\"", an increase from 2 characters to 6.
--	* "abc" encodes to "\"abc\"", an increase from 5 characters to 9.
--	* "aaa\"aaa" encodes to "\"aaa\\\"aaa\"", an increase from 10 characters to 16.
--	* "\x27" encodes to "\"\\x27\"", an increase from 6 characters to 11.
--
--	Your task is to find the total number of characters to represent the newly encoded strings minus the number of characters
--	of code in each original string literal. For example, for the strings above, the total encoded length (6 + 9 + 16 + 11 =
--	42) minus the characters in the original code representation (23, just like in the first part of this puzzle) is 42 - 23 = 19.
--
-- Test:
--	% cd ../.. && echo -e '""\n"abc"\n"aaa\\"aaa"\n"\\x27"' | cabal run 2015_day08-matchsticks_b ## 19

-- NOTE: this would be simpler w/ just a fold or concatMap, but my goal is to learn parsing : )

type Parser = Parsec Void String

aocString :: Parser String -- not adding a leading and trailing quote, just add 2 chars per parsed string
aocString = concat <$> many unescapedChar <* eof

unescapedChar :: Parser String
unescapedChar = (:"") <$> noneOf "\"\\"
            <|> (oneOf  "\"\\" >>= (\c -> pure ['\\',c]))

parse ss = zip <$> mapM (runParser aocString "<stdin>") ss <*> Right ss

solve :: Either a [(String, String)] -> Int
solve (Right ps) = sum . map ((+ 2) . uncurry (-) . bimap length length) $ ps


main :: IO ()
main = interact $ show . solve . parse . lines

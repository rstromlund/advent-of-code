module Main where
import Data.List (foldr)
import Data.Set (Set, deleteFindMin, empty, insert, notMember, singleton)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, optional, runParser)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	At the start of each player turn (before any other effects apply), you lose 1 hit point. If this brings you to or below 0 hit points, you lose.
--	With the same starting stats for you and the boss, what is the least amount of mana you can spend and still win the fight?


data Combatant = Combatant{ hitPoints :: !Int
                          , armor     :: !Int
                          , mana      :: !Int
                          , damage    :: !Int
                          } deriving (Show, Ord, Eq)

mkCombatant hp dmg = Combatant{hitPoints = hp, armor = 0, mana = maxBound, damage = dmg}

you = Combatant{ hitPoints =  10
               , armor     =   0
               , mana      = 250
               , damage    =   0
               }

type Parser = Parsec Void String

boss :: Parser Combatant
boss = string "Hit Points: " >> mkCombatant <$> decimal <* string "\nDamage: " <*> decimal <* optional newline <* eof

-- (mana cost, damage, healing, eShield, ePoison, eRecharge)
spells = [ ( 53, 4, 0, 0, 0, 0) -- Magic Missile
         , ( 73, 2, 2, 0, 0, 0) -- Drain
         , (113, 0, 0, 6, 0, 0) -- Shield
         , (173, 0, 0, 0, 6, 0) -- Poison
         , (229, 0, 0, 0, 0, 5) -- Recharge
         ]

data GameState = GameState{ manaSpent :: !Int
                          , p1        :: !Combatant
                          , p2        :: !Combatant
                          , eShield   :: !Int
                          , ePoison   :: !Int
                          , eRecharge :: !Int
                          } deriving (Eq, Ord, Show)

deadCombatant Combatant{mana = m, hitPoints = hp} = hp <= 0 || m < 53 -- need at least 1 hit point and mana enough for cheapest spell

validSpell _ s@(_,_,_,0,0,0) = True -- no lasting effect, valid to cast
validSpell GameState{eShield = gssh, ePoison = gspo, eRecharge = gsre} s@(_,_,_, spsh, sppo, spre)
  | gssh > 1 && 0 /= spsh = False
  | gspo > 1 && 0 /= sppo = False
  | gsre > 1 && 0 /= spre = False
  | otherwise             = True

applyEffects :: GameState -> GameState
applyEffects gs@GameState{p1=you, p2=him, eShield = gssh, ePoison = gspo, eRecharge = gsre} = gs'
  where you' = you { armor = if gssh > 0 then 7 else 0 -- shield adds 7 armor while active
                   , mana = mana you + if gsre > 0 then 101 else 0 -- recharge adds 101 mana while active
                   }
        him' = him { hitPoints = hitPoints him - if gspo > 0 then 3 else 0 -- poison deals 3 damage while active
                   }
        gs' = gs { p1 = you'
                 , p2 = him'
                 , eShield   = max 0 $ gssh - 1
                 , ePoison   = max 0 $ gspo - 1
                 , eRecharge = max 0 $ gsre - 1
                 }

cast :: (Int, Int, Int, Int, Int, Int) -> GameState -> GameState
cast (sCost, sDmg, sHeal, spsh, sppo, spre) gs@GameState{p1 = op1@Combatant{hitPoints = p1hp}}
  -- This guard patch is pretty ugly ... but it works : )
  | p1hp <= 1 = gs{p1 = op1{hitPoints = p1hp - 1}} -- he's dead jim; couldn't get a spell off
  | otherwise = gs'
  where GameState{manaSpent = ms, p1 = you@Combatant{hitPoints = yhp, mana = ym}, p2 = him@Combatant{hitPoints = hhp}, eShield = gssh, ePoison = gspo, eRecharge = gsre} = applyEffects gs
        you' = you { mana      = ym  - sCost
                   , hitPoints = yhp - 1 + sHeal
                   }
        him' = him { hitPoints = hhp - sDmg
                   }
        --
        gs' = GameState{ manaSpent = ms + sCost
                       , p1        = you'
                       , p2        = him'
                       , eShield   = gssh + spsh
                       , ePoison   = gspo + sppo
                       , eRecharge = gsre + spre
                       }

attack gs@GameState{p1 = Combatant{hitPoints = p1hp}, p2 = op2@Combatant{hitPoints = p2hp}}
  -- This guard patch is pretty ugly ... but it works : )
  | p1hp <= 0 = gs
  | p2hp <= 1 = gs{p2 = op2{hitPoints = p2hp - 1}} -- he's dead jim; couldn't attack us
  | otherwise = gs' {p1 = you'}
  where gs'@GameState{p1 = you, p2 = him} = applyEffects gs
        you' = you{hitPoints = hitPoints you - max 1 (damage him - armor you)}
        him' = him{hitPoints = hitPoints him - 1}

dijkstra :: Set GameState -> Set GameState -> GameState
dijkstra gss ckd | deadCombatant $ p2 gs = gs                -- boss died!  We won in this game state : )
                 | deadCombatant $ p1 gs = dijkstra gss' ckd -- we died :( ... drop game state and continue search
                 | otherwise = dijkstra gss'' ckd'           -- ho. haha. guard. turn. parry. dodge. spin. ha. thrust!, etc...
  where (gs, gss') = deleteFindMin gss
        ckd'    = insert gs ckd
        gss''   = foldr insert gss' $ filter (`notMember` ckd') $ next gs
        next gs = map (round gs) spells -- cast all (valid) spells at this game state
        round gs s
          | validSpell gs s = attack . cast s $ gs
          | otherwise       = gs

solve :: Either a Combatant -> Int
solve (Right boss) = manaSpent $ dijkstra (singleton gs) empty
  where you = Combatant{hitPoints = 50, armor = 0, mana = 500, damage = 0}
        gs = GameState{manaSpent = 0, p1 = you, p2 = boss, eShield = 0, ePoison = 0, eRecharge = 0}


main :: IO ()
main = interact $ show . solve . runParser boss "<stdin>"

module Main where
import Data.List (last, maximum, nub, permutations, tails, take)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... So, add yourself to the list, and give all happiness relationships that involve you a score of 0.
--
--	What is the total change in happiness for the optimal seating arrangement that actually includes yourself?
--
-- Test:
--	% ./day13-knights-of-the-dinner-table_b.hs < 13a.tst # 286

type Scenario = (String, Int, String)

type Parser = Parsec Void String

preferences :: Parser [Scenario]
preferences = many preference <* eof

preference :: Parser Scenario
preference = (,,) <$> (actor <* string " would ") <*> score <* string " happiness units by sitting next to " <*> actor <* char '.' <* optional newline
  where actor = choice [ string "Alice"
                       , string "Bob"
                       , string "Carol"
                       , string "David"
                       , string "Eric"
                       , string "Frank"
                       , string "George"
                       , string "Mallory"
                       ]
        score = f <$> choice [string "lose ", string "gain "] <*> decimal
          where f ('l':_) s = -s
                f ('g':_) s = s

solve :: Either a [Scenario] -> Int
solve (Right ss') = maximum . map (foldl' (\acc (_, s, _) -> acc + s) 0 . mkScenarios . filter ((==) 2 . length) . map (take 2) . tails) . permutations $ actors
  where actors' = nub . map (\(a1, _, _) -> a1) $ ss'
        actors = "Me":actors'
        ss = ss' ++ concatMap (\a -> [("Me", 0, a), (a, 0, "Me")]) actors'
        mkScenarios :: [[String]] -> [Scenario]
        mkScenarios as = findScenario [head . head $ as, last . last $ as] ++ concatMap findScenario as
        findScenario [a1, a2] = filter (\(s1, _, s2) -> (a1 == s1 && a2 == s2) || (a1 == s2 && a2 == s1)) ss


main :: IO ()
main = interact $ show . solve . runParser preferences "<stdin>"

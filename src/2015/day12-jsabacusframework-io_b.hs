module Main where
import Data.Bool (bool)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, many, noneOf, optional, runParser, sepBy1)
import Text.Megaparsec.Char (char)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Ignore any object (and all of its children) which has any property with the value "red". Do this only for objects ({...}), not arrays ([...]).
--
--	* [1,2,3] still has a sum of 6.
--	* [1,{"c":"red","b":2},3] now has a sum of 4, because the middle object is ignored.
--	* {"d":"red","e":[1,2,3,4],"f":5} now has a sum of 0, because the entire structure is ignored.
--	* [1,"red",5] has a sum of 6, because "red" in an array has no effect.
--
-- Test:
--	% cd ../.. && echo -e '[1,2,3]' | cabal run 2015_day12-jsabacusframework-io_b ## 6
--	% cd ../.. && echo -e '[1,{"c":"red","b":2},3]' | cabal run 2015_day12-jsabacusframework-io_b ## 4
--	% cd ../.. && echo -e '{"d":"red","e":[1,2,3,4],"f":5}' | cabal run 2015_day12-jsabacusframework-io_b ## 0
--	% cd ../.. && echo -e '[1,"red",5]' | cabal run 2015_day12-jsabacusframework-io_b ## 6

type Parser = Parsec Void String

data JSON = JNumber Int
          | JString String
          | JArray [JSON]
          | JObject [(JSON, JSON)]
  deriving (Show)

json :: Parser JSON
json = jnumber <|> jstring <|> jarray <|> jobject

jnumber :: Parser JSON
jnumber = JNumber <$> (f <$> optional (char '-') <*> decimal)
  where f Nothing x = x
        f _       x = negate x

jstring :: Parser JSON
jstring = JString <$> (char '"' *> many (noneOf "\"") <* char '"')

jarray :: Parser JSON
jarray = JArray <$> (char '[' *> json `sepBy1` char ',' <* char ']')

jobject :: Parser JSON
jobject = JObject <$> (char '{' *> kv `sepBy1` char ',' <* char '}')
  where kv = (,) <$> (jstring <* char ':') <*> json

solve :: Either a JSON -> Int
solve (Right js) = traverse 0 js
  where traverse :: Int -> JSON -> Int
        traverse acc (JNumber n)      = acc + n
        traverse acc (JString _)      = acc
        traverse acc (JArray [])      = acc
        traverse acc (JArray (j:js))  = traverse (traverse acc j) (JArray js)
        -- If we peel apart a JObject like a JArray, we'd compute redundant 'hasRed'-es.  So use fold and consume the non-red values.
        traverse acc (JObject js)     = bool (foldl' (\acc' (_,v) -> traverse acc' v) acc js) acc $ hasRed js
        --
        hasRed :: [(JSON, JSON)] -> Bool
        hasRed js = not . null $ [k | (k, JString "red") <- js]


main :: IO ()
main = interact $ show . solve . runParser json "<stdin>" . head . lines

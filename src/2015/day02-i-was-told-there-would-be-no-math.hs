module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, sepBy)
import Text.Megaparsec.Char (char)
import Text.Megaparsec.Char.Lexer (decimal)

--	.. find the surface area of the box, which is 2*l*w + 2*w*h + 2*h*l. The elves also need a little extra paper for each present: the area of the smallest side.
--
--	For example:
--
--	* A present with dimensions 2x3x4 requires 2*6 + 2*12 + 2*8 = 52 square feet of wrapping paper plus 6 square feet of slack, for a total of 58 square feet.
--	* A present with dimensions 1x1x10 requires 2*1 + 2*10 + 2*10 = 42 square feet of wrapping paper plus 1 square foot of slack, for a total of 43 square feet.
--
--	All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?
--
-- Test:
--	% cd ../.. && echo -e '2x3x4' | cabal run 2015_day02-i-was-told-there-would-be-no-math ## 58
--	% cd ../.. && echo -e '1x1x10' | cabal run 2015_day02-i-was-told-there-would-be-no-math ## 43
--	% cd ../.. && echo -e '2x3x4\n1x1x10' | cabal run 2015_day02-i-was-told-there-would-be-no-math ## 101

type Parser = Parsec Void String

-- Two possibilities for tuple return types (monadic and applicative):

--dim :: Parser (Int, Int, Int) -- length, width, height
--dim = do
--  l <- decimal
--  _ <- char 'x'
--  w <- decimal
--  _ <- char 'x'
--  h <- decimal
--  return (l,w,h)

--dim :: Parser (Int, Int, Int) -- length, width, height
--dim = (,,) <$> decimal <* char 'x' <*> decimal <* char 'x' <*> decimal

-- This list return type is shorter and probably more readable:

dim :: Parser [Int] -- length, width, height
dim = decimal `sepBy` char 'x' <* eof

solve :: Either e [[Int]] -> Int
solve (Right bs) = sum . map area $ bs
  where area [l,w,h] = 2 * sum sides + minimum sides
          where sides = [l*w, w*h, l*h]

main :: IO ()
main = interact $ show . solve . mapM (runParser dim "<stdin>") . lines

module Main where
import Data.List (elem, filter, )
import Data.Map.Strict (Map, elems, empty, findWithDefault, foldlWithKey', fromList, union, singleton, (!?))

--	... You arrange them (lights) in a 100x100 grid.
--
--	Never one to let you down, Santa again mails you instructions on the ideal lighting configuration. With so few lights, he
--	says, you'll have to resort to animation.
--
--	Start by setting your lights to the included initial configuration (your puzzle input). A # means "on", and a . means "off".
--
--	Then, animate your grid in steps, where each step decides the next configuration based on the current one. Each light's
--	next state (either on or off) depends on its current state and the current states of the eight lights adjacent to it
--	(including diagonals). Lights on the edge of the grid might have fewer than eight neighbors; the missing ones always count
--	as "off".
--
--	For example, in a simplified 6x6 grid, the light marked A has the neighbors numbered 1 through 8, and the light marked B,
--	which is on an edge, only has the neighbors marked 1 through 5:
--	* 1B5...
--	* 234...
--	* ......
--	* ..123.
--	* ..8A4.
--	* ..765.
--
--	The state a light should have next is based on its current state (on or off) plus the number of neighbors that are on:
--	* A light which is on stays on when 2 or 3 neighbors are on, and turns off otherwise.
--	* A light which is off turns on if exactly 3 neighbors are on, and stays off otherwise.
--
--	All of the lights update simultaneously; they all consider the same current state before moving to the next.
--
--	Here's a few steps from an example configuration of another 6x6 grid:
--
--	Initial state:
--	.#.#.#
--	...##.
--	#....#
--	..#...
--	#.#..#
--	####..
--
--	After 1 step:
--	..##..
--	..##.#
--	...##.
--	......
--	#.....
--	#.##..
--
--	After 2 steps:
--	..###.
--	......
--	..###.
--	......
--	.#....
--	.#....
--
--	After 3 steps:
--	...#..
--	......
--	...#..
--	..##..
--	......
--	......
--
--	After 4 steps:
--	......
--	......
--	..##..
--	..##..
--	......
--	......
--
--	After 4 steps, this example has four lights on.
--
--	In your grid of 100x100 lights, given your initial configuration, how many lights are on after 100 steps?
--
-- Test:
--	% cd ../.. && echo -e '.#.#.#\n...##.\n#....#\n..#...\n#.#..#\n####..' | cabal run 2015_day18-like-a-gif-for-your-yard # 4

stepCnt = 100
-- stepCnt = 4 -- for test case

gridDim = 100
-- gridDim = 6 -- for test case

neighbors = [ (-1, -1), (-1, 0), (-1, 1)
            , ( 0, -1),          ( 0, 1)
            , ( 1, -1), ( 1, 0), ( 1, 1)
            ]

-- Part 1: uses a sparse map and loops over this keys value; but it doesn't seem to save much time.
-- Part 2: uses a map for the full field. A vector of vectors would probably perform better.  Oh yeah, arrays can be index with tuples, so that'd be better.
keys = [(y, x) | y <- [0..(gridDim - 1)], x <- [0..(gridDim - 1)]]

solve :: String -> Int
solve cs = length . filter ('#' ==) . elems . (!! stepCnt) . iterate step $ initialState
  where initialState = fromList . zip keys . filter (`elem` ".#") $ cs
        step :: Map (Int, Int) Char -> Map (Int, Int) Char
        step old = foldl' (\acc k -> step' acc k $ old !? k) empty keys
          where step' :: Map (Int, Int) Char -> (Int, Int) -> Maybe Char -> Map (Int, Int) Char
                step' acc k@(y, x) v = generate v . length . filter ('#' == ) . map getChar $ neighbors
                  where getChar (dy, dx) = findWithDefault '.' (y+dy, x+dx) old
                        generate (Just '#') n
                          | n >= 2 && n <= 3 = union acc $ singleton k '#'
                          | otherwise        = acc
                        generate _ n
                          | n == 3    = union acc $ singleton k '#'
                          | otherwise = acc


main :: IO ()
main = interact $ show . solve

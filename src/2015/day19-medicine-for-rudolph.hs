module Main where
import Data.List (isPrefixOf, last, maximum, nub, stripPrefix)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... So, add yourself to the list, and give all happiness relationships that involve you a score of 0.
--
--	What is the total change in happiness for the optimal seating arrangement that actually includes yourself?
--
-- Test:
--	% cd ../.. && echo -e 'H => HO\nH => OH\nO => HH\n\nHOH' | cabal run 2015_day19-medicine-for-rudolph # 4
--	% cd ../.. && echo -e 'H => HO\nH => OH\nO => HH\n\nHOHOHO' | cabal run 2015_day19-medicine-for-rudolph # 7

type Replacement = (String, String)
type Calibration = ([Replacement], String) -- replacement pairs, molecule

type Parser = Parsec Void String

calibration :: Parser Calibration
calibration = (,) <$> many replacement <* newline <*> some letterChar <* optional newline <* eof

replacement :: Parser Replacement
replacement = (,) <$> some letterChar <* string " => " <*> some letterChar <* newline

solve :: Either a Calibration -> Int
solve (Right (rs, mol)) = length . nub . concatMap repl $ rs
  where repl (key, val) = repl' [] mol
          where repl' left ms@(m:ms')
                  | key `isPrefixOf` ms = let (Just right) = stripPrefix key ms
                                          in (left ++ val ++ right):repl' (left ++ key) right
                  | otherwise           = repl' (left ++ [m]) ms'
                repl' left []           = []


main :: IO ()
main = interact $ show . solve . runParser calibration "<stdin>"

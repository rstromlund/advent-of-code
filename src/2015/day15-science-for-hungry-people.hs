module Main where
import Data.List (maximum, transpose, zipWith)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Your recipe leaves room for exactly 100 teaspoons of ingredients. You make a list of the remaining ingredients you
--	could use to finish the recipe (your puzzle input) and their properties per teaspoon:
--
--	* capacity (how well it helps the cookie absorb milk)
--	* durability (how well it keeps the cookie intact when full of milk)
--	* flavor (how tasty it makes the cookie)
--	* texture (how it improves the feel of the cookie)
--	* calories (how many calories it adds to the cookie)
--
--	You can only measure ingredients in whole-teaspoon amounts accurately, and you have to be accurate so you can reproduce
--	your results in the future. The total score of a cookie can be found by adding up each of the properties (negative totals
--	become 0) and then multiplying together everything except calories.
--
--	For instance, suppose you have these two ingredients:
--
--	Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8
--	Cinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3
--
--	Then, choosing to use 44 teaspoons of butterscotch and 56 teaspoons of cinnamon (because the amounts of each ingredient
--	must add up to 100) would result in a cookie with the following properties:
--
--	* A capacity of 44*-1 + 56*2 = 68
--	* A durability of 44*-2 + 56*3 = 80
--	* A flavor of 44*6 + 56*-2 = 152
--	* A texture of 44*3 + 56*-1 = 76
--
--	Multiplying these together (68 * 80 * 152 * 76, ignoring calories for now) results in a total score of 62842880, which
--	happens to be the best score possible given these ingredients. If any properties had produced a negative total, it would
--	have instead become zero, causing the whole score to multiply to zero.
--
--	Given the ingredients in your kitchen and their properties, what is the total score of the highest-scoring cookie you can make?
--
-- Test:
--	% cd ../.. && echo -e 'Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\nCinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3' | cabal run 2015_day15-science-for-hungry-people # 62842880

type Recipe = (String, [Int], Int) -- ingredient, [capacity, durability, flavor, texture], calories
type Parser = Parsec Void String

recipes :: Parser [Recipe]
recipes = many recipe <* eof

recipe :: Parser Recipe
recipe = mkRecipe <$> ingredient
  <* string ": capacity " <*> value
  <* string ", durability " <*> value
  <* string ", flavor " <*> value
  <* string ", texture " <*> value
  <* string ", calories " <*> value
  <* optional newline
  where ingredient = choice [string "Frosting", string "Candy", string "Butterscotch", string "Sugar", string "Cinnamon"]
        value = f <$> optional (char '-') <*> decimal
          where f (Just '-') s = -s
                f Nothing    s = s
        mkRecipe ing cap dur flav txt cal = (ing, [cap, dur, flav, txt], cal)

solve :: Either a [Recipe] -> Int
solve (Right rs) = maximum . map scenario $ tspPermutaions
  where tspPermutaions = [[t1, t2, t3, 100 - t1 - t2 - t3] | t1 <- [1..99], t2 <- [1..99], t3 <- [1..99], t1 + t2 + t3 < 100]
        -- ^^^ hardcoded to my input of 4 ingredients; produce #combinations based on input and not hardcoding.
        --
        scenario tsps = product . map (max 0 . sum) . transpose . zipWith score tsps $ rs
        score :: Int -> Recipe -> [Int]
        score tsp (_, as@[cap, dur, flav, txt], _) = map (tsp *) as
        add :: [Int] -> [Int] -> [Int]
        add = zipWith (\i1 i2 -> max 0 $ i1 + i2)


main :: IO ()
main = interact $ show . solve . runParser recipes "<stdin>"

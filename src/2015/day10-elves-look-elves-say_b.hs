module Main where
import Data.Char (intToDigit)
import Data.List (group)

--	... For example, 211 is read as "one two, two ones", which becomes 1221 (1 2, 2 1s).
--
--	Look-and-say sequences are generated iteratively, using the previous value as input for the next step. For each step, take
--	the previous value, and replace each run of digits (like 111) with the number of digits (3) followed by the digit itself
--	(1).
--
--	For example:
--	* 1 becomes 11 (1 copy of digit 1).
--	* 11 becomes 21 (2 copies of digit 1).
--	* 21 becomes 1211 (one 2 followed by one 1).
--	* 1211 becomes 111221 (one 1, one 2, and two 1s).
--	* 111221 becomes 312211 (three 1s, two 2s, and one 1).
--
--	Starting with the digits in your puzzle input, apply this process 40 times. What is the length of the result?
--
--	Your puzzle input is 1113122113.
--
-- Test:
--	% cd ../.. && echo -e '1' | cabal run 2015_day10-elves-look-elves-say ## 11
--	% cd ../.. && echo -e '11' | cabal run 2015_day10-elves-look-elves-say ## 21
--	% cd ../.. && echo -e '21' | cabal run 2015_day10-elves-look-elves-say ## 1211
--	% cd ../.. && echo -e '1211' | cabal run 2015_day10-elves-look-elves-say ## 111221
--	% cd ../.. && echo -e '111221' | cabal run 2015_day10-elves-look-elves-say ## 312211

solve :: String -> Int
solve = length . (!! 50) . iterate step
  where step = concatMap (\s -> [intToDigit . length $ s, head s]) . group


main :: IO ()
main = interact $ show . solve . head . lines

module Main where
import Data.List (foldr)
import Data.Set (Set, deleteFindMin, empty, insert, notMember, singleton)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, optional, runParser)
import Text.Megaparsec.Char (newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... On each of your turns, you must select one of your spells to cast. If you cannot afford to cast any spell, you
--	lose. Spells cost mana; you start with 500 mana, but have no maximum limit. You must have enough mana to cast a spell, and
--	its cost is immediately deducted when you cast it. Your spells are Magic Missile, Drain, Shield, Poison, and Recharge.
--
--	* Magic Missile costs 53 mana. It instantly does 4 damage.
--	* Drain costs 73 mana. It instantly does 2 damage and heals you for 2 hit points.
--	* Shield costs 113 mana. It starts an effect that lasts for 6 turns. While it is active, your armor is increased by 7.
--	* Poison costs 173 mana. It starts an effect that lasts for 6 turns. At the start of each turn while it is active, it deals the boss 3 damage.
--	* Recharge costs 229 mana. It starts an effect that lasts for 5 turns. At the start of each turn while it is active, it gives you 101 new mana.
--
--	Effects all work the same way. Effects apply at the start of both the player's turns and the boss' turns. Effects are
--	created with a timer (the number of turns they last); at the start of each turn, after they apply any effect they have,
--	their timer is decreased by one. If this decreases the timer to zero, the effect ends. You cannot cast a spell that would
--	start an effect which is already active. However, effects can be started on the same turn they end.


data Combatant = Combatant{ hitPoints :: !Int
                          , armor     :: !Int
                          , mana      :: !Int
                          , damage    :: !Int
                          } deriving (Show, Ord, Eq)

mkCombatant hp dmg = Combatant{hitPoints = hp, armor = 0, mana = maxBound, damage = dmg}

you = Combatant{ hitPoints =  10
               , armor     =   0
               , mana      = 250
               , damage    =   0
               }

type Parser = Parsec Void String

boss :: Parser Combatant
boss = string "Hit Points: " >> mkCombatant <$> decimal <* string "\nDamage: " <*> decimal <* optional newline <* eof

-- (mana cost, damage, healing, eShield, ePoison, eRecharge)
spells = [ ( 53, 4, 0, 0, 0, 0) -- Magic Missile
         , ( 73, 2, 2, 0, 0, 0) -- Drain
         , (113, 0, 0, 6, 0, 0) -- Shield
         , (173, 0, 0, 0, 6, 0) -- Poison
         , (229, 0, 0, 0, 0, 5) -- Recharge
         ]

data GameState = GameState{ manaSpent :: !Int
                          , p1        :: !Combatant
                          , p2        :: !Combatant
                          , eShield   :: !Int
                          , ePoison   :: !Int
                          , eRecharge :: !Int
                          } deriving (Eq, Ord, Show)

deadCombatant Combatant{mana = m, hitPoints = hp} = hp <= 0 || m < 53 -- need at least 1 hit point and mana enough for cheapest spell

validSpell _ s@(_,_,_,0,0,0) = True -- no lasting effect, valid to cast
validSpell GameState{eShield = gssh, ePoison = gspo, eRecharge = gsre} s@(_,_,_, spsh, sppo, spre)
  | gssh > 1 && 0 /= spsh = False
  | gspo > 1 && 0 /= sppo = False
  | gsre > 1 && 0 /= spre = False
  | otherwise             = True

applyEffects :: GameState -> GameState
applyEffects gs@GameState{p1=you, p2=him, eShield = gssh, ePoison = gspo, eRecharge = gsre} = gs'
  where you' = you { armor = if gssh > 0 then 7 else 0 -- shield adds 7 armor while active
                   , mana = mana you + if gsre > 0 then 101 else 0 -- recharge adds 101 mana while active
                   }
        him' = him { hitPoints = hitPoints him - if gspo > 0 then 3 else 0 -- poison deals 3 damage while active
                   }
        gs' = gs { p1 = you'
                 , p2 = him'
                 , eShield   = max 0 $ gssh - 1
                 , ePoison   = max 0 $ gspo - 1
                 , eRecharge = max 0 $ gsre - 1
                 }

cast :: (Int, Int, Int, Int, Int, Int) -> GameState -> GameState
cast (sCost, sDmg, sHeal, spsh, sppo, spre) gs = gs'
  where GameState{manaSpent = ms, p1 = you@Combatant{hitPoints = yhp, mana = ym}, p2 = him@Combatant{hitPoints = hhp}, eShield = gssh, ePoison = gspo, eRecharge = gsre} = applyEffects gs
        you' = you { mana      = ym  - sCost
                   , hitPoints = yhp + sHeal
                   }
        him' = him { hitPoints = hhp - sDmg
                   }
        --
        gs' = GameState{ manaSpent = ms + sCost
                       , p1        = you'
                       , p2        = him'
                       , eShield   = gssh + spsh
                       , ePoison   = gspo + sppo
                       , eRecharge = gsre + spre
                       }

attack gs = gs' {p1 = you'}
  where gs'@GameState{p1 = you, p2 = him} = applyEffects gs
        you' = you{hitPoints = hitPoints you - max 1 (damage him - armor you)}

dijkstra :: Set GameState -> Set GameState -> GameState
dijkstra gss ckd | deadCombatant $ p2 gs = gs                -- boss died!  We won in this game state : )
                 | deadCombatant $ p1 gs = dijkstra gss' ckd -- we died :( ... drop game state and continue search
                 | otherwise = dijkstra gss'' ckd'           -- ho. haha. guard. turn. parry. dodge. spin. ha. thrust!, etc...
  where (gs, gss') = deleteFindMin gss
        ckd'    = insert gs ckd
        gss''   = foldr insert gss' $ filter (`notMember` ckd') $ next gs
        next gs = map (round gs) spells -- cast all (valid) spells at this game state
        round gs s
          | validSpell gs s = attack . cast s $ gs
          | otherwise       = gs

solve :: Either a Combatant -> Int
solve (Right boss) = manaSpent $ dijkstra (singleton gs) empty
  where you = Combatant{hitPoints = 50, armor = 0, mana = 500, damage = 0}
        gs = GameState{manaSpent = 0, p1 = you, p2 = boss, eShield = 0, ePoison = 0, eRecharge = 0}


main :: IO ()
main = interact $ show . solve . runParser boss "<stdin>"

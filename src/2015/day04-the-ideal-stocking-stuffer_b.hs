module Main where
import Crypto.Hash             (hashWith, MD5 (..))
import Data.ByteArray.Encoding (convertToBase, Base (Base16))
import Data.ByteString         (isPrefixOf)
import Data.List               (dropWhile)
import Data.Text               (pack)
import Data.Text.Encoding      (encodeUtf8)

--	Now find one that starts with six zeroes.

solve :: String -> Int
solve cs = fst . head . dropWhile (not . isPrefixOf zeros . snd) . map md5 $ [1..]
  where str2bs = encodeUtf8 . pack
        zeros  = str2bs "000000"
        md5 n  = (n, convertToBase Base16 . hashWith MD5 . str2bs $ cs ++ show n)


main :: IO ()
main = interact $ show . solve . head . words

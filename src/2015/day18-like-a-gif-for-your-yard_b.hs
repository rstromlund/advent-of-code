module Main where
import Data.List (elem, filter)
import Data.Map.Strict (Map, elems, empty, findWithDefault, foldlWithKey', fromList, union, singleton)

--	... You flip the instructions over; Santa goes on to point out that this is all just an implementation of Conway's Game of
--	Life. At least, it was, until you notice that something's wrong with the grid of lights you bought: four lights, one in
--	each corner, are stuck on and can't be turned off. The example above will actually run like this:
--
--	Initial state:
--	##.#.#
--	...##.
--	#....#
--	..#...
--	#.#..#
--	####.#
--
--	After 1 step:
--	#.##.#
--	####.#
--	...##.
--	......
--	#...#.
--	#.####
--
--	After 2 steps:
--	#..#.#
--	#....#
--	.#.##.
--	...##.
--	.#..##
--	##.###
--
--	After 3 steps:
--	#...##
--	####.#
--	..##.#
--	......
--	##....
--	####.#
--
--	After 4 steps:
--	#.####
--	#....#
--	...#..
--	.##...
--	#.....
--	#.#..#
--
--	After 5 steps:
--	##.###
--	.##..#
--	.##...
--	.##...
--	#.#...
--	##...#
--
--	After 5 steps, this example now has 17 lights on.
--
--	In your grid of 100x100 lights, given your initial configuration, but with the four corners always in the on state, how many lights are on after 100 steps?
--
-- Test:
--	% cd ../.. && echo -e '.#.#.#\n...##.\n#....#\n..#...\n#.#..#\n####..' | cabal run 2015_day18-like-a-gif-for-your-yard_b # 17

stepCnt = 100
-- stepCnt = 5 -- for test case

gridDim = 100
-- gridDim = 6 -- for test case

neighbors = [ (-1, -1), (-1, 0), (-1, 1)
            , ( 0, -1),          ( 0, 1)
            , ( 1, -1), ( 1, 0), ( 1, 1)
            ]

solve :: String -> Int
solve cs = length . filter ('#' ==) . elems . (!! stepCnt) . iterate step $ initialState
  where initialState = union (singleton (0, 0) '#') . union (singleton (0, gridDim - 1) '#') . union (singleton (gridDim - 1, 0) '#') . union (singleton (gridDim - 1, gridDim - 1) '#') $ is
          where is = fromList . zip [(y, x) | y <- [0..(gridDim - 1)], x <- [0..(gridDim - 1)]] . filter (`elem` ".#") $ cs
        step :: Map (Int, Int) Char -> Map (Int, Int) Char
        step old = foldlWithKey' step' empty old
          where step' :: Map (Int, Int) Char -> (Int, Int) -> Char -> Map (Int, Int) Char
                step' acc k@(y, x) _
                  | (y == 0           && x == 0) ||
                    (y == 0           && x == gridDim - 1) ||
                    (y == gridDim - 1 && x == 0) ||
                    (y == gridDim - 1 && x == gridDim - 1) = union acc $ singleton k '#'
                step' acc k@(y, x) v = generate v . length . filter ('#' == ) . map getChar $ neighbors
                  where getChar (dy, dx) = findWithDefault '.' (y+dy, x+dx) old
                        generate '#' n
                          | n >= 2 && n <= 3 = union acc $ singleton k '#'
                          | otherwise        = union acc $ singleton k '.'
                        generate '.' n
                          | n == 3    = union acc $ singleton k '#'
                          | otherwise = union acc $ singleton k '.'


main :: IO ()
main = interact $ show . solve

module Main where
import Data.Void (Void)
import Text.Megaparsec (Parsec, eof, runParser, sepBy)
import Text.Megaparsec.Char (char)
import Text.Megaparsec.Char.Lexer (decimal)

--	.. The ribbon required to wrap a present is the shortest distance around its sides, or the smallest perimeter of any one
--	face. Each present also requires a bow made out of ribbon as well; the feet of ribbon required for the perfect bow is
--	equal to the cubic feet of volume of the present. Don't ask how they tie the bow, though; they'll never tell.
--
--	For example:
--	* A present with dimensions 2x3x4 requires 2+2+3+3 = 10 feet of ribbon to wrap the present plus 2*3*4 = 24 feet of ribbon for the bow, for a total of 34 feet
--	* A present with dimensions 1x1x10 requires 1+1+1+1 = 4 feet of ribbon to wrap the present plus 1*1*10 = 10 feet of ribbon for the bow, for a total of 14 feet
--
--	All numbers in the elves' list are in feet. How many total square feet of wrapping paper should they order?
--
-- Test:
--	% cd ../.. && echo -e '2x3x4' | cabal run 2015_day02-i-was-told-there-would-be-no-math_b ## 34
--	% cd ../.. && echo -e '1x1x10' | cabal run 2015_day02-i-was-told-there-would-be-no-math_b ## 14
--	% cd ../.. && echo -e '2x3x4\n1x1x10' | cabal run 2015_day02-i-was-told-there-would-be-no-math_b ## 48

type Parser = Parsec Void String

-- Two possibilities for tuple return types:

--dim :: Parser (Int, Int, Int) -- length, width, height
--dim = do
--  l <- decimal
--  _ <- char 'x'
--  w <- decimal
--  _ <- char 'x'
--  h <- decimal
--  return (l,w,h)

--dim :: Parser (Int, Int, Int) -- length, width, height
--dim = (,,) <$> decimal <* char 'x' <*> decimal <* char 'x' <*> decimal

-- This list return type is shorter and probably more readable:

dim :: Parser [Int] -- length, width, height
dim = decimal `sepBy` char 'x' <* eof

solve :: Either e [[Int]] -> Int
solve (Right bs) = sum . map area $ bs
  where area [l,w,h] = minimum sides + l*w*h
          where sides = [l+l+w+w, w+w+h+h, l+l+h+h]

main :: IO ()
main = interact $ show . solve . mapM (runParser dim "<stdin>") . lines

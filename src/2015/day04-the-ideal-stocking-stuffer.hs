module Main where
import Crypto.Hash             (hashWith, MD5 (..))
import Data.ByteArray.Encoding (convertToBase, Base (Base16))
import Data.ByteString         (isPrefixOf)
import Data.List               (dropWhile)
import Data.Text               (pack)
import Data.Text.Encoding      (encodeUtf8)

--	Santa needs help mining some AdventCoins (very similar to bitcoins) to use as gifts for all the economically forward-thinking little girls and boys.
--
--	To do this, he needs to find MD5 hashes which, in hexadecimal, start with at least five zeroes. The input to the MD5 hash
--	is some secret key (your puzzle input, given below) followed by a number in decimal. To mine AdventCoins, you must find
--	Santa the lowest positive number (no leading zeroes: 1, 2, 3, ...) that produces such a hash.
--
--	For example:
--	* If your secret key is abcdef, the answer is 609043, because the MD5 hash of abcdef609043 starts with five zeroes
--		(000001dbbfa...), and it is the lowest such number to do so.
--	* If your secret key is pqrstuv, the lowest number it combines with to make an MD5 hash starting with five zeroes is
--		1048970; that is, the MD5 hash of pqrstuv1048970 looks like 000006136ef....
--
--	Your puzzle input is bgvyzdsv.
--
-- Test:
--	% cd ../.. && echo -e 'abcdef' | cabal run 2015_day04-the-ideal-stocking-stuffer ## 609043
--	% cd ../.. && echo -e 'pqrstuv' | cabal run 2015_day04-the-ideal-stocking-stuffer ## 1048970

solve :: String -> Int
solve cs = fst . head . dropWhile (not . isPrefixOf zeros . snd) . map md5 $ [1..]
  where str2bs = encodeUtf8 . pack
        zeros  = str2bs "00000"
        md5 n  = (n, convertToBase Base16 . hashWith MD5 . str2bs $ cs ++ show n)


main :: IO ()
main = interact $ show . solve . head . words

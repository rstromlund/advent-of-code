{-# LANGUAGE TupleSections #-}
module Main where
import Data.Bits ((.&.), (.|.), complement, shiftL, shiftR)
import qualified Data.Map.Strict as M (Map, (!?), empty, insert)
import Data.Void (Void)
import Data.Word (Word16)
import Text.Megaparsec ((<|>), Parsec, choice, eof, runParser, some, try)
import Text.Megaparsec.Char (letterChar, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	This year, Santa brought little Bobby Tables a set of wires and bitwise logic gates!  Unfortunately, little Bobby is a little
--	under the recommended age range, and he needs help assembling the circuit.
--
--	Each wire has an identifier (some lowercase letters) and can carry a 16-bit signal (a number from 0 to 65535).  A
--	signal is provided to each wire by a gate, another wire, or some specific value. Each wire can only get a signal from one
--	source, but can provide its signal to multiple destinations.  A gate provides no signal until all of its inputs have a signal.
--
--	The included instructions booklet describes how to connect the parts together: x AND y -> z means to connect wires x and y
--	to an AND gate, and then connect its output to wire z.
--
--	For example:
--	* 123 -> x means that the signal 123 is provided to wire x.
--	* x AND y -> z means that the bitwise AND of wire x and wire y is provided to wire z.
--	* p LSHIFT 2 -> q means that the value from wire p is left-shifted by 2 and then provided to wire q.
--	* NOT e -> f means that the bitwise complement of the value from wire e is provided to wire f.
--
--	Other possible gates include OR (bitwise OR) and RSHIFT (right-shift).  If, for some reason, you'd like to emulate the
--	circuit instead, almost all programming languages (for example, C, JavaScript, or Python) provide operators for these gates.
--
--	For example, here is a simple circuit:
--	123 -> x
--	456 -> y
--	x AND y -> d
--	x OR y -> e
--	x LSHIFT 2 -> f
--	y RSHIFT 2 -> g
--	NOT x -> h
--	NOT y -> i
--
--	After it is run, these are the signals on the wires:
--	d: 72
--	e: 507
--	f: 492
--	g: 114
--	h: 65412
--	i: 65079
--	x: 123
--	y: 456
--
--	In little Bobby's kit's instructions booklet (provided as your puzzle input), what signal is ultimately provided to wire a?
--
-- Test:
--	% cd ../.. && echo -e '123 -> x\n456 -> y\nx AND y -> d\nx OR y -> e\nx LSHIFT 2 -> f\ny RSHIFT 2 -> g\nNOT x -> h\nNOT y -> a' | cabal run 2015_day07-some-assembly-required ## 65079

data Term = Symbol String | Number Word16 deriving (Show, Ord, Eq)
data Instruction = Assign Term Term | AND Term Term Term | OR Term Term Term | LSHIFT Term Term Term | RSHIFT Term Term Term | NOT Term Term deriving (Show, Ord, Eq)
type Variables = M.Map Term Term

type Parser = Parsec Void String

instructions :: Parser Instruction
instructions = instruction <* eof

instruction :: Parser Instruction
instruction =
  NOT <$> (string "NOT " *> term) <*> (string " -> " *> term)
  <|> try opInfix -- there has to be a more idomatic haskell way.
  <|> Assign <$> term <* string " -> " <*> term
  where
    opInfix :: Parser Instruction
    opInfix = do
      op1 <- term
      act <- choice [AND <$ string " AND ", OR <$ string " OR ", LSHIFT <$ string " LSHIFT ", RSHIFT <$ string " RSHIFT "]
      op2 <- term
      string " -> "
      act op1 op2 <$> term

term :: Parser Term
term = choice [ Number <$> decimal
              , Symbol <$> some letterChar
              ]

solve :: Either a [Instruction] -> Maybe Term
solve (Right acts) = reduce ((acts, M.empty), 0)
  where reduce (([], vm), _) = vm M.!? Symbol "a" -- reduced completely.
        reduce ((is, vm), cnt) | cnt > 200 = undefined -- safety net to ensure pgm termination.
        reduce st@((is, vm), cnt) = reduce . (, cnt + 1) . foldl' step ([], vm) $ is -- reduce until done.
        --
        reduce' _ _ (Just acc) = acc
        reduce' (is, vm) i Nothing = (i:is, vm)
        --
        step :: ([Instruction], Variables) -> Instruction -> ([Instruction], Variables)
        step (is, vm) i@(Assign op1 res) = reduce' (is, vm) i $ do
          v <- getVal vm op1
          return (is, M.insert res v vm)
        -- NOT:
        step (is, vm) i@(NOT op1 res) = reduce' (is, vm) i $ do
          (Number v1) <- getVal vm op1
          return (is, M.insert res (Number $ complement v1) vm)
        -- AND:
        step (is, vm) i@(AND op1 op2 res) = reduce' (is, vm) i $ do
          (Number v1) <- getVal vm op1
          (Number v2) <- getVal vm op2
          return (is, M.insert res (Number $ v1 .&. v2) vm)
        -- OR:
        step (is, vm) i@(OR op1 op2 res) = reduce' (is, vm) i $ do
          (Number v1) <- getVal vm op1
          (Number v2) <- getVal vm op2
          return (is, M.insert res (Number $ v1 .|. v2) vm)
        -- LSHIFT:
        step (is, vm) i@(LSHIFT op1 op2 res) = reduce' (is, vm) i $ do
          (Number v1) <- getVal vm op1
          (Number v2) <- getVal vm op2
          return (is, M.insert res (Number $ v1 `shiftL` fromIntegral v2) vm)
        -- RSHIFT:
        step (is, vm) i@(RSHIFT op1 op2 res) = reduce' (is, vm) i $ do
          (Number v1) <- getVal vm op1
          (Number v2) <- getVal vm op2
          return (is, M.insert res (Number $ v1 `shiftR` fromIntegral v2) vm)
        --
        getVal vm v@(Number _) = Just v
        getVal vm t@(Symbol _) = vm M.!? t


main :: IO ()
main = interact $ show . solve . mapM (runParser instructions "<stdin>") . lines

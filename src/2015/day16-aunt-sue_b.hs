module Main where
import Data.List (takeWhile)
import Data.Map.Strict (Map, foldlWithKey', fromList, notMember, (!))
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, some, optional, runParser)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... it has an outdated retroencabulator, and so the output from the machine isn't exact values - some of them indicate ranges.
--
--	In particular, the cats and trees readings indicates that there are greater than that many (due to the unpredictable
--	nuclear decay of cat dander and tree pollen), while the pomeranians and goldfish readings indicate that there are fewer
--	than that many (due to the modial interaction of magnetoreluctance).
--
--	What is the number of the real Aunt Sue?
--
-- Test:
--	% cd ../.. && echo -e 'Sue 7: pomeranians: 5, samoyeds: 0, perfumes: 10' | cabal run 2015_day16-aunt-sue_b # debug parse

type Aunt = Map String Int -- e.g. cars: 9
type Parser = Parsec Void String

aunts :: Parser [Aunt]
aunts = some aunt <* eof

aunt :: Parser Aunt
aunt = word *> char ' ' *> decimal *> string ": " *> (fromList <$> some items) <* optional newline
  where word = some letterChar
        items :: Parser (String, Int)
        items = (,) <$> word <* string ": " <*> decimal <* optional (string ", ")

mysteryAunt :: Aunt
mysteryAunt = fromList [("children", 3), ("cats", 7), ("samoyeds", 2), ("pomeranians", 3), ("akitas", 0), ("vizslas", 0), ("goldfish", 5), ("trees", 3), ("cars", 2), ("perfumes", 1)]

solve :: Either a [Aunt] -> Int
solve (Right as) = (1 +) . length . takeWhile (False ==) . map (foldlWithKey' compare True) $ as
  where compare :: (Bool -> String -> Int -> Bool)
        compare acc lbl val
          | not acc = False
          | lbl `notMember` mysteryAunt = acc
          | otherwise = compare' lbl val (mysteryAunt ! lbl) -- we know lbl exists, skip (!?) maybe value
        compare' "cats"        val mval = val > mval
        compare' "trees"       val mval = val > mval
        compare' "pomeranians" val mval = val < mval
        compare' "goldfish"    val mval = val < mval
        compare' lbl           val mval = val == mval -- all other items must match quantities.


main :: IO ()
main = interact $ show . solve . runParser aunts "<stdin>"

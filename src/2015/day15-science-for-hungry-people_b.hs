module Main where
import Data.List (maximum, transpose, zip, zipWith)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... another recipe that has exactly 500 calories per cookie (so they can use it as a meal replacement). Keep the rest of
--	your award-winning process the same (100 teaspoons, same ingredients, same scoring system).
--
--	For example, given the ingredients above, if you had instead selected 40 teaspoons of butterscotch and 60 teaspoons of
--	cinnamon (which still adds to 100), the total calorie count would be 40*8 + 60*3 = 500. The total score would go down,
--	though: only 57600000, the best you can do in such trying circumstances.
--
--	Given the ingredients in your kitchen and their properties, what is the total score of the highest-scoring cookie you can
--	make with a calorie total of 500?
--
-- Test:
--	% cd ../.. && echo -e 'Butterscotch: capacity -1, durability -2, flavor 6, texture 3, calories 8\nCinnamon: capacity 2, durability 3, flavor -2, texture -1, calories 3' | cabal run 2015_day15-science-for-hungry-people_b # 50633660

type Recipe = (String, [Int], Int) -- ingredient, [capacity, durability, flavor, texture], calories
type Parser = Parsec Void String

recipes :: Parser [Recipe]
recipes = many recipe <* eof

recipe :: Parser Recipe
recipe = mkRecipe <$> ingredient
  <* string ": capacity " <*> value
  <* string ", durability " <*> value
  <* string ", flavor " <*> value
  <* string ", texture " <*> value
  <* string ", calories " <*> value
  <* optional newline
  where ingredient = choice [string "Frosting", string "Candy", string "Butterscotch", string "Sugar", string "Cinnamon"]
        value = f <$> optional (char '-') <*> decimal
          where f (Just '-') s = -s
                f Nothing    s = s
        mkRecipe ing cap dur flav txt cal = (ing, [cap, dur, flav, txt], cal)

solve :: Either a [Recipe] -> Int
solve (Right rs) = fst . maximum . filter ((500 ==) . snd) . map scenario $ tspPermutaions
  where tspPermutaions = [[t1, t2, t3, 100 - t1 - t2 - t3] | t1 <- [1..99], t2 <- [1..99], t3 <- [1..99], t1 + t2 + t3 < 100]
        -- ^^^ hardcoded to my input of 4 ingredients; produce #combinations based on input and not hardcoding.
        --
        scenario tsps = (val, cal)
          where val = product . map (max 0 . sum) . transpose . zipWith score tsps $ rs
                cal = sum . zipWith (\tsp (_, _, cal) -> tsp * cal) tsps $ rs
        score :: Int -> Recipe -> [Int]
        score tsp (_, as@[cap, dur, flav, txt], _) = map (tsp *) as
        add :: [Int] -> [Int] -> [Int]
        add = zipWith (\i1 i2 -> max 0 $ i1 + i2)


main :: IO ()
main = interact $ show . solve . runParser recipes "<stdin>"

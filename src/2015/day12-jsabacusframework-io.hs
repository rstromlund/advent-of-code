module Main where
import Data.Char (isNumber)
import Data.Functor (($>))
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, noneOf, optional, runParser, some)
import Text.Megaparsec.Char (char, numberChar)
import Text.Megaparsec.Char.Lexer (decimal)

--	... They have a JSON document which contains a variety of things: arrays ([1,2,3]), objects ({"a":1, "b":2}), numbers, and
--	strings. Your first job is to simply find all of the numbers throughout the document and add them together.
--
--	For example:
--	* [1,2,3] and {"a":2,"b":4} both have a sum of 6.
--	* [[[3]]] and {"a":{"b":4},"c":-1} both have a sum of 3.
--	* {"a":[-1,1]} and [-1,{"a":1}] both have a sum of 0.
--	* [] and {} both have a sum of 0.
--
--	You will not encounter any strings containing numbers.
--
--	What is the sum of all numbers in the document?
--
-- Test:
--	% cd ../.. && echo -e '[1,2,3]' | cabal run 2015_day12-jsabacusframework-io ## 6
--	% cd ../.. && echo -e '{"a":2,"b":4}' | cabal run 2015_day12-jsabacusframework-io ## 6
--	% cd ../.. && echo -e '[[[3]]]' | cabal run 2015_day12-jsabacusframework-io ## 3
--	% cd ../.. && echo -e '{"a":{"b":4},"c":-1}' | cabal run 2015_day12-jsabacusframework-io ## 3
--	% cd ../.. && echo -e '{"a":[-1,1]}' | cabal run 2015_day12-jsabacusframework-io ## 0
--	% cd ../.. && echo -e '[-1,{"a":1}]' | cabal run 2015_day12-jsabacusframework-io ## 0
--	% cd ../.. && echo -e '[]' | cabal run 2015_day12-jsabacusframework-io ## 0
--	% cd ../.. && echo -e '{}' | cabal run 2015_day12-jsabacusframework-io ## 0

type Parser = Parsec Void String

nbrs :: Parser [Int]
nbrs = optional notNbr *> many nbr <* eof

nbr :: Parser Int
nbr = (f <$> optional (char '-') <*> decimal) <* notNbr
  where f Nothing x = x
        f _       x = negate x

notNbr :: Parser ()
notNbr = many (noneOf ('+':'-':['0'..'9'])) $> ()

solve :: Either a [Int] -> Int
solve (Right is) = sum is


main :: IO ()
main = interact $ show . solve . runParser nbrs "<stdin>" . head . lines

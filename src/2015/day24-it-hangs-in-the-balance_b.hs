module Main where
import Data.List (sort, subsequences)
import Data.List.Extra (minimumOn)

--	Balance the sleigh again, but this time, separate the packages into four groups
--	instead of three. The other constraints still apply.
--
--	Given the example packages above, this would be some of the new unique first
--	groups, their quantum entanglements, and one way to divide the remaining packages:
--
--	11 4    (QE=44); 10 5;   9 3 2 1; 8 7
--	10 5    (QE=50); 11 4;   9 3 2 1; 8 7
--	9 5 1   (QE=45); 11 4;   10 3 2;  8 7
--	9 4 2   (QE=72); 11 3 1; 10 5;    8 7
--	9 3 2 1 (QE=54); 11 4;   10 5;    8 7
--	8 7     (QE=56); 11 4;   10 5;    9 3 2 1
--
--	Of these, there are three arrangements that put the minimum (two) number of
--	packages in the first group: 11 4, 10 5, and 8 7. Of these, 11 4 has the lowest
--	quantum entanglement, and so it is selected.
--
--	Now, what is the quantum entanglement of the first group of packages in the ideal configuration?
--
--	What is the quantum entanglement of the first group of packages in the ideal configuration?
--
-- Test:
--	% cd ../.. && echo -e '1 2 3 4 5 7 8 9 10 11' | cabal run 2015_day24-it-hangs-in-the-balance ## 44

solve :: [Int] -> Int
solve is = ideal
  where bagWt = sum is `div` 4
        ideal = snd . minimumOn snd . (\b -> let sz = fst . head $ b in filter ((== sz) . fst) b) . sort $ bag1
        bag1 = [(length s, product s) | s <- subsequences is, bagWt == sum s]


main :: IO ()
main = interact $ show . solve . map read . words

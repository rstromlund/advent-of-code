module Main where
import Data.List (nub)
import Data.Void (Void)
import Text.Megaparsec (Parsec, choice, eof, many, optional, runParser)
import Text.Megaparsec.Char (char, newline)

--	The next year, to speed up the process, Santa creates a robot version of himself, Robo-Santa, to deliver presents with him.
--
--	Santa and Robo-Santa start at the same location (delivering two presents to the same starting house), then take turns
--	moving based on instructions from the elf, who is eggnoggedly reading from the same script as the previous year.
--
--	This year, how many houses receive at least one present?
--
--	For example:
--	* ^v delivers presents to 3 houses, because Santa goes north, and then Robo-Santa goes south.
--	* ^>v< now delivers presents to 3 houses, and Santa and Robo-Santa end up back where they started.
--	* ^v^v^v^v^v now delivers presents to 11 houses, with Santa going one direction and Robo-Santa going the other.
--
-- Test:
--	% cd ../.. && echo -e '^v' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum_b ## 3
--	% cd ../.. && echo -e '^>v<' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum_b ## 3
--	% cd ../.. && echo -e '^v^v^v^v^v' | cabal run 2015_day03-perfectly-spherical-houses-in-a-vacuum_b ## 11

type Parser = Parsec Void String

deliver :: Parser [(Int, Int)] -- (x-dir, y-dir)
deliver = many move <* optional newline <* eof

move :: Parser (Int, Int)
move = choice
  [ (0, 1) <$ char '^'
  , (0,-1) <$ char 'v'
  , (1, 0) <$ char '>'
  , (-1,0) <$ char '<' ]

solve :: Either e [(Int, Int)] -> Int
solve (Right ms) = length . nub . uncurry (++) . foldl' santa ([(0,0)], [(0,0)]) $ ms
  where santa (l@((x,y):_), r) (xd,yd) = (r, (x+xd, y+yd):l)


main :: IO ()
main = interact $ show . solve . runParser deliver "<stdin>"

module Main where
import Data.List (filter, find, init, nub, permutations, tails, take)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, eof, many, runParser)
import Text.Megaparsec.Char (letterChar, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... He can start and end at any two (different) locations he wants, but he must visit each location exactly once. What is
--	the shortest distance he can travel to achieve this?
--
--	For example, given the following distances:
--	* London to Dublin = 464
--	* London to Belfast = 518
--	* Dublin to Belfast = 141
--
--	The possible routes are therefore:
--	* Dublin -> London -> Belfast = 982
--	* London -> Dublin -> Belfast = 605
--	* London -> Belfast -> Dublin = 659
--	* Dublin -> Belfast -> London = 659
--	* Belfast -> Dublin -> London = 605
--	* Belfast -> London -> Dublin = 982
--
--	The shortest of these is London -> Dublin -> Belfast = 605, and so the answer is 605 in this example.
--
--	What is the distance of the shortest route?
--
-- Test:
--	% cd ../.. && echo -e 'London to Dublin = 464\nLondon to Belfast = 518\nDublin to Belfast = 141' | cabal run 2015_day09-all-in-a-single-night ## 605

type Parser = Parsec Void String

route :: Parser ((String, String), Int)
route = do
  l1 <- location
  string " to "
  l2 <- location
  string " = "
  dist <- decimal
  eof
  return ((l1, l2), dist)

location :: Parser String
location = many letterChar

solve :: Either a [((String, String), Int)] -> Int
solve (Right rs) = minimum . map distance . filter (/= Nothing) . map sequence $ roads
  where distinctLocs :: [String] -- Each city to visit
        distinctLocs = nub . foldl' (\acc ((l1, l2), _) -> l1:l2:acc) [] $ rs
        distance (Just rs') = sum . map snd $ rs'
        --
        roads :: [[Maybe ((String, String), Int)]] -- All permutations of roads to our cities
        roads = map (map findRoad . filter ((==) 2 . length) . map (take 2) . tails) . permutations $ distinctLocs
          -- If this road exits, return Just city pair and distance, otherwise return Nothing.
          where findRoad [p1, p2] = find (\((r1, r2), _) -> (p1 == r1 && p2 == r2) || (p1 == r2 && p2 == r1)) rs


main :: IO ()
main = interact $ show . solve . mapM (runParser route "<stdin>") . lines

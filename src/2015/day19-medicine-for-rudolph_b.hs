module Main where
import Debug.Trace (trace)
import Data.Char (isUpper)
import Data.List (isInfixOf, isPrefixOf, last, maximum, nub, reverse, sortOn, splitAt, stripPrefix)
import Data.List.Utils (subIndex)
import Data.Maybe (fromMaybe)
import Data.Set (Set, empty, fromList, singleton)
import Data.Void (Void)
import Text.Megaparsec ((<|>), Parsec, choice, eof, many, optional, runParser, some)
import Text.Megaparsec.Char (char, letterChar, newline, string)
import Text.Megaparsec.Char.Lexer (decimal)

--	... Molecule fabrication always begins with just a single electron, e, and applying replacements one at a time, just like the ones during calibration.
--
--	For example, suppose you have the following replacements:
--	* e => H
--	* e => O
--	* H => HO
--	* H => OH
--	* O => HH
--
--	If you'd like to make HOH, you start with e, and then make the following replacements:
--	* e => O to get O
--	* O => HH to get HH
--	* H => OH (on the second H) to get HOH
--
--	So, you could make HOH after 3 steps. Santa's favorite molecule, HOHOHO, can be made in 6 steps.
--
--	How long will it take to make the medicine? Given the available replacements and the medicine molecule in your puzzle
--	input, what is the fewest number of steps to go from e to the medicine molecule?
--
-- Test:
--	% cd ../.. && echo -e 'e => H\ne => O\nH => HO\nH => OH\nO => HH\n\nHOH' | cabal run 2015_day19-medicine-for-rudolph_b # 3
--	% cd ../.. && echo -e 'e => H\ne => O\nH => HO\nH => OH\nO => HH\n\nHOHOHO' | cabal run 2015_day19-medicine-for-rudolph_b # 6

type Results = Set (String, Int)

type Replacement = (String, String)
type Calibration = ([Replacement], String) -- replacement pairs, molecule

type Parser = Parsec Void String

calibration :: Parser Calibration
calibration = (,) <$> many replacement <* newline <*> some letterChar <* optional newline <* eof

replacement :: Parser Replacement
replacement = (,) <$> some letterChar <* string " => " <*> some letterChar <* newline

-- Thanks to https://www.reddit.com/r/adventofcode/comments/3xflz8/day_19_solutions/
-- Reddit gave some very intelligent insights based on analyzing the inputs ... wow.

solve :: Either a Calibration -> Int
solve (Right (rs, mol)) = cntUpper - cntStr "Rn" - cntStr "Ar" - 2 * cntStr "Y" - 1
  where cntUpper = length . filter isUpper $ mol
        cntStr pat = cntStr' 0 mol
          where cntStr' :: Int -> String -> Int
                cntStr' n str = let pos = pat `subIndex` str
                                in case pos of
                                     (Just p) -> cntStr' (n + 1) $ drop (p + length pat) str
                                     Nothing  -> n


main :: IO ()
main = interact $ show . solve . runParser calibration "<stdin>"

#!/usr/bin/env bash
exec docker run --rm -it -v "$HOME/.stack:/root/.stack" -v "$PWD:/app" -w /app --entrypoint /bin/bash amd64/haskell
exit ${?}
